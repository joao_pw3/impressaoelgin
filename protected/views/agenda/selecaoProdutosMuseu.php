<?php
use yii\jui\Slider;
use yii\helpers\Url;
use \app\assets\MapaAsset;
use app\assets\EventoAsset;
use app\models\Tags;

MapaAsset::register($this);
EventoAsset::register($this);

?>
<div class="col-md-9 semPad semMargin" id="boxdomapa">
    <div class="blocoIngr1" id="holder-mapa">
        <div class="div-loading">
            <div id="img-loading"></div>
        </div>
        <?php if(isset($_GET['produtos']) && $_GET['produtos']=='dropdown')
            echo $this->render('/produtos/selecaoProdutosDropdown',['produtos'=>$evento->produtos]);
        ?>
		<div style="text-align:center;">
			<br>
			<p>Informe a quantidade desejada no box ao lado.</p>
            <img src="<?=Yii::getAlias("@web")?>/images/museu.jpg" style="width:70%;">
		</div>
    </div>
</div>

<div class="col-md-3 semPad semMargin">
    <input type="hidden" id="url-add-produto" value="<?= Url::to(['compra/adicionar-produto-carrinho']); ?>">
    <input type="hidden" id="url-del-produto" value="<?= Url::to(['compra/remover-produto-carrinho']); ?>">
    <input type="hidden" id="url-ocupantes" value="<?= Url::to(['compra/ingresso']); ?>">
    <input type="hidden" id="url-qtd-produto" value="<?= Url::to(['agenda/qtde-produtos']); ?>">
    <input type="hidden" id="evento" value="<?= $_GET['evento']; ?>">
    <input type="hidden" id="data" value="<?=$evento->datas[0]->id?>">
    <span id="setaEsq"></span>
    <div class="blocoIngr2 font-assentos row">
        <h2>Ingressos</h2>
        <hr style="margin-top:10px; margin-bottom:10px; border-bottom: groove 2px #fff!important;">
        <div class="caixa-assentos barScroll semEscolhaAss"> 
			<div class="assQtd">
                <div><span style="font-family:'FontAwesome';">&#xf145;</span> Quantidade:</div><br>
                <input id="qtdeIngressos" type="number" min="1" max="20" value="1" style="text-align:center;">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 totProd" id="total-itens-carrinho">
                <i class="material-icons text-left" title="Carrinho de compras" style="font-size:14px;">shopping_cart</i>
                <span class="itensNoCarrinho"></span> 
            </div> 
            <div class="col-sm-6">
                <button id="btn-assentos" class="btn btn-primary btn-assentos" disabled>Continuar</button>
            </div> 
		</div>
    </div>
</div>
<br clear="all">
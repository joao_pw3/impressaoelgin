<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use yii\bootstrap\Modal;

Modal::begin([
    'header' => '<h4 align="center" id="title-fileira">Fileiras</h4>',
    'size'   => 'modal-md',
    'id'     => 'modal-fileira',
    'options'=> [
        'class' => 'modal-center'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ]
]);

$tags->scenario = 'create';

$form = ActiveForm::begin([
    'id' => 'fileira-form', 
    'action' => Url::to(['produto/nova-fileira'])
]); ?>

<?php $model->ativo = '1'; ?>

<?= $form->field($model, 'id_agenda')->hiddenInput(['id' => 'id_agenda'])->label(false); ?>
<?= $form->field($model, 'nome')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'valor')->hiddenInput(['value' => '0'])->label(false); ?>
<?= $form->field($model, 'descricao')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'ativo')->radioList(['1' => 'Ativo', '0' => 'Inativo'])->label('Status'); ?>
<?= $form->field($tags, '[0]nome')->hiddenInput(['value' => 'matriz'])->label(false); ?>
<?= $form->field($tags, '[0]valor')->hiddenInput(['id' => 'tag-matriz'])->label(false); ?>
<?= $form->field($tags, '[1]nome')->hiddenInput(['value' => 'setor'])->label(false); ?>
<?= $form->field($tags, '[1]valor')->hiddenInput(['id' => 'tag-setor'])->label(false); ?>
<?= $form->field($tags, '[2]nome')->hiddenInput(['value' => 'tipo'])->label(false); ?>
<?= $form->field($tags, '[2]valor')->hiddenInput(['value' => 'fileira'])->label(false); ?>

<div class="row">
    <div class="col-xs-6">
        <?= $form->field($tags, '[3]nome')->hiddenInput(['value' => 'fileira'])->label(false); ?>
        <?= $form->field($tags, '[3]valor')->textInput()->widget(MaskedInput::className(), [
            'mask' => '**',
            'options' => [
                'class' => 'form-control',
                'placeholder' => 'Letra inicial da fileira',
                'title' => 'Letra inicial da fileira',
                'id' => 'fileira-ini'
            ],
            'clientOptions' => [
                'removeMaskOnSubmit' => true
            ]
        ])->label('de fileira:');?>
    </div>
    <div class="col-xs-6">
        <?= $form->field($tags, '[4]nome')->hiddenInput(['value' => 'fileira'])->label(false); ?>
        <?= $form->field($tags, '[4]valor')->textInput()->widget(MaskedInput::className(), [
            'mask' => '**',
            'options' => [
                'class' => 'form-control',
                'placeholder' => 'Letra final da fileira',
                'title' => 'Letra final da fileira',
                'id' => 'fileira-fim'
            ],
            'clientOptions' => [
                'removeMaskOnSubmit' => true
            ]
        ])->label('até fileira:');?>
    </div>
</div>

<?= Html::submitButton('Cadastrar', ['class' => 'btn btn-primary', 'id' => 'btn-nova-fileira']); ?>

<?php ActiveForm::end(); ?>

<?php Modal::end(); ?>
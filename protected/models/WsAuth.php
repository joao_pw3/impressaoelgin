<?php
namespace app\models;
use app\models\Ws;

class WsAuth extends Ws
{
    public function _post($post)
    {	
        return json_decode($this->call('auth/', 'POST', $post, true, 'A'));
    }

    public function listarUnidades()
    {	
        return json_decode($this->call('auth/', 'POST', [], true));
    }    
}
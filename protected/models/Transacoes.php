<?php
namespace app\models;

use Yii;
use app\models\WsCarrinho;

/**
 * Classe model Transacoes
 * Esta classe é a responsável por fazer a requisição à API do pagtudo e retornar uma array com as transações por dia
 * @var string $dataDe data inicial da consulta à api
 * @var string $dataAte data final da consulta à api
 * @var array $retorno lista de transações formatada e com os filtros aplicados para exibição ao usuário final
 * @const TAXA_CRED_VISTA a taxa aplicada para vendas à vista
 * @const TAXA_CRED_PARCELADO a taxa aplicada para vendas parceladas
*/
class Transacoes{
	public $dataInicial;
	public $dataFinal;
	public $timestampInicio;

	public $transacoes=[];

	public $total=0;

	public $visao='compras';
        public $unidade;

	const TAXA_DEB = 5;
	const TAXA_CRED_VISTA = 5;
	const TAXA_CRED_PARCELADO = 5;
	
	const DIAS_VCTO_DEBITO = 5;
	const DIAS_VCTO_CREDITO_VISTA = 33;
	const DIAS_VCTO_CREDITO_PARCELAS = 30;

	/**
	 * Método construtor. Por enquanto, apenas atribui o timestamp do dia 01/11/2017 
	*/
	public function __construct(Cliente $unidade){
            $this->timestampInicio=mktime(0,0,0,11,01,2017);
            $this->unidade = $unidade;
	}

	/**
	 * retornar as transações em ordem
	*/
	public function transacoesPeriodo($naoAutorizados=false){
            //$apis = $this->consultaApis();
            //$merge = array_merge($apis[0],$apis[1]);
            $paguetudo=$this->paguetudo($this->dataInicial->getTimeStamp(),$this->dataFinal->getTimeStamp(),$naoAutorizados,false);
            foreach($paguetudo as $k=>$venda){
                if($k==='erro') continue;
                try{
                    $c=$this->calculos($venda);
                    if(preg_match('/D([0-9]{1,})S([0-9]{1,})N/i', $c['promocode'])==true)
                        $c['solicitacao']=$this->getInfoSolicitacao($c['promocode']);
                    $this->transacoes[]=$c;
                } catch (yii\base\ErrorException $e){
                    echo '<h2>Erro!</h2>';
                    print_r($e->getMessage());
                    echo '<br />';
                    echo 'Registro: ';
                    echo '<pre>';
                    print_r($venda);
                    echo '</pre>';
                }
            }
	}

    /**
	 * Consulta à API paguetudo 
	 * @param int $time1 timestamp da data inicial
	 * @param int $time2 timestamp da data final
	 * @param boolean $incluirNaoAutorizado se o retorno deve trazer também transações que não foram autorizadas. Padrão false
	 * @param boolean $incluirDistribuidores se deve incluir compras com promocode de distribuidores. Padrão false
	 * @return array com cada transação
	 * @throws Exception se o retorno da propriedade successo após a consulta for diferente de 1
	*/
	public function paguetudo($time1,$time2,$incluirNaoAutorizado=false,$incluirDistribuidores=false){
            $session = Yii::$app->session;
            $session->open();

            $data1 = date('d-m-Y\TH:i:s',$time1);
            $data2 = date('d-m-Y\TH:i:s',$time2);
            //$api = new Paguetudo();
            $vendas = (new WsCarrinho($this->unidade->user_token))->consultarVenda($data1, $data2);
            $transacoes=[];
            if($vendas->successo != true)
                return $transacoes;
			//throw new \Exception($vendas->erro->mensagem, $vendas->erro->codigo);		
            foreach($vendas->objeto as $obj){
                $v = $obj->vendas;
                $descricao = explode('|', $v->descricao);
                $promocode = isset($descricao[3]) ?$descricao[3] :null;
                $qtdProdutos=[];
                if(isset($descricao[1])){
                    $produtos=explode(';', $descricao[1]);
                    $qtdProdutos=['passaportes'=>$produtos[0]];/*,'estacionamento'=>$produtos[1],'combo'=>$produtos[2]];*/
                }
                if($incluirDistribuidores==false && preg_match('/D([0-9]{1,})S([0-9]{1,})N/i', $promocode)==true)
                    continue;
                if($v->status != 'Autorizado' && $incluirNaoAutorizado==false)
                        continue;
                if($v->total == 0)		
                        continue;
                $transacoes[]=[
	            'codigo'=>$v->codigo,
	            'status'=>$v->status,
	            'codigo_status'=>$v->codigo_status,
			    'tipo'=>'Online',
	            'dataTransacao'=>$v->data,
	            'tipoTransacao'=>$v->tipo,
	            'idConciliacao'=>$v->id_conciliacao,
	            'parcelas'=>$v->forma,
	            'valor'=>$v->total,
	            'produtos'=>$qtdProdutos,
	            'promocode'=>$promocode,
	            'carrinho'=>$obj->id_carrinho,
	            'comprador'=>[
	            	'nome'=>$v->comprador->nome,
		            'documento'=>$v->comprador->documento,
	            	'bandeira_cartao'=>$v->comprador->cartao->bandeira,
		            'email'=>$v->comprador->email,
		            'telefone'=>$v->comprador->telefone,
		            'cep'=>$v->comprador->cep,
		            'endereco'=>$v->comprador->endereco,
		            'numero'=>$v->comprador->numero,
		            'complemento'=>$v->comprador->complemento,
		            'cidade'=>$v->comprador->cidade,
		            'estado'=>$v->comprador->uf,
		            'bairro'=>$v->comprador->bairro,
		        ]
		    ];
	    }
	    return $transacoes;
	}	

	/**
	 * Calcula taxas e data de vencimento. Em caso de compra parcelada, cada parcela representa uma linha
	 * Se a var $visao for vencimentos, mostrar somente registros onde a data de vencimento esteja no intervalo
	 * @param array $venda informações de uma venda
	 * @return array informações da venda com taxas aplicadas e parcelas separadas
	*/
	public function calculos($venda){
        $dataCompra = \DateTime::createFromFormat('d/m/Y G:i:s',$venda['dataTransacao']);
		if($venda['parcelas'] == '1'){
        	$dataVencimento = clone $dataCompra;
			$taxa = $venda['tipoTransacao'] == 'debito' ?self::TAXA_DEB :self::TAXA_CRED_VISTA;
			$intervalo = $venda['tipoTransacao'] == 'debito' ?self::DIAS_VCTO_DEBITO :self::DIAS_VCTO_CREDITO_VISTA;
            $dataVencimento->add(new \DateInterval('P'.$intervalo.'D'));
			if($this->visao == 'vencimentos' && ($dataVencimento->getTimestamp()>$this->dataFinal->getTimestamp() || $dataVencimento->getTimestamp()<$this->dataInicial->getTimestamp())) {
				return null;
			}
            
            $vlrLiquido = $venda['valor']-($venda['valor']*$taxa/100);
            $this->total += $vlrLiquido;
            //$comprador=$this->infoComprador($venda['carrinho'],$venda['comprador'],$venda['idConciliacao']);
            return[
            		'dataTransacao'=>$dataCompra,
            		'codigo'=>$venda['codigo'],
            		'status'=>$venda['status'],
            		'codigo_status'=>$venda['codigo_status'],
            		'comprador'=>$venda['comprador'],
            		'carrinho'=>$venda['carrinho'],
            		'tipo'=>$venda['tipo'],
            		'dataVencimento'=>$dataVencimento,
            		'codTransacao'=>$venda['codigo'],
            		'bruto'=>$venda['valor'],
            		'valor'=>$vlrLiquido,
            		'formaPgto'=>$venda['tipoTransacao'],
            		'idConciliacao'=>$venda['idConciliacao'],
            		'parcelas'=>$venda['parcelas'],
            		'produtos'=>$venda['produtos'],
            		'promocode'=>$venda['promocode'],
            		'detalhes'=>false,
            	]
            ;
        } else{
            if(!is_numeric($venda['valor']) || !is_numeric($venda['parcelas'])) throw new \Exception("Valor não numérico na transação ".$venda['codigo'],5);
            $taxa = self::TAXA_CRED_PARCELADO*$venda['parcelas'];
            $vlrParcela = $venda['valor']/$venda['parcelas'];
            $arrDetalhes=[];
            $vlrLiquidoTotal=0;
        	$dataVencimentoParcela1 = clone $dataCompra;
            $dataVencimentoParcela1->add(new \DateInterval('P33D'));
            for($parcela=1;$parcela <= $venda['parcelas'];$parcela++) { 
        		$dataVencimento = clone $dataCompra;
                $intervalo = self::DIAS_VCTO_CREDITO_VISTA + (self::DIAS_VCTO_CREDITO_PARCELAS*($parcela-1)); 
                $dataVencimento->add(new \DateInterval('P'.$intervalo.'D'));
                if($this->visao == 'vencimentos' && ($dataVencimento->getTimestamp()>$this->dataFinal->getTimestamp() || $dataVencimento->getTimestamp()<$this->dataInicial->getTimestamp())) {
					continue;
				}
				
                $vlrLiquido = $vlrParcela-($vlrParcela*self::TAXA_CRED_PARCELADO/100);
            	$vlrLiquidoTotal += $vlrLiquido;
                $range = range('A','Z');
                $this->total += $vlrLiquido;
            	$arrDetalhes[]=[
            		'dataVencimento'=>$dataVencimento,
            		'codTransacao'=>$venda['codigo'].$range[$parcela-1],
            		'bruto'=>$vlrParcela,
            		'valor'=>$vlrLiquido,
            		'parcela'=>$parcela,
            	];
            } 
            return[
            		'dataTransacao'=>$dataCompra,
            		'codigo'=>$venda['codigo'],
            		'status'=>$venda['status'],
            		'codigo_status'=>$venda['codigo_status'],
            		'comprador'=>$venda['comprador'],
            		'carrinho'=>$venda['carrinho'],
            		'tipo'=>$venda['tipo'],
            		'dataVencimento'=>$dataVencimentoParcela1,
            		'codTransacao'=>$venda['codigo'],
            		'bruto'=>$venda['valor'],
            		'valor'=>$vlrLiquidoTotal,
            		'formaPgto'=>$venda['tipoTransacao'],
            		'idConciliacao'=>$venda['idConciliacao'],
            		'parcelas'=>$venda['parcelas'],
            		'produtos'=>$venda['produtos'],
            		'promocode'=>$venda['promocode'],
					'detalhes'=>$arrDetalhes
				];
        }
	}	

	/**
	 * Linha do consolidado - mostrar no último registro ou após o término de registros de um dia
	 * @param array $transacoes a array de transações para comparação de datas
	 * @param integer $i o índice atual do ponteiro dentro de um for
	*/
	public static function linhaConsolidado($transacoes,$i){
		$total=count($transacoes);
		if($i<$total-1){
			$d1=\DateTime::createFromFormat('d/m/Y',$transacoes[$i]['dataTransacao']->format('d/m/Y'));
			$d2=\DateTime::createFromFormat('d/m/Y',$transacoes[$i+1]['dataTransacao']->format('d/m/Y'));
			$interval=$d1->diff($d2);
			return $interval->d >= 1;
		} elseif($i==$total-1){
			return true;
		}
	}	

	/**
	 * Dados de uma venda para relatório CSV
	*/
	public static function dadosVenda($r){
		$data=$r['dataTransacao']->format('d/m/Y H:i:s');
		$nome=$r['comprador']['nome'];
		$doc=$r['comprador']['documento'];
		$endereco=$r['comprador']['endereco']
			.', '
			.$r['comprador']['numero']
			.($r['comprador']['complemento'] != '' 
				?' compl '.$r['comprador']['complemento']
				:'');
		$cidade=$r['comprador']['cidade'];
		$estado=$r['comprador']['estado'];
		$bairro=$r['comprador']['bairro'];
		$cep=$r['comprador']['cep'];
		$telefone=$r['comprador']['telefone'];
		$email=$r['comprador']['email'];
		$bandeira=$r['comprador']['bandeira_cartao'];
		$valor=$r['bruto'];
		$formaPgto=$r['formaPgto']=='' ?'Cartão de crédito' :$r['formaPgto'];
		$idConciliacao=$r['idConciliacao'];
		$condicoesPgto=$r['parcelas']==1 ?'à vista' :$r['parcelas'].' parcelas';
		$taxa=$formaPgto=='Cartão de débito' 
			?self::TAXA_DEB 
			:($r['parcelas']==1 ?self::TAXA_CRED_VISTA :self::TAXA_CRED_PARCELADO);
			$taxa.=' %';
		$primeiraCompra=self::qtdeComprasCliente($doc,true,$data) > 0 ?'Não' :'Sim';
		$carrinho=self::produtosCarrinho($r['carrinho']);
		
		//formatar cep
		$cepMask = "%s%s%s%s%s-%s%s%s";
		if($cep!='')
			$cep=self::_format($cepMask,$cep);
		//formatar CPF
		$cpfMask = "%s%s%s.%s%s%s.%s%s%s-%s%s";
		if($doc!='')
			if(strlen($doc)==11) $doc=self::_format($cpfMask,$doc);

		$return;
		if($carrinho==false || !isset($carrinho->produtos)){
			$return[]=[$data,utf8_decode($nome),utf8_decode($doc),utf8_decode($endereco),utf8_decode($cidade),utf8_decode($estado),utf8_decode($bairro),$cep,$telefone,utf8_decode($email),'Nenhum produto encontrado','R$ '.number_format($valor,2,',','.'),utf8_decode($formaPgto),utf8_decode($bandeira),utf8_decode($idConciliacao),utf8_decode($condicoesPgto),$taxa,utf8_decode($primeiraCompra)];
		}
		else{
			foreach ($carrinho->produtos as $produto) {
				$valorProduto='R$ '.number_format($produto->valor,2,',','.');
				if ($produto->desconto->fixo > 0 || $produto->desconto->percentual > 0) {
                    $valorProduto='R$ ' . $produto->desconto->percentual > 0 
                    	?number_format(floatval($produto->valor) - (floatval($produto->valor) * floatval($produto->desconto->percentual)), 2, ',' . '.') 
                        :number_format($produto->valor - $produto->desconto->fixo, 2, ',', '.');
                }
				$return[]=[$data,utf8_decode($nome),utf8_decode($doc),utf8_decode($endereco),utf8_decode($cidade),utf8_decode($estado),utf8_decode($bairro),$cep,$telefone,utf8_decode($email),utf8_decode($produto->nome),$valorProduto,utf8_decode($formaPgto),utf8_decode($bandeira),utf8_decode($idConciliacao),utf8_decode($condicoesPgto),$taxa,utf8_decode($primeiraCompra)];
			}
		}
		return $return;
	}	

	/**
	 * função para formatar uma string com um padrão qualquer
	 * ex.: $cepMask = "%s%s%s%s%s-%s%s%s"; echo format($cepMask,'09899000'); '09899-000'
	*/
	private static function _format($mask,$string)
	{
	    return  vsprintf($mask, str_split($string));
	}	

	/**
	 * Encontrar a quantidade de compras do cliente
	 * @param string $doc o documento para filtrar as vendas
	 * @param boolean $autorizado trazer somente autorizadas ou todas; padrão true que traz só as vendas autorizadas
	 * @param string $dataInicial data de referência a ser comparada com as retornadas na consulta
	*/
	public static function qtdeComprasCliente($doc,$autorizado=true,$dataInicial){
		$session = Yii::$app->session;
        $session->open();

		$vendas=(new WsCarrinho($session['user_token']))->consultarVendasFiltro(['comprador'=>$doc]);
		$qtd=0;
		if($vendas->successo==1) {
			$d1=\DateTime::createFromFormat('d/m/Y H:i:s',$dataInicial);
			foreach ($vendas->objeto as $index=>$obj) {
				$d2=\DateTime::createFromFormat('d/m/Y H:i:s',$obj->vendas->data);
				if($d2->getTimestamp()<$d1->getTimestamp() && (!$autorizado || ($autorizado && $obj->vendas->status=='Autorizado')))
					$qtd++;
			}
		}
		return $qtd;
	}	

	/**
	 * Retornar os nomes dos produtos de uma compra
	 * @param string $idCarrinho o id do carrinho no registro da venda
	*/
	public static function produtosCarrinho($idCarrinho){
		$session = Yii::$app->session;
        $session->open();
		$consulta=(new WsCarrinho($session['user_token']))->consultarCarrinho($idCarrinho);
		if($consulta->successo==1 && isset($consulta->objeto))
			return $consulta->objeto;
		else return false;
	}	
}
?>
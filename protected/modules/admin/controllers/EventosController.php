<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\EventosModel;
use app\modules\admin\controllers\AbstractController;

/**
 * Controller de Eventos
 */
class EventosController extends AbstractController
{
    public function actionIndex(){
    }
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionNovoEvento()
    {
    	try{
        	$eventos=new EventosModel();
        	$carga=$eventos->cargaIngressos(3,808,'A');
        }catch(\Exception $e){
        	return $this->render('//site/error',[
        		'name'=>'Erro buscando novos ingressos',
        		'message'=>$e->getMessage()
        	]);
        }
    }
}

<?php
namespace app\models;
use app\models\ApiCarrinho;

/**
 * This is the model class for table "pessoa".
 *
 * @property integer $id
 * @property string $id_compra
 * @property string $produto
 * @property string $nome
 * @property string $documento
 * @property string $email
 * @property string $telefone
 */
class Mineiraocard extends \yii\db\ActiveRecord
{
    public $nome;
    public $documento;
    public $email;
    public $produto;
    public $telefone;

    /**
     * @inheritdoc
     */
    /*public static function tableName()
    {
        return 'mineiraocard';
    }*/
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['nome', 'produto', 'documento'], 'required'],
            [['email','telefone'], 'safe'],
            [['nome'], 'string', 'max' => 24],
            [['documento'], 'string', 'min' => 5, 'max' => 100],
            [['id_compra'], 'integer'],
            [['email'], 'email'],
        ];
    }

    /**
     * @Override
     * Usar o método save() com endpoints de tags no carrinho
     * @todo corrigir retorno da api que traz successo=0 mesmo inserindo corretamente o valor
    */
    public function save( $runValidation = true, $attributeNames = null ){
        if(!$this->validate())
            return false;
        $api=new ApiCarrinho;
        $nome=$api->atualizarCarrinho(['tag'=>['nome'=>$this->produto.'_nome','valor'=>$this->nome]]);
        $documento=$api->atualizarCarrinho(['tag'=>['nome'=>$this->produto.'_documento','valor'=>$this->documento]]);
        $email=$api->atualizarCarrinho(['tag'=>['nome'=>$this->produto.'_email','valor'=>$this->email]]);
        $telefone=$api->atualizarCarrinho(['tag'=>['nome'=>$this->produto.'_telefone','valor'=>$this->telefone]]);
        return $this->nome!=''&&$this->documento!='';
    }

    /**
     * Verificar existência do valor de uma tag para um produto
     * @param mixed $tag o objeto da tag
     * @param integer $codigo o código do produto que faz parte do nome da tag
     * @param string $campo o campo da tag que, junto com o código do produto, forma o nome da tag
     * @return void|string nada se a tag não corresponder ao campo, string com o valor da tag ou uma string vazia se não tiver valor ou se a tag não existir
     */
    public static function checkTagCarrinho($tag,$codigo,$campo){        
        if(strpos($tag->nome,$campo)===FALSE || strpos($tag->nome,$codigo)===FALSE) return;
        // if($tag->nome == $codigo.$campo && $tag->valor != '') return $tag->valor;
        if(strpos($tag->nome,$codigo)!==FALSE && strpos($tag->nome,$campo)!==FALSE && $tag->valor != '') return $tag->valor;
        else return '';
    }

    /**
     * Verificar se as tags do carrinho contém dados de nome e documento para cada assento
     * @param mixed $carrinho o objeto do carrinho atual que contém produtos e tags
     * @return boolean se as informações mais básicas foram inseridas - nome e documento de cada produto
     */
    public function infoOcupantes($carrinho){
        if(!isset($carrinho->tags)) return false;
        $nomesOk=[];
        $docsOk=[];
        $emailsOk=[];
        $telsOk=[];
        if(!isset($carrinho->produtos)) return false;
        foreach($carrinho->produtos as $prod){
            $nome=false;
            $doc=false;
            $email=false;
            $tel=false;
            foreach($carrinho->tags as $tag){
                if(strpos($tag->nome,$prod->codigo)!==FALSE && strpos($tag->nome,'_nome')!==FALSE && $tag->valor!='')
                    $nome=true;
                if(strpos($tag->nome,$prod->codigo)!==FALSE && strpos($tag->nome,'_documento')!==FALSE && $tag->valor!='')
                    $doc=true;
                if(strpos($tag->nome,$prod->codigo)!==FALSE && strpos($tag->nome,'_email')!==FALSE && $tag->valor!='')
                    $email=true;
                if(strpos($tag->nome,$prod->codigo)!==FALSE && strpos($tag->nome,'_telefone')!==FALSE && $tag->valor!='')
                    $tel=true;
            }
            $nomesOk[]=$nome;
            $docsOk[]=$doc;
            $emailsOk[]=$email;
            $telsOk[]=$tel;
        }
        if(count($nomesOk)!=count($carrinho->produtos) || in_array(0,$nomesOk)) return false;
        if(count($docsOk)!=count($carrinho->produtos) || in_array(0,$docsOk)) return false;
        //if(count($emailsOk)!=count($carrinho->produtos) || in_array(0,$emailsOk)) return false;
        //if(count($telsOk)!=count($carrinho->produtos) || in_array(0,$telsOk)) return false;
        return true;
    }

    /**
     * Pegar informações do ocupante / dono do ingresso
     * @param mixed $carrinho retorno de consulta à API
     * @return mixed nome, documento, telefone e e-mail do ocupante
     */
    public static function ocupanteIngresso($carrinho,$produto){
        $mineiraocardNome='';
        $mineiraocardDoc='';
        $mineiraocardTel='';
        $mineiraocardEmail='';
        
        if(isset($carrinho->objeto->tags)){
            foreach($carrinho->objeto->tags as $tag){
                $nome=self::checkTagCarrinho($tag,$produto->codigo,'_nome');
                if($nome!=false)
                    $mineiraocardNome=$tag;
                $doc=self::checkTagCarrinho($tag,$produto->codigo,'_documento');
                if($doc!=false)
                    $mineiraocardDoc=$tag;
                $tel=self::checkTagCarrinho($tag,$produto->codigo,'_telefone');
                if($tel!=false)
                    $mineiraocardTel=$tag;
                $email=self::checkTagCarrinho($tag,$produto->codigo,'_email');
                if($email!=false)
                    $mineiraocardEmail=$tag;
            }
        }

        return (object)['nome'=>$mineiraocardNome,'doc'=>$mineiraocardDoc,'tel'=>$mineiraocardTel,'email'=>$mineiraocardEmail];
    }

    /**
     * Tags de ocupante direto do objeto "produto" trazido do carrinho
     * @param mixed $produto retorno de consulta à API
     * @return mixed nome, documento, telefone e e-mail do ocupante
     */
    public static function ocupanteIngressoObjetoProduto($produto){
        $mineiraocardNome='';
        $mineiraocardDoc='';
        $mineiraocardTel='';
        $mineiraocardEmail='';
        if(is_array($produto->tags)){
            foreach($produto->tags as $tag){
                $nome=self::checkTagCarrinho($tag,$produto->codigo,'_nome');
                if($nome!=false)
                    $mineiraocardNome=$tag->valor;
                $doc=self::checkTagCarrinho($tag,$produto->codigo,'_documento');
                if($doc!=false)
                    $mineiraocardDoc=$tag->valor;
                $tel=self::checkTagCarrinho($tag,$produto->codigo,'_telefone');
                if($tel!=false)
                    $mineiraocardTel=$tag->valor;
                $email=self::checkTagCarrinho($tag,$produto->codigo,'_email');
                if($email!=false)
                    $mineiraocardEmail=$tag->valor;
            }
        }

        return (object)['nome'=>$mineiraocardNome,'doc'=>$mineiraocardDoc,'tel'=>$mineiraocardTel,'email'=>$mineiraocardEmail];
        
    }
}

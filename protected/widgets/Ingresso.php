<?php
namespace app\widgets;
use \yii\bootstrap\Widget;

class Ingresso extends Widget
{
    /**
     * À partir dos objetos produto e cupons disponíveis, montar um select para mostrar aos clientes
     * @param object $produto Objeto com os dados do produto
     * @param objeto $cupons Objeto com os dados de cupons disponíveis
     * @param integer $index Índice do item dentro do formulário de ocupantes
     * @param Taxa $taxa Calcular a taxa (ou false para ignorar)
     * @param boolean $label Se vai mostrar o label com o select
     */
    public static function getSelectTipo($produto, $cupons, $index, $taxa, $label=true)
    {
        $tipoIngresso = '';
        if (!empty($produto) && !empty($cupons)) {
            $tipoIngresso .= $label == true ? '<label for="ingresso-' . $produto->codigo . '-'.$index . '" class="control-label" style="margin-left:5px;">Escolha o tipo</label>' : '';
            $taxaConveniencia = $taxa ? 'data-conv="' . $taxa . '"' :'';
            $tipoIngresso .= '
                <select data-indice="'.$index.'" name="Ocupante[cupom_codigo][' . $index . ']" id="ingresso-' . $produto->codigo . '-'.$index.'" class="form-control cupom-codigo">
                    <option value="inteira" data-valor="' . $produto->valor . '" '.$taxaConveniencia.'>Inteira - R$ ' . number_format($produto->valor, 2, ',', '.') . '</option>';
            foreach ($cupons as $index => $cupom) {
                if ($cupom->estoque->disponiveis > 0) {
                    $valor = self::getPrecoIngresso($produto, $cupom);
                    $selected='';
                    if(isset($produto->cupom)&&$produto->cupom->id==$cupom->id)
                        $selected=' selected="selected"';
                    $tipoIngresso .= '
                    <option '.$selected.' value="' . $cupom->id . '" data-descricao="' . $cupom->descricao . '" data-valor="' . number_format($valor, 2, '.' , '') . '" '.$taxaConveniencia.'>' . $cupom->nome . ' - R$ ' . number_format($valor, 2, ',' , '.') . '</option>';
                }
            }
            $tipoIngresso .= '
                </select>';
        }
        return $tipoIngresso;
    }
    
    /**
     * À partir dos objetos produto e cupom, calcular o valor do ingresso, aplicando os descontos configurados
     * @param object $produto Objeto com os dados do produto
     * @param object $cupom Objeto com os dados do cupom (se não existir, retornar o próprio valor do produto - Inteira)
     * @return type
     */
    public static function getPrecoIngresso($produto, $cupom)
    {
        if (property_exists($cupom, 'desconto_fixo')) {
            return !empty($cupom) ? $produto->valor - ($cupom->desconto_fixo != 0 ? $cupom->desconto_fixo : $produto->valor * $cupom->desconto_percentual / 100) : $produto->valor;
        }
        return !empty($cupom) ? $produto->valor - ($cupom->desconto->fixo != 0 ? $cupom->desconto->fixo : $produto->valor * $cupom->desconto->percentual / 100) : $produto->valor;
    }
}


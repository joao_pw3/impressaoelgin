<!DOCTYPE html>
<html lang="pt-BR"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/normalize.min.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<body>

<div class="cab fundoBlue">
    <div class="row">
        <div class="small-12 columns" style="color:#fff;">
            <img src="../images/offline.png" alt="" title="" border="0" width="25"> Offline
        </div>
    </div>
</div>

<div class="container contTop">
	<div class="col-md-12">
		<div class="boxLogar2">
			<div class="col-md-8">
				<img src="../images/mineirao-50-anos.png" alt="Estádio Mineirão" title="Estádio Mineirão" border="0"><br>
				<h2><?= $texto; ?></h2>
			</div>
			<div class="col-md-4 fundoEstadio2">
				
			</div>
			<br clear="all">
		</div>
	</div>
</div>

<footer style="padding:20px 0px; position:absolute; bottom:0px; width:100%;">
	<div style="text-align:center; color:#fff; margin:0px; padding:0px;">EasyforPay System - © Copyright 2018</div>
</footer>
</body>
</html>
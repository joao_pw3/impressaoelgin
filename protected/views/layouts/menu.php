<!-- header_mobile -->
<section class="header_mobile">
    <div class="row align-middle">
        <div class="columns shrink">
            <div class="icon icon-list roxo bt_menu"></div>
        </div>
    </div>
</section>
<!-- /header_mobile -->

<!-- menu normal -->
<div class="row menu show-for-large">
    <div id="main-menu" class="small-12 columns text-center">
        <div class="menu-menu-cabecalho-container"><ul id="menu" class="menu"><li id="menu-item-76" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-76 level-0"><a href="http://estadiomineirao.com.br/o-mineirao/" class="level-0">O Mineirão</a>
<ul class="sub-menu level-0">
	<li id="menu-item-475" class="menu-item menu-item-type-post_type_archive menu-item-object-history menu-item-475 level-1"><a href="http://estadiomineirao.com.br/o-mineirao/historia/" class="level-1">História</a></li>
	<li id="menu-item-175" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-175 level-1"><a href="http://estadiomineirao.com.br/o-mineirao/estrutura/" class="level-1">Estrutura</a></li>
	<li id="menu-item-7010" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7010 level-1"><a href="http://estadiomineirao.com.br/o-mineirao/jogos-e-eventos/" class="level-1">Fichas Técnicas</a></li>
	<li id="menu-item-265" class="menu-item menu-item-type-post_type_archive menu-item-object-news menu-item-has-children menu-item-265 level-1"><a href="http://estadiomineirao.com.br/o-mineirao/imprensa/noticias/" class="level-1">Imprensa</a>
	<ul class="sub-menu level-1">
		<li id="menu-item-266" class="menu-item menu-item-type-post_type_archive menu-item-object-news menu-item-has-children menu-item-266 level-2"><a href="http://estadiomineirao.com.br/o-mineirao/imprensa/noticias/" class="level-2">Notícias</a>
		<ul class="sub-menu level-2">
			<li id="menu-item-270" class="menu-item menu-item-type-taxonomy menu-item-object-news_category menu-item-270 level-3"><a href="http://estadiomineirao.com.br/o-mineirao/imprensa/noticias/categorias/jogos/" class="level-3">Jogos</a></li>
			<li id="menu-item-271" class="menu-item menu-item-type-taxonomy menu-item-object-news_category menu-item-271 level-3"><a href="http://estadiomineirao.com.br/o-mineirao/imprensa/noticias/categorias/noticia/" class="level-3">Notícia</a></li>
		</ul>
</li>
		<li id="menu-item-324" class="menu-item menu-item-type-post_type_archive menu-item-object-photo_gallery menu-item-324 level-2"><a href="http://estadiomineirao.com.br/o-mineirao/imprensa/galeria-de-fotos/" class="level-2">Galeria de fotos</a></li>
		<li id="menu-item-333" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-333 level-2"><a href="http://estadiomineirao.com.br/o-mineirao/imprensa/marcas/" class="level-2">Marcas</a></li>
		<li id="menu-item-14067" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14067 level-2"><a href="http://estadiomineirao.com.br/contato-imprensa/" class="level-2">Contato Imprensa</a></li>
	</ul>
</li>
	<li id="menu-item-7009" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7009 level-1"><a href="http://estadiomineirao.com.br/o-mineirao/sustentabilidade/" class="level-1">Sustentabilidade</a></li>
	<li id="menu-item-283" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283 level-1"><a href="http://estadiomineirao.com.br/o-mineirao/mapa/" class="level-1">Mapa do Estádio</a></li>
	<li id="menu-item-1582" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1582 level-1"><a href="http://estadiomineirao.com.br/contato/" class="level-1">Contato</a></li>
	<li id="menu-item-13137" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13137 level-1"><a target="_blank" href="http://estadiomineirao.com.br/saibamais/" class="level-1">Saiba Mais</a></li>
</ul>
</li>
<li id="menu-item-77" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-77 level-0"><a href="http://estadiomineirao.com.br/esplanada/" class="level-0">Esplanada</a></li>
<li id="menu-item-78" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-78 level-0"><a href="http://estadiomineirao.com.br/museu-e-visita/" class="level-0">Museu e Visitas</a>
<ul class="sub-menu level-0">
	<li id="menu-item-409" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-409 level-1"><a href="http://estadiomineirao.com.br/museu-e-visita/salas/" class="level-1">Salas</a></li>
	<li id="menu-item-123" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-123 level-1"><a href="http://estadiomineirao.com.br/museu-e-visita/educativo/" class="level-1">Educativo</a></li>
	<li id="menu-item-410" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-410 level-1"><a href="http://estadiomineirao.com.br/museu-e-visita/ingressos/" class="level-1">Ingressos</a></li>
	<li id="menu-item-411" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-411 level-1"><a href="http://estadiomineirao.com.br/museu-e-visita/visita-guiada/" class="level-1">Visita guiada</a></li>
</ul>
</li>
<li id="menu-item-79" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79 level-0"><a href="http://estadiomineirao.com.br/jogos-e-shows/" class="level-0">INGRESSOS</a></li>
<li id="menu-item-14740" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14740 level-0"><a href="http://estadiomineirao.com.br/eventos/" class="level-0">Eventos</a></li>
<li id="menu-item-11957" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-11957 level-0"><a href="http://estadiomineirao.com.br/temporada-mineirao/" class="level-0">INGRESSOS MINEIRÃO</a>
<ul class="sub-menu level-0">
	<li id="menu-item-15007" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15007 level-1"><a href="http://vendas.estadiomineirao.com.br/" class="level-1">CAMAROTE N1</a></li>
	<li id="menu-item-13906" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13906 level-1"><a href="http://vendas.estadiomineirao.com.br/" class="level-1">CAMAROTES MINEIRÃO</a></li>
	<li id="menu-item-11949" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11949 level-1"><a href="http://vendas.estadiomineirao.com.br/" class="level-1">MINEIRÃO TRIBUNA</a></li>
	<li id="menu-item-13905" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13905 level-1"><a href="http://vendas.estadiomineirao.com.br/" class="level-1">EMBAIXADA MINEIRÃO</a></li>
</ul>
</li>
<li id="menu-item-81" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-81 level-0"><a href="http://estadiomineirao.com.br/faca-seu-evento/" class="level-0">Faça seu Evento</a></li>
<li id="menu-item-14980" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14980 level-0"><a href="http://estadiomineirao.com.br/feirao-de-domingo/" class="level-0">Feirão de Domingo</a></li>
<li id="menu-item-991" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-991 level-0"><a href="http://estadiomineirao.com.br/como-chegar/mobilidade/" class="level-0">Como Chegar</a>
<ul class="sub-menu level-0">
	<li id="menu-item-372" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-372 level-1"><a href="http://estadiomineirao.com.br/como-chegar/mobilidade/" class="level-1">Mobilidade</a></li>
	<li id="menu-item-373" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-373 level-1"><a href="http://estadiomineirao.com.br/como-chegar/vagas-disponiveis/" class="level-1">Vagas disponíveis</a></li>
	<li id="menu-item-415" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-415 level-1"><a target="_blank" href="http://www.estapar.com.br/reservavagas/eventos" class="level-1">Garanta sua vaga</a></li>
</ul>
</li>
</ul></div>    </div>
</div>
<!-- fim do menu normal -->

<!-- menu mobile -->
<div class="row menu_mobile">
    <div class="menu-menu-cabecalho-container"><ul id="menu-menu-cabecalho">
                                <li class="text-right">
                                    <img class="close" border="0" src="<?=Yii::getAlias("@web")?>/images/close-menu.png" alt="Close" title="Close" />
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-76"><a href="http://estadiomineirao.com.br/o-mineirao/">O Mineirão</a><a href="#" class="icon icon-plus-square mais right"></a><a href="#" class="icon icon-minus-square menos right"></a>
<ul class='small-12 columns'>
	<li class="menu-item menu-item-type-post_type_archive menu-item-object-history menu-item-475"><a href="http://estadiomineirao.com.br/o-mineirao/historia/">História</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-175"><a href="http://estadiomineirao.com.br/o-mineirao/estrutura/">Estrutura</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7010"><a href="http://estadiomineirao.com.br/o-mineirao/jogos-e-eventos/">Fichas Técnicas</a></li>
	<li class="menu-item menu-item-type-post_type_archive menu-item-object-news menu-item-has-children menu-item-265"><a href="http://estadiomineirao.com.br/o-mineirao/imprensa/noticias/">Imprensa</a><a href="#" class="icon icon-plus-square mais right"></a><a href="#" class="icon icon-minus-square menos right"></a>
	<ul class='small-12 columns submenu level-1'>
		<li class="menu-item menu-item-type-post_type_archive menu-item-object-news menu-item-has-children menu-item-266"><a href="http://estadiomineirao.com.br/o-mineirao/imprensa/noticias/">Notícias</a><a href="#" class="icon icon-plus-square mais right"></a><a href="#" class="icon icon-minus-square menos right"></a>
		<ul class='small-12 columns submenu level-2'>
			<li class="menu-item menu-item-type-taxonomy menu-item-object-news_category menu-item-270"><a href="http://estadiomineirao.com.br/o-mineirao/imprensa/noticias/categorias/jogos/">Jogos</a></li>
			<li class="menu-item menu-item-type-taxonomy menu-item-object-news_category menu-item-271"><a href="http://estadiomineirao.com.br/o-mineirao/imprensa/noticias/categorias/noticia/">Notícia</a></li>
		</ul>
</li>
		<li class="menu-item menu-item-type-post_type_archive menu-item-object-photo_gallery menu-item-324"><a href="http://estadiomineirao.com.br/o-mineirao/imprensa/galeria-de-fotos/">Galeria de fotos</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-333"><a href="http://estadiomineirao.com.br/o-mineirao/imprensa/marcas/">Marcas</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14067"><a href="http://estadiomineirao.com.br/contato-imprensa/">Contato Imprensa</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7009"><a href="http://estadiomineirao.com.br/o-mineirao/sustentabilidade/">Sustentabilidade</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283"><a href="http://estadiomineirao.com.br/o-mineirao/mapa/">Mapa do Estádio</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1582"><a href="http://estadiomineirao.com.br/contato/">Contato</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13137"><a target="_blank" href="http://estadiomineirao.com.br/saibamais/">Saiba Mais</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-77"><a href="http://estadiomineirao.com.br/esplanada/">Esplanada</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-78"><a href="http://estadiomineirao.com.br/museu-e-visita/">Museu e Visitas</a><a href="#" class="icon icon-plus-square mais right"></a><a href="#" class="icon icon-minus-square menos right"></a>
<ul class='small-12 columns'>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-409"><a href="http://estadiomineirao.com.br/museu-e-visita/salas/">Salas</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-123"><a href="http://estadiomineirao.com.br/museu-e-visita/educativo/">Educativo</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-410"><a href="http://estadiomineirao.com.br/museu-e-visita/ingressos/">Ingressos</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-411"><a href="http://estadiomineirao.com.br/museu-e-visita/visita-guiada/">Visita guiada</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79"><a href="http://estadiomineirao.com.br/jogos-e-shows/">INGRESSOS</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14740"><a href="http://estadiomineirao.com.br/eventos/">Eventos</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-11957"><a href="http://estadiomineirao.com.br/temporada-mineirao/">INGRESSOS MINEIRÃO</a><a href="#" class="icon icon-plus-square mais right"></a><a href="#" class="icon icon-minus-square menos right"></a>
<ul class='small-12 columns'>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15007"><a href="http://vendas.estadiomineirao.com.br/">CAMAROTE N1</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13906"><a href="http://vendas.estadiomineirao.com.br/">CAMAROTES MINEIRÃO</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11949"><a href="http://vendas.estadiomineirao.com.br/">MINEIRÃO TRIBUNA</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13905"><a href="http://vendas.estadiomineirao.com.br/">EMBAIXADA MINEIRÃO</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-81"><a href="http://estadiomineirao.com.br/faca-seu-evento/">Faça seu Evento</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14980"><a href="http://estadiomineirao.com.br/feirao-de-domingo/">Feirão de Domingo</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-991"><a href="http://estadiomineirao.com.br/como-chegar/mobilidade/">Como Chegar</a><a href="#" class="icon icon-plus-square mais right"></a><a href="#" class="icon icon-minus-square menos right"></a>
<ul class='small-12 columns'>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-372"><a href="http://estadiomineirao.com.br/como-chegar/mobilidade/">Mobilidade</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-373"><a href="http://estadiomineirao.com.br/como-chegar/vagas-disponiveis/">Vagas disponíveis</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-415"><a target="_blank" href="http://www.estapar.com.br/reservavagas/eventos">Garanta sua vaga</a></li>
</ul>
</li>
</ul></div></div>
<!-- fim do menu mobile -->

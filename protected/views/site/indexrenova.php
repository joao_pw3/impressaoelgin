<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="eventos" id="eventos">
    
    <input type="hidden" id="url-areas" value="<?= Url::to(['site/areas']); ?>">
    
    <div class="container">
		<br>
        <br>
		<div class="row">
			<div class="col-sm-5 columns wow fadeInUp margFinal" data-wow-delay=".3s">
				<?= Html::img('@web/images/renove.jpg', ["class" => "fotoN"]); ?>
			</div>
			<div class="col-sm-7">
				<h1>RENOVAÇÃO DE CAMAROTE E TRIBUNA</h1>
				<p>
				O Mineirão está renovando os lugares nos camarotes e nas tribunas. Você que possuía um espaço na temporada 2017, pode agora garantir para a temporada 2018. Foi enviado um e-mail com as instruções de como você deve proceder para efetuar e sua renovação. Caso não tenha recebido, entre em contato conosco, clicando no link abaixo.
				</p>
				<h2>Como renovar</h2>
				<p>
				Confira seu e-mail com um link que enviamos para efetuar sua renovação. Você precisará de um cartão de crédito disponível para compras online. Não se esqueça de verificar seu limite.
				</p>
				<h2>Vantagens</h2>
				<p>
				<ul>
					<li>Você sempre terá seu espaço garantido nos jogos da temporada.</li>
					<li>Entrada exclusiva com mais segurança e conforto.</li>
					<li>Visão privilegiada do campo.</li>
				</ul>
				</p>
				<hr>
				<span class="row contato">
                    <input type="button" value="Contate-nos se precisar de mais informações" class="areas" id="index-renova">
                </span>
			</div>
		</div>
        <br>
        <br>
    </div>	
</div>

<?php $this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]); ?>
<?php
namespace app\modules\admin\controllers;

use Yii;
use yii\web\Response;
use app\modules\admin\models\Transacoes;
use app\models\EventosModel;
use app\models\CompraModel;
use app\models\ApiCarrinho;
use app\models\ApiProduto;
use app\models\ApiCliente;
use app\modules\admin\controllers\AbstractController;
use app\models\ApiBoleto;
use app\models\BoletoModel;
use app\modules\admin\models\FinanceiroModel;


/**
 * Controller de vendas.
 */
class FinanceiroController extends AbstractController
{
    /**
     * Relatório de vendas. Trazer as vendas com taxas e cálculo de datas
     * Pesquisa o endpoint vendas e grava em sessão; ao consultar novamente, aproveita o que está gravado em sessão. 
     * Faz o mesmo com a lista de produtos
    */
    public function actionIndex(){
        $model=new FinanceiroModel;
        $modelProdutos=new ApiProduto;
        $model->vendas=$this->getItemSession('vendas_painel_financeiro');
        if(!$model->vendas){
            $model->vendasPeriodos();
            $this->setItemSession('vendas_painel_financeiro',$model->vendas);
        }
        
        $modelProdutos->produtos=$this->getItemSession('produtos_painel_financeiro');
        if(!$modelProdutos->produtos){
            $modelProdutos->listaNomesProdutos(['bloco','camarote']);
            $this->setItemSession('produtos_painel_financeiro',$modelProdutos->produtos);
        }
        $model->produtos=$modelProdutos->produtos;
        
        $sumario=$this->getItemSession('sumario_painel_financeiro');
        if(!$sumario){
            $sumario=$model->sumarioExecutivo();
            $this->setItemSession('sumario_painel_financeiro',$sumario);
        }        
        
        return $this->render('index',[
            'vendas'=>array_reverse($model->vendas),
            'sumario'=>$sumario,
            'periodos'=>array_keys($sumario['tipos']),
            'model'=>$model
        ]);
    }

    /**
     * API - Geração do boleto com form (dados do cliente e valor do boleto)
    */
    public function actionBoleto()
    {
        $api = new ApiBoleto();
        $boleto = new BoletoModel();

        if (Yii::$app->request->post()) 
        {
            $post = Yii::$app->request->post('BoletoModel');
            $file = file_get_contents(Yii::getAlias('@app').'/data/controles.js');
            $jsonFile = json_decode($file);
            $vencimento = explode('/', $post['vencimento']);
            $date = (new \DateTime('now'))->format('ymd');
            $contadorJson = substr($jsonFile->boleto->contador, 0, -6);
            $numeroBoleto = $contadorJson.$date;

            $endereco = ','. $post['numero'].' - '.$post['complemento'];
            $data['seunumero'] = $numeroBoleto;

            $data['valor'] = preg_replace('/(\D)/', '', $post['valor']*100);

            $data['mensagem'] = nl2br($post['mensagem']);

            $data['vencimento'] = [
                'ano' => $vencimento[2],
                'mes' => $vencimento[1],
                'dia' => $vencimento[0]
            ];

            $data['sacado'] = [
                'nome' => $post['nome'],
                'documento' => preg_replace('/(\D)/', '', $post['documento']),
                'endereco'  => $post['endereco'].$endereco,
                'bairro'    => $post['bairro'],
                'cep'       => preg_replace('/(\D)/', '', $post['cep']),
                'municipio' => $post['municipio'],
                'uf'        => $post['uf'],
                'email'     => $post['email']
            ];

            $retorno = (object) json_decode($api->call('boleto', 'post', $data, true, 'BP'));     

            if($retorno->successo == 0)
            {
                $mensagem = $retorno->codigo.' - '.$retorno->mensagem;

                return $this->render('boleto', ['mensagem' => $mensagem]);
            } else {

                $jsonFile->boleto->contador = ltrim(($contadorJson+1).$date);

                $jsonFileNovo = json_encode($jsonFile);
                file_put_contents(Yii::getAlias('@app').'/data/controles.js', $jsonFileNovo);  

                $mensagem = '<a style="color:#00000;0" target="_blank" href="'.$retorno->objeto->boletos[0]->url.'">Visualizar Boleto</a>';

                return $this->render('boleto', ['mensagem' => $mensagem]);
            }              
        }

        return $this->render('boleto', ['boleto' => $boleto]);
    }

    /**
     * Gravar as vendas retornadas pelo endpoint da sessão
     * Por enquanto, deixar só para o ambiente de desenvolvimento
     */
    public function setItemSession($item,$vendas){
        if(!YII_ENV_DEV) return;
        $session = $this->_sessionOpen();
        $session->set($item,$vendas);
    }

    /**
     * Trazer as vendas da sessão
     * Por enquanto, deixar só para o ambiente de desenvolvimento
     */
    public function getItemSession($item){
        if(!YII_ENV_DEV) return false;
        $session = $this->_sessionOpen();
        //$session->remove($item);
        return $session->get($item);
    }
}

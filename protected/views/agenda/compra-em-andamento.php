<?php
use \app\assets\LightboxAsset;
use \app\assets\SlickAsset;
use \app\models\Agenda;
use app\models\Tags;
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\CarrouselSimplesAsset; 
use app\components\TempoCarrinhoWidget;

CarrouselSimplesAsset::register($this);
LightboxAsset::register($this);
SlickAsset::register($this);

$session = Yii::$app->session;
$session->open(); 
?>
<div class="row">
    <div class="col-sm-12 semPad semMargin">
        <div class="alert alert-warning" style="font-size:16px; text-align:center;">
            Você já iniciou a compra de outro tipo de evento
        </div>
    </div>
    <p class="col-xs-6 text-right"><a class="btn btn-primary btn-lg" href="<?= Url::to(['/compra/concluir-compra-mineirao']) ?>">Concluir compra</a></p>
    <p class="col-xs-6 text-left"><a class="btn btn-primary btn-lg" href="<?= Url::to(['/compra/nova-compra']) ?>">Iniciar nova compra</a></p>
</div>
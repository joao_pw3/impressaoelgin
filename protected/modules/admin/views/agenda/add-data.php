<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use app\models\Tags;
$tags = (new Tags)->objetoTags($model->tags);
$this->title = 'ADM Easy for Pay';
?>
<div class="container">
	<div class="row">
		<h2 class="tituloGeral"><i class="fa fa-clone"></i> Agenda - Nova Data</h2>
	</div>
	<!-- <div class="boxArredondado">
		<h2 style="margin-top:0px;"><?=$model->nome?> <?=$model->stringDatas($model->datas)?></h2>
	</div> -->

	<div div class="col-xs-6" style="padding-left:0px;">
		<?php $form = ActiveForm::begin(['id' => 'form-data']); ?>
		<?= $form->field($model, 'data')->widget(DateTimePicker::className(), [
				'type' => DateTimePicker::TYPE_COMPONENT_APPEND,//mostrar botões no fim do input (há outras opções)
				'readonly' => true,// impede o usuário de escrever (evita erros de digitação)
				'pluginOptions' => [
						'todayBtn' => true,// destaca o dia atual no calendário
						'autoclose' => true,// fecha o calendário automaticamente no final das escolhas do cliente
						'format' => 'dd/mm/yyyy hh:ii', // formato da data (já coloquei no formato da API)
						'startDate' => date('d/m/Y H:i'), // bloquear datas anteriores ao dia atual
				]

		]); ?>
		<?= $form->field($model, 'codigo')->hiddenInput()->label(false) ?>
	</div>
	<div div class="col-xs-6" style="padding-left:0px;">
		<?php $model->tipoEvento = isset($tags->assento) && $tags->assento == 'livre' ? 0 : 1; ?>
		<?= $form->field($model, 'tipoEvento')->radioList(['1' => 'Lugares marcados', '0' => 'Lugares livres'])->label('Tipo de evento'); ?>
	</div>
	<br clear="all">

	<div class="form-group" style="text-align:center;">
		<?= Html::submitButton('Cadastrar', ['class' => 'btn btn-primary btn-pagar']) ?>
	</div>

	<?php ActiveForm::end(); ?>
</div>
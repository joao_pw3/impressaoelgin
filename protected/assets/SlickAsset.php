<?php
namespace app\assets;

use yii\web\AssetBundle;

class SlickAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/slick.css'
    ];
    public $js = [
        'js/slick.js'
    ];
    public $publishOptions = [
        'forceCopy' => true,
    ];
}

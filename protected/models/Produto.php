<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\WsProduto;

class Produto extends Model {

    public $lista;
    public $codigo;
    public $nome;
    public $descricao;
    public $valor;
    public $descontoFixo;
    public $descontoPercentual;
    public $oferecido;
    public $vendido;
    public $reservado;
    public $disponivel;
    public $reserva_exclusiva;
    public $ativo;
    public $tags;
    public $anexos;
    public $tipoMovimentoEstoque;
    public $unidadesEstoque;
    public $id_agenda;
    public $unidade;
    public $agenda;
    public $matriz;
    public $vendedor;

    /**
     * Método construtor sempre pede a unidade atual
    */
    public function __construct(Cliente $unidade){
        $this->unidade = $unidade;
    }

    public function rules(){
        return [
            [['nome', 'valor', 'ativo'], 'required'],
            [['tipoMovimentoEstoque', 'unidadesEstoque'], 'required', 'on' => 'movimento-estoque'],
            [['codigo', 'descricao', 'descontoPercentual', 'descontoFixo', 'id_agenda', 'matriz'], 'safe']
        ];
    }

    /**
     * definição dos labels de atributos
     */
    public function attributeLabels(){
        return[
            'codigo'=>'Código',
            'nome'=>'Nome',
            'descricao'=>'Descrição',
            'valor'=>'Valor',
            'descontoFixo'=>'Desconto fixo',
            'descontoPercentual'=>'Desconto %',
            'oferecido'=>'Oferecido',
            'vendido'=>'Vendido',
            'reservado'=>'Reservado',
            'disponivel'=>'Disponível',
            'reserva_exclusiva'=>'Reserva exclusiva',
            'ativo'=>'Ativo',
            'tags'=>'Tags',
            'anexos' => 'Anexos',
            'unidadesEstoque'=>'Unidades',
            'tipoMovimentoEstoque'=>'Tipo de movimento',
            'id_agenda'=>'Data do evento relacionado'
        ];
    }

    /**
     * Consulta todos os produtos
     * @param integer $registro Número do registro inicial para a busca (uso em paginação) - Sem $pagina, a busca retorna uma quantidade padrão de registros
     */
    public function listaTodosProdutos($registro) {
        $api = new WsProduto($this->unidade->user_token);
        $produtos = $api->buscarProduto(null, null, $registro);

        if (!$produtos->successo)
            return $this->addError('lista', $produtos->erro->mensagem);

        foreach ($produtos->objeto as $produto) {
            $obj = new Produto($this->unidade);
            $obj->loadFromApi($produto);
            $this->lista[] = $obj;
        }
    }

    /**
    * Cria um novo produto
    */
    public function criarProduto(){
        $ws=new WsProduto($this->unidade->user_token);
        $objeto=[
            'nome'=>$this->nome,
            'descricao'=>$this->descricao,
            'valor'=>$this->valor,
            'desconto'=>[
                'fixo'=>$this->descontoFixo,
                'percentual'=>$this->descontoPercentual
            ],
            'ativo'=>$this->ativo,
            'id_agenda'=>$this->id_agenda
        ];
        if ($this->validate())
            return $ws->criarProduto($objeto);
    }
    
    /**
     * Procedimento para criar fileiras
     */
    public function criarFileira($post)
    {
        if (isset($post) && !empty($post)) {
            $this->load($post);
            $fileiraInicio = str_replace('_', '', $post['Tags']['3']['valor']);
            $fileiraFim = str_replace('_', '', $post['Tags']['4']['valor']);
            $arrayTags = [];
            foreach($post['Tags'] as $index => $tag) {
                if ($tag['nome'] != 'fileira') {
                    array_push($arrayTags, $tag);
                }
            }
            if (!empty($fileiraFim)) { //várias fileiras
                $return = true;
                $msg = '';
                foreach (range($fileiraInicio, $fileiraFim) as $letra) {
                    $returnApi = $this->salvarFileira($arrayTags, strtoupper($letra));
                    if ($returnApi->successo == '0') {
                        $return = false;
                        $msg .= $returnApi->erro['mensagem'];
                    }
                }
                return $return ? (new WsProduto($this->unidade->user_token))->getApiSuccess('Fileiras cadastradas com sucesso.') : (new WsProduto($this->unidade->user_token))->getApiError($msg);
            } 
            //apenas uma fileira
            return $this->salvarFileira($arrayTags, $fileiraInicio);
        }
    }
    
    /**
     * Salvar fileira
     * @param array $tags Array de tags
     * @param string $letra Fileira atual
     * @return type
     */
    public function salvarFileira ($tags, $letra)
    {
        if (!empty($tags) && !empty($letra)) {
            $tagTmp = array_merge($tags, [['nome' => 'fileira', 'valor' => $letra]]);
            if (!$this->checkTag($tagTmp)) {
                $this->nome = 'Fileira ' . $letra;
                $this->descricao = 'Fileira ' . $letra;
                $returnApi = $this->criarProduto();
                if ($returnApi->successo == '1') {
                    return $this->salvarTag($tagTmp, $returnApi->objeto->codigo);
                }
            }
            return (new WsProduto($this->unidade->user_token))->getApiError('<br>Fileira já existe: ' . $letra);
        }
        return (new WsProduto($this->unidade->user_token))->getApiError('Faltam informações para salvar fileira.');
    }
    
    /**
     * Procedimento para criar assentos
     */
    public function criarAssento($post)
    {
        if (isset($post) && !empty($post)) {
            $this->load($post);
            $assentoInicio = str_replace('_', '', $post['Tags']['4']['valor']);
            $assentoFim = str_replace('_', '', $post['Tags']['5']['valor']);
            $arrayTags = [];
            foreach($post['Tags'] as $index => $tag) {
                if ($tag['nome'] != 'assento') {
                    array_push($arrayTags, $tag);
                }
            }
            if (!empty($assentoFim)) { //vários assentos
                $return = true;
                $msg = '';
                for ($assento = $assentoInicio; $assento <= $assentoFim; $assento++) {
                    $returnApi = $this->salvarAssento($arrayTags, $assento);
                    if ($returnApi->successo == '0') {
                        $return = false;
                        $msg .= $returnApi->erro['mensagem'];
                    }
                }
                return $return ? (new WsProduto($this->unidade->user_token))->getApiSuccess('Assentos cadastrados com sucesso.') : (new WsProduto($this->unidade->user_token))->getApiError($msg);
            } 
            //apenas um assento
            return $this->salvarAssento($arrayTags, $assentoInicio);
        }
    }
    
    /**
     * Salvar assento
     * @param array $tags Array de tags
     * @param string $assento Assento atual
     * @return type
     */
    public function salvarAssento ($tags, $assento)
    {
        if (!empty($tags) && !empty($assento)) {
            $tagTmp = array_merge($tags, [['nome' => 'assento', 'valor' => $assento]]);
            if (!$this->checkTag($tagTmp)) {
                $titulo = '';
                foreach($tagTmp as $index => $tag) {
                    if ($tag['nome'] == 'fileira') {
                        $titulo = 'Fileira ' . $tag['valor'];
                    }
                    if ($tag['nome'] == 'assento') {
                        $titulo .= ' - Assento ' . $tag['valor'];
                    }
                }
                $this->nome = $titulo;
                $this->descricao = $titulo;
                $returnApi = $this->criarProduto();
                if ($returnApi->successo == '1') {
                    return $this->salvarTag($tagTmp, $returnApi->objeto->codigo);
                }
            }
            return (new WsProduto($this->unidade->user_token))->getApiError('<br>Assento já existe: ' . $assento);
        }
        return (new WsProduto($this->unidade->user_token))->getApiError('Faltam informações para salvar assento.');
    }

    /**
     * Editar produto
     * @todo somente um usuário logado como operador 
     */
    public function atualizarProduto() 
    {
        if ($this->validate()) {
            $api = new WsProduto($this->unidade->user_token);
            $cadastro = $api->alterarProduto([
                'codigo' => $this->codigo,
                'nome' => $this->nome,
                'descricao' => $this->descricao,
                'valor' => $this->valor,
                'desconto' => [
                    'fixo' => $this->descontoFixo,
                    'percentual' => $this->descontoPercentual
                ],
                'ativo' => $this->ativo,
                'id_agenda'=>$this->id_agenda
            ]);
            if (!$cadastro->successo)
                return $this->addError('ativo', $cadastro->erro->mensagem);
            return true;
        }
    }

    /**
     * Consulta um produto pelo ID
     */
    public function buscarProduto() {        
        $api = new WsProduto($this->unidade->user_token);
        $produto = $api->buscarProduto($this->codigo);
        if (!$produto->successo)
            return $this->addError('lista', $produto->erro->mensagem);
        return $this->loadFromApi($produto->objeto[0]);
    }

    /**
     * Buscar produtos usando filtros
     */
    public function buscarProdutosFiltro($filtro, $cache=true) 
    {
        $api = new WsProduto($this->unidade->user_token);
        $produtos = $api->listarProdutos($filtro, $cache);
        if (isset($produtos->successo) && $produtos->successo == '0')
            return $this->addError('lista', $produtos->erro->mensagem);
        foreach ($produtos->objeto as $produto) {
            $obj = new Produto($this->unidade);
            $obj->loadFromApi($produto);
            $this->lista[] = $obj;
        }
    }

    /**
     * Atribuir valores retornado pela API às propriedades
     */
    public function loadFromApi($objeto) {
        if (!isset($objeto->nome, $objeto->valor, $objeto->ativo))
            return false;
        $this->codigo = $objeto->codigo;
        $this->nome = $objeto->nome;
        $this->descricao = $objeto->descricao;
        $this->valor = $objeto->valor;
        $this->descontoFixo = $objeto->desconto->fixo;
        $this->descontoPercentual = $objeto->desconto->percentual;
        $this->oferecido = $objeto->estoque->oferecido;
        $this->vendido = $objeto->estoque->vendido;
        $this->reservado = $objeto->estoque->reservado;
        $this->disponivel = $objeto->estoque->disponivel;
        $this->reserva_exclusiva = $objeto->estoque->reserva_exclusiva;
        $this->ativo = $objeto->ativo;
        $this->tags = isset($objeto->tags) ? $objeto->tags : null;
        $this->anexos = isset($objeto->anexos) ? $objeto->anexos : null;
        $this->id_agenda = $objeto->agenda->id_agenda;
        $this->agenda = $objeto->agenda;
        $this->vendedor = $objeto->vendedor;
        return true;
    }

    /**
     * Excluir um produto
     */
    public function excluirProduto() 
    {
        if (!isset($this->codigo))
            return false;
        $wsProduto = new WsProduto($this->unidade->user_token);
        return $wsProduto->excluirProduto(['codigo' => $this->codigo]);
    }

    /**
     * Trazer a lista de produtos com base na tag "matriz", que deve ser igual ao parâmetro $evento
     * @param string $evento valor da tag "apelido" de um registro de evento
     */
    public function listarProdutosDeEvento($evento) 
    {
        $ws = new WsProduto($this->unidade->user_token);
        $this->buscarProdutosFiltro(['tags' => ['matriz' => $evento]]);
        return $this->lista;
    }

    /**
     * Trazer a lista de produtos com as mesmas regras de listarProdutosDeEvento adicionando que devem ter ativo=1 e ser do tipo setor
     * @param string $evento valor da tag "apelido" de um registro de evento
     */
    public function listarSetoresProdutosAtivosDeEvento($evento)
    {
        $this->buscarProdutosFiltro(['ativo'=>'1', 'tags'=>[
            ['nome'=>'matriz','valor'=>$evento],
            ['nome'=>'tipo','valor'=>'setor']
        ]]);
        return $this->lista;
    }

    /**
     * Buscar um ou mais produtos com base em código ou critérios de pesquisa
     * @param mixed $post array de consulta ou código do produto
    */
    public function selecionarProdutos($post){
        if (is_array($post['filtro']))
            $this->buscarProdutosFiltro($post['filtro']);
        if (!is_array($post['filtro']) && is_integer(intval($post['filtro']))) {
            $this->codigo=$post['filtro'];
            $this->buscarProduto();
        }        
    }

    /**
     * Remover tag
     */
    public function removerTag($nome){
        $ws = new WsProduto($this->unidade->user_token);
        if (!$ws->excluirTag(['codigo'=>$this->codigo,'nome'=>$nome]))
            $this->addError('tags',$ws->erro->mensagem);
        return true;
    }

    /**
     * Remover anexo
     */
    public function removerAnexo($nome,$tipo){
        $ws = new WsProduto($this->unidade->user_token);
        if (!$ws->excluirAnexo(['codigo'=>$this->codigo,'nome'=>$nome,'tipo'=>$tipo]))
            $this->addError('anexos',$ws->erro->mensagem);
        return true;
    }

    /**
     * Alterar estoque de um produto
     */
    public function alterarEstoque($post){
        $ws = new WsProduto($this->unidade->user_token);
        $alterar=$ws->alterarEstoque($post);
        if(!$alterar->successo)
            $this->addError('unidadesEstoque',$alterar->erro->mensagem);
        return true;
    }

    /**
     * Retorna o objeto do webservice WsAgenda com o objeto Cliente correspondente
     */
    public function getWs(){
        return new WsProduto($this->unidade->user_token);
    }

    /* Checar se tags já estão cadastradas - Indicar se produto já está cadastrado
     * @param array $post Array de tags
     * @return boolean true: existe / false: não existe
     */
    public function checkTag($post) 
    {
        if (isset($post) && !empty($post)) {
            $this->buscarProdutosFiltro(['tags' => $post]);
        }
        return empty($this->lista) ? false : true;
    }
    
    /**
     * Salvar tags para registros
     * @param array $post Tags
     * @param integer $codigo Código do produto
     * @return type
     */
    public function salvarTag($post, $codigo) 
    {
        $ws = new WsProduto($this->unidade->user_token);
        if (!empty($post) && (int)$codigo > 0) {
            $tags = new Tags();
            $tags->scenario = 'create';
            $return = true;
            foreach($post as $index => $tag) {
                $tags->load(['Tags' => $tag]);
                $returnApi = $tags->adicionar($ws, $codigo);
                if ($returnApi->successo == '0') {
                    $return = false;
                }
            }
            return $return ? $ws->getApiSuccess('Cadastro efetuado com sucesso.') : $ws->getApiError('Falha ao efetuar cadastrar.');
        }
        return $ws->getApiError('Faltam parâmetros para salvar as tags.');
    }
    
    /**
     * Ao modificar status, gravar tag correspondente
     * @param type $post
     */
    public function salvarTagStatus ($post) 
    {
        $ws = new WsProduto($this->unidade->user_token);
        if (!empty($post)) {
            $return = true;
            $msg = '';
            foreach($post['codigos'] as $codigo) {
                $returnApi = $this->salvarTag([[
                    'nome' => ((int)$post['alterar_status'] == 1 ? 'desbloqueio' : 'bloqueio')  . '_' . date('YmdHis'), 
                    'valor' => $post['justificativa']
                ]], $codigo);
                if ($returnApi->successo == '0') {
                    $return = false;
                    $msg .= '<br>' . $returnApi->erro['mensagem'];
                }
            }
            return $return ? $ws->getApiSuccess('Bloqueio/desbloqueio efetuado com sucesso.') : $ws->getApiError($msg);
        }
        return $ws->getApiError('Falha ao registrar bloqueio/desbloqueio de produtos.');
    }
    
    /**
     * Reservar produtos para clientes
     * @param array $post Array de informações para registrar tag de reserva de produto
     */
    public function salvarTagReserva ($post) 
    {
        $ws = new WsProduto($this->unidade->user_token);
        if (isset($post['codigos']) && !empty($post['codigos']) && isset($post['justificativa']) && isset($post['documento'])) {
            $dtReserva = date('YmdHis');
            $return = true;
            $msg = '';
            foreach($post['codigos'] as $codigo) {
                $returnApi = $this->salvarTag([[
                    'nome' => ((int)$post['reserva'] == 1 ? 'reservar' : 'liberar') . '_' . $dtReserva,
                    'valor' => $post['documento']
                ]], $codigo);
                if ($returnApi->successo == '0') {
                    $return = false;
                    $msg .= '<br>' . $returnApi->erro['mensagem'];
                }
                $returnApi1 = $this->salvarTag([[
                    'nome' => ((int)$post['reserva'] == 1 ? 'reservarjust' : 'liberarjust') . '_' . $dtReserva,
                    'valor' => $post['justificativa']
                ]], $codigo);
                if ($returnApi1->successo == '0') {
                    $return = false;
                    $msg .= '<br>' . $returnApi1->erro['mensagem'];
                }
            }
            return $return ? $ws->getApiSuccess('Reserva/liberação efetuada com sucesso.') : $ws->getApiError($msg);
        }
        return $ws->getApiError('Falha ao registrar reserva/liberação de produtos.');
    }
    
    /**
     * Definir assentos especiais
     * @param array $post Array de parâmetros para definir tags de assentos especiais
     */
    public function assentoEspecial($post) 
    {
        if (isset($post['acao'])) {
            return $post['acao'] == '1' ? $this->addAssentoEspecial($post) : $this->delAssentoEspecial($post);
        }
        return (new WsProduto($this->unidade->user_token))->getApiError('Faltam parâmetros para definir assentos especiais.');
    }

    /**
     * Adicionar tags de assentos especiais
     * @param array $post Array de parâmetros para adicionar tags de assentos especiais
     */
    public function addAssentoEspecial ($post)
    {
        $return = true;
        $msg = '';
        foreach($post['codigos'] as $index => $codigo) {
            $returnApi = $this->salvarTag([['nome' => 'especial', 'valor' => $post['tipo']]], $codigo);
            if ($returnApi->successo == '0') {
                $msg .= $returnApi->erro['mensagem'];
                $return = false;
            }
        }
        return $return ? (new WsProduto($this->unidade->user_token))->getApiSuccess('Assentos especiais adicionados com sucesso.') : (new WsProduto($this->unidade->user_token))->getApiError($msg);
    }
    
    /**
     * Remover tags de assentos especiais
     * @param array $post Array de parâmetros para remover tags de assentos especiais
     */
    public function delAssentoEspecial ($post)
    {
        $return = true;
        $msg = '';
        $ws = new WsProduto($this->unidade->user_token);
        foreach($post['codigos'] as $index => $codigo) {
            $returnApi = $ws->excluirTag(['codigo' => $codigo, 'nome' => 'especial']);
            if ($returnApi->successo == '0') {
                $msg .= $returnApi->erro['mensagem'];
                $return = false;
            }
        }
        return $return ? (new WsProduto($this->unidade->user_token))->getApiSuccess('Assentos especiais definidos com sucesso.') : (new WsProduto($this->unidade->user_token))->getApiError($msg);
    }
    
    /**
     * Alterar produtos em lote
     * @param array $post Informações para alterar produtos em lote
     */
    public function alterarEmLote($post)
    {
        if (!empty($post)) {
            $filtro = [];
            if (isset($post['codigos']) && !empty($post['codigos'])) {
                $filtro['codigos'] = [];
                foreach($post['codigos'] as $codigo) {
                    array_push($filtro['codigos'], ['codigo' => $codigo]);
                }
            }
            if (isset($post['nome']) && !empty($post['nome'])) {
                $filtro['nome'] = $post['nome'];
            }
            if (isset($post['descricao']) && !empty($post['descricao'])) {
                $filtro['descricao'] = $post['descricao'];
            }
            if (isset($post['valor']) && !empty($post['valor'])) {
                $filtro['valor'] = $post['valor'];
            }
            if (isset($post['descontoFixo']) && !empty($post['descontoFixo'])) {
                $filtro['desconto']['fixo'] = $post['descontoFixo'];
            }
            if (isset($post['descontoPercentual']) && !empty($post['descontoPercentual'])) {
                $filtro['desconto']['percentual'] = $post['descontoPercentual'];
            }
            if (isset($post['status']) && !empty($post['status'])) {
                $filtro['ativo'] = $post['status'];
            }
            if (isset($post['tags']) && !empty($post['tags'])) {
                $filtro['tags'] = $post['tags'];
            }
            if (isset($post['anexos']) && !empty($post['anexos'])) {
                $filtro['anexos'] = $post['anexos'];
            }
            if (isset($post['estoque']) && !empty($post['estoque'])) {
                $filtro['estoque'] = $post['estoque'];
            }
            
            $alterar = [];
            if (isset($post['alterar_nome']) && !empty($post['alterar_nome'])) {
                $alterar['nome'] = $post['alterar_nome'];
            }
            if (isset($post['alterar_descricao']) && !empty($post['alterar_descricao'])) {
                $alterar['descricao'] = $post['alterar_descricao'];
            }
            if (isset($post['alterar_valor']) && !empty($post['alterar_valor'])) {
                $alterar['valor'] = $post['alterar_valor'];
            }
            if (isset($post['alterar_descontoFixo']) && !empty($post['alterar_descontoFixo'])) {
                $alterar['desconto']['fixo'] = $post['alterar_descontoFixo'];
            }
            if (isset($post['alterar_descontoPercentual']) && !empty($post['alterar_descontoPercentual'])) {
                $alterar['desconto']['percentual'] = $post['alterar_descontoPercentual'];
            }
            if (isset($post['alterar_validadeInicial']) && !empty($post['alterar_validadeInicial'])) {
                $alterar['validade']['inicial'] = $post['alterar_validadeInicial'];
            }
            if (isset($post['alterar_validadeFinal']) && !empty($post['alterar_validadeFinal'])) {
                $alterar['validade']['final'] = $post['alterar_validadeFinal'];
            }
            if (isset($post['alterar_status'])) {
                $alterar['ativo'] = (int)$post['alterar_status'];
            }
            $returnApi = (new WsProduto($this->unidade->user_token))->alterarProdutoEmLote([
               'filtro' => $filtro,
               'alterar' => $alterar
            ]);
            if ($returnApi->successo == '1' && isset($alterar['ativo']) && isset($post['justificativa']) && !empty($post['justificativa'])) {
                $this->salvarTagStatus($post);
            }
            return $returnApi;
        }
        return (new WsProduto($this->unidade->user_token))->getApiError('Faltam parâmetros para alterar produtos em lote.');
    }

    /**
     * Alterar produtos em lote passando o objeto sem validações
     * @param array $post objeto para filtrar/alterar
     */
    public function alterarEmLoteSemValidacao($post)
    {
        return (new WsProduto($this->unidade->user_token))->alterarProdutoEmLote($post);
    }
    
    /**
     * Alterar estoque de produto em lote
     * @param array $post Array de informações de produtos para alterar o estoque
     */
    public function alterarEstoqueLote ($post)
    {
        return (new WsProduto($this->unidade->user_token))->alterarEstoque([
            'codigo' => implode(',', $post['codigos']), 
            'tid' => substr(time() . mt_rand(), 8, 11), 
            'unidades' => ($post['acao'] == '1' ? '' : '-') . (int)$post['alterar_estoque']
        ]);
    }
    
    /**
     * Clonar produtos de uma data para outra. Método necessário para atribuir mapa de assentos ao evento
     * @param int $origem o ID da data de origem a ser clonada (normalmente será o id de evento matriz da unidade)
     * @param int $destino o ID da data para a qual serão clonados os produtos
     */
    public function clonarProdutoAgenda($origem, $destino){
        $ws=new WsProduto($this->unidade->user_token);
        return $ws->clonarProdutoAgenda(['de'=>$origem, 'para'=>$destino]);
    }

    /**
     Checar se o produto possui tags de fileira e assento. Serve para identificar entre produto de assento marcado ou livre
     */
    public function tagAssento(){
        if(!isset($this->tags)) return false;
        $tags=Tags::objetoTags($this->tags);
        return isset($tags->fileira,$tags->assento);
    }
}
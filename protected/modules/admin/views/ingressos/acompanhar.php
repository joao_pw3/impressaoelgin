 <?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\modules\admin\assets\BilheteriaAsset;

BilheteriaAsset::register($this);

$this->title = 'ADM Estádio Mineirão :: Acompanhar lotes';
$session = Yii::$app->session;
$session->open();
?>
<div class="container">
    
    <div class="row">
        <h2 class="tituloGeral"><i class="fa fa-clone"></i> Acompanhar lotes</h2>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'form-acompanhar-ingresso',
    ]); ?>
    <div id="localizar" class="row boxLer">
        <div class="ingresso col-md-4">
            <div class="form-group field-idEvento">
                <label for="idEvento" class="control-label">Eventos</label>
                <select name="BilheteriaModel[idEvento]" class="form-control">
                    <?php foreach($eventos as $index => $evento) { ?>
                    <option value="<?= $evento->codigo ; ?>"<?= isset($post['idEvento']) && $post['idEvento'] == $evento->codigo ? ' selected' : ''; ?>><?= $evento->nome; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <button type="submit" id="consultar-ingresso" class="btn btn-success btn-consulta"><i class="fa fa-check"></i> Buscar</button>
        </div>    
    </div>
    <br><hr class="bordaPont">
    <?php ActiveForm::end(); ?>
    
    <div class="row">
        <div class="col-md-12" id="data-venda">
<?php if (!empty($lotes)) { ?>
            <table class="uppercase table table-responsive table-striped">
                <thead>
                    <tr>
                        <th>Lote</th>
                        <th>Lote gráfica</th>
                        <th>Códigos de venda</th>
                        <th>Total de ingressos</th>
                        <th>1 - Reserva de lote</th>
                        <th>2 - Lote "em produção"</th>
                        <th>3 - Lote "enviado ao estádio"</th>
                        <th>4 - Lote "recebido no estádio"</th>
                        <th>5 - Associado ingresso a mifare</th>
                        <th>6 - Lote "liberado para bilheteria"</th>
                    </tr>
                </thead>
                <tbody>            
    <?php foreach ($lotes as $lote => $historico) { ?>
                    <tr>
                        <td><?= $lote; ?></td>
                        <td><?= $historico['lote_grafica']; ?></td>
                        <td><strong><?= $historico['corte_inferior'] . '</strong> a <strong>' . $historico['corte_superior']; ?></strong></td>
                        <td><?= $historico['total']; ?></td>
                        <td><?= date('d/m/Y H:i', strtotime($historico['data_reserva'])); ?></td>
                        <td><?= isset($historico['data_producao']) ? 
                                date('d/m/Y H:i', strtotime($historico['data_producao'])) : 
                                (in_array('grafica/lote-producao', $session['permissoes_usuario']) ? 
                                Html::a('enviar para produção', Url::to(['grafica/lote-producao'])) : '-'); ?></td>
                        <td><?= isset($historico['data_envio']) ? 
                                date('d/m/Y H:i', strtotime($historico['data_envio'])) : 
                                (in_array('grafica/lote-estadio', $session['permissoes_usuario']) && isset($historico['data_producao']) ? 
                                Html::a('enviar ao estádio', Url::to(['grafica/lote-estadio'])) : '-'); ?></td>
                        <td><?= isset($historico['data_entrega']) ? 
                                date('d/m/Y H:i', strtotime($historico['data_entrega'])) : 
                                (in_array('ingressos/lote-entrega', $session['permissoes_usuario']) && isset($historico['data_envio']) ? 
                                Html::a('receber no estádio', Url::to(['ingressos/lote-entrega'])) : '-'); ?></td>
                        <td><?= isset($historico['data_associacao']) ? 
                                date('d/m/Y H:i', strtotime($historico['data_associacao'])) : 
                                (in_array('ingressos/associar-ingressos', $session['permissoes_usuario']) && isset($historico['data_entrega']) ? 
                                Html::a('associar mifare', Url::to(['ingressos/associar-ingressos'])) : '-'); ?></td>
                        <td><?= isset($historico['data_liberado']) ? 
                                date('d/m/Y H:i', strtotime($historico['data_liberado'])) : 
                                (in_array('ingressos/lote-liberar', $session['permissoes_usuario']) && isset($historico['data_associacao']) ? 
                                Html::a('liberar para bilheteria', Url::to(['ingressos/lote-liberar'])) : '-'); ?></td>
                    </tr>
    <?php } ?>
                </tbody>
            </table>  
<?php } else if (isset($post['idEvento']) && isset($lotes) && empty($lotes)) { ?>
    <div class="alert alert-warning">Não há lote de ingressos para o evento escolhido.</div>
<?php } else { ?>
    <div class="alert alert-warning">Escolha um evento para acompanhar os seus lotes de ingressos.</div>
<?php } ?>
        </div>
    </div>
</div>
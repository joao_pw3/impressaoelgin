<?php
$this->title = 'ADM Easy for Pay';
?>

<div class="container">
	<div class="row">
		<h2 class="tituloGeral"><i class="fa fa-clone"></i> Agenda - Adicionar TAG</h2>
	</div>
	<div class="boxArredondado" style="margin-top:30px;">
		<h2 style="margin-top:0px;"><?=$evento->nome?></h2>
		<p><?=$evento->dataInicial?> - <?=$evento->dataFinal?></p>
	</div>

	<h4 style="margin-top:30px;">Adicione a TAG</h4>
	<?= $this->render('/commons/form-tag',['model'=>$model])?>
</div>
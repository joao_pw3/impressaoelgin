<?php
namespace app\controllers;

use Yii;
use yii\web\Response;
use app\controllers\ClienteController;
use app\models\Agenda;
use app\models\Cliente;
use app\models\Carrinho;
use app\models\Tags;
use app\models\Cupom;
use app\models\Produto;

class AgendaController extends ClienteController
{

    /**
     * Mostrar evento. Exibe apenas um se for passado o ID pelo parâmetro $evento, caso contrário retorna um erro
     * O método getMapa do objeto Mapa criado no render pede como parâmetros o primeiro horário registrado e a matriz (unidade)
     * @param integer $evento Id do evento
     * @return string
     */
    public function actionIndex($evento)
    {
        $session = Yii::$app->session;
        $session->open();
        $tokenEmSessao=$session->get('token_unidade',false);
        $objApiCliente = new Cliente;
        if($tokenEmSessao!==$objApiCliente->getTokenMatriz())
            $session->set('lista_unidades',null);
        $objApiCliente->buscarUnidadePor('user_token',$objApiCliente->getTokenMatriz());
        $session->set('token_unidade',$objApiCliente->user_token);
        $agenda = new Agenda($objApiCliente);
        $agenda->codigo = $evento;
        $agenda->umEvento();
        //$objApiCliente->buscarUnidadePor('documento',$agenda->empresa->documento);
        $tags = new Tags();
        $tagAgenda = $tags->arrayTags($agenda->tags);
        if ($agenda->hasErrors() || !$agenda->ativo) {
            return $this->render('/site/error',['name'=>'Evento não disponível', 'message'=>'Evento não disponível']);
        }

        $produtoApi=new Produto($objApiCliente);
        $produtoApi->buscarProdutosFiltro(['id_agenda'=>$agenda->datas[0]->id]);
        
        $carrinho = (new Carrinho($objApiCliente))->consultarCarrinho();
        $session->set('cobrar_taxa',false);
        
        if(isset($carrinho->objeto,$carrinho->objeto->produtos) && $produtoApi->lista[0]->codigo!=$carrinho->objeto->produtos[0]->codigo){
            return $this->render('compra-em-andamento', [
                'evento' => $agenda,
                'tags' => $tagAgenda,
            ]);
        }

        return $this->render('evento', [
            'outroEvento' => $agenda->selecionouOutraUnidade($agenda->empresa->documento),
            'evento' => $agenda,
            'horarios' => $agenda->separarHorariosDoMesmoDia(),
            'tags' => $tagAgenda,
            'tipoAssento' => $tagAgenda['assento'],
            'carrinho' => $carrinho,
            'produtos'=>$produtoApi->lista,
        ]);
    }

    /**
     * Mostrar evento especial para venda externa.
     * Semelhante ao actionIndex, porém este pede um código de um cupom privado que permite uma compra especial
     * @param integer $evento Id do evento
     * @param string $cupom o código do cupom
     */
    public function actionVendaExterna($evento,$cupom)
    {
        $session = Yii::$app->session;
        $session->open();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token',$objApiCliente->getTokenMatriz());
        
        $agenda = new Agenda($objApiCliente);
        $agenda->codigo = $evento;
        $agenda->umEvento();
        $primeiroEvento=$agenda->separarHorariosDoMesmoDia();
        $objApiCliente->buscarUnidadePor('documento',$agenda->empresa->documento);
        $session->set('token_unidade',$objApiCliente->user_token);
        
        $tags = new Tags();
        $tagAgenda = $tags->objetoTags($agenda->tags);
        
        $objCupom=new Cupom($objApiCliente);
        $objCupom->buscarCupom(['id'=>$cupom]);
        $cupomOk=$agenda->checkCupom($objCupom,$evento,$primeiroEvento[key($primeiroEvento)]['horarios'][0]);
        
        if ($agenda->hasErrors()) {
            $msgErro='Evento não disponível';
            if(!$cupomOk)
                $msgErro=$agenda->getErrors('cupom')[0];
            return $this->render('/site/error',['name'=>'Evento não disponível', 'message'=>$msgErro]);
        }
        $session->set('cobrar_taxa',true);
        return $this->render('evento', [
            'outroEvento' => $agenda->selecionouOutraUnidade($agenda->empresa->documento),
            'evento' => $agenda,
            'horarios' => $primeiroEvento,
            'tags' => $tagAgenda,
            'tipoAssento' => $tagAgenda->assento,
            'carrinho' => (new Carrinho($objApiCliente))->consultarCarrinho()
        ]);
    }

    /**
     * Trazer a quantidade de produtos do evento atual (utilizado para produtos livres)
     * @param int $data código da data do evento sendo exibido
    */
    public function actionQtdeProdutos($data){
        $session = Yii::$app->session;
        $session->open();
        $token=$session->get('token_unidade',false);
        if (!Yii::$app->request->isAjax || !$token) return false;
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token',$token);
        
        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $qtde=Carrinho::qtdeProdutosEvento((new Carrinho($objApiCliente))->consultarCarrinho(),$data);
            if($qtde>0)
                return ['successo'=>'1','objeto'=>['qtde'=>$qtde]];
            return ['successo'=>'0','erro'=>['mensagem'=>'Falha ao consultar a quantidade']];
        }
        
    }
}

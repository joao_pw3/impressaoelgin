<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="container row">
	<div class="col-md-3">
		<h2>Gestão de permissões</h2>
		<ul>
			<li><a href="<?=Url::to(['usuarios/permissoes'])?>">Adicionar permissão</a></li>
			<li><a href="<?=Url::to(['usuarios/listar-permissoes'])?>">Listar/gerenciar permissões</a></li>
		</ul>
	</div>
	<div class="col-md-9">
		<h3>Permissões salvas com sucesso</h3>
		<p><a href="<?=Url::to(['/admin/usuarios/permissoes'])?>">Voltar</a></p>
	</div>
</div>


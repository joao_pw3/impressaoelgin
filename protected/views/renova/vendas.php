<table class="table table-striped table-hover">
  <thead>
    <tr>
      <th>Código da transação</th>
      <th>Online / presencial</th>
      <th>Data da compra</th>
      <th>Primeira parcela</th>
      <th>Valor (bruto/líquido)</th>
      <th>Forma de pagamento</th>
      <th>Parcelas<!-- <a class="mostrar infoTodasParcelas">[+]</a>--></th>
    <tr>
  </thead>
  </tbody>
  <?php
    foreach ($transacoes as $t) {
      echo '<tr>'
        . '<td>'.$t['codTransacao'].'</td>'
        . '<td>'.$t['tipo'].'</td>'
        . '<td>'.$t['dataTransacao']->format('d/m/Y G:i:s').'</td>'
        . '<td>'.$t['dataVencimento']->format('d/m/Y').'</td>'
        . '<td>R$ '.number_format($t['bruto'],2,',','.').' / R$ '.number_format($t['valor'],2,',','.').'</td>'
        . '<td>'.$t['formaPgto'].'</td>'
        . '<td>'.$t['parcelas'].($t['parcelas']>1 ?' <a class="mostrar infoParcelas" id="linhaCompra'.$t['codTransacao'].'">[+]</a>': '').'</td>'
      . '</tr>';

      if($t['parcelas']>1){
        echo '<tr class="infoParcelas hidden" id="headerInfo'.$t['codTransacao'].'">'
          . '<td></td>'
          . '<td></td>'
          . '<td>Parcela</td>'
          . '<td>Vencimento</td>'
          . '<td>Valor (bruto/líquido)</td>'
          . '<td></td>'
          . '<td></td>'
          . '</tr>';
        foreach ($t['detalhes'] as $detalhe) {
          echo '<tr class="infoParcelas hidden" id="bodyInfo'.$t['codTransacao'].'_'.$detalhe['parcela'].'">'
            . '<td></td>'
            . '<td></td>'
            . '<td>'.$detalhe['parcela'].'</td>'
            . '<td>'.$detalhe['dataVencimento']->format('d/m/Y').'</td>'
            . '<td>R$ '.number_format($detalhe['bruto'],2,',','.').' / R$ '.number_format($detalhe['valor'],2,',','.').'</td>'
            . '<td></td>'
            . '<td></td>'
            . '</tr>';
        }
      }
    }
    ?>
  </tbody>
</table>
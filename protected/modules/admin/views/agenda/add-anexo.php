<?php
$this->title = 'ADM Easy for Pay';
?>
<div class="container">
	<div class="row">
		<h2 class="tituloGeral"><i class="fa fa-clone"></i> Agenda - Adicionar Anexo</h2>
	</div>
	<?= $this->render('/commons/form-anexo',['model'=>$model])?>
</div>
<?php
namespace app\models;

use yii\base\Model;

class VendaBilheteria extends Model 
{
    public $total;
    public $parcelas;
    public $forma_pagamento;
    public $valor_final;
    public $troco;

    public function rules() 
    {
    	return [
            [['total'], 'required', 'on' => 'iniciar-pedido'],
            [['total','valor_final','troco'], 'string'],
            [['parcelas', 'forma_pagamento'], 'integer']
    	];
    }

    /**
     * definição dos labels de atributos
     */
    public function attributeLabels()
    {
        return[
            'total' => 'Total',
            'parcelas' => 'Parcelamento',
            'forma_pagamento' => 'Forma de pagamento',
            'troco'=> 'Recebido'
        ];
    } 

    public function geraParcelas($minParcelas = 1, $maxParcelas = 12)
    {
        if($maxParcelas>1)
            $parcelas = [''=>'Parcelas'];

        $this->total = preg_replace('/(\D)/','', $this->total)/100;
        if($this->total > 0)
        {
            while($minParcelas<=$maxParcelas)
            {
                $label=$minParcelas>1 ?' parcelas ' :' parcela ';
                $parcelas[$minParcelas]=$minParcelas.$label.'de R$ '.number_format($this->total/$minParcelas,2,',','.');
                $minParcelas++;                        
            }     
            return (object) ['successo'=>'1', 'parcelas'=>$parcelas, 'total'=>$this->total];
        }   
        return (object) ['successo'=>'0', 'message'=>'Valor inválido.'];      
    }

    public function formasPagamento($find=false)
    {
        $formas = [''=>'Escolha',4 => 'Especie',2 => 'Debito',1 => 'Credito', 100=>'Vale Cultura'];
        if($find !== false) {
            return $formas[$find];
        }
        return $formas;        
    }
}
<?php
namespace app\assets;

use yii\web\AssetBundle;

class CapptaAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        YII_ENV_DEV ? 'js/cappta_init_dev.js' : 'js/cappta_init.js',
        'js/cappta.js',
        'js/venda-cappta.js',
        'https://s3.amazonaws.com/cappta.api/v2/dist/cappta-checkout.js'         
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    public $publishOptions = [
        'forceCopy' => true,
    ];
}

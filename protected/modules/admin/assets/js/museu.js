var capptaObj;
// submit de pagamento avulso
$("#museu-finaliza-venda-form :submit").click(function(e){
    e.preventDefault();
    e.stopPropagation();
    var tipo=$("#museu-finaliza-venda-form input[name='tipo']:checked").val();
    var total=$("#museu-finaliza-venda-form input[name='total']").val();
    var parcelas=$("#museu-finaliza-venda-form select[name='parcelas']").val();
    var prosseguir=parametrosVendaOk(capptaObj,tipo,total,parcelas);
    if(prosseguir.ok){
        $("#cappta-checkout-iframe").removeClass('hidden');
        switch(tipo){
            case 'debito':
                pagamentoDebito(capptaObj,parseFloat(total));
                break;
            case 'credito':
                pagamentoCredito(capptaObj,parseFloat(total),parcelas);
                break;
        }
    }
    if(prosseguir.erro)
        showMsg(prosseguir.mensagem);
}); 

// mostrar/esconder campo de parcelamento
$("#museu-finaliza-venda-form input[name='tipo']").click(function(e){
    if(e.target.value=='credito') {
        $("#museu-finaliza-venda-form select[name='parcelas']").removeClass('hidden').prop('disabled', false);
        //$("#museu-finaliza-venda-form select[name='parcelas'] option:nth-child(2)").prop('selected', true);
    }
    else
        $("#museu-finaliza-venda-form select[name='parcelas']").addClass('hidden');
}); 

$('.cancelamento').click(function(){
    var controle = $(this).data('numero');
    $('#controle').val(controle);
    var prosseguir=parametrosCancelar(capptaObj, controle);
    if(prosseguir.ok){
        $("#cappta-checkout-iframe").removeClass('hidden');
        cancelamentoVenda(capptaObj, controle);
    }
    if(prosseguir.erro)
        console.log(prosseguir.mensagem);


});

function printRecibo($this){
    var tela = window.open('', 'Impressão', 'left=0,top=5,right=0,height=800,width=800');
        tela.document.open('text/html');
        tela.document.write('<pre>'+$($this).data('recibo'));
        tela.document.close();
        tela.window.focus();
        tela.window.print();
        tela.document.write('Aguarde, o recibo está sendo impresso. Após o término, feche a janela.'); 
}

var capptaAuthSuccess = function (response) {
    merchantCheckoutGuid = response.merchantCheckoutGuid;
    $("#cappta-status").html('Autenticado com sucesso' + '<br>' + 'Checkout GUID: ' + merchantCheckoutGuid);
    $("#cappta-checkout-iframe").addClass('hidden');
};

var capptaAuthError = function (error) {
    $("#cappta-status").html('Código: ' + error.reasonCode + '<br>' + error.reason);
};

var capptaInit=function(){
    capptaObj=CapptaCheckout.authenticate(capptaAuth, capptaAuthSuccess, capptaAuthError);
};

var parametrosVendaOk=function(obj,tipo,total,parcelas){
    var retorno={'ok':true};
    if(obj instanceof Object == false)
        retorno={'erro':true,'mensagem':'O pinpad não está conectado ou está com problemas. Conecte-o e recarregue a página'};
    if(tipo!='debito' && tipo!='credito')
        retorno={'erro':true,'mensagem':'Selecione a forma de pagamento'};
    if(isNaN(total) || !total>0 || isNaN(parseFloat(total)))
        retorno={'erro':true,'mensagem':'O total da compra é inválido. Reinicie a operação'};
    if(tipo=='credito' && (isNaN(parcelas) || !parcelas>0))
        retorno={'erro':true,'mensagem':'Selecione a quantidade de parcelas'};
    return retorno;
}

var parametrosCancelar=function(obj, controle){
    var retorno={'ok':true};
    if(obj instanceof Object == false)
        retorno={'erro':true,'mensagem':'O pinpad não está conectado ou está com problemas. Conecte-o e recarregue a página'};
    if(isNaN(controle) || controle.length != 11)
        retorno={'erro':true,'mensagem':'O número de controle é inválido. Reinicie a operação'};
    return retorno;
}

function processamentoOK(data) 
{
    $('.impressao_recibo').removeClass('hide');
    $('#reciboConsumidor').data('recibo',data.recibo_consumidor);
    $('#reciboEstabelecimento').data('recibo',data.recibo_estabelecimento);
}

function cancelamentoOK(data) {
    $('.impressao_estorno').removeClass('hide');
    $('#reciboConsumidor').data('recibo',data.recibo_consumidor);
    $('#reciboEstabelecimento').data('recibo',data.recibo_estabelecimento);
}

function processamentoWrong(data) {
    console.log(data);
}

var pgtoOk=function(r){
    var total = $("#museu-finaliza-venda-form input[name='total']").val(),
        tipo = $("#museu-finaliza-venda-form input[name='tipo']:checked").val(); 
    var data = {'total' : total, 'tipo': tipo, 'guid': merchantCheckoutGuid, 'data': r};
    doAjax('confirma-venda', 'post', data, 'json', 3000, 'processamentoOK', 'processamentoWrong');
};

var pgtoErr=function(r){
    console.log(r.reasonCode);
    console.log(r.reason);
};

var cancelOk = function(r) {
    var data = {'controle': $('#controle').val(),'guid': merchantCheckoutGuid, 'data': r};
    doAjax('confirma-cancelamento', 'post', data, 'json', 3000, 'cancelamentoOK', 'processamentoWrong');
};

var pagamentoDebito=function(objCappta,valor){
    objCappta.debitPayment({amount:valor}, pgtoOk, pgtoErr);
};

var pagamentoCredito=function(objCappta,valor,parcelas){
    objCappta.creditPayment({
        amount:valor,
        installments:parcelas,
        installmentsType:1
    }, pgtoOk, pgtoErr);
};

var cancelamentoVenda=function(objCappta, controle){
    objCappta.paymentReversal({
        administrativePassword: 'cappta',
        administrativeCode: controle
    }, cancelOk, pgtoErr);
};
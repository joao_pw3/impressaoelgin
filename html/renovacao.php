<?php
die();
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
//$server = $_SERVER['HTTP_HOST'];
//if (stripos($server, 'localhost') > -1) {
//    require 'C:/wamp/www/mineirao/html/yii_init.php';
//    require 'C:/wamp/www/mineirao/protected/models/Renovacao.php';
//    require 'C:/wamp/www/mineirao/protected/models/Api.php';
//}
//if (stripos($server, 'easyp.com.br') > -1) {
//    require '/var/www/html/mineirao/web/yii_init.php';
//    require '/var/www/html/mineirao/models/Renovacao.php';
//    require '/var/www/html/mineirao/models/Api.php';
//}
//if (stripos($server, 'vendas.mineirao.com.br') > -1) {
    require '/var/www/html/yii_init.php';
    require '/var/www/protected/models/Renovacao.php';
    require '/var/www/protected/models/Api.php';
//}

class Temporada {

    /**
     * Enviar e-mail para clientes com link para renovação de temporada
     */
    public function sendMailRenovacao () 
    {
        $cliente = \app\models\Renovacao::find()->where(['envio_email' => 0])->one();
        if (!empty($cliente)) {
            $api = new \app\models\Api;
            $from = ['vendas@estadiomineirao.com.br' => 'Vendas Mineirão'];
            $to = [$cliente->email => $cliente->nome];
//            $to = ['marco.moraes@gmail.com' => 'Marco Moraes'];
            $subject = 'Mineirão Tribuna :: chegou a sua hora!';
            $content = '<img src="http://vendas.estadiomineirao.com.br/images/mineirao_tribuna.jpg"><br><br>
            Olá, ' . $cliente->nome . '! Como vai?<br><br>
            Temos certeza que você viveu momentos incríveis este ano no Mineirão! E agora (com alegria!) informamos que, por ter sido cliente 
            Temporada Tribuna 2017, você tem o direito preferencial para aquisição da sua cadeira temporada Mineirão Tribuna 2018. Para conferir as 
            condições e já garantir seu lugar no Mineirão para a temporada de 2018 acesse o link abaixo (ou copie-o para seu navegador), faça o 
            cadastro e adquira seu pacote com condições especiais até 02/01/2017.<br><br>
            Este link é específico para você: clique e garanta o seu direito preferencial de compra.<br><br>
            <a href="' . $cliente->url_compra . '">' . $cliente->url_compra . '</a><br><br>
            Como o pagamento é feito via cartão de crédito, se planeje junto ao seu banco para que a transação seja efetuada com sucesso.<br><br>
            Em caso de dúvidas relacionadas ao Mineirão Tribuna Temporada 2018, entre em contato com a Central de Atendimento do Mineirão 
            (31) 3499-4333 ou através do e-mail atendimento@estadiomineirao.com.br.<br><br>
            Para dúvidas relacionadas ao processamento do seu pagamento entre em contato com o Plantão de Atendimento da EasyForPay por telefone ou 
            WhatsApp (11) 97190-8834, ou também, através do e-mail contato@easyforpay.com.br. A EasyForPay é a empresa de pagamentos dos ingressos 
            Mineirão.<br><br>
            Estamos à disposição para tornar sua experiência cada vez mais incrível.<br><br>
            Um abraço Gigante.<br><br>
            <img src="http://vendas.estadiomineirao.com.br/images/mineirao_email.png">';
            $returnApi = $api->enviarEmailSwift($from, $to, $subject, $content);
            
            $db = Yii::$app->db;
            $db->createCommand('UPDATE renovacao 
                SET envio_email = "1",
                envio_ok = "' . ($returnApi->successo == '1' ? 1 : 0) . '",
                envio_erro = "' . ($returnApi->successo == '0' ? $returnApi->erro : '') . '",
                envio_data = "' . date('YmdHis') . '"
                WHERE email = "' . $cliente->email . '"')->execute();
            
//            $cliente->envio_email = 1;
//            $cliente->envio_ok = $returnApi->successo == '1' ? 1 : 0;
//            $cliente->envio_erro = $returnApi->successo == '0' ? $returnApi->erro : '';
//            $cliente->envio_data = date('YmdHis');
//            $cliente->update(false);
        }
    }
    
}

$renovar = new Temporada();
$renovar->sendMailRenovacao();

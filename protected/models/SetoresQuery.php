<?php
namespace app\models;

/**
 * This is the ActiveQuery class for [[SetoresModel]].
 *
 * @see SetoresModel
 */
class SetoresQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return SetoresModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SetoresModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

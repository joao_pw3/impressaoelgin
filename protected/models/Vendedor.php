<?php
namespace app\models;

use yii\base\Model;
use app\models\WsConta;

class Vendedor extends Model {
	public $user_token;
	public $user_id;
	public $user_ativo;
	public $documento;
	public $matriz;
	public $nome;
	public $razao;
	public $contato;
	public $ddd;
	public $telefone;
	public $cep;
	public $endereco;
	public $num;
	public $compl;
	public $cidade;
	public $cidade_nome;
	public $uf;
	public $bairro;
	public $segmento;
	public $segmento_nome;
	public $categoria;
	public $categoria_nome;
	public $site;
	public $descricao;
	public $url_logo;
	public $banco;
	public $banco_nome;
	public $ag;
	public $ag_dig;
	public $conta;
	public $conta_dig;

	public function rules(){
		return [
			[['documento'],'required'],
			[['nome','matriz'],'required', 'on'=>'cadastro-gerente'],
			[['matriz','nome','razao','contato','ddd','telefone','cep','endereco','num','compl','cidade','cidade_nome','uf','bairro','segmento','segmento_nome','categoria','categoria_nome','site','descricao','url_logo','banco','banco_nome','ag','ag_dig','conta','conta_dig'],'safe'],
		];
	}
	
	/**
	 * Alterar o cadastro de um vendedor
	 * @param string $tokenUsuario se for alterar a partir do ADMIN, é necesário passar o token do vendedor para poder atualizar. Por padrão é NULL, portanto deve buscar o token de vendedor do usuário logado
	 */
	
	public function alterarCadastro($tokenUsuario=null){
		$ws=new WsConta;
		$alterar=$ws->_put(is_null($tokenUsuario) ?$ws->getToken('V') :$tokenUsuario,[
			'vendedor'=>[
				'documento'=>$this->documento,
				'matriz'=>$this->matriz,
				'nome'=>$this->nome,
				'razao'=>$this->razao,
				'contato'=>$this->contato,
				'ddd'=>$this->ddd,
				'telefone'=>$this->telefone,
				'cep'=>$this->cep,
				'endereco'=>$this->endereco,
				'num'=>$this->num,
				'compl'=>$this->compl,
				'cidade'=>$this->cidade,
				'cidade_nome'=>$this->cidade_nome,
				'uf'=>$this->uf,
				'bairro'=>$this->bairro,
				'segmento'=>$this->segmento,
				'segmento_nome'=>$this->segmento_nome,
				'categoria'=>$this->categoria,
				'categoria_nome'=>$this->categoria_nome,
				'site'=>$this->site,
				'descricao'=>$this->descricao,
				'url_logo'=>$this->url_logo,
				'banco'=>[
					'banco'=>$this->banco,
					'banco_nome'=>$this->banco_nome,
					'ag'=>$this->ag,
					'ag_dig'=>$this->ag_dig,
					'conta'=>$this->conta,
					'conta_dig'=>$this->conta_dig
				]
			]
		]);
		if(!$alterar->successo)
			$this->addError('documento',$alterar->erro->mensagem);
		return $alterar;
	}

	/**
     * Atribuir valores retornado pela API às propriedades
     */
    public function loadFromApi($objeto) {
        if (!isset($objeto->user, $objeto->vendedor))
            return false;
        $this->user_token=$objeto->user->token;
        $this->user_id=$objeto->user->id;
        $this->user_ativo=$objeto->user->ativo;
        $this->documento=$objeto->vendedor->documento;
        $this->matriz=$objeto->vendedor->matriz;
        $this->nome=$objeto->vendedor->nome;
        $this->razao=$objeto->vendedor->razao;
        $this->contato=$objeto->vendedor->contato;
        $this->ddd=$objeto->vendedor->ddd;
        $this->telefone=$objeto->vendedor->telefone;
        $this->cep=$objeto->vendedor->cep;
        $this->endereco=$objeto->vendedor->endereco;
        $this->num=$objeto->vendedor->num;
        $this->compl=$objeto->vendedor->compl;
        $this->cidade=$objeto->vendedor->cidade;
        $this->cidade_nome=$objeto->vendedor->cidade_nome;
        $this->uf=$objeto->vendedor->uf;
        $this->bairro=$objeto->vendedor->bairro;
        $this->segmento=$objeto->vendedor->segmento;
        $this->segmento_nome=$objeto->vendedor->segmento_nome;
        $this->categoria=$objeto->vendedor->categoria;
        $this->categoria_nome=$objeto->vendedor->categoria_nome;
        $this->site=$objeto->vendedor->site;
        $this->descricao=$objeto->vendedor->descricao;
        $this->url_logo=$objeto->vendedor->url_logo;
        $this->banco=$objeto->vendedor->banco;
        $this->banco_nome=$objeto->vendedor->banco->banco_nome;
        $this->ag=$objeto->vendedor->banco->ag;
        $this->ag_dig=$objeto->vendedor->banco->ag_dig;
        $this->conta=$objeto->vendedor->banco->conta;
        $this->conta_dig=$objeto->vendedor->banco->conta_dig;
        return true;
    }

	/**
     * Atribuir valores retornado pela API às propriedades
     */
    public function loadFromApiUnico($objeto) {
        if (!isset($objeto->user_token, $objeto->vendedor))
            return false;
        $this->user_token=$objeto->user_token;
        $this->user_id=$objeto->user_id;
        $this->documento=$objeto->vendedor->documento;
        $this->matriz=$objeto->vendedor->matriz;
        $this->nome=$objeto->vendedor->nome;
        $this->razao=$objeto->vendedor->razao;
        $this->contato=$objeto->vendedor->contato;
        $this->ddd=$objeto->vendedor->ddd;
        $this->telefone=$objeto->vendedor->telefone;
        $this->cep=$objeto->vendedor->cep;
        $this->endereco=$objeto->vendedor->endereco;
        $this->num=$objeto->vendedor->num;
        $this->compl=$objeto->vendedor->compl;
        $this->cidade=$objeto->vendedor->cidade;
        $this->cidade_nome=$objeto->vendedor->cidade_nome;
        $this->uf=$objeto->vendedor->uf;
        $this->bairro=$objeto->vendedor->bairro;
        $this->segmento=$objeto->vendedor->segmento;
        $this->segmento_nome=$objeto->vendedor->segmento_nome;
        $this->categoria=$objeto->vendedor->categoria;
        $this->categoria_nome=$objeto->vendedor->categoria_nome;
        $this->site=$objeto->vendedor->site;
        $this->descricao=$objeto->vendedor->descricao;
        $this->url_logo=$objeto->vendedor->url_logo;
        $this->banco=$objeto->vendedor->banco;
        $this->banco_nome=$objeto->vendedor->banco->banco_nome;
        $this->ag=$objeto->vendedor->banco->ag;
        $this->ag_dig=$objeto->vendedor->banco->ag_dig;
        $this->conta=$objeto->vendedor->banco->conta;
        $this->conta_dig=$objeto->vendedor->banco->conta_dig;
        return true;
    }
}
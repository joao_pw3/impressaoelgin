<?php
namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\controllers\AbstractController;
use app\models\Agenda;
use app\models\Cliente;
use app\models\Produto;
use app\models\Mapa;
use app\models\Cupom;
use app\widgets\Ingresso;
use yii\web\Response;
use app\models\VendaBilheteria;
use app\widgets\Filipeta;
use app\models\Caixa;
use app\models\Cobranca;
use app\models\CarrinhoBilheteria;
use app\models\Ocupante;

/**
 * Controle para vendas
 */
class VendaController extends AbstractController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $model = new VendaBilheteria;
        $model->scenario = 'iniciar-pedido';

        return $this->render('index', [
            'model' => $model
        ]);
    }

    public function actionIniciaPedido()
    {        
        $model = new VendaBilheteria;
        $model->scenario = 'iniciar-pedido';
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $model->load($post);
            if($model->validate()) {
                return $model->geraParcelas(1,1);                
            }
            return (object) ['successo'=>'0', 'message'=>'Valor inválido.'];
        }        
    }

    /**
     * Página de venda de ingressos
     */
    public function actionIngressos () 
    {
        $session = $this->_sessionOpen();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
        $agenda = new Agenda($objApiCliente);
        $agenda->listaTodosEventos();
        $datasEventos = $agenda->listaDatasEventos('MATRIZ');
        $mapa = '';
        $cupom = new Cupom($objApiCliente);
        $modelVnB = new VendaBilheteria;
        $modelVnB->scenario = 'iniciar-pedido';
        (new CarrinhoBilheteria($objApiCliente))->limparDaSessao();
        if (Yii::$app->request->post()) {
            $agendaData = explode('_', Yii::$app->request->post('Produto')['id_agenda']);
            $mapaModel = new Mapa();
            $mapa = $mapaModel->getMapa($agendaData[1]);
            $cupom->buscarCupom([
                'id' => 'PUB_' . Yii::$app->request->post('Produto')['id_agenda'] . '_%',
                'ativo' => '1'
            ]);
            
        }
        $apiCaixa = new Caixa();
        $caixa = $apiCaixa->resumoCaixa(['id' => 'caixa_' . Yii::$app->user->identity->cod_operador]);
        $render= isset($mapaModel) && $mapaModel->getAssentos() == 'livre' ? 'ingressos-sn' : 'ingressos';
        return $this->render($render, [
            'model' => new Produto($objApiCliente),
            'modelVnB' => $modelVnB,
            'mapa' => $mapa,
            'eventos' => $datasEventos,
            'id_agenda' => isset(Yii::$app->request->post('Produto')['id_agenda']) ? Yii::$app->request->post('Produto')['id_agenda'] : '',
            'carrinho' => (new CarrinhoBilheteria($objApiCliente))->consultarCarrinho(),
            'cupons' => isset($cupom->lista) && !empty($cupom->lista) ? $cupom->lista : $cupom,
            'caixa' => $caixa
        ]);
    }
    
    /**
     * Montar select de ingresso, usando o widget correspondente
     * @return string
     */
    public function actionSelectIngresso()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
            $session = $this->_sessionOpen();
            $objApiCliente = new Cliente;
            $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
            $post = Yii::$app->request->post();
            $produto = new Produto($objApiCliente);
            $produto->codigo = $post['produto_codigo'];
            $produto->buscarProduto();
            $cupom = new Cupom($objApiCliente);
            $cupom->buscarCupom([
                'id' => 'PUB_' . $post['id_agenda'] . '_%',
                'ativo' => '1'
            ]);
            return (object)['successo' => '1', 'select' => Ingresso::getSelectTipo($produto, $cupom->lista, $post['index'], '', false)];
        }
        return (object)['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros para montar o select de ingresso']];
    }

    public function actionFilipeta()
    {
        try {
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $post=Yii::$app->request->post();
                $compra=isset($post['objeto']['compra']) ? $post['objeto']['compra'] : $post['compra'];
                $filipeta = Filipeta::geraExibicao($compra);
                return (object)['successo'=>'1', 'html'=>$filipeta];
            }
        } catch (Exception $e) {
            return json_encode(['successo'=>'0']);
            $this->render('error', ['name' => 'Erro ao gerar a filipeta', 'message' => $e->getMessage()]);
        }         
    }
    
    /**
     * Adicionar produto ao carrinho de compras - Bilheteria
     * Formato do Post: [produto]: {'codigo': '', 'unidades': ''}
     */
    public function actionAdicionarProdutoCarrinho () 
    {
        try {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                $session = Yii::$app->session;
                $session->open();
                $objApiCliente = new Cliente;
                $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
                $produtos = new Produto($objApiCliente);  
                $produtos->codigo = Yii::$app->request->post()['produto_codigo'];
                $produtos->buscarProduto();
                $session->set('documento_unidade', $produtos->vendedor->documento);
                return (new CarrinhoBilheteria($objApiCliente))->atualizarCarrinho(Yii::$app->request->post());
            }
            return (object)['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros para adicionar produto ao carrinho - bilheteria.']];
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao adicionar produto ao carrinho - bilheteria', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Remover produto do carrinho de compras e da sessão - Bilheteria
     */
    public function actionRemoverProdutoCarrinho () 
    {
        try {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                $session = Yii::$app->session;
                $session->open();
                $objApiCliente = new Cliente;
                $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
                return (new CarrinhoBilheteria($objApiCliente))->atualizarCarrinho([
                    'produto_codigo' => Yii::$app->request->post()['produto_codigo'], 
                    'produto_unidades' => Yii::$app->request->post()['produto_unidades']
                ]);
            }
            return (object)['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros para adicionar produto ao carrinho - bilheteria.']];
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao remover produto do carrinho - bilheteria', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Adicionar cupom ao carrinho de compras - Bilheteria
     * Formato do Post: [cupom]: {'id': '', 'unidades': ''}
     */
    public function actionAdicionarCupomCarrinho () 
    {
        try {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                $session = Yii::$app->session;
                $session->open();
                $objApiCliente = new Cliente;
                $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
                $post = Yii::$app->request->post();
                $return = true;
                $returnApi = (new CarrinhoBilheteria($objApiCliente))->atualizarCarrinho($post, (boolean)$post['assentos']);
                if ($returnApi->successo == '0') {
                    $return = false;
                }
                return $return ? (object)['successo' => '1'] : (object)['successo' => '0', 'erro' => ['mensagem' => 'Falha ao adicionar cupom ao carrinho - bilheteria.']];
            }
            return (object)['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros para adicionar cupom ao carrinho - bilheteria.']];
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao adicionar cupom ao carrinho - bilheteria', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Remover cupom do carrinho de compras - Bilheteria
     */
    public function actionRemoverCupomCarrinho ()
    {
        try {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                $session = Yii::$app->session;
                $session->open();
                $objApiCliente = new Cliente;
                $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
                return (new CarrinhoBilheteria($objApiCliente))->removerCupomCarrinho(Yii::$app->request->post('Ocupante'), (boolean)Yii::$app->request->post()['assentos']);
            }
            return (object)['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros para remover cupom do carrinho - bilheteria.']];
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao remover cupom do carrinho - bilheteria', 'message' => $e->getMessage()]);
        }
    }
   
    /**
     * Registrar cobrança conciliada - Bilheteria
     */
    public function actionCobrancaConciliada()
    {
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                $session = Yii::$app->session;
                $session->open();
                $objApiCliente = new Cliente;
                $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
                $post = Yii::$app->request->post();
                return (new Cobranca($objApiCliente))->cobrancaConciliada($post);
            }
            return (object)['successo' => '0', 'erro' => ['mensagem' => 'Erro no registro de compra conciliada - bilheteria.']];
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Não foi possível registrar compra conciliada - bilheteria.', 'message' => $e->getMessage()]);
        }
    }

    public function actionSalvarCarrinho($tokenOperador)
    {
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                $post = Yii::$app->request->post('Ocupante');
                $ocupantes = new Ocupante($tokenOperador);
                return $ocupantes->salvarEmLote($post);
            }
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao salvar ocupantes', 'message' => $e->getMessage()]);
        }        
    }
    
}
<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
?>

<div class="container">

    <div class="row">

        <input type="hidden" id="url-index" value="<?= Url::to(['site/index']); ?>">
        <div class="col-md-12">
            <table class="tabela-credito">
                <thead>
                    <tr>
                        <th>Produto</th>
                        <th>Valor</th>
                        <th>Quantidade</th>
                        <th>Totais</th>
                    </tr>
                </thead>	
                <tbody>
                    <tr>
                        <td>Tribuna</td>
                        <td>R$ 1800,00</td>
                        <td>
                            <?= MaskedInput::widget([
                                'name' => 'qtde-prod',
                                'id' => 'qtde-prod',
                                'mask' => '99'
                            ]);?>
                        </td>
                        <td><input readonly="readonly" type="text" id="total-prod"></td>
                    </tr>
                    <tr>
                        <td>Embaixada</td>
                        <td>R$ 1000,00</td>
                        <td>
                            <?= MaskedInput::widget([
                                'name' => 'qtde-embaix',
                                'id' => 'qtde-embaix',
                                'mask' => '99'
                            ]);?>
                        </td>
                        <td><input readonly="readonly" type="text" id="total-embaix"></td>
                    </tr>
                    <tr>
                        <td>Camarote</td>
                        <td>R$ 2500,00</td>
                        <td>
                            <?= MaskedInput::widget([
                                'name' => 'qtde-camarot',
                                'id' => 'qtde-camarot',
                                'mask' => '99'
                            ]);?>
                        </td>
                        <td><input readonly="readonly" type="text" id="total-camarot"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>Total Geral:</td>
                        <td>
                            <input readonly="readonly" type="text" id="total-tela">
                            <input readonly="readonly" type="hidden" id="total-geral">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        
        <br>
        <div class="contato" style="width:100%;">
            <div class="boxAceite">
                <p><strong>Termos para aquisição de créditos</strong></p>
                <ul>
                    <li>A aquisição de créditos não garante seu assento, é necessário voltar ao nosso site e fazer sua compra utilizando os mesmos.</li>
                    <li>Se você não utilizar o crédito, poderá requerer a devolução do valor pago.</li>
                    <li>O crédito estará disponível após a compensação bancária, que costuma levar 2 dias úteis.</li>
                </ul>

                <?= Html::beginForm('', 'POST', ['id' => 'form-aceite-boleto', 'class' => 'tabela-credito']) ?>
                <label for="form-aceite-checkbox"><input type="checkbox" name="aceite" id="form-aceite-checkbox" value="1">Entendi e concordo com os termos apresentados</label>
                <input type="hidden" name="total_boleto" id="total_boleto" value="0">
                <button type="submit" class="carrinho seta" id="botContinua">Emitir Boleto</button>
                <?= $msg != '' ? Html::tag('p', $msg) : ''; ?>
                <?= Html::endForm();
                ?>
            </div>
            <span class="botaoVoltar2"><i class="fa fa-arrow-left"></i><a href="<?= Url::to(['pagamento']) ?>">Voltar</a></span>
        </div>
    </div>
</div>

<?php
$session = Yii::$app->session;
$session->open();

$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset', 'yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/carrinho.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset', 'yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset', 'yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/compra.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset', 'yii\bootstrap\BootstrapPluginAsset',]]);
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    $this->registerJs("deadline = setDataExpira('" . $session['tempoCarrinho'] . "'); initializeClock('clockdiv', deadline);");
}
?>
<?php
use yii\widgets\ActiveForm;
use app\components\MenuWidget;
use app\components\TempoCarrinhoWidget;

$session = Yii::$app->session;
$session->open();
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    echo TempoCarrinhoWidget::getBreadCrumb('losang2.png');
}

echo MenuWidget::getUrls();
?>
<div class="container">
	<div class="row">
	    <?php echo MenuWidget::getMenuLateral(); ?>
	    <div class="col-md-10">
	        <div class="tab-content">
				<div class="boxDados">
					<?php $form = ActiveForm::begin(['id' => 'form-cadastro']); ?>
						<div class="row" id="logout">
							<div class="col-md-4">
								Confirma que deseja desconectar de sua conta?<br><br>
								<span class="contato">
									<input type="button" value="Sim" id="btn-logout" class="icon-next carrinho">
								</span>
							</div>
						</div>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/cadastro.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset', 'yii\bootstrap\BootstrapPluginAsset']]);
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    $this->registerJs("deadline = setDataExpira('" . $session['tempoCarrinho'] . "'); initializeClock();");
}
?>
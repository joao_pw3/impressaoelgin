<?php
use \yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\number\NumberControl;
use yii\helpers\Html;
use app\assets\CupomAsset;

CupomAsset::register($this);
$this->title = 'Cupom - Estoque';
$model->tipo = 1;
?>
<div class="container">
    <div class="row">
        <h1><span style="font-weight:200;">Cupom</span> - Estoque</h1>
        <input type="hidden" id="url-movimento" value="<?= Url::to(['movimento-estoque']); ?>">
        <?php $form = ActiveForm::begin(['id' => 'estoque-form']); ?>
        <?= $form->field($model, 'id')->hiddenInput(['value' => Yii::$app->request->get('cupom'), 'id' => 'cupom'])->label(false); ?>
        <div class="col-xs-4">
            <?= $form->field($model, 'estoque')->textInput()->widget(NumberControl::className(), [
                'maskedInputOptions' => [
                    'allowMinus' => true,
                    'autoUnmask'=>true,
                    'unmaskAsNumber'=>true
                ],
                'displayOptions' => [
                    'placeholder' => 'Quantas unidades?',
                    'class' => 'form-control kv-monospace',
                    'id' => 'estoque'
                ]
            ]); ?>
        </div>
        <div class="col-xs-4" align="center">
            <?= $form->field($model, 'tipo')->radioList(['1' => 'Adicionar', '0' => 'Remover'])->label('Ação'); ?>  
        </div>
        <div class="col-xs-4">
            <?= Html::button('Movimentar estoque', ['class' => 'btn btn-primary', 'id' => 'btn-estoque']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <hr>
<?php
if ($estoque->successo == '1' && is_array($estoque->objeto) && count($estoque->objeto) > 0) {
?>
    <div class="row">
        <h4>Histórico de movimentação:</h4>
        <table id="tb-estoque" class="table table-bordered table-striped fixedtable">
            <thead align="center">
                <th>Data</th>
                <th>Unidades</th>
            </thead>
            <tbody>
    <?php 
    $total = 0;
    foreach ($estoque->objeto as $index => $cupom) { 
        $tbClass = ($index % 2) ? 'danger' : '';
        $total += $cupom->unidades;
    ?>
                <tr class="<?= $tbClass; ?>">
                    <td><?= $cupom->data; ?></td>
                    <td align="right"><?= $cupom->unidades; ?></td>
                </tr>
    <?php 
    } 
    ?>
                <tr class="warning">
                    <td>Total:</td>
                    <td align="right"><?= $total; ?></td>
                </tr>                
            </tbody>    
        </table>
    </div>
    <?php
} else if (!is_array($model->lista) || count($model->lista) == 0) { ?>
    <div class="row">
        <h4>Não movimentação de estoque para este cupom.</h4>
    </div>
<?php } ?>
    </div>
</div>
<br><br><br>
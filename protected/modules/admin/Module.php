<?php
namespace app\modules\admin;

use Yii;
use app\models\ApiCliente;
use app\models\Logs;

/**
 * admin module definition class
 * Essa classe deve cuidar das permissões de acesso mais básicas
 * usuários comuns (comprador) não devem acessar nada
 * usuários com permissões especiais:
    * opbilheteria@estadiomineirao.com.br - operador de bilheteria
    * Pode acessar todas as actions da classe BilheteriaController
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\controllers';
    public $logs;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->layout = "mainAdm";
        Yii::$app->user->loginUrl = ['admin/default/login'];
    }
}

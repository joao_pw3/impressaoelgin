<?php
use yii\helpers\Url;
use app\components\TempoCarrinhoWidget;

$session = Yii::$app->session;
$session->open();
?>
<div class="bloco" id="bloco">
    
    <input type="hidden" id="url-add-cart" value="<?= Url::to(['site/add-carrinho']); ?>">
    <input type="hidden" id="url-index" value="<?= Url::to(['site/index']); ?>">
    <input type="hidden" id="url-resumo" value="<?= Url::to(['site/resumo']); ?>">
    <input type="hidden" id="matriz-produtos" value="<?= $evento->tags['apelido']; ?>">
    <input type="hidden" id="area-estadio" value="<?= $this->context->area ?>">
    <input type="hidden" id="url-get-produto" value="<?= Url::to(['site/produto-id']); ?>">
    <input type="hidden" id="url-get-produto-tags" value="<?= Url::to(['site/produto-por-tag']); ?>">
    <input type="hidden" id="url-bloco-assento" value="<?= Url::to(['site/get-bloco-assento']); ?>">
    <input type="hidden" id="url-tempo-carrinho" value="<?= Url::to(['site/tempo-carrinho']); ?>">
	
    <div class="container">
        <?= TempoCarrinhoWidget::getBreadCrumb('losang1.png'); ?>
        <div id="mapaPjax">
            <?= $this->render('blocos'.$this->context->area,[
                    'evento' => $evento ,
                    'setores' => $setores,
                    'compra' => $compra,
                    'carrinho' => $carrinho,
                ]);
            ?>
        </div>
    </div>
</div> 

<div id="infoSetor"></div>

<?php 
$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/carrinho.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    $this->registerJs("deadline = setDataExpira('" . $session['tempoCarrinho'] . "'); initializeClock(deadline);");
}

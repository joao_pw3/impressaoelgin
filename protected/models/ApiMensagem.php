<?php
namespace app\models;
use app\models\Api;
use Yii;

/**
 * Classe para Mensagens - Easy for Pay - Mineirão
 */
class ApiMensagem extends Api
{
    public $logs;

    public $urlComprovante;
    public $urlEsqueci;
    public $urlMineira;
    public $protocolo;
    public $host;
    
    /**
     * Definir url´s padrão, de acordo com os ambientes em uso
     */
    public function __construct() 
    {
        $this->protocolo = YII_ENV_DEV ? 'http://' : 'https://';
        $this->host = YII_ENV_DEV ? ($_SERVER['HTTP_HOST'] == 'localhost' ? 'localhost' : 'easyp.com.br') . '/mineirao/html/' : 'vendas.estadiomineirao.com.br/';
        
        $this->urlComprovante = $this->protocolo . $this->host . 'site/resumo-compra?hash_compra=';
        $this->urlEsqueci = $this->protocolo . $this->host . 'site/trocar-senha?hash_senha=';
        $this->urlMineirao = $this->protocolo . $this->host;
    }

    /**
     * Enviar e-mail para cliente
     * @param array $post Dados para mensagem: assunto, mensagem
     */
    public function enviarEmail($post=[]) 
    {
        $this->setLog('Envio de e-mail');
        $session = Yii::$app->session;
        $session->open();
        if (!empty($post) && isset($session['user_token']) && !empty($session['user_token'])) {
            $this->logs->log('Enviar e-mail', $post);
            $returnApi = json_decode($this->call('email/', 'POST', $post, true, 'U'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao enviar e-mail' : 'E-mail enviado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Envio de e-mail inválido. Formulário vazio ou cliente deslogado.');
        return $this->getApiError('Não foi possível enviar o e-mail.');
    }
    
    /**
     * Enviar SMS para cliente
     * @param array $post Dados para SMS: mensagem
     */
    public function enviarSms($post=[]) 
    {
        $this->setLog('Envio de SMS');
        $session = Yii::$app->session;
        $session->open();
        if (!empty($post) && isset($session['user_token']) && !empty($session['user_token'])) {
            $returnApi = json_decode($this->call('sms_service/' . $session['user_token'] . '/C', 'POST', $post, true, 'A')); //C = tipo Comprador
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao enviar SMS' : 'SMS enviado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Envio de SMS inválido. Formulário vazio ou cliente deslogado.');
        return $this->getApiError('Não foi possível enviar o SMS.');
    }
    
    /**
     * Enviar mensagem de e-mail de compra para o cliente
     * @param object $compra Dados da compra
     */
    public function enviarEmailCompra($compra=[])
    {
        $this->setLog('Envio de e-mail de compra para cliente.');
        $session = Yii::$app->session;
        $session->open();
        if (isset($compra->objeto[0]->vendas->codigo) && (int)$compra->objeto[0]->vendas->codigo > 0 && isset($session['user_id']) && !empty($session['user_id'])) {
            $this->logs->log('Enviar e-mail', $compra);
            $hash = (new ApiCarrinho)->setHashCompra($compra->objeto[0]->vendas->data, $compra->objeto[0]->vendas->comprador->documento, $compra->objeto[0]->vendas->codigo);
            $this->msgClient = 
            "<div style='border:1px solid #aaa; width:100%; max-width:700px; font-family:arial; margin:0px auto;'>
                <img src='" . $this->urlMineirao . "images/topEmail.jpg' width='100%'><br>
                <div style='padding:30px 50px;'>
                    <div style='font-size:30px; color:#aaa; width:100%; text-align:center; font-weight:bold; margin:40px 0px;'>
                        Oi" . (isset($session['nome']) && !empty($session['nome']) ? ', ' . $session['nome'] : '') . ".
                    </div>
                    <div style='font-size:20px; color:#aaa; display:block; width:100%; text-algin:left;'>
                        Você acaba de adquirir produtos Mineirão.<br>Segue o link para acesso aos seus comprovantes:<br><br><br>
                        <a href='" . $this->urlComprovante . $hash . "' target='_blank' style='background:#ddd; padding:5px; -webkit-border-radius:4px; -moz-border-radius:4px; border-radius:4px;  text-decoration:none;'>Clique aqui para vê-los.</a><br><br><br>
                    </div>
                    <div style='font-size:16px; color:#aaa;'>
                        Bom divertimento!<br>
                        Equipe Mineirão.
                    </div><br><br>
                    <div style='font-size:12px; color:#aaa;'>
                        Copyright Minas Arena ".date("Y")." | Todos os direitos reservados <span style='font-size:16px;'>•</span> <a href='http://estadiomineirao.com.br/'>www.estadiomineirao.com.br</a> <span style='font-size:16px;'>•</span> <a href='http://estadiomineirao.com.br/contato/'>Contato</a>
                    </div>
                </div>
            </div>";
            $returnApi = $this->enviarEmailSwift(['vendas@estadiomineirao.com.br' => 'Vendas Mineirão'], [$session['user_id']], 'Atendimento Mineirão', $this->msgClient);
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao enviar e-mail' : 'E-mail enviado com sucesso', $returnApi);
            $returnApi->hash_compra = $hash;
            return $returnApi;
        }    
        $this->logs->log('Envio de e-mail de compra inválido. Parâmetros não informados.');
        return $this->getApiError('Não foi possível enviar e-mail de compra para cliente. Parâmetros não informados.');
    }
    
    /**
     * Enviar um SMS de compra para o cliente
     * @param object $compra Dados da compra
     */
    public function enviarSmsCompra($compra=[]) 
    {
        $this->setLog('Envio de sms de compra para cliente.');
        $session = Yii::$app->session;
        $session->open();
        if (isset($compra->objeto[0]->vendas->codigo) && (int)$compra->objeto[0]->vendas->codigo > 0 && isset($session['user_id']) && !empty($session['user_id'])) {
            $this->logs->log('Enviar sms', $compra);
            $hash = (new ApiCarrinho)->setHashCompra($compra->objeto[0]->vendas->data, $compra->objeto[0]->vendas->comprador->documento, $compra->objeto[0]->vendas->codigo);
            $this->msgClient = 
                "Olá" . (isset($session['nome']) && !empty($session['nome']) ? ', ' . $session['nome'] : '') . ".<br><br>" . 
                "Você acaba de adquirir produtos Mineirão. Segue o link para acesso aos seus comprovantes" . 
                "<a href='" . $this->urlComprovante . $hash . "' target='_blank'>Clique aqui para vê-los.</a><br><br>" . 
                "Bom divertimento!<br><br>Equipe Mineirão.";
            $returnApi = $this->enviarSms(['mensagem' => $this->msgClient]);
            $returnApi->hash_compra = $hash;
            return $returnApi;
        }
        $this->logs->log('Envio de sms de compra inválido. Parâmetros não informados.');
        return $this->getApiError('Não foi possível enviar sms de compra para cliente. Parâmetros não informados.');
    }
    
    /**
     * Enviar mensagem para cliente trocar sua senha
     * @param array $post Array de dados para mensagem: user_id
     */
    public function enviarMensagemEsqueciSenha($post=[])
    {
        $this->setLog('Mensagem de troca de senha');
        if (!empty($post)) {
            $mensagem = 
                "<div style='border:1px solid #aaa; width:100%; max-width:700px; font-family:arial; margin:0px auto;'>
                    <img src='" . $this->urlMineirao . "images/topEmail.jpg' width='100%'><br>
                    <div style='font-size:20px; color:#aaa; text-algin:left; padding:30px 50px;'>
                        <div style='font-size:30px; color:#aaa; width:100%; text-align:center; font-weight:bold; margin:40px 0px;'>
                                Olá
                        </div>
                        Recebemos uma solicitação para a troca de sua senha de acesso ao site de vendas do Mineirão. Caso tenha feito esta solicitação, por favor clique no link abaixo para efetuar a troca de sua senha.<br><br>
                        <a href='" . $this->urlEsqueci . base64_encode('senh@mineir@o|' . $post['user_id'] . '|' . date('YmdHis')) . "' target='_blank' style='background:#ddd; padding:5px; -webkit-border-radius:4px; -moz-border-radius:4px; border-radius:4px;  text-decoration:none;'>clique aqui</a><br><br>
                        Ao clicar neste link, uma mensagem com uma nova senha numérica de 6 dígitos será enviada para este e-mail.<br><br>
                        Atenciosamente!<br><br>
                        Equipe Mineirão.<br><br>
                        <div style='font-size:12px; color:#aaa;'>
                                Copyright Minas Arena ".date("Y")." | Todos os direitos reservados <span style='font-size:16px;'>•</span> <a href='http://estadiomineirao.com.br/'>www.estadiomineirao.com.br</a> <span style='font-size:16px;'>•</span> <a href='http://estadiomineirao.com.br/contato/'>Contato</a>
                        </div>
                    </div>
                </div>";
            $returnApi = $this->enviarEmailSwift(['vendas@estadiomineirao.com.br' => 'Vendas Mineirão'], [$post['user_id']], 'Atendimento Mineirão - Troca de senha', $mensagem);
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao enviar mensagem de troca de senha' : 'Mensagem de troca de senha enviada com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Envio de mensagem de troca de senha inválido. Formulário vazio.');
        return $this->getApiError('Não foi possível enviar mensagem de troca de senha.');
    }    
    
    /**
     * Enviar nova senha para cliente quando ele esquecer
     * @param array $hash_senha Hash de troca de senha, enviada ao cliente por e-mail
     */
    public function enviarNovaSenha($hash_senha='')
    {
        $this->setLog('Envio de senha');
        if (!empty($hash_senha)) {
            header("Content-Type: application/json; charset=utf-8", true);
            $this->logs->log('Enviar senha', $hash_senha);
            $hashArr = explode('|', base64_decode($hash_senha));
            if ($hashArr[0] == 'senh@mineir@o' && isset($hashArr[1]) && !empty($hashArr[1])) {
                $mensagem = [
                    'assunto' => 'Atendimento Mineirão - Troca de senha',
                    'mensagem' => '
                        <div style="border:1px solid #aaa; width:100%; max-width:700px; font-family:arial; margin:0px auto;">
                            <img src="' . $this->urlMineirao . 'images/topEmail.jpg" width="100%"><br>
                            <div style="font-size:20px; color:#aaa; text-algin:left; padding:30px 50px;">
                                <div style="font-size:30px; color:#aaa; width:100%; text-align:center; font-weight:bold; margin:40px 0px;">
                                    Olá
                                </div>
                                <br>Conforme sua solicitação, segue sua nova senha para efetuar compras no site do Mineirão:<br> 
                                <div style="background:#ddd; padding:5px; -webkit-border-radius:4px; -moz-border-radius:4px; border-radius:4px;">
                                    [SENHA]
                                </div><br>
                                Atenciosamente!<br>Equipe Mineirão.<br><br>
                                <div style="font-size:12px; color:#aaa;">
                                    Copyright Minas Arena "' . date("Y") . '" | Todos os direitos reservados <span style="font-size:16px;">•</span> 
                                    <a href="http://estadiomineirao.com.br/">www.estadiomineirao.com.br</a> 
                                    <span style="font-size:16px;">•</span> 
                                    <a href="http://estadiomineirao.com.br/contato/">Contato</a>
                                </div>
                            </div>
                        </div>',
                ];
                $returnApi = json_decode($this->call('novasenha/' . $hashArr[1], 'PUT', $mensagem, true, 'A'));
                $this->logs->log($returnApi->successo === '0' ? 'Falha ao enviar nova senha' : 'Nova senha enviada com sucesso', $returnApi);
                return $returnApi;
            }
            $this->logs->log('Envio de nova senha inválida. Hash inválido.');
            return $this->getApiError('Não foi possível enviar a nova senha. Hash inválido.');
        }
        $this->logs->log('Envio de nova senha inválida. Faltam parâmetros para o envio.');
        return $this->getApiError('Não foi possível enviar a nova senha. Faltam parâmetros para o envio.');
    }    
    
    public function emailSegundaViaMifare ($ingressos)
    {
        if (!empty($ingressos)) {
            $mensagem = " 
                <div style='border:1px solid #aaa; width:100%; font-family:arial; margin:0px auto;'>
                    <div style='font-size:16px; color:#000; text-algin:left; padding:10px 10px;'>
                        Recebemos uma solicitação para fazermos a segunda via de cartão mifare. Seguem os detalhes abaixo:<br><br>
                        <table width='100%' cellpadding='5' cellspacing='5' style='min-width:100%;'>
                            <thead>
                                <th>Qrcode</th>
                                <th>Ocupante</th>
                                <th>Documento</th>
                                <th>Assentos</th>
                                <th>Venda</th>
                                <th>Data venda</th>
                            </thead>
                            <tbody>";
            foreach($ingressos as $index => $ingresso) {
                $mensagem .= "
                            <tr style='background:#" . ($index % 2 == 0 ? 'fff' : 'DCDCDC')  . "'>
                                <td style='padding: 5px;'>" . $ingresso['dados']->objeto[0]->qrcode . "</td>
                                <td style='padding: 5px;'>" . $ingresso['dados']->objeto[0]->ocupante_nome . "</td>
                                <td style='padding: 5px;'>" . $ingresso['dados']->objeto[0]->documento . "</td>
                                <td style='padding: 5px;'>" . $ingresso['dados']->objeto[0]->produto . "</td>
                                <td style='padding: 5px;'>" . $ingresso['dados']->objeto[0]->codigo_venda . "</td>
                                <td style='padding: 5px;'>" . $ingresso['dados']->objeto[0]->data_venda . "</td>
                            </tr>
                ";
            }
            $mensagem .= " 
                            </tbody>
                        </table><br><br>
                        Atenciosamente!<br><br>
                        Equipe Mineirão.<br><br>
                        <div style='font-size:12px; color:#aaa;'>
                            Copyright Minas Arena 2017-" . date("Y") . " | Todos os direitos reservados <span style='font-size:16px;'>•</span> <a href='http://estadiomineirao.com.br/'>www.estadiomineirao.com.br</a> <span style='font-size:16px;'>•</span> <a href='http://estadiomineirao.com.br/contato/'>Contato</a>
                        </div>
                    </div>
                </div>";
            return $this->enviarEmailSwift(
                    ['vendas@estadiomineirao.com.br' => 'Vendas Mineirão'], 
                    [
                        'marco@easyforpay.com.br' => 'Marco', 
                        'eduardo@easyforpay.com.br' => 'Eduardo',
                        'monica@easyforpay.com.br' => 'Mônica',
                        'atendimento@estadiomineirao.com.br' => 'Central de Atendimento - Mineirão',
                        'ti@estadiomineirao.com.br' => 'Tecnologia da Informação',
                        'financeiro.bilheteria@estadiomineirao.com.br' => 'Financeiro - Mineirão'
//                        'felipe.ligorio@estadiomineirao.com.br' => 'Felipe Ligorio',
//                        'marco.ramos@estadiomineirao.com.br' => 'Marco Ramos', 
//                        'otavio.goes@estadiomineirao.com.br' => 'Otavio Goes', 
//                        'willian.fialho@estadiomineirao.com.br' => 'Willian Fialho',
                    ],
                    (YII_ENV_DEV ? '[HOMOLOGAÇãO]' : '[ATENÇãO]') . ' Segunda via - Mifare', 
                    $mensagem
            );
        }
        return $this->getApiError('Não foi possível enviar a mensagem sobre segunda via de cartão mifare.');
    }
    
}
<?php
use yii\helpers\Url;
use app\models\ApiAgenda;
use app\models\ApiProduto;
use yii\helpers\Html;
use app\models\Mineiraocard;
use yii\widgets\ActiveForm;
use app\components\TempoCarrinhoWidget;

$session = Yii::$app->session;
$session->open();

echo TempoCarrinhoWidget::getBreadCrumb('losang3.png');
?>

<div class="resumo" id="resumo">
    <input type="hidden" id="url-index" value="<?= Url::to(['site/index']); ?>">
    <input type="hidden" id="url-visitante" value="<?= Url::to(['site/visitante']); ?>">
    <input type="hidden" id="url-del-cart" value="<?= Url::to(['site/del-carrinho']); ?>">
    <input type="hidden" id="url-email-compra" value="<?= Url::to(['site/email-compra']); ?>">
    <input type="hidden" id="url-cobranca" value="<?= Url::to(['site/enviar-cobranca']); ?>">
    <input type="hidden" id="url-consulta" value="<?= Url::to(['site/consultar-cobranca']); ?>">
    <input type="hidden" id="url-pagamento" value="<?= Url::to(['site/pagamento']); ?>">
    <input type="hidden" id="url-cliente" value="<?= Url::to(['site/cliente']); ?>">
    <input type="hidden" id="url-temporada" value="<?= Url::to(['site/valida-temporada']); ?>">
    <input type="hidden" id="url-tempo-carrinho" value="<?= Url::to(['site/tempo-carrinho']); ?>">
    <input type="hidden" id="id_evento" value="1">
    <input type="hidden" id="id_compra" value="">
    
    <div class="container">

        <?php if (isset($carrinho) && empty($carrinho->objeto->produtos)) { ?>
        <div class="boxMensagem2" style="height:350px; margin-bottom:25px;">
            Seu carrinho de compras está vazio. Acesse a <a href="<?= Url::to(['site/index']); ?>">página de eventos</a> para comprar os ingressos de seu interesse.
        </div>
        <?php } else {
                //echo '<h3>Confirme os lugares e preencha os dados dos ocupantes (campos com * são obrigatórios):</h3>';
				echo '<div class="pintooltip" data-toggle="tooltip" data-placement="right" title="Confira os lugares, preencha os dados dos OCUPANTES, clique em OK e depois em PAGAMENTO para finalizar sua compra."><i class="fa fa-info-circle"></i></div>';
                $precoFinal = 0;
                $total = 0;
                $arrEventos= [];
                foreach ($carrinho->objeto->produtos as $index => $produto) {
                    $apiProduto = new ApiProduto();
                    $produtoValor = $apiProduto->descontoProduto($produto->codigo);
                    $precoFinal += $produtoValor * $produto->unidades;
                    $total += $produto->unidades;
                    $agenda = new ApiAgenda();
                    $bloco = '';
                    $area = '';
                    $banner = '';
                    $evento;
                   if(isset($produto->tags)){
                        foreach ($produto->tags as $index_tag => $tag) {
                            if ($tag->nome == 'area') {
                                switch($tag->valor){
                                    case 'Leste':
                                        $area='Vermelha';
                                        break;
                                    case 'Oeste':
                                        $area='Roxa';
                                        break;
                                }
                            }
                            if ($tag->nome == 'matriz') {
                                $evento = $agenda->buscarEvento(json_encode(['tags'=>[['nome'=>'apelido','valor'=>$tag->valor]]]));
                            }
                        }
                    }
                    foreach ($evento->objeto[0]->anexos as $index_anexo => $anexo) {
                        if (isset($anexo->nome) && $anexo->nome == 'bannervitrine') {
                            $banner = $anexo->url;
                        }                  
                    }

                    $mineiraocardNome='';
                    $mineiraocardDoc='';
                    $mineiraocardTel='';
                    $mineiraocardEmail='';
                    $mineiraocardValido=false;
                    
                    if(isset($carrinho->objeto->tags)){
                        foreach($carrinho->objeto->tags as $tag){
                            $nome=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_nome');
                            if($nome!=false)
                                $mineiraocardNome=$nome;
                            $doc=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_documento');
                            if($doc!=false)
                                $mineiraocardDoc=$doc;
                            $tel=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_telefone');
                            if($tel!=false)
                                $mineiraocardTel=$tel;
                            $email=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_email');
                            if($email!=false)
                                $mineiraocardEmail=$email;
                        }
                        $mineiraocardValido=$mineiraocardNome!=false&&$mineiraocardDoc!=false;
                    }

        ?>
        <div class="row about-left wow fadeInLeft animated div-<?= $produto->codigo; ?>" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
            <div class="col-md-12 col-xs-12 tarjaCz">
                <?php //if (!empty($evento['datahora'])) { ?>
                <!-- dia --> <?php //date('d/m', strtotime($evento['datahora'])) . ' às ' . date('H:m', strtotime($evento['datahora'])) . ' - '; ?>
                <?php //} ?>
                <?php echo $evento->objeto[0]->nome; ?>
				<!-- <div style="float:right;" data-toggle="tooltip" title="Confirme os lugares e preencha os dados dos ocupantes e seguida clique em OK"><i class="fa fa-info-circle"></i></div> -->
            </div>
        </div>
        <br>
        <div class="row">           
            <div class="col-md-12 zeraesp">
				<div class="tabProds">
					<div>
						<label>Itens</label>
						<p><?= number_format($produto->unidades, 0); ?></p>
					</div>
					<div>
						<label>Área <?= $area; ?></label>
						<?php echo Html::img($banner, ["class" => "imgBanner"]); ?>
					</div>
					<div>
						<label>Produto</label>
						<p><?= $produto->nome; ?></p>
					</div>
					<div>
						<label>Subtotal</label>
						<p>
							<?php if($produtoValor == $produto->valor) {
								echo 'R$ '.number_format($produtoValor * $produto->unidades, 2, ',', '.'); 
							} else {
								echo '<span class="strikeDesconto">R$ '.number_format($produto->valor * $produto->unidades, 2, ',', '.').'</span><br> '; 
								echo ' R$ '.number_format($produtoValor * $produto->unidades, 2, ',', '.'); 
							}?>
						</p>
					</div>
					<!-- <div class="riscoLateral"></div> -->
					<?php $form = ActiveForm::begin([
						'id' => 'formMineiraocard-' . $produto->codigo,
						'options'=>	['class' => 'pull-right form-mineiraocard-ocupantes'],
						'action' => Url::to(['site/info-mineirao-card'])
					]); ?>
						<input type="hidden" name="Mineiraocard[produto]" value="<?=$produto->codigo?>">
						<input type="hidden" id="mineiraocard-valido-<?= $produto->codigo ?>" class="Mineiraocard_valido" value="<?=(string)$mineiraocardValido?>">
					<div>						
						<label>Nome do ocupante*<label />
                       <p><input name="Mineiraocard[nome]" value="<?=$mineiraocardNome?>" type="text" placeholder="Nome"></p>
					</div>
					<div>
						<label>RG ou CPF do ocupante*</label>
						<p><input name="Mineiraocard[documento]" value="<?=$mineiraocardDoc?>" type="text" placeholder="RG ou CPF" id="doc-<?=$produto->codigo?>"></p>
					</div>
					<div>
						<label>E-mail do ocupante</label>
						<p><input name="Mineiraocard[email]" value="<?=$mineiraocardEmail?>" type="text" placeholder="E-mail do ocupante" id="doc-<?=$produto->codigo?>"></p>
					</div>
					<div>
						<label>Telefone do ocupante</label>
                        <p><input name="Mineiraocard[telefone]" value="<?=$mineiraocardTel?>" type="text" placeholder="Telefone"></p>
					</div>
					<div class="cellBots">
						<button type="button" id="mineiraocard-submit-<?= $produto->codigo ?>" class="btn btn-sm btn-primary mineiraocard-submit">OK</button>
					</div>
					<div class="cellBots">
						<a target="_top" href="javascript:;" id="trash-<?= $produto->codigo; ?>" data-trash="<?= $produto->codigo; ?>" class="icon icon-trash corCz" title="Remover do carrinho"></a>
					</div>
					<p class="mineiraocard-info mineiraocard-retorno"></p>
					<?php ActiveForm::end(); ?>
				</div>

                    <!--<div class="col-md-3 col-xs-3 mineiraocard-info mineiraocard-info-nome"></div>
                    <div class="col-md-3 col-xs-3 mineiraocard-info mineiraocard-info-documento"></div>
                    <div class="col-md-2 col-xs-2 mineiraocard-info mineiraocard-info-telefone"></div>
                    <div class="col-md-1 col-xs-1 mineiraocard-info"></div> 
                    <p class="clearfix"></p>-->
                
            </div>	
	</div>
		
        <?php } ?>
        <br clear="all">
        
        <div class="row contato">
            <div class="col-md-3 col-xs-6">
                <a class="carrinho seta" href="<?= Url::to(['site/index']); ?>" style="display:block;">Continuar comprando</a>
            </div>
            <?php if (isset($cartao->ultimos_digitos) || $pgtoConciliacao) { ?>
            <div class="col-md-9  col-xs-6 text-right carrDest" align="right">
                <input type="button" value="Pagamento" id="ir-para-pagamento" class="icon-next carrinho ">
            </div>
            <?php } else { ?>
            <div class="col-md-9  col-xs-6 text-right" align="right">
                <p>Por favor, <a href="<?= Url::to(['site/cliente', '#' => 'tab-pagamentos']); ?>">clique aqui</a>, escolha seu cartão favorito ou<br>cadastre um novo cartão.</p>
            </div>
           <?php } ?>
        </div>        
        <?php } ?>
    </div>	
</div>

<?php 
if($session['user_token'] && isset($cliente->objeto->user_id)) 
{
    $this->registerJs("
        var customer = '".$cliente->objeto->user_id."'
        kondutoCustomer(customer);
    ", \yii\web\View::POS_END);    
}

$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/carrinho.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/compra.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
if(Yii::$app->getSession()->hasFlash('info-campos')){
    $this->registerJs("
        $('#modal-mensagem .modal-body').html('".Yii::$app->getSession()->getFlash('info-campos')."');
        $('#modal-mensagem').modal('toggle');
    ");
}
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    $this->registerJs("deadline = setDataExpira('" . $session['tempoCarrinho'] . "'); initializeClock('clockdiv', deadline);");
}
?>
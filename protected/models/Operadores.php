<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\WsOperadores;

/**
 * Esse é o formulário para cadastro inicial de uma conta de usuário
 */
class Operadores extends Model
{
    public $codigo;
    public $email;
    public $nome;
    public $senha;
    public $mensagem;
    public $ativo;
    public $unidade;
    public $lista;

    /**
     * Método construtor sempre pede a unidade atual
    */
    public function __construct(Cliente $unidade){
        $this->unidade=$unidade;
    }
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email','nome','senha'], 'required', 'on'=>'default'],
            [['email','nome'], 'required', 'on'=>'update'],
            [['mensagem'],'safe'],
            [['email'], 'email'],
            [['email','nome'], 'string', 'max' => 64],
            [['senha'], 'string', 'max' => 14],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Endereço de e-mail',
            'nome' => 'Nome',
            'senha' => 'Senha',
            'mensagem' => 'Mensagem',
        ];
    }

    /**
     * Salvar um registro de operador
     */
    public function save(){
        if($this->validate()){
            $apiModel=new WsOperadores($this->unidade->user_token);
            $cadastro=$apiModel->cadastrarOperador([
                'nome'=>$this->nome,
                'email'=>$this->email,
                'senha'=>$this->senha,
                'mensagem'=>$this->mensagem,
            ]);
            if($cadastro->successo){
                $operadores=$apiModel->listaOperadores();
                if(!$operadores->successo)
                    $this->addError('email',$operadores->erro->mensagem);
                else{
                    foreach ($operadores->objeto as $operador) {
                        if($operador->email==$this->email)
                            $this->codigo=$operador->codigo;
                    }
                    return true;
                }
            }else
                switch ($cadastro->erro->codigo) {
                    case '029':
                        $attribute='nome';
                        break;
                    case '170':
                    default:
                        $attribute='email';
                        break;
                    
                }
                $this->addError($attribute,$cadastro->erro->mensagem);
        }
        return false;
    }

    /**
     * Atualizar um registro de operador
     */
    public function update(){
        if($this->validate()){
            $apiModel=new WsOperadores($this->unidade->user_token);
            $update=$apiModel->alterarOperador([
                'nome'=>$this->nome,
                'email'=>$this->email,
                'senha'=>$this->senha,
                'mensagem'=>$this->mensagem,
            ]);
            if($update->successo){
                $operadores=$apiModel->listaOperadores();
                if(!$operadores->successo)
                    $this->addError('email',$operadores->erro->mensagem);
                else{
                    foreach ($operadores->objeto as $operador) {
                        if($operador->email==$this->email)
                            $this->codigo=$operador->codigo;
                    }
                    return true;
                }
            }else
                $this->addError('email',$update->erro->mensagem);
        }
        return false;
    }

    /**
     * Lista de operadores
     * @return object {'successo': '1', 'objeto': '...'}
     */
    public function listaOperadores()
    {
        $lista=(new WsOperadores($this->unidade->user_token))->listaOperadores();
        if($lista->successo)
            return $lista->objeto;
    }

    /**
     * Mostrar info de um operador
     * Por enquanto, a única forma de mostrar é selecionando todos e isolando o código requisiado
     */
    public function dadosOperador(){
        $apiModel=new WsOperadores($this->unidade->user_token);
        $operadores=$apiModel->listaOperadores();
        if($operadores->successo){
            foreach ($operadores->objeto as $operador) {
                if($operador->codigo==$this->codigo){
                    $this->loadFromApi($operador);
                }
            }
        }
    }

    /**
     * Excluir operador
     */
    public function excluirOperador(){
        return (new WsOperadores($this->unidade->user_token))->excluirOperador($this->email);
    }

    /**
     * Atribuir valores retornado pela API às propriedades
     */
    public function loadFromApi($objeto) {
        if (!isset($objeto->codigo, $objeto->email))
            return false;
        $this->codigo = $objeto->codigo;
        $this->email = $objeto->email;
        $this->nome = $objeto->nome;
        $this->ativo = $objeto->ativo;
        return true;
    }

    public static function getOperador($token){
        $ws=new WsOperadores($token);
        $ws->setToken($token,'U');
        return $ws->getOperador();
    }
}
<?php
use \kartik\date\DatePicker;
use \app\models\ApiProduto;

$this->title = 'ADM Estádio Mineirão :: Relatório de ocupantes';
?>
<div class="container">
    <div class="row">
        <h2 class="tituloGeral"><i class="fa fa-clone"></i> Relatório de ocupantes</h2>
    </div>
	<h3>Busca por data</h3>
	<?= \yii\helpers\Html::beginForm('','post',['class'=>'form-inline'])?>
		<div class="form-group">
			<label>Data inicial:</label>
			<?= DatePicker::widget([
			    'name'  => 'de',
			    'value'  => $de,
			    'language' => 'pt-BR',
			    'pluginOptions'=>[
			    	'format' => 'dd/mm/yyyy'
			    ]
			]);?>
		</div>
		<div class="form-group">
			<label>Data final:</label>
			<?= DatePicker::widget([
			    'name'  => 'ate',
			    'value'  =>$ate,
			    'language' => 'pt-BR',
			    'pluginOptions'=>[
			    	'format' => 'dd/mm/yyyy'
			    ]
			]);?>
		</div>
		<div class="form-group">
			<input type="hidden" name="consulta" value="vendas">
			<button class="btn btn-default" type="submit">Buscar</button>
		</div>
	<?= \yii\helpers\Html::endForm()?>

	<h3>Busca por assento</h3>
	<?= \yii\helpers\Html::beginForm('','post',['class'=>'form-inline'])?>
        <div class="form-group">
            <label>Evento:</label>
            <select name="matriz">
                <?php foreach ($listaEventos as $evento) {
                    if (isset($evento->tags['apelido']) && $evento->tags['apelido'] == 'mifare2018')
                        continue;
                    ?>
                <option value="<?= isset($evento->tags['apelido']) ? $evento->tags['apelido'] : ''; ?>"><?= $evento->nome ?></option>
                <?php } ?>
            </select>
        </div>
		<div class="form-group">
			<label>Bloco:</label>
			<input name="bloco" />
		</div>
		<div class="form-group">
			<label>Fileira:</label>	
			<input name="fileira" />			
		</div>
		<div class="form-group">
			<label>Cadeira:</label>
			<input name="cadeira" />				
		</div>
		
		<div class="form-group">
			<input type="hidden" name="consulta" value="produto">
			<button class="btn btn-default" type="submit">Buscar</button>
		</div>
	<?= \yii\helpers\Html::endForm()?>

	<h3>Busca por ocupante ou comprador</h3>
	<?= \yii\helpers\Html::beginForm('','post',['class'=>'form-inline'])?>
		<div class="form-group">
			<label>Nome:</label>
			<input name="%_nome" />
		</div>
		<div class="form-group">
			<label>Documento:</label>	
			<input name="%_documento" />			
		</div>
		
		<div class="form-group">
			<input type="hidden" name="consulta" value="ocupante">
			<button class="btn btn-default" type="submit">Buscar</button>
		</div>
	<?= \yii\helpers\Html::endForm()?>

	<?php 
	if($consulta->successo=='1' && count($consulta->objeto)>0){
		if($tipoConsulta=='vendas') echo $this->render('pesquisa-ocupantes-vendas',['produtos'=>$consulta]);
		else{
		?>
			<table class="uppercase table table-responsive table-striped">
				<thead>
					<tr>
						<th>Data da venda</th>
						<th>Código da venda</th>
						<th>Bloco</th>
						<th>Fileira</th>
						<th>Cadeira</th>
						<th>Nome do ocupante</th>
						<th>Documento</th>
						<th>Telefone</th>
						<th>Email</th>
						<th>Nome do comprador</th>
						<th>Documento do comprador</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($consulta->objeto as $o){?>
						<?php foreach($o->produtos as $p){
							$tagsProduto=ApiProduto::tagsArray($p->tags);
							$produto=explode(' ', $p->nome);
							$fileira=preg_replace('/\d/', '', $produto[1]);
							$assento=preg_replace('/\D/', '', $produto[1]);
							$bloco=$produto[3];
							$nomeOcupante = isset($tagsProduto[$p->codigo.'_nome']) ?$tagsProduto[$p->codigo.'_nome'] :'';
							$docOcupante = isset($tagsProduto[$p->codigo.'_documento']) ?$tagsProduto[$p->codigo.'_documento'] :'';
							$telOcupante = isset($tagsProduto[$p->codigo.'_telefone']) ?$tagsProduto[$p->codigo.'_telefone'] :'';
							$emailOcupante = isset($tagsProduto[$p->codigo.'_email']) ?$tagsProduto[$p->codigo.'_email'] :'';
							?>
							<tr>
								<td><?=$o->compra->data?></td>
								<td><?=$o->compra->codigo?></td>
								<td><?=$bloco?></td>
								<td><?=$fileira?></td>
								<td><?=$assento?></td>
								<td><?=$nomeOcupante?></td>
								<td><?=$docOcupante?></td>
								<td><?=$telOcupante?></td>
								<td><?=$emailOcupante?></td>
								<td><?=$o->comprador->nome?></td>
								<td><?=$o->comprador->documento?></td>
							</tr>
						<?php }?>
					<?php }?>
					<?php if(isset($consultaOcupante) && $consultaOcupante->successo){
						foreach($consultaOcupante->objeto as $o){?>
							<?php foreach($o->produtos as $p){
								$tagsProduto=ApiProduto::tagsArray($p->tags);
								$produto=explode(' ', $p->nome);
								$fileira=preg_replace('/\d/', '', $produto[1]);
								$assento=preg_replace('/\D/', '', $produto[1]);
								$bloco=$produto[3];
								$nomeOcupante = isset($tagsProduto[$p->codigo.'_nome']) ?$tagsProduto[$p->codigo.'_nome'] :'';
								$docOcupante = isset($tagsProduto[$p->codigo.'_documento']) ?$tagsProduto[$p->codigo.'_documento'] :'';
								$telOcupante = isset($tagsProduto[$p->codigo.'_telefone']) ?$tagsProduto[$p->codigo.'_telefone'] :'';
								$emailOcupante = isset($tagsProduto[$p->codigo.'_email']) ?$tagsProduto[$p->codigo.'_email'] :'';
								?>
								<tr>
									<td><?=$o->compra->data?></td>
									<td><?=$o->compra->codigo?></td>
									<td><?=$bloco?></td>
									<td><?=$fileira?></td>
									<td><?=$assento?></td>
									<td><?=$nomeOcupante?></td>
									<td><?=$docOcupante?></td>
									<td><?=$telOcupante?></td>
									<td><?=$emailOcupante?></td>
									<td><?=$o->comprador->nome?></td>
									<td><?=$o->comprador->documento?></td>
								</tr>
							<?php }?>
						<?php }?>
					<?php }?>
				</tbody>
			</table>
	<?php }
	} else { ?>
		<p>Nenhuma venda encontrada neste período ou produto</p>
	<?php } ?>
</div>
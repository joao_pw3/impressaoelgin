<?php
use yii\jui\Slider;
use yii\helpers\Url;
use \app\assets\MapaAsset;
use app\assets\EventoAsset;
use app\models\Tags;
use app\models\Mapa;

MapaAsset::register($this);
EventoAsset::register($this);
?>

<div class="deslizante" title="Zoom">
    <?= Slider::widget([
        'id' => 'zoomMapa',
        'clientOptions' => [
            'min' => 1,
            'max' => 200,
            'range' => 'min',
            'orientation' => 'vertical'
        ],
        'clientEvents' => [
            'create' => 'function() {
                zoomMapaReset();
                zoomMapaInit();
            }'
        ],
    ]);?>
</div>

<div class="col-md-9 semPad semMargin">
    <div class="blocoIngr1 mapa-selecao" id="holder-mapa" style="margin-top:0px;">
        <div class="div-loading">
            <div id="img-loading"></div>
        </div>
        <?= (new Mapa)->getMapa($evento->primeiroHorario); ?>
    </div>
    <div class="legendaMapa">
        <span id="circle1"></span> Livre
        <span id="circle2"></span> Reservado
        <span id="circle3"></span> Indisponível
        <span id="circle4"><p style="position:absolute; top:-3px; left:1px;">&#xf007;</p><p style="position:absolute; top:-3px; left:7px;">&#xf067;</p></span> Obeso
        <span id="circle5"><p style="position:absolute; top:-3px; left:3px;">&#xf193;</p></span> Cadeirante
        <span id="circle6"><p style="position:absolute; top:-3px; left:3px;">&#xf29d;</p></span> Mob.Reduzida		
    </div>
</div>

<div class="col-md-3 semPad semMargin">
    <input type="hidden" id="url-add-produto" value="<?= Url::to(['compra/adicionar-produto-carrinho']); ?>">
    <input type="hidden" id="url-del-produto" value="<?= Url::to(['compra/remover-produto-carrinho']); ?>">
    <input type="hidden" id="url-ocupantes" value="<?= Url::to(['compra/ingresso']); ?>">
    <input type="hidden" id="evento" value="<?= $_GET['evento']; ?>">
    <input type="hidden" id="data" value="<?= $evento->datas[0]->id; ?>">
    <span id="setaEsq"></span>
    <div class="blocoIngr2 font-assentos row" style="margin-top:0px;">
        <h2>Assentos selecionados</h2>
        <hr style="margin-top:10px; margin-bottom:10px; border-bottom: groove 2px #fff!important;">
        <div class="caixa-assentos barScroll">
            <form id="form-selecionados">
                <div id="assentos-selecionados" align="center">
                    <?php 
                    if (isset($carrinho->objeto->produtos) && !empty($carrinho->objeto->produtos)) {
                        $tags = new Tags();
                        foreach($carrinho->objeto->produtos as $index => $produto) {
                            if(!isset($produto->tags)) continue;
                            $tagsTmp = $tags->objetoTags($produto->tags);
                    ?>
                    <div id="div-<?= $produto->codigo; ?>" class="col-xs-3 selecionado idData-<?=$produto->agenda->id_data_agenda?>">
                        <div class="div-circle">
                            <?= $tagsTmp->fileira . '-' . $tagsTmp->assento; ?>
                        </div>
                    </div>
                    <?php
                        }
                    } 
                    ?>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-sm-6 totProd" id="total-itens-carrinho">
                <i class="material-icons text-left" title="Carrinho de compras" style="font-size:14px;">shopping_cart</i>
                <span class="itensNoCarrinho"></span> 
            </div> 
            <div class="col-sm-6">
                <input type="button" id="btn-assentos" class="btn btn-primary btn-assentos" value="Continuar" disabled>
            </div> 
		</div>
    </div>
</div>
<br clear="all">
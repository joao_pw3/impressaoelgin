<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\ApiOperadores;

/**
 * Esse é o formulário para cadastro inicial de uma conta de usuário
 */
class CadastroInicialForm extends Model
{
    public $codigo;
    public $email;
    public $nome;
    public $senha;
    public $mensagem;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email','nome','senha'], 'required', 'on'=>'default'],
            [['email','nome'], 'required', 'on'=>'update'],
            [['mensagem'],'safe'],
            [['email'], 'email'],
            [['email','nome'], 'string', 'max' => 64],
            [['senha'], 'string', 'max' => 14],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Endereço de e-mail',
            'nome' => 'Nome',
            'senha' => 'Senha',
            'mensagem' => 'Mensagem',
        ];
    }

    /**
     * Salvar um registro de operador
     */
    public function save(){
        if($this->validate()){
            $apiModel=new ApiOperadores;
            $cadastro=$apiModel->cadastrarOperador([
                'nome'=>$this->nome,
                'email'=>$this->email,
                'senha'=>$this->senha,
                'mensagem'=>$this->mensagem,
            ]);
            if($cadastro->successo){
                $operadores=$apiModel->listaOperadores();
                if(!$operadores->successo)
                    $this->addError('email',$operadores->erro->mensagem);
                else{
                    foreach ($operadores->objeto as $operador) {
                        if($operador->email==$this->email)
                            $this->codigo=$operador->codigo;
                    }
                    return true;
                }
            }else
                switch ($cadastro->erro->codigo) {
                    case '029':
                        $attribute='nome';
                        break;
                    case '170':
                    default:
                        $attribute='email';
                        break;
                    
                }
                $this->addError($attribute,$cadastro->erro->mensagem);
        }
        return false;
    }

    /**
     * Atualizar um registro de operador
     */
    public function update(){
        if($this->validate()){
            $apiModel=new ApiOperadores;
            $update=$apiModel->alterarOperador([
                'nome'=>$this->nome,
                'email'=>$this->email,
                'senha'=>$this->senha,
                'mensagem'=>$this->mensagem,
            ]);
            if($update->successo){
                $operadores=$apiModel->listaOperadores();
                if(!$operadores->successo)
                    $this->addError('email',$operadores->erro->mensagem);
                else{
                    foreach ($operadores->objeto as $operador) {
                        if($operador->email==$this->email)
                            $this->codigo=$operador->codigo;
                    }
                    return true;
                }
            }else
                $this->addError('email',$update->erro->mensagem);
        }
        return false;
    }

    /**
     * Lista de operadores
     * @return object {'successo': '1', 'objeto': '...'}
     */
    public function listaOperadores()
    {
        $lista=(new ApiOperadores)->listaOperadores();
        if($lista->successo)
            return $lista->objeto;
    }

    /**
     * Mostrar info de um operador
     * Por enquanto, a única forma de mostrar é selecionando todos e isolando o código requisiado
     */
    public function dadosOperador(){
        $apiModel=new ApiOperadores;
        $operadores=$apiModel->listaOperadores();
        if($operadores->successo){
            foreach ($operadores->objeto as $operador) {
                if($operador->codigo==$this->codigo){
                    $this->nome=$operador->nome;
                    $this->email=$operador->email;
                    //$this->senha=$operador->senha;
                    //$this->mensagem=$operador->mensagem;
                }
            }
        }
    }

    /**
     * Excluir operador
     */
    public function excluirOperador(){
        return (new ApiOperadores)->excluirOperador($this->email);
    }
}

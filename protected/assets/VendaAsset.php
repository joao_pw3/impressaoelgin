<?php
namespace app\assets;

use yii\web\AssetBundle;

class VendaAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/venda.css'
    ];
    public $js = [
        'js/ajax.js',
        'js/venda.js',
        'js/venda-sn.js',
        'js/print.js',
        'js/validar-entrada.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    public $publishOptions = [
        'forceCopy' => true,
    ];
}

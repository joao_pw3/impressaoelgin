<?php
namespace app\models;

/**
 * This is the model class for table "pessoa".
 *
 * @property integer $id
 * @property string $pedido
 * @property string $autorizacao
 * @property string $nome
 * @property string $clube
 * @property integer $cpf
 * @property integer $qtdcompraporcpf
 * @property string $telefone
 * @property string $email
 * @property string $endereco
 * @property string $setor
 * @property string $pacote
 * @property string $bloco
 * @property string $fileira
 * @property string $assento
 * @property string $datadacompra
 * @property integer $parcelas
 * @property integer $formadepagamento
 * @property integer $canaldevenda
 * @property string $codproduto
 * @property string $produtobase64
 * @property string $cpfbase64
 * @property boolean $compra_efetuada
 * @property integer $id_compra
 */
class Renovacao extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'renovacao';
    }
    
}

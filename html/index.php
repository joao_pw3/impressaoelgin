<?php
setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');

/*
if(file_exists(realpath('offline.php')) && (strpos($_SERVER['REQUEST_URI'], 'site') !== false || strpos($_SERVER['REQUEST_URI'], 'admin') === false))
{
	header("Location: offline.php", true, 307);		
} 
*/

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../protected/vendor/autoload.php');
require(__DIR__ . '/../protected/vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../protected/config/web.php');

(new yii\web\Application($config))->run();
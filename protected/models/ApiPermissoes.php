<?php
namespace app\models;
use app\models\Api;

/**
 * Classe para Permissões de usuários - Easy for Pay - Mineirão
 */
class ApiPermissoes extends Api
{
    public $logs;
    
    /**
     * Incluir permissão:
     * @param $idUsuario email do usuário a ser atribuída a permissão
     * @param $controleAcao controller e action para permitir o acesso, separado por barra ex.: vendas/index
     * @return object {'successo': '1'}
     */
    public function incluirPermissao($idUsuario,$controleAcao)
    {
        $returnApi = json_decode($this->call('permissoes/'.$idUsuario, 'POST', ['permissao'=>$controleAcao], true, 'A'));
        $this->setLog('Incluir permissão');
        if($returnApi->successo==0)
            $this->logs->log('Falha ao registrar a permissão '.$controleAcao.' ao usuário '.$idUsuario);
        else
            $this->logs->log('Permissão de '.$controleAcao.' concedida ao usuário '.$idUsuario);
        return $returnApi; 
    }
    
    /**
     * Consultar permissões trazendo todas de um usuário
     * @param $idUsuario email do usuário a ser atribuída a permissão
     * @return object {'successo': '1', 'objeto': '...'}
     */
    public function consultarPermissoes($idUsuario)
    {
        $returnApi = json_decode($this->call('permissoes/'.$idUsuario, 'GET', null, true, 'A'));
        if($returnApi->successo==0){
            $this->setLog('Falha ao buscar permissões do usuário');
            $this->logs->log('usuário '.$idUsuario);
        }        
        return $returnApi;        
    }
    
    /**
     * Ativar / desativar permissão. Muda o estado da permissão. Se inativo=0 a permissão é válida, senão (inativo=1) a permissão não é válida
     * @param $idUsuario email do usuário a ser ativada ou desativada a permissão
     * @param $controleAcao controller e action separado por barra ex.: vendas/index
     * @param $inativo 1 para desativar ou 0 para ativar uma permissão
     * @return object {'successo': '1'}
     */
    public function ativarDesativarPermissao($idUsuario, $controleAcao, $inativo)
    {
        $returnApi = json_decode($this->call('permissoes/'.$idUsuario, 'PUT', ['permissao'=>$controleAcao,'inativo'=>$inativo], true, 'A'));
        if($returnApi->successo==0){
            $this->setLog('Falha ao revogar permissões');
            $this->logs->log('usuário '.$idUsuario);
        }        
        return $returnApi;
    }
    
}
<?php
namespace app\models;
use app\models\Ws;

/**
 * Classe para Agenda - Easy for Pay
 * obs.: parâmetros entre colchetes são opcionais
 */
class WsAgenda extends Ws
{    
    
    /**
     * Método construtor - obrigado a passar o token do vendedor para separação de unidades
    */
    public function __construct($tokenVendedor=''){
        if(empty($tokenVendedor))
            throw new \Exception("Token vendedor não foi declarado", 1);        
        $this->setToken($tokenVendedor,'V');
    }
    
    /**
     * Criar evento na agenda
     * @param type $post Dados do evento: nome, [descricao], data (inicial, final, [ativa]), ativo
     * @return object {'successo': '1', 'codigo': <id do evento criado>}
     */
    public function criarEvento($post)
    {
        return json_decode($this->call('agenda/', 'POST', $post));
    }
    
    /**
     * Alterar evento
     * @param type $post Dados do evento: [nome], [descricao], [data (inicial, final, [ativa])], [ativo]
     * @return object {'successo': '1', 'alterado': '1'}
     */
    public function alterarEvento($post)
    {
        return json_decode($this->call('agenda/' . $post['codigo'], 'PUT', $post));
    }
    
    /**
     * Excluir evento
     * @param type $post Dados do evento: codigo
     * @return object {'successo': '1', 'removido': '1'}
     */
    public function excluirEvento($post)
    {
        return json_decode($this->call('agenda/' . $post['codigo'], 'DELETE'));
    }
    
    /**
     * Listar eventos
     * @param json $post Parâmetros para busca
     * @return object {'successo': '1', 'eventos': {...}}
     */
    public function listarEventos($post=null)
    {
        return json_decode($this->call('agenda/buscar/', 'PUT',  $post));
    }
    
    /**
     * Buscar um evento
     * @param integer $id ID de um evento
     * @return object {'successo': '1', 'eventos': {...}}
     */
    public function buscarEvento($id)
    {
        return json_decode($this->call('agenda/buscar/'.$id,'PUT',[]));
    }


    public function buscarEventoRede($id,$post)
    {
        return json_decode($this->call('agenda/buscar/'.$id,'PUT',$post));
    }

     /**
     * Adicionar tag para evento
     * @param type $post Dados da tag: nome, [valor]
     * @return object
     */
    public function adicionarTag($post)
    {
        return json_decode($this->call('agenda/' . $post['codigo'] . '/tag', 'POST',  $post));
    }

    /**
     * Excluir tag para evento
     * @param type $post Dados da tag: nome
     * @return object
     */
    public function excluirTag($post)
    {
        return json_decode($this->call('agenda/' . $post['codigo'] . '/tag', 'DELETE', $post));
    }
    
    /**
     * Adicionar anexo para evento
     * @param type $post Dados de anexo: nome, tipo, objeto
     * @return object
     */
    public function adicionarAnexo($post)
    {
        return json_decode($this->call('agenda/' . $post['codigo'] . '/anexo', 'POST', $post));
    }

    /**
     * Excluir anexo para evento
     * @param type $post Dados de anexo: nome
     * @return object
     */
    public function excluirAnexo($post)
    {
        return json_decode($this->call('agenda/' . $post['codigo'] . '/anexo', 'DELETE', $post));        
    }    

    /**
     * Adiciona uma nova data ao evento
     * @param json $post objeto a ser inserido - {codigo:[int],data:[DD/MM/AAAATHH:MM:SS]}
     * @return object
     */
    public function adicionarData($post){
        return json_decode($this->call('agenda/' . $post['codigo'] . '/data', 'POST', $post));
    }   

    /**
     * Excluir data de um evento
     * @param json $post objeto com código do evento e a data a ser removida - {codigo:[int],data:[DD/MM/AAAATHH:MM:SS]}
     * @return object
     */
    public function excluirData($post){
        return json_decode($this->call('agenda/' . $post['codigo'] . '/data', 'DELETE', $post));
    }
    
    /**
     * Clonar anexos de um evento origem para um evento destino
     * @param type $post
     */
    public function clonarAnexos($post)
    {
        return json_decode($this->call('agenda/clonaranexos/' . $post['de'] . '/' . $post['para'], 'PUT'));
    }

    /**
     * Clonar tags de um evento origem para um evento destino
     * @param type $post
     */
    public function clonarTags($post)
    {
        return json_decode($this->call('agenda/clonartags/' . $post['de'] . '/' . $post['para'], 'PUT'));
    }
    
}
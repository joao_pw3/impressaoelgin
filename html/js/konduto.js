function konduto (page, evento = 'page') {

	(function() {     
		var period = 300;     
		var limit = 20 * 1e3;     
		var nTry = 0;     
		var intervalID = setInterval(function() {         
			var clear = limit/period <= ++nTry;         
			if ((typeof(Konduto) !== "undefined") &&
			    (typeof(Konduto.sendEvent) !== "undefined")) {             
			Konduto.sendEvent(evento, page);
			clear = true;         
			}         
		if (clear) { 
	clearInterval(intervalID); 
		}     
	}, 
	period); 
})(page);
}

function kondutoCustomer (customerID) {
	(function() {     
		var period = 30;     
		var limit = 10 * 1e3;     
		var nTry = 0;     
		var intervalID = setInterval(function() { 
			var clear = limit/period <= ++nTry;         
			if ((typeof(Konduto) !== "undefined") && 
			   (typeof(Konduto.setCustomerID) !== "undefined")) {           
			   	window.Konduto.setCustomerID(customerID);
				clear = true;         
				}         
			if (clear) {
		 		clearInterval(intervalID); 
			}
		}, period);
	})(customerID);
}
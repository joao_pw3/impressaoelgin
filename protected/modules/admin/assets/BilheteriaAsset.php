<?php
namespace app\modules\admin\assets;
use yii\web\AssetBundle;

class BilheteriaAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/assets';
    
    public $css = [
    ];
    
    public $js = [
        'js/ajax.js',
        'js/bilheteria.js',
        'https://s3.amazonaws.com/cappta.api/js/cappta-checkout.js'
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];
}

<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\WsIngresso;
use app\models\Compras;
use app\models\Cliente;
use Da\QrCode\QrCode;
use app\models\Agenda;

/**
 * Classe que gerencia ingressos de um usuário
 */
class Ingressos extends Model {

    public $id_agenda;
    public $id_agenda_data;
    public $qrcode;
    public $codigo;
    public $bloqueado;
    public $lote_reserva;
    public $lote_entrega;
    public $lote_envio;
    public $id_carrinho;
    public $ocupante_nome;
    public $documento;
    public $status;
    public $retirado_nome;
    public $retirado_documento;
    public $produto;
    public $produto_codigo;
    public $codigo_venda;
    public $data_venda;
    public $comprador;
    public $comprador_documento;
    public $utilizacoes;
    public $utilizacoes_ultima_data;
    public $motivo_invalidado;
    public $data_reserva;
    public $data_producao;
    public $data_envio;
    public $data_entrega;
    public $data_associacao;
    public $data_liberado;
    public $data_retirado;
    public $data_invalidado;
    public $data_segunda_via;
    public $agenda;
    public $compra;
    public $lista;
    public $imgQrCode;
    public $evento;

    /**
     * Um ingresso precisa ter uma compra relacionada
     */
    public function __construct(Compras $compra) {
        $this->compra = $compra;
    }

    /**
     * Buscar os qrcodes dos produtos
     * Desconsidera a taxa de conveniência
     */
    public function qrcodes() {
        $session = Yii::$app->session;
        $session->open();
        $cliente = new Cliente;
        $cliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
        $this->compra->ingresso = [];
        foreach ($this->compra->carrinho->produtos as $produto) {
            if (isset($produto->tags))
                $tags = \app\models\Tags::objetoTags($produto->tags);
            if (isset($tags) && isset($tags->taxa_conveniencia))
                continue;
            $ws = new WsIngresso($cliente->user_token);
            $consultaIngresso = $ws->consultar(['id_agenda_data' => $produto->agenda_data, 'lote_origem' => 'COMPRA_' . $this->compra->carrinho->codigo_cobranca]);

            if (!$consultaIngresso->successo) {
                $this->addError('lista', $consultaIngresso->erro->mensagem);
                continue;
            }

            foreach ($consultaIngresso->objeto as $objQrCode) {
                if ($objQrCode->produto_codigo == $produto->codigo) {
                    $obj = new Ingressos($this->compra);
                    $evento = new Agenda($cliente);
                    $evento->codigo = $objQrCode->id_agenda;
                    $evento->umEvento();
                    $obj->evento = $evento;
                    $obj->loadFromApi($objQrCode);
                    $obj->imgQrCode = $this->getQrCode($obj->qrcode)->writeDataUri();
                    $this->compra->ingresso[$produto->codigo][$obj->codigo] = $obj;
                }
            }
        }
    }

    /**
     * Para um código de QrCode informado, gerar a imagem correspondente
     * @param string $qrCode Código para geração de imagem QrCode
     */
    public function getQrCode($qrCode) {
        if (!empty($qrCode)) {
            return (new QrCode($qrCode))
                ->setSize(150)
                ->setMargin(5)
                ->useForegroundColor(0, 0, 0);
        }
    }

    /**
     * Identificar lotes e totalizar registros - identificar data de evento
     * @param array $registros Registros para serem separados e contados
     * @param string $data Data do evento: reserva, producao, envio, entrega, associacao, liberado, retirado, invalidado, segunda_via
     */
    public function registrosLote($registros = [], $data = "reserva") {
        $lotes = [];
        if (!empty($registros)) {
            foreach ($registros->objeto as $index => $ingresso) {
                $lotes[$ingresso->lote_reserva]['total'] = isset($lotes[$ingresso->lote_reserva]['total']) ? $lotes[$ingresso->lote_reserva]['total'] + 1 : 1;
                $lotes[$ingresso->lote_reserva]['data'] = $ingresso->{'data_' . $data};
            }
        }
        return $lotes;
    }

    /**
     * Identificar todos os lotes, totalizar registros e identificar datas por etapa
     * @param array $registros Registros para serem separados e contados
     */
    public function registrosLoteConsolidado($registros = []) {
        $lotes = [];
        if (!empty($registros->objeto)) {
            $arrDatas = ['reserva', 'producao', 'envio', 'entrega', 'associacao', 'liberado'];
            foreach ($registros->objeto as $index => $ingresso) {
                $lotes[$ingresso->lote_reserva]['total'] = isset($lotes[$ingresso->lote_reserva]['total']) ? $lotes[$ingresso->lote_reserva]['total'] + 1 : 1;
                $lotes[$ingresso->lote_reserva]['corte_inferior'] = !isset($lotes[$ingresso->lote_reserva]['corte_inferior']) ?
                        $ingresso->codigo_venda :
                        ($ingresso->codigo_venda < $lotes[$ingresso->lote_reserva]['corte_inferior'] ?
                        $ingresso->codigo_venda :
                        $lotes[$ingresso->lote_reserva]['corte_inferior']);
                $lotes[$ingresso->lote_reserva]['corte_superior'] = !isset($lotes[$ingresso->lote_reserva]['corte_superior']) ?
                        $ingresso->codigo_venda :
                        ($ingresso->codigo_venda > $lotes[$ingresso->lote_reserva]['corte_superior'] ?
                        $ingresso->codigo_venda :
                        $lotes[$ingresso->lote_reserva]['corte_superior']);
                foreach ($arrDatas as $data) {
                    if (!empty($ingresso->{'data_' . $data}) && !isset($lotes[$ingresso->lote_reserva]['data_' . $data])) {
                        $lotes[$ingresso->lote_reserva]['data_' . $data] = $ingresso->{'data_' . $data};
                    }
                }
            }
        }
        return $lotes;
    }

    /**
     * Encontra e retorna um único ingresso pelo código
     */
    public function informacoesIngresso() {
        $session = Yii::$app->session;
        $session->open();
        $cliente = new Cliente;
        $cliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
        $ws = new WsIngresso($cliente->user_token);
        $consultaIngresso = $ws->consultar(['qrcode' => $this->qrcode]);

        if (!isset($consultaIngresso->successo))
            $this->addError('qrcode', 'Não foi possível consultar o ingresso');
        if (!$consultaIngresso->successo)
            $this->addError('qrcode', $consultaIngresso->erro->mensagem);

        if ($this->hasErrors('qrcode'))
            return $this->render('/default/error', ['name' => 'Erro ao buscar informações', 'message' => $this->getErrors('qrcode')[0]]);

        $this->loadFromApi($consultaIngresso->objeto[0]);
        $this->imgQrCode = $this->getQrCode($this->qrcode)->writeDataUri();
    }

    /**
     * Atribuir valores retornado pela API às propriedades
     */

    public function consultar($post=[]) {
        $session = Yii::$app->session;
        $session->open();
        $cliente = new Cliente;
        $consultaIngresso = (new WsIngresso($cliente->getTokenMatriz()))->consultar($post);
        return $consultaIngresso;        
    }

    public function loadFromApi($obj) {
        if (!isset($obj->id_agenda, $obj->qrcode))
            return false;

        $this->id_agenda = $obj->id_agenda;
        $this->id_agenda_data = $obj->id_agenda_data;
        $this->qrcode = $obj->qrcode;
        $this->codigo = $obj->codigo;
        $this->bloqueado = $obj->bloqueado;
        $this->lote_reserva = $obj->lote_reserva;
        $this->lote_entrega = $obj->lote_entrega;
        $this->lote_envio = $obj->lote_envio;
        $this->id_carrinho = $obj->id_carrinho;
        $this->ocupante_nome = $obj->ocupante_nome;
        $this->documento = $obj->documento;
        $this->status = $obj->status;
        $this->retirado_nome = $obj->retirado_nome;
        $this->retirado_documento = $obj->retirado_documento;
        $this->produto = $obj->produto;
        $this->produto_codigo = $obj->produto_codigo;
        $this->codigo_venda = $obj->codigo_venda;
        $this->data_venda = $obj->data_venda;
        $this->comprador = $obj->comprador;
        $this->comprador_documento = $obj->comprador_documento;
        $this->utilizacoes = $obj->utilizacoes;
        $this->utilizacoes_ultima_data = $obj->utilizacoes_ultima_data;
        $this->motivo_invalidado = $obj->motivo_invalidado;
        $this->data_reserva = $obj->data_reserva;
        $this->data_producao = $obj->data_producao;
        $this->data_envio = $obj->data_envio;
        $this->data_entrega = $obj->data_entrega;
        $this->data_associacao = $obj->data_associacao;
        $this->data_liberado = $obj->data_liberado;
        $this->data_retirado = $obj->data_retirado;
        $this->data_invalidado = $obj->data_invalidado;
        $this->data_segunda_via = $obj->data_segunda_via;
        $this->agenda = $obj->agenda;
    }

}

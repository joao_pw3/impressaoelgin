<?php
use \yii\helpers\Url;

$this->title = 'ADM Easy for Pay' . (isset($matriz) && $matriz == true ? ' (matriz)' : '');
?>

<div class="container">
	<div class="row">
		<h2 class="tituloGeral"><i class="fa fa-clone"></i> Produto Consulta <?= isset($matriz) && $matriz == true ? ' (matriz)' : ''; ?></h2>
	</div>
	
	<div class="gridAgenda6Out">
		<div class="gridAgenda6 gridAgendaHead">
			<div>Nome</div>
			<div>Descrição</div>
			<div>Valor</div>
			<div>Desc.Fixo</div>
			<div>Desc.Perc.</div>
			<div>Status</div>
			<div>Tags</div>
			<div>Anexo</div>
		</div>
		
		<div class="gridAgenda6">	
			<div><?=$produto->nome?></div>
			<div><?=$produto->descricao?></div>
			<div>R$ <?=number_format($produto->valor,2,'.',',')?></div>
			<div><?=$produto->descontoFixo?></div>
			<div><?=$produto->descontoPercentual?></div>
			<div><?=$produto->ativo?></div>
			<div><?= isset($produto->tags) ?\app\models\Tags::stringTags($produto->tags).' - <a href="'.Url::to(['remover-tags','produto'=>$produto->codigo]).'">Remover tags</a>'	 :'nenhuma'?></div>
			<div><?= isset($produto->anexos) ?\app\models\Anexos::stringAnexos($produto->anexos).' - <a href="'.Url::to(['remover-anexos','produto'=>$produto->codigo]).'">Remover anexos</a>' :'nenhuma'?></div>
		</div>
	</div>	
	
	<div class="aliCentro">
		<a href="<?=Url::to(['adicionar-tags','produto'=>$produto->codigo])?>" class="btn btn-primary retoB">Adicionar tags</a>
		<a href="<?=Url::to(['adicionar-anexo','produto'=>$produto->codigo])?>" class="btn btn-primary retoB">Adicionar anexo</a>
		<a href="<?=Url::to(['editar-produto','produto'=>$produto->codigo])?>" class="btn btn-primary retoB">Editar produto</a>
		<a href="<?=Url::to(['excluir','produto'=>$produto->codigo])?>" class="btn btn-primary retoB">Excluir produto</a>	
	</div>
	
	<div class="boxGerEst">
		<h3 style="color:#fff;">Gerenciar estoque</h3>
		<hr style="height:1px; background:#fff; border:none;">
		<div>oferecido: <?=$produto->oferecido?> - <a href="<?=Url::to(['alterar-estoque','produto'=>$produto->codigo])?>" class="btn btn-primary retoB2">Alterar</a></div>
		<div>vendido: <?=$produto->vendido?></div>
		<div>reservado: <?=$produto->reservado?></div>
		<div>disponivel: <?=$produto->disponivel?></div>
	</div>
	
</div>
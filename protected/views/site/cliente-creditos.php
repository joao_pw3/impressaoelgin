<?php

use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use yii\jui\DatePicker;
use app\components\MenuWidget;
use app\components\TempoCarrinhoWidget;
use yii\helpers\Html;
use yii\helpers\Url;

$session = Yii::$app->session;
$session->open();

if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    echo TempoCarrinhoWidget::getBreadCrumb('losang2.png');
}

echo MenuWidget::getUrls();
?>
<div class="container">
    <div class="row">
        <?php echo MenuWidget::getMenuLateral(); ?>
        <div class="col-md-10">
            <div class="tab-content">
                <div class="boxDados">
                    <div class="content" id="cli-creditos">    
                        <?php if ($msgErro == false) { ?>
                        <div class="row cli-creditos">
                            <p>Seu saldo atual é de <strong><?= $saldo; ?></strong>.</p>
                            <p>Para utilizar estes créditos, vá até a <?= Html::a('página de eventos', Url::to(['index'])); ?> e escolha os lugares que deseja. Quando estiver na página de Pagamento, seus créditos estarão disponíveis e você poderá utilizá-los, desde que o valor seja suficiente. Em <strong>Formas de Pagamento</strong>, escolha <strong>Pagar com Créditos</strong> e clique em <strong>Autorizar pagamento</strong>.</p>
                        </div>
                        <?php } else { ?>
                        <div>
                            Não foi possível calcular o saldo atual (<?= $msgErro; ?>).
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/carrinho.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/cadastro.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset', 'yii\bootstrap\BootstrapPluginAsset']]);
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    $this->registerJs("deadline = setDataExpira('" . $session['tempoCarrinho'] . "'); initializeClock();");
}
?>
<?php
use yii\helpers\Url;
use app\assets\SvgAsset;
use kartik\money\MaskMoney;
use yii\widgets\MaskedInput;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

SvgAsset::register($this);

$this->title = 'ADM Easy for Pay';
?>
<div class="row">
    <div class="col-xs-12">
        <h1><span style="font-weight:200;">Produtos</span> - Editar assentos</h1>
		<hr class="riscoAdm">
        <?php $form = ActiveForm::begin(['id' => 'cupom-form']); ?>
        <?php $model->id_agenda = isset($id_agenda) ? $id_agenda : ''; ?>
        <?= $form->field($model, 'id_agenda')->dropDownList(ArrayHelper::map($eventos, 'codigo', 'data', 'evento')); ?>
        <div style="text-align:center; margin-top:30px;">
			<?= Html::submitButton('Carregar mapa', ['class' => 'btn btn-default btn-Padrao', 'id' => 'btn-mapa']) ?>
		</div>
        <?php ActiveForm::end(); ?>
        <hr>  
    </div>
</div>
<?php if (isset($id_agenda) && !empty($id_agenda)) { ?>
<div class="col-xs-8">
    <input type="checkbox" id="todos" class="todos"<?= $mapa == '' ? ' disabled' : ''; ?>> Selecionar todos
    <?= !empty($mapa) ? $mapa : '<p style="margin-top:100px; width:100%; text-align:center; font-size:18px; color:#E10019;">Não há produtos para este mapa</p>'; ?>
</div>
<div class="col-xs-4">
    <input type="hidden" id="url-especiais" value="<?= Url::to(['produto/assento-especial']); ?>">
    <input type="hidden" id="url-lote" value="<?= Url::to(['produto/alterar-lote']); ?>">
    <input type="hidden" id="url-estoque" value="<?= Url::to(['produto/alterar-estoque-lote']); ?>">
    <input type="hidden" id="url-reserva" value="<?= Url::to(['produto/reservar-produto']); ?>">
    <input type="hidden" id="url-carrinho" value="<?= Url::to(['produto/consultar-carrinho-produto']); ?>">
    <div class="row colAssento">
        <div>Assentos Especiais:</div>
        <div class="col-xs-5">
            <select id="especiais" class="form-control">
                <option value="">Escolha um tipo</option>
                <option value="acompanhante">Acompanhante</option>
                <option value="cadeirante">Cadeirante</option>
                <option value="mobilidade">Mobilidade reduzida</option>
                <option value="obeso">Obeso</option>
            </select>
        </div>
        <div class="col-xs-7">
            <input type="radio" name="upd-especial" value="1" checked> Incluir &nbsp;&nbsp;<input type="radio" name="upd-especial" value="0"> Remover
        </div>
        <br clear="all">
        <div class="alinhaCenter">
            <input type="button" class="btn btn-primary" id="btn-especial" value="Definir assentos especiais" disabled>
        </div>
    </div>
    <hr>
    <div class="row colAssento">
        <div>Preço:</div>
        <div class="col-xs-5">
            <?= MaskMoney::widget([
                'name' => 'preco',
                'pluginOptions' => [
                    'prefix' => 'R$ ',
                    'thousands' => '.',
                    'decimal' => ',',
                    'allowNegative' => false
                ],
                'options' => [
                    'id' => 'preco',
                    'placeholder' => 'Preço ',
                    'title' => 'Preço',
                    'class' => 'form-control',
					'style' => 'padding:2px;'
                ]
            ]); ?>
        </div>
		<div class="col-xs-7">
			&nbsp;
		</div>
		<br clear="all">
        <div class="alinhaCenter">
			<input type="button" class="btn btn-primary" id="btn-preco" value="Definir preços dos produtos" disabled>
		</div>
    </div>
    <hr>
	
    <div class="row colAssento">
        <div>Estoque:</div>
        <div class="col-xs-5">
            <?= MaskedInput::widget([
                'mask' => '999',
                'name' => 'estoque',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => 'Estoque',
                    'title' => 'Estoque',
                    'id' => 'estoque',
                    'value' => '1'
                ],
                'clientOptions' => [
                    'removeMaskOnSubmit' => true
                ]
                ]); ?>
        </div>
        <div class="col-xs-7">
			<input type="radio" name="upd-estoque" value="1" checked> Incluir &nbsp;&nbsp;<input type="radio" name="upd-estoque" value="0"> Remover 
		</div>
		<br clear="all">
		<div class="alinhaCenter">
            <input type="button" class="btn btn-primary" id="btn-estoque" value="Definir estoque dos produtos" disabled style="margin-top:0px;">
        </div>
    </div>
    <hr>
	
    <div class="row colAssento">
        <div>Status:</div>
        <div>
            <input type="radio" name="upd-status" value="1" checked> Ativo &nbsp;&nbsp;<input type="radio" name="upd-status" value="0"> Inativo<br>
        </div>
        <div style="padding-left:20px; padding-right:20px;">
            <textarea name="justifica-status" id="justifica-status" class="form-control" cols="30" rows="3" placeholder="Justificativa"></textarea>
        </div>
		<div class="alinhaCenter">
			<input type="button" class="btn btn-primary" id="btn-status" value="Mudar status dos produtos" disabled>
		</div>
    </div>
    <hr>
	
    <div class="row colAssento">
        <div>Reserva:</div>
        <div class="col-xs-5">
            <input type="text" name="documento" id="documento" class="form-control" value="" placeholder="Documento">
        </div>
        <div class="col-xs-7" style="padding-right:0px;">
			<input type="radio" name="upd-reserva" value="1" checked> Reservar &nbsp;&nbsp;<input type="radio" name="upd-reserva" value="0"> Liberar
		</div>
		<br clear="all">		
		<div style="padding-left:20px; padding-right:20px;">
            <textarea name="justifica-reserva" id="justifica-reserva" class="form-control" cols="30" rows="3" placeholder="Justificativa"></textarea>
        </div>
		<div class="alinhaCenter" style="margin-top:5px;">
			<input type="button" class="btn btn-primary" id="btn-reserva" value="Definir reserva de produtos" disabled>
		</div>
    </div>
	<br>
	<br>
</div>
<?php } ?>
<?php
use \kartik\date\DatePicker;
use app\modules\admin\assets\RelatorioAsset;

RelatorioAsset::register($this);

$session = Yii::$app->session;
$session->open();
?>
<div class="container">
    <h1><span style="font-weight:200;">Relatório</span> - Vendas</h1>
    <?php if(isset($session['report-export-csv'])) { ?>
    	<div class="alert alert-warning"><?= $session['report-export-csv']; ?></div>
    <?php } ?>

	<?= \yii\helpers\Html::beginForm('','post',['class'=>'form-inline'])?>
		<div class="col-md-4 form-group semPleft">
			<label class="margT0">Data inicial:</label>
			<?= DatePicker::widget([
				'name'  => 'de',
				'value'  => date('d/m/Y',$de->getTimestamp()),
				'language' => 'pt-BR',
				'pluginOptions'=>[
					'format' => 'dd/mm/yyyy'
				]
			]);?>
		</div>
		<div class="col-md-4 form-group">
			<label class="margT0">Data final:</label>
			<?= DatePicker::widget([
				'name'  => 'ate',
				'value'  => date('d/m/Y',$ate->getTimestamp()),
				'language' => 'pt-BR',
				'pluginOptions'=>[
					'format' => 'dd/mm/yyyy'
				]
			]);?>
		</div>
		<div class="col-md-4 form-group spcTop semPright">
			<button class="btn btn-default btn-Padrao" type="submit">Buscar</button>&nbsp;&nbsp;
			<button id="vendas-mostrar-tudo-btn" type="button" class="btn btn-default btn-Padrao" title="Mostrar transações não autorizadas" style="width:126px;">Mostrar não aut.</button>&nbsp;&nbsp;
			<a class="btn btn-primary btn-Padrao" href="<?=\yii\helpers\Url::to(['exportar-csv'])?>" <?= $total==0 ? 'disabled' : null; ?>>Gerar CSV</a>
		</div>
		<br clear="all">
	<?= \yii\helpers\Html::endForm()?>
	<br>
	<table id="tabela-vendas-transacoes" class="table table-responsive table-hover">
		<thead>
			<tr>
				<th>#</th>
				<th>Data</th>
				<th>Nome</th>
				<th>Documento</th>
				<th>E-mail</th>
				<th>Telefone</th>
				<th>Total</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$consolidadoDia=0;
			$linha=0;
			for($i=0; $i<$total ; $i++) {
				if($transacoes[$i]['codigo_status'] == 1){
					$consolidadoDia+=$transacoes[$i]['bruto'];
					$linha++;
				}
				?>
			<tr class="linhaTransacao <?=$transacoes[$i]['codigo_status']!=1 ?'hidden nao-processada' :''?>">
				<td><?=$transacoes[$i]['codigo_status'] == 1 ?$linha :'-'?></td>
				<td><?=$transacoes[$i]['dataTransacao']->format('d/m/Y H:i:s')?></td>
				<td><?=$transacoes[$i]['comprador']['nome']?></td>
				<td><?=$transacoes[$i]['comprador']['documento']?></td>
				<td><?=$transacoes[$i]['comprador']['email']?></td>
				<td><?=$transacoes[$i]['comprador']['telefone']?></td>
				<td>R$ <?=number_format($transacoes[$i]['bruto'],2,',','.')?> (<?=$transacoes[$i]['parcelas']?>)</td>
				<td><?=$transacoes[$i]['status']?> <?=$transacoes[$i]['codigo']?></td>
			</tr>
			<?php
			if(\app\models\Transacoes::linhaConsolidado($transacoes,$i)){?>
				<tr class="tabela-linha-consolidado">
					<td colspan="6">Subtotal do dia <?=$transacoes[$i]['dataTransacao']->format('d/m/Y')?></td>
					<td colspan="2">R$ <?=number_format($consolidadoDia,2,',','.')?></td>
				</tr>
				<?php
				$consolidadoDia=0;			
			}
		}?>
		</tbody>
	</table>
</div>
<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\WsCarrinho;
use Da\QrCode\QrCode;
use app\models\Taxa;

class Carrinho extends Model
{
    public $carrinho;
    public $tempoCarrinho = '+20 minutes';
    
    public $idCarrinho;
    public $produto_codigo;
    public $produto_unidades;
    public $cupom_codigo;
    public $cupom_unidades;
    public $tag_nome;
    public $tag_valor;
    public $unidade;

    /**
     * Regras de validação. 
     */
    public function rules() {
        return [
            [['idCarrinho', 'produto_codigo', 'produto_unidades'], 'required'],
            [['idCarrinho', 'tag_nome', 'tag_valor'], 'string', 'max' => '128'],
            [['produto_codigo', 'cupom_unidades'], 'integer'],
            [['produto_unidades'], 'number'],
            [['cupom_codigo', 'cupom_unidades', 'tag_nome', 'tag_valor'], 'safe']
        ];
    }

    /**
     * definição dos labels de atributos
     */
    public function attributeLabels() {
        return[
            'idCarrinho' => 'ID do Carrinho',
            'idProduto' => 'ID do Produto',
            'unidadesProduto' => 'Unidades',
            'idCupom' => 'ID do Cupom',
            'unidadesCupom' => 'Unidades',
            'tagNome' => 'Tag',
            'tagValor' => 'Valor',
        ];
    }
    
    /**
     * Método construtor sempre pede a unidade atual
     */
    public function __construct(Cliente $unidade){
        $this->unidade = $unidade;
    }
    
    /**
     * Criar carrinho de compras
     * @return object {'successo': '1', 'id': ''}
     */
    public function criarCarrinho()
    {
        $session = Yii::$app->session;
        $session->open();
        if (!isset($session['idCarrinho']) || $session['idCarrinho'] == '') {
            $this->carrinho = (new WsCarrinho($session->get('token_unidade')))->criarCarrinho();
            if ($this->carrinho->successo == '1') {
                $this->definirSessaoCarrinho($this->carrinho->objeto->id);
                $this->definirTempoCarrinho();
            }
            return $this->carrinho;
        }
        $this->definirSessaoCarrinho($session['idCarrinho']);
    }

    /**
     * Definir tempo de Expiração de Carrinho na sessão
     * Se o tempo já estiver em andamento, modificá-lo de acordo com o retorno do carrinho
     * Padrão em 03/04/2018: 20 minutos
     */
    public function definirTempoCarrinho()
    {
        if (!empty($this->carrinho) && $this->carrinho->successo == '1' && isset($this->carrinho->objeto->minutos_expirar) && (int)$this->carrinho->objeto->minutos_expirar > 0) {
            $this->tempoCarrinho = '+' . $this->carrinho->objeto->minutos_expirar . ' minutes';
        }
        $this->atualizarTempoCarrinho();
    }
    
    /**
     * Atualizar tempo do carrinho na sessão
     */
    private function atualizarTempoCarrinho() 
    {
        $session = Yii::$app->session;
        $session->open();
        $session['tempoCarrinho'] = date('Y,m,d,H,i,s', strtotime($this->tempoCarrinho));
        $session->close();
    }
    
    /**
     * Registrar id do carrinho criado/modificado na Sessão
     * @param string $idCarrinho Id do carrinho
     */
    public function definirSessaoCarrinho($idCarrinho)
    {
        if (!empty($this->carrinho->objeto->id) || !empty($idCarrinho)) {
            $session = Yii::$app->session;
            $session->open();
            $session['idCarrinho'] = !empty($this->carrinho->objeto->id) ? $this->carrinho->objeto->id : $idCarrinho;
            $session->close();
        }
    }
    
    /**
     * Remover id do carrinho criado na Session
     */
    public function limparSessaoCarrinho ()
    {
        $this->limparCarrinho();
        $this->limparDaSessao();
    }

    /**
     * Limpar carrinho de compras - API
     * @return object {'successo': '1', 'removido': '1'}
     */
    public function limparCarrinho ()
    {
        $api = new WsCarrinho($this->unidade->user_token);
        $session = Yii::$app->session;
        $session->open();
        if (isset($session['idCarrinho']) && !empty($session['idCarrinho'])) {
            $session->set('documento_unidade','');
            $session->remove('url-finaliza-pagamento');
            return $api->limparCarrinho($session['idCarrinho']);
        }
        return $api->getApiError('Não foi possível limpar o carrinho de compras.');
    }
    
    /**
     * Limpar carrinho de compras - Sessão
     */
    public function limparDaSessao ()
    {
        $session = Yii::$app->session;
        $session->open();
        $session['idCarrinho'] = '';
        $session['tempoCarrinho'] = '';
        $session['url-finaliza-pagamento'] = '';
        $session['documento_unidade'] = '';
        unset($session['idCarrinho']);
        unset($session['tempoCarrinho']);
        unset($session['url-finaliza-pagamento']);
        unset($session['documento_unidade']);
        $session->close();
    }
    
    /**
     * Atualizar carrinho de compras
     * @param $post Dados do carrinho: 
     *      '[produto]': {'codigo': '', 'unidades': ''}, 
     *      '[cupom]': {'id': '', 'unidades': ''}, 
     *      '[tag]': {'nome': '', 'valor': ''}
     * @return object {'successo': '1'}
     */
    public function atualizarCarrinho ($post)
    {
        $session = Yii::$app->session;
        $session->open();
        if (!isset($session['idCarrinho']) || empty($session['idCarrinho'])) {
            $this->criarCarrinho();
        }
        $api = new WsCarrinho($session->get('token_unidade'));
        if (!empty($post) && isset($session['idCarrinho']) && !empty($session['idCarrinho'])) {
            $object = [];
            if (isset($post['produto_codigo']) && !empty($post['produto_codigo'])) {
                $object['produto'] = ['codigo' => $post['produto_codigo'], 'unidades' => isset($post['produto_unidades']) ? (int)$post['produto_unidades'] : '1'];
                $cobrarTaxa=$session->get('cobrar_taxa',false);
                if($object['produto']['unidades']!=0 && $cobrarTaxa){
                    $taxaModel=new Taxa($this->unidade);
                    $object['taxa']=['id'=>$taxaModel->id];
                }
            }
            if (isset($post['cupom_codigo']) && !empty($post['cupom_codigo']) && $post['cupom_codigo'] != 'inteira') {
                $object['cupom'] = ['id' => $post['cupom_codigo'], 'unidades' => isset($post['cupom_unidades']) ? (int)$post['cupom_unidades'] : '1'];
            }
            if (isset($post['tag_nome']) && isset($post['tag_valor'])){// && !empty($post['tag_nome']) && !empty($post['tag_valor'])) {
                $object['tag'] = ['nome' => $post['tag_nome'], 'valor' => $post['tag_valor']];
            }
            $this->carrinho = $api->atualizarCarrinho($object, $session['idCarrinho']);
            $this->definirTempoCarrinho();
            return $this->carrinho;
        }
        return $api->getApiError('Não foi possível atualizar o carrinho de compras.');
    }
    
    /**
     * Consultar carrinho de compras
     * @param string $idCarrinho Id de um carrinho
     * @return object {'successo': '1', '[produtos]': {...}, '[cupons]': {...}, '[tags]': {...}, '[totais]': {...}}
     */
    public function consultarCarrinho ($idCarrinho='')
    {
        $api = new WsCarrinho($this->unidade->user_token);
        $session = Yii::$app->session;
        $session->open();
        $id_carrinho = (!empty($idCarrinho) ? $idCarrinho : (isset($session['idCarrinho']) && !empty($session['idCarrinho']) ? $session['idCarrinho'] : ''));
        if (!empty($id_carrinho)) {
            $this->carrinho = $api->consultarCarrinho($id_carrinho);
            return $this->carrinho;
        }
        return $api->getApiError('Não foi possível consultar o carrinho de compras.');
    }
    
    /**
     * Ao finalizar uma venda, consultar o seu carrinho para uso em outros métodos
     * @param integer $idVenda Id da compra
     */
    public function consultarCarrinhoVenda ($idVenda='')
    {
        if (!empty($idVenda) && (int)$idVenda > 0) {
            $venda = (new WsCarrinho($this->unidade->user_token))->consultarVendaFiltro(['codigo' => $idVenda]);
            if ($venda->successo === '1' && !empty($venda->objeto[0])) {
                $this->carrinho = $this->consultarCarrinho($venda->objeto[0]->id_carrinho);
            }
        }
    }

    /**
     * Enviar cobrança
     * @param $post Dados de cobrança: trid, id_carrinho, documento, [email], [tipo], [validade], [forma], [descricao]
     */
    public function enviarCobranca ()
    {
        $api = new WsCarrinho($this->unidade->user_token);
        $post = Yii::$app->request->post();
        if (is_array($post) && !empty($post)) {
            return $api->enviarCobranca($post);
        }
        return $api->getApiError('Não foi possível enviar a cobrança.');
    }
    
    /**
     * Identifica dados de uma cobrança e atualizer o id do carrinho, que recebe um timestamp ao valor original
     * @param $post Dados da cobrança: trid
     */
    public function consultarCobranca ()
    {
        $api = new WsCarrinho($this->unidade->user_token);
        $post = Yii::$app->request->post();
        if (isset($post) && !empty($post)) {
            $returnApi = $api->consultarCobranca($post);
            if ($returnApi->successo == '1' && $returnApi->objeto->id_carrinho) {
                $this->definirSessaoCarrinho($returnApi->objeto->id_carrinho);
            }
            return $returnApi;
        } 
        return $api->getApiError('Não foi possível consultar a cobrança.');
    }   
    
    /**
     * Autorizar pagamento
     * @param array $post Dados para autorizar pagamento: codigo (codigo_cobrança), codigo_cartao, senha_cartao
     */
    public function autorizarPagamento ()
    {
        $api = new WsCarrinho($this->unidade->user_token);
        $post = Yii::$app->request->post();
        if (isset($post) && !empty($post)) {
            return $api->autorizarPagamento($post);
        } 
        return $api->getApiError('Não foi possível autorizar a cobrança.');
    }
    
    /**
     * Após autorizar um pagamento, efetuar outros procedimentos importantes
     */
    public function posVenda () 
    {
        $this->limparDaSessao();
        return (object)['successo' => '1'];
    }
    
    /**
     * Para um código de QrCode informado, gerar a imagem correspondente
     * @param string $qrCode Código para geração de imagem QrCode
     * @return type
     */
    public function getQrCode($qrCode) 
    {
        if (!empty($qrCode)) {
            return (new QrCode($qrCode))
                ->setSize(150)
                ->setMargin(5)
                ->useForegroundColor(0, 0, 0);
        }
    }
    
    /**
     * Gerar o hash para uma venda
     * @param string $data Data da venda
     * @param string $documento Documento do cliente
     * @param integer $codigo Id da venda
     */
    public function setHashCompra($data='', $documento='', $codigo='') 
    {
        $hash = '';
        if (!empty($data) && !empty($documento) && !empty($codigo) && (int)$codigo > 0) {
            $returnApi = (new WsCarrinho($this->unidade->user_token))->setCriptoHash(['mensagem' => $data . '|' . $documento . '|' . $codigo]);
            if ($returnApi->successo === '1') {
                $hash = base64_encode($returnApi->objeto->criptografia);
            }
        }
        return $hash;
    }
    
    /**
     * Dado um hash, retornar suas informações originais
     * @param string $hash Hash de compra
     */
    public function getHashCompra($hash='')
    {
        if (!empty($hash)) {
            $criptografia = base64_decode($hash);
            return (new WsCarrinho($this->unidade->user_token))->getCriptoHash(['criptografia' => $criptografia]);
        }
        return [];
    }
    
    /**
     * Checar ocupantes de compras por período
     */
    public function consultarComprasOcupantes ($dateStart='', $dateEnd='')
    {
        return (new WsCarrinho($this->unidade->user_token))->consultarVendaOcupantes($dateStart, $dateEnd);
    }
    
    /**
     * Checar produtos por venda
     */
    public function consultarProdutosVenda ($idVenda) 
    {
        if ((int)$idVenda > 0) {
            return (new WsCarrinho($this->unidade->user_token))->consultarProdutosVenda($idVenda);
        }
        return (new WsCarrinho)->getApiError('Faltam informações para consultar os produtos da venda.');
    }
    
    /**
     * Consultar carrinho por tags
     * @param array $post Array de informações para consulta
     * @param string $tagsDe Tipo de tag: carrinho ou produto
     */
    public function consultarCarrinhoTags ($tagsDe='carrinho') 
    {
        $api = new WsCarrinho($this->unidade->user_token);
        $post = Yii::$app->request->post();
        if (isset($post) && !empty($post)) {
            return $api->consultarCarrinhoTags($post, $tagsDe);
        }
        return $api->getApiError('Faltam informações para consultar o carrinho por tags.');
    }
    
    /**
     * Consultar carrinho pelo documento do comprador
     * @param string $documento Documento do comprador
     * @return type
     */
    public function consultarCarrinhoComprador ($documento) 
    {
        $api = new WsCarrinho($this->unidade->user_token);
        if (!empty($documento)) {
            return $api->consultarCarrinhoComprador($documento);
        }
        return $api->getApiError('Faltam informações para consultar o carrinho do comprador.');
    }

    /**
     * Enviar cobranca de boleto. Este método apenas prepara o objeto e o envio é feito usando o método enviarCobranca
    */
    public function enviarCobrancaBoleto($cobranca, $sacado, $boleto){
        $post = [
            'trid' => $cobranca['trid'],
            'documento' => $sacado['documento'],
            'valor' => $cobranca['valor'],
            'unidades' => $cobranca['unidades'],
            'forma' => $cobranca['forma'],
            'tipo' => $cobranca['tipo'],
            'mensagem' => $cobranca['mensagem'],
            'boleto' => [
                'seunumero' => $boleto['seunumero'],
                'mensagem' => $boleto['mensagem'],
            ],
            'validade' => $cobranca['validade'],
            'creditar'=>$cobranca['creditar']
        ];
        return $this->enviarCobranca($post);
    }
    
    /**
     * Remover cupom do carrinho.
     */
    public function removerCupomCarrinho ($post) 
    {
        if (!empty($post)) {
            $carrinho = $this->consultarCarrinho();
            foreach($carrinho->objeto->produtos as $index => $produto) {
                if (isset($produto->cupom->id)) {
                    return $this->atualizarCarrinho([
                        'produto_codigo' => $produto->codigo, 
                        'cupom_codigo' => $produto->cupom->id, 
                        'cupom_unidades' => 0
                    ]);
                }
            }
        }
    }
    
    /**
     * Consultar carrinho pelo código do produto
     * @param integer $codigo
     */
    public function consultarCarrinhoProduto($codigo)
    {
        $api = new WsCarrinho($this->unidade->user_token);
        if ((int)$codigo > 0) {
            return $api->consultarCarrinhoProduto($codigo);
        }
        return $api->getApiError('Falta o código do produto para consultar o carrinho.');
    }
    
    /**
     * Clonagem de carrinho de compras
     * @param string $idCarrinho Id do carrinho atual
     * @param string $newIdCarrinho Id do novo carrinho
     */
    public function clonarCarrinho($idCarrinho, $newIdCarrinho)
    {
        $api = new WsCarrinho($this->unidade->user_token); 
        if (!empty($idCarrinho) && !empty($newIdCarrinho)) {
            return $api->clonarCarrinho(['id_origem' => $idCarrinho, 'id_destino' => $newIdCarrinho]);
        }
        return $api->getApiError('Faltam informações para clonar o carrinho');
    }
    
    /**
     * Remover tags de ocupante do carrinho
     */
    public function removerTags ($post,$idCarrinho)
    {
        $api = new WsCarrinho($this->unidade->user_token);
        if (!empty($post)){
            $ocupante=$post['tag_ocupante'];
            $object=['tag'=>['nome'=>$ocupante.'_nome']];
            $atualizar = $api->atualizarCarrinho($object,$idCarrinho);
            $object=['tag'=>['nome'=>$ocupante.'_documento']];
            $atualizar = $api->atualizarCarrinho($object,$idCarrinho);
            $object=['tag'=>['nome'=>$ocupante.'_telefone']];
            $atualizar = $api->atualizarCarrinho($object,$idCarrinho);
            $object=['tag'=>['nome'=>$ocupante.'_email']];
            $atualizar = $api->atualizarCarrinho($object,$idCarrinho);
            $this->definirTempoCarrinho();
            return $this->carrinho;
        }
        return $api->getApiError('Não foi possível remover as informações de ocupante.');
    }
    
    /**
     * Atualizar tags em lote. As tags anteriores são descartadas
     */
    public function atualizarTagsCarrinhoLote ($post,$idCarrinho)
    {
        $api = new WsCarrinho($this->unidade->user_token);
        if (!empty($post)){
            $atualizar=$api->atualizarTagsCarrinhoLote($post,$idCarrinho);
            $this->definirTempoCarrinho();
            return $atualizar;
        }
        return $api->getApiError('Não foi possível salvar informações de ocupante.');
    }

    /**
     * Trazer as unidades de um produto no carrinho pelo ID da agenda
     * @param Carrinho $carrinho o objeto do carrinho com produtos a serem consultados
     * @param int $data o código do id da data do evento
     */
    public static function qtdeProdutosEvento($carrinho,$data){
        if(isset($carrinho->objeto->produtos)){
            foreach ($carrinho->objeto->produtos as $p) {
                if($p->agenda_data==$data)
                    return $p->unidades;
            }
        }
    }

    /**
     * Enviar carrinho completo (todos os produtos e tags simultâneos)
     */
    public function salvarCarrinhoCompleto($post,$carrinho){
        $api = new WsCarrinho($this->unidade->user_token);
        $salvar=$api->salvarCarrinhoCompleto($post,$carrinho);
        //print_r($salvar);exit();
        $this->definirTempoCarrinho();
        return $salvar;
        
    }
}

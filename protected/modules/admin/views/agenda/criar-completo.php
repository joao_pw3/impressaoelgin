<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\AdminAgendaAsset;
use kartik\datetime\DateTimePicker;
AdminAgendaAsset::register($this);
$this->title = 'ADM Easy for Pay';

if($model->hasErrors()){
	echo '<p class="alert alert-warning">Erro ao cadastrar evento! Veja detalhes abaixo para corrigir</p><ul>';
	foreach ($model->getErrors() as $key => $value) {
		echo '<li>'.$value[0].'</li>';
	}
	echo '</ul>';
}

?>

<div class="container">
 <div class="row">
        <h2 class="tituloGeral"><i class="fa fa-clone"></i> Agenda - Adicionar Evento</h2>
    </div>
    
		<?php $form = ActiveForm::begin(['id' => 'agenda-form','options'=>['enctype'=>'multipart/form-data']]); ?>
		<?php $model->tipoEvento = 0; ?>
		<div div class="col-md-6" style="padding-left:0px;"><?= $form->field($model, 'espaco')->dropDownList($model->listaEspacos); ?></div>
		<div div class="col-md-6" style="padding-left:0px;"><?= $form->field($model, 'tipoEvento')->radioList(['1' => 'Lugares marcados', '0' => 'Lugares livres'])->label('Tipo de evento'); ?></div>
		<br clear="all">
		<div div class="col-md-12" style="padding-left:0px;"><?= $form->field($model, 'nome') ?></div>
		<div div class="col-md-6" style="padding-left:0px;"><?= $form->field($model, 'descricao')->textarea(['rows' =>5]) ?></div>
		<!-- <div div class="col-md-6" style="padding-right:0px;"><?= $form->field($model, 'sinopse')->textarea(['rows' =>5]) ?></div>
		<br clear="all">
		<div div class="col-md-6" style="padding-left:0px;"><?= $form->field($model, 'direcao')->textarea(['rows' =>3]) ?></div>
		<div div class="col-md-6" style="padding-right:0px;"><?= $form->field($model, 'atores')->textarea(['rows' =>3]) ?></div>
		<br clear="all">
		<div div class="col-md-6" style="padding-left:0px;"><?= $form->field($model, 'premios')->textarea(['rows' =>3]) ?></div>
		<div div class="col-md-6" style="padding-right:0px;"><?= $form->field($model, 'classificacao')->textarea(['rows' =>3]) ?></div>
		<br clear="all"> -->


		<div class="col-md-4" style="padding-left:0px;"><?= $form->field($model, 'duracao') ?></div>
		<br clear="all">
		<div class="col-md-12">
			<h5>Qual o período em que o evento ficará em cartaz?</h5>
		</div>
		<div class="col-md-6 semPleft">
		<?= $form->field($model, 'dataInicial')->widget(DateTimePicker::className(), [
			'type' => DateTimePicker::TYPE_COMPONENT_APPEND,//mostrar botões no fim do input (há outras opções)
			'readonly' => true,// impede o usuário de escrever (evita erros de digitação)
			'pluginOptions' => [
				'todayBtn' => true,// destaca o dia atual no calendário
				'autoclose' => true,// fecha o calendário automaticamente no final das escolhas do cliente
				'format' => 'dd/mm/yyyy hh:ii', // formato da data (já coloquei no formato da API)
				'startDate' => date('d/m/Y H:i'), // bloquear datas anteriores ao dia atual
			]

		]); ?>
		</div>
		<div class="col-md-6" style="padding-right:0px;">
		<?= $form->field($model, 'dataFinal')->widget(DateTimePicker::className(), [
			'type' => DateTimePicker::TYPE_COMPONENT_APPEND,//mostrar botões no fim do input (há outras opções)
			'readonly' => true,// impede o usuário de escrever (evita erros de digitação)
			'pluginOptions' => [
				'todayBtn' => true,// destaca o dia atual no calendário
				'autoclose' => true,// fecha o calendário automaticamente no final das escolhas do cliente
				'format' => 'dd/mm/yyyy hh:ii', // formato da data (já coloquei no formato da API)
				'startDate' => date('d/m/Y H:i'), // bloquear datas anteriores ao dia atual
			]

		]); ?>
		</div>
		<br clear="all">
		<div class="col-md-12">
			<h5>Definir quando o evento começa a ser exibido no site (opcional)</h5>
		</div>
		<div class="col-md-6 semPleft">
		<?= $form->field($model, 'dataAtivar')->widget(DateTimePicker::className(), [
			'type' => DateTimePicker::TYPE_COMPONENT_APPEND,//mostrar botões no fim do input (há outras opções)
			'readonly' => true,// impede o usuário de escrever (evita erros de digitação)
			'pluginOptions' => [
				'todayBtn' => true,// destaca o dia atual no calendário
				'autoclose' => true,// fecha o calendário automaticamente no final das escolhas do cliente
				'format' => 'dd/mm/yyyy hh:ii', // formato da data (já coloquei no formato da API)
				'startDate' => date('d/m/Y H:i'), // bloquear datas anteriores ao dia atual
			]

		]); ?>
		</div>
		<div class="col-md-6" style="padding-right:0px;">
		<?= $form->field($model, 'dataDesativar')->widget(DateTimePicker::className(), [
			'type' => DateTimePicker::TYPE_COMPONENT_APPEND,//mostrar botões no fim do input (há outras opções)
			'readonly' => true,// impede o usuário de escrever (evita erros de digitação)
			'pluginOptions' => [
				'todayBtn' => true,// destaca o dia atual no calendário
				'autoclose' => true,// fecha o calendário automaticamente no final das escolhas do cliente
				'format' => 'dd/mm/yyyy hh:ii', // formato da data (já coloquei no formato da API)
				'startDate' => date('d/m/Y H:i'), // bloquear datas anteriores ao dia atual
			]


		]); ?>
		</div>
		<br clear="all">
		<div style="padding:20px;">
			<?= $form->field($modelImgEvento, 'objeto')->fileInput()->label('Imagem principal do evento - tam. 920x226 px (L x A)') ?>
			<?= $form->field($model, 'destaque')->checkBox() ?>
			<?= $form->field($modelImgDestaque, 'objeto')->fileInput(/*['class'=>'hidden'])->label(null,['class'=>'hidden']*/)->label('Imagem para carrossel do Site - tam. 1200x502 px (L x A)') ?>
			<?= $form->field($modelImgDestaqueApp, 'objeto')->fileInput()->label('Imagem para carrossel do APP - tam. 1200x1534 px (L x A)') ?>


			<?= $form->field($model, 'preco')->label('Informe o preço dos ingressos. Se forem vários, informe o da maioria e você poderá editar o restante posteriormente') ?>
			<?= $form->field($model, 'ativo')->radioList(['1'=>'Sim','0'=>'Não'])->label('Publicar imediatamente') ?>
			<?php if($model->hasErrors()){
				echo '<p class="alert alert-warning">Erro ao cadastrar evento! Veja detalhes abaixo para corrigir</p><ul>';
				foreach ($model->getErrors() as $key => $value) {
					echo '<li>'.$value[0].'</li>';
				}
				echo '</ul>';
			}?>
			<HR>
			<div class="form-group" style="text-align:center;">
				<?= Html::submitButton('Cadastrar', ['class' => 'btn btn-primary btn-pagar']) ?>
			</div>
	

			<?php ActiveForm::end(); ?>
			<?php $this->registerJs("
			  $('#agenda-duracao').mask('00:00:00');
			", \yii\web\View::POS_READY);
			?>
		</div>
		<br>
		<br>
</div>
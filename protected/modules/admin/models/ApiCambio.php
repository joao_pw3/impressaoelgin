<?php
namespace app\modules\admin\models;

use app\models\Api;
/**
 * Description of ApiCambio
 *
 * @author User
 */
class ApiCambio extends Api 
{
    public $logs;
    
    /**
     * Colocar créditos em um cartão
     * @param array $post Dados para colocar créditos em um cartão: [trid, codigo_operador, qrcode, senha, valor, forma_pagamento, parcelas]
     */
    public function colocarCreditoCartao($post=[])
    {
        $this->setLog('Colocar credito em QR Code');
        if (!empty($post) && is_array($post)) {
            $this->logs->log('Colocar crédito em QR Code', $post);
            $returnApi = json_decode($this->call('creditar/', 'POST', $post, true));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao colocar crédito em QR Code' : 'QR Code creditado com sucesso', $returnApi);
            return $returnApi;
        }  
        $this->logs->log('Crédito de QR Code inválido.');
        return $this->getApiError('Não foi possível creditar o QR Code informado. Faltam informações para efetuar o procedimento.');
    }
    
    /**
     * Ler saldo de um cartão
     * @param type $post
     */
    public function lerSaldoCartao($post=[])
    {
        $this->setLog('Ler saldo em QR Code');
        if (!empty($post) && is_array($post)) {
            $this->logs->log('Leitura de saldo em QR Code', $post);
            $returnApi = json_decode($this->call('creditar/' . $post['documento'], 'GET', NULL, false, 'A'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao ler saldo em QR Code' : 'Saldo de QR Code identificado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Leitura de saldo em Qr Code inválida.');
        return $this->getApiError('Não foi possível consultar o saldo. Faltam informações para efetuar o procedimento.');
    }
    
    /**
     * Ler o extrato de um cliente
     * @param type $post
     */
    public function lerExtratoCliente($post=[])
    {
        $this->setLog('Ler extrato de cliente');
        if (!empty($post) && is_array($post)) {
            $this->logs->log('Leitura de extrato de cliente', $post);
            $returnApi = json_decode($this->call('creditar/' . (isset($post['documento']) ? $post['documento'] : $post['cpf']) . '/detalhado', 'GET', NULL, false, 'A'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao ler extrato do cliente' : 'Extrato lido com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Leitura de extrato do cliente inválida.');
        return $this->getApiError('Não foi possível listar o extrato. Faltam informações para efetuar o procedimento.');
    }
    
}

<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\controllers\AbstractController;
use app\modules\admin\models\ManutencaoModel;
/**
 * Controller de Eventos
 */
class SistemaController extends AbstractController
{
    public function actionIndex(){

    	return $this->render('index');
    }
    
    public function actionManutencao()
    {	
    	$message = null;
        $model = new ManutencaoModel();

        $informacao = [
            'button' => ['saia' => 'Site saia da manutenção', 'entra' => 'Site entre em manutenção.']
        ];

        $file = file_get_contents(Yii::getAlias('@app').'/data/controles.js');
        $jsonFile = json_decode($file);	

        if (Yii::$app->request->post()) 
        {
            $post = Yii::$app->request->post('ManutencaoModel');
            
            if($post['texto'] != '')
            {
            	$jsonFile->sistema->manutencao->offline = true;
                $jsonFile->sistema->manutencao->texto = nl2br($post['texto']);
				$message['warning'] = 'O site entrou em manutenção.';
                $info['btn'] = $informacao['button']['saia'];
			}
			else
            {
				$jsonFile->sistema->manutencao->offline = false;
                $jsonFile->sistema->manutencao->texto = null;
				$message['warning'] = 'O site saiu da manutenção.';
                $info['btn'] = $informacao['button']['entra'];
			}

            $jsonFileNovo = json_encode($jsonFile);
            file_put_contents(Yii::getAlias('@app').'/data/controles.js', $jsonFileNovo);
        } 
        else 
        {
			if($jsonFile->sistema->manutencao->offline === true) {
				$message['info'] = 'O site atualmente está em manutenção';
                $info['btn'] = $informacao['button']['saia'];
            }
			else 
            {
				$message['info'] = 'O site atualmente está em produção'; 
                $info['btn'] = $informacao['button']['entra'];       	
            }
        }

        return $this->render('manutencao', ['manutencao' => $model, 'message' => $message, 'info' => $info]);
    }
}
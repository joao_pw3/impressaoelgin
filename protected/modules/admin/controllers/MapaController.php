<?php
namespace app\modules\admin\controllers;

/**
 * Gerador de mapas
 */
class MapaController extends AbstractController 
{

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex() 
    {
        return $this->render('index');
    }
    
}
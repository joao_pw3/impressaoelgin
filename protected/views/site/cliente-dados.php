<?php

use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use yii\jui\DatePicker;
use app\components\MenuWidget;
use app\components\TempoCarrinhoWidget;
use yii\helpers\Url;


$session = Yii::$app->session;
$session->open();
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    echo TempoCarrinhoWidget::getBreadCrumb('losang2.png');
}

$cliente->user_id = isset($clienteApi->objeto->user_id) ? $clienteApi->objeto->user_id : '';
?>
<div class="container">
    <div class="row">
        <?php
        if($cliente->user_id) {
            echo MenuWidget::getUrls();
            echo MenuWidget::getMenuLateral(); 
        }
        else {
            echo MenuWidget::getUrlsVisitante();      
            echo MenuWidget::getMenuVisitante(); 
        }
        ?>
        <div class="col-md-10">
            <div class="tab-content">
                <?php
                if(Yii::$app->session->hasFlash('aviso-cadastro')){?>
                    <p class="alert alert-warning"><?=Yii::$app->session->getFlash('aviso-cadastro');?></p>
                <?php } ?>
                
                <div class="boxDados">

                    <?php $form = ActiveForm::begin(['id' => 'form-cadastro']); ?>

                        <input type="hidden" id="url-cadastro" value="<?= Url::to(['site/cadastro-basico']); ?>">
                        <input type="hidden" id="url-login" value="<?= Url::to(['site/login']); ?>">
                        <input type="hidden" id="url-cadastro-completo" value="<?= Url::to(['site/cadastro-completo']); ?>">
                        <input type="hidden" id="url-cartao" value="<?= Url::to(['site/cadastro-cartao']); ?>">
                        <input type="hidden" id="url-cep" value="<?= Url::to(['site/check-cep']); ?>">
                        <input type="hidden" id="url-resumo" value="<?= Url::to(['site/resumo']); ?>">
                        <input type="hidden" id="url-visitante" value="<?= Url::to(['site/visitante']); ?>">
                        <input type="hidden" id="url-cliente" value="<?= Url::to(['site/cliente']); ?>">
                        <input type="hidden" id="url-cliente-cartao" value="<?= Url::to(['site/cliente-pagamentos']); ?>">
                        <input type="hidden" id="url-index" value="<?= Url::to(['site/index']); ?>">
                        <input type="hidden" id="url-verificar-documento" value="<?= Url::to(['site/verificar-documento']); ?>">

                        <div class="row">
                            <div class="col-md-6">
                                <?php $cliente->nome = isset($clienteApi->objeto->comprador->nome) ? $clienteApi->objeto->comprador->nome : ''; ?>
                                <?= $form->field($cliente, 'nome')->textInput([
                                    'class' => 'form-control',
                                    'placeholder' => 'Nome completo / Razão Social',
                                    'title' => 'Nome completo / Razão Social',
                                    'id' => 'nome'
                                ]); ?>
                            </div>
                            <div class="col-md-6">
                                <?php $cliente->documento = isset($session['renovacao_temporada']) && $session['renovacao_temporada'] == true && !empty($session['documento_temporada']) ? base64_decode($session['documento_temporada']) : (isset($clienteApi->objeto->comprador->documento) ? $clienteApi->objeto->comprador->documento : ''); ?>
                                <?= $form->field($cliente, 'documento')->textInput()
                                    ->widget(MaskedInput::className(), [
                                        'mask' => ['999.999.999-99', '99.999.999/9999-99'],
                                        'options' => [
                                            'class' => 'form-control cpf',
                                            'id' => 'cpf',
                                            'clean' => false,
                                            'placeholder' => 'CPF ou CNPJ',
                                            'title' => 'CPF ou CNPJ',
                                            'readonly' => isset($session['renovacao_temporada']) && $session['renovacao_temporada'] == true && !empty($session['documento_temporada']) ? true : false,
                                            //'disabled' => !empty($clienteApi->objeto->comprador->documento) ? true : false
                                        ]
                                ]); ?>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-3">
                                <?php $cliente->data_nasc = isset($clienteApi->objeto->comprador->data_nasc) ? $clienteApi->objeto->comprador->data_nasc : ''; ?>
                                <?= $form->field($cliente, 'data_nasc')->textInput()
                                    ->widget(DatePicker::className(), [
                                        'dateFormat' => 'php:d/m/Y',
                                        'clientOptions' => [
                                            'changeMonth' => true,
                                            'changeYear' => true,
                                        ]
                                    ])
                                    ->widget(MaskedInput::className(), [
                                        'mask' => '99/99/9999',
                                        'options' => [
                                            'class' => 'form-control',
                                            'placeholder' => 'Nascimento / Abertura',
                                            'title' => 'Nascimento / Abertura',
                                        ],
                                ]); ?>
                            </div>
                            <div class="col-md-3">
                                <?php $cliente->sexo = isset($clienteApi->objeto->comprador->sexo) ? $clienteApi->objeto->comprador->sexo : ''; ?>
                                <?php $items = ['' => 'Não se aplica', 'M' => 'Masculino', 'F' => 'Feminino']; ?>
                                <?= $form->field($cliente, 'sexo')->dropDownList($items, [
                                    'class' => 'form-control',
                                    'id' => 'sexo',
                                    'placeholder' => 'Gênero',
                                    'title' => 'Gênero'
                                ]); ?>
                            </div>
                            <div class="col-md-3">
                                <?php $cliente->ddd = isset($clienteApi->objeto->comprador->ddd) ? $clienteApi->objeto->comprador->ddd : ''; ?>
                                <?= $form->field($cliente, 'ddd')->textInput()
                                    ->widget(MaskedInput::className(), [
                                        'mask' => '99',
                                        'options' => [
                                            'class' => 'form-control',
                                            'placeholder' => 'DDD',
                                            'title' => 'DDD',
                                            'id' => 'ddd',
                                        ]
                                ]); ?>
                            </div>
                            <div class="col-md-3">
                                <?php $cliente->celular = isset($clienteApi->objeto->comprador->celular) ? $clienteApi->objeto->comprador->celular : ''; ?>
                                <?= $form->field($cliente, 'celular')->textInput()
                                    ->widget(MaskedInput::className(), [
                                        'mask' => '[9]9999-9999',
                                        'options' => [
                                            'class' => 'form-control',
                                            'placeholder' => 'Celular',
                                            'title' => 'Celular',
                                            'id' => 'celular',
                                        ]
                                ]); ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <?php $cliente->cep = isset($clienteApi->objeto->comprador->cep) ? $clienteApi->objeto->comprador->cep : ''; ?>
                                <?= $form->field($cliente, 'cep')->textInput()
                                    ->widget(MaskedInput::className(), [
                                        'mask' => '99999-999',
                                        'options' => [
                                            'class' => 'form-control',
                                            'id' => 'cep',
                                            'placeholder' => 'CEP',
                                            'title' => 'CEP',
                                            'id' => 'cep',
                                        ]
                                ]); ?>
                            </div>
                            <div class="col-md-3">
                                <?php $cliente->num = isset($clienteApi->objeto->comprador->num) ? $clienteApi->objeto->comprador->num : ''; ?>
                                <?= $form->field($cliente, 'num')->textInput([
                                    'class' => 'form-control',
                                    'placeholder' => 'Número',
                                    'title' => 'Número',
                                    'id' => 'numero',
                                ]); ?>
                            </div>
                            <div class="col-md-6">
                                <?php $cliente->compl = isset($clienteApi->objeto->comprador->compl) ? $clienteApi->objeto->comprador->compl : ''; ?>
                                <?= $form->field($cliente, 'compl')->textInput([
                                    'class' => 'form-control',
                                    'placeholder' => 'Complemento',
                                    'title' => 'Complemento',
                                    'id' => 'compl'
                                ]); ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <?php $cliente->endereco = isset($clienteApi->objeto->comprador->endereco) ? $clienteApi->objeto->comprador->endereco : ''; ?>
                                <?= $form->field($cliente, 'endereco')->textInput([
                                    'class' => 'form-control',
                                    'id' => 'endereco',
                                    'placeholder' => 'Endereço',
                                    'title' => 'Endereço',
                                    'readonly' => true
                                ]); ?>
                            </div>
                            <div class="col-md-4">
                                <?php $cliente->bairro = isset($clienteApi->objeto->comprador->bairro) ? $clienteApi->objeto->comprador->bairro : ''; ?>
                                <?= $form->field($cliente, 'bairro')->textInput([
                                    'class' => 'form-control',
                                    'id' => 'bairro',
                                    'placeholder' => 'Bairro',
                                    'title' => 'Bairro',
                                    'readonly' => true
                                ]); ?>
                            </div>
                            <div class="col-md-4">
                                <?php $cliente->cidade_uf = isset($clienteApi->objeto->comprador->cidade_nome) ? $clienteApi->objeto->comprador->cidade_nome . ' / ' . $clienteApi->objeto->comprador->uf : ''; ?>
                                <?= $form->field($cliente, 'cidade_uf')->textInput([
                                    'class' => 'form-control',
                                    'id' => 'cidade_uf',
                                    'placeholder' => 'Cidade / UF',
                                    'title' => 'Cidade / UF',
                                    'readonly' => true
                                ]); ?>
                                <?= $form->field($cliente, 'cidade')->hiddenInput(['id' => 'cidade', 'value' => isset($clienteApi->objeto->comprador->cidade) ? $clienteApi->objeto->comprador->cidade : ''])->label(false); ?>
                                <?= $form->field($cliente, 'cidade_nome')->hiddenInput(['id' => 'cidade_nome', 'value' => isset($clienteApi->objeto->comprador->cidade_nome) ? $clienteApi->objeto->comprador->cidade_nome : ''])->label(false); ?>
                                <?= $form->field($cliente, 'uf')->hiddenInput(['id' => 'uf', 'value' => isset($clienteApi->objeto->comprador->uf) ? $clienteApi->objeto->comprador->uf : ''])->label(false); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                    			<span class="contato">
                    				<input type="button" value="<?= isset($clienteApi->objeto->user_id) ? 'Atualizar' : 'Prosseguir'; ?>" id="btn-cli-dados" class="icon-next carrinho">
                    			</span>
                            </div>
                        </div>    

                    <?php ActiveForm::end(); ?>        
                </div>
            </div>
        </div>
    </div>
</div>
<?php

$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/carrinho.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/cadastro.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset', 'yii\bootstrap\BootstrapPluginAsset']]);
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    $this->registerJs("deadline = setDataExpira('" . $session['tempoCarrinho'] . "'); initializeClock();");
}
?>
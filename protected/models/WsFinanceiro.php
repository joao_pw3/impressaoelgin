<?php
namespace app\models;
use app\models\Ws;

/**
 * Classe para Financeiro
 */
class WsFinanceiro extends Ws 
{
    
    /**
     * Método construtor - obrigado a passar o token do vendedor para separação de unidades
     */
    public function __construct($tokenVendedor=''){
        if (empty($tokenVendedor)) {
            throw new \Exception("Token vendedor não foi declarado", 1);        
        }
        $this->setToken($tokenVendedor, 'V');
    }

    public function tabela($post) {
        return json_decode($this->call('financeiro/', 'POST', $post));
    }
    
    /**
     * Consultar valores financeiros
     * @param type $post
     */
    public function consulta($post) 
    {
        return json_decode($this->call('financeiro', 'PUT', $post));
    }
    
}
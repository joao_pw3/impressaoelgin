<?php
use yii\helpers\Url;

$this->title = 'ADM Estádio Mineirão :: Gerenciar operadores';
?>
<div class="container row">
    <div class="col-md-12">
        <h2 class="tituloGeral"><i class="fa fa-clone"></i> Gerenciar operadores</h2>
        
        <ul>
            <?php
            foreach ($model as $o) {
                echo '<a href="'.Url::to(['ficha','codigo'=>$o->codigo]).'"><li>'.$o->nome.' &lt;'.$o->email.'&gt;</a></li>';
            }
            ?>
        </ul>
    </div>
</div>
<?php
namespace app\modules\admin\models;

use Yii;
use app\models\ApiCarrinho;
use app\models\ApiProduto;

/**
 * Classe FinanceiroModel
 * faz os cálculos para apresentar relatórios financeiros das vendas
*/
class FinanceiroModel{
	
	public $dataInicial;
	public $intervalo;
	public $limite;
	public $periodos;
	public $vendas;
	public $produtos;

	public function __construct(){
		// $this->dataInicial=new \DateTime("first day of November 2017");
		$this->dataInicial=new \DateTime("first day of March 2018");
		$this->intervalo='P1M';
		// $this->limite=new \DateTime("last day of December 2018");
		$this->limite=new \DateTime("last day of March 2018");
		$this->setPeriodos();

	}

	/**
	 * Definir os períodos a serem utilizados na pesquisa.
	 * Se baseia em $dataInicial e vai percorrendo pelo $intervalo até o $limite
	 */
	public function setPeriodos(){
		$dataAtual=$this->dataInicial;
		while($dataAtual<$this->limite){
			$periodos[]=[$dataAtual->format('d-m-Y').'t00:00:00',$dataAtual->format('t-m-Y').'t23:59:59'];
			$dataAtual=$dataAtual->add(new \DateInterval($this->intervalo));
		}

		$this->periodos=$periodos;
	}

	/**
	 * Faz a consulta no endpoint de vendas trazendo as vendas para cada período.
	 * Não considerar se nenhuma venda for encontrada
	 */
	public function vendasPeriodos(){
		$apiCarrinho=new ApiCarrinho;
		foreach($this->periodos as $periodo){
			$vendas=$apiCarrinho->consultarVenda($periodo[0], $periodo[1], $token='V');
			$vendas=$apiCarrinho->dadosCompletos($vendas);
			$key=substr($periodo[0], 3, 7);
			if($vendas->successo)
				$this->vendas[$key]=$vendas->objeto;
		}
	}

	/**
	 * Sumário Executivo - separar e organizar as informações com base nas vendas.
	 * Separar as pesquisas por mês
	 * Mostrar os tipos de transação e receita para cada tipo
	 * Informações de cada bloco
	 */
	public function sumarioExecutivo(){
		$tiposTransacao;
		$vendasPorProduto;
		foreach($this->vendas as $periodo=>$vendas){
			$tiposTransacao[$periodo]=$this->tiposTransacao($vendas);
			$vendasPorProduto[$periodo]=$this->separarVendasProdutos($vendas);
		}
		
		return ['tipos'=>$tiposTransacao,'produtos'=>$vendasPorProduto];
	}

	/**
	 * Separar os tipos de transação - CC, Boleto e Depósito - informando pct do total e receita
	 */
	public function tiposTransacao($objVendas){
		$total=0;
		$cc=['unidades'=>0,'receita'=>0];
		$boleto=['unidades'=>0,'receita'=>0];
		$deposito=['unidades'=>0,'receita'=>0];
		$outros=['unidades'=>0,'receita'=>0];
		foreach ($objVendas as $o) {
			switch($o->vendas->tipo){
				case '':
					$cc=['unidades'=>$cc['unidades']+1,'receita'=>$cc['receita']+$o->vendas->total];
					break;
				case 'Boleto':
					$boleto=['unidades'=>$boleto['unidades']+1,'receita'=>$boleto['receita']+$o->vendas->total];
					break;
				case 'Depósito/TED/Doc':
					$deposito=['unidades'=>$deposito['unidades']+1,'receita'=>$deposito['receita']+$o->vendas->total];
					break;
				default:
					$outros=['unidades'=>$outros['unidades']+1,'receita'=>$outros['receita']+$o->vendas->total];
					break;
			}
			$total++;
		}
		return ['total'=>$total,'cc'=>$cc,'boleto'=>$boleto,'deposito'=>$deposito,'outros'=>$outros];
	}

	/**
	 * Percorrer lista de produtos adicionando dados para cada bloco/setor
	 */
	public function separarVendasProdutos($vendas){
		foreach ($vendas as $v) {
			$this->dadosProdutosVendidos($v->id_carrinho);
		}
		return $this->produtos;
	}

	/**
	 * Pegar as propriedades de cada produto vendido para separa-los em tipos (bloco, camarotes, etc)
	 */
	public function dadosProdutosVendidos($idCarrinho){
		$dados=(new ApiCarrinho)->dadosProdutos($idCarrinho);
		$apiProduto=new ApiProduto;
		if(isset($dados->produtos)){
			foreach ($dados->produtos as $produto) {
				$tagsArray=ApiProduto::tagsArray($produto->tags,true);
				//if($tagsArray->tipo=='camarote') continue;
				$valorProdutoVendido=$apiProduto->calcDesconto($produto);				
				$this->produtos[$tagsArray->area][$this->infoProdutoConsolidado($tagsArray->tipo,$tagsArray)]['valor']+=$valorProdutoVendido;
				$this->produtos[$tagsArray->area][$this->infoProdutoConsolidado($tagsArray->tipo,$tagsArray)]['vendidos']+=1;
			}
		}
	}

	/**
	 * Retornar o índice correspondente ao nome do produto na tag
	 * @param array $tipo tipo de produto no array de objetos
	 * @param array $tags tags de produto no array de objetos
	 * @return array
	 */
	public function infoProdutoConsolidado($tipo,$tags){
		switch ($tipo) {
			case 'assento':
				return $tags->bloco;
				break;
			
			case 'camarote':
				return $tags->camarote;
				break;
		}
	}
}
?>
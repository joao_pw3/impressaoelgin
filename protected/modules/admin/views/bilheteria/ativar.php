 <?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\modules\admin\assets\BilheteriaAsset;

BilheteriaAsset::register($this);
$this->title = 'ADM Estádio Mineirão :: Ativar ingresso';
$todosAtivos = true;

$form = ActiveForm::begin([
    'id' => 'form-ativar-ingresso',
]);
?>
<div class="container">
    
    <input type="hidden" id="url-check-venda" value="<?= Url::to(['/admin/bilheteria/check-venda']); ?>">
    <input type="hidden" id="url-check-carrinho" value="<?= Url::to(['/admin/bilheteria/check-carrinho']); ?>">
    <input type="hidden" id="url-ativar-ingresso" value="<?= Url::to(['/admin/bilheteria/ativar-ingresso']); ?>">
    
    <div class="row">
        <h2 class="tituloGeral"><i class="fa fa-clone"></i> Ativar ingresso - Temporada Mineirão Tribuna e Embaixada</h2>
    </div>

    <div id="localizar" class="row boxLer">
        <div class="ingresso col-md-6">
            <h3><?= $radioList[0] ?></h3>
            <?= $form->field($model, 'qrcode_voucher')->textInput(['id' => 'qrcode_voucher', 'tabindex' => '2', 'autofocus'=>'autofocus']); ?>
        </div>
        <div class="ingresso col-md-6">
            <h3><?= $radioList[1] ?></h3>
            <?= $form->field($model, 'documento')->textInput(['disabled' => true, 'id' => 'documento', 'tabindex' => '4']); ?>
        </div>
        <div class="col-md-12">
            <button type="submit" id="ativar-ingresso" class="btn btn-success"><i class="fa fa-check"></i> Buscar</button>                            
        </div>    
    </div>
    
    <br><hr class="bordaPont">
<?php ActiveForm::end(); ?>
    
    <div class="row">
        <div class="col-md-12" id="data-venda">
<?php
if (isset($resultadoBusca) && count($resultadoBusca) > 0) {
    foreach ($resultadoBusca as $venda) {
        if (isset($venda['objeto'])) {
?>
            <div class="row zeraesp tableResumo">
                <div class="col-xs-12" style="background:#ddd; padding-top:10px; padding-bottom:10px;">
                    <div class="col-xs-4 zeraesp text10">
                        <div class="col-md-3 zeraesp tableResumoBold">Comprador: </div>
                        <div class="col-md-9 zeraesp"><?= $venda['objeto']->vendas->comprador->nome ?></div>
                        <div class="col-md-3 zeraesp tableResumoBold">Documento: </div>
                        <div class="col-md-9 zeraesp"><?= $venda['objeto']->vendas->comprador->documento ?></div>
                        <div class="col-md-3 zeraesp tableResumoBold">Identificador: </div>
                        <div class="col-md-9 zeraesp"><?= $venda['objeto']->vendas->codigo ?></div>
                    </div>
                    <div class="col-xs-4 zeraesp text10">
                        <div class="col-md-3 tableResumoBold">Data: </div>
                        <div class="col-md-9"><?= $venda['objeto']->vendas->data ?></div>
                        <div class="col-md-3 tableResumoBold">Status: </div>
                        <div class="col-md-9<?= ($venda['objeto']->vendas->status == 'Autorizado' ? ' plus-green' : ' plus-red') ?>">
                            <?= $venda['objeto']->vendas->status; ?>
                        </div>
                        <div class="col-md-3 tableResumoBold">Total: </div>
                        <div class="col-md-9">R$ <?= number_format($venda['objeto']->vendas->valor, 2, ',', '.') ?></div>
                    </div>
                    <div class="col-xs-4 zeraesp text10 text-right">
                        <button type="button" class="btn btn-default btn-lg imprimir" onclick="window.open('<?= Url::to(['/admin/bilheteria/filipetas']); ?>?tipo=venda&codigo=<?= $venda['objeto']->vendas->codigo; ?>', 'filipeta', 'width=550,height=350,toolbar=no,scrollbars=no,location=no')">
                            <span class="glyphicon glyphicon-print"></span> Imprimir filipetas de todos
                        </button>
                    </div>
                </div>
            <?php 
            if (isset($venda['detalhes']) && count($venda['detalhes']>0)) {
                foreach ($venda['detalhes'] as $detalhe) { 
            ?>
                <div class="col-md-12">

                    <div class="tracejado">
                        <div class="col-xs-2" style="padding-left:0px;">
                            <?=$detalhe['evento']['nome'].' '.$model->documento?><br />
                            <img alt="<?= $detalhe['evento']['nome']?>" src="<?= $detalhe['evento']['banner']?>">
                        </div>

                        <div class="col-xs-2">
                            <?= $detalhe['produto']->nome ?><br>
                            <?= number_format($detalhe['produto']->unidades, 0)?> unidade <?= $detalhe['produto']->unidades > 1 ? 's' : ''?><br>

                            <?php
                            if($detalhe['produto']->valor!=''){
                                if ($detalhe['produto']->desconto->fixo > 0 || $detalhe['produto']->desconto->percentual > 0) { ?>
                                <span class="strikeDesconto">
                                R$ <?= number_format($detalhe['produto']->valor, 2, ',', '.') ?>
                                </span><br>
                                R$ <?= $detalhe['produto']->desconto->percentual > 0 
                                    ? number_format(floatval($detalhe['produto']->valor) - (floatval($detalhe['produto']->valor) * floatval($detalhe['produto']->desconto->percentual)), 2, ',' . '.') 
                                    : number_format($detalhe['produto']->valor - $detalhe['produto']->desconto->fixo, 2, ',', '.'); ?>
                                
                                <?php } else {?>
                                R$ <?= number_format($detalhe['produto']->valor, 2, ',', '.');
                                }
                            } ?>                                   
                        </div>
                        
                        <div class="col-xs-3">
                            Nome:<br>
                            <span><?= isset($detalhe['mineiraoCard']->nome->valor) ? $detalhe['mineiraoCard']->nome->valor : '' ?></span>
                        </div>
                        
                        <div class="col-xs-2">
                            Documento:<br>
                            <span><?= isset($detalhe['mineiraoCard']->doc->valor) ? $detalhe['mineiraoCard']->doc->valor : '' ?></span>
                        </div>
                        
                        <div class="col-xs-3 zeraesp text-right">
                    <?php
                    if (isset($detalhe['mineiraoCard']->doc->valor)) {
                        $docClienteVoucher = $model->localizar == 1 ? $venda['voucher'][2] : $model->documento;
                        $btnIngresso = $model->botaoIngresso($venda['objeto']->vendas->codigo, strtolower($docClienteVoucher), strtolower($venda['objeto']->vendas->comprador->documento), strtolower($detalhe['mineiraoCard']->doc->valor), $detalhe['evento']['id']);
                        $todosAtivos = $btnIngresso['ativo'];
                    ?>
                            <button type="button" class="btn btn-default imprimir" onclick="window.open('<?= Url::to(['/admin/bilheteria/filipetas']); ?>?tipo=produto&codigo=<?= $venda['objeto']->vendas->codigo; ?>&produto=<?= $detalhe['produto']->codigo; ?>&documento=<?= $detalhe['mineiraoCard']->doc->valor; ?>', 'filipeta', 'width=550,height=350,toolbar=no,scrollbars=no,location=no')">
                                <span class="glyphicon glyphicon-print"></span> Imprimir filipeta
                            </button>
                        <?php 
                        if ($btnIngresso['mostrar'] && $venda['objeto']->vendas->status == 'Autorizado') { ?>
                            <button type="button" id="salvar-<?= $detalhe['produto']->codigo; ?>" rel="<?= $detalhe['mineiraoCard']->doc->valor; ?>" class="salvar-ingresso btn-lg btn-success modal-ingresso" data-toggle="modal">INGRESSO</button>
                            <?php Modal::begin([
                                'header' => '<h4 align="center">Ingresso</h4>',
                                'size'   => 'modal-sm',
                                'id'     => 'modal-' . $detalhe['produto']->codigo,
                                'options'=> [
                                    'class' => 'modal-center'
                                ]
                            ]); ?>
                            <?= $this->render('salvar-form',[
                                'id_agenda' => $detalhe['evento']['id'],
                                'id_carrinho_produto' => $detalhe['produto']->id_carrinho,
                                'id_carrinho_id' => $detalhe['mineiraoCard']->doc->id_carrinho,
                                'qrcode_check' => $btnIngresso['qrcode'],
                                'venda' => $venda,
                                'detalhe' => $detalhe,
                                'id' => 'form-ingresso-' . $detalhe['produto']->codigo,
                                'hidden' => true,
                                'status' => isset($btnIngresso['status']) ? $btnIngresso['status'] : '',
                                'ocupante_nome' => $detalhe['mineiraoCard']->nome->valor,
                                'ocupante_documento' => $detalhe['mineiraoCard']->doc->valor
                            ]); ?>
                            <?php Modal::end(); ?>
                        <?php } ?>
                            <p class="text-center alert alert-<?= $btnIngresso['tipo']; ?> msg-<?= $detalhe['produto']->codigo; ?>"><?= $btnIngresso['msg']; ?></p>
                    <?php } ?>
                        </div>
                    </div>
                </div>
                <?php }
            } ?>
            </div>
            <hr>
        <?php 
        } else {
            echo '<p>Não há compra para as informações fornecidas.</p>';
        }
    }
} else { ?>
    <div class="alert alert-warning">Escolha um dos campos acima e forneça as informações para busca.</div>
<?php } ?>
        </div>
    </div>
    
    <div class="col-md-12<?= (count($resultadoBusca) > 0 && $todosAtivos) ? '' : ' hidden';?>" id="div-next">
        <button type="button" id="btn-restart" class="btn btn-success"><i class="fa fa-check"></i> Próximo cliente</button>                            
    </div> 
    
</div>

<!-- Modal - Mensagens -->
<?php Modal::begin([
    'header' => '<h4 align="center">Atenção</h4>',
    'size'   => 'modal-sm',
    'id'     => 'modal-mensagem',
    'options'=> [
        'class' => 'modal-center'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ]
]);
Modal::end(); ?>
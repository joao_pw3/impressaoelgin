<?php
namespace app\models;

use app\models\Api;

/**
 * Classe para Cobrança - Easy for Pay
 * obs.: parâmetros entre colchetes são opcionais
 */
class WsCobranca extends Api
{
    
    /**
     * Método construtor - obrigado a passar o token do vendedor para separação de unidades
    */
    public function __construct($tokenVendedor=''){
        if(empty($tokenVendedor))
            throw new \Exception("Token vendedor não foi declarado", 1);        
        $this->setToken($tokenVendedor,'V');
    }
    
    /**
     * Enviar cobrança
     * @param $post Dados de cobrança: trid, id_carrinho, documento, [email], [tipo], [validade], [forma], [descricao]
     * @return object 
     */
    public function enviarCobranca($post)
    {
        return json_decode($this->call('cobranca/', 'POST', $post));
    }
    
    /**
     * Identifica dados de uma cobrança
     * @param string $trid Id da transação
     */
    public function consultarCobranca($trid)
    {
        return json_decode($this->call('cobranca/' . $trid, 'GET'));
    }   
    
    /**
     * Autorizar pagamento. Não chamar o endpoint se for uma conciliação (venda por depósito, boleto ou TEF)
     * @param string $token Token do usuário
     * @param array $post Array de dados para autorizar pagamento: codigo (codigo_cobrança), codigo_cartao, senha_cartao
     * @return object
     */
    public function autorizarPagamento($token, $post)
    {
        $this->setToken($token, 'U');
        return json_decode($this->call('cobranca/' . $post['codigo'], 'PUT', [
            'codigo_cartao' => $post['codigo_cartao'], 
            'senha_cartao' => $post['senha_cartao']
        ], true, 'U'));
    }
    
    /**
     * Efetuar estorno de pagamento
     * @param integer $post Dados para estorno
     * @return object
     */
    public function estornarPagamento ($codigo, $post=[])
    {   
        return json_decode($this->call('vendas/estornar/' . $codigo, 'PUT', $post, true));
    }
      
    /**
     * Consultar vendas por período
     * @param string $dateStart Início do período
     * @param string $dateEnd Fim do período
     * @param string $token Token: 'U' = Usuário (cliente específico) / 'V' = Vendedor
     * @return object
     */
    public function consultarVenda($dateStart, $dateEnd, $token='V')
    {
        return json_decode($this->call('vendas/' . (empty($dateStart) && empty($dateEnd) ? 
                date('d-m-Yt00:00:00', strtotime('-30days')) . '/' . date('d-m-Yt23:59:59') :
                $dateStart . '/' . $dateEnd),
            'GET', NULL, true, $token));
    }
    
    /**
    * Consultar vendas com filtros
    * @param array $post Filtros para busca: data_ini, data_fim, descricao, operador, total, parcelas, trid, comprador, codigo
    * @param string $token V - loja
    * @return object
    */
    public function consultarVendaFiltro($post, $token='V')
    {
        return json_decode($this->call('vendas/', 'POST', $post, true, $token));
    }    
    
    /**
    * Consultar vendas por período trazendo ocupantes de assentos
    * @param string $dateStart Início do período
    * @param string $dateEnd Fim do período
    * @return object
    */
    public function consultarVendaOcupantes($dateStart, $dateEnd)
    {
        return json_decode($this->call('mineirao/vendas/' . (empty($dateStart) && empty($dateEnd) ? 
                date('d-m-Yt00:00:00', strtotime('-30days')) . '/' . date('d-m-Yt23:59:59') :  
                $dateStart . '/' . $dateEnd),
            'GET')
        );
    }

}
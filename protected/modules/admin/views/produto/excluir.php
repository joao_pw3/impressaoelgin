<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Produto - Excluir';
?>
<h1><span style="font-weight:200;">Produto</span> - Excluir</h1>
<h2><?=$produto->nome?></h2>
<p>Tem certeza que quer excluir esse produto? Esta ação não pode ser revertida</p>
<?php
$form = ActiveForm::begin(['id' => 'produto-excluir']); ?>

<?= $form->field($produto, 'codigo')->hiddenInput()->label(false) ?>

<div class="form-group">
    <?= Html::submitButton('Sim, excluir', ['class' => 'btn btn-warning']) ?>
    <?= Html::button('Voltar', ['class' => 'btn btn-default', 'onclick'=>'javascript:history.back()']) ?>
</div>

<?php ActiveForm::end(); ?>	
<?php
namespace app\modules\admin\assets;
use yii\web\AssetBundle;

class UsuariosAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/assets';
    
    public $css = [
    ];
    
    public $js = [
        'js/ajax.js',
        'js/usuarios.js',
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];
}

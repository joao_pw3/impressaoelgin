<?php
namespace app\models;
use Yii;

class Logs
{
    public $buffer;
    public $name;
    public $pasta;

    /**
     * construtor para Logs
     * 
     * @param type $name identificador do Log
     * @param type $pasta nome da pasta de armazenagem
     */
    public function __construct($name, $pasta) 
    {
        $this->name = $name;
        $pasta .= date('Ymd') . '/';
        if (!is_dir($pasta)) {
            mkdir($pasta);
        }
        $this->pasta = $pasta;
        $data = date('d/m/Y H:i:s');
        $this->buffer .= "{$data} | {$name}\r\n";
        $this->write();
    }

    /**
     * Registrar log em buffer
     * 
     * @param type $id identificador do Log
     * @param type $obj Objeto para armazenar no log
     */
    public function log($id, $obj = null) 
    {
        $data = date('H:i:s');
        $this->buffer .= "{$data} | {$id}\r\n";
        if ($obj) {
            $this->buffer .= str_replace("\n", "\r\n", print_r($obj, true)) . "\r\n";
        }
        $this->write();
    }

    /**
     * Registrar log em arquivo
     * 
     * @global type $_SESSION
     * @return boolean
     */
    private function write() 
    {
        $session = Yii::$app->session;
        $session->open();
        if (!$this->buffer) {
            return false;
        }
        if (isset($session['log_file'])) {
            $file = $session['log_file'];
        } else {
            $file = date('Y-m-d_His') . "_{$this->name}";
        }
        file_put_contents("{$this->pasta}log_{$file}.txt", str_replace("Array\r\n", "", $this->buffer), FILE_APPEND);
        $this->clear();
        return true;
    }

    /**
     * Formatar log em HTML
     * @return type
     */
    public function html() 
    {
        return str_replace("Array<br>", "", str_replace(" ", "&nbsp;", str_replace("\r\n", "<br>", $this->buffer)));
    }

    /**
     * Limpar log do buffer
     */
    public function clear() 
    {
        $this->buffer = '';
    }

}
<?php

use app\models\ApiProduto;
use app\modules\admin\assets\VendasAsset;
use yii\bootstrap\Modal;

VendasAsset::register($this);
$this->title = 'ADM Estádio Mineirão :: Reserva de assentos';
?>
<div class="col-md-12 col-sm-12 col-xs-12 nopadding">
    <div id="mapaVermelho">
        <div class="titArea row">
            <div class="col-md-6 zeraesp">
                <h4>Reservas / Bloqueios - Setor <?= $area == '' ? 'Indefinido' : ($area == 'Leste' ? 'Vermelho' : 'Roxo'); ?></h4>
            </div>
            <div class="col-md-6 zeraesp">
                <div class="legendaBl2">
                    <div><span></span></div>
                    <div>Bloqueado (fora do site)</div>
                    <div><span></span></div>
                    <div>Disponível</div>
                    <div><span></span></div>
                    <div>Vendido</div>
                    <div><span></span></div>
                    <div>Reservado (carrinho)</div>
                    <div><span></span></div>
                    <div>Reservado (cliente)</div>				
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <?php
            $option = '';
            if (!empty($setores)) {
                foreach ($setores as $a) {
                    $modelProduto = new ApiProduto;
                    // 31/01/2018 - MARCO: NO MOMENTO, NÃO FAREMOS PARA CAMAROTES
                    if (!isset($a->anexos) || $a->tags['tipo'] == 'camarote') {
                        continue;
                    }
                    $totais = $a->estoque;
                    if (isset($a->tags['bloco'])) {
                        $modelConsulta = new ApiProduto;
                        $consultaTotais = $modelConsulta->consultarTotaisEstoqueProduto([
                            'tags' => [
                                [
                                    "nome" => "matriz",
                                    "valor" => $a->tags['matriz']
                                ],
                                [
                                    "nome" => "tipo",
                                    "valor" => "assento"
                                ],
                                [
                                    "nome" => "bloco",
                                    "valor" => $a->tags['bloco']
                                ]
                            ],
                        ]);
                        if ($consultaTotais->successo == 1) {
                            $totais = $consultaTotais->objeto;
                        }
                    }
                    $option .= '<option value="' . $a->codigo . '" id="' . $a->tags['tipo'] . '_' . ($a->tags['tipo'] == 'bloco' ? $a->tags['bloco'] : $a->tags['camarote']) . '"' . (intval($totais->oferecido) == 0 ? ' disabled' : '') . ' class="' . ($a->tags['tipo'] == 'bloco' ? 'bloco' : 'camarote') . ($a->ativo == '0' ? ' plus-red' : ' plus-green') . '">' .
                            ($a->tags['tipo'] == 'bloco' ? $a->tags['bloco'] : $a->tags['camarote']) .
                            ($a->ativo == '0' ? ' (Bloqueado)' : ' (Desbloqueado)') . ' | ' .
                            number_format(intval($totais->oferecido), 0) . ' (Total) = ' .
                            number_format(intval($totais->vendido), 0) . ' (Vendido) + ' .
                            number_format(intval($totais->reservado), 0) . ' (Res.carrinho) + ' .
                            (isset($totais->ativos->disponivel) ? number_format(intval($totais->ativos->disponivel), 0) : '0') . ' (Disp./Res.cliente) + ' .
                            (isset($totais->inativos->disponivel) ? number_format(intval($totais->inativos->disponivel), 0) : '0') . ' (Bloqueado)' .
                            '</option>';
                }
            }
            if ($option != '') {
                ?>
                <div class="boxBorder">
                    <h4 style="text-align:center;">Blocos disponíveis</h4><br>
                    <select id="blocos">
                        <option value="">Selecione um bloco</option>
                        <?= $option; ?>
                    </select>
                </div>
            <?php } else { ?>
                Não há informações para este evento.    
            <?php } ?>
        </div>
    </div>
    <br>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 about-right esconde <?= (!isset($this->context->setor)) ? 'wow fadeInRight animated' : ''; ?> textosAprest3" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;" id="boxAssentos">
    <div id="map-div"></div>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 1000" preserveAspectRatio="xMidYMin slice" id="map-svg"></svg>
</div>

<!-- Modal - Reservas -->
<?php
Modal::begin([
    'header' => '<h2 align="center"><span class="res-produto"></span></h2>',
    'size' => 'modal-md',
    'id' => 'modal-reserva',
    'options' => [
        'class' => 'modal-center'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ]
]);
?>
<form id="form-reserva" method="post">
    <input type="hidden" id="id_evento" name="id_evento" value="<?= isset($evento->codigo) ? $evento->codigo : ''; ?>">
    <input type="hidden" id="reserva-codigo" name="codigo">
    <input type="hidden" id="reserva-produto" name="produto">
    <input type="hidden" id="reserva-status" name="status">
    <input type="hidden" id="reserva-disponivel" name="disponivel">
    <input type="hidden" id="reserva-reservado" name="reservado">
    <input type="hidden" id="reserva-vendido" name="vendido">
    <input type="hidden" id="reserva-documento" name="documento">
    <div class="content">
        <!-- <h2 align="center"><span id="res-produto"></span></h2>
        <hr> -->
        <div class="row">
            <div class="col-md-6">
                <label>Reservado (cliente): <span id="res-cliente">não</span></label>
                <label>Status: <span id="res-status"></span></label>
                <label>Oferecido: <span id="res-oferecido"></span></label>
            </div>
            <div class="col-md-6">
                <label>Disponível: <span id="res-disponivel"></span></label>
                <label>Vendido: <span id="res-vendido"></span></label>
                <label>Reservado (carrinho): <span id="res-reservado"></span></label>
            </div>
        </div>
        <div class="boxCz2">
            <div class="row">
                <div class="col-md-12">
                    <label>Alterações no assento <span class="res-produto"></span>. Entre com a justificativa:</label>
                    <textarea name="justificativa" id="reserva-justificativa" rows="3" cols="80" class="padraoTexta"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 reserva-border">
                    <label align="center">Status</label>
                    <div class="col-md-12" align="center">
                        <button type="button" class="btn btn-primary cli-cartao" id="btn-status">Bloquear / Desbloquear</button>
                    </div>
                    <div class="col-md-12 reserva-div zeraesp" id="historico-bloqueio"></div>
                </div>
                <div class="col-md-6">
                    <label align="center">Reserva</label>
                    <div class="col-md-5" style="font-size:10px;">DOC./ID operador</div>
                    <div class="col-md-7"><input type="text" name="reserva-documento" id="res-documento" value="<?= Yii::$app->user->identity->cod_operador;  ?>" class="padraoInput" style="height:20px;"></div>
                    <div class="col-md-12" align="center">
                        <button type="button" class="btn btn-primary cli-cartao" id="btn-reservar">Reservar assento</button>
                    </div>
                    <div class="col-md-12 reserva-div zeraesp" id="historico-reserva"></div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php Modal::end(); ?>

<!-- Modal - Confirmar Documento -->
<?php Modal::begin([
    'header' => '<h4 align="center">Atenção</h4>',
    'size' => 'modal-sm',
    'id' => 'modal-confirmar',
    'options' => [
        'class' => 'modal-center'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ],
    'footer' => '
        <div class="col-md-12" align="center">
            <button type="button" class="btn btn-danger" id="btn-cancel">Cancelar</button>
            <button type="button" class="btn btn-success" id="btn-continue">Continuar</button>
        </div>'
]);
Modal::end(); ?>
 <?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Html;
use app\modules\admin\assets\MuseuAsset;
use kartik\money\MaskMoney;

MuseuAsset::register($this);

?>
<div class="container">
    <div class="row">

        <?php if(isset($message) && !is_null($message)) { ?>
        <div class="col-md-12">
            <div class="alert alert-warning"><?php echo $message; ?></div>
        </div>
        <?php } ?>

        <div class="col-md-3">
            <?php $form = ActiveForm::begin(['options'=>['id'=>'museu-registra_venda']]) ?>
            <p><label>Valor Total: </label></p>
            <?=  MaskMoney::widget([
                    'name' => 'total',
                    'pluginOptions' => [
                        'prefix' => 'R$ ',
                        'thousands' => '.',
                        'decimal' => ',',
                        'allowNegative' => false
                    ],
                    'options' => [
                        'placeholder' => 'R$ 0,00'
                    ]
                ]); 
            ?>            
            <button class="btn btn-primary" type="submit">Fechar Pedido</button>
            <?php ActiveForm::end()?>
            <a class="btn btn-default" href="<?=Url::to(['registra-vendas','cancelar'=>1])?>">Nova operação</a>
            <a class="btn btn-danger" href="<?=Url::to(['vendas', 'todas' => 0])?>">Cancelar venda</a>
            <hr>
            <p id="cappta-status"></p>
        </div>
        <div class="col-md-9">
            <?php if($total > 0){?>
                <div id="retorno-carrinho">
                   
                    <p class="alert alert-info">Total a pagar: R$ <?=number_format($total,2,',','.')?></p>
                    <p>Confirme os dados acima e selecione a forma de pagamento</p>
                    <?php
                        $this->registerJs("
                            capptaInit();
                        ",View::POS_READY);?>
                </div>
                <div id="cappta">
                    <p>Forma de pagamento</p>
                    <?php ActiveForm::begin(['options'=>['id'=>'museu-finaliza-venda-form']]);
                    echo Html::radioList('tipo', '1',['debito' => 'Débito','credito' => 'Crédito']);
                    echo Html::dropDownList('parcelas',null,$parcelas,['class'=>'hidden']);
                    echo Html::hiddenInput('total', $total);
                    echo Html::button('CONFIRMAR',['class'=>'btn btn-primary','type'=>'submit']);
                    ActiveForm::end()?>
                </div>
            <?php } ?>

            <div class="impressao_recibo hide" style="padding: 18px 0;">
                <button id="reciboConsumidor" class="btn btn-success" data-recibo onclick="printRecibo(this)">Recibo Consumidor <span class="glyphicon glyphicon-print"></span></button>
                <button id="reciboEstabelecimento" class="btn btn-warning" data-recibo onclick="printRecibo(this)">Recibo Estabelecimento <span class="glyphicon glyphicon-print"></span></button>

                <div class="alert alert-info">
                    <p>Após a impressão dos recibos, clique em "Nova operação"</p>
                </div>
            </div>
        </div>
    </div>
</div>
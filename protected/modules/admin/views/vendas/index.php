<?php
use \kartik\date\DatePicker;
use app\modules\admin\assets\VendasAsset;

VendasAsset::register($this);
$this->title = 'ADM Estádio Mineirão :: Relatório de vendas';
?>
<div class="container">
    <h2 class="tituloGeral"><i class="fa fa-clone"></i> Relatório de Vendas</h2>
	<?= \yii\helpers\Html::beginForm('','post',['class'=>'form-inline'])?>
		<div class="form-group">
			<label>Data inicial:</label>
			<?= DatePicker::widget([
				'name'  => 'de',
				'value'  => date('d/m/Y',$de->getTimestamp()),
				'language' => 'pt-BR',
				'pluginOptions'=>[
					'format' => 'dd/mm/yyyy'
				]
			]);?>
		</div>
		<div class="form-group">
			<label>Data final:</label>
			<?= DatePicker::widget([
				'name'  => 'ate',
				'value'  => date('d/m/Y',$ate->getTimestamp()),
				'language' => 'pt-BR',
				'pluginOptions'=>[
					'format' => 'dd/mm/yyyy'
				]
			]);?>
		</div>
		<div class="form-group">
			<button class="btn btn-default" type="submit">Buscar</button>
		</div>
	<?= \yii\helpers\Html::endForm()?>
	<p class="text-right">
		<button id="vendas-mostrar-tudo-btn" type="button" class="btn btn-default">Mostrar transações não autorizadas</button>
		<a class="btn btn-primary" href="<?=\yii\helpers\Url::to(['exportar-csv'])?>">Exportar para CSV</a>
	</p>
	<table id="tabela-vendas-transacoes" class="table table-responsive table-hover">
		<thead>
			<tr>
				<th>#</th>
				<th>Data</th>
				<th>Nome</th>
				<th>Documento</th>
				<th>E-mail</th>
				<th>Telefone</th>
				<th>Total</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$consolidadoDia=0;
			$linha=0;
			for($i=0; $i<$total ; $i++) {
				if($transacoes[$i]['codigo_status'] == 1){
					$consolidadoDia+=$transacoes[$i]['bruto'];
					$linha++;
				}
				?>
			<tr class="linhaTransacao <?=$transacoes[$i]['codigo_status']!=1 ?'hidden nao-processada' :''?>">
				<td><?=$transacoes[$i]['codigo_status'] == 1 ?$linha :'-'?></td>
				<td><?=$transacoes[$i]['dataTransacao']->format('d/m/Y H:i:s')?></td>
				<td><?=$transacoes[$i]['comprador']['nome']?></td>
				<td><?=$transacoes[$i]['comprador']['documento']?></td>
				<td><?=$transacoes[$i]['comprador']['email']?></td>
				<td><?=$transacoes[$i]['comprador']['telefone']?></td>
				<td>R$ <?=number_format($transacoes[$i]['bruto'],2,',','.')?> (<?=$transacoes[$i]['parcelas']?>)</td>
				<td><?=$transacoes[$i]['status']?> <?=$transacoes[$i]['codigo']?></td>
			</tr>
			<?php
			if(\app\modules\admin\models\Transacoes::linhaConsolidado($transacoes,$i)){?>
				<tr class="tabela-linha-consolidado">
					<td colspan="6">Subtotal do dia <?=$transacoes[$i]['dataTransacao']->format('d/m/Y')?></td>
					<td colspan="2">R$ <?=number_format($consolidadoDia,2,',','.')?></td>
				</tr>
				<?php
				$consolidadoDia=0;			
			}
		}?>
		</tbody>
	</table>
</div>
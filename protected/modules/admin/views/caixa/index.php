<?php
use kartik\money\MaskMoney;
use yii\helpers\Url;
use app\assets\CaixaAsset;
use yii\web\View;

CaixaAsset::register($this);
$this->title = 'Caixa - Resumo';
?>
<div class="admin-caixa">
    <h1 style="margin-bottom:0px;"><span style="font-weight:200;">Caixa</span> - Resumo <button type="button" class="btn btn-primary btnCaixStatus" id="botCxStatus" title="Status do Caixa"><span class="font-awesome" style="font-size:12px;">&#xf1ec;</span> Situação</button></h1>
    <div style="width:100%; position:relative;">
        <div class="botCxStatusSt" id="botCxStatusSt">
            <div class="<?= isset($caixa->resumo['abertura']) ? 'plus-green' : 'plus-red'; ?>" style="text-align:center;">
                <span style="font-size:16px; font-weight:900;">
                    Caixa <?= isset($caixa->resumo['abertura']) ? 'aberto' : 'fechado'; ?>
                </span>
                <?= Yii::$app->user->identity->usuario ? Yii::$app->user->identity->usuario : ''; ?>
            </div>
            <br>Valor <?= isset($caixa->resumo['abertura']) ? 'de abertura' : (isset($caixa->resumo['fechamento']) ? 'de fechamento' : 'sem movimento'); ?>: 
            R$ <strong><?= isset($caixa->resumo['abertura']['valor']) ? number_format((float)$caixa->resumo['abertura']['valor'], 2, ',', '.') : 
                          (isset($caixa->resumo['fechamento']['valor']) ? number_format((float)$caixa->resumo['fechamento']['valor'], 2, ',', '.') : '0,00'); ?></strong>.<br>
            <?= isset($caixa->resumo['abertura']) ? 'Aberto' : (isset($caixa->resumo['fechamento']) ? 'Fechado' : 'Abra o caixa abaixo'); ?> 
            <?= isset($caixa->resumo['abertura']) ? 'em ' . $caixa->resumo['abertura']['data'] : 
            (isset($caixa->resumo['fechamento']['data']) ? 'em ' . $caixa->resumo['fechamento']['data'] : ''); ?>
            <?php if (isset($caixa->resumo['abertura']) && $dataControle < date('YmdHis')) { ?>
            <br><span class="plus-red">O caixa está aberto há mais de 12 horas. Feche-o e abra-o novamente.</span>
            <?php } ?>
        </div>
    </div>

    <form id="form-caixa" method="post">
    <input type="hidden" id="url-acao" value="<?= Url::to(['caixa/acao-caixa']); ?>">
    <input type="hidden" id="url-operacao" value="<?= Url::to(['caixa/index']); ?>">
    <input type="hidden" id="url-relatorio" value="<?= Url::to(['caixa/salvar-relatorio']); ?>">
    <input type="hidden" name="id_vendedor" id="id_vendedor" value="<?= Yii::$app->user->identity->cod_operador; ?>">
    <input type="hidden" name="saida" id="saida" value="">
    <input type="hidden" name="ajuste" id="ajuste" value="">
    <input type="hidden" name="valor" id="valor" value="">
    
    <div class="content">
        <div class="row">
            <div class="col-md-offset-7 col-md-5" id="lbl-caixa"></div>
        </div>
        <div class="bordArredondBilhet3">
            <table id="tabela-caixa" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th scope="col" width="20%">Operação</th>
                    <th scope="col" width="30%">Valor atual</th>
                    <th scope="col" width="25%">Atualizar</th>
                    <th scope="col" width="25%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td width="20%">Abertura</td>
                    <td width="30%" align="right" id="td-abertura">
                        <?php if (!isset($caixa->resumo['abertura'])) { ?>
                        R$ <?= number_format((float)$saldo, 2, ',', '.'); ?> (saldo anterior)
                        <?php } else { ?>
                        R$ <?= number_format((float)$saldo, 2, ',', '.'); ?> (saldo anterior) + R$ <?= number_format((float)$caixa->resumo['abertura']['valor'], 2, ',', '.'); ?>
                        <input type="hidden" id="caixa-aberto" value="<?= $caixa->resumo['abertura']['valor']; ?>">
                        <?php } ?>
                    </td>
                    <td width="25%"><?= MaskMoney::widget([
                        'name' => 'caixa-abrir',
                        'value' => 0,
                        'pluginOptions' => [
                            'prefix' => 'R$ ',
                            'thousands' => '.',
                            'decimal' => ',',
                            'allowNegative' => false
                        ],
                        'options' => [
                            'id' => 'caixa-abrir',
                            'placeholder' => 'Valor inicial do caixa em R$',
                            'title' => 'Valor inicial do caixa em R$',
                            'class' => 'form-control',
                            'disabled' => isset($caixa->resumo['abertura']) ? true : false
                        ]
                    ]); ?></td>
                    <td width="25%" align="center"><button type="button" class="btn btn-primary" id="btn-abrir"<?= isset($caixa->resumo['abertura']) ? ' disabled' : ''; ?>>+</button></td>
                </tr>
                <tr>
                    <td width="20%">Reforço</td>
                    <td width="30%" align="right" class="plus-green">
                        R$ <?= isset($caixa->resumo['reforço']) ? number_format($caixa->resumo['reforço'], 2, ',', '.') : '0,00'; ?>
                        <input type="hidden" id="caixa-reforco" value="<?= isset($caixa->resumo['reforço']) ? $caixa->resumo['reforço'] : 0; ?>">
                    </td>
                    <td width="25%"><?= MaskMoney::widget([
                        'name' => 'caixa-reforco',
                        'value' => 0,
                        'pluginOptions' => [
                            'prefix' => 'R$ ',
                            'thousands' => '.',
                            'decimal' => ',',
                            'allowNegative' => false
                        ],
                        'options' => [
                            'id' => 'caixa-reforco',
                            'placeholder' => 'Valor em R$',
                            'title' => 'Valor em R$',
                            'class' => 'form-control',
                            'disabled' => !isset($caixa->resumo['abertura']) ? true : false
                        ]
                    ]); ?></td>
                    <td width="25%" align="center"><button type="button" class="btn btn-primary" id="btn-reforco"<?= !isset($caixa->resumo['abertura']) ? ' disabled' : ''; ?>>+</button></td>
                </tr>
                <tr>
                    <td width="20%">Sangria</td>
                    <td width="30%" align="right" class="plus-red">
                        R$ <?= isset($caixa->resumo['sangria']) ? number_format($caixa->resumo['sangria'], 2, ',', '.') : '0,00'; ?>
                        <input type="hidden" id="caixa-sangria" value="<?= isset($caixa->resumo['sangria']) ? $caixa->resumo['sangria'] : 0; ?>">
                    </td>
                    <td width="25%"><?= MaskMoney::widget([
                        'name' => 'caixa-sangria',
                        'value' => 0,
                        'pluginOptions' => [
                            'prefix' => 'R$ ',
                            'thousands' => '.',
                            'decimal' => ',',
                            'allowNegative' => false
                        ],
                        'options' => [
                            'id' => 'caixa-sangria',
                            'placeholder' => 'Valor em R$',
                            'title' => 'Valor em R$',
                            'class' => 'form-control',
                            'disabled' => !isset($caixa->resumo['abertura']) ? true : false
                        ]
                    ]); ?></td>
                    <td width="25%" align="center"><button type="button" class="btn btn-primary" id="btn-sangria"<?= !isset($caixa->resumo['abertura']) ? ' disabled' : ''; ?>>-</button></td>
                </tr>
                <tr>
                    <td width="20%">Fechamento</td>
                    <td width="30%" align="right"><span id="tbl-fechamento"></span></td>
                    <td width="25%">
                        <?= MaskMoney::widget([
                            'name' => 'caixa-fechar',
                            'value' => 0,
                            'pluginOptions' => [
                                'prefix' => 'R$ ',
                                'thousands' => '.',
                                'decimal' => ',',
                                'allowNegative' => false
                            ],
                            'options' => [
                                'id' => 'caixa-fechar',
                                'placeholder' => 'Fechamento em R$',
                                'title' => 'Fechamento em R$',
                                'class' => 'form-control',
                                'disabled' => !isset($caixa->resumo['abertura']) ? true : false
                            ]
                        ]); ?>
                    </td>
                    <td width="25%" align="center"><button type="button" class="btn btn-primary" id="btn-fechar"<?= !isset($caixa->resumo['abertura']) ? ' disabled' : ''; ?>>Fechar</button></td>
                </tr>
                <tr>
                    <td colspan="4">
                        Vendas: Crédito (<?= isset($vendas->vendas['Autorizado']['Cartão de crédito']) ? 
                        'R$ ' . number_format($vendas->vendas['Autorizado']['Cartão de crédito']['valor'], 2, ',', '.') . ' - ' . 
                        $vendas->vendas['Autorizado']['Cartão de crédito']['unidades'] . ' unidade' . 
                        ($vendas->vendas['Autorizado']['Cartão de crédito']['unidades'] > 1 ? 's' : '') : 'Nenhuma'; ?>) / 
                        Débito (<?= isset($vendas->vendas['Autorizado']['Cartão de débito']) ? 
                        'R$ ' . number_format($vendas->vendas['Autorizado']['Cartão de débito']['valor'], 2, ',', '.') . ' - ' . 
                        $vendas->vendas['Autorizado']['Cartão de débito']['unidades'] . ' unidade' . 
                        ($vendas->vendas['Autorizado']['Cartão de débito']['unidades'] > 1 ? 's' : '') : 'Nenhuma'; ?>)
                    </td>
                </tr>
            </tbody>
            </table>
        </div>
        
        <div class="hidden" id="div-resumo">
            <img src="<?= Url::to(['/'], true); ?>images/logoMineirap.png"><br><br>
            Resumo de caixa: <strong><?= Yii::$app->user->identity->usuario; ?></strong><br>
            <br>
            <div class="row">
                <div class="col-xs-3"><strong>Operação</strong></div>
                <div class="col-xs-3"><strong>Valor</strong></div>
            </div>
            <div class="row">
                <div class="col-xs-3">Abertura</div>
                <div class="col-xs-3" id="resumo-abertura" align="right"></div>
            </div>
            <div class="row">
                <div class="col-xs-3">Reforço</div>
                <div class="col-xs-3" id="resumo-reforco" align="right"></div>
            </div>
            <div class="row">
                <div class="col-xs-3">Sangria</div>
                <div class="col-xs-3" id="resumo-sangria" align="right"></div>
            </div>
            <div class="row">
                <div class="col-xs-3">Saldo</div>
                <div class="col-xs-3" id="resumo-saldo" align="right"></div>
            </div><br>
        </div>
        
        <div class="hidden" id="div-fechamento">
            <img src="<?= Url::to(['/'], true); ?>images/logoMineirap.png"><br><br>
            Relatório de caixa: <strong><?= Yii::$app->user->identity->usuario; ?></strong><br>
            <?php if (isset($caixa->resumo['abertura']['data'])) { ?>
            Caixa aberto em <?= $caixa->resumo['abertura']['data']; ?><br>
            <?php } ?>
            Fechamento em <?= date('d/m/Y H:i:s'); ?><br><br>
            <br>
            <table width="60%">
                <thead>
                    <th><strong>Operação</strong></th>
                    <th><strong>Valor</strong></th>
                </thead>
                <tbody>
                    <tr>
                        <td>Abertura (saldo anterior: R$ <?= number_format($saldo, 2, ',', '.'); ?>)</td>
                        <td id="vlr-abertura" align="right">R$ <?= number_format(isset($caixa->resumo['abertura']['valor']) ? (float)$caixa->resumo['abertura']['valor'] : 0, 2, ',', '.'); ?></td>
                    </tr>
                    <tr>
                        <td>Reforço</td>
                        <td id="vlr-reforco" align="right"></td>
                    </tr>
                    <tr>
                        <td>Sangria</td>
                        <td id="vlr-sangria" align="right"></td>
                    </tr>
                    <tr>
                        <td>Vendas (espécie)</td>
                        <td align="right">
                            R$ <?= isset($vendas->vendas['Autorizado']['Espécie']) ? number_format($vendas->vendas['Autorizado']['Espécie']['valor'], 2, ',', '.') : '0,00'; ?>
                            <input type="hidden" id="vlr-venda" value="<?= isset($vendas->vendas['Autorizado']['Espécie']) ? $vendas->vendas['Autorizado']['Espécie']['valor'] : 0; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>Estornos (espécie)</td>
                        <td align="right">
                            R$ <?= isset($vendas->vendas['Estornado']['Espécie']) ? number_format($vendas->vendas['Estornado']['Espécie']['valor'], 2, ',', '.') : '0,00'; ?>
                            <input type="hidden" id="vlr-estorno" value="<?= isset($vendas->vendas['Estornado']['Espécie']) ? $vendas->vendas['Estornado']['Espécie']['valor'] : 0; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>Fechamento</td>
                        <td id="vlr-fechamento" align="right"></td>
                    </tr>
                    <tr>
                        <td>Saldo</td>
                        <td id="vlr-saldo" align="right"></td>
                    </tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td align="center">___________________________________<br>supervisor</td>
                        <td align="center">___________________________________<br><?= Yii::$app->user->identity->usuario; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        
        <div class="row">
            <div class="col-xs-12" align="center">
                <a href="<?= Url::to(['relatorio-vendas']); ?>" class="btn btn-primary">Ver detalhes</a>
                <button type="button" class="btn btn-primary" id="btn-print-resumo">Imprimir resumo</button>
                <a href="<?= Url::to(['exportar-csv-resumo']); ?>" class="btn btn-primary">CSV resumo</a>
            </div>
        </div>
        
    </div>
    </form>
</div>
<?php 
if (isset($caixa->resumo['abertura']) && $dataControle < date('YmdHis')) {
    $this->registerJs("
        $('#botCxStatus').click();
    ", View::POS_READY);
}
?>
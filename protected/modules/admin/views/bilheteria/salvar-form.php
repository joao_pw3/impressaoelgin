<?php
use yii\helpers\Url;
if (!empty($status) && $status == 'L') { ?>
<form method="post" id="<?= $id; ?>" class="form <?= $hidden ? 'hidden' : ''; ?>" action="<?= Url::to(['retirar-cartao']); ?>">
    <p>Entregar cartão para:<br />
        Comprador: <?= $venda['objeto']->vendas->comprador->nome; ?> <strong>(<?= $venda['objeto']->vendas->comprador->documento; ?>)</strong><br />
        Ocupante: <?= $ocupante_nome; ?> <strong>(<?= $ocupante_documento; ?>)</strong><br />
        Terceiro: <strong>Exigir Declaração de Autorização</strong>
    </p>
    <fieldset disabled="disabled" class="formIngressoCz">
        <p>Leia o Qrcode do cartão</p>
        <input type="text" name="qrcode" size="16" maxlength="16" id="<?= $id; ?>-qrcode" autofocus>
        <p id="retira-<?= $id; ?>">
            Quem está retirando o ingresso?<br>
            <input type="radio" name="retira" class="radio-retira" value="c" id="retiraComprador"><label class="label-opcaoRetira" for="retiraComprador">comprador</label>
            <input type="radio" name="retira" class="radio-retira" value="o" id="retiraOcupante"><label class="label-opcaoRetira" for="retiraOcupante">ocupante</label>
            <input type="radio" name="retira" class="radio-retira" value="t" id="retiraTerceiro"><label class="label-opcaoRetira" for="retiraTerceiro">terceiro</label>
        </p>
        <input type="text" name="retira-nome" id="retira-nome" value="" placeholder="Nome de quem retira">
        <input type="text" name="retira-documento" id="retira-documento" placeholder="Documento de quem retira">
        <input type="hidden" name="id_agenda" value="<?=$id_agenda?>">
        <input type="hidden" name="id_carrinho_produto" value="<?=$id_carrinho_produto?>">
        <input type="hidden" name="id_carrinho_id" value="<?=$id_carrinho_id?>">
        <input type="hidden" id="qrcode_check" value="<?= $qrcode_check; ?>">
        <input type="hidden" id="comprador-nome" value="<?= $venda['objeto']->vendas->comprador->nome; ?>">
        <input type="hidden" id="comprador-documento" value="<?= $venda['objeto']->vendas->comprador->documento; ?>">
        <input type="hidden" id="ocupante-nome" value="<?= $ocupante_nome; ?>">
        <input type="hidden" id="ocupante-documento" value="<?= $ocupante_documento; ?>">
        
        <p class="info-salvar"></p>
        <input type="button" class="btn btn-primary btn-salvar btn-salvar-<?= $detalhe['produto']->codigo; ?> btnAzEscuro" id="<?= $detalhe['produto']->codigo; ?>" value="Salvar" />
    </fieldset>
</form>
 <?php } ?>
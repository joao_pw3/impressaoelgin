<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\ApiProduto;

$session = Yii::$app->session;
$session->open();
?>
<div class="bloco" id="bloco">
    
    <input type="hidden" id="url-add-cart" value="<?= Url::to(['site/add-carrinho']); ?>">
    <input type="hidden" id="url-index" value="<?= Url::to(['site/index']); ?>">
    <input type="hidden" id="url-resumo" value="<?= Url::to(['site/resumo']); ?>">
    <input type="hidden" id="matriz-produtos" value="<?= $evento->tags['apelido']; ?>">
    <input type="hidden" id="area-estadio" value="<?= $this->context->area ?>">
    <input type="hidden" id="url-get-produto" value="<?= Url::to(['site/produto-id']); ?>">
    <input type="hidden" id="url-get-produto-tags" value="<?= Url::to(['site/produto-por-tag']); ?>">
    <input type="hidden" id="url-bloco-assento" value="<?= Url::to(['site/get-bloco-assento']); ?>">
    
    <div class="container">
        
        <div style="margin-bottom:25px;">
            <div class="col-md-6 about-left wow fadeInLeft animated" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                <?= Html::img($evento->anexos['bannervitrine']); ?>
            </div>
            <div class="col-md-4 about-right wow fadeInRight animated textosAprest" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                <div><?= $evento->nome; ?></div>                    
                    <?php if ($evento->tags['apelido']!='temporada2018') { 
                    $objData=\DateTime::createFromFormat('d/m/Y H:i:s',$evento->data->inicial);
                    $diaSemana=['','Seg','Ter','Qua','Qui','Sex','Sab','Dom'];
                    ?>
                    <div>
                        <?= $diaSemana[$objData->format('N')].' '.$objData->format('d/m') ?> - 
                        Horário: <?= $objData->format('H'); ?>h
                    </div>
                    <?php } ?>                  
                <?= $evento->descricao; ?>
            </div>
            <hr style="padding-top:25px;">
        </div>        
        <div id="mapaPjax">
            <input type="hidden" id="url-add-cart" value="<?= Url::to(['site/add-carrinho']); ?>">
            <input type="hidden" id="url-del-cart" value="<?= Url::to(['site/del-carrinho']); ?>">
            <input type="hidden" id="url-resumo" value="<?= Url::to(['site/resumo']); ?>">
            <input type="hidden" id="url-visitante" value="<?= Url::to(['site/visitante']); ?>">
            <input type="hidden" id="user" value="<?= isset($session['user_token']) && !empty($session['user_token']) ? 1 : 0; ?>">

            <div class="col-md-8 col-sm-12 col-xs-12 nopadding">
                <div id="mapaRoxo">
                    <div class="titArea">
                        <h4>Arena Mineirão - Área Oeste</h4>
                        Escolha o setor que você deseja comprar (passe o mouse sobre o mapa e clique para selecionar)<br><br>
                    </div>

                    <div class="fundoEstadio">
                        <div class="campoDiv">
                            <?= Html::img('@web/images/campo2.jpg', ["class" => "campoP"]); ?>
                        </div>
                        <?php
                        $modelProduto=new ApiProduto;
                        $mapa='';
                        foreach($setores as $a){
                            if(!isset($a->anexos)) continue;
                            $setor = $a->tags['tipo'].'_'.($a->tags['tipo']=='bloco' ? $a->tags['bloco'] : $a->tags['camarote']);
                            $anexo = $modelProduto->findAnexo($a->anexos, 'svgmapa');
                            if ($this->context->setor!=null && $this->context->setor==$setor) {
                                $anexo = str_replace('class="', 'class="selecionado ', $anexo);
                            }
                            $anexo=str_replace('<path ', '<path rel="'.$a->codigo.'" ', $anexo);
                            $mapa .= $anexo;
                            echo Html::input('hidden','oferecido',number_format(floatval($a->estoque->oferecido), 0),['id'=>'oferecido_'.$setor]);
                            echo Html::input('hidden','disponivel',number_format(floatval($a->estoque->disponivel), 0),['id'=>'disponivel_'.$setor]);
                        }
                        ?>
                        <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="100%" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                             viewBox="0 0 2480 707"
                             xmlns:xlink="http://www.w3.org/1999/xlink" id="lugares" class="oeste">  
                            <path class="fil0 str0" d="M1240 490l-1 0 -1 0 0 0c-345,0 -663,-85 -916,-226l27 -52 -34 -60 169 -151c213,90 458,143 721,147l0 51 33 0 2 0 33 0 0 -51c263,-4 508,-57 721,-147l169 151 -34 60 27 52c-253,142 -571,226 -916,226l0 0z"/>
                            <?php echo $mapa ?>
                            <path class="fil4 str1 arquibancada" d="M89 339l110 -158c270,187 636,303 1040,303l0 0 0 0 0 0 0 0c404,0 770,-116 1040,-303l109 157c-801,473 -1566,453 -2301,2z"/>
                            <path class="fil3 str1 arquibancada" d="M1240 484l0 201"/>
                            <path class="fil3 str1 arquibancada" d="M266 439l93 -162"/>
                            <path class="fil3 str1 arquibancada" d="M449 525l76 -172"/>
                            <path class="fil3 str1 arquibancada" d="M638 594l59 -183"/>
                            <path class="fil3 str1 arquibancada" d="M833 645l41 -193"/>
                            <path class="fil3 str1 arquibancada" d="M1032 675l22 -200"/>
                            <path class="fil3 str1 arquibancada" d="M2028 519l-74 -167"/>
                            <path class="fil3 str1 arquibancada" d="M1840 588l-57 -177"/>
                            <path class="fil3 str1 arquibancada" d="M1646 639l-40 -188"/>
                            <path class="fil3 str1 arquibancada" d="M1448 672l-21 -197"/>
                            <path class="fil3 str1 arquibancada" d="M2212 435l-90 -158"/>
                        </svg>
                    </div>
                    <div class="titArea oesteInfo">
                        <div><?= Html::img('@web/images/mapa_peq.png', ["class" => "campoP2"]); ?></div>
                        <div>
                            <ul>
                                <li>Está com dúvida de como fazer sua compra? Clique Aqui.</li>
                                <li>Camarotes da Área Oeste, veja aqui as fotos antes de comprar.</li>
                                <li>As tribunas do Mineirão dão visão privilegiadas do campo, saiba mais.</li>
                            </ul>
                        </div>
                        <div>
                            Vista lado<br>esquerdo<br>
                            <i class="fa fa-camera tam30 flipHor" aria-hidden="true" onclick="vistaOesteEsquerda()"></i>
                        </div>
                        <div>
                            Vista lado<br>direito<br>
                            <i class="fa fa-camera tam30 flipHor" aria-hidden="true" onclick="vistaOesteDireita()"></i>
                        </div>
                    </div>
                </div>
                <br>
            </div>

            <div class="col-md-4 col-sm-12 col-xs-12 about-right <?= (!isset($this->context->setor)) ? 'wow fadeInRight animated' : ''; ?> textosAprest3" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                <div class="tabProd2 hidden" id="div-item">
                    <form id="form-carrinho" method="post">
                        <input type="hidden" name="id_evento" id="id_evento" value="<?= $evento->codigo; ?>">
                        <input type="hidden" id="carrinho-codigo" name="carrinho-codigo">
                        <input type="hidden" id="carrinho-area" name="carrinho-area">
                        <input type="hidden" id="carrinho-bloco" name="carrinho-bloco">
                        <input type="hidden" id="carrinho-desconto" name="carrinho-desconto">
                        <table class="table table-condensed" id="table-item">
                            <thead>
                                <tr>
                                    <th align="center" width="50%">Assento</th>
                                    <th align="center" width="35%">Preço unitário</th>
                                    <th align="center" width="10%">Quantidade</th>
                                    <th align="center" width="5%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td id="tbl-assento" width="50%"></td>
                                    <td width="35%"><input type="text" id="carrinho-preco-unitario" name="carrinho-preco-unitario" readonly></td>
                                    <td width="10%"><input type="text" id="carrinho-quantidade" name="carrinho-quantidade" min="1" maxlength="3"></td>
                                    <td width="5%" align="center" valign="middle"><i class="fa fa-cart-plus fa-2x plus-white" title="Adicionar ao seu carrinho"></i></td>
                                </tr>
                            </tbody>
                        </table>
                        <span class="msg-carrinho">* Selecione o assento, a quantidade e clique em <i class="fa fa-cart-plus fa-2x"></i></span>
                    </form>
                </div>
                <form method="post" id="form-bloco">
                    <input type="hidden" name="id_evento" id="id_evento" value="<?= $evento->codigo; ?>">
                    <div class="tabProd">
                        <div><i class="fa fa-shopping-cart fa-2x"></i> Meu Carrinho:</div>
                        <table class="table table-condensed table-stripped">
                            <thead id="tbl-header" class="<?= empty($carrinho->objeto->produtos) ? 'hidden' : ''; ?>">
                                <tr>
                                    <th width="15%">Área</th>
                                    <th width="40%">Setor / Bloco</th>
                                    <th width="10%">Itens</th>
                                    <th width="30%">Subtotal</th>
                                    <th width="5%"></th>
                                </tr>
                            </thead>
                            <tbody id="linhasLugares">
                            <?php 
                            // listando produtos do carrinho
                            // o for serve também para criar array de checagem para funções JS (permitir somente os produtos que o usuário pode renovar)
                            $blocosSelect=[];
                            $assentosSelect=[];
                            if (!empty($carrinho->objeto->produtos)) {  
                                foreach ($carrinho->objeto->produtos as $index => $produto) {
                                    $bloco = '';
                                    $area = '';
                                    $assentosSelect[]=$produto->codigo;
                                    foreach ($produto->tags as $index_tag => $tag) {
                                        if ($tag->nome == 'bloco') {
                                            $bloco = $tag->valor;
                                            $blocosSelect[]=$tag->valor;
                                        }
                                        if ($tag->nome == 'area') {
                                            $area = $tag->valor;
                                        }
                                    }
                                ?>
                                <tr id="tr-<?= $produto->codigo; ?>" class="linha-carrinho">
                                    <td width="15%"><?= $area; ?></td>
                                    <td width="40%"><?= $produto->nome; ?></td>
                                    <td width="10%" align="right"><?= number_format($produto->unidades, 0); ?></td>
                                    <td width="30%">
                                        <?php if (floatval($produto->desconto->fixo) > 0) { ?>
                                        R$ <?= number_format(($produto->valor - $produto->desconto->fixo) * $produto->unidades, 2, ',', ''); ?><br>
                                        <span class="strikeDesconto">R$ <?= number_format($produto->valor * $produto->unidades, 2, ',', ''); ?></span>
                                        <?php } else { ?>
                                        R$ <?= number_format($produto->valor * $produto->unidades, 2, ',', ''); ?>
                                        <?php } ?>
                                    </td>
                                    <td width="5%" align="center"><i class="fa fa-trash trash-item plus-red mouseMao" id="trash-<?= $produto->codigo; ?>" data-trash="<?= $produto->codigo; ?>" title="Remover este item do seu carrinho"></i></td>
                                    <input type="hidden" name="unidades[]" value="<?= number_format($produto->unidades, 0); ?>" min="0" />
                                    <input type="hidden" name="id[]" value="<?= $produto->codigo; ?>">
                                    <input type="hidden" name="area[]" value="<?= $area; ?>">
                                    <input type="hidden" name="setor[]" value="<?= $bloco; ?>">
                                    <input type="hidden" name="preco_unitario[]" value="<?= $produto->valor; ?>">
                                    <input type="hidden" name="desconto[]" value="<?= $produto->desconto->fixo; ?>">
                                </tr>                    
                                <?php 
                                } 
                            } else if(count($produtosReserva) == 0) { ?>
                                <tr>
                                    <td colspan="5">Os produtos estão indisponíveis. Confira seu <a href="<?= Url::to(['site/cliente']); ?>" target="_top">histórico de compras</a>, ou tente novamente mais tarde.</td>
                                </tr>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="5">A compra referente à sua renovação já foi efetuada. Por favor, confira o histórico de compras em sua <a href="<?= Url::to(['site/cliente']); ?>" target="_top">área de cliente</a>.</td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                
                <?php if (!empty($carrinho->objeto->produtos)) { ?>
                <div style="margin:0px auto;">
                    <br>
                    <span class="row contato carrDest">
                        <input type="submit" value="Finalizar a Compra&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" id="finaliza-compra" class="icon-next carrinho">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                </div>
                <?php } ?>
                
            </div>

            <p id="infoSetor"></p>
            <hr style="padding-top:25px;">
    
        </div>
    </div>
</div> 

<div id="infoSetor"></div>

<?php 
$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);

$this->registerJs('
    var blocosSelect='.json_encode($blocosSelect).';
    var assentosSelect='.json_encode($assentosSelect).';

    $("#mapaRoxo #lugares .bloco, #mapaRoxo #lugares .camarote").mouseover(function(){
        infoConteudo($(this));
    });
    $("#mapaRoxo #lugares").mouseout(function(){
        offInfoConteudo();
    });
    $("#mapaRoxo #lugares path").on("click",function(e){
        if($(this).hasClass("selecionado")) e.preventDefault();
        var id=$(this).attr("id");
        if($(this).hasClass("bloco") && blocosSelect.indexOf(id.replace("bloco_","")) > -1){
            listaLugares(id);
        }
    });
', \yii\web\View::POS_END); ?>
<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Cliente;
use app\models\Carrinho;
use app\models\CarrinhoBilheteria;
use app\models\Conta;
use app\models\Compras;
use app\models\VendaBilheteria;
use app\models\WsCarrinho;

class Cobranca extends Model
{
    public $trid;
    public $id_carrinho;
    public $documento;
    public $email;
    public $tipo; 
    public $validade;
    public $forma;
    public $descricao;
    public $codigo_cartao;
    public $cvv;
    public $valor;
    public $id_venda;

    public $unidade;
    
    /**
     * Método construtor sempre pede a unidade atual
    */
    public function __construct(Cliente $unidade){
        $this->unidade = $unidade;
    }

    public function rules()
    {
        return [
            [['trid', 'id_carrinho', 'documento', 'codigo_cartao', 'cvv'], 'required'],
            [['email', 'tipo', 'validade', 'forma', 'descricao', 'valor', 'id_venda'], 'safe'],
            [['trid', 'codigo_cartao', 'id_venda'], 'integer'],
            [['documento'], 'string', 'max' => '16'],
            [['email', 'tipo'], 'string', 'max' => '64'],
            [['descricao'], 'string', 'max' => '255'],
            [['email'], 'email'],
            [['validade'], 'date', 'format' => 'php: d/m/Y'],
            [['cvv'], 'match', 'pattern' => '/^\d{3,4}$/', 'message' => 'CVV é número de 3 ou 4 dígitos.'],
            [['valor'], 'double']
        ];
    }

    public function attributeLabels()
    {
        return [
            'trid' => 'ID de transação',
            'id_carrinho' => 'ID do carrinho',
            'documento' => 'Documento do cliente',
            'email' => 'E-mail do cliente',
            'tipo' => 'Tipo', 
            'validade' => 'Validade',
            'forma' => 'Forma',
            'descricao' => 'Descrição',
            'codigo_cartao' => 'Código do cartão',
            'cvv' => 'CVV'
        ];
    }
    
    /**
     * Enviar cobrança
     * @param $post Dados de cobrança: trid, id_carrinho, documento, [email], [tipo], [validade], [forma], [descricao]
     */
    public function enviarCobranca ($post)
    {
        $ws=new WsCobranca($this->unidade->user_token);
        if (isset($post) && !empty($post)) {
            $returnApi=$ws->enviarCobranca([
                'id_carrinho' => $post['id_carrinho'], 
                'documento' => $post['documento'], 
                'trid' => $post['trid'], 
                'forma' => $post['forma'], 
                'descricao' => 'Easy for Pay - Site'
            ]);

            /*Yii::debug(['Enviar cobrança',[
                'id_carrinho' => $post['id_carrinho'], 
                'documento' => $post['documento'], 
                'trid' => $post['trid'], 
                'forma' => $post['forma'], 
                'descricao' => 'Easy for Pay - Site'
                ],
                ['Retorno da api'=>$returnApi]
            ],__METHOD__);*/

            return $returnApi;
        }
        return $ws->getApiError('Não foi possível enviar a cobrança - POST vazio');
    }
    
    /**
     * Identifica dados de uma cobrança e atualizer o id do carrinho, que recebe um timestamp ao valor original
     * @param $post Dados da cobrança: trid
     */
    public function consultarCobranca ($post)
    {        
        if (isset($post) && !empty($post)) {
            $ws=new WsCobranca($this->unidade->user_token);
            $returnApi = $ws->consultarCobranca($post['trid']);
            /*Yii::debug([
                'Consultar cobrança',
                    ['Objeto enviado'=>$post['trid']], 
                    ['Unidade'=>$objApiCliente->attributes],
                    ['Retorno da API',$returnApi] 
                ]
            ,__METHOD__);*/

            if ($returnApi->successo == '1' && $returnApi->objeto->id_carrinho) {

                (new Carrinho($this->unidade))->definirSessaoCarrinho($returnApi->objeto->id_carrinho);
            }
            return $returnApi;
        } 
        return $ws->getApiError('Não foi possível consultar a cobrança.');
    }   
    
    /**
     * Autorizar pagamento
     * @param array $post Dados para autorizar pagamento: codigo (codigo_cobrança), codigo_cartao, senha_cartao
     */
    public function autorizarPagamento ($post)
    {
        if (isset($post) && !empty($post)) {
            $session = Yii::$app->session;
            $session->open();
            $ws=new WsCobranca($this->unidade->user_token);
            $returnApi=$ws->autorizarPagamento($session['user_token'], [
                'codigo' => $post['id_venda'],
                'codigo_cartao' => $post['codigo_cartao'],
                'senha_cartao' => $post['cvv']
            ]);

            /*Yii::debug([
                'Autorizar pagamento',
                'Token: '.$session['user_token'], 
                'Objeto enviado: '=>[
                    'codigo' => $post['id_venda'],
                    'codigo_cartao' => $post['codigo_cartao'],
                    'senha_cartao' => $post['cvv']
                ],
                ['Retorno da API',$returnApi] 
            ]
            ,__METHOD__);*/

            return $returnApi;
        } 
        return $ws->getApiError('Não foi possível autorizar a cobrança.');
    }
    
    /**
     * Efetuar estorno de cobrança - token de usuário logado (cliente ou operador)
     */
    public function estornarPagamento($post)
    {
        $api = new WsCobranca($this->unidade->user_token);
        return $api->estornarPagamento($post);
    }    
    
    /**
     * Efetuar estorno de cobrança - token de usuário logado (cliente ou operador)
     */
    public function estornarPagamentoBilheteria($codigo, $post)
    {
        $api = new WsCobranca($this->unidade->user_token);
        return $api->estornarPagamento($codigo, $post);
    }  
    
    /**
     * Gerar o hash para uma venda
     * @param string $data Data da venda
     * @param string $documento Documento do cliente
     * @param integer $codigo Id da venda
     */
    public function setHashVenda($data='', $documento='', $codigo='') 
    {
        $hash = '';
        if (!empty($data) && !empty($documento) && !empty($codigo) && (int)$codigo > 0) {
            $returnApi = (new WsCobranca)->setCriptoHash(['mensagem' => $data . '|' . $documento . '|' . $codigo]);
            if ($returnApi->successo === '1') {
                $hash = base64_encode($returnApi->objeto->criptografia);
            }
        }
        return $hash;
    }
    
    /**
     * Dado um hash, retornar as informações originais da venda correspondente
     * @param string $hash Hash de venda
     */
    public function getHashVenda($hash='')
    {
        if (!empty($hash)) {
            $criptografia = base64_decode($hash);
            return (new WsCobranca)->getCriptoHash(['criptografia' => $criptografia]);
        }
        return [];
    }

    /**
     * Checar ocupantes de compras por período
     */
    public function consultarComprasOcupantes ($dateStart='', $dateEnd='')
    {
        return (new WsCobranca($this->unidade->user_token))->consultarVendaOcupantes($dateStart, $dateEnd);
    }
            
    /**
     * Checar produtos por venda
     */
    public function consultarProdutosVenda ($idVenda) 
    {
        $ws = new WsCobranca($this->unidade->user_token);
        if ((int)$idVenda > 0) {
            return $ws->consultarProdutosVenda($idVenda);
        }
        return $ws->getApiError('Faltam informações para consultar os produtos da venda.');
    }
    
    /**
     * Enviar cobranca de boleto. Este método apenas prepara o objeto e o envio é feito usando o método enviarCobranca
    */
    public function enviarCobrancaBoleto($cobranca, $sacado, $boleto){
        $post = [
            'trid' => $cobranca['trid'],
            'documento' => $sacado['documento'],
            'valor' => $cobranca['valor'],
            'unidades' => $cobranca['unidades'],
            'forma' => $cobranca['forma'],
            'tipo' => $cobranca['tipo'],
            'mensagem' => $cobranca['mensagem'],
            'boleto' => [
                'seunumero' => $boleto['seunumero'],
                'mensagem' => $boleto['mensagem'],
            ],
            'validade' => $cobranca['validade'],
            'creditar'=>$cobranca['creditar']
        ];
        return $this->enviarCobranca($post);
    }

    public function consultarVendaFiltro($post){
        return (new WsCobranca($this->unidade->user_token))->consultarVendaFiltro($post);       
    }    
    
    public function envioEmailConfirmacao(){
        $modelConta=new Conta();
        $comprador=$modelConta->getDadosComprador();
        $modelConta->loadFromApi($comprador->objeto);
        
        $modelCompra=new Compras($modelConta);
        $modelCompra->codigo=$this->id_venda;
        $modelCompra->umaCompra();
        
        $mensagemModel=new Mensagem();
        return $mensagemModel->enviarEmailCompra($modelCompra);
    }
    
    /**
     * Cobrança conciliada - vendas avulsas de ingressos (bilheteria)
     * Usar o token armazenado na sessão, que pertence ao operador logado, para que fique registrado em seu nome e possamos identificar facilmente quem fez a compra
     * @param array $post 
     * @return type
     */
    public function cobrancaConciliada ($post) 
    {
        $session = Yii::$app->session;
        $session->open();
        $objApiCliente=new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));        
        $apiWs = new WsCarrinho($session['user_token']); //token do operador
        if (isset($post) && !empty($post) && !empty($session['idCarrinho'])) {
            $formaPagto = (new VendaBilheteria)->formasPagamento($post['formaPagto']);
            $arrayCobranca = [
                'trid' => $post['trid'], 
                'id_carrinho' => $session['idCarrinho'],
                'documento' => '49901387086',
                'email' => 'venda_avulsa@estadiomineirao.com.br', 
                'parcelas' => isset($post['forma']) ? $post['forma'] : '1', 
                'descricao' => 'Mineirão - Bilheteria',
                'id_conciliacao' => $formaPagto == 'Especie' ? $formaPagto : $post['conciliador']['administrativeCode'],
                'codigo_meio_pagamento' => $post['formaPagto']
            ];
            if (isset($post['adquirente'])) {
                $arrayCobranca['codigo_adquirente'] = $post['adquirente'];
                $arrayCobranca['retorno_conciliacao'] = base64_encode(json_encode($post['conciliador']));
            }
            $returnApi = $apiWs->enviarCobranca($arrayCobranca);
            $return = [];
            if ($returnApi->successo == '1') {
                (new CarrinhoBilheteria($objApiCliente))->limparDaSessao();
                $return = [
                    'successo' => '1', 
                    'objeto' => ['compra' => $returnApi->objeto->codigo_cobranca]
                ];
            }
            return sizeof($return)>0 ? $return : $returnApi;
        }
        return $apiWs->getApiError('Não foi possível registrar a cobrança conciliada');
    }
    
    /**
     * Verificar se estorno é possível, considerando-se regras definidas na política do site:
     *      Compras realizadas via internet só podem ser canceladas no prazo de até 07 (sete) dias corridos 
     *      à partir da data de compra. Solicitações de cancelamento efetivadas a menos de 02 (dois) dias do 
     *      horário previsto para o início do evento não serão reembolsadas, mesmo que haja menos de 07 (sete) 
     *      dias corridos da data de compra.
     *      Neste momento (11/09/2018), faremos uma verificação se na compra haverá algum evento em 2 dias, 
     *      ou com data anterior à atual, e, neste caso, impedir o estorno; em caso negativo, permitir o estorno. (Marco)
     * @parameter object $carrinho Objeto com informações do carrinho
     * @return boolean true (estorno é possível) / false (estorno indisponível)
     */
    public function checarEstorno($carrinho)
    {
        $estorno2dias = false;
        $estorno7dias = false;
        if (!empty($carrinho) && $carrinho->objeto->produtos) {
            $data7Dias = date('Y-m-d', strtotime('-7days'));
            $data2Dias = date('Y-m-d H:i:s', strtotime('+2days'));
            $arrayDataCompra = explode(' ', $carrinho->objeto->cobranca->data);
            $dataCompra = date('Y-m-d', strtotime(str_replace('/', '-', $arrayDataCompra[0])));
            if ($dataCompra > $data7Dias) {
                $estorno7dias = true;
                $podeEstornar = 0;
                foreach($carrinho->objeto->produtos as $index => $produto) {
                    $dataEvento = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $produto->agenda->horario)));
                    $podeEstornar += ($dataEvento > $data2Dias) ? 1 : 0;
                }
                $estorno2dias = count($carrinho->objeto->produtos) == $podeEstornar ? true : false;
            }
        }
        return ['dias7' => $estorno7dias, 'dias2' => $estorno2dias];
    }
    
}
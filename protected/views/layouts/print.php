<?php

use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);

$this->beginPage() 

?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <?= Html::csrfMetaTags() ?>
        <title>Vendas | Estádio Mineirão</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width">
        <meta name="viewport" content="initial-scale=1.0">
        <link href="http://estadiomineirao.com.br/wp-content/themes/Mineirao/favicon.ico" rel="shortcut icon">
        <link href="http://estadiomineirao.com.br/wp-content/themes/Mineirao/favicon.ico" rel="icon">
        <link href="<?=Yii::getAlias("@web")?>/css/print.css" rel="stylesheet">
        <link href="<?=Yii::getAlias("@web")?>/css/style.css" rel="stylesheet">
        <?php $this->head() ?>
    </head>
    <?php $this->beginBody() ?>
    <body>
        <?= $content ?>
        <?php $this->endBody() ?>    
    </body>
</html>
<?php $this->endPage() ?>
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin(['id' => 'form-anexo','options'=>['enctype'=>'multipart/form-data']]); ?>

<div div class="col-md-6" style="padding-left:0px;">
	<?= $form->field($model, 'nome') ?>
</div>
<div div class="col-md-6" style="padding-left:0px;">
	<?= $form->field($model, 'tipo') ?>
</div>
<?= $form->field($model, 'objeto')->fileInput() ?>

<div class="form-group" style="text-align:center;">
    <?= Html::submitButton($model->scenario=='create'? 'Cadastrar' :'Salvar', ['class' => 'btn btn-primary btn-pagar']) ?>
</div>

<?php ActiveForm::end(); ?>
<br>
<br>
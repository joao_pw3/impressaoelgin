<?php
use yii\helpers\Url;
?>
<div class="container">
    <input type="hidden" id="matriz-produtos" value="<?= isset($evento->tags['apelido']) ? $evento->tags['apelido'] : ''; ?>">
    <input type="hidden" id="area-estadio" value="<?= $this->context->area ? $this->context->area : '' ?>">
    <input type="hidden" id="url-get-produto-tags" value="<?= Url::to(['/admin/vendas/produto-por-tag']); ?>">
    <input type="hidden" id="url-status" value="<?= Url::to(['/admin/vendas/mudar-status']); ?>">
    <input type="hidden" id="url-reserva" value="<?= Url::to(['/admin/vendas/reservar-produto']); ?>">
    <input type="hidden" id="url-check-user" value="<?= Url::to(['/admin/vendas/checar-documento']); ?>">
    <input type="hidden" id="url-map" value="<?= Url::to(['/maps/bloco_']); ?>">

    <div class="row">
        <h2 class="tituloGeral"><i class="fa fa-clone"></i> Setor <?= $this->context->area == '' ? 'Indefinido' : ($this->context->area == 'Leste' ? 'Vermelho' : 'Roxo'); ?></h2>
    </div>
			
    <!-- <h4>
        <a href="javascript: window.history.back();" class="bt-icon-small light">Voltar</a>
    </h4> -->

    <ul class="nav nav-tabs">
        <li class="boxTabs active"><a data-toggle="tab" href="#tab-bloco">Blocos</a></li>
        <li class="boxTabs"><a data-toggle="tab" href="#tab-reserva">Assentos</a></li>
    </ul>

    <div class="tab-content clearfix boxTabsInferior">
        <div id="tab-bloco" class="tab-pane fade in active">
            <?= $this->render('blocos', [
                'area' => $this->context->area ? $this->context->area : '',
                'blocos' => $blocos
            ]); ?>
        </div>
        <div id="tab-reserva" class="tab-pane fade">
            <?= $this->render('assentos', [
                'evento' => $evento ,
                'setores' => $setores,
                'compra' => $compra,
                'carrinho' => $carrinho,
                'area' => $this->context->area ? $this->context->area : '',
            ]); ?>
        </div>
    </div>
</div>&nbsp;<br>&nbsp;<br>
 
<div id="infoSetor"></div>
var dataAgenda, codigo_sn;

$("#qtd-ocupantes").click(function () {
	$('#qtd-ocupantes').attr('disabled',true);
	var qtdOcupantes=$("#qtd-ingresssos").val();
	dataAgenda=$('#id_agenda').val().split('_');
	if(qtdOcupantes>0){
		doAjax($('#url-produto-buscar').val(), 'POST', {'filtro':{'id_agenda':dataAgenda[1]}}, 'JSON', 10000, 'getProdutoSNOK', 'getProdutoSNErro');
	}	
});

$(document).on('click','.div-trash2',function(){
	$(this).parents('.prodBilhet').remove();
	calcularTotal();
});

function getProdutoSNOK(data){  
	var produto = data.produtos[0];
	codigo_sn=produto.codigo;
    if(produto.ativo != '1' || produto.oferecido <= 0){
        showMsg("Esse produto não está disponível");
        return;
    }
	doAjax ($('#url-add-produto').val(), 'POST', {'produto_codigo': codigo_sn, 'produto_unidades':$('#qtd-ingresssos').val()}, 'JSON', 10000, 'addProdutoSNOK', 'addProdutoSNErr');
}

function addProdutoSNOK(data) {
    $('#documento_comprador').removeClass('hidden');
    doAjax($('#url-select-ingresso').val(), 'POST', {'produto_codigo': codigo_sn, 'id_agenda': dataAgenda.join('_'), 'index':0}, 'JSON', 10000, 'ingressoSNOK', 'ingressoSNErr');
}

function addProdutoSNErr(data) {
	$('#qtd-ocupantes').attr('disabled',false);
    showMsg('Falha ao adicionar produto ao carrinho: ' + data.erro.mensagem);
}

function getProdutoSNErro(data){
	showMsg(data.erro.mensagem);
}

function ingressoSNOK (data) {
	htmlOcupantes(data);
	calcularTotal();
}

function ingressoSNErr (data) {
	console.log(data.erro.mensagem);
}

function htmlOcupantes(data) {
	var qtdOcupantes=$("#qtd-ingresssos").val();
	for (var indexOcupante=1; qtdOcupantes>=indexOcupante; indexOcupante++) {
		var selectIngresso=data.select;
		selectIngresso=selectIngresso.replace("Ocupante[cupom_codigo][0]", "Ocupante[cupom_codigo]["+indexOcupante+"]");
		selectIngresso=selectIngresso.replace('data-indice="0"', 'data-indice="'+indexOcupante+'"');
		selectIngresso=selectIngresso.replace('id="ingresso-'+codigo_sn+'-0"', 'id="ingresso-'+codigo_sn+'-'+indexOcupante+'"');
		var divIngresso='';
	    divIngresso+='<div id="div-'+indexOcupante+'" class="col-xs-12 prodBilhet">'
	    divIngresso+=    '<input type="hidden" name="Ocupante[produto_codigo]['+indexOcupante+']" value="'+codigo_sn+'">'
	    divIngresso+=    '<div class="col-xs-2 semPad div-trash2">'
	    divIngresso+=    	'<div class="font-awesome del-carrinho" title="Excluir Ocupante">&#xf1f8;</div>'
	    divIngresso+=    '</div>'	        
	    divIngresso+=    '<div class="col-xs-10">'
	    divIngresso+=        '<input type="text" name="Ocupante[nome][' + indexOcupante + ']" class="form-control ocupante-nome ocupBilhet" placeholder="Nome do ocupante">'
	    divIngresso+=        '<input type="text" name="Ocupante[documento][' + indexOcupante + ']" class="form-control ocupante-doc ocupBilhet" placeholder="Documento do ocupante">'
	    divIngresso+=        '<input type="text" name="Ocupante[email][' + indexOcupante + ']" class="form-control ocupante-email ocupBilhet" placeholder="Email do ocupante">'
	    divIngresso+=        '<input type="text" name="Ocupante[telefone][' + indexOcupante + ']" class="form-control ocupante-tel ocupBilhet" placeholder="Telefone do ocupante">'
	    divIngresso+=    selectIngresso
	    divIngresso+=    '</div>'
		divIngresso+='	<hr>'
	    divIngresso+='</div>';
	    $('#venda-selecionados').append(divIngresso);		
	}
}
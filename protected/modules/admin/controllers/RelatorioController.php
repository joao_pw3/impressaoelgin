<?php
namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\controllers\AbstractController;
use app\models\Transacoes;
use app\models\Cliente;

/**
 * Controle para relatórios
 */
class RelatorioController extends AbstractController
{   
    public function getUrl() {
        return YII_ENV_DEV ? 'https://www.zpointz.com.br/mineirao' : 'http://balanceador-372858385.sa-east-1.elb.amazonaws.com/mineirao';

    }
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $firstDay=new \DateTime('today');
        $lastDay=new \DateTime('today');
        $lastDay->setTime(23,59,59);
        $dataDe = $firstDay->format('d/m/Y H:i:s');
        $dataAte = $lastDay->format('d/m/Y H:i:s');
        $post=Yii::$app->request->post();
        if($post){
            $dataDe=$post['de'].' 00:00:00';
            $dataAte=$post['ate'].' 23:59:59';
        }
        $session = $this->_sessionOpen();
        $objApiCliente=new Cliente;
        $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));        
        $transacoes = new Transacoes($objApiCliente);
        $transacoes->dataInicial = \DateTime::createFromFormat('d/m/Y H:i:s',$dataDe);
        $transacoes->dataFinal =  \DateTime::createFromFormat('d/m/Y H:i:s',$dataAte);
        $transacoes->transacoesPeriodo(true);
        $session['admVendasTransacoes'] = $transacoes->transacoes;

        return $this->render('index',[
            'de'=>$transacoes->dataInicial,
            'ate'=>$transacoes->dataFinal,
            'transacoes'=>$transacoes->transacoes,
            'total'=>count($transacoes->transacoes)
        ]);        
    }

    /**
     * Exportar para CSV com informações adicionais - produtos da compra e se é usuário novo
    */
    public function actionExportarCsv(){
        $session = Yii::$app->session;
        $session->open();        
        if($session['admVendasTransacoes']==null) {
            $session['report-export-csv'] = 'Não foi possível gerar o relatório com as informações enviadas';
            return $this->redirect(['vendas']);
        }

        $registros=$session['admVendasTransacoes'];
        ini_set('max_execution_time', 300);
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=vendas-site-'.date('d-m-Y_H-i-s').'.csv');
        $output = fopen('php://output', 'w');
        fputcsv($output, ['Data da compra','Nome completo','CPF',utf8_decode('Endereço completo'),'Cidade','Estado','Bairro','CEP','Telefone de contato','E-mail','Nome do produto vendido','Valor','Forma de pagamento','Bandeira',utf8_decode('Conciliação'),utf8_decode('Condições de pagamento'),'Taxa','Primeira compra'],';');
        
        foreach($registros as $r){
            if($r['codigo_status']!=1) continue;
            $venda=\app\models\Transacoes::dadosVenda($r);
            foreach($venda as $linha){
                fputcsv($output, $linha,';');
            }
        }
        die();
    }    

    public function actionControleCaixa()
    {
        return $this->render('controle-caixa', ['url'=>$this->getUrl()]);      
    }  

    public function actionVendasCancelamentosAnalitico()
    {
        return $this->render('vendas-cancelamentos-analitico', ['url'=>$this->getUrl()]);      
    }    

    public function actionVendasCancelamentosSintetico()
    {
        return $this->render('vendas-cancelamentos-sintetico', ['url'=>$this->getUrl()]);
    }    

    public function actionRepasses()
    {
        return $this->render('repasses', ['url'=>$this->getUrl()]);      
    }    

    public function actionDetalhamentoEventos()
    {
        return $this->render('detalhamento-eventos', ['url'=>$this->getUrl()]);      
    }    

    public function actionVendasCliente()
    {
        return $this->render('vendas-cliente', ['url'=>$this->getUrl()]);      
    }  
    
    public function actionTabelaGeral(){
        $session = $this->_sessionOpen();
        $objApiCliente=new Cliente;
        $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
        $model=new \app\models\Financeiro;
        $model->unidade=$objApiCliente;
        $model->tabelaFinanceira();
        if($model->hasErrors()){
            return $this->render('/default/error', ['name' => 'Erro na consulta ao relatório', 'message' => $model->getErrors('lista')[0]]);
        }

        return $this->render('tabela-geral',['model'=>$model]);
    }    
}
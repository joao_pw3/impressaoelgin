<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\ApiAgenda;
use app\models\ApiProduto;
use app\models\Mineiraocard;
use app\components\TempoCarrinhoWidget;

$session = Yii::$app->session;
$session->open();
echo TempoCarrinhoWidget::getBreadCrumb('losang4.png');

?>
<div class="container">
    <input type="hidden" id="url-index" value="<?= Url::to(['site/index']); ?>">
    <?php if (isset($carrinho) && empty($carrinho->objeto->produtos)) { ?>
        <div class="boxMensagem2" style="height:350px; margin-bottom:25px;">
            Seu carrinho de compras está vazio. Acesse a <a href="<?= Url::to(['site/index']); ?>">página de eventos</a> para comprar os ingressos de seu interesse.
        </div>
        <?php } else {
                //echo '<h3>Lugares e ocupantes: (<a href="'.Url::to(['site/resumo']).'">voltar e editar</a>):</h3>';
                $precoFinal = 0;
                $total = 0;
                foreach ($carrinho->objeto->produtos as $index => $produto) {
                    $apiProduto=new ApiProduto();
                    $produtoValor=$apiProduto->descontoProduto($produto->codigo);
                    $precoFinal += $produtoValor * $produto->unidades;
                    $total += $produto->unidades;
                    $bloco = '';
                    $area = '';
                    $agenda=new ApiAgenda();
                    $evento;
                    $banner = '';
                    foreach ($produto->tags as $index_tag => $tag) {
                        if ($tag->nome == 'area') {
                            switch($tag->valor){
                                case 'Leste':
                                    $area='Vermelha';
                                    break;
                                case 'Oeste':
                                    $area='Roxa';
                                    break;
                            }
                        }
                        if ($tag->nome == 'matriz') {
                            $evento = $agenda->buscarEvento(json_encode(['tags'=>[['nome'=>'apelido','valor'=>$tag->valor]]]));
                        }
                    }
                    foreach ($evento->objeto[0]->anexos as $index_anexo => $anexo) {
                        if ($anexo->nome == 'bannervitrine') {
                            $banner = $anexo->url;
                        }                    
                    }

                    $mineiraocardNome='';
                    $mineiraocardDoc='';
                    $mineiraocardTel='';
                    $mineiraocardEmail='';
                    $mineiraocardValido=false;
                    
                    if(isset($carrinho->objeto->tags)){
                        foreach($carrinho->objeto->tags as $tag){
                            $nome=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_nome');
                            if($nome!=false)
                                $mineiraocardNome=$nome;
                            $doc=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_documento');
                            if($doc!=false)
                                $mineiraocardDoc=$doc;
                            $tel=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_telefone');
                            if($tel!=false)
                                $mineiraocardTel=$tel;
                            $email=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_email');
                            if($email!=false)
                                $mineiraocardEmail=$email;
                        }
                        $mineiraocardValido=$mineiraocardNome!=false&&$mineiraocardDoc!=false;
                    }
        
        ?>
        
            <div class="row about-left wow fadeInLeft animated div-<?= $produto->codigo; ?>" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                <div class="col-md-12 col-xs-12 tarjaCz">
                    <?php //if (!empty($evento['datahora'])) { ?>
                    <!-- dia --> <?php //date('d/m', strtotime($evento['datahora'])) . ' às ' . date('H:m', strtotime($evento['datahora'])) . ' - '; ?>
                    <?php //} ?>
                    <?php echo $evento->objeto[0]->nome; ?>
                </div>
            </div>
            <br>
            <div class="row">           
                <div class="col-md-12 zeraesp">
                    <div class="tabProds">
                        <div>
                            <label>Itens</label>
                            <?= number_format($produto->unidades, 0); ?>
                        </div>
                        <div>
                            <?php echo Html::img($banner, ["class" => "imgBanner"]); ?>
                        </div>
                        <div>
                            <label>Produto</label>
                            <?= $produto->nome; ?>
                        </div>
                        <div>
                            <label>Área</label>
                            <?= $area; ?>
                        </div>
                        <div>
                            <label>Nome do Ocupante</label>
                            <?= $mineiraocardNome ?>
                        </div>
                        <div>
                            <label>Documento</label>
                            <?= $mineiraocardDoc ?>
                        </div>
                        <div>
                            <label>Subtotal</label>
                            <?php
                            if ($produtoValor == $produto->valor) {
                                echo 'R$ ' . number_format($produtoValor * $produto->unidades, 2, ',', '.');
                            } else {
                                echo '<span class="strikeDesconto">R$ ' . number_format($produto->valor * $produto->unidades, 2, ',', '.') . '</span><br> ';
                                echo ' R$ ' . number_format($produtoValor * $produto->unidades, 2, ',', '.');
                            }
                            ?>
                        </div>
                        <div class="cellBots">
                            <a target="_top" href="javascript:;" id="trash-<?= $produto->codigo; ?>" data-trash="<?= $produto->codigo; ?>" class="icon icon-trash corCz" title="Remover do carrinho"></a>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php } ?>
            <br clear="all">
            <span class="botaoVoltar"><i class="fa fa-arrow-left"></i> <a href="<?=Url::to(['site/resumo'])?>">voltar e editar</a></span>
            <br><br>
        	<div class="row">
                <p>O boleto será gerado com os dados abaixo. Confira os campos e altere se necessário</p>
                <?=Html::beginForm(Url::to(['gerar-boleto']),'POST',['class'=>'form-horizontal col-md-8','id' => 'form-gerar-boleto'])?>
                <label>Sacado:</label>
                <div class="row">
                    <label class="form-group col-md-6  col-xs-12">Nome:
                    <input class="form-control" type="text" name="Sacado[nome]" value="<?=$cliente->nome ?>" placeholder="Nome" title="Nome"></label>
                    <label class="form-group col-md-6  col-xs-12">Documento:
                    <input class="form-control" type="text" name="Sacado[documento]" value="<?=$cliente->documento ?>" placeholder="Documento" title="Documento"></label>
                    <label class="form-group col-md-6  col-xs-12">Endereço:
                    <input class="form-control" type="text" name="Sacado[endereco]" value="<?=$cliente->endereco ?>" placeholder="Endereço" title="Endereço"></label>
                    <label class="form-group col-md-3  col-xs-6">Número:
                    <input class="form-control" type="text" name="Sacado[num]" value="<?=$cliente->num ?>" placeholder="Número" title="Número"></label>
                    <label class="form-group col-md-3  col-xs-6">Complemento:
                    <input class="form-control" type="text" name="Sacado[compl]" value="<?=$cliente->compl ?>" placeholder="Complemento" title="Complemento"></label>
                    <label class="form-group col-md-6  col-xs-12">Cidade:
                    <input class="form-control" type="text" name="Sacado[cidade_nome]" value="<?=$cliente->cidade_nome ?>" placeholder="Cidade" title="Cidade"></label>
                    <label class="form-group col-md-6  col-xs-12">Estado:
                    <input class="form-control" type="text" name="Sacado[uf]" value="<?=$cliente->uf ?>" placeholder="Estado" title="Estado"></label>
                <p class="clearfix"></p>
                </div>
                <dl class="dl-horizontal">
                    <dt>Valor:</dt> <dd>R$ <?= number_format($carrinho->objeto->totais->total,2,',','.')?></dd>
                    <dt>Vencimento:</dt> <dd><?= date('d/m/Y')?></dd>
                    <dt>Mensagem:</dt> <dd>Compra de crédito</dd>
                </dl>
                <?= Html::hiddenInput('Cobranca[trid]',substr(time() . mt_rand(), 8, 11))?>
                <?= Html::hiddenInput('Cobranca[valor]',$carrinho->objeto->totais->total)?>
                <?= Html::hiddenInput('Cobranca[mensagem]','Compra de crédito')?>
                <?= Html::hiddenInput('Cobranca[unidades]','1')?>
                <?= Html::hiddenInput('Cobranca[validade]',date('d/m/Y'))?>
                <?= Html::hiddenInput('Cobranca[forma]','1')?>
                <?= Html::hiddenInput('Cobranca[creditar]','1')?>
                <?= Html::hiddenInput('Cobranca[tipo]','Boleto')?>
                <?= Html::hiddenInput('Boleto[seunumero]',substr(time() . mt_rand(), 8, 10))?>
                <?= Html::hiddenInput('Boleto[mensagem]','Compra de crédito Mineirão')?>
                <button type="submit" class="btn btn-primary">Gerar boleto</button>
                <?=Html::endForm();?>
            </div>
        <?php }
    ?>
</div>

<?php 
$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/carrinho.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/compra.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    $this->registerJs("deadline = setDataExpira('" . $session['tempoCarrinho'] . "'); initializeClock('clockdiv', deadline);");
}
?>
<?php
namespace app\models;
use Yii;
use app\models\ApiProduto;
use app\models\ApiAgenda;
use yii\base\Exception;

/**
 * This is the model class for table "eventos".
 *
 * @property string $id
 * @property string $tipo
 * @property string $nome
 * @property string $data
 * @property string $descricao
 * @property integer $status
 */
class EventosModel extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'eventos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo', 'nome', 'data_hora'], 'required'],
            [['data_hora'], 'safe'],
            [['descricao'], 'string'],
            [['status'], 'integer'],
            [['tipo'], 'string', 'max' => 32],
            [['nome'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo' => 'Tipo',
            'nome' => 'Nome',
            'data_hora' => 'Data Hora',
            'descricao' => 'Descricao',
            'status' => 'Status',
        ];
    }

    /**
     * @inheritdoc
     * @return EventosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EventosQuery(get_called_class());
    }
    
    /**
     * Listagem de eventos disponíveis. Organizar os anexos e tags como array associativa para view
     */
    public function listaEventos($post=null) 
    {
        $agenda=new ApiAgenda();
        $eventos=$agenda->buscarEvento($post);
        if($eventos->successo!=1)
            throw new Exception('Erro na consulta da agenda');
        if(count($eventos->objeto)==0)
            throw new Exception('Nenhum evento encontrado');
        $eventosReturn=[];
        foreach ($eventos->objeto as $evento) {            
			if(isset($evento->datas) && $this->eventoDiaDisponivel($evento->datas[0]->data)==false) continue;
			if(!isset($evento->datas) && $this->eventoDiaDisponivel($evento->data->final)==false) continue;
            $eventosReturn[]=$this->eventoTagsAnexosAssoc($evento);
        }
        return $eventosReturn;
    }
    
    /**
     * Retornar evento específico 
     * @param type $id
     */
    public function infoEventoComAreas($id) 
    {
        $agenda=new ApiAgenda();
        $evento=$agenda->buscarEvento(null,$id);
        if($evento->successo!=1)
            throw new Exception('Erro na consulta da agenda');
        if(count($evento->objeto)==0)
            throw new Exception('Evento não encontrado');
        return $this->eventoTagsAnexosAssoc($evento->objeto[0]);
    }

    /**
     * Retornar evento específico com mapa de assentos da matriz a ser carregada
     * @param integer $id o ID do evento
     * @param string $area nome da área selecionada
     * @param array $blocos Blocos da área selecionada
     * @param array $camarotes Camarotes da área selecionada
     * 
     * @todo Adicionar camarotes, assim que estiverem disponíveis para venda
     */
    public function infoEventoComLugares($id, $area, $blocos=[], $camarotes=[]) 
    {
        if (!in_array($area, ['Oeste', 'Leste'])) { 
            return ['evento' => '', 'jsonAssentos' => ''];
        }
        $evento = $this->infoEventoComAreas($id);
        /* BLOCOS */
        $assentos = $this->getLugares($evento, $area, 'bloco', $blocos);
        
        /* CAMAROTES - 2018-02-07 - MARCO: NO MOMENTO, APENAS BLOCOS */
//        $camarotes = $this->getLugares($id, $area, 'camarote', $camarotes);

        return ['evento' => $evento, 'jsonAssentos' => $assentos];
    }
    
    /**
     * Busca de lugares no estádio
     * @param object $evento Objeto com dados do evento
     * @param string $area Área do estádio
     * @param string $tipo Tipo de lugar: bloco / camarote
     * @param array $lugares Todos os lugares ou array de id´s de lugares específicos
     * @return array
     */
    private function getLugares($evento, $area, $tipo, $lugares) 
    {
        $setores = []; 
        if (!empty($evento) && !empty($area) && !empty($tipo)) {
            $modelProdutos = new ApiProduto;
            $arrTags = ['tags' => [
                ['nome' => 'area', 'valor' => $area],
                ['nome' => 'tipo', 'valor' => $tipo],
                ['nome' => 'matriz', 'valor' => $evento->tags['apelido']]
            ]];
            if (empty($lugares)) {
                $objBloco = $modelProdutos->buscarProdutoTags(json_encode($arrTags), NULL, false);
                $setores['objeto'] = empty($setores['objeto']) ? $objBloco->objeto : array_merge($setores['objeto'], $objBloco->objeto);
            } else {
                foreach ($lugares as $lugar) {
                    $newArray=$arrTags;
                    array_push($newArray['tags'],['nome'=>$tipo,'valor'=>$lugar]);
                    $objBloco = $modelProdutos->buscarProdutoTags(json_encode($newArray), NULL, false);
                    $setores['objeto'] = empty($setores['objeto']) ? $objBloco->objeto : array_merge($setores['objeto'], $objBloco->objeto);
                }
            }
        }
        return (object) $setores;
    }

    /**
     * Fazer uma array associativa de anexos e tags para permitir um uso mais direto
     * @param object $evento um registro de evento da API
     * @return object $evento com tags e anexos modificados
    */
    public function eventoTagsAnexosAssoc($evento){
        $anexosObj=[];
        $tagsObj=[];
        if (isset($evento->anexos) && !empty($evento->anexos)) {
            foreach ($evento->anexos as $key => $value) {
                $anexosObj[$value->nome]=$value->url;
            }
        }
        if (isset($evento->tags) && !empty($evento->tags)) {
            foreach ($evento->tags as $key => $value) {
                $tagsObj[$value->nome]=$value->valor;
            }
        }
        $evento->anexos=$anexosObj;
        $evento->tags=$tagsObj;
        return $evento;
    }
    
    /**
     * Identificar evento através de um apelido
     */
    public function getEventoByApelido($tags=[]) 
    {
        $evento;
        if (!empty($tags)) {
            $apelido = '';
            foreach($tags as $tag) {
                if ($tag->nome == 'matriz') {
                    $apelido = $tag->valor;
                    break;
                }
            }
            if ($apelido != '') {
                $evento = (new ApiAgenda)->buscarEvento(['tags' => [[
                        'nome' => 'apelido',
                        'valor' => $apelido
                ]]]);
            }
        }
        return $evento;
    }

    /**
     * Confirmar evento no futuro ou no passado
     * @param string $dataEvento data do evento no formato dd/mm/aaaa
     * @param string $tolerancia o tempo de tolerância no formato interval_spec que o evento ainda está disponível após o horário ter passado. Padrão 15 minutos
     * @return boolean
    */
    private function eventoDiaDisponivel($dataEvento, $tolerancia='PT15M'){
        $objDataEvento=\DateTime::createFromFormat('d/m/Y H:i:s',$dataEvento);
        $intervalo=new \DateInterval($tolerancia);
        if($objDataEvento == false) return true;
        $objDataEvento->add($intervalo);
        $timeStampEvento=strtotime($objDataEvento->format(\Datetime::ATOM));
        $timeStampAgora=time();
        return $timeStampEvento>=$timeStampAgora;
    }
    
}

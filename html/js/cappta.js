var desabilitaEstorno=function(error) {
    fnEstorno={'desabilitado': true, 'error': error};
}

var capptaInit=function(){
    capptaObj=CapptaCheckout.authenticate(capptaAuth, capptaAuthSuccess, capptaAuthError);
};

var pagamentoDebito=function(objCappta,valor){
    objCappta.debitPayment({amount:valor}, pgtoOk, pgtoErr);
};

var pagamentoCredito=function(objCappta,valor,parcelas){
    objCappta.creditPayment({amount:valor,installments:parcelas,installmentsType:1}, pgtoOk, pgtoErr);
};

var capptaAuthSuccess = function (response) {
    fnEstorno=false;
    merchantCheckoutGuid = response.merchantCheckoutGuid;
    $("#cappta-checkout-iframe").addClass('hidden');
};

var capptaAuthError = function (error) {
    removeCartao();
    desabilitaEstorno(error);
    $("#cappta-status").html('<div class="col-md-5" style="margin-top:0px; text-align:center;"><img src="../../images/pinpad.gif" style="width:200px;"></div><div class="col-md-7 alert alert-danger" style="margin-top:15px;" role="alert"><label class="control-label">Erro</label>: ' + error.reason + ' (código: ' + error.reasonCode + ')</div>');
};

var parametrosCancelar=function(obj, conciliador){
    if(obj instanceof Object == false) {
        showMsg('O pinpad não está conectado ou está com problemas. Conecte-o e recarregue a página');
        return false;         
    }
    if(typeof conciliador != 'string' || conciliador.length != 11) {
        showMsg('O número do controle é inválido. Reinicie a operação');
        return false; 
    }    
    return {'ok':true};
};

var cancelamentoVenda=function(objCappta, conciliador){
    objCappta.paymentReversal({
        administrativePassword: 'cappta',
        administrativeCode: conciliador
    }, cancelarOk, cancelarErr);
};



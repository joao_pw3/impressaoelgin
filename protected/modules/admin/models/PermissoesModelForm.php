<?php
namespace app\modules\admin\models;

use yii\base\Model;
use app\models\ApiPermissoes;

/**
 * Classe Model para Permissões - Easy for Pay - Mineirão
 * Responsável por criar regras de validação e tratar dados da API
 */
class PermissoesModelForm extends Model
{
    public $listaPermissoes;
    public $permissoesUsuario;
    public $usuario;
    public $controleAcao;
    public $inativo;

    /**
     * Lista de permissões que podem ser usadas no sistema
     */
    public function __construct(){
      $this->listaPermissoes = [
        'Agenda'=>[
            'agenda/index',
            'agenda/novo-evento',
            'agenda|index',
            'agenda|novo-evento',
        ],
        'Produtos'=>[
            'produto/index',
            'produto/editar-assentos',
            'produto|index',
            'produto|editar-assentos',
            'cupom/index',
            'cupom/novo-cupom',
            'cupom|index',
            'cupom|novo-cupom',
        ],
        'Bilheteria - entrega de cartões'=>[
            'bilheteria/index',
            'bilheteria/ativar-ingresso',
            'bilheteria/check-venda',
            'bilheteria/retirar-cartao',
            'bilheteria/filipetas',
            'bilheteria/venda-avulsa',
        ],
        'Relatório de vendas'=>[
            'vendas/index',
            'vendas/exportar-csv',
            'vendas/bordero',
            'relatorio/controle-caixa',
            'relatorio/vendas-cancelamentos-analitico',
            'relatorio/vendas-cancelamentos-sintetico',
            'relatorio/repasses',
            'relatorio/detalhamento-eventos',
        ],
        'Reserva de assentos'=>[
            'vendas/reserva',
            'vendas/estadio',
            'vendas/produto-por-tag',
            'vendas/mudar-status',
            'vendas/reservar-produto',
            'vendas/checar-documento',
        ],
        'Ocupantes'=>[
            'ingressos/ocupantes'
        ],
        'Usuários'=>[
            'usuarios/cadastro',
            'usuarios/ficha',
        ],
        'Caixa' => [
            'caixa/index',
            'caixa/operacao',
            'caixa/acao-caixa',
            'caixa/salvar-relatorio',
            'caixa/relatorio',
            'caixa/relatorio-vendas',
            'caixa/detalhes',
            'caixa/estorno',
            'caixa/exportar-csv-resumo',
            'caixa/exportar-csv-vendas'
        ],
        'Financeiro' => [
            'financeiro/index',
            'financeiro/boleto'
        ],
        'Gráfica' => [
            'grafica/index',
            'grafica/reservar',
            'grafica/lote-estadio',
            'grafica/lote-producao',
            'grafica/exportar-csv',
            'grafica/exportar-csv-sem-lote',
            'grafica/status-lote-producao',
            'grafica/status-lote-estadio',
        ],
        'Sistema' => [
            'sistema/manutencao',
        ],
        'Museu' => [
            'museu/registra-vendas'     
        ],
        'Ingressos' => [
            'venda/ingressos',
            'grafica/exportar-csv',
            'grafica/exportar-csv-sem-lote',
            'ingressos/consultar',
            'ingressos/associar-ingressos',
            'ingressos/lote-entrega',
            'ingressos/status-lote-entrega',
            'ingressos/invalidos',
            'ingressos/bloquear-ingresso',
            'ingressos/segunda-via-ingresso',
            'ingressos/invalidar-ingresso',
            'ingressos/acompanhar',
            'ingressos/lote-liberar',
            'ingressos/status-lote-liberar'
        ],
        'Caixa' => [
            'caixa/index',
            'relatorio-vendas'
        ],        
      ];
    }

    /**
     * Lista de permissões especiais - apenas administradores podem conceder
     */
    public function listaEspecial(){
      array_push($this->listaPermissoes,[
        'Permissões'=>[
          'usuarios/permissoes',
          'usuarios/listar-permissoes',
        ]
      ]);
    }

    /**
     * Regras para o modelo. Usuário, controle/acao e inativo são obrigatórios
     */
    public function rules()
    {
        return [
            [['usuario','permissoesUsuario','controleAcao','inativo'], 'required'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'listaPermissoes' => 'Lista de permissões',
            'permissoesUsuario' => 'Permissões',
            'usuario' => 'Usuário (e-mail)',
            'controleAcao' => 'Ação',
            'inativo' => 'Desativado'
        ];
    }

    /**
     * Lista de permissões simplificada para dropDown
    */
    public function listaResumida(){
      return array_combine(array_keys($this->listaPermissoes),array_keys($this->listaPermissoes));
    }

    /**
     * pegar a lista de nomes de permissão a partir do formulário enviado
     */
    public function salvarListaPermissoes(){
      foreach($this->permissoesUsuario as $k){
        foreach($this->listaPermissoes[$k] as $controllerAction){
          $this->controleAcao=$controllerAction;
          $this->salvarPermissao();
        }
      }
      return !$this->hasErrors();      
    }

    /**
     * Salvar uma permissão a um usuário
     */
    public function salvarPermissao(){
      $save=(new ApiPermissoes)->incluirPermissao($this->usuario,$this->controleAcao);
      if(!$save->successo){
        switch($save->erro->codigo){
          case 394:           
            $this->addError('permissoesUsuario',$save->erro->mensagem);
            break;
          case 071:
          default:
            $this->addError('usuario',$save->erro->mensagem);
            break;
        }
      }
    }

    /**
     * Lista de permissões de um usuário
     */
    public function listaPermissoesUsuario(){
      $lista=(new ApiPermissoes)->consultarPermissoes($this->usuario);
      if($lista->successo=='0'){
        $this->addError('usuario','Erro ao consultar permissões ('.$lista->erro->mensagem.')');
        return false;
      } 
      return $lista->objeto;
    }

    /**
     * Lista de permissões de um usuário agrupados para entendimento do operador
     */
    public function listaGrupoPermissoesUsuario(){
      $lista=$this->listaPermissoesUsuario();
      if($lista==false) return false;
      $grupoAtivo=[];
      $grupoInativo=[];
      foreach ($this->listaPermissoes as $k=>$v) {
        foreach ($lista as $l) {
          if(in_array($l->permissao,$v)){
            if($l->inativo && !in_array($k,$grupoInativo))
              $grupoInativo[]=$k;
            if(!$l->inativo && !in_array($k,$grupoAtivo))
              $grupoAtivo[]=$k;
          }
        }
      }
      return [$grupoAtivo,$grupoInativo];
    }

    /**
     * Selecionar um grupo de permissões para revogar ou ativar o acesso - faz uma chamada à WS de cada permissão do grupo
     */
    public function ativarRevogarGrupoPermissao(){
      $permissoes=$this->listaPermissoes[$this->permissoesUsuario];
      if(is_array($permissoes)){
        foreach ($permissoes as $_controleacao) {
          $this->controleAcao=$_controleacao;
          $this->ativarRevogarPermissao();
        }
      } 
      else 
        $this->addError('permissoesUsuario','Não foi informado um grupo de permissões válido');

      return !$this->hasErrors();
    }

    /**
     * Revogar uma permissão - muda o inativo para 1 em uma permissão para um usuário
     */
    public function ativarRevogarPermissao(){
      if($this->validate()){
        $acao=(new ApiPermissoes)->ativarDesativarPermissao($this->usuario, $this->controleAcao,$this->inativo);
        if($acao->successo=='0')
          $this->addError('usuario',$acao->erro->mensagem);
      }
    }
}
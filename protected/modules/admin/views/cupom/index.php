<?php
use \yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\assets\CupomAsset;

CupomAsset::register($this);
$this->title = 'Cupom - Consulta' . (isset($matriz) && $matriz == true ? ' (matriz)' : '');
?>

<div class="container">
	<div class="row">
		<h2 class="tituloGeral"><i class="fa fa-clone"></i> Cupom - Consulta<?= isset($matriz) && $matriz == true ? ' (matriz)' : ''; ?></h2>
	</div>	
	
	<?php $form = ActiveForm::begin(['id' => 'consulta-form']); ?>
	<div class="col-xs-10">
		<?php $model->id_agenda = $id_agenda; ?>
		<?= $form->field($model, 'id_agenda')->dropDownList(ArrayHelper::map($eventos, 'codigo', 'data', 'evento'), ['class'=>'apareceSetSelect']); ?>
	</div>
	<div class="col-xs-2" style="padding-top:25px;">
		<?= Html::submitButton('Consultar cupons', ['class' => 'btn btn-primary', 'id' => 'btn-consulta']) ?>
	</div>
	<?php ActiveForm::end(); ?>
	
    <hr>        
<?php
if (!empty($id_agenda) && is_array($model->lista) && count($model->lista) > 0) {
?>
        <div class="col-xs-12">
            <input type="hidden" id="url-del-cupom" value="<?= Url::to(['remover-cupom']); ?>">
            <h2>Cupons cadastrados:</h2>
			<?php foreach ($model->lista as $cupom) { ?>
			<div class="gridTabela">
				<div><h3><?= $cupom->nome; ?></h3></div>
				<div><p><?= $cupom->descricao; ?></p></div>
				<div><p><strong>Desconto:</strong><br> <?= $cupom->desconto_fixo > 0 ? 'R$ ' . number_format($cupom->desconto_fixo, 2, ',', '.') : number_format($cupom->desconto_percentual, 2, ',', '.') . ' %'; ?></p></div>
				<div><p><strong>Cupom válido de:</strong><br> <?= !empty($cupom->validade_inicial) ? $cupom->validade_inicial : '-'; ?> até <?= !empty($cupom->validade_final) ? $cupom->validade_final : '-'; ?></p></div>
				<div><p><strong>Cumulativo:</strong><br> <?= $cupom->cumulativo; ?></p></div>
				<div><p><strong>Status:</strong><br> <?= $cupom->ativo; ?></p></div>
				<div><p>
					<strong>Estoque:</strong><br> 
					Disponíveis = <?= (int)$cupom->estoque->disponiveis; ?><br>
					Oferecidos = <?= (int)$cupom->estoque->oferecidos; ?><br> 
					Utilizados = <?= (int)$cupom->estoque->utilizados; ?><br>
					Reservados = <?= (int)$cupom->estoque->reservados; ?>
				</p></div>
			</div>
            <p><a href="<?= Url::to(['editar-cupom', 'cupom' => $cupom->id]) ?>">Editar cupom</a> | 
            <?php if ($cupom->estoque->utilizados == 0 && $cupom->estoque->reservados == 0) { ?>
            <a href="javascript:;" class="del-cupom" data-cupom="<?= $cupom->id; ?>">Remover cupom</a> | 
            <?php } ?>
            <a href="<?= Url::to(['estoque-cupom', 'cupom' => $cupom->id]) ?>">Estoque do cupom</a></p>
            <?php if($cupom->tipo=='privado') {
                $idsEventoData=explode('_',$id_agenda);
                $url_venda_externa=Url::to(['/agenda/venda-externa','evento'=>$idsEventoData[0],'cupom'=>$cupom->id],1);
                $promocode=substr($cupom->id, strrpos($cupom->id, '_')+1);
                ?>
                <p>ID do Cupom - Copie e mande este ID para o(s) comprador(es):</p>
                <input type="text" readonly name="input-cupom" id="input-cupom" value="<?=$promocode?>" />
                
            <?php }?>
            <hr>
			<?php } ?>
        </div>
<?php } else if (!is_array($model->lista) || count($model->lista) == 0) { ?>
        <div class="col-xs-12">
            <h4>Não há cupons disponíveis.</h4>
        </div>
<?php } ?>
    </div>
</div>
<br><br><br>
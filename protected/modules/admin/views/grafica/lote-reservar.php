<?php
use app\modules\admin\assets\GraficaAsset;

GraficaAsset::register($this);
use yii\widgets\ActiveForm;

$this->title = 'ADM Estádio Mineirão :: Reservar lote';

$codigoMaior = 0;
if (isset($vendas) && !empty($vendas) && !empty($vendas->objeto)) {
    foreach ($vendas->objeto as $index => $venda) {
        if ($venda->vendas->status == 'Autorizado' && $venda->vendas->codigo > $codigoMaior) {
            $codigoMaior = $venda->vendas->codigo;
        }
    }
}
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h2 class="tituloGeral"><i class="fa fa-clone"></i> Reservar lote</h2> 
            <?php if ($retorno != '') {?>
                <p class="alert alert-<?= $retorno['tipo']; ?>"><?= $retorno['msg']; ?></p>
            <?php }?>
        </div>
        <div class="col-xs-4">
            <?php $form = ActiveForm::begin(['options'=>['class'=>'form form-horizontal']]);?>
            <label for="idEvento">Eventos</label>
            <select class="form-control" name="idEvento">
                <?php foreach($eventos as $index => $evento) { ?>
                <option value="<?= $evento->codigo ; ?>"<?= isset($codigo_evento) && $codigo_evento == $evento->codigo ? ' selected' : ''; ?>><?= $evento->nome; ?></option>
                <?php } ?>
            </select>
            <button type="submit" class="btn btn-success btn-evento"><i class="fa fa-check"></i> Buscar</button>
            <?php if (!empty($vendas) && !empty($vendas->objeto)) { ?>
            <label for="codigoCompra">Código de corte (venda)</label>
            <input class="form-control" name="codigoCompra" value="<?= $codigoMaior; ?>" /><br />
            <p><input type="submit" class="btn btn-primary" value="Reservar" /></p>
            <?php } ?>
            <?php ActiveForm::end();?>
        </div>
        <?php if (isset($vendas) && !empty($vendas) && !empty($vendas->objeto)) { ?>
        <div class="col-xs-8">
            <div class="col-xs-12 lote-reserva">
                <table class="uppercase table table-responsive table-striped">
                    <thead>
                        <tr>
                            <th colspan="5" scope="col" class="aliCentro">Vendas autorizadas após último corte (<?= !empty($ultimo_corte) ? $ultimo_corte : '-' ?>)</th>
                        </tr>
                        <tr>
                            <th>Identificador</th>
                            <th>Data</th>
                            <th>Total</th>
                            <th>Nome</th>
                            <th>Documento</th>
                        </tr>
                    </thead>
                    <tbody> 
            <?php
            foreach($vendas->objeto as $index => $venda) { 
                if ($venda->vendas->status == 'Autorizado') {
            ?>
                        <tr>
                            <td><?= $venda->vendas->codigo; if($venda->vendas->tipo == 'Taxa - 2ª via cartão mifare') echo ' <small>2ª via de cartão</small>'?></td>
                            <td><?= $venda->vendas->data; ?></td>
                            <td><?= 'R$ ' . number_format($venda->vendas->total, 2, ',', '.'); ?></td>
                            <td><?= $venda->vendas->comprador->nome; ?></td>
                            <td><?= $venda->vendas->comprador->documento; ?></td>
                        </tr>                    
            <?php
                }
            } 
            ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php } else if (!empty($ultimo_corte)) { ?>
        <div class="col-xs-8">
            Não há registros para a opção escolhida.
        </div>
        <?php } ?>
    </div>
    <hr style="margin-top: 10px;">   
</div>
var relatorio;
var controleCaixa = false;
        
$(document).ready(function () {
    
    $("#botCxStatus").click(function() {
        $("#botCxStatusSt").slideToggle("slow");
    });
    
    $('#btn-abrir').click(function () {
        $('#saida').val('0');
        $('#ajuste').val('0');
        $('#valor').val(parseFloat($('#caixa-abrir').val()));
        acaoCaixa();
    });    
    
    $('#btn-reforco').click(function () {
        if ($('#caixa-reforco').val() == '' || $('#caixa-reforco').val() == 0 || isNaN($('#caixa-reforco').val())) {
            showMsg('O valor deve ser um número maior que zero.');
            $('#caixa-reforco').focus();
            return false;
        }
        $('#saida').val('0');
        $('#ajuste').val('1');
        $('#valor').val($('#caixa-reforco').val());
        acaoCaixa();
    });    
    
    $('#btn-sangria').click(function () {
        if ($('#caixa-sangria').val() == '' || $('#caixa-sangria').val() == 0 || isNaN($('#caixa-sangria').val())) {
            showMsg('O valor deve ser um número maior que zero.');
            $('#caixa-sangria').focus();
            return false;
        }
        $('#saida').val('1');
        $('#ajuste').val('1');
        $('#valor').val($('#caixa-sangria').val());
        acaoCaixa(); 
    });
    
    $('#btn-fechar').click(function () {
        if ($('#caixa-fechar').val() == '' || $('#caixa-fechar').val() == 0 || isNaN($('#caixa-fechar').val())) {
            showConfirmCallback('O caixa não possui valor final. Fechar o caixa mesmo assim?', printFechamento);
            return false;
        }
        printFechamento();
    });
    
    $('#caixa-fechar').change(function () {
        controleCaixa = true;
        var valor = $('#totalCaixa').val() - $(this).val();
        $('#quebraCaixa').html('R$ ' + valor.toFixed(2).replace('.', ',')).addClass(valor > 0 ? 'plus-green' : 'plus-red');
        $('#caixa-saldo').val(valor);
    });
    
    $('#btn-print-resumo').click(function() {
        printResumo();
    });
});

function printResumo () {
    var total = 0;
    $('#resumo-abertura').html($('#td-abertura').html());
    total += parseFloat($('#caixa-aberto').val());
    $('#resumo-reforco').html('R$ ' + parseFloat($('#caixa-reforco').val()).toFixed(2).replace('.', ','));
    total += parseFloat($('#caixa-reforco').val());
    $('#resumo-sangria').html('R$ ' + parseFloat($('#caixa-sangria').val()).toFixed(2).replace('.', '.'));
    total += parseFloat($('#caixa-sangria').val());
    $('#resumo-saldo').html('R$ ' + total.toFixed(2).replace('.', ','));
    var resumo = $('#div-resumo');
    printWindow(resumo.html());
}

function printFechamento () {
    var total = 0;
    total += parseFloat($('#caixa-aberto').val());
    var valor = parseFloat($('#caixa-reforco').val()).toFixed(2).replace('.', ',');
    $('#vlr-reforco').html('R$ ' + valor);
    total += parseFloat($('#caixa-reforco').val());
    $('#vlr-sangria').html('R$ ' + parseFloat($('#caixa-sangria').val()).toFixed(2).replace('.', ','));
    total -= parseFloat($('#caixa-sangria').val());
    total += parseFloat($('#vlr-venda').val());
    total -= parseFloat($('#vlr-estorno').val());
    $('#vlr-fechamento').html('R$ ' + parseFloat($('#caixa-fechar').val()).toFixed(2).replace('.', ','));
    total -= parseFloat($('#caixa-fechar').val());
    $('#vlr-saldo').html('R$ ' + total.toFixed(2).replace('.', ','));
    var relatorio = $('#div-fechamento');
    printWindow(relatorio.html());
    fecharCaixa();
}

function fecharCaixa() {
    $('#saida').val('1');
    $('#ajuste').val('0');
    $('#valor').val($('#caixa-fechar').val());
    acaoCaixa(); 
}

function acaoCaixa () {
    doAjax ($('#url-acao').val(), 'POST', $('#form-caixa').serialize(), 'JSON', 10000, 'acaoCaixaOK', 'acaoCaixaErr');
    return false;
}

function acaoCaixaOK (data) {
    showMsg('Ação efetuada com sucesso.');
    if (controleCaixa) {
        salvarRelatorio();
    }
    redirPage($('#url-operacao').val());
}

function acaoCaixaErr (data) {
    showMsg('Falha ao executar ação. Erro: ' + data.erro.mensagem);
    return false;
}

function salvarRelatorio () {
    var data = {
        'abertura': $('#caixa-aberto').val(), 
        'reforco': $('#caixa-reforco').val(), 
        'sangria': $('#caixa-sangria').val(), 
        'fechamento': $('#caixa-fechar').val(), 
        'vendas': $('#caixa-venda').val(), 
        'estornos': $('#caixa-estorno').val(), 
        'saldo': $('#caixa-saldo').val()
    };
    doAjax ($('#url-relatorio').val(), 'POST', data, 'JSON', 10000, 'relatorioOK', 'relatorioErr');
}

function relatorioOK (data) {
    reloadPage();
}

function relatorioErr (data) {
    showMsg('Erro ao gravar relatório: ' + data.erro.mensagem);
}
<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\WsAgenda;
use app\models\Anexos;
use app\models\Cliente;
use yii\web\UploadedFile;
use app\models\Tags;

class Agenda extends Model {

    public $lista;
    public $codigo;
    public $nome;
    public $descricao;
    public $dataInicial;
    public $dataFinal;
    public $dataAtivar;
    public $dataDesativar;
    public $ativo;
    public $tags;
    public $anexos;
    public $destaques;
    public $produtos;
    public $data;
    public $datas;
    public $unidade;
    public $primeiroHorario;
    public $tipoEvento;

    //cadastro completo
    public $sinopse;
    public $direcao;
    public $atores;
    public $premios;
    public $duracao;
    public $classificacao;
    public $cupom;

    //anexos
    public $imgEvento;
    public $imgMosaico;
    public $imgDestaque;
    public $imgDestaqueApp;

    //tags
    public $destaque;

    //produtos
    public $preco;

    //empresa
    public $empresa;

    //espaços
    public $listaEspacos;
    public $espaco;

    // eventos em cartaz
    public $listaEmCartaz;
    
    /**
     * Método construtor sempre pede a unidade atual
    */
    public function __construct(Cliente $unidade){
        $this->unidade = $unidade;
    }

    /**
     * Regras de validação. Nome, data e ativo são obrigatórios (data é um array com dois ou três índices)
     */
    public function rules() {
        return [
            [['nome', 'dataInicial', 'dataFinal', 'ativo'], 'required'],
            [['descricao'], 'required', 'on'=>'novoEvento'],
            [['nome', 'dataInicial', 'dataFinal', 'ativo', 'imgEvento', 'duracao', 'preco', 'espaco'], 'required', 'on'=>'novoEventoCompleto'],
            [['nome', 'dataInicial', 'dataFinal', 'ativo', 'duracao'], 'required', 'on'=>'update'],
            [['data'], 'required', 'on'=>'novaData'],
            [['descricao', 'sinopse', 'data', 'datas', 'dataAtivar', 'dataDesativar', 'direcao', 'atores', 'premios', 'classificacao', 'destaque', 'imgMosaico', 'imgDestaque', 'imgDestaqueApp', 'cupom', 'tipoEvento'], 'safe'],
        ];
    }

    /**
     * definição dos labels de atributos
     */
    public function attributeLabels() {
        return[
            'lista' => 'Lista',
            'nome' => 'Nome',
            'descricao' => 'Descrição',
            'dataInicial' => 'Início do evento',
            'dataFinal' => 'Fim do evento',
            'dataAtivar' => 'Exibir no site em',
            'dataDesativar' => 'Remover exibição do site em',
            'data' => 'Data',
            'datas' => 'Datas',
            'ativo' => 'Ativo',
            'tags' => 'Tags',
            'anexos' => 'Anexos',
            'sinopse' => 'Ficha técnica - Sinopse',
            'direcao' => 'Ficha técnica - Direção',
            'atores' => 'Atores',
            'premios' => 'Prêmios',
            'duracao' => 'Duração',
            'classificacao' => 'Classificação etária',
            'destaque' => 'Evento em destaque',
            // 'destaque' => 'Evento em destaque (marque para habilitar o upload da imagem)',
            'preco' => 'Preço dos ingressos',
            'cupom' => 'Cupom',
            'espaco' => 'Espaço',
            'listaEspacos' => 'Espaço',
        ];
    }

    /**
     * Consulta eventos com a tag destaque=1 e com anexo imgDestaque
     */
    public function listaEventosDestaque($rede=0) {
        $wsAgenda=new WsAgenda($this->unidade->user_token);
        $filtro = ['ativo' => 1, 'tags'=>[['nome'=>'destaque','valor'=>'1']], 'rede'=>(int)$rede];
        $eventos = $wsAgenda->listarEventos($filtro);
        if (!$eventos->successo)
            return $this->addError('destaques', $eventos->erro->mensagem);

        foreach ($eventos->objeto as $evento) {
            if(!isset($evento->anexos)) continue;
            $evento->anexos=Anexos::objetoAnexos($evento->anexos);
            if (time() > strtotime(\DateTime::createFromFormat('d/m/Y H:i:s',$evento->data->final)->format('Y-m-d H:i:s')) || !isset($evento->anexos->imgDestaque))
                continue;

            $obj=new Agenda($this->unidade);
            $obj->loadFromApi($evento);
            $this->destaques[] = $obj;
        }
    }

    /**
     * Consulta eventos com ativo=1 desconsiderando os que tem data final no passado e o evento matriz da unidade
     * @param bool $rede se a pesquisa será feita em toda a rede ou apenas na unidade atual. O padrão é true
     * @todo dar a opção de desconsiderar os destaques que estiverem presentes em $this->destaques
     */
    public function listaEventosPublicosFuturo($rede=1) {
        $wsAgenda=new WsAgenda($this->unidade->user_token);
        $filtro = ['ativo' => 1, 'rede'=>(int)$rede];
        
        $eventos = $wsAgenda->listarEventos($filtro);
        if (!$eventos->successo)
            return $this->addError('lista', $eventos->erro->mensagem);

        foreach ($eventos->objeto as $evento) {
            $obj=new Agenda($this->unidade);
            $obj->loadFromApi($evento);
            // não mostrar eventos no passado
            if (time() > strtotime(\DateTime::createFromFormat('d/m/Y H:i:s',$obj->dataFinal)->format('Y-m-d H:i:s')))
                continue;
            // não mostrar eventos que tiverem definição de data ativar/desativar
            if($this->eventoNaoAtivo($obj, time()))
                continue;
            // separar eventos em cartaz em outra lista
            if($this->eventoEmCartaz($obj,time())){
                $this->listaEmCartaz[]=$obj;
                continue;
            }
            // ignorar eventos com nome MATRIZ (só usados internamente)
            if($obj->nome=='MATRIZ')
                continue;
            $this->lista[] = $obj;
        }
    }

    /**
     * Consulta todos os eventos. Espera-se que o user_token esteja na propriedade $unidade
     * @param string $ignore Informação que precisa ser ignorada
     */
    public function listaTodosEventos($ignore='') {
        $api = new WsAgenda($this->unidade->user_token);
        $eventos = $api->listarEventos();
        if (!$eventos->successo)
            return $this->addError('lista', $eventos->erro->mensagem);

        foreach ($eventos->objeto as $evento) {
            if ($ignore == '' || (!empty($ignore) && !stristr($evento->nome, $ignore))) {
                $obj = new Agenda($this->unidade);
                $obj->loadFromApi($evento);
                $this->lista[] = $obj;
            }
        }
    }

    /**
     * Consulta um evento pelo código
     */
    public function umEvento() {
        $api = new WsAgenda($this->unidade->user_token);
        // $evento = $api->buscarEventoRede($this->codigo,['rede'=>1]);
        $evento = $api->buscarEvento($this->codigo);
        if (!$evento->successo)
            return $this->addError('codigo', $evento->erro->mensagem);

        $this->loadFromApi($evento->objeto[0]);
        
        if($this->nome=='MATRIZ')
            return $this->addError('codigo', 'Evento não encontrado');
        
        $this->anexos=(new Anexos)->objetoAnexos($this->anexos,0);
        
    }

    public function selecionouOutraUnidade($unidade)
    {   
        $session = Yii::$app->session;
        $session->open();        
        $documento_unidade = $session->get('documento_unidade',false);
        if(!empty($documento_unidade) && $documento_unidade != $unidade ){
            $mensagem = "Você já iniciou uma compra com outra unidade. Finalize-a antes de comprar para outro evento";
            return (object) ['successo'=>'0', 'erro'=>['mensagem'=>$mensagem]];
        }
        return (object)['successo'=>'1'];
    }    

    /**
     * Organizar as datas de forma que os horários de um mesmo dia fiquem agrupados e não repita o mesmo dia com horários diferentes
     */
    public function separarHorariosDoMesmoDia(){
        $_dias=[];
        foreach ($this->datas as $data) {
            $_dias[$data->dia]['horarios'][]=['id'=>$data->id,'hora'=>$data->hora];
        }
        $this->primeiroHorario=$this->datas[0]->id;
        return $_dias;
    }

    /**
     * Cadastrar novo evento. Deve seguir as regras de validação (método rules)
     * @todo somente um usuário logado como operador 
     */
    public function criarEvento() {
        if ($this->validate()) {
            $api = new WsAgenda($this->unidade->user_token);
            $cadastro = $api->criarEvento([
                'nome' => $this->nome,
                'descricao' => $this->direcao,
                'data' => [
                    'inicial' => $this->dataInicial,
                    'final' => $this->dataFinal,
                    'ativar' => $this->dataAtivar,
                    'desativar' => $this->dataDesativar
                ],
                'ativo' => $this->ativo,
                'sinopse' => $this->sinopse,
                'direcao' => $this->direcao,
                'atores' => $this->atores,
                'premios' => $this->premios,
                'duracao' => $this->duracao,
                'classificacao' => $this->classificacao,
            ]);
            /*echo "<pre>";
            echo 'criarEvento <br />';
            print_r($cadastro);
            echo "</pre>";*/
            if (!$cadastro->successo){
                $this->addError('nome', $cadastro->erro->mensagem);
                return false;
            }
            $this->codigo=$cadastro->objeto->codigo;
            return true;
        }
    }

    /**
     * upload das imagens do evento
     * @param string $attr o atributo desse model o qual a imagem se refere
     * @param int $evento 
     */
    private function uploadImgEvento($attr,$post){
        $modelAnexos=new Anexos;
        $modelAnexos->scenario='create';
        $modelAnexos->load($post);
        $modelAnexos->imgBase64(UploadedFile::getInstance($modelAnexos, $attr));            
        if($modelAnexos->validate()){
            return $modelAnexos->adicionar($this->getWs(),$evento);
        }        
    }

    /**
     * Editar registro de evento
     * @todo somente um usuário logado como operador 
     */
    public function atualizarEvento() {
        if ($this->validate()) {
            $api = new WsAgenda($this->unidade->user_token);
            $cadastro = $api->alterarEvento([
                'codigo' => $this->codigo,
                'nome' => $this->nome,
                'descricao' => $this->descricao,
                'data' => [
                    'inicial' => $this->dataInicial,
                    'final' => $this->dataFinal,
                    'ativar' => $this->dataAtivar,
                    'desativar' => $this->dataDesativar
                ],
                'ativo' => $this->ativo,
                'sinopse' => $this->sinopse,
                'direcao' => $this->direcao,
                'atores' => $this->atores,
                'premios' => $this->premios,
                'duracao' => $this->duracao,
                'classificacao' => $this->classificacao,
            ]);
            if (!$cadastro->successo)
                return $this->addError('ativo', $cadastro->erro->mensagem);
            return true;
        }
    }

    /**
     * Consulta um evento pelo ID
     */
    public function buscarEvento() {
        $api = new WsAgenda($this->unidade->user_token);
        $evento = $api->buscarEvento($this->codigo);
       
        if (!$evento->successo){
            $this->addError('lista', $evento->erro->mensagem);
            return false;
        }
        return $this->loadFromApi($evento->objeto[0]);
    }

    /**
     * Atribuir valores retornado pela API às propriedades
     */
    public function loadFromApi($objeto) {
        if (!isset($objeto->codigo, $objeto->nome, $objeto->data->inicial, $objeto->data->final, $objeto->ativo))
            return false;
        $this->codigo = $objeto->codigo;
        $this->nome = $objeto->nome;
        $this->descricao = $objeto->descricao;
        $this->dataInicial = $objeto->data->inicial;
        $this->dataFinal = $objeto->data->final;
        $this->dataAtivar = $objeto->data->ativar;
        $this->dataDesativar = $objeto->data->desativar;
        $this->ativo = $objeto->ativo;
        $this->tags = isset($objeto->tags) ? $objeto->tags : null;
        $this->anexos = isset($objeto->anexos) ? $objeto->anexos : null;
        $this->datas = isset($objeto->datas) ? $objeto->datas : null;
        $this->sinopse = $objeto->sinopse;
        $this->direcao = $objeto->direcao;
        $this->atores = $objeto->atores;
        $this->premios = $objeto->premios;
        $this->duracao = $objeto->duracao;
        $this->classificacao = $objeto->classificacao;
        $this->empresa = $objeto->empresa;
        return true;
    }

    /**
     * Excluir um evento
     */
    public function excluirEvento(){
        if(!isset($this->codigo)) return false;
        $wsAgenda=new WsAgenda($this->unidade->user_token);
        return $wsAgenda->excluirEvento(['codigo'=>$this->codigo]);
    }

    /**
     * Verificar se um evento pode ser comprado considerando: 
     * propriedades codigo, nome e tags do evento devem existir
     * o evento está ativo ($ativo=1)
     * existem produtos com a tag "matriz" igual ao apelido do evento;
     * tem ingresso no estoque
     */
    public function disponivelParaCompra($evento) {
        if (!isset($evento->codigo, $evento->nome, $evento->tags) || $evento->ativo != '1')
            return false;
        $this->produtos = new \app\models\Produto($evento->unidade);
        $tags = Tags::objetoTags($evento->tags);
        $this->produtos->listarSetoresProdutosAtivosDeEvento($tags->apelido);
        return isset($this->produtos->lista) && is_array($this->produtos->lista) && count($this->produtos->lista) > 0;
    }

    /**
     * Remover tag
     */
    public function removerTag($nome){
        $ws=new WsAgenda($this->unidade->user_token);
        if(!$ws->excluirTag(['codigo'=>$this->codigo,'nome'=>$nome]))
            $this->addError('tags',$ws->erro->mensagem);
        return true;
    }

    /**
     * Remover anexo
     */
    public function removerAnexo($nome,$tipo){
        $ws=new WsAgenda($this->unidade->user_token);
        if(!$ws->excluirAnexo(['codigo'=>$this->codigo,'nome'=>$nome,'tipo'=>$tipo]))
            $this->addError('anexos',$ws->erro->mensagem);
        return true;
    }

    /**
     * Converter a data do evento para diversos formatos
     */
    public static function dataEventoFormato($data, $formatoObjeto, $formatoCliente){
        $dataObj= \DateTime::createFromFormat($formatoObjeto,$data);
        return \IntlDateFormatter::formatObject($dataObj, $formatoCliente,'pt_BR');
    }
    
    /**
     * Formatar tags de um evento no formato key => value
     * @return array
     */
    public function formatarTags() 
    {
        if (!empty($this->tags)) {
            $tags = [];
            foreach ($this->tags as $index => $tag) {
                $tags[$tag->nome] = $tag->valor;
            }
            return $tags;
        }
        return [];
    }

    /**
     * Adiciona uma data a um evento existente
     */
    public function adicionarDataEvento($data){
        $this->data=$data;
        $ws=new WsAgenda($this->unidade->user_token);
        if(!$this->validate()) return $ws->getApiError('A data é inválida');
        return $ws->adicionarData(['codigo'=>$this->codigo,'data'=>$data]);
    }

    /**
     * Trazer as datas como string para exibição
     */  
    public function stringDatas($datas){
        if(is_array($datas))
            foreach ($datas as $data)
                echo $data->data.', ';
    }

    /**
     * Remover anexo
     */
    public function removerData($data){
        $ws=new WsAgenda($this->unidade->user_token);
        $excluir=$ws->excluirData(['codigo'=>$this->codigo,'data'=>$data]);
        if(!$excluir->successo)
            $this->addError('datas',$excluir->erro->mensagem);
        return true;
    }

    /**
     * Montar uma lista (array) com as datas do evento para manipular com ArrayHelper
     * @param string $ignore Informação que precisa ser ignorada
     */
    public function listaDatasEventos($ignore=''){
        $eventos=[];
        if(sizeof($this->lista)) {
            foreach ($this->lista as $key => $value) {
                if (!empty($value->datas)) {
                    foreach ($value->datas as $data) {
                        if ($ignore == '' || stristr($value->nome, $ignore) != true) {
                            $eventos[] = [
                                'codigo' => $value->codigo . '_' . $data->id, 
                                'evento' => $value->nome, 
                                'id' => $data->id, 
                                'data' => $data->data
                            ];
                        }
                    }
                }
            }
        }
        return $eventos;
    }

    /**
     * Retorna o objeto do webservice WsAgenda com o objeto Cliente correspondente
     */
    public function getWs(){
        return new WsAgenda($this->unidade->user_token);
    }

    /**
     * Trazer uma data a partir do ID de agenda de um evento
     */
    public function getDataEventoProduto($id){
        if(!isset($this->datas) || !is_array($this->datas)) return (object)['data'=>$this->dataInicial];
        foreach ($this->datas as $data) {
            if($data->id=$id) return $data;
        }
    }

    /**
     * Consulta eventos por filtro
     */
    public function buscarEventosPorFiltro($post) {
        $api = new WsAgenda ($this->unidade->user_token);
        $eventos = $api->listarEventos($post);        
        if (!$eventos->successo){
            $this->addError('lista', $eventos->erro->mensagem);
            return false;
        }
        foreach ($eventos->objeto as $evento) {
            $obj=new Agenda($this->unidade);
            $obj->loadFromApi($evento);
            $this->lista[] = $obj;
        }

    }

    /**
     * Trazer o id do evento matriz da unidade
     */
    public function getIdDataMatriz(){
        $this->buscarEventosPorFiltro(['tags'=>[['nome'=>'matriz','valor'=>'base']]]);
        if(!$this->lista)
            return false;
        $evento=$this->lista[0];
        /*echo "<pre>";
        echo 'getIdDataMatriz <br />';
        print_r($evento);
        echo "</pre>";*/        
        if($evento->nome=='MATRIZ' && isset($evento->datas[0]->id))
            return $evento->datas[0]->id;
    }

    /**
     * Iniciar model já com valores para testes
    */
    public function testar(){
        $this->nome='Nome do Evento';
        $this->descricao='Descrição';
        $this->sinopse='Sinopse';
        $this->dataInicial='12/12/2019 10:00:00';
        $this->dataFinal='15/12/2019 10:00:00';
        $this->duracao='01:00:00';
        $this->preco=10;
    }

    /**
     * Salvar tag de destaque em um novo evento, se esta for a opção do operador
     */
    public function salvarTagDestaque(){
        if($this->destaque){
            $modelTags=new Tags;
            $modelTags->scenario='create';
            $post['Tags']['nome']='destaque';
            $post['Tags']['valor']='1';
            $post['Tags']['codigo']=$this->codigo;
            
            $modelTags->load($post);
            $salvar=$modelTags->adicionar($this->getWs(),$this->codigo);
            /*echo "<pre>";
            echo 'salvarTagDestaque <br />';
            print_r($salvar);
            echo "</pre>";*/
            
            if(!isset($salvar->successo))
                $this->addError('nome','Erro ao salvar a tag de destaque');
            if(isset($salvar->successo) && !$salvar->successo)
                $this->addError('nome',$salvar->erro->mensagem);
        }
    }
    
    /**
     * Para uma lista de eventos, identificar os ids referentes à data principal (usado para matrizes)
     */
    public function getAgendaData()
    {
        $eventos = [];
        if (!empty($this->lista)) {
            foreach ($this->lista as $index => $evento) {
                if (isset($evento->datas[0]->id)) {
                    $eventos[] = $evento->datas[0]->id;
                }
            }
        }
        return $eventos;
    }

    /**
     * Encontrar os espaços da unidade atual com base nos eventos matriz
     */
    public function listaEspacos(){
        if(!isset($this->unidade->user_token))
            return $this->addError('espacos','Token não foi declarado');
        $api = new WsAgenda($this->unidade->user_token);

        $eventosMatriz=$api->listarEventos([
            'rede'=>'0',
            'nome'=>'MATRIZ%',
            'tags'=>[
                ['nome'=>'Espaço']
            ]
        ]);
        if(!isset($eventosMatriz->successo))
            return $this->addError('espacos', 'Não foi possível pesquisar a matriz de eventos');
        if($eventosMatriz->successo == 0)
            return $this->addError('espacos', $eventosMatriz->erro->mensagem);
        $this->listaEspacos=[];
        foreach ($eventosMatriz->objeto as $evento) {
            $tags = Tags::arrayTags($evento->tags);
            if(!in_array($tags['Espaço'], $this->listaEspacos))
                $this->listaEspacos[$tags['Espaço']]=$tags['Espaço'];
        }
        array_unique($this->listaEspacos);
    }
    
    /**
     * Clonar anexos de um evento origem para um evento destino
     * @param type $post
     * @return type
     */
    public function clonarAnexos($post)
    {
        $ws = new WsAgenda($this->unidade->user_token);
        return $ws->clonarAnexos($post);
    }
    
    /**
     * Clonar tags de um evento origem para um evento destino
     * @param type $post
     * @return type
     */
    public function clonarTags($post)
    {
        $ws = new WsAgenda($this->unidade->user_token);
        return $ws->clonarTags($post);
    }

    /**
     * Retornar se o evento está ou não ativo, comparando o valores de dataAtivar e dataDesativar com outra data passada por parâmetro
     * @param object evento objeto do evento atual
     * @param string time o timestamp para usar de comparação
     */
    public function eventoNaoAtivo($evento, $time){
        if($evento->dataAtivar=='' || $evento->dataDesativar=='') return false;
        $dataAtivar=\DateTime::createFromFormat('d/m/Y H:i:s',$evento->dataAtivar)->format('Y-m-d H:i:s');
        $dataDesativar=\DateTime::createFromFormat('d/m/Y H:i:s',$evento->dataDesativar)->format('Y-m-d H:i:s');
        return strtotime($dataAtivar) > $time || strtotime($dataDesativar) < $time;
    }

    /**
     * Retornar se o evento está em cartaz, comparando o valores de dataInicial e dataFinal com outra data passada por parâmetro
     * @param object evento objeto do evento atual
     * @param string time o timestamp para usar de comparação
     */
    public function eventoEmCartaz($evento, $time){
        $dataInicial=\DateTime::createFromFormat('d/m/Y H:i:s',$evento->dataInicial)->format('Y-m-d H:i:s');
        $dataFinal=\DateTime::createFromFormat('d/m/Y H:i:s',$evento->dataFinal)->format('Y-m-d H:i:s');
        return strtotime($dataInicial) <= $time && strtotime($dataFinal) > $time;
    }

    /**
     * Verificar se existe um cupom para uma data específica e se pode ser utilizado, comparando ids e estoque
     * @param Cupom $cupom o objeto do cupom 
     * @param integer $idEvento o id do evento
     * @param object $data objeto da data a ser consultado o cupom
     */
    public function checkCupom($cupom,$idEvento,$data){
        $c=$cupom->lista[0];
        $prefix = substr($c->id, 0, strrpos($c->id, '_'));
        $arrPrefix=explode('_', $prefix);
        if($idEvento!=$arrPrefix[1])
            $this->addError('cupom','O cupom não é válido para este evento');
        if($data['id']!=$arrPrefix[2])
            $this->addError('cupom','O cupom não é válido para esta data');
        if($cupom->lista[0]->estoque->disponiveis<=0)
            $this->addError('cupom','Não existem mais cupons disponíveis');
        if(count($cupom->lista)>1)
            $this->addError('cupom','O id do cupom não é único');
        return !$this->hasErrors('cupom');
    }
}
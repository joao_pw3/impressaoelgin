 <?php
use yii\widgets\ActiveForm;
use app\modules\admin\assets\BilheteriaAsset;
use yii\bootstrap\Modal;
use yii\helpers\Url;

BilheteriaAsset::register($this);

$this->title = 'ADM Estádio Mineirão :: Ingressos inválidos';
?>
<div class="container">
    
    <div class="row">
        <h2 class="tituloGeral"><i class="fa fa-clone"></i> Ingressos inválidos</h2>
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'form-ingressos-invalidos',
    ]); ?>
    <div id="localizar" class="row boxLer">
        <div class="ingresso col-md-4">
            <label for="idEvento">Eventos</label>
            <select name="BilheteriaModel[idEvento]">
                <?php foreach($eventos as $index => $evento) { ?>
                <option value="<?= $evento->codigo ; ?>"<?= isset($post['idEvento']) && $post['idEvento'] == $evento->codigo ? ' selected' : ''; ?>><?= $evento->nome; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col-md-4">
            <button type="submit" id="consultar-ingresso" class="btn btn-success btn-consulta"><i class="fa fa-check"></i> Buscar</button>                            
        </div>    
    </div>
    
    <br><hr class="bordaPont">
    <?php ActiveForm::end(); ?>
    
    <div class="row">
        <div class="col-md-12" id="data-venda">
<?php 
if (!empty($resultadoBusca) && $resultadoBusca->successo == '1') {
    if (count($resultadoBusca->objeto) > 0) {
    ?>
            <input type="hidden" id="url-bloq-ingresso" value="<?= Url::to(['ingressos/bloquear-ingresso']); ?>">
            <input type="hidden" id="url-2via-ingresso" value="<?= Url::to(['ingressos/segunda-via-ingresso']); ?>">
            
            <table class="uppercase table table-responsive table-striped">
                <thead>
                    <tr>
                        <th>Qrcode</th>
                        <th>Ativo</th>
                        <th>Lote</th>
                        <th>Produto</th>
                        <th>Invalidado em</th>
                        <th>Motivo</th>
                        <th>Bloqueio</th>
                        <th>2ª via</th>
                    </tr>
                </thead>
                <tbody>            
        <?php
        foreach ($resultadoBusca->objeto as $index => $ingresso) {
            if (!isset($post['produto']) || empty($post['produto']) || (isset($post['produto']) && !empty($post['produto']) && stristr($ingresso->produto, $post['produto']))) {
        ?>
                    <tr>
                        <td><?= $ingresso->qrcode; ?></td>
                        <td><?= $ingresso->bloqueado == 0 ? 'Sim' : 'Não'; ?></td>
                        <td><?= $ingresso->lote_reserva; ?></td>
                        <td><?= $ingresso->produto; ?></td>
                        <td><?= !empty($ingresso->data_invalidado) ? date('d/m/Y H:i:s', strtotime($ingresso->data_invalidado)) : '-'; ?></td>
                        <td><?= $ingresso->motivo_invalidado; ?></td>
                        <td>
                            <?= $ingresso->bloqueado == 0 ? 
                                '<button class="btn btn-primary btn-bloq" data-evento="' . $post['idEvento'] . '" data-qrcode="' . $ingresso->qrcode . '" id="btnb-' . $ingresso->qrcode . '">Bloquear</button>' : 
                                'Bloqueado'; ?>
                        </td>
                        <td>
                            <?= $ingresso->bloqueado == 0 && empty($ingresso->data_segunda_via) ? 
                                '<button class="btn btn-info btn-2" data-evento="' . $post['idEvento'] . '" data-qrcode="' . $ingresso->qrcode . '" id="btn2-' . $ingresso->qrcode . '">2ª via</button>' : 
                                ($ingresso->bloqueado == 0 && !empty($ingresso->data_segunda_via) ? 
                                'Solicitada em ' . date('d/m/Y H:i:s', strtotime($ingresso->data_segunda_via)) : 
                                '-'); ?>
                        </td>
                    </tr>
    <?php 
            } 
        }
    ?>
                </tbody>
            </table>  
    <?php
    } else {
        echo '<p>Não há compra para as informações fornecidas.</p>';
    }
} else if (!empty(!empty($resultadoBusca)) && $resultadoBusca->successo == '0') { 
?>
    <div class="alert alert-warning"><?= $resultadoBusca->erro->mensagem; ?>.</div>
<?php } else { ?>
    <div class="alert alert-warning">Escolha o evento para o qual deseja identificar os ingressos inválidos.</div>
<?php } ?>
        </div>
    </div>
</div>

<?php Modal::begin([
    'header' => '<h4 align="center">Atenção</h4>',
    'size' => 'modal-md',
    'id' => 'modal-confirmar',
    'options' => [
        'class' => 'modal-center'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ],
    'footer' => '
       <div class="col-md-12" align="center">
           <button type="button" class="btn btn-danger" id="btn-cancel">Cancelar</button>
           <button type="button" class="btn btn-success" id="btn-continue">Continuar</button>
       </div>'
]);
?>
<?php Modal::end(); ?>
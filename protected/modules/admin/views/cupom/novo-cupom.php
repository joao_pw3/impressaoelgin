<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;
use app\assets\CupomAsset;
use kartik\number\NumberControl;

CupomAsset::register($this);

$form = ActiveForm::begin([
    'id' => 'cupom-form'
]);

$model->cumulativo = 0;
$model->ativo = 1;
$model->tipo = 'PUB';
$this->title = 'Cupom - Cadastrar' . (isset($matriz) && $matriz == true ? ' (matriz)' : '');
?>

<div class="container">
	<div class="row">
		<h2 class="tituloGeral"><i class="fa fa-clone"></i> Cupom - Cadastrar <?= isset($matriz) && $matriz == true ? ' (matriz)' : ''; ?></h2>
	</div>
	
	<div class="row">
		<div class="col-xs-6">
			<?= $form->field($model, 'id_agenda')->dropDownList(ArrayHelper::map($eventos, 'codigo', 'data', 'evento')) ?>
		</div>
		<div class="col-xs-6">
			<?= $form->field($model, 'nome')->textInput(['id' => 'nome']); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<?= $form->field($model, 'descricao')->textarea(['rows' =>5]); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-6">
			<div class="sidebyside">
				<div class="ajustTop">
					<label class="containerR sidebysideMargT">
						<input type="radio" name="desconto" value="desconto_fixo" class="checkbox-desconto" checked>
						<span class="checkmark"></span>
					</label>
				</div>
			</div>
			<div class="sidebyside largMax">		
				<?= $form->field($model, 'desconto_fixo')->textInput()->widget(NumberControl::className(), [
					'maskedInputOptions' => [
						'prefix' => 'R$ ',
						'groupSeparator' => '.',
						'radixPoint' => ',',
						'digits' => 2,
						'allowMinus' => false,
						'autoUnmask'=>true,
						'unmaskAsNumber'=>true
					],
					'displayOptions' => [
						'placeholder' => 'Desconto em R$',
						'class' => 'form-control kv-monospace',
						'id' => 'desconto_fixo'
					]
				]); ?>
			</div>
		</div>
		<div class="col-xs-6">
			<div class="sidebyside">
				<div class="ajustTop">
					<label class="containerR sidebysideMargT">
						<input type="radio" name="desconto" value="desconto_percentual" class="checkbox-desconto">
						<span class="checkmark"></span>
					</label>
				</div>
			</div>
			<div class="sidebyside largMax">
				<?= $form->field($model, 'desconto_percentual')->textInput()->widget(NumberControl::className(), [
					'disabled' => true,
					'maskedInputOptions' => [
						'suffix' => ' %',
						'groupSeparator' => '.',
						'radixPoint' => ',',
						'max' => 100,
						'digits' => 2,
						'allowMinus' => false,
						'autoUnmask'=>true,
						'unmaskAsNumber'=>true
					],
					'displayOptions' => [
						'placeholder' => 'Desconto em %',
						'class' => 'form-control kv-monospace',
						'id' => 'desconto_percentual'
					]
				]); ?>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-6">
			<?= $form->field($model, 'validade_inicial')->widget(DateTimePicker::className(), [
				'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
				'readonly' => true,
				'pluginOptions' => [
					'todayBtn' => true,
					'autoclose' => true,
					'format' => 'dd/mm/yyyy hh:ii',
					'startDate' => date('d/m/Y H:i'),
				],
				'options' => [
					'id' => 'validade_inicial'
				]
			]); ?>
		</div>
		<div class="col-xs-6">
			<?= $form->field($model, 'validade_final')->widget(DateTimePicker::className(), [
				'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
				'readonly' => true,
				'pluginOptions' => [
					'todayBtn' => true,
					'autoclose' => true,
					'format' => 'dd/mm/yyyy hh:ii',
					'startDate' => date('d/m/Y H:i'),
				],
				'options' => [
					'id' => 'validade_final'
				]
			]); ?>
		</div>
	</div>
	<div class="row" align="center">
		<div class="col-xs-4">
			<?= $form->field($model, 'tipo')->radioList(['PUB' => 'Público', 'PRIV' => 'Privado']); ?>    
		</div>
		<div class="col-xs-4">
			<?= $form->field($model, 'cumulativo')->radioList(['0' => 'Não', '1' => 'Sim']); ?>
		</div>
		<div class="col-xs-4">
			<?= $form->field($model, 'ativo')->radioList(['0' => 'Não', '1' => 'Sim']); ?>
		</div>
	</div>

	<div class="form-group" align="right">
		<?= Html::submitButton('Cadastrar', ['class' => 'btn btn-primary', 'id' => 'btn-cupom']) ?>
	</div>
	<?php ActiveForm::end(); ?>
	<br><br><br>
	
</div>
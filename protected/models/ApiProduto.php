<?php
namespace app\models;
use app\models\Api;
use yii\base\Exception;

/**
 * Classe para Produtos - Easy for Pay - Mineirão
 * obs.: parâmetros entre colchetes são opcionais
 */
class ApiProduto extends Api
{
    public $logs;
    public $produtos;
    
    /**
     * Criar produto permitindo verificar a existência
     * @param type $post Dados do produto: nome, [descricao], valor, [desconto] ([fixo], [percentual]), [validade] ([inicial], [final]), ativo
     * @param boolean $checkNomeExiste para buscar o produto com o mesmo post para evitar duplicatas
     * @return object {'successo': '1', 'codigo': <id do produto criado>} ou $generic um objeto genérico com valor successo=0 se for encontrado um registro com mesmo nome e o parâmetro $checkNomeExiste for true
     */
    public function criarProduto($post=[],$checkNomeExiste=true)
    {
        $this->setLog('Cadastro de produto');
        if (!empty($post) && is_array($post)) {
            $this->logs->log('Cadastrar produto', $post);
            if ($checkNomeExiste) {
                $verifProd = $this->buscarProduto(json_encode($post),null);
                if ($verifProd->successo == 1 && count($verifProd->objeto) == 1) {
                    $this->logs->log('Produto já existe');
                    return $this->getApiError('Produto já existe');
                }
            }   
            $returnApi = json_decode($this->call('produtos/', 'POST', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao cadastrar produto' : 'Produto cadastrado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Cadastro de produto inválido. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível cadastrar o produto.');
    }
    
    /**
     * Alterar produto
     * @param type $post Dados do produto: codigo, [nome], [descricao], [valor], [desconto] ([fixo], [percentual]), [validade] ([inicial], [final]), ativo
     * @return object {'successo': '1', 'alterado': '1'}
     */
    public function alterarProduto($post=[])
    {
        $this->setLog('Alteração de produto');
        if (!empty($post) && is_array($post) && isset($post['codigo']) && $post['codigo'] > 0) {
            $this->logs->log('Alterar produto', $post);
            $returnApi = json_decode($this->call('produtos/' . $post['codigo'], 'PUT', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao alterar produto' : 'Produto alterado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Alteração de produto inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível alterar o produto.');
    }
    
    /**
     * Excluir produto
     * @param type $post Dados do produto: codigo
     * @return object {'successo': '1', 'removido': '1'}
     */
    public function excluirProduto($post=[])
    {
        $this->setLog('Exclusão de produto');
        if (isset($post['codigo']) && $post['codigo'] > 0) {
            $this->logs->log('Excluir produto', $post);
            $returnApi = json_decode($this->call('produtos/' . $post['codigo'], 'DELETE'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao excluir produto' : 'Produto excluído com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Exclusão de produto inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível excluir o produto.');
    }
    
    /**
     * Consultar produto
     * @param json $post Parâmetros para busca
     * @param integer $id ID de um produto
     * @param boolean $cache Buscar registros com ou sem cache
     * @return object {'successo': '1', 'produtos': {...}}
     */
    public function buscarProduto($post=null, $id=null, $cache=true)
    {
        $this->setLog('Consulta de produto');
        $this->logs->log('Consultar produto', $post);
        $returnApi = json_decode($this->call('produtos/' . ($cache == true ? 'buscar' : 'buscarsemcache') . '/' . (isset($id) ? $id : ''), 'PUT', isset($post) ? $post : NULL, false));
        if($returnApi)
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar produto' : 'Produto consultado com sucesso', $returnApi);
        return $returnApi;
    }
    
    /**
     * Consultar produto e retornar tags em array associativa
     * @param json $post Parâmetros para busca
     * @param integer $id ID de um produto
     * @param boolean $cache Buscar registros com ou sem cache
     * @return object {'successo': '1', 'produtos': {...}}
     */
    public function buscarProdutoTags($post=null, $id=null, $cache=true)
    {
        $this->setLog('Consulta de produto e tags associativas');
        $produto = $this->buscarProduto($post, $id, $cache);
        if ($produto->successo != 1) {
            $this->logs->log('Falha ao consultar produto', [$post, $id]);
//            throw new Exception('Falha ao consultar produto',1);
        }
        foreach ($produto->objeto as $prod) {
            $tags=[];
            if (isset($prod->tags)) {
                foreach ($prod->tags as $tag) {
                    $tags[$tag->nome]=$tag->valor;
                }
            }
            $prod->tags = $tags;
        }
        return $produto;
    }
    
    /**
     * Adicionar tag para produto
     * @param type $post Dados da tag: nome, [valor]
     * @return object
     */
    public function adicionarTagProduto($post=[])
    {
        $this->setLog('Adicao de tag de produto');
        if (isset($post['codigo']) && $post['codigo'] > 0) {
            $this->logs->log('Adicionar tag de produto', $post);
            $returnApi = json_decode($this->call('produtos/' . $post['codigo'] . '/tag', 'POST', ['nome' => $post['nome'], 'valor' => isset($post['valor']) && !empty($post['valor']) ? $post['valor'] : '']));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao adicionar tag de produto' : 'Tag de produto adicionada com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Adicao de tag de produto invalida. Formulario vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível adicionar tag de produto.');
    }
    
    /**
     * Excluir tag para produto
     * @param type $post Dados da tag: nome
     * @return object
     */
    public function excluirTagProduto($post=[])
    {
        $this->setLog('Exclusão de tag de produto');
        if (isset($post['codigo']) && $post['codigo'] > 0) {
            $this->logs->log('Excluir tag de produto', $post);
            $returnApi = json_decode($this->call('produtos/' . $post['codigo'] . '/tag', 'DELETE', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao excluir tag de produto' : 'Tag de produto excluída com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Exclusão de tag de produto inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível excluir tag de produto.');
    }
    
    /**
     * Adicionar anexo para produto
     * @param type $post Dados de anexo: nome, tipo, objeto
     * @return object
     */
    public function adicionarAnexoProduto($post=[])
    {
        $this->setLog('Adição de anexo de produto');
        if (isset($post['codigo']) && $post['codigo'] > 0) {
            $this->logs->log('Adicionar anexo de produto', $post);
            $returnApi = json_decode($this->call('produtos/' . $post['codigo'] . '/anexo', 'POST', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao adicionar anexo de produto' : 'Anexo de produto adicionado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Adição de anexo de produto inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível adicionar anexo de produto.');
    }

    /**
     * Excluir anexo para produto
     * @param type $post Dados de anexo: nome
     * @return object
     */
    public function excluirAnexoProduto($post=[])
    {
        $this->setLog('Exclusão de anexo de produto');
        if (isset($post['codigo']) && $post['codigo'] > 0) {
            $this->logs->log('Excluir anexo de produto', $post);
            $returnApi = json_decode($this->call('produtos/' . $post['codigo'] . '/anexo', 'DELETE', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao excluir anexo de produto' : 'Anexo de produto excluído com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Exclusão de anexo de produto inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível excluir anexo de produto.');
    }    
    
    /**
     * Adicionar estoque para produto
     * @param type $post Dados para estoque: unidades
     * @return object
     */
    public function adicionarEstoqueProduto($post=[])
    {
        $this->setLog('Adição de estoque de produto');
        if (isset($post['codigo']) && $post['codigo'] > 0) {
            $this->logs->log('Adicionar estoque de produto', $post);
            $returnApi = json_decode($this->call('produtos/' . $post['codigo'] . '/estoque', 'POST', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao adicionar estoque de produto' : 'Estoque de produto adicionado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Adição de estoque de produto inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível adicionar estoque de produto.');
    } 
    
    /**
     * Remover estoque para produto
     * @param type $post Dados para estoque: unidades
     * @return object
     */
    public function removerEstoqueProduto($post=[])
    {
        $this->setLog('Baixa de estoque de produto');
        if (isset($post['codigo']) && $post['codigo'] > 0) {
            $this->logs->log('Baixar estoque de produto', $post);
            $returnApi = json_decode($this->call('produtos/' . $post['codigo'] . '/estoque', 'DELETE', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao baixar estoque de produto' : 'Estoque de produto baixado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Baixa de estoque de produto inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível baixar estoque de produto.');
    } 
    
    /**
     * Consultar estoque de produto
     * @return object {'successo': '1', 'data': 'DD/MM/AAAA HH:MM:SS', 'unidades': '+-F(9,2)'}
     */
    public function consultarEstoqueProduto($post=[])
    {
        $this->setLog('Consulta de estoque de produto');
        if (isset($post['codigo']) && $post['codigo'] > 0) {
            $this->logs->log('Consultar estoque de produto', $post);
            $returnApi = json_decode($this->call('produtos/' . $post['codigo'] . '/estoque', 'GET', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar estoque de produto' : 'Estoque de produto consultado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Consulta de estoque de produto inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível consultar estoque de produto.');
    } 
    
    
    /**
     * Consultar totais de produto
     * @return object {'successo': '1', 'objeto': Array}
     */
    public function consultarTotaisEstoqueProduto($post=[])
    {
        $this->setLog('Consulta de totais de produto');
        if (isset($post['tags'])) {
            $this->logs->log('Consultar totais de produto', $post);
            $returnApi = json_decode($this->call('produtos/buscar/0/totais', 'PUT', $post));
            $this->logs->log(isset($returnApi->successo) && $returnApi->successo === '0' ? 'Falha ao consultar totais de produto' : 'Totais de produto consultado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Consulta de totais de produto inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível consultar totais de produto.');
    } 
    
    /**
     * Clonagem de produto por tag
     * @param array $post Array de informações: tag, nova_tag
     * @return object {'successo': '1', 'produtos_clonados': 'N(11)'}
     */
    public function clonarProduto($post=[])
    {
        $this->setLog('Clonagem de produto por tag');
        if (isset($post['tag']) && !empty($post['tag']) && isset($post['nova_tag']) && !empty($post['nova_tag'])) {
            $this->logs->log('Clonar produto por tag', $post);
            $returnApi = json_decode($this->call('produtos/clonar/' . $post['tag'] . '/' . $post['nova_tag'], 'PUT', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao clonar produto por tag' : 'Produto clonado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Clonagem de produto por tag inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível clonar produto por tag.');
    }

    /**
     * Encontrar anexo específico de um produto pelo nome. Retirar o endereço da API antes de solicitar o endpoint
     * @param object $anexo registro de anexo de um produto
     * @param string $nome nome do anexo a ser buscado
     * @return object anexo
    */
    public function findAnexo($anexo, $nome){
//        $this->setLog('Buscando anexo');
        $urlAnexo;
        $saveUrl=$this->getUrl();
        foreach($anexo as $a){
            if($a->nome == $nome){
                $urlAnexo = $a->url; 
            }
        }
        if(!isset($urlAnexo)) return '';
        if (stristr($urlAnexo, 'v1/') == false) {
            $apiIndex=strpos($urlAnexo, 'v1/');
            $urlAnexo=substr($urlAnexo, $apiIndex+3);
        } else {
            $this->setUrl('');
        }
        $call=$this->call($urlAnexo, 'GET');
        if($call == ''){
//            $this->setLog('Erro localizando anexo',$anexo);
            throw new Exception('Não foi possível encontrar o anexo solicitado',1);
        }
//        $this->logs->log('Retorno: ',$call);
        $this->setUrl($saveUrl);
        return $call;
    }
    
    /**
     * Registrar produtos de temporada no carrinho do cliente
     * @param string $identificador Documento do cliente (Oeste) OU e-mail do cliente (Leste)
     * @param string $area Área do estádio
     */
    public function setProdutosReserva($identificador, $area)
    {
        $prodReserva = [];
        $this->setLog('Reserva de produtos - Temporada');
        if (!empty($identificador) && !empty($area)) {
            $this->logs->log('Reservar produtos para o cliente de ' . ($area == 'Oeste' ? 'cpf: ' : 'e-mail: '), base64_decode($identificador));
            $produtosReserva = $this->getProdutosReserva($identificador, $area);
            if (!empty($produtosReserva)) {
                foreach($produtosReserva as $index => $produtoReserva) {
                    $produto = (new ApiProduto)->buscarProdutoTags(null, base64_decode($produtoReserva->produtobase64));
                    if ($produto->successo === '1' && !empty($produto->objeto[0])) {
                        $prodReserva[$produto->objeto[0]->tags['area']][$produto->objeto[0]->codigo] = $produto->objeto[0]->tags;
                        $returnApi = (new ApiCarrinho)->atualizarCarrinho(['produto' => ['codigo' => $produto->objeto[0]->codigo, 'unidades' => 1]]);
                        $this->logs->log($returnApi->successo === '0' ? 'Falha ao adicionar produto ao carrinho' : 'Produto adicionado ao carrinho', $returnApi);
                    }
                }
                if (isset($prodReserva) && !empty($prodReserva)) {
                    $this->setSessionTemporada($identificador, $prodReserva, $area);
                }
                return (object)['successo' => '1', 'produtos' => $prodReserva];
            } 
            $this->logs->log('Produtos não encontrados', $produtosReserva);
            return (object)['successo' => '1', 'produtos' => $prodReserva];
        } 
        $this->logs->log('Documento não informado');
        return (object)['successo' => '0', 'produtos' => $prodReserva];
    }
    
    /**
     * Buscar os produtos para renovação associados a um cliente - tabela de controle
     * @param string $identificador Documento do cliente (Oeste) OU e-mail do cliente (Leste)
     * @param string $area Área do estádio
     */
    public function getProdutosReserva($identificador, $area) 
    {
        $this->setLog('Identificar produtos de reserva - Temporada');
        $produtosReserva = [];
        if (!empty($identificador) && !empty($area)) {
            $produtosReserva = $area == 'Oeste' ? 
                Renovacao::find()->where(['cpf' => base64_decode($identificador), 'compra_efetuada' => 0])->all() : 
                Renovacao::find()->where(['email' => base64_decode($identificador), 'compra_efetuada' => 0])->all();
        }
        return $produtosReserva;
     }
    
    /**
     * Registrar na sessão o documento do cliente para renovação de temporada
     * @param string $documento CPF do cliente, codificado com base64
     * @param array $produtos Produtos que fazem parte da renovação do cliente
     * @param string $area Área para renovação
     */
    private function setSessionTemporada($documento, $produtos, $area) 
    {
        if (!empty($documento) && !empty($produtos)) {
            $session = \Yii::$app->session;
            $session->open();
            $session['renovacao_temporada'] = true;
            $session['documento_temporada'] = $documento;
            $session['produtos_temporada'] = $produtos;
            $session['area_temporada'] = $area;
            $session->close();
        }
    }

    /**
     * Calcular desconto de produto e retornar preço final (fixo ou percentual) com base no registro do produto. Se tiver desconto fixo, sobrepõe o percentual
    */
    public function descontoProduto($id){
        $this->setLog('Consulta de desconto do produto');
        $produto=$this->buscarProduto(null,$id);
        if($produto->successo!=1)
            throw new Exception('Falha ao consultar produto',1);
        return $this->calcDesconto($produto->objeto[0]);
    }

    /**
     * Cálculo do desconto
     */
    public function calcDesconto($produto){
        $desconto=$produto->desconto;
        if($desconto->fixo==0 && $desconto->percentual==0) return $produto->valor;
        if($desconto->fixo>0) return $produto->valor-$desconto->fixo;
        if($desconto->percentual>0) return $produto->valor-($produto->valor*$desconto->percentual/100);
    }
    
    /**
     * Verificar se o produto solicitado faz parte de um bloco ativo
     */
    public function checkBloco($tagsPost){
        $tagsBusca=[];
        foreach ($tagsPost as $key => $value) {
            if($value['nome']=='tipo' && $value['valor']=='assento')
                $tagsBusca[]=['nome'=>'tipo','valor'=>'bloco'];
            else
                $tagsBusca[$key]=$value;
        }
        try{
            $bloco=$this->buscarProduto(json_encode(["ativo"=>"1","tags"=>$tagsBusca]));
            return $bloco->successo==1 ?'true':'false';
        }catch(Exception $e){
            return 'false';
        }
    }

    /**
     * Converter array de tags em array única ou objeto
     */
    public static function tagsArray($tags,$object=false){
        $obj=[];
        if(is_array($tags))
            foreach ($tags as $key => $value) {
                $obj[$value->nome]=$value->valor;
            }
        return $object ?(object)$obj :$obj;
    }

    /**
     * Lista dos grupos de produtos vendidos para relatório financeiro
     */
    public function listaNomesProdutos($tipos){
        $lista=[];
        if(is_array($tipos)){
            foreach ($tipos as $tipo) {
                $tags=['tags'=>[
                        ['nome'=>'matriz','valor'=>'temporada2018'],
                        ['nome'=>'tipo','valor'=>$tipo]
                    ]
                ];
                $busca=$this->buscarProdutoTags(json_encode($tags), null, true);
                if($busca->successo){
                    foreach ($busca->objeto as $p) {
                        if(isset($p->tags['area'],$p->tags[$tipo]))
                            $lista[$p->tags['area']][$p->tags[$tipo]]=[
                                'valor'=>0,
                                'vendidos'=>0,
                                'vendas'=>0,
                                'capacidade'=>$p->estoque->oferecido,
                                'bloqueados'=>0,
                                'disponiveis'=>0,
                                'ocupacao'=>0,
                            ];
                    }
                }
            }
        }
        $this->produtos=$lista;
    }

    /**
     * Verifica se há reserva para um bloco (mostrar no mapa de assentos se o usuário tiver uma reserva nas tags dos assentos independente do status do bloco)
     */
    public function checkReserva($bloco){
        $session = \Yii::$app->session;
        $session->open();
        if(!isset($session['documento'])) return false;
        if(!isset($bloco->tags,$bloco->tags['bloco'])) return false;
        $objBusca=[
            'tags'=>[
                ['nome'=>'matriz','valor'=>'temporada2018'],
                ['nome'=>'bloco','valor'=>$bloco->tags['bloco']],
                ['nome'=>'tipo','valor'=>'assento'],
            ],
                'reserva_exclusiva'=>$session['documento']
            ];       
        $busca=$this->buscarProduto(json_encode($objBusca),'',false);
        if($busca && $busca->successo && count($busca->objeto)>0)
            return true;

        return false;
    }
}
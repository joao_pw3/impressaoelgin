<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'ADM Estádio Mineirão :: Reserva de assentos';
?>
<div class="container compra-avulsa" id="compra-avulsa">
	<div class="row">
        <h2 class="tituloGeral"><i class="fa fa-clone"></i> Reserva de assentos</h2>
    </div>
    <div class="container">
        <?php 
        foreach ($eventos as $index => $evento) {
            if (isset($evento->tags['apelido']) && $evento->tags['apelido'] == 'temporada2018') {
        ?>
        <div style="margin-bottom:25px;">
            <div class="col-md-4" style="visibility: visible; -webkit-animation-delay: .5s;">
                <?= Html::img($evento->anexos['bannervitrine_2']); ?>
            </div>
            <div class="col-md-6 textosAprest" style="visibility: visible; -webkit-animation-delay: .5s;">
                <div>Mineirão Tribuna</div>                    
                Muito mais do que futebol. Garanta já o seu lugar, compre aqui!
            </div>
            <div class="col-md-2" style="visibility: visible; -webkit-animation-delay: .5s;">               
                <span class="row contato">               
                    <a class="btn btn-success" href="<?= Url::to(['/admin/vendas/estadio', 'id' => $evento->codigo, 'area' => 'Oeste']); ?>"><i class="fa fa-check"></i> Bloqueio / Reserva</a> 
                </span>
            </div>
            <hr style="padding-top:25px;">
        </div>
        <div style="margin-bottom:25px;">
            <div class="col-md-4" style="visibility: visible; -webkit-animation-delay: .5s;">
                <?= Html::img($evento->anexos['bannervitrine_1']); ?>
            </div>
            <div class="col-md-6 textosAprest" style="visibility: visible; -webkit-animation-delay: .5s;">
                <div>Mineirão Embaixada</div>                    
                Você mais perto da história. Garanta já o seu lugar, compre aqui!
            </div>
            <div class="col-md-2" style="visibility: visible; -webkit-animation-delay: .5s;">               
                <span class="row contato">               
                    <a class="btn btn-success" href="<?= Url::to(['/admin/vendas/estadio', 'id' => $evento->codigo, 'area' => 'Leste']); ?>"><i class="fa fa-check"></i> Bloqueio / Reserva</a> 
                </span>
            </div>
            <hr style="padding-top:25px;">
        </div>
<!--        <div style="margin-bottom:25px;">
            <div class="col-md-6" style="visibility: visible; -webkit-animation-delay: .5s;">
                <?= Html::img($evento->anexos['bannervitrine_0']); ?>
            </div>
            <div class="col-md-4 textosAprest" style="visibility: visible; -webkit-animation-delay: .5s;">
                <div>Mineirão Camarote</div>                    
                Garanta já o seu lugar, compre aqui!
            </div>
            <div class="col-md-2" style="visibility: visible; -webkit-animation-delay: .5s;">               
                <span class="row contato">               
                    <a class="areas seta" href="<?= Url::to(['/admin/vendas/estadio', 'id' => $evento->codigo]); ?>">Bloqueio / Reserva</a> 
                </span>
            </div>
            <hr style="padding-top:25px;">
        </div>-->
    <?php 
            } else {
    ?>
        <div style="margin-bottom:25px;">
            <div class="col-md-4" style="visibility: visible; -webkit-animation-delay: .5s;">
                <?= isset($evento->anexos['bannervitrine']) ? Html::img($evento->anexos['bannervitrine']) : Html::img('@web/images/semimg.jpg', ['alt' => 'Sem imagem', 'title' => 'Sem imagem', 'border' => '0', 'width' => '100%']); ?>
            </div>
            <div class="col-md-6 textosAprest" style="visibility: visible; -webkit-animation-delay: .5s;">
                <div><?= $evento->nome; ?></div>                    
                <?php
                $objData = \DateTime::createFromFormat('d/m/Y H:i:s', $evento->data->inicial);
                $diaSemana = ['', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab', 'Dom'];
                ?>
                <div>
                <?= $diaSemana[$objData->format('N')] . ' ' . $objData->format('d/m') ?> - 
                    Horário: <?= $objData->format('H'); ?>h
                </div>
                <?= $evento->descricao; ?>
            </div>
            <div class="col-md-2" style="visibility: visible; -webkit-animation-delay: .5s;">               
                <span class="row contato">               
                    <a class="btn btn-success" href="<?= Url::to(['/admin/vendas/estadio', 'id' => $evento->codigo, 'area' => '']); ?>"><i class="fa fa-check"></i> Bloqueio / Reserva</a> 
                </span>
            </div>
            <hr style="padding-top:25px;">
        </div>
    <?php 
            } 
        } 
    ?>
    </div>
</div>

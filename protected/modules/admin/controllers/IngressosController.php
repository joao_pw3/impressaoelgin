<?php
namespace app\modules\admin\controllers;

use Yii;
use yii\web\Response;
use app\models\ApiCarrinho;
use app\modules\admin\models\ApiIngresso;
use app\models\EventosModel;
use app\modules\admin\controllers\AbstractController;
use app\modules\admin\models\BilheteriaModel;

/**
 * Controller de ingressos.
 */

class IngressosController extends AbstractController 
{
    public $enableCsrfValidation = false;
    
    public function actionIndex() { }

    /**
     * Trazer os ocupantes de lugares vendidos
     * Permite filtrar por um produto específico
     */
    public function actionOcupantes() {
        $firstDay = new \DateTime('today');
        $lastDay = new \DateTime('today');
        $lastDay->setTime(23, 59, 59);
        $dataDe = $firstDay->format('d-m-Y\TH:i:s');
        $dataAte = $lastDay->format('d-m-Y\TH:i:s');
        $post = Yii::$app->request->post();
        $renderParams = [
            'tipoConsulta' => 'vendas',
            'de' => $firstDay->format('d/m/Y'),
            'ate' => $lastDay->format('d/m/Y'),
            'listaEventos' => (new EventosModel)->listaEventos(),
            'consulta' => (new ApiCarrinho)->consultarVendaOcupantes($dataDe, $dataAte),
        ];
        if ($post) {
            $tags = [];
            $documentoCarrinho='';
            foreach ($post as $key => $value) {
                if ($key == '_csrf' || $key == 'consulta' || $post[$key] == '')
                    continue;
                $valor = $post[$key];
                if ($key == '%_documento'){
                    $valor = preg_replace('/(\W)/', '', $valor);
                    $documentoCarrinho=$valor;
                }
                $tags[] = [
                    'nome' => $key,
                    'valor' => $valor
                ];
            }
            switch ($post['consulta']) {
                case 'produto':
                    $renderParams = [
                        'tipoConsulta' => 'produto',
                        'de' => $firstDay->format('d/m/Y'),
                        'ate' => $lastDay->format('d/m/Y'),
                        'listaEventos' => (new EventosModel)->listaEventos(),
                        'consulta' => (new ApiCarrinho)->consultarCarrinhoTags($tags, 'produto'),
                    ];
                    break;
                case 'ocupante':
                    $apiCarrinho=new ApiCarrinho;
                    $ocupante = $apiCarrinho->consultarCarrinhoTags($tags, 'carrinho');
                    if($documentoCarrinho!=''){
                        $comprador = $apiCarrinho->consultarCarrinhoComprador($documentoCarrinho);
                        $renderParams = [
                            'tipoConsulta' => 'ocupante',
                            'de' => $firstDay->format('d/m/Y'),
                            'ate' => $lastDay->format('d/m/Y'),
                            'listaEventos' => (new EventosModel)->listaEventos(),
                            'consulta' => $comprador,
                            'consultaOcupante' => $ocupante,
                        ];
                    }
                    break;
                case 'vendas':
                default:
                    $postDe = \DateTime::createFromFormat('d/m/Y', $post['de']);
                    $dataDe = $postDe->format('d-m-Y') . 'T00:00:00';
                    $postAte = \DateTime::createFromFormat('d/m/Y', $post['ate']);
                    $dataAte = $postAte->format('d-m-Y') . 'T23:59:59';
                    $renderParams = [
                        'tipoConsulta' => 'vendas',
                        'de' => $post['de'],
                        'ate' => $post['ate'],
                        'listaEventos' => (new EventosModel)->listaEventos(),
                        'consulta' => (new ApiCarrinho)->consultarVendaOcupantes($dataDe, $dataAte),
                    ];
                    break;
            }
        } else
            $produtos = (new ApiCarrinho)->consultarVendaOcupantes($dataDe, $dataAte);
        //atualização
        //return $this->render('//site/atualizando-servidor');
        //</atualização>
        return $this->render('ocupantes', $renderParams);
    }

    /**
     * Consultar ingresso
     */
    public function actionConsultar() 
    {
        try {
            $resultadoBusca = [];
            if (Yii::$app->request->post()) {
                $post = Yii::$app->request->post('BilheteriaModel');
                $arrConsulta = [];
                $arrConsulta['id_agenda'] = $post['idEvento'];
                if (isset($post['status']) && !empty($post['status'])) {
                    $arrConsulta['status'] = $post['status'];
                }
                if (isset($post['lote']) && !empty($post['lote'])) {
                    $arrConsulta['lote_origem'] = $post['lote'];
                }
                if (isset($post['qrcode_voucher']) && !empty($post['qrcode_voucher'])) {
                    $arrConsulta['qrcode'] = $post['qrcode_voucher'];
                }
                if (isset($post['documento']) && !empty($post['documento'])) {
                    $arrConsulta['documento'] = $post['documento'];
                }
                if (isset($post['comprador']) && !empty($post['comprador'])) {
                    $arrConsulta['nome'] = $post['comprador'];
                }
                $resultadoBusca = (new ApiIngresso())->consultar($arrConsulta);
            }
            $eventos = (new EventosModel())->listaEventos();
            return $this->render('consultar', [
                'model' => new BilheteriaModel,
                'resultadoBusca' => $resultadoBusca,
                'eventos' => $eventos,
                'post' => isset($post) && !empty($post) ? $post : [],
                'btExportarCsv'=>isset($post) && $post['idEvento']!='' && $post['status']!=''
            ]);
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao fazer consulta de ingresso', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Acompanhamento de lotes
     */
    public function actionAcompanhar() 
    {
        try {
            $resultadoBusca = [];
            $lotes = [];
            if (Yii::$app->request->post()) {
                $post = Yii::$app->request->post('BilheteriaModel');
                $resultadoBusca = (new ApiIngresso())->consultar(['id_agenda' => $post['idEvento']]);
                $lotes = (new ApiIngresso())->registrosLoteConsolidado($resultadoBusca);
            }
            $eventos = (new EventosModel())->listaEventos();
            return $this->render('acompanhar', [
                'model' => new BilheteriaModel,
                'lotes' => $lotes,
                'eventos' => $eventos,
                'post' => isset($post) && !empty($post) ? $post : []
            ]);
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao fazer consulta de ingresso', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Associação de RFID - tela para ler o RFID e QRCode de um ingresso e valida-lo com o endpoint ingresso. Marcar o ingresso como associado (A) ou invalidar (I)
     */
    public function actionAssociarIngressos(){
        $eventos=(new EventosModel)->listaEventos();
        $recebidos=[];
        $associados=[];
        $msg='';
        $msgAssociados='';
        $id_agenda='';
        $post=Yii::$app->request->post();
        $apiIngresso=new ApiIngresso;
        if($post && isset($post['id_agenda'])){
            //consulta aos ingressos recebidos da gráfica (status E) do evento selecionado e que não estejam bloqueados
            $id_agenda=$post['id_agenda'];
            $consulta=$apiIngresso->consultar(['id_agenda'=>$id_agenda,'status'=>'E','bloqueado'=>0]);
            if($consulta->successo)
                $recebidos=$consulta->objeto;
            if(count($recebidos)==0)
                $msg='Nenhum ingresso entregue para este evento.';

            $consultaAssociados=$apiIngresso->consultar(['id_agenda'=>$id_agenda,'status'=>'A','bloqueado'=>0]);
            if($consultaAssociados->successo)
                $associados=$consultaAssociados->objeto;
            if(count($associados)==0)
                $msgAssociados='Nenhum ingresso associado para este evento.';
        }
        if($post && isset($post['qrcode'],$post['rfid'],$post['id_agenda']) && Yii::$app->request->isAjax){
            //associar - Status "A"
            $associar=$apiIngresso->associa($post);
            echo json_encode($associar);
            exit();
        }
        return $this->render('associar-ingressos',['eventos'=>$eventos,'id_agenda'=>$id_agenda,'recebidos'=>$recebidos,'msg'=>$msg,'associados'=>$associados,'msgAssociados'=>$msgAssociados]);
    }

    
    /**
     * Registrar lote de cartões como "entregue ao Estádio"
     */
    public function actionLoteEntrega()
    {
        try {
            $eventos = (new EventosModel())->listaEventos();
            $ingressos = '';
            $lotes = '';
            if (Yii::$app->request->post()) {
                $ingresso = new ApiIngresso();
                $ingressos = $ingresso->consultar([
                    'id_agenda' => Yii::$app->request->post('estadio_evento'), 
                    'status' => 'N'
                ]);
                if ($ingressos->successo == '1') {
                    $lotes = $ingresso->registrosLote($ingressos, 'envio');
                }
            }
            return $this->render('lote-entrega', [
                'codigo_evento' => Yii::$app->request->post('estadio_evento') != null ? Yii::$app->request->post('estadio_evento') : '',
                'eventos' => $eventos,
                'ingressos' => $ingressos, 
                'lotes' => $lotes,
            ]);
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro - Entrega de Lotes para o Estádio', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Mudar status de lote para "entregue ao Estádio"
     */
    public function actionStatusLoteEntrega ()
    {
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                $post = Yii::$app->request->post();
                return (new ApiIngresso)->entregar([
                    'id_agenda' => $post['evento'], 
                    'lote_origem' => $post['lote']
                ]);
            }
            return (new ApiIngresso)->getApiError('Faltam parâmetros para mudar o status do lote.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro - Mudança de status de lote para "entregue ao Estádio"', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Consultar ingressos inválidos para efetuar ações de bloqueio ou solicitação de segunda via
     */
    public function actionInvalidos() 
    {
        try {
            $resultadoBusca = [];
            if (Yii::$app->request->post()) {
                $resultadoBusca = (new ApiIngresso())->consultar([
                    'id_agenda' => Yii::$app->request->post('BilheteriaModel')['idEvento'], 
                    'status' => 'I'
                ]);
            }
            return $this->render('invalidos', [
                'resultadoBusca' => $resultadoBusca,
                'eventos' => (new EventosModel())->listaEventos(),
                'post' => Yii::$app->request->post('BilheteriaModel')
            ]);
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro - Mudança de status de lote para "entregue ao Estádio"', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Bloquear ingresso
     */
    public function actionBloquearIngresso()
    {
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                $post = Yii::$app->request->post();
                return (new ApiIngresso)->bloquear([
                    'id_agenda' => $post['id_agenda'],
                    'qrcode' => $post['qrcode'],
                    'bloqueado' => 1
                ]);
            }
            return (new ApiIngresso)->getApiError('Faltam parâmetros para bloquear ingresso.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao bloquear ingresso', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Solicitar segunda via de ingresso
     */
    public function actionSegundaViaIngresso()
    {
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                $post = Yii::$app->request->post();
                return (new ApiIngresso)->segundavia([
                    'id_agenda' => $post['id_agenda'],
                    'qrcode' => $post['qrcode']
                ]);
            }
            return (new ApiIngresso)->getApiError('Faltam parâmetros para solicitar segunda via de ingresso.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao solicitar segunda via de ingresso', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Invalidar ingresso
     */
    public function actionInvalidarIngresso ()
    {
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                $post = Yii::$app->request->post();
                return (new ApiIngresso)->invalidar([
                    'id_agenda' => $post['id_agenda'], 
                    'qrcode' => $post['qrcode'], 
                    'motivo' => $post['motivo']
                ]);
            }
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao invalidar ingresso', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Registrar lote de cartões como "liberado para bilheteria"
     */
    public function actionLoteLiberar()
    {
        try {
            $eventos = (new EventosModel())->listaEventos(['ativo' => '1']);
            $ingressos = '';
            $lotes = '';
            if (Yii::$app->request->post()) {
                $ingresso = new ApiIngresso();
                $ingressos = $ingresso->consultar([
                    'id_agenda' => Yii::$app->request->post('estadio_evento'), 
                    'status' => 'A'
                ]);
                if ($ingressos->successo == '1') {
                    $lotes = $ingresso->registrosLote($ingressos, 'associacao');
                }
            }
            return $this->render('lote-liberar', [
                'codigo_evento' => Yii::$app->request->post('estadio_evento') != null ? Yii::$app->request->post('estadio_evento') : '',
                'eventos' => $eventos,
                'ingressos' => $ingressos, 
                'lotes' => $lotes,
            ]);
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro - Liberar ingressos para bilheteria', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Mudar status de lote para "liberado para bilheteria"
     */
    public function actionStatusLoteLiberar ()
    {
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                $post = Yii::$app->request->post();
                return (new ApiIngresso)->liberar([
                    'id_agenda' => $post['evento'], 
                    'lote_origem' => $post['lote']
                ]);
            }
            return (new ApiIngresso)->getApiError('Faltam parâmetros para mudar o status do lote.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro - Mudança de status de lote para "liberado para bilheteria"', 'message' => $e->getMessage()]);
        }
    }
    
}
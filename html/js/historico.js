$(document).ready(function() {
    
    $('.btn-reembolso').click(function () {
        $('#id_compra').val($(this).attr('data-code'));
        $('#trid').val($(this).attr('data-trid'));
        $('#reemb-total').html($(this).attr('data-value').replace('.', ','));
        $('#modal-reembolso').modal('toggle');
    });
    
    $('#btn-reembolso-cancel').click(function () {
       $('#modal-reembolso').modal('hide'); 
    });
    
    $('#btn-reembolso-confirm').click(function () {
       doReembolso(); 
    });

    $(".pgto-taxa-segunda-via").click(function(){
        showMsg("<p>A segunda via leva até 15 dias úteis para ser produzida.</p><p>Caso haja algum jogo ou evento neste período, um ingresso avulso será fornecido para o seu acesso na bilheteria Sul.</p>");
    })
    
});

function doReembolso () {
    $('#modal-reembolso').modal('hide');
    doAjax ($('#url-estorno').val(), 'POST', $('#form-reembolso').serialize(), 'JSON', 7000, 'reembolsoOK', 'reembolsoErr');
    return false;
}

function reembolsoOK (data) {
    window.location.href = $('#url-cliente').val() + '#tab-historico';
}

function reembolsoErr (data) {
    $('#modal-mensagem .modal-body').html('Não foi possível efetuar o estorno. Erro: ' + data.erro.mensagem);
    $('#modal-mensagem').modal('toggle');
}

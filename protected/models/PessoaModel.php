<?php
namespace app\models;
use Yii;

/**
 * This is the model class for table "pessoa".
 *
 * @property string $id
 * @property string $nome
 * @property string $email
 * @property string $sexo
 * @property string $documento
 * @property string $data_nascimento
 * @property string $telefone
 * @property string $time
 * @property string $cep
 * @property string $logradouro
 * @property integer $numero
 * @property string $complemento
 * @property string $bairro
 * @property string $cidade
 * @property string $estado
 * @property string $pais
 * @property integer $status
 */
class PessoaModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pessoa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_nascimento'], 'safe'],
            [['numero', 'status'], 'integer'],
            [['nome', 'email', 'logradouro', 'complemento'], 'string', 'max' => 128],
            [['sexo'], 'string', 'max' => 1],
            [['documento'], 'string', 'max' => 16],
            [['telefone'], 'string', 'max' => 15],
            [['time', 'bairro', 'cidade', 'estado', 'pais'], 'string', 'max' => 64],
            [['cep'], 'string', 'max' => 9],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'email' => 'Email',
            'sexo' => 'Sexo',
            'documento' => 'Documento',
            'data_nascimento' => 'Data Nascimento',
            'telefone' => 'Telefone',
            'time' => 'Time',
            'cep' => 'Cep',
            'logradouro' => 'Logradouro',
            'numero' => 'Numero',
            'complemento' => 'Complemento',
            'bairro' => 'Bairro',
            'cidade' => 'Cidade',
            'estado' => 'Estado',
            'pais' => 'Pais',
            'status' => 'Status',
        ];
    }

    /**
     * @inheritdoc
     * @return PessoaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PessoaQuery(get_called_class());
    }
}

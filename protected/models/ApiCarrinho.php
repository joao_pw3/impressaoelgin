<?php
namespace app\models;

use Yii;
use app\models\Api;
use Da\QrCode\QrCode;
use app\models\Renovacao;
use app\models\ApiProduto;

/**
 * Classe para Carrinho de compras - Easy for Pay - Mineirão
 * obs.: parâmetros entre colchetes são opcionais
 */
class ApiCarrinho extends Api
{
    public $logs;
    private $carrinhoVenda;
    private $tempoCarrinho = '+20 minutes';
    
    /**
     * Criar carrinho de compras
     * @return object {'successo': '1', 'id': ''}
     */
    public function criarCarrinho()
    {
        $this->setLog('Criacao de carrinho de compras');
        $this->logs->log('Criar carrinho de compras');
        $session = Yii::$app->session;
        $session->open();
        /*if (isset($_COOKIE['idCarrinho']) && !empty($_COOKIE['idCarrinho']) && !isset($_COOKIE['user_id']) ||
            (isset($_COOKIE['idCarrinho']) && isset($_COOKIE['user_id']) && $_COOKIE['user_id'] == $session['user_id'])) {//cookie só é válido para mesmo usuário
            $this->logs->log('Existe cookie de carrinho. Id: ', $_COOKIE['idCarrinho']);
            return $this->criarCarrinhoCookie();
        }*/
        if (!isset($session['idCarrinho']) || $session['idCarrinho'] == '') {
            $returnApi = json_decode($this->call('carrinho/', 'GET'));
            if ($returnApi->successo == '1') {
                $this->setSessionCarrinho($returnApi->objeto->id);
                $this->definirExpiracaoCarrinho($returnApi);
            }
            $this->logs->log($returnApi->successo == '1' ? 'Carrinho de compras criado com sucesso' : 'Falha ao criar carrinho de compras', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Carrinho de compras ja aberto. Id: ', $session['idCarrinho']);
        $this->setSessionCarrinho($session['idCarrinho']);
        return (object)['successo' => '1', 'objeto' => ['id' => $session['idCarrinho']]];
    }

    /**
     * Definir tempo de Expiração de Carrinho na sessão
     * Se o tempo já estiver em andamento, modificá-lo de acordo com o retorno do carrinho
     * Padrão em 16/02/2018: 20 minutos
     * @param object $carrinho Objeto do carrinho
     * @param boolean $atualizar Atualizar ou não o contador do tempo do carrinho
     */
    private function definirExpiracaoCarrinho($carrinho='', $atualizar=true)
    {
        $session = Yii::$app->session;
        $session->open();
        if ($carrinho->successo == '1' && isset($carrinho->objeto->minutos_expirar) && (int)$carrinho->objeto->minutos_expirar > 0) {
            $this->tempoCarrinho = '+' . $carrinho->objeto->minutos_expirar . ' minutes';
        }
        if ($atualizar) {
            $session['tempoCarrinho'] = date('Y,m,d,H,i,s', strtotime($this->tempoCarrinho));
        }
        $session->close();
    }
    /**
     * Se existir o id do carrinho no cookie, se ainda for válido e se há produtos, utilizá-lo
     */
    private function criarCarrinhoCookie()
    {
        $this->setLog('Criacao de carrinho de compras - Cookie');
        $returnApi = $this->consultarCarrinho($_COOKIE['idCarrinho']);
        $this->logs->log($returnApi->successo == '0' || ($returnApi->successo == '1' && empty($returnApi->objeto->produtos))? 'Cookie de carrinho não é mais válido ou está vazio' : 'Cookie de carrinho ainda ativo', $returnApi);
        if ($returnApi->successo == '1' && !empty($returnApi->objeto->produtos)) {
            $this->logs->log('Carrinho criado à partir do id no Cookie.', $_COOKIE['idCarrinho']);
            $this->setSessionCarrinho($_COOKIE['idCarrinho']);
            return $returnApi;
        }
        $this->logs->log('Erro ao criar carrinho. Pode ser que já tenha expirado o tempo.', $returnApi);
        $this->limparCookieCarrinho();
        $this->criarCarrinho();
        return $returnApi;
    }
    
    /**
     * Registrar id do carrinho criado na Session
     * @param string $carrinhoId Id do carrinho
     */
    private function setSessionCarrinho($carrinhoId='')
    {
        if (!empty($carrinhoId)) {
            $session = Yii::$app->session;
            $session->open();
            $session['idCarrinho'] = $carrinhoId;
            setcookie('idCarrinho', $carrinhoId, strtotime('+1 days'));
            setcookie('user_id', $session['user_id'], strtotime('+1 days'));
            $session->close();
        }
    }
    
    /**
     * Caso o id do carrinho gravado no cookie não seja mais válido, removê-lo
    */
    private function limparCookieCarrinho ()
    {
        setcookie('idCarrinho', '');
        $_COOKIE['idCarrinho'] = '';
        unset($_COOKIE['idCarrinho']);
    }
    
    /**
     * Remover id do carrinho criado na Session se a cobrança não foi autorizada
     * @param type $returnApi
     */
    public function limparSessionCarrinho ($returnApi)
    {
        if ($returnApi->successo === '1') {
            $this->limparCarrinho();
            $this->limparDaSessao();
            $this->limparCookieCarrinho();
        }
    }
    
    /**
     * Remover id do carrinho mesmo com compra não autorizada
     * @param type $returnApi
     */
    public function forcarLimparSessionCarrinho ()
    {
        $this->limparCarrinho();
        $this->limparDaSessao();
        $this->limparCookieCarrinho();        
    }

    /**
     * tirar o id do carrinho da sessão
     */
    private function limparDaSessao(){
        $session = Yii::$app->session;
        $session->open();
        $session['idCarrinho'] = '';
        unset($session['idCarrinho']);
        $session['tempoCarrinho'] = '';
        unset($session['tempoCarrinho']);
        $session->close();
    }
    
    /**
     * Atualizar carrinho de compras
     * @param $post Dados do carrinho: '[produto]': {'codigo': '', 'unidades': ''}, '[cupom]': {'id': '', 'unidades': ''}, '[tag]': {'nome': '', 'valor': ''}}
     * @return object {'successo': '1'}
     */
    public function atualizarCarrinho($post=[], $id=null)
    {
        $this->setLog('Atualizacao de carrinho de compras');
        $session = Yii::$app->session;
        $session->open();
        $idCarrinho = $id != null ? $id : $session['idCarrinho'];
        if (!$idCarrinho) {
            $this->criarCarrinho();
            $idCarrinho = $session['idCarrinho'];
        }
        if (!empty($post) && is_array($post) && isset($idCarrinho) && !empty($idCarrinho)) {
            $this->logs->log('Atualizar carrinho de compras', $post);
            $returnApi = json_decode($this->call('carrinho/' . $idCarrinho, 'POST', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao atualizar carrinho de compras' : 'Carrinho de compras atualizado com sucesso', $returnApi);
            $this->definirExpiracaoCarrinho($returnApi);
            return $returnApi;
        }
        $this->logs->log('Atualizacao de carrinho de compras invalido. Formulario vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível atualizar o carrinho de compras.');
    }
    
    /**
     * Limpar carrinho de compras
     * @return object {'successo': '1', 'removido': '1'}
     */
    public function limparCarrinho()
    {
        $this->setLog('Limpeza de carrinho de compras');
        $session = Yii::$app->session;
        $session->open();
        if (isset($session['idCarrinho']) && !empty($session['idCarrinho'])) {
            $this->logs->log('Limpar carrinho de compras', $session['idCarrinho']);
            $returnApi = json_decode($this->call('carrinho/' . $session['idCarrinho'], 'DELETE'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao limpar carrinho de compras' : 'Carrinho de compras limpo com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Limpeza de carrinho de compras inválido. Formulário vazio.', $_SESSION);
        return $this->getApiError('Não foi possível limpar o carrinho de compras.');
    }

    /**
     * Consultar carrinho de compras
     * @param string $idCarrinho Id de um carrinho
     * @return object {'successo': '1', '[produtos]': {...}, '[cupons]': {...}, '[tags]': {...}, '[totais]': {...}}
     */
    public function consultarCarrinho($idCarrinho='')
    {
        $this->setLog('Consulta de carrinho de compras');
        $session = Yii::$app->session;
        $session->open();
        $id_carrinho = (!empty($idCarrinho) ? $idCarrinho : 
            (isset($session['idCarrinho']) && !empty($session['idCarrinho']) ? $session['idCarrinho'] : 
            (isset($_COOKIE['idCarrinho']) && !empty($_COOKIE['idCarrinho']) ? $_COOKIE['idCarrinho'] : '')));
        if (!empty($id_carrinho)) {
            $this->logs->log('Consultar carrinho de compras', $id_carrinho);
            $returnApi = json_decode($this->call('carrinho/' . $id_carrinho, 'PUT'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar carrinho de compras' : 'Carrinho de compras consultado com sucesso', $id_carrinho);
            $this->definirExpiracaoCarrinho($returnApi, false);
            return $returnApi;
        }
        $this->logs->log('Consulta de carrinho de compras inválida. Carrinho inexistente.', $id_carrinho);
        return $this->getApiError('Não foi possível consultar o carrinho de compras.');
    }

    /**
     * Enviar cobrança
     * @param $post Dados de cobrança: trid, id_carrinho, documento, [email], [tipo], [validade], [forma], [descricao]
     */
    public function enviarCobranca($post=[])
    {
        $this->setLog('Envio de cobranca');
        if (is_array($post) && !empty($post)) {
            $this->logs->log('Enviar cobrança', $post);
            $valido=true;
            if(isset($post['Conciliacao']) && $post['Conciliacao']['tipo']!=''){
                $conciliacaoValida=$this->validarConciliacao($post); 
                $valido=$conciliacaoValida->successo;
            }
            $session = Yii::$app->session;
            $session->open();
            if($valido==false) return (object)['successo'=>0,'mensagem'=>['erro'=>'A cobrança não é válida']];
            $returnApi = json_decode($this->call('cobranca/', 'POST', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao enviar cobrança' : 'Envio de cobrança feito com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Envio de cobrança inválido. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível enviar a cobrança.');
    }
    
    /**
     * Identifica dados de uma cobrança
     * @param $post Dados da cobrança: trid
     */
    public function consultarCobranca()
    {
        $this->setLog('Consulta de cobranca');
        $post = Yii::$app->request->post();
        if (isset($post) && !empty($post)) {
            $this->logs->log('Consultar cobrança', $post);
            $returnApi = json_decode($this->call('cobranca/' . $post['trid'], 'GET'));
            if ($returnApi->successo == '1' && $returnApi->objeto->id_carrinho) {
                $this->setSessionCarrinho($returnApi->objeto->id_carrinho);
            }
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar cobrança' : 'Consulta de cobrança feita com sucesso', $returnApi);
            return $returnApi;
        } 
        $this->logs->log('Contulta de cobrança inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível consultar a cobrança.');
    }   
    
    /**
     * Autorizar pagamento. Não chamar o endpoint se for uma conciliação (venda por depósito, boleto ou TEF)
     * @param $post Array de dados para autorizar pagamento: codigo (codigo_cobrança), codigo_cartao, senha_cartao
     */
    public function autorizarPagamento($post=[])
    {
        $this->setLog('Autorizacao de pagamento');
        if (isset($post['id_compra']) && !empty($post['id_compra'])) {
//            if(isset($post['Conciliacao']) && $post['Conciliacao']['tipo']!='')
//                return $this->infoCompraConciliacao($post,$post['Conciliacao']);
            $this->logs->log('Autorizar pagamento', $post);
            $returnApi = json_decode($this->call('cobranca/' . $post['id_compra'], 'PUT', ['codigo_cartao' => $post['codigo_cartao'], 'senha_cartao' => $post['senha_cartao']], true, 'U'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao autorizar pagamento' : 'Pagamento autorizado com sucesso', $returnApi);
            if ($returnApi->successo === '1') {
                $this->limparSessionCarrinho($returnApi);
                $this->getCarrinhoVenda($post['id_compra']);
                //$this->associarProdutoCliente($post);
                $this->controleRenovaTemporada($post);
                (new ApiCliente)->setSessaoLimiteCompra();
            } else
                $this->forcarLimparSessionCarrinho();
            return $returnApi;
        } 
        $this->logs->log('A cobrança não foi autorizada.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível autorizar a cobrança.');
    }
    
    /**
     * Ao finalizar uma venda, consultar o seu carrinho para uso em outros métodos
     * @param integer $idVenda Id da compra
     */
    private function getCarrinhoVenda($idVenda='')
    {
        if (!empty($idVenda) && $idVenda > 0) {
            $venda = $this->consultarVendasFiltro(['codigo' => $idVenda]);
            if ($venda->successo === '1' && !empty($venda->objeto[0])) {
                $this->carrinhoVenda = $this->consultarCarrinho($venda->objeto[0]->id_carrinho);
            }
        }
    }
    
    /**
     * Ao efetuar uma venda, criar tag no produto com os dados do cliente
     */
    private function associarProdutoCliente($post)
    {
        $session = Yii::$app->session;
        $session->open();
        $this->setLog('Produto x Cliente');
        if (!empty($post) && isset($session['user_id']) && !empty($session['user_id']) && $this->carrinhoVenda->successo == '1' && !empty($this->carrinhoVenda->objeto->produtos)) {
            $this->logs->log('Associar produto a cliente', $this->carrinhoVenda);
            $api = new ApiProduto();
            foreach ($this->carrinhoVenda->objeto->produtos as $index => $produto) {
                $api->adicionarTagProduto([
                    'codigo' => $produto->codigo,
                    'nome' => 'id_compra',
                    'valor' => $post['id_compra']
                ]);
            }
        } else {
            $this->logs->log('Faltam informações para associar produto e cliente, ou o mesmo não está logado', array_merge($post, $_SESSION));
        }
    }
    
    /**
     * Ao autorizar pagamento, checar se é renovação de temporada e atualizar os registros correspondentes
     */
    private function controleRenovaTemporada ($post)
    {
        $session = Yii::$app->session;
        $session->open();
        $this->setLog('Renovacao de temporada - tabela auxiliar');
        $arrProdutos = [];
        if ($this->carrinhoVenda->successo == '1' && !empty($this->carrinhoVenda->objeto->produtos)) {
            foreach ($this->carrinhoVenda->objeto->produtos as $index => $produto) {
                $arrProdutos[] = $produto->codigo;
            }
        }
        if (!empty($post) && isset($session['renovacao_temporada']) && $session['renovacao_temporada'] == true && isset($session['documento_temporada']) && base64_encode($session['documento']) == $session['documento_temporada'] && !empty($arrProdutos)) {
            $renovacaoRegs = $session['area_temporada'] == 'Oeste' ? 
                Renovacao::find()->where(['cpfbase64' => $session['documento_temporada'], 'compra_efetuada' => 0])->andWhere(['in', 'codproduto', $arrProdutos])->all() : 
                Renovacao::find()->where(['email' => base64_decode($session['documento_temporada']), 'compra_efetuada' => 0])->andWhere(['in', 'codproduto', $arrProdutos])->all();
            if ($renovacaoRegs) {
                $this->logs->log('Renovar temporada - atualizar registros', array_merge((array)$post, (array)$renovacaoRegs));
                foreach ($renovacaoRegs as $index => $renovacao) {
                    $renovacao->compra_efetuada = 1;
                    $renovacao->id_compra = $post['id_compra'];
                    $renovacao->update(false);
                }
            } else {
                $this->logs->log('Não há registros de renovação para as informações fornecidas', array_merge($post, $_SESSION, $arrProdutos));
            }
        } else {
            $this->logs->log('Faltam informações para atualizar tabela auxiliar', ['renovacao' => $session['renovacao_temporada'], 'documento' => $session['documento_temporada'], 'produtos' => $arrProdutos, 'post' => $post]);
        }
    }
    
    /**
     * Se for uma compra de assento, dar baixa no estoque do bloco
    */
    private function baixaEstoqueBloco($post){
        $session = Yii::$app->session;
        $session->open();
        $this->setLog('Baixa de estoque do bloco');
        if (!empty($post) && isset($session['user_id']) && !empty($session['user_id'])) {
            $compra = $this->consultarVendasFiltro(['codigo' => $post['id_compra']]);
            if ($compra->successo === '1' && !empty($compra->objeto[0])) {
                $carrinho = $this->consultarCarrinho($compra->objeto[0]->id_carrinho);
                if ($carrinho->successo === '1' && !empty($carrinho->objeto->produtos)) {
                    $this->logs->log('Carrinho', $carrinho);
                    $api = new ApiProduto();
                    foreach ($carrinho->objeto->produtos as $index => $produto) {
                        $prodJson=$api->buscarProdutoTags(null,$produto->codigo);
                        if($prodJson->tags['tipo']=='assento'){
                            echo 'é um assento - ';
                            print_r($prodJson);
                        } else {
                            echo 'não é assento - ';
                            print_r($prodJson);
                        }
                    }
                    exit();
                } else {
                    $this->logs->log('Carrinho não encontrado', $compra);
                }
            } else {
                $this->logs->log('Dados da compra não encontrados', $post);
            }
        } else {
            $this->logs->log('Faltam informações para a baixa do estoque', array_merge($post, $_SESSION));
        }
    }
     
    /**
     * Consultar vendas por período
     * @param $dateStart Início do período
     * @param $dateEnd Fim do período
     * @param $token Token: 'U' = Usuário (cliente específico) / 'V' = Vendedor
     * @return type
     */
    public function consultarVenda($dateStart='', $dateEnd='', $token='V')
    {
        $this->setLog('Consulta vendas');
        $returnApi = json_decode($this->call(
            'vendas/' . (empty($dateStart) && empty($dateEnd) ? date('d-m-Yt00:00:00', strtotime('-30days')) . '/' . date('d-m-Yt23:59:59') :  $dateStart . '/' . $dateEnd),
            'GET', NULL, true, $token));
        $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar vendas' : 'Consulta de vendas efetuada com sucesso', $returnApi);
        return $returnApi;
    }
    
    /**
    * Consultar vendas com filtros
    * @param $post Filtros para busca: data_ini, data_fim, descricao, operador, total, parcelas, trid, comprador, codigo
    * @param $token V - loja
    * @return type
    */
    public function consultarVendasFiltro($post=[], $token='V')
    {
        $this->setLog('Consulta vendas por filtros');
        if (isset($post) && !empty($post)) {
            $this->logs->log('Consultar vendas', $post);
            $returnApi = json_decode($this->call('vendas/', 'POST', $post, true, $token));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar vendas com filtros' : 'Consulta de vendas com filtros efetuada com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Consulta vendas com filtros inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível consultar vendas com filtros.');
    }    
    
    /**
     * Efetuar estorno de pagamento
     * @param array $post Dados para estorno: id_compra
     */
    public function estornarPagamento($post=[])
    {
        $this->setLog('Estorno de pagamento');
        $session = Yii::$app->session;
        $session->open();
        if (!empty($post) && is_array($post) && isset($session['user_token']) && !empty($session['user_token'])) {         
            $this->logs->log('Estornar pagamento', $post);
            $returnApi = json_decode($this->call('vendas/estornar/' . $post['id_compra'], 'PUT'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao estornar pagamento' : 'Estorno efetuado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Estorno de cobrança inválido. Formulário vazio ou cliente deslogado.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível efetuar o estorno da cobrança.');
    }
    
    /**
     * Para um código informado, gerar a imagem do qr-code
     * @param string $qr_code Código para geração de imagem qr-code
     * @return type
     */
    public function getQrCode($qr_code) 
    {
        if (!empty($qr_code)) {
            return (new QrCode($qr_code))
                ->setSize(150)
                ->setMargin(5)
                ->useForegroundColor(0, 0, 0);
        }
    }
    
    /**
     * Gerar o hash para uma compra
     * @param object $compra Dados de uma compra
     */
    public function setHashCompra($data='', $documento='', $codigo='') 
    {
        $hash = '';
        $this->setLog('Definicao de hash de compra');
        if (!empty($data) && !empty($documento) && !empty($codigo) && (int)$codigo > 0) {
            $this->logs->log('Definir hash de compra', $data . ', ' . $documento . ', ' . $codigo);
            $returnApi = $this->setCriptoHash(['mensagem' => $data . '|' . $documento . '|' . $codigo]);
            if ($returnApi->successo === '1') {
                $hash = base64_encode($returnApi->objeto->criptografia);
            }
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao gerar hash de compra' : 'Hash de compra gerado com sucesso', $returnApi);
            return $hash;
        }
        $this->logs->log('Definição de hash de compra inválida. Faltam parâmetros.', $data . ', ' . $documento . ', ' . $codigo);
        return $hash;
    }
    
    /**
     * Dado um hash, retornar suas informações originais
     * @param string $hash Hash de compra
     */
    public function getHashCompra($hash='')
    {
        if (!empty($hash)) {
            $criptografia = base64_decode($hash);
            return $this->getCriptoHash(['criptografia' => $criptografia]);
        }
        return [];
    }
    
    /**
     * Checar a quantidade de compras autorizadas por documento, por evento
     * @param integer $documento Documento para verificação
     * @param string $apelidoEvento Apelido do evento (deve ser único)
     */
    public function checarComprasDocumento($documento, $apelidoEvento)
    {
        $total = 0;
        $this->setLog('Checagem de compras por documento');
        if (!empty($documento) && !empty($apelidoEvento)) {
            $this->logs->log('Checar compras', $documento . ', ' . $apelidoEvento);
            $returnApi = $this->consultarVendasFiltro(['comprador' => $documento]);
            if ($returnApi->successo == '1' && count($returnApi->objeto) > 0) {
                foreach($returnApi->objeto as $index => $venda) {
                    if ($venda->vendas->status == 'Autorizado') {
                        $carrinho = (new ApiCarrinho)->consultarCarrinho($venda->id_carrinho);
                        if ($carrinho->successo == '1' && isset($carrinho->objeto->produtos[0]->tags)) {
                            foreach($carrinho->objeto->produtos[0]->tags as $tag) {
                                if ($tag->nome == 'matriz' && $tag->valor == $apelidoEvento) {
                                    $total += (int)$carrinho->objeto->produtos[0]->unidades;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $total;
    }
    
    /**
     * Validar limite de compra por cliente, por evento
     * @return type
     */
    public function validarLimiteCompra() 
    {
        $this->setLog('Validacao de limite de compra');
        $carrinho = (new ApiCarrinho)->consultarCarrinho();
        $session = Yii::$app->session;
        $session->open();
        if ($carrinho->successo == '1' && isset($carrinho->objeto->produtos) && !empty($carrinho->objeto->produtos)) {
            $this->logs->log('Carrinho de compras', $carrinho);
            $limite = [];
            foreach($carrinho->objeto->produtos as $produto) {
                $pularLimite=false;
                if(isset($produto->tags)){
                    $tags=ApiProduto::tagsArray($produto->tags);
                    foreach ($tags as $k => $v) {
                        if(substr($k, 0, 9) == 'reservar_' && $v==$session['documento']){
                            $pularLimite=true;
                            break;
                        }
                    }
                }
                if(!$pularLimite && isset($produto->tags))
                    $limite[$tags['matriz']] = isset($limite[$tags['matriz']]) ? $limite[$tags['matriz']] + (int)$produto->unidades : (int)$produto->unidades;                
            }
            $nivelAcesso=$session['nivelAcesso'];
            if($nivelAcesso=='super-admin' || $nivelAcesso=='operadorBilheteria')
                return (new ApiCarrinho)->getApiSuccess('Limite de compra não foi atingido');
            $this->logs->log('Compras efetuadas', $session['limiteCompra']);
            foreach($limite as $key => $value) {
                $total = (int)$value + (int)$session['limiteCompra'][$key];
                if ($total > 6) {
                    $this->logs->log('Limite ultrapassado', $key);
                    return (new ApiCarrinho)->getApiError('Limite de compra por CPF e evento atingido.');
                }
            }
            return (new ApiCarrinho)->getApiSuccess('Limite de compra não foi atingido');
        }
        $this->logs->log('Carrinho vazio.');
        return (new ApiCarrinho)->getApiError('Carrinho vazio.');
    }    
    
    /**
    * Consultar venda e ocupante a partir do qrcode do voucher
    * @param array $vendaArr array com id da venda, id produto, ocupante
    * @return mixed o retorno da consulta da API
    */
    public function consultarVendasVoucher($vendaArr, $token='V')
    {
        $this->setLog('Consultar voucher');
        $this->logs->log('venda', $vendaArr);
        $returnApi = json_decode($this->call('vendas/', 'POST', ['codigo'=>$vendaArr[0]], true, $token));
        if($returnApi->successo === '0') throw new Exception("Venda não localizada (".$vendaArr[0].")", 1);            
        $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar vendas pelo qrcode' : 'Consulta de venda com qrcode efetuada com sucesso', $returnApi);
        return $returnApi;
        
        $this->logs->log('Consulta venda com qrcode inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível consultar venda com qrcode.');
    }
    /**
     * Verificar a conciliação do pagamento para operador de bilheteria ou super-admin
     * @param $post os dados enviados pelo formulário no fim do pagamento
     */
    public function validarConciliacao($post){
        $this->setLog('Iniciar venda com conciliacao de pagamento',$post);
        $session = Yii::$app->session;
        $session->open();
        if($session['nivelAcesso']=='super-admin' || $session['nivelAcesso']=='operadorBilheteria'){
            $postConciliacao=$post['Conciliacao'];
            if(empty($postConciliacao['tipo']) || empty($postConciliacao['id_conciliacao']))
                return (object)['successo'=>0,['mensagem'=>['erro'=>'Não foi encontrado o tipo e ID de conciliação. Venda recusada']]];            
            $this->logs->log('Conciliação autorizada com sucesso', $post);
            return (object)['successo'=>1, ['mensagem'=>'Conciliação autorizada com sucesso']];

         }
        return (object)['successo'=>0,['mensagem'=>['erro'=>'O usuário atual não tem permissão para venda com conciliação de pagamento.']]];
    }

    /**
     * Salvar informações do produto para a venda conciliada
     * @param $post os dados enviados pelo formulário no fim do pagamento
     * @param $postConciliacao tipo e id da conciliação
     */
//    public function infoCompraConciliacao($post,$postConciliacao){       
//        $this->getCarrinhoVenda($post['id_compra']);
//        $this->associarProdutoCliente($post);
//        $this->controleRenovaTemporada($post);
//        (new ApiCliente)->setSessaoLimiteCompra();
//        $this->setLog('Venda conciliada com forma de pagamento alternativa',[$post,$postConciliacao]);
//        $this->logs->log('Conciliação realizada com sucesso', [$post,$postConciliacao]);
//        return (object)['successo'=>'1', 'objeto'=>['mensagem'=>'Pagamento com conciliação efetuado com sucesso']];        
//     }

    /**
     * Recuperar carrinho aberto em sessão
     * @return boolean true se o carrinho está em aberto
    */
    public static function carrinhoAberto(){
        $session = Yii::$app->session;
        $session->open();
        $carrinho=(new ApiCarrinho)->consultarCarrinho($session['idCarrinho']);
        if(isset($carrinho->objeto))
            return !$carrinho->objeto->codigo_cobranca>0;
        else return false;
    }
     
    /**
     * Consultar produtos de uma venda por período
     * @param int $codigo o código da venda
     * @return produtos a partir do carrinho
     */
    public function buscarProdutosVenda($codigo)
    {
        $this->setLog('Consulta produtos da venda');
        $returnVenda = json_decode($this->call('vendas/', 'POST', ['codigo'=>$codigo], true));
        $this->logs->log($returnVenda->successo === '0' ? 'Falha ao consultar vendas' : 'Consulta de vendas efetuada com sucesso', $codigo);
        $this->logs->log('Consultar carrinho de compras', $returnVenda->objeto[0]->id_carrinho);
        $returnCarrinho = json_decode($this->call('carrinho/' . $returnVenda->objeto[0]->id_carrinho, 'PUT'));
        $this->logs->log($returnCarrinho->successo === '0' ? 'Falha ao consultar carrinho de compras' : 'Carrinho de compras consultado com sucesso', $returnVenda->objeto[0]->id_carrinho);
        return $returnCarrinho;
    }

    /**
     * Consultar carrinho por tags
     * @param array $post array de tags para consulta
     * @param string $tagsDe carrinho ou produto
     * @return object IDs de carrinho que possuam as tags pesquisadas
     */
    public function consultarCarrinhoTags($post,$tagsDe='carrinho')
    {
        $this->setLog('Consulta de carrinho de compras por tags');
        $returnApi = json_decode($this->call('carrinho/', 'PUT', ['cobranca'=>['status'=>'1'], 'tags'=>[$tagsDe=>$post]]));
        $this->logs->log('Tags:',['tags'=>[$tagsDe=>$post]]);
        $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar carrinho de compras por tags' : 'Consulta de carrinho de compras por tags com sucesso', $returnApi);
        return $returnApi;        
        $this->logs->log('Consulta de carrinho de compras por tags inválido. Formulário vazio.');
        return $this->getApiError('Não foi possível consultar o carrinho de compras por tags.');
    }

    /**
     * Consultar carrinho pelo comprador
     * @param string $comprador documento do comprador
     * @return object IDs de carrinho que possuam as tags pesquisadas
     */
    public function consultarCarrinhoComprador($documento)
    {
        $this->setLog('Consulta de carrinho de compras por documento');
        $returnApi = json_decode($this->call('carrinho/', 'PUT', ['cobranca'=>['status'=>'1'], 'comprador'=>['documento'=>$documento]]));
        $this->logs->log('Documento: '.$documento);
        $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar carrinho de compras por documento' : 'Consulta de carrinho de compras por documento com sucesso', $returnApi);
        return $returnApi;        
        $this->logs->log('Consulta de carrinho de compras por documento inválido. Formulário vazio.');
        return $this->getApiError('Não foi possível consultar o carrinho de compras por documento.');
    }
     
    /**
    * Consultar vendas por período trazendo ocupantes de assentos
    * @param $dateStart Início do período
    * @param $dateEnd Fim do período
    * @return type
    */
    public function consultarVendaOcupantes($dateStart='', $dateEnd='')
    {
      $this->setLog('Consulta ocupantes');
      $returnApi = json_decode($this->call(
          'mineirao/vendas/' . (empty($dateStart) && empty($dateEnd) ? date('d-m-Yt00:00:00', strtotime('-30days')) . '/' . date('d-m-Yt23:59:59') :  $dateStart . '/' . $dateEnd),
          'GET', NULL, true, 'V'));
      $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar ocupantes' : 'Consulta de ocupantes efetuada com sucesso');
      return $returnApi;
    }

    /**
     * Completar o objeto de vendas com todas as informações de produto e ocupante para visão total do evento de compra
     */
    public function dadosCompletos($vendas){
        if($vendas->successo=='0') return $vendas;
        $_vendas;
        foreach ($vendas->objeto as $objVenda) {
            if($objVenda->vendas->codigo_status==2) continue;
            $carrinho = json_decode($this->call('carrinho/', 'PUT', ['cobranca'=>['codigo'=>$objVenda->vendas->codigo]]));
            $_vendas[]=$this->addCarrinhoVenda($objVenda,$carrinho);
        }
        $vendas->objeto=$_vendas;
        return $vendas;
    }

    /**
     * Copiar o objeto de venda para adicionar uma propriedade "carrinho"
     */
    public function addCarrinhoVenda($venda,$carrinho){
        $obj=new \stdClass();
        foreach (get_object_vars($venda) as $k=>$v) {
            $obj->$k=$v;
        }
        $obj->carrinho=$carrinho;
        return $obj;
    }

    /**
     * Trazer somentes dados de produtos vendidos em um carrinho
     */
    public function dadosProdutos($idCarrinho){
        $returnCarrinho = json_decode($this->call('carrinho/' . $idCarrinho, 'PUT'));
        if ($returnCarrinho->successo && isset($returnCarrinho->objeto))
            return $returnCarrinho->objeto;
    }

    /**
     * Validar objeto do carrinho e se dados do boleto estão corretos
     */
    public function verificarInfoCompraBoleto($cobranca,$documento){
        $carrinho=$this->consultarCarrinho();
        if(!$carrinho->successo || !isset($carrinho->objeto)) return false;
        if($cobranca['valor']!=$carrinho->objeto->totais->total) return false;
        if($cobranca['validade']!=date('d/m/Y')) return false;
        if($cobranca['unidades']!='1') return false;
        if($cobranca['forma']!='1') return false;
        if($cobranca['tipo']!='Boleto') return false;
        if(empty($documento)) return false;
        return true;
    }

    /**
     * Enviar cobranca de boleto. Este método apenas prepara o objeto e o envio é feito usando o método enviarCobranca
    */
    public function enviarCobrancaBoleto($cobranca,$sacado,$boleto){
        $post=[
            'trid'=>$cobranca['trid'],
            'documento'=>$sacado['documento'],
            'valor'=>$cobranca['valor'],
            'unidades'=>$cobranca['unidades'],
            'forma'=>$cobranca['forma'],
            'tipo'=>$cobranca['tipo'],
            'mensagem'=>$cobranca['mensagem'],
            'boleto'=>[
                'seunumero'=>$boleto['seunumero'],
                'mensagem'=>$boleto['mensagem'],
            ],
            'validade'=>$cobranca['validade'],
            'creditar'=>$cobranca['creditar']
        ];
        return $this->enviarCobranca($post);
    }
    
    /**
     * Identifica cobrança e conclui o pagamento usando QRCode
     * @param $codigo o código da cobrança
     * @param $qrcode QR Code do usuário a ser debitado
     * @param $senha a senha para pagamento por QR Code parametrizada como 999999
     * @todo solicitar senha do usuário?
     */
    public function concluirCobrancaCreditos($codigo,$qrcode,$senha)
    {
        $this->setLog('Compra usando créditos');
        $returnApi = json_decode($this->call('cobranca/'.$codigo.'/'.$qrcode, 'PUT', ['senha'=>$senha],true, 'A'));
        $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar cobrança' : 'Consulta de cobrança feita com sucesso', $returnApi);
        if ($returnApi->successo === '1') {
            $this->limparSessionCarrinho($returnApi);
            $this->getCarrinhoVenda($codigo);
            //$this->associarProdutoCliente(['id_compra'=>$codigo]);
            $this->controleRenovaTemporada(['id_compra'=>$codigo]);
            (new ApiCliente)->setSessaoLimiteCompra();
        } else
            $this->forcarLimparSessionCarrinho();
        return $returnApi;
    }  
}
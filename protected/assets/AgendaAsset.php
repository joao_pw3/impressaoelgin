<?php
namespace app\assets;

use yii\web\AssetBundle;

class AgendaAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'nginelt/js/visuallightbox.js'
    ];
    public $depends = [
    ];
    public $publishOptions = [
        'forceCopy' => true,
    ];
}

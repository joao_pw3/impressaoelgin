$(".imprimir-filipeta").click(function () {
	var compra=$(this).data("compra");
	data={"compra":compra};
	doAjax($("#url-filipeta").val(), "post", data, "json", 3000, "filipetaEntradaOK", "filipetaEntradaWrong");
});

$("#bilheteria-qrcode").click(function(){
	$(this).val("");
	$("#bilheteria-documento").val("");
});
$("#bilheteria-documento").click(function(){
	$(this).val("");
	$("#bilheteria-qrcode").val("");
});
$("#consultar-qrcode-form").submit(function(){
	if($("#bilheteria-qrcode").val()=="" && $("#bilheteria-documento").val()=="") return false;
});
var filipetaEntradaOK=function(data){
	$(data.html).ready(function() {
		printWindow(data.html);    	
	});
};
var filipetaEntradaWrong=function(data){
	showMsg("Erro ao gerar a filipeta. Entre em contato com o administrador");
};
<?php
use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error container">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
        
    </div>

    <p>
        O erro acima ocorreu enquanto o servidor processava sua requisição.
    </p>
    <p>
        Por favor, contate-nos se você acredita que este é um erro de servidor. Grato.
    </p>

</div>

<?php
namespace app\assets;

use yii\web\AssetBundle;

class CupomAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'js/ajax.js',
        'js/cupom.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\validators\ValidationAsset',
        'kartik\number\NumberControlAsset'
    ];
    public $publishOptions = [
        'forceCopy' => true,
    ];
}

var fazParcelamento = false;
var parcelas=[];
$("#formaPagamento").change(function(e){
    $('#parcelas option').remove();
    //$('.field-parcelas').hide();
    if(e.target.value !='') {
        if(e.target.value==1){
            if(fazParcelamento){
                doAjax ($('#url-venda').val(), 'POST', $('#form-venda').serialize(), 'json', 5000, 'vendaOk', 'vendaWrong');
            } else {
                var total=$('#form-venda #vendaTotal').val();
                parcelas[1] = '1 parcela de R$ '+total;
                data={'total': total, 'parcelas':  parcelas};
                vendaOk(data);
            }
        } else {
            if(e.target.value==4) {
                $('#especie-mostra').show();
            } 
            var total=$('#form-venda #vendaTotal').val();
            data={'total': total};
            vendaOk(data);
        }
    }
}); 

$('#imprimir-filipeta').click(function () {
    var conteudo=$('#mostra-filipeta').html(); 
    printWindow(conteudo);
});

function removeCartao(){
    var forma=$("#formaPagamento option");
    $(forma).each(function(index) {
        if(index!=1&&index!=0)
            $(this).remove();
    });
}

function vendaOk (data) {
    if(data.parcelas){
        var dropdown = $("#parcelas");
        $('.field-parcelas').show();
        $.each(data.parcelas, function(k,v) {
            if(k!=0)
                dropdown.append($("<option />").val(k).text(v));
        }); 
    }
} 
function vendaWrong(data) {
    showMsg(data.message);
}
function pagamento(data) {
    switch(parseInt(data.forma)){
        case 4:
            var dataCobranca={
                'trid': $('#trid').val(),
                'documento_comprador': $('#documento_comprador').val(),
                'formaPagto': $('#formaPagamento option:selected').val()
            };
            doAjax ($('#url-cobranca-conciliada').val(), 'POST', dataCobranca, 'JSON', 10000, 'cobrancaOK', 'cobrancaErr');
            break;
        case 2:
            $("#cappta-checkout-iframe").removeClass('hidden');
            pagamentoDebito(capptaObj,parseFloat(data.valor));
            break;
        case 1:
            $("#cappta-checkout-iframe").removeClass('hidden');
            pagamentoCredito(capptaObj,parseFloat(data.valor),data.parcelas);
            break;
    }   
    return false;
} 

var pgtoOk=function(r){ 
    var formaPgto =parseInt($("#confirmaVenda #formaPagamento").val()),
        trid=$("#trid").val(), 
        forma=r.installments,
        data={'trid':trid,'forma':forma, 'formaPagto':formaPgto, 'conciliador':r,'adquirente':3};
    doAjax($('#url-cobranca-conciliada').val(), 'post', data, 'json', 3000, 'cobrancaOK', 'cobrancaWrong');
};
var pgtoErr=function(r){
    showMsg('Erro ao processar o pagamento. Entre em contato com o administrador');
};

var cobrancaOK=function(data){
    doAjax($('#url-filipeta').val(), 'post', data, 'json', 3000, 'filipetaOK', 'filipetaWrong');
};

var cobrancaWrong=function(data){
    showMsg('Erro ao processar a cobrança. Entre em contato com o administrador');
};

var cancelarOk = function(r) {
    var data = {'guid': merchantCheckoutGuid, 'conciliador': r};
    estornar(data);
};

var cancelarErr = function(r) {
    showMsg('Erro ao processar o cancelamento. Entre em contato com o administrador');
};

var filipetaOK=function(data){
    showMsg('A compra foi realizada com sucesso! Foi gerada a filipeta para o cliente');
    $('.bordArredondBilhet').addClass('esconde-mapa');
    $('.cupom-codigo').attr('disabled', 'disabled');
    $('#div-pagamento-efetuado').removeClass('hidden');
    $('#confirmaVenda').remove();
    $('#especie-mostra').remove();        
    $(data.html).ready(function() {
        $('#mostra-filipeta').html(data.html);
    });
};

var filipetaWrong=function(data){
    showMsg('Erro ao gerar a filipeta. Entre em contato com o administrador');
};

var estorno=function(id){
    var cancelar=parametrosCancelar(capptaObj, id);
    if(cancelar.ok){
        $("#cappta-checkout-iframe").removeClass('hidden').css('z-index','3000');
        cancelamentoVenda(capptaObj,id);
    }
};

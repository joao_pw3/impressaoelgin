<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Api;
use app\modules\admin\controllers\AbstractController;
use app\modules\admin\models\MuseuVendas;
use SimpleXMLElement;
/**
 * Controller de Eventos
 */
class MuseuController extends AbstractController
{   
    public function actionIndex(){

        return $this->render('index');
    }
    
    public function actionRegistraVendas($cancelar = false)
    {   
        if($cancelar) {
            $this->redirect('registra-vendas');
        }

        $post = Yii::$app->request->post();
        $total = 0;
        $parcelas = [''=>'Parcelas'];
        $message = null;

        if ($post) 
        {
            try {

                $total = number_format($post['total'],2,'.','');
                
                if($total > 0)
                {
                    $maxParcelas=12;
                    $initParcela=1;

                    while($initParcela<=$maxParcelas)
                    {
                        $label=$initParcela>1 ?' parcelas ' :' parcela ';
                        $parcelas[$initParcela]=$initParcela.$label.'de R$ '.number_format($total/$initParcela,2,',','.');
                        $initParcela++;                        
                    }     
                }     
                else 
                {
                    $message = 'Valor total precisa ser maior que zero';
                }  
            } catch (\Exception $e)
            {
                $message = 'Valor total não é um número válido';
            }
        } 

        return $this->render('registra-vendas',['total'=> $total, 'parcelas'=> $parcelas, 'message' => $message]);
    }

    public function actionConfirmaVenda()
    {   
        if (Yii::$app->request->post()) 
        {
            $content = null;
            $post = Yii::$app->request->post();
            
            $museuVendas = new MuseuVendas();

                try {
                $data = [
                    'adq_afiliacao_key' => $post['data']['acquirerAffiliationKey'], 
                    'adq_codigo_autorizacao' => $post['data']['acquirerAuthorizationCode'], 
                    'adquirente' => $post['data']['acquirerName'], 
                    'administrative_code' => $post['data']['administrativeCode'], 
                    'merchant_checkout_guid' => $post['guid'], 
                    'cartao_bandeira' => $post['data']['cardBrandName'], 
                    'cartao_bandeira_codigo' => $post['data']['cardBrandCode'], 
                    'cartao' => $post['data']['customerCardPan'], 
                    'pagamento_tipo' => $post['tipo'],
                    'pagamento_valor' => $post['total'],
                    'pagamento_parcelas' => $post['data']['installments'],
                    'sequencia_unica' => $post['data']['uniqueSequentialNumber'],
                    'autorizacao_data' => $post['data']['acquirerAuthorizationDateTime'],
                    'recibo_consumidor' => $post['data']['receipt']['customerReceipt'],
                    'recibo_estabelecimento' => $post['data']['receipt']['merchantReceipt'],
                    'recibo_simples' => $post['data']['receipt']['reducedReceipt'],
                    'status' => 'Autorizado',
                    'user_id' => Yii::$app->user->identity->usuario
                ];

                $museuVendas->attributes = $data;

                if ($museuVendas->save())
                {
                    $json = [
                        'successo' => '1',
                        'result' => 'O seu pagamento e o processo foi realizado'
                    ];
                } 
                else 
                {
                    $json = [
                        'successo' => '0',
                        'code' => 300,
                        'result' => "O seu pagamento foi realizado. Porém no processo houve uma falha."
                    ];

                    $content .= 'Code: 300<br />';
                    $content .= 'Message: Problemas na validação dos dados. Com isso Não grava no db.';

                    //$this->disparaEmail($content);
                }

                $json['recibo_consumidor'] = $data['recibo_consumidor'];
                $json['recibo_estabelecimento'] = $data['recibo_estabelecimento'];

            
            } catch (\Exception $e) {
                
                $json = [
                    'successo' => 0,
                    'code' => $e->getCode(),
                    'result' => "O seu pagamento foi realizado. Porém no processo houve uma falha."
                ];

                $content .= 'Code: '.$e->getCode().'<br />';
                $content .= 'Message: '.$e->getMessage();

                //$this->disparaEmail($content);
            }
        }

        return json_encode($json);
    }

    public function actionVendas($todas = 1)
    {
        $museuVendas = new MuseuVendas();

        if($todas == 0) {
            $vendas = $museuVendas->find()
                        ->orderBy([
                            'autorizacao_data' => SORT_DESC,
                            'id' => SORT_DESC,
                        ])
                        ->where('status = "Autorizado"')
                        ->andWhere('DATE_FORMAT(autorizacao_data, "%Y-%m-%d") = CURDATE()')
                        ->all();            
        } elseif($todas == 1) {
            $vendas = $museuVendas->find()
                        ->orderBy([
                            'autorizacao_data' => SORT_DESC,
                            'id' => SORT_DESC,
                        ])
                        ->all();                   
        }
            
        return $this->render('vendas',['vendas'=> $vendas, 'todas' => $todas]);        
    }

    public function actionConfirmaCancelamento()
    {
        if (Yii::$app->request->post()) 
        {
            $content = null;
            $post = Yii::$app->request->post();
            
            $museuVendas = new MuseuVendas();

            try {

                $venda = $museuVendas->find()
                    ->where('administrative_code = '.$post['controle'])
                    ->where('status = "Autorizado"')
                    ->where('DATE_FORMAT(autorizacao_data, "%Y-%m-%d") = CURDATE()')
                    ->one();    

                $venda->status = 'Cancelado';    
                $venda->save(false);        

                $data = [
                    'adq_afiliacao_key' => $post['data']['acquirerAffiliationKey'], 
                    'adq_codigo_autorizacao' => $post['data']['acquirerAuthorizationCode'], 
                    'adquirente' => $post['data']['acquirerName'], 
                    'administrative_code' => $post['data']['administrativeCode'], 
                    'merchant_checkout_guid' => $post['guid'], 
                    'cartao_bandeira' => $post['data']['cardBrandName'], 
                    'cartao_bandeira_codigo' => $post['data']['cardBrandCode'], 
                    'cartao' => $post['data']['customerCardPan'], 
                    'pagamento_tipo' => $venda->pagamento_tipo,
                    'pagamento_valor' => $venda->pagamento_valor,
                    'pagamento_parcelas' => $post['data']['installments'],
                    'sequencia_unica' => $post['data']['uniqueSequentialNumber'],
                    'autorizacao_data' => $post['data']['acquirerAuthorizationDateTime'],
                    'recibo_consumidor' => $post['data']['receipt']['customerReceipt'],
                    'recibo_estabelecimento' => $post['data']['receipt']['merchantReceipt'],
                    'recibo_simples' => $post['data']['receipt']['reducedReceipt'],
                    'status' => 'Solicitação de Cancelamento',
                    'user_id' => Yii::$app->user->identity->usuario
                ];

                $museuVendas->attributes = $data;

                if ($museuVendas->save()) {
                    $json = [
                        'successo' => '1',
                        'result' => 'O seu pagamento foi cancelado.'
                    ];
                } else {
                    $json = [
                        'successo' => '0',
                        'code' => 300,
                        'result' => "O seu cancelamento foi realizado. Porém no processo houve uma falha."
                    ];

                    $content .= 'Code: 300<br />';
                    $content .= 'Message: Problemas na validação dos dados. Com isso Não grava no db.';

                    //$this->disparaEmail($content);
                }

                $json['recibo_consumidor'] = $data['recibo_consumidor'];
                $json['recibo_estabelecimento'] = $data['recibo_estabelecimento'];

            
            } catch (\Exception $e) {
                
                $json = [
                    'successo' => 0,
                    'code' => $e->getCode(),
                    'result' => "O seu pagamento foi realizado. Porém no processo houve uma falha."
                ];

                $content .= 'Code: '.$e->getCode().'<br />';
                $content .= 'Message: '.$e->getMessage();

                //$this->disparaEmail($content);
            }
        }

        return json_encode($json);
    }        

    public function actionExportarCsv(){
        $museuVendas = new MuseuVendas();
        $vendasExportar = $museuVendas->find()
            ->select('autorizacao_data, administrative_code, cartao, pagamento_valor, user_id, status')
            ->orderBy([
                'administrative_code' => SORT_ASC,
            ])
            ->all();    
        ini_set('max_execution_time', 300);
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=museu-vendas-site-mineirao_'.date('d-m-Y_H-i-s').'.csv');
        $output = fopen('php://output', 'w');
        fputcsv($output, ['Data', 'Controle','Cartao','Tipo','Valor','Operador','Status'],';');
        foreach ($vendasExportar as $row) {
            fputcsv($output, [
                $row->autorizacao_data, 
                $row->administrative_code, 
                $row->cartao,
                $row->pagamento_tipo == 'debito' ? 'Débito' : 'Crédito',
                number_format($row->pagamento_valor, 2, '.',','),
                $row->user_id,
                utf8_decode($row->status)
            ],';');
            }
        
    }

    protected function disparaEmail($content)
    {
        $api = new Api();

        $from = 'vendas@estadiomineirao.com.br';
        $to = 'ariel@easyforpay.com.br';
        $subject = 'Informações da operação de Vendas do Museu';

        $api->enviarEmailSwift($from, $to, $subject, $content);
    }
}
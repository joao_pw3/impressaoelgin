<?php
namespace app\modules\admin\controllers;

use Yii;
use app\models\CadastroInicialForm;
use app\modules\admin\models\PermissoesModelForm;
use app\modules\admin\controllers\AbstractController;

/**
 * Controller de ingressos.
 */
class UsuariosController extends AbstractController
{
    public function actionIndex(){
        return $this->render('index');
    }

    /**
     * Fazer um novo cadastro
     * Pesquisar antes se já existe um cadastro para este usuário (pesquisando pelo user_id)
     * Se estiver ok, chama endpoint da API para criar a conta
     */
    public function actionCadastro(){
        $cadInicial=new CadastroInicialForm();
        if ($cadInicial->load(Yii::$app->request->post()) && $cadInicial->save()) {
            return $this->redirect(['ficha','codigo'=>$cadInicial->codigo]);
        }
        return $this->render('cadastro_inicial', [
            'model'=>$cadInicial
        ]);

    }

    /**
     * Lista de operadores
     * Link para ver a ficha individual
     */
    public function actionListaOperadores(){
        $cadInicial=new CadastroInicialForm();
        $operadores=$cadInicial->listaOperadores();
        if($operadores!=null)
            return $this->render('lista-operadores', [
                'model'=>$operadores
            ]);
    }

    /**
     * Mostrar informações de um operador
    */
    public function actionFicha($codigo){
        $cadInicial=new CadastroInicialForm();
        $cadInicial->codigo=$codigo;
        $cadInicial->dadosOperador();

        return $this->render('usuario', [
            'model'=>$cadInicial
        ]);
    }

    /**
     * Editar informações de um operador
    */
    public function actionAtualizar($codigo){
        $cadInicial=new CadastroInicialForm();
        $cadInicial->codigo=$codigo;
        $cadInicial->dadosOperador();
        $cadInicial->scenario='update';
        if ($cadInicial->load(Yii::$app->request->post()) && $cadInicial->update()) {
            return $this->redirect(['ficha','codigo'=>$cadInicial->codigo]);
        }

        return $this->render('atualizar', [
            'model'=>$cadInicial
        ]);
    }

    /**
     * Excluir um operador. Somente POST
     */
    public function actionExcluir(){
        $cadInicial=new CadastroInicialForm();
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $cadInicial->email=Yii::$app->request->post()['PermissoesModelForm']['email'];
            $excluir=$cadInicial->excluirOperador();
            echo json_encode($excluir);
        }
    }

    /**
     * Gestão de permissões. Mostrar as permissões em uma lista e atribuir a um usuário identificado pelo e-mail
    */
    public function actionPermissoes(){
        $model=new PermissoesModelForm;
        if ($model->load(Yii::$app->request->post())) {
            if($model->salvarListaPermissoes()){
                self::sessaoActionsPermissoes(true);
                return $this->render('permissoesOk');
            }
        }
        return $this->render('permissoes',[
            'model'=>$model,
            'permissoes'=>$model->listaResumida()
        ]);
    }

    /**
     * Listar as permissões de um usuário e ativar ou desativar
    */
    public function actionListarPermissoes(){
        $model=new PermissoesModelForm;
        $lista=null;
        if ($model->load(Yii::$app->request->post())) {
            $lista=$model->listaGrupoPermissoesUsuario();
        }
        return $this->render('listar-permissoes',[
            'model'=>$model,
            'lista'=>$lista,
        ]);
    }

    /**
     * Listar as permissões de um usuário e ativar ou desativar
    */
    public function actionAtivarRevogarPermissoes(){
        $model=new PermissoesModelForm;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            if($model->ativarRevogarGrupoPermissao()){
                self::sessaoActionsPermissoes(true);
                echo json_encode(['successo'=>'1']);
            }
            else
                echo json_encode(['successo'=>'0','erro'=>$model->getErrors()]);
        }
        
    }
}
<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CompraModel]].
 *
 * @see CompraModel
 */
class CompraQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return CompraModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CompraModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

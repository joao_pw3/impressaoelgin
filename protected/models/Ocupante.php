<?php
namespace app\models;

use Yii;
use app\models\Carrinho;
use app\models\CarrinhoBilheteria;
use yii\base\Model;
use app\models\Tags;
use app\models\Produto;

/**
 * Classe model "Ocupante" de um produto
 *
 * @property string $produto
 * @property string $nome
 * @property string $documento
 * @property string $email
 * @property string $telefone
 * @property string $id_compra
 */
class Ocupante extends Model
{
    public $produto_codigo;
    public $cupom_codigo;
    public $nome;
    public $documento;
    public $email;
    public $telefone;
    public $id_compra;
    public $indice;
    public $lista;
    
    private $tokenBilheteria;

    /**
     * Construtor prepara os atributos para se comportarem como tags
     */
    public function __construct($tokenBilheteria=false){
        $this->nome = (object)['nome'=>'','valor'=>''];
        $this->documento = (object)['nome'=>'','valor'=>''];
        $this->email = (object)['nome'=>'','valor'=>''];
        $this->telefone = (object)['nome'=>'','valor'=>''];
        $this->tokenBilheteria = $tokenBilheteria;
    }
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['nome', 'produto_codigo', 'cupom_codigo', 'documento'], 'required', 'message' => 'Campo obrigatório'],
            [['email', 'telefone', 'indexOcupante'], 'safe'],
            [['nome'], 'string', 'max' => 50, 'tooLong' => 'máx.50 caracteres'],
            [['documento'], 'string', 'min' => 5, 'max' => 20, 'tooLong' => 'máx.20 caracteres', 'tooShort' => 'mín.5 caracteres'],
            [['id_compra'], 'integer'],
        ];
    }

    /**
     * Salvar tags de ocupante - As tags são os dados dos ocupantes, precedidas do código do produto correspondente
     * @Override
     * Usar o método save() com endpoints de tags no carrinho
    */
    public function save($runValidation=true, $attributeNames=null ) 
    {
        $session = Yii::$app->session;
        $session->open();
        $objApiCliente=new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));        
        if (!$this->validate()) {
            return false;
        }
        $api = $this->tokenBilheteria ? new CarrinhoBilheteria($objApiCliente) : new Carrinho($objApiCliente);
        $produto=new Produto($objApiCliente);
        $produto->codigo=$this->produto_codigo;
        $produto->buscarProduto();
        $indice=$this->indice;
        if($produto->tagAssento())
            $indice=1;
        $return1 = $api->atualizarCarrinho(['tag_nome' => $this->produto_codigo . '_'.$indice.'_nome', 'tag_valor' => $this->nome]);
        $return2 = $api->atualizarCarrinho(['tag_nome' => $this->produto_codigo . '_'.$indice.'_documento', 'tag_valor' => $this->documento]);
        if (!empty($this->email)) {
            $api->atualizarCarrinho(['tag_nome' => $this->produto_codigo . '_'.$indice.'_email', 'tag_valor' => $this->email]);
        }
        if (!empty($this->telefone)) {
            $api->atualizarCarrinho(['tag_nome' => $this->produto_codigo . '_'.$indice.'_telefone', 'tag_valor' => $this->telefone]);
        }
        return $return1->successo == '1' && $return2->successo == '1';
    }
    
    /**
     * Verificar existência do valor de uma tag para um produto
     * @param mixed $tag Objeto da tag
     * @param integer $codigo Código do produto que faz parte do nome da tag
     * @param integer $indice contagem da linha do ingresso
     * @param string $campo Campo da tag que, junto com o código do produto, forma o nome da tag
     * @return void|string nada se a tag não corresponder ao campo | string com o valor da tag ou uma string vazia se não tiver valor ou se a tag não existir
     */
    public static function checkTagCarrinho($tag, $codigo, $indice, $campo) {
        if (strpos($tag->nome, $campo) === FALSE || strpos($tag->nome, $codigo) === FALSE) {
            return false;
        }
        if ($tag->nome == $codigo . '_' .$indice . '_' . $campo && $tag->valor != '') { 
            return $tag->valor;
        } 
        return false;
    }

    /**
     * Verificar se as tags do carrinho contém dados de nome e documento para cada assento
     * @param mixed $carrinho Objeto do carrinho atual que contém produtos e tags
     * @return boolean se as informações mais básicas foram inseridas - nome e documento de cada produto
     */
    public function infoOcupantes($carrinho){
        if (!isset($carrinho->tags) || !isset($carrinho->produtos)) {
            return false;
        }
        $nomesOk = [];
        $docsOk = [];
        $emailsOk = [];
        $telsOk = [];
        foreach($carrinho->produtos as $produto){
            $nome = false;
            $doc = false;
            $email = false;
            $tel = false;
            foreach($carrinho->tags as $tag){
                if ($tag->nome == $produto->codigo . '_nome' && $tag->valor != '') {
                    $nome = true;
                }
                if ($tag->nome == $produto->codigo . '_documento' && $tag->valor != '') {
                    $doc = true;
                }
                if ($tag->nome == $produto->codigo . '_email' && $tag->valor != '') {
                    $email = true;
                }
                if ($tag->nome == $produto->codigo . '_telefone' && $tag->valor != '') {
                    $tel = true;
                }
            }
            $nomesOk[] = $nome;
            $docsOk[] = $doc;
            $emailsOk[] = $email;
            $telsOk[] = $tel;
        }
        if (count($nomesOk) != count($carrinho->produtos) || in_array(0, $nomesOk)) {
            return false;
        }
        if (count($docsOk) != count($carrinho->produtos) || in_array(0, $docsOk)) {
            return false;
        }
        return true;
    }

    /**
     * Retornar informações do ocupante de um produto e setar atributos como tags
     * @param mixed $carrinho retorno de consulta à API
     * @param mixed $produto retorno de consulta à API
     * @param integer $index contagem da linha do ingresso
     */
    public function ocupanteIngresso($carrinho, $produto, $index){
        if (isset($carrinho->objeto->tags)) {
            $arrTag=Tags::arrayTags($carrinho->objeto->tags);
            foreach ($arrTag as $key => $value){
                $this->setValorOcupantePorTag($key,$value,$produto->codigo,$index,'nome');
                $this->setValorOcupantePorTag($key,$value,$produto->codigo,$index,'documento');
                $this->setValorOcupantePorTag($key,$value,$produto->codigo,$index,'telefone');
                $this->setValorOcupantePorTag($key,$value,$produto->codigo,$index,'email');
            }
        }
    }

    /**
     * Percorre um array de tags comparando índices para atribuir os valores às propriedades desta classe
     */
    private function setValorOcupantePorTag($key,$value,$produto,$index,$attr){    
        if($key == $produto.'_'.$index.'_'.$attr){
            $this->$attr->nome=$attr;
            $this->$attr->valor=$value;
        }
    }

    /**
     * Salvar ocupantes em lote
     */
    public function salvarEmLote($post)
    {
        $session = Yii::$app->session;
        $session->open();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));        
        $ws = $this->tokenBilheteria ? new CarrinhoBilheteria($objApiCliente) : new Carrinho($objApiCliente);
        $objeto = [];
        $carrinho = [];
        if (!isset($post['cupom_codigo']) || count($post['cupom_codigo']) == 0) {
            $ws->limparDaSessao();
            return (object)['successo' => '0', 'erro' => ['mensagem' => 'Seu carrinho foi esvaziado.']];
        }
        // primeiro, isolamos os tipos de ingresso e calcula a qtde
        foreach ($post['cupom_codigo'] as $key => $value) {
            $carrinho[$post['produto_codigo'][$key]][$value]['tags'][]=['nome'=>$post['nome'][$key],'documento'=>$post['documento'][$key],'telefone'=>$post['telefone'][$key],'email'=>$post['email'][$key]];
            $carrinho[$post['produto_codigo'][$key]][$value]['unidades']=count($carrinho[$post['produto_codigo'][$key]][$value]['tags']);
            if(isset($post['taxa'][$key]))
                $carrinho[$post['produto_codigo'][$key]][$value]['taxa']= $post['taxa'][$key];
        }
        //depois, montamos o objeto para a API
        $api = [];
        foreach ($carrinho as $codigo => $tipo) {
            foreach ($tipo as $ingresso => $obj) {
                $produto = ['codigo' => $codigo, 'unidades' => $obj['unidades']];
                if(isset($obj['taxa']))
                    $produto['taxa'] = ['id' => $obj['taxa']];
                $cupom = '';
                if ($ingresso != 'inteira') {
                    $cupom =  '_' . $ingresso;
                    $produto['cupom'] = ['id' => $ingresso];
                }
                $api['produtos'][] = $produto;
                foreach ($obj['tags'] as $index => $tag) {  
                    $strTag = $codigo . $cupom . '_' . ($index + 1);
                    foreach($tag as $key => $value) {
                        $api['tags'][] = ['nome' => $strTag . '_' . $key, 'valor' => $value];
                    }
                }
            }
        }
        return $ws->salvarCarrinhoCompleto($api, $session->get('idCarrinho'));
    }
}
<?php
use \yii\helpers\Url;
?>
<h1>Produto</h1>
<h2><?=$produto->nome?></h2>
<h3>Remover anexos</h3>
<?php foreach ($anexos->lista as $model) {?>
	<p>Nome: <?=$model->nome?></p>
	<p>Tipo: <?=$model->tipo?></p>
	<p>Url: <a href="<?=$model->url?>" target="_blank">Abrir em nova janela</a></p>
	<p><a href="<?=Url::to(['remover-anexos', 'produto'=>$produto->codigo, 'nomeAnexo'=>$model->nome, 'tipoAnexo'=>$model->tipo])?>" class="btn btn-danger">Excluir</a></p>
<?php }
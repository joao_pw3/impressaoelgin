<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\modules\admin\assets\UsuariosAsset;

UsuariosAsset::register($this);
$this->title = 'ADM Estádio Mineirão :: Gerenciar permissão';
?>
<div class="container row">
    <h2 class="tituloGeral"><i class="fa fa-clone"></i> Gerenciar permissão</h2>    
    <div class="col-md-12">
        <?php $form = ActiveForm::begin(['id'=>'form-remover-ativar-permissao']); ?>
        <div class="form-group">
            <?= $form->field($model, 'usuario')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Buscar' , ['class'=>'btn btn-success']) ?>
        </div>
        <?php ActiveForm::end(); 
        
        if(isset($lista,$model->usuario) && !$model->hasErrors()){?>
        <h3>Mostrando permissões para <?=$model->usuario?></h3>
        <div class="row">
            <?php if(count($lista[0]>0)) { ?>
            <div class="col-md-6">
                <p>Permissões ativas</p>
                <ul>
                <?php  
                    foreach ($lista[0] as $k=>$v) {
                        echo '<li>'.$v.' - <a class="removerAtivarPermissao removerPermissao" data-key="'.$v.'"><span class="glyphicon glyphicon-remove"></span></a></li>';
                    }
                ?>
                </ul>
            </div>
            <?php } 
            if(count($lista[1]>0)) { ?>
            <div class="col-md-6">
                <p>Permissões inativas</p>
                <ul>
                <?php  
                    foreach ($lista[1] as $k=>$v) {
                        echo '<li>'.$v.' - <a class="removerAtivarPermissao ativarPermissao" data-key="'.$v.'"><span class="glyphicon glyphicon-ok"></span></a></li>';
                    }
                ?>
                </ul>
            </div>
            <?php } ?>
        </div>
        <?php }?>
    </div>
</div>
<?php
yii\bootstrap\Modal::begin([
   'size'   => 'modal-md',
   'id'     => 'modal-confirmar',
   'options'=> [
       'class' => 'modal-center'
   ],
   'clientOptions' => [
       'keyboard' => false,
       'backdrop' => 'static',
   ],
   'footer' => '
       <div class="col-md-12" align="center">
           <button type="button" class="btn btn-danger" id="btn-cancel">Cancelar</button>
           <button type="button" class="btn btn-success" id="btn-continue">Continuar</button>
       </div>'
]);
yii\bootstrap\Modal::end();

$this->registerJs('
	$(".removerAtivarPermissao").click(function(){
		var key=$(this).data("key");
		if($(this).hasClass("ativarPermissao")){
			modalPermissoes("Ativar Permissão","ativar","'.Url::to(['/admin/usuarios/ativar-revogar-permissoes']).'","'.$model->usuario.'",key,0,"ativarPermissaoOk")
		}
		if($(this).hasClass("removerPermissao")){
			modalPermissoes("Revogar Permissão","revogar","'.Url::to(['/admin/usuarios/ativar-revogar-permissoes']).'","'.$model->usuario.'",key,1,"removerPermissaoOk")
		}
	});
	',\yii\web\View::POS_END
);?>
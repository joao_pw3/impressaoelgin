<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\bootstrap\Modal;

AppAsset::register($this);

$this->beginPage() 
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <?= Html::csrfMetaTags() ?>
        <title>Vendas | Estádio Mineirão</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width">
        <meta name="viewport" content="initial-scale=1.0">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <link href="<?=Yii::getAlias("@web")?>/images/favicon.ico" rel="shortcut icon">
        <link href="<?=Yii::getAlias("@web")?>/images/favicon.ico" rel="icon">
        <?php $this->head() ?>
        <script src='https://www.google.com/recaptcha/api.js?hl=pt-BR'></script>
    </head>

    <?php $this->beginBody() ?>

    <body>
        <?php
        if(YII_ENV=='dev')
            echo '<div id="identHomolog" class="text-center" style="display:none;">Você está em ambiente de homologação</div>';
        ?>

        <section class="header show-for-large">
            <?php include "header.php"; ?>
        </section>
		
		<div style="margin:0px auto; width:1200px;">
			<?php include "menu.php"; ?>
		</div>
		
		<section class="headerMobile">
			<div>
				<?= Html::img('@web/images/mineirao-50-anos.png', ['alt' => 'Estádio Mineirão', 'id' => 'logo50', 'title' => 'Estádio Mineirão', 'border' => '0']); ?>
				<?= Html::img('@web/images/mineirao-50-anos2.png', ['alt' => 'Estádio Mineirão', 'id' => 'logo502', 'title' => 'Estádio Mineirão', 'border' => '0']); ?><br>
				<a target="_top" href="<?= Url::to(['site/cliente-acesso']); ?>" class="icon icon-user" about="_blank" title="Olá<?= !empty($session['nome']) ? ', ' . $session['nome'] : ''; ?>. Clique para ir à área de cliente."></a>
				<a target="_top" href="<?= Url::to(['site/resumo']); ?>" class="icon icon-shopping" about="_blank" title="<?= empty($session['user_id']) || empty($session['documento']) || empty($session['codigo_cartao']) ? 'Para acessar o resumo de sua compra, finalize o seu cadastro e coloque produtos em seu carrinho de compras.': 'Resumo de sua compra'; ?>"></a>
			</div>
		</section>
        
        <?= $content ?>

        <footer>
            <?php include "rodape.php"; ?>
        </footer>

        <div class="copyright wow fadeInUp" style="visibility: hidden; animation-name: none;">
            <div class="row align-middle">
                <?php include "copyright.php"; ?>
            </div>
        </div>


<!-- Modal - Mensagens -->
<?php Modal::begin([
    'header' => '<h4 align="center">Atenção</h4>',
    'size'   => 'modal-sm',
    'id'     => 'modal-mensagem',
    'options'=> [
        'class' => 'modal-center'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ]
]);
Modal::end(); ?>

<!-- Modal - Imagens -->
<?php Modal::begin([
    'size'   => 'modal-lg',
    'id'     => 'modal-imagem',
    'options'=> [
        'class' => 'modal-center fade'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ]
]);
Modal::end(); ?>

    <?php $this->endBody() ?>    
<?php $this->endPage() ?>
    </body>
</html>
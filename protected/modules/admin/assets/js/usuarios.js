function modalPermissoes(header,acao,url,usuario,key,ativo,callback){
	$("#modal-confirmar .modal-header").html("<h3 align=\"center\">"+header+"</h3>");
	$("#modal-confirmar .modal-body").html("Você vai "+acao+" a permissão \""+key+"\" para este usuário. Continuar?");
	$("#modal-confirmar").show();
	$("#btn-cancel").click(function(){$("#modal-confirmar").hide();})
	$("#btn-continue").click(function(){
		$(this).button("loading");
		doAjax(url, "POST", 
			{
				"PermissoesModelForm[usuario]":usuario,
				"PermissoesModelForm[permissoesUsuario]":key,
				"PermissoesModelForm[inativo]":ativo
			},
			"JSON", 5000, callback, "ativarRemoverPermissaoErro"
		);
	})
}

function removerPermissaoOk(data){
	controleModalOk("Permissão revogada com sucesso");
}

function ativarPermissaoOk(data){
	controleModalOk("Permissão concedida com sucesso");
}

function controleModalOk(msg){
	$("#btn-continue").button("reset");
	$("#modal-confirmar").hide();
	$("#modal-mensagem .modal-body").html("<p>"+msg+"</p>");
	$("#modal-mensagem .modal-body").append("<p class=\"text-center\"><button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal-mensagem\" aria-hidden=\"true\">Fechar</button></p>");
	$("#modal-mensagem").show();
	$("#modal-mensagem .modal-body .btn, #modal-mensagem .modal-header .close").on("click",function(){
		$("#form-remover-ativar-permissao").submit();
	});
}

function ativarRemoverPermissaoErro(data){
	$("#btn-continue").button("reset");
	$("#modal-confirmar").hide();
	var infoErro=data.erro;
	if(infoErro.usuario){
		$("#permissoesmodelform-usuario ~ .help-block").html(infoErro.usuario);
		$(".field-permissoesmodelform-usuario").addClass("has-error");
	}
}
<?php
use \yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="container">
	<div class="row">
		<h2 class="tituloGeral"><i class="fa fa-clone"></i> Agenda - Excluir Evento</h2>
	</div>
	<br>
<br>	
	<div class="boxArredondado">
		<h2 style="margin-top:0px;"><?=$evento->nome?></h2>
	</div>
	<br>
	<br>
	<div class="alert alert-danger">
		Tem certeza que quer excluir esse evento? Esta ação não pode ser revertida.
	</div>
	<br>
	<br>
	<?php
	$form = ActiveForm::begin(['id' => 'evento-excluir']); ?>

	<?= $form->field($evento, 'codigo')->hiddenInput()->label(false) ?>

	<div class="form-group" style="text-align:center;">
		<?= Html::submitButton('Excluir', ['class' => 'btn btn-primary btn-pagar']) ?>
		<?= Html::button('Cancelar', ['class' => 'btn btn-primary btn-pagar', 'onclick'=>'javascript:history.back()']) ?>
	</div>

	<?php ActiveForm::end(); ?>	
	<br>
	<br>
</div>
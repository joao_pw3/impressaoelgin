 <?php
use yii\helpers\Url;
?>
<div class="container">
    <div class="col-md-8">
        <div id="mapaVermelho">
            <div class="fundoEstadio">
                
                <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="100%" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd; margin-left:25px;"
                    viewBox="0 0 2480 707"
                    xmlns:xlink="http://www.w3.org/1999/xlink" id="lugares" class="leste">
                    
                    <path class="fil4 str1 arquibancada" d="M2 323l110 -158c269,187 636,303 1040,303l0 0 0 0 0 0 0 0c404,0 770,-116 1040,-303l109 157c-800,473 -1566,453 -2300,2z"/>
                    <path class="fil4 str1 arquibancada" d="M1153 468l0 201"/>
                    <path class="fil4 str1 arquibancada" d="M179 424l93 -162"/>
                    <path class="fil4 str1 arquibancada" d="M362 509l76 -172"/>
                    <path class="fil4 str1 arquibancada" d="M551 578l59 -183"/>
                    <path class="fil4 str1 arquibancada" d="M746 629l41 -193"/>
                    <path class="fil4 str1 arquibancada" d="M944 660l22 -200"/>
                    <path class="fil4 str1 arquibancada" d="M1941 503l-74 -166"/>
                    <path class="fil4 str1 arquibancada" d="M1752 572l-57 -177"/>
                    <path class="fil4 str1 arquibancada" d="M1558 624l-40 -188"/>
                    <path class="fil4 str1 arquibancada" d="M1360 657l-21 -197"/>
                    <path class="fil4 str1 arquibancada" d="M2124 419l-90 -158"/>
                </svg>
            </div>
        </div>
    </div>
</div>
<?php $this->registerJs('
    var blocos = mapa[1].blocos;
    $.each(blocos,function(i,o){
        var a=o.anexos;
        var svg;
        $.each(a,function(i,o){
            console.log(o.svg)
            if(o.svg!=undefined)
                $("#lugares").prepend(o.svg)
        })
        $("#lugares").html($("#lugares").html())
    })
    ',\yii\web\View::POS_END)?>
<div class="container">
    <br>
    <h1 class="titPass">Troca de senha de acesso</h1>

    <div class="col-md-12 col-xs-12" align="left">
        <div>
            <?php  if ($retorno->successo === '1') { ?>
            <p>Sua senha de acesso foi trocada com sucesso.</p> 
            <p>Uma mensagem foi enviada para o e-mail de seu cadastro com uma nova senha numérica de 6 dígitos. Por favor, verifique.</p>
            <?php } else { ?>
            Não foi possível trocar sua senha de acesso. Erro: <?= $retorno->erro->mensagem; ?>.
            <?php } ?> 
        </div>
    </div>
</div>

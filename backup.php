<?php
set_time_limit(0);
ini_set('memory_limit', '256M'); 

//use ZipArchive;
//use Phar;
//use PharData;

class Backup
{
    //PRODUÇÃO
    //public $logPath = '/var/www/protected/logs/';

    //HOMOLOGAÇÃO
    public $logPath = '/var/www/html/mineirao/protected/logs/';
    
    //LOCAL
//    public $logPath = "C:/wamp/www/mineirao/protected/logs/";

    /**
     * Função para compactar pasta e seus arquivos - ZIP
     */
    public function compressZip($pasta)
    {
        // Normaliza o caminho do diretório a ser compactado
        $pastaCompleta = realpath($this->logPath . $pasta);
        
        if (!is_dir($pastaCompleta)) {
            return 'A pasta ' . $this->logPath . $pasta . ' não existe. Por favor, verifique.';
        }
        
        // Caminho com nome completo do arquivo compactado
        $zip_file = $this->logPath . basename($pastaCompleta) . '.zip';

        // Inicializa o objeto ZipArchive
        $zip = new ZipArchive();
        $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        //Identificar os arquivos da pasta e contá-los
        $files = array_diff(scandir($this->logPath . $pasta), array('..', '.'));
        $total = count($files);

        //Adicionar os arquivos ao zip
        foreach ($files as $index => $file) {
            // Pula os diretórios.
            if (!is_dir($file)) {
                $zip->addFile($pastaCompleta . '\\' . $file, $file);
            }
        }
        $totalFiles = $zip->numFiles;
        $msg = 'Arquivos compactados: ' . $totalFiles . ($total == $totalFiles ? ' (Ok)' : '(Erro)') . '<br>';
        
        // Fecha o objeto. Necessário para gerar o arquivo zip final.
        $zip->close();

        // Retorna o caminho completo do arquivo gerado
        $msg .= 'Arquivo zip gerado: ' . $zip_file;
        return $msg;
    }
    
    /**
     * Função para compactar pasta e arquivos - GZ
     * @param string $pasta Pasta a ser compactada 
     */
    public function compressGz($pasta) 
    {
        $retorno = [];
        
        // Normaliza o caminho do diretório a ser compactado
        $pastaCompleta = realpath($this->logPath . $pasta);
        
        //verificar se pasta de logs existe
        if (!is_dir($pastaCompleta)) {
            return 'A pasta ' . $this->logPath . $pasta . ' não existe. Por favor, verifique.';
        }
        
        //Identificar os arquivos da pasta e contá-los
        $files = array_diff(scandir($this->logPath . $pasta), array('..', '.'));
        $total = count($files);
        
        // Caminho com nome completo do arquivo compactado
        $tar_file = $this->logPath . basename($pastaCompleta) . '.tar';
        $gz_file = $this->logPath . basename($pastaCompleta) . '.tar.gz';
        
        //validar se arquivo tar.gz já foi criado anteriormente
        if (!is_file($gz_file)) {
            $backup = new PharData($tar_file);
            $backup->buildFromDirectory($pastaCompleta);
            $backup->compress(Phar::GZ);

            $totalFiles = $backup->count();
            $retorno = [
                'status' => '1', 
                'msg' => 'Arquivos compactados: ' . $totalFiles . ($total == $totalFiles ? ' (Ok)' : '(Erro)') . '<br>' . 'Arquivo gz gerado: ' . $gz_file . '<hr>'
            ];
            unset($backup);

            if (is_file($tar_file)) {
                unlink($tar_file);
            }
        } else {
            $retorno = [
                'status' => '0',
                'msg' => 'Arquivo já existe: ' . $gz_file . '<hr>'
            ];
        }
        return $retorno; 
    }
    
    /**
     * Após fazer o backup de uma pasta de logs, remover seus arquivo e remover a pasta
     * @param string $pasta Pasta a ser removida
     */
    public function apagarLog($pasta) 
    {
        if (!empty($pasta)) {
            $files = array_diff(scandir($this->logPath . $pasta), array('..', '.'));
            foreach ($files as $index => $file) {
                if (!is_dir($this->logPath . $pasta . '/' . $file)) {
                    unlink($this->logPath . $pasta . '/' . $file);
                }
            }
            rmdir($this->logPath . $pasta);
            return 'Pasta removida: ' . $this->logPath . $pasta . '<hr>';
        }
    }    
    
    /**
     * Identificar pastas de logs do sistema e gerar os arquivos GZ correspondentes (backup)
     */
    public function Backuplog () 
    {
        $files = array_diff(scandir($this->logPath), array('..', '.'));
        
        foreach($files as $index => $file) {
            //ignorar pasta de logs do próprio dia - validar se $file é diretório
            if (date('Ymd') != $file && is_dir($this->logPath . $file)) {
                //ajustar permissões da pasta para permitir gravação
                chmod($this->logPath . $file, 0777);
                $return = $this->compressGz($file); 
                echo $return['msg'];
                //apagar arquivos e pasta que foi compactada
                if ($return['status'] == '1') {
                    echo $this->apagarLog($file);
                } 
            }
        }
    }
    
}

$backup = new Backup();

echo $backup->Backuplog();
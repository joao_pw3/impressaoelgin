<?php
use yii\helpers\Url;
use yii\helpers\Html;
$session = Yii::$app->session;
$session->open();
?>

<div class="cab">
    <div class="row">
        <div class="small-12 columns">
            <a href="http://estadiomineirao.com.br/" class="bt-icon-small icon-next verde light"><span>Home</span></a>
        </div>
    </div>
</div>
<div class="row align-bottom boxe">
    <div class="small-12 large-3 columns branding">
        <a href="http://estadiomineirao.com.br/">
            <?= Html::img('@web/images/mineirao-50-anos.png', ['alt' => 'Estádio Mineirão', 'title' => 'Estádio Mineirão', 'border' => '0']); ?>
        </a>
    </div>
    <div class="small-12 large-4 columns podcast reset text-right">
        <?php if(!Yii::$app->user->isGuest){ ?>
            <p class="text_medium">Olá, <strong><?=Yii::$app->user->identity->nome_cliente?></strong></p>
        <?php } ?>
    </div>
    <div class="small-12 large-5 columns text-right social">
        <div class="box">
            <a target="_top" href="<?= Url::to(['site/cliente-acesso']); ?>" class="icon icon-user black larg10" about="_blank" title="Olá<?= !empty($session['nome']) ? ', ' . $session['nome'] : ''; ?>. Clique para ir à área de cliente."></a>
            <a target="_top" href="<?= Url::to(['site/resumo']); ?>" class="icon icon-shopping black" about="_blank" title="<?= empty($session['user_id']) || empty($session['documento']) || empty($session['codigo_cartao']) ? 'Para acessar o resumo de sua compra, finalize o seu cadastro e coloque produtos em seu carrinho de compras.': 'Resumo de sua compra'; ?>"></a>
            <div class="social-network">
                <a target="_blank" href="https://www.facebook.com/mineiraooficial" class="icon icon-facebook white" about="_blank"></a>
                <a target="_blank" href="https://twitter.com/mineirao" class="icon icon-twitter white"></a>
                <a target="_blank" href="https://www.instagram.com/mineirao" class="icon icon-instagram white"></a>
                <a target="_blank" href="https://www.youtube.com/channel/UCIJey5lcfMIjoI4I7wUtbSw" class="icon icon-youtube white"></a>
            </div>
            <a href="http://estadiomineirao.com.br/#" class="icon icon-search white"></a>        
        </div>
    </div>
</div>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="row">
	<div class="small-12 medium-5 large-6 columns">
		<div class="row">
			<div class="small-12 large-6 columns item box wow fadeInUp" data-wow-delay=".6s" style="visibility: hidden; animation-delay: 0.6s; animation-name: none;">
				<h2 class="uper verde">Localização</h2>
				<hr>
				<p>Endereço: Av. Antônio Abrahão Caram, <br>1001 - São José, Belo Horizonte - MG, <br> CEP: 31275-000</p>
				<a href="http://estadiomineirao.com.br/como-chegar/mobilidade/">
                                    <?= Html::img('@web/images/mapa-mineirao.jpg', ['alt' => 'Mapa Mineirão', 'title' => 'Mapa Mineirão', 'border' => '0']); ?>
				</a>
			</div>
		</div>
	</div>
	<div class="small-12 medium-7 large-6 columns box wow fadeInUp" data-wow-delay=".7s" style="visibility: hidden; animation-delay: 0.7s; animation-name: none;">
		<h2 class="uper verde">Veja Também</h2>
		<hr>
		<div class="row">
		<ul class="column small-order-1 medium-order-1 no-list">
			<li id="menu-item-1262" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1262">
				<a href="http://estadiomineirao.com.br/o-mineirao/">O Mineirão</a>
				<ul class="sub-menu">
					<li id="menu-item-1494" class="menu-item menu-item-type-post_type_archive menu-item-object-history menu-item-1494"><a href="http://estadiomineirao.com.br/o-mineirao/historia/">História</a></li>
					<li id="menu-item-1490" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1490"><a href="http://estadiomineirao.com.br/o-mineirao/estrutura/">Estrutura</a></li>
					<li id="menu-item-1492" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1492"><a href="http://estadiomineirao.com.br/o-mineirao/imprensa/">Imprensa</a></li>
					<li id="menu-item-1493" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1493"><a href="http://estadiomineirao.com.br/o-mineirao/mapa/">Mapa do Estádio</a></li>
				</ul>
			</li>
			<li id="menu-item-1495" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1495"><a href="http://estadiomineirao.com.br/esplanada/">Esplanada</a></li>
			<li id="menu-item-1496" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1496"><a href="http://estadiomineirao.com.br/compre-seu-ingresso/">Compre seu Ingresso</a></li>
			<li id="menu-item-13220" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13220"><a href="http://estadiomineirao.com.br/feirao/">Feirão</a></li>
		</ul>
		<ul class="column small-order-1 medium-order-1 no-list">
			<li id="menu-item-1497" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1497">
				<a href="http://estadiomineirao.com.br/museu-e-visita/">Museu e Visita</a>
				<ul class="sub-menu">
					<li id="menu-item-1498" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1498"><a href="http://estadiomineirao.com.br/museu-e-visita/salas/">Salas</a></li>
					<li id="menu-item-1499" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1499"><a href="http://estadiomineirao.com.br/museu-e-visita/educativo/">Educativo</a></li>
					<li id="menu-item-1500" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1500"><a href="http://estadiomineirao.com.br/museu-e-visita/ingressos/">Ingressos</a></li>
					<li id="menu-item-1501" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1501"><a href="http://estadiomineirao.com.br/museu-e-visita/visita-guiada/">Visita guiada</a></li>
				</ul>
			</li>
			<li id="menu-item-1502" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1502">
				<a href="http://estadiomineirao.com.br/passaportes-mineirao/">Passaportes Mineirão</a>
				<ul class="sub-menu">
					<li id="menu-item-1503" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1503"><a href="http://estadiomineirao.com.br/passaportes-mineirao/tribuna/">Tribuna</a></li>
					<li id="menu-item-1504" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1504"><a href="http://estadiomineirao.com.br/passaportes-mineirao/exclusivo/">Exclusivo</a></li>
					<li id="menu-item-1505" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1505"><a href="http://estadiomineirao.com.br/passaportes-mineirao/camarote/">Vermelho</a></li>
				</ul>
			</li>
		</ul>
		<ul class="column small-order-1 medium-order-1 no-list">
			<li id="menu-item-1506" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1506"><a href="http://estadiomineirao.com.br/faca-seu-evento/">Faça seu Evento</a></li>
			<li id="menu-item-1507" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1507">
				<a href="http://estadiomineirao.com.br/como-chegar/">Como Chegar</a>
				<ul class="sub-menu">
					<li id="menu-item-1508" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1508"><a href="http://estadiomineirao.com.br/como-chegar/mobilidade/">Mobilidade</a></li>
					<li id="menu-item-1509" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1509"><a href="http://estadiomineirao.com.br/como-chegar/vagas-disponiveis/">Vagas disponíveis</a></li>
				</ul>
			</li>
			<li id="menu-item-1510" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1510"><a href="http://estadiomineirao.com.br/contato/">Contato</a></li>
			<li id="menu-item-1511" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1511"><a href="<?= Url::to(['site/faq']); ?>">FAQ</a></li>
			<li id="menu-item-1512" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1512"><a href="<?= Url::to(['site/ativar']); ?>">ATIVAR</a></li>			
		</ul>
	</div>
</div>    

<?php 
$publicKey = YII_ENV_DEV ? 'T65806F4EE8' : 'P14B22BD377';

$this->registerJs("
	var __kdt = __kdt || [];
__kdt.push({'public_key': '".$publicKey."'}); 
	(function() {   
		var kdt = document.createElement('script');   
		kdt.id = 'kdtjs'; kdt.type = 'text/javascript';   
		kdt.async = true;    kdt.src = 'https://i.k-analytix.com/k.js';   
		var s = document.getElementsByTagName('body')[0];   
		s.parentNode.insertBefore(kdt, s);
  	 })(); 
", \yii\web\View::POS_END); ?>
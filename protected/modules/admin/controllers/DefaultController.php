<?php
namespace app\modules\admin\controllers;

use Yii;
use app\models\LoginForm;
use app\modules\admin\controllers\AbstractController;
use app\models\Cliente;
use app\models\Operadores;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends AbstractController
{
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Efeturar logout do site
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(['index']);
    }

    /**
     * Login action.
     * Procurar token de usuário na sessão
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['index']);
        }
        $model = new LoginForm();
        if($model->autologin($this->getTokenSessao())){
            self::sessaoActionsPermissoes();
            $this->setUnidadeSessao();
            return $this->goBack();
        }
        //se não houver token, pegar dados via post e gravar o cookie
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $this->setTokenSessao(Yii::$app->user->identity->user_token) or $model->addError('username','Erro validando usuário');
            self::sessaoActionsPermissoes(true);
            $this->setUnidadeSessao();
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Gravar a unidade em sessão após o login
     */
    protected function setUnidadeSessao(){
        $session = $this->_sessionOpen();
        $session->set('token_unidade', (Yii::$app->user->identity->user_token));
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', Yii::$app->user->identity->user_token);
        if(Yii::$app->user->identity->tipo=='O'){
            $op=Operadores::getOperador($this->getTokenSessao());
            $objApiCliente->buscarUnidadePor('documento',$op->objeto->empresa->documento);
            $session->set('token_unidade', $objApiCliente->user_token);
        }
        $session->set('nome_unidade', $objApiCliente->nome);
    }
}

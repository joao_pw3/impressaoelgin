<?php
namespace app\models;
use app\models\Ws;

/**
 * Classe para gerenciamento de operadores - Easy for Pay - Easy for Pay
 */
class WsOperadores extends Ws
{
    /**
     * Método construtor - obrigado a passar o token do vendedor para separação de unidades
    */
    public function __construct($tokenVendedor=''){
        if(empty($tokenVendedor))
            throw new \Exception("Token vendedor não foi declarado", 1);        
        $this->setToken($tokenVendedor,'V');
    }
    
    /**
     * Incluir operador
     * @return object {'successo': '1'}
     */
    public function cadastrarOperador($post)
    {
        return json_decode($this->call('operador/', 'POST', $post, true, 'V'));
    }
    
    /**
     * Lista de operadores
     * @return object {'successo': '1', 'objeto': '...'}
     */
    public function listaOperadores()
    {
        return json_decode($this->call('operador/', 'GET', null, true, 'V'));
    }
    
    /**
     * Atualizar um registro de operador
     * @return object {'successo': '1', 'objeto': '...'}
     */
    public function alterarOperador($post){
        return json_decode($this->call('operador/'.$post['email'], 'PUT', $post, true, 'V'));
    }
    
    /**
     * Excluir um registro de operador
     * @return object {'successo': '1', 'objeto': '...'}
     */
    public function excluirOperador($email){
        return json_decode($this->call('operador/'.$email, 'DELETE', null, true, 'V'));
    }

    
    /**
     * Dados do operador
     * @return object {'successo': '1', 'objeto': '...'}
     */
    public function getOperador()
    {
        return json_decode($this->call('operador/'.$this->getToken('U'), 'GET', null, true, 'U'));
    }
}
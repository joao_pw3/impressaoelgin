<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "compra".
 *
 * @property string $id
 * @property string $id_compra
 * @property string $area
 * @property string $bloco
 * @property double $valor
 * @property integer $unidades
 * @property integer $aceite
 */
class CompraModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'compra';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_compra', 'area', 'bloco', 'valor'], 'required'],
            [['id_compra', 'unidades', 'aceite'], 'integer'],
            [['valor'], 'number'],
            [['area', 'bloco'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_compra' => Yii::t('app', 'Id Compra'),
            'area' => Yii::t('app', 'Area'),
            'bloco' => Yii::t('app', 'Bloco'),
            'valor' => Yii::t('app', 'Valor'),
            'unidades' => Yii::t('app', 'Unidades'),
            'aceite' => Yii::t('app', 'Li e aceito os termos de uso'),
        ];
    }

    /**
     * @inheritdoc
     * @return CompraQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CompraQuery(get_called_class());
    }
}

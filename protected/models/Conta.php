<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\WsConta;
use app\models\Cartao;
use app\models\WsAuth;

/**
 * Conta é o modelo para o formulário de cliente
 *
 */
class Conta extends Model
{
    public $user_id;
    public $user_token;
    public $documento;
    public $nome;
    public $apelido;
    public $sexo;
    public $sexo_nome;
    public $data_nasc;
    public $ddd;
    public $celular;
    public $cep;
    public $endereco;
    public $num;
    public $compl;
    public $cidade;
    public $cidade_nome;
    public $cidade_uf;
    public $uf;
    public $bairro;
    public $url_foto;
    public $senha;
    public $senha_repeat;
    public $retornoConsulta;
    public $cliente_aceite;
    public $objetoContaUsuario;
    public $generico_1;
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['user_id'], 'required', 'message' => 'O e-mail não pode ficar em branco','on'=>['cadastro','cadastro-gerente']],
            [['senha', 'senha_repeat'],'required','on'=>['cadastro','cadastro-gerente']],
            ['senha_repeat', 'compare', 'compareAttribute' => 'senha', 'on' => ['cadastro','cadastro-gerente']],
            [['user_id'], 'required', 'message' => 'O e-mail não pode ficar em branco','on'=>'login'],
            [['senha', 'finalizar_compra'],'required','on'=>'login'],  
            [['nome', 'documento','data_nasc','ddd','celular','cep','endereco','num','cidade','cidade_nome','cidade_uf','uf','bairro', 'generico_1'], 'required', 'on' => 'alterar'],   
            [['cliente_aceite'], 'required', 'requiredValue' => '1', 'message' => 'Leia e aceite o regulamento para continuar','on'=>'cadastro'],
            [['documento'], 'string', 'min' => 14, 'max' => 18],
            [['nome'], 'string', 'min' => 7, 'max' => 64],
            [['apelido'], 'string', 'max' => 20],
            [['sexo'], 'string', 'max' => 1],
            [['sexo_nome'], 'string', 'max' => 9],
            [['data_nasc'], 'date', 'format' => 'php:d/m/Y'],
            [['ddd'], 'integer'],
            [['ddd'], 'string', 'max' => 2],
            [['celular'], 'string', 'min' => 9, 'max' => 10],
            [['senha', 'senha_repeat'], 'string', 'min' => 4, 'max' => 10],
            [['cep'], 'string', 'max' => 9],
            [['endereco'], 'string', 'max' => 128],
            [['num'], 'string', 'max' => 20],
            [['compl'], 'string', 'max' => 20],
            [['cidade'], 'integer'],
            [['cidade'], 'string', 'max' => 11],
            [['cidade_nome'], 'string', 'max' => 64],
            [['cidade_uf'], 'string', 'max' => 64],
            [['uf'], 'string', 'max' => 2],
            [['bairro'], 'string', 'max' => 32],
            [['url_foto'], 'string', 'max' => 255],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'nome' => 'Nome',
            'user_id' => 'E-mail',
            'documento' => 'CPF/CNPJ',
            'data_nasc' => 'Data de Nascimento',
            'sexo' => 'Gênero',
            'ddd' => 'DDD',
            'celular' => 'Celular',
            'cep' => 'CEP',
            'endereco' => 'Endereço',
            'num' => 'Número',
            'compl' => 'Complemento',
            'bairro' => 'Bairro',
            'cidade_nome' => 'Cidade',
            'cidade_uf' => 'Cidade / UF',
            'senha_repeat' => 'Confirme sua Senha',
            'generico_1' => 'Industriário',
            'cliente_aceite' => 'Leia e aceite os temos de uso e as políticas de privacidade'
        ];        
    }

    public function criarConta() 
    {
        if($this->validate())
        {
            $apiAr = [
                'user_id'=> $this->user_id,
                'senha'=> $this->senha                
            ];
            $cadastro = (new WsConta)->_post($apiAr);

            if($cadastro->successo=='0')
                return $this->addError('user_id', $cadastro->erro->mensagem);
            
            $this->user_token=$cadastro->objeto->user_token;
            return true;   
        }
        return false;           
    }    

    public function criarContaCompleta()
    {
        $session = Yii::$app->session;
        $session->open();

        if ($this->validate() && isset($session['user_token']) && !empty($session['user_token']))
        {

            $comprador = [
                'generico_1' => $this->generico_1,
                'documento' => isset($this->documento) ? preg_replace('/(\D)/','', $this->documento) : '',
                'nome' => $this->nome,
                'sexo' => $this->sexo,
                'data_nasc' => $this->data_nasc,
                'ddd' => $this->ddd,
                'celular' => preg_replace('/(\D)/','', $this->celular),
                'cep' => $this->cep,
                'endereco' => $this->endereco,
                'num' => $this->num,
                'compl' => $this->compl,
                'cidade' => $this->cidade,
                'uf' => $this->uf,
                'bairro' => $this->bairro,
            ];

            $wsConta = new WsConta;
            $cadastro = $wsConta->_put($session['user_token'], ['comprador' => $comprador]);
           
            if ($cadastro->successo == '1')
                return true;
            
            if($cadastro->successo == '0'){
                switch ($cadastro->erro->codigo){
                    case '023':
                        $this->addError('documento',$cadastro->erro->mensagem);
                        $this->documento=null;
                        return false;
                    break;
                    default:
                        $this->addError('nome', 'Não foi possível enviar os dados de cadastro - '.$cadastro->erro->mensagem);
                     return false;
                }
            }
            
        }
    }    

    /**
     * Atualizar conta de cliente
     * @param $post Dados do cliente
     */
    public function alterar()
    {   
        $session = Yii::$app->session;
        $session->open();
        if ($this->validate() && isset($session['user_token']) && !empty($session['user_token'])) {
            $comprador = [
                'generico_1' => $this->generico_1, 
                'nome' => $this->nome,
                'sexo' => $this->sexo,
                'data_nasc' => $this->data_nasc,
                'ddd' => $this->ddd,
                'celular' => preg_replace('/(\D)/','', $this->celular),
                'cep' => $this->cep,
                'endereco' => $this->endereco,
                'num' => $this->num,
                'compl' => $this->compl,
                'cidade' => $this->cidade,
                'uf' => $this->uf,
                'bairro' => $this->bairro,
            ];
            if (!isset($session['documento']) || empty($session['documento']) && !empty($this->documento)) {
                $comprador['documento'] = $this->documento;
            }

            $alterar = (new WsConta)->_put($session['user_token'], ['comprador' => $comprador]);

            if ($alterar->successo === '1') {
                $this->setSessaoClienteDados([
                    'nome' => $this->nome, 
                    'documento' => $this->documento,
                ]);
            }
            return $alterar;
        }
    }    

    public function getDadosComprador()
    {
        $session = Yii::$app->session;
        $session->open();
        if (isset($session['user_token']) && !empty($session['user_token'])) 
        {
            $wsConta = new WsConta;
            $cadastro = $wsConta->_get($session['user_token']);
            if(!$cadastro->successo)
                return $this->addError('user_id', $cadastro->erro->mensagem);

            return $cadastro;
        }
    }

    public function setSessaoCliente($post=[], $api=[])
    {
        if (is_array($post) && !empty($post) && isset($api->successo) && $api->successo === '1') {
            $session = Yii::$app->session;
            $session->open();
            $session['user_id'] = $post['user_id'];
            $session['user_token'] = $api->objeto->user_token;
            $this->setSessaoClienteDados();            
            //$this->setSessaoClienteCartao();
            $session->close();
        }
    }  

    public function setSessaoClienteDados() 
    {
        $returnApi = $this->getDadosComprador();
        if (isset($returnApi->objeto->comprador) && !empty($returnApi->objeto->comprador) && !empty($returnApi->objeto->comprador->nome) && !empty($returnApi->objeto->comprador->documento)) {
            $session = Yii::$app->session;
            $session->open();
            $session['nome'] = $returnApi->objeto->comprador->nome;
            $session['documento'] = $returnApi->objeto->comprador->documento;
            $session->close();
        }
    }    

    public function setSessaoClienteCartao()
    {
        $returnApi = (new Cartao)->consultarFavorito();
        if (!empty($returnApi->codigo_cartao)) {
            $session = Yii::$app->session;
            $session->open();
            $session['codigo_cartao'] = $returnApi->codigo_cartao;
            $session->close();
        }
    } 

    public function existeCliente()
    {
        $session = Yii::$app->session;
        $session->open();
        $clienteApi = isset($session['user_token']) && !empty($session['user_token']) ? $this->getDadosComprador() : [];    
        if(!isset($clienteApi->successo) || $clienteApi->successo!=1){
            return false;
        }
        return $clienteApi;
    }
    
    /**
     * Verificar se documento ou e-mail informado já estão em uso na API
     * @return boolean
     */
    public function existeCadastro ($post)
    {
        if (isset($post['documento']) || isset($post['user_id'])) {
            return (new WsAuth())->_post($post);
        }
        return false;
    }

    public function operador($token)
    {
        $checkTokenOperador=(new WsConta)->_getConta($token);
        if($checkTokenOperador && $checkTokenOperador->objeto->tipo =='O') {
            return true;
        }
        return false;
    }

    public function processoCadastro($statusConta)
    {
        $session = Yii::$app->session;
        $session->open();

        switch($statusConta){
            case 'CadastroIndex':
                if(!isset($session['user_token'])){
                    return '/cadastro';
                }
            break;
            case 'CadastroCompleto':
                if(!isset($session['documento'])){
                    return '/cadastro/completo';
                }
            break;  
        }   
        return false;
    }

    public function verificaClienteLogOn($statusConta=false, $acesso=false)
    {
        $session = Yii::$app->session;
        $session->open();
        
        $clienteApi = $this->existeCliente();

        if($clienteApi){
            if($this->operador($session['user_token']))
                return 'acesso-administrativo';
            
            if(!$acesso) {
                if(strpos(Yii::$app->request->url, 'cadastro'))
                    return '/cliente/index';

            }
            $this->loadFromApi($clienteApi->objeto);
             
        } else {
            if(strpos(Yii::$app->request->url, 'cliente'))
                return '/login';
            
            $processo = $this->processoCadastro($statusConta);
            if($processo) 
                return $processo;
            
        }
        return false;
    }

    public function loadFromApi($obj){
        if(!isset($obj->user_id,$obj->user_token,$obj->comprador)) return false;
        $this->user_token=$obj->user_token;
        $this->user_id=$obj->user_id;
        $this->documento=$obj->comprador->documento;
        $this->nome= $obj->comprador->nome;
        $this->apelido=$obj->comprador->apelido;
        $this->sexo=$obj->comprador->sexo;
        $this->data_nasc=$obj->comprador->data_nasc;
        $this->ddd=$obj->comprador->ddd;
        $this->celular=$obj->comprador->celular;
        $this->cep=$obj->comprador->cep;
        $this->endereco=$obj->comprador->endereco;
        $this->num=$obj->comprador->num;
        $this->compl=$obj->comprador->compl;
        $this->cidade=$obj->comprador->cidade;
        $this->cidade_nome=$obj->comprador->cidade_nome;
        $this->uf=$obj->comprador->uf;
        $this->bairro=$obj->comprador->bairro;
        $this->url_foto=$obj->comprador->url_foto;
    }

    public function historicoCompras() {}

    /** 
     * Criar conta de gerente a partir do ADM
    */
    public function criarContaGerente() 
    {
        if($this->validate())
        {
            $apiAr = [
                'user_id'=> $this->user_id,
                'senha'=> $this->senha                
            ];

            $cadastro = (new WsConta)->_post($apiAr);

            if(!$cadastro->successo==1){
                $this->addError('user_id',$cadastro->erro->mensagem);
                return false;
            }
            $this->user_token=$cadastro->objeto->user_token;
            return true;   
        }

        return false;           
    }
}
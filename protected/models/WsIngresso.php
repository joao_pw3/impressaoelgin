<?php
namespace app\models;

use app\models\Ws;

/**
 * Classe para Ingressos - Easy for Pay - Easy for Pay
 * Carga e ativação de ingressos
 */
class WsIngresso extends Ws
{
	/**
     * Método construtor - obrigado a passar o token do vendedor para separação de unidades
     */
    public function __construct($tokenVendedor=''){
        if (empty($tokenVendedor)) {
            throw new \Exception("Token vendedor não foi declarado", 1);        
        }
        $this->setToken($tokenVendedor, 'V');
    }

    /**
     * Reservar (Reserva uma quantidade N de ingressos para um evento específico) 
     * @param array $post Dados para a carga de ingresso: id_agenda, lote_origem, codigo_compra_limite
     */
    public function reservar($post = []){
        return json_decode($this->call('ingresso/reservar', 'PUT', $post));
    }

    /**
     * Consultar ingressos
     * @param array $post Dados para consulta: id_agenda, status, bloqueado, documento, lote_origem, lote_entrega, codigo_venda, nome, qrcode
     */
    public function consultar($post = []) {
        return json_decode($this->call('ingresso', 'POST', $post));
    }
    
    /**
     * Registrar entrada de lote em produção
     * @param $post Dados do lote: id_agenda, lote_origem
     */
    public function producao($post = []) {
        return json_decode($this->call('ingresso/producao', 'PUT', $post));
    }
    
    /**
     * Registrar envio de lote da gráfica para o estádio
     * @param $post Dados do lote: id_agenda, lote_origem, lote_entrega
     */
    public function enviar($post = []) 
    {
        return json_decode($this->call('ingresso/enviar', 'PUT', $post));
    }

    /**
     * Registrar recebimento de lote de ingressos no estádio
     * @param $post Dados do lote: id_agenda, lote_origem
     */
    public function entregar($post = []) 
    {
        return json_decode($this->call('ingresso/entregar', 'PUT', $post));
    }

    /**
     * Associar (Vincula um ingresso a uma compra de um cliente, está será a operação de troca de voucher por ingresso)
     * @deprecated ATENÇAÕ: Este ending point vai deixar de ser usado, devido à melhorias no fluxo do ingresso
     */
    public function associar($post = []) 
    {
       return json_decode($this->call('ingresso/associar/', 'PUT', $post));
    }
    
    /**
     * Associar RFID a um ingresso
     * @param $post Dados: id_agenda, qrcode, rfid
     */
    public function associa($post = []) 
    {
        return json_decode($this->call('ingresso/associa', 'PUT', $post));
    }
    
    /**
     * Registrar liberação de ingresso(s) para a bilheteria
     * (lote ou individual)
     * @param $post Dados: id_agenda, qrcode, lote
     */
    public function liberar($post = []) 
    {
       return json_decode($this->call('ingresso/liberar', 'PUT', $post));
    }
    
    /**
     * Registrar retirada de ingresso na bilheteria
     * @param $post Dados: id_agenda, qrcode, nome, documento
     */
    public function retirar($post = []) 
    {
        return json_decode($this->call('ingresso/retirar', 'PUT', $post));
    }

    /**
     * Registrar a utilização de um ingresso em um evento
     * @param $post Dados: id_agenda, qrcode, data
     */
    public function utilizar($post = []) 
    {
        return json_decode($this->call('ingresso/utilizar', 'PUT', $post));
    }
    
    /**
     * Invalidar um ingresso
     * @param $post Dados: id_agenda, qrcode, motivo
     */
    public function invalidar($post = []) 
    {
        return json_decode($this->call('ingresso/invalidar', 'PUT', $post));
    } 
    
    /**
     * Bloquear / desbloquear ingresso
     * @param $post Dados: id_agenda, qrcode, bloqueado
     */
    public function bloquear($post = []) 
    {
        return json_decode($this->call('ingresso/bloquear', 'PUT', $post));
    } 
    
    /**
     * Solicitar segunda via de ingresso
     * @param $post Dados: id_agenda, qrcode
     */
    public function segundavia($post = []) 
    {
        return json_decode($this->call('ingresso/segundavia', 'PUT', $post));
    } 
    
    /**
     * Consultar totais - mesmos filtros de consulta de ingressos
     * @param array $post Dados para consulta: id_agenda, status, bloqueado, documento, lote_origem, lote_entrega, codigo_venda
     */
    public function consultarTotais($post = []) {
        return json_decode($this->call('ingresso/totais', 'POST', $post));
    }
    
}
<?php
namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\controllers\AbstractController;
use yii\web\Response;
use app\models\Cupom;
use app\models\Cliente;
use app\models\Agenda;

/**
 * Controle de Cupom
 */
class CupomController extends AbstractController
{
    /**
     * Lista de cupons
     * @param boolean $matriz Boolean indicando se é uma ação para matriz ou não
     */
    public function actionIndex($matriz=false)
    {
        $session = $this->_sessionOpen();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
        
        $agenda = new Agenda($objApiCliente);
        (boolean)($matriz) ? $agenda->buscarEventosPorFiltro(['nome' => 'MATRIZ%']) : $agenda->listaTodosEventos();
        
        $model = new Cupom($objApiCliente);
        $datasEventos = $agenda->listaDatasEventos($matriz == true ? '' : 'MATRIZ');
        $model->buscarCupom(isset($_REQUEST['Cupom']['id_agenda']) ? ['id' => '%_' . $_REQUEST['Cupom']['id_agenda'] . '_%'] : []);
        return $this->render('index', [
            'model' => $model,
            'eventos' => $datasEventos,
            'id_agenda' => isset($_REQUEST['Cupom']['id_agenda']) ? $_REQUEST['Cupom']['id_agenda'] : '',
            'matriz' => $matriz
        ]);
    }
    
    /**
     * Criar cupom
     * @param boolean $matriz Boolean indicando se é uma ação para matriz ou não
     */
    public function actionNovoCupom ($matriz=false)
    { 
        $session = $this->_sessionOpen();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
        
        $model = new Cupom($objApiCliente);
        
        $agenda = new Agenda($objApiCliente);
        (boolean)($matriz) ? $agenda->buscarEventosPorFiltro(['nome' => 'MATRIZ%']) : $agenda->listaTodosEventos();
        $datasEventos = $agenda->listaDatasEventos($matriz == true ? '' : 'MATRIZ');
        
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $post['Cupom']['id'] = $model->getCupomId($post['Cupom']['tipo'], $post['Cupom']['id_agenda']);
            if ($model->load($post) && $model->save()) {
                return $this->redirect(['cupom/index', 'Cupom' => ['id_agenda' => $post['Cupom']['id_agenda']]]);
            }
            $model->addError('lista', $model->errors);
        }
        return $this->render('novo-cupom', [
            'model' => $model,
            'eventos' => $datasEventos,
            'matriz' => $matriz
        ]);
    }     
    
    /**
     * Formulário para editar cupom
     * @param string $cupom Id do cupom
     */
    public function actionEditarCupom ($cupom)
    {
        $session = $this->_sessionOpen();
        $objApiCliente=new Cliente;
        $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
        $model = new Cupom($objApiCliente);        
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            if ($model->load($post) && $model->update()) {
                return $this->redirect(['cupom/index', 'Cupom' => ['id_agenda' => $post['Cupom']['id_agenda']]]);
            }
            return $this->addError($model->lista, $model->errors);
        }
        $model->buscarCupom(['id' => $cupom]);
        return $this->render('editar-cupom', [
            'model' => $model->lista[0]
        ]);
    }    
    
    /**
     * Remover cupom
     */
    public function actionRemoverCupom ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
            $session = $this->_sessionOpen();
            $objApiCliente=new Cliente;
            $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
            $cupom = new Cupom($objApiCliente);
            $cupom->scenario = 'delete';
            if ($cupom->load(Yii::$app->request->post())) {
                return $cupom->delete();
            }
            return (object)['successo' => '0', 'erro' => ['mensagem' => 'Não foi possível carregar o cupom.']];
        }
        return (object)['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros para remover o cupom.']];
    }    

    /**
     * Formulário para movimentar estoque do cupom e consulta ao histórico de movimentação
     * @param string $cupom Id do cupom
     */
    public function actionEstoqueCupom ($cupom)
    {
        $session = $this->_sessionOpen();
        $objApiCliente=new Cliente;
        $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
        $model = new Cupom($objApiCliente);
        $estoque = $model->buscarHistoricoEstoque(['id' => $cupom]);
        return $this->render('estoque-cupom', [
            'model' => $model,
            'estoque' => $estoque
        ]);
    }
    
    /**
     * Movimentar estoque do cupom
     * @return type
     */
    public function actionMovimentoEstoque() 
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->post()) {
            $session = $this->_sessionOpen();
            $objApiCliente=new Cliente;
            $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
            $model = new Cupom($objApiCliente);
            if ($model->load(Yii::$app->request->post())) {
                return $model->movimentarEstoque();
            }
            return (object)['successo' => '0', 'erro' => ['mensagem' => 'Não foi possível movimentar o estoque do cupom.']];
        }
        return (object)['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros para movimentar o estoque do cupom.']];
    }
    
    /**
     * Clonar cupons de evento de origem para evento destino
     * @param string $idOrigem Id do evento origem - padrão id_evento + '_' + id_horário
     * @param string $idDestino Id do evento destino - padrão id_evento + '_' + id_horário
     */
    public function actionClonarCupom ($idOrigem, $idDestino) 
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        if (!empty($idOrigem) && !empty($idDestino)) {
            $session = $this->_sessionOpen();
            $objApiCliente=new Cliente;
            $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
            $api = new Cupom($objApiCliente);
            return $api->clonarCupom($idOrigem, $idDestino);
        }
        return (object) ['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros clonar cupons.']];
    }
    
    /**
     * load model - trazer um registro em model a partir de um parâmetro
     * @param int $cupom Código do cupom a ser buscado
     */
    private function loadModel($cupom)
    {
        $session = $this->_sessionOpen();
        $objApiCliente=new Cliente;
        $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
        $model = new Cupom($objApiCliente);
        $model->buscarCupom(['id' => $cupom]);
        return $model->lista;
    }


    /**
     * Método auxiliar para criação de cupons
     */
    public function actionCriarCupons($qtde=null,$idEvento=null,$idData=null){
        //ini_set('max_execution_time',0);
        if(!isset($qtde,$idEvento,$idData))
            return $this->redirect(Yii::$app->request->referrer);
        // echo 'iniciar<br>';
        $objApiCliente=new Cliente;
        $objApiCliente->buscarUnidadePor('user_token',$objApiCliente->getTokenMatriz());
        $index=0;
        while ($index < $qtde) {
            // echo 'início while<br>';
            $arrTime=str_split(time());
            shuffle($arrTime);
            $timeShuffle=join($arrTime);
            $timeShuffleHex=base_convert($timeShuffle, 10, 16);
            $idCupom=strtoupper($timeShuffleHex);
            $cupom = new Cupom($objApiCliente);
            $cupom->id='PRIV_'.$idEvento.'_'.$idData.'_'.$idCupom;
            $cupom->id_agenda=$idData;
            $cupom->nome='Gratuidade';
            $cupom->descricao='Favor confirmar seu nome e e-mail na entrada do evento';
            $cupom->desconto_percentual=100.00;
            $cupom->cumulativo=0;
            $cupom->ativo=1;
            $cupom->tipo=1;
            $cupom->estoque=1;

            if($cupom->save()){
                // echo 'cupom ok<br>';
                $estoque=$cupom->movimentarEstoque();
                if(!$estoque->successo)
                    \app\widgets\Util::p($estoque->erro);
                \app\widgets\Util::p($cupom->id);
                sleep(1);
            }
            if(!$cupom->validate())
                \app\widgets\Util::p($cupom->getErrors());//exit('Interrompeu no índice '.$index);

            $index++;
        }
        // echo 'fim<br>';
    }

    /**
     * Método auxiliar para listar ids na tela dos cupons
     */
    public function actionListarIdCupons($idEvento=null,$idData=null){
        ini_set('max_execution_time',0);
        if(!isset($idEvento,$idData))
            return $this->redirect(Yii::$app->request->referrer);
        // echo 'iniciar<br>';
        $objApiCliente=new Cliente;
        $objApiCliente->buscarUnidadePor('user_token',$objApiCliente->getTokenMatriz());
        $cupom = new Cupom($objApiCliente);
        $cupom->buscarCupom(['id'=>'PRIV_'.$idEvento.'_'.$idData.'_%']);
        $listaPromocodes='';
        foreach ($cupom->lista as $key => $value) {
            $listaPromocodes.=substr($value->id,strrpos($value->id, '_')+1)."\r\n";
        }

        file_put_contents('C:\Users\Administrador\Downloads\cupons.txt', $listaPromocodes);
    }
}
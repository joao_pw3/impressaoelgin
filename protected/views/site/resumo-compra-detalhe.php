<?php
use yii\helpers\Url;
?>
<div class="resumo-compra-detalhe boxDados" id="resumo-compra-detalhe">

    <input type="hidden" id="url-index" value="<?= Url::to(['site/index']); ?>">

    <?php if (empty($detalheVenda)) { ?>

    Não há informações sobre a compra informada.

    <?php } else { ?>

        <?= $detalheVenda; ?>
        <hr>
        <p></p>

        <?php if (!stristr($_SERVER['QUERY_STRING'], 'resumo-compra')) { ?>
        <div class="row alert alert-success">
            Enviamos uma mensagem para o e-mail de seu cadastro com um link de acesso a este resumo.<br>
        </div>
        <?php } ?>

    </div>

    <?php } ?>

    <?php if (!stristr($_SERVER['QUERY_STRING'], 'resumo-compra')) { ?>
    <div class="row btn-resumo">
        <hr>			
        <span class="row contato">
            <input type="submit" value="Efetuar nova compra" id="nova-compra" class="icon-next carrinho">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="submit" value="Home Mineirão" id="home-mineirao" class="icon-next carrinho">
        </span>
    </div>        
    <?php } ?>

</div>
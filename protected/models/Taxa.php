<?php
namespace app\models;

use Yii;
use yii\base\Model;
use \app\models\Cliente;
use \app\models\Carrinho;
use \app\models\WsCarrinho;
use app\models\WsCupom;

class Taxa extends Model{
	
	public $id;
    public $unidade;
    public $nome;
    public $descricao;
    public $desconto_fixo;
    public $desconto_percentual;
    public $desconto_maximo;
    public $desconto_minimo;
    public $validade_inicial;
    public $validade_final;
    public $cumulativo;
    public $ativo;
    public $estoque;
    public $lista;
    public $tipo;
    public $id_agenda;

	/**
     * Método construtor sempre pede a unidade atual. Deixa a taxa de conveniência persistir na sessão até que seja trocado o token do vendedor
     */
    public function __construct(Cliente $unidade){
        $this->unidade = $unidade;
        $session = Yii::$app->session;
        if (!$session->isActive)
            $session->open();
        $taxa_conveniencia=$session->get('taxa_conveniencia',false);
        if(!$taxa_conveniencia || $taxa_conveniencia['vendedor']!=$this->unidade->documento){
        	$ws=new WsCupom($this->unidade->user_token);
        	$consulta=$ws->consultarCupom(['id'=>'taxa-de-conveniencia']);
	        if(!isset($consulta->successo)){
	        	$this->addError('id','Falha ao buscar a taxa de conveniência');
	        	return;
	        }
	        if($consulta->successo)
	        	$session->set('taxa_conveniencia',['vendedor'=>$this->unidade->documento,'objeto'=>$consulta->objeto[0]]);
	        if($consulta->successo == '0' || !isset($consulta->objeto) || !count($consulta->objeto)>0){
	        	$this->addError('id',$consulta->erro->mensagem);
	        	return;
	        }
        }
        $this->loadFromApi($session->get('taxa_conveniencia')['objeto']);
    }

	/**
	 * Retorna o valor da taxa aplicada ao valor calculando os limites
	 * @param float $valor o valor base para cálculo 
	 */
	public function exibirTaxa($valor){
		return (float)$this->calculoTaxaLimites($valor*$this->desconto_percentual/100);
	}

	/**
	 * Retorna o valor com a taxa aplicada calculando os limites
	 * @param float $valor o valor base para cálculo 
	 */
	public function exibirValorComTaxa($valor){
		return (float)$valor+$this->calculoTaxaLimites($valor*$this->desconto_percentual/100);
	}

	/**
	 * Retorna limite máximo ou mínimo de taxa se estes forem ultrapassados. Caso contrário retorn o valor da taxa
	 * @param float $valorTaxa o valor da taxa aplicada para cálculo 
	 */
	private function calculoTaxaLimites($valorTaxa){
		if($valorTaxa > $this->desconto_maximo) return $this->desconto_maximo;
		if($valorTaxa < $this->desconto_minimo) return $this->desconto_minimo;
		return $valorTaxa;
	}
    
    /**
     * Atribuir valores retornado pela API às propriedades
     */
    public function loadFromApi($objeto) {
        if (!isset($objeto->id)) {
            return false;
        }
        $this->id = $objeto->id;
        $this->nome = $objeto->nome;
        $this->descricao = $objeto->descricao;
        $this->desconto_fixo = $objeto->desconto->fixo;
        $this->desconto_percentual = $objeto->desconto->percentual;
        $this->desconto_maximo = $objeto->desconto->maximo;
        $this->desconto_minimo = $objeto->desconto->minimo;
        $this->validade_inicial = $objeto->validade->inicial;
        $this->validade_final = $objeto->validade->final;
        $this->cumulativo = (int)$objeto->cumulativo;
        $this->ativo = (int)$objeto->ativo;
        $this->estoque = $objeto->estoque;
        return true;
    }
}
<?php
use yii\helpers\Html;
use app\assets\AdminAsset;
use yii\bootstrap\Modal;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\models\Cliente;

AdminAsset::register($this);

$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <?= Html::csrfMetaTags() ?>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width">
        <meta name="viewport" content="initial-scale=1.0">
        <link href="http://estadiomineirao.com.br/wp-content/themes/Mineirao/favicon.ico" rel="shortcut icon">
        <link href="http://estadiomineirao.com.br/wp-content/themes/Mineirao/favicon.ico" rel="icon">
        <title>ADM Vendas | Estádio Mineirão</title>
        <?php $this->head() ?>
    </head>

    <?php $this->beginBody() ?>

    <body>
        <?php
        if (YII_ENV == 'dev')
            echo '<div id="identHomolog" class="text-center">Você está em ambiente de homologação</div>';
        ?>
        <section class="header show-for-large">
            <?php include "header.php"; ?>
        </section>

		<div class="admNav">
            <?php
            if (!Yii::$app->user->isGuest && !Yii::$app->user->identity->comprador) {
                $permissoes=$this->context->getPermissoes();
                NavBar::begin();
                $nome = isset(Yii::$app->user->identity) ? Yii::$app->user->identity->nome_cliente : '';
                
                $arrMenu = [];

                $session=Yii::$app->session;
                $permissoes=$session['permissoes_usuario'];

                $master=Cliente::permissaoPara('*|*',$permissoes);
                if($master) {
                    array_push($arrMenu, ['label' => 'Matrizes', 'items' => [
                        ['label' => '1. Cadastrar evento matriz', 'url' => ['/admin/agenda/novo-evento-simples', 'matriz' => true]],
                        ['label' => '1.1. Consultar eventos', 'url' => ['/admin/agenda/', 'matriz' => true]],
                        ['label' => '2. Cadastrar grid (assento marcado)', 'url'=>['/admin/produto/assentos', 'matriz' => true]],
                        ['label' => '2.1. Configurar mapa (assento marcado)', 'url'=>['/admin/produto/editar-assentos', 'matriz' => true]],
                        ['label' => '3. Cadastrar produto (assento livre)', 'url'=>['/admin/produto/novo-produto', 'matriz' => true]],
                        ['label' => '3.1. Consultar produtos', 'url'=>['/admin/produto/', 'matriz' => true]],
                        ['label' => '4. Cadastrar cupom', 'url'=>['/admin/cupom/novo-cupom', 'matriz' => true]],
                        ['label' => '4.1. Consultar cupom', 'url'=>['/admin/cupom/', 'matriz' => true]],
                        ['label' => '5. Editar permissões', 'url'=>['/admin/permissoes/editar']],
                        ['label' => '5.1 Listar permissões', 'url'=>['/admin/permissoes']]
                    ]]);
                }

                if($master || Cliente::grupoPermissao('agenda',$permissoes)){
                     array_push($arrMenu, ['label' => 'Agenda', 'items' => [
                        ['label'=>'Adicionar Evento', 'url'=>['/admin/agenda/novo-evento'], 'visible'=>($master || Cliente::permissaoPara('agenda|novo-evento',$permissoes))],
                        ['label'=>'Consultar eventos', 'url'=>['/admin/agenda'], 'visible'=>($master || Cliente::permissaoPara('agenda|index',$permissoes))]
                     ]]);
                }

                if($master || Cliente::grupoPermissao('produto',$permissoes)){
                     array_push($arrMenu, ['label' => 'Produtos', 'items' => [
                        ['label'=>'Editar assentos', 'url'=>['/admin/produto/editar-assentos'], 'visible'=>($master || Cliente::permissaoPara('produto/editar-assentos',$permissoes))],
                        ['label'=>'Consultar', 'url'=>['/admin/produto'], 'visible'=>($master || Cliente::permissaoPara('produto|index',$permissoes))],
                        ['label' => 'Cadastrar cupom', 'url'=>['/admin/cupom/novo-cupom'], 'visible'=>($master || Cliente::permissaoPara('produto|index',$permissoes))],
                        ['label' => 'Consultar cupom', 'url'=>['/admin/cupom/'], 'visible'=>($master || Cliente::permissaoPara('produto|index',$permissoes))],
                     ]]);
                }

                if (in_array('financeiro/index', $session['permissoes_usuario']) || in_array('ingressos/ocupantes', $session['permissoes_usuario']) || in_array('vendas/index', $session['permissoes_usuario'])) {
                    array_push($arrMenu, ['label' => 'Relatórios (legado)', 'items' => [
                        ['label' => 'Financeiro', 'url' => ['financeiro/index'], 'visible' => in_array('financeiro/index', $session['permissoes_usuario'])],
                        ['label' => 'Ocupantes', 'url' => ['ingressos/ocupantes'], 'visible' => in_array('ingressos/ocupantes', $session['permissoes_usuario'])],
                        ['label' => 'Vendas', 'url' => ['vendas/index'], 'visible' => in_array('vendas/index', $session['permissoes_usuario'])]
                    ]]);

                    array_push($arrMenu, ['label' => 'Relatórios', 'items' => [
                            ['label' => 'Vendas', 'url' => ['/admin/relatorio/'], 'visible' => in_array('vendas/index', $session['permissoes_usuario'])],
                            ['label' => 'Controle de Caixa', 'url' => ['/admin/relatorio/controle-caixa'], 'visible' => in_array('relatorio/controle-caixa', $session['permissoes_usuario'])],
                            ['label' => 'Vendas e Cancelamentos Analítico', 'url' => ['/admin/relatorio/vendas-cancelamentos-analitico'], 'visible' => in_array('relatorio/vendas-cancelamentos-analitico', $session['permissoes_usuario'])],
                            ['label' => 'Vendas e Cancelamentos Sintético', 'url' => ['/admin/relatorio/vendas-cancelamentos-sintetico'], 'visible' => in_array('relatorio/vendas-cancelamentos-sintetico', $session['permissoes_usuario'])],
                            ['label' => 'Repasses', 'url' => ['/admin/relatorio/repasses'], 'visible' => in_array('relatorio/repasses', $session['permissoes_usuario'])],
                            ['label' => 'Detalhamento de Eventos', 'url' => ['/admin/relatorio/detalhamento-eventos'], 'visible' => in_array('detalhamento-eventos', $session['permissoes_usuario'])],
                            ['label' => 'Vendas por Cliente', 'url' => ['/admin/relatorio/vendas-cliente'], 'visible' => in_array('relatorio/vendas-cliente', $session['permissoes_usuario'])]
                    ]]);
                }
                if (in_array('bilheteria/index', $session['permissoes_usuario']) || in_array('bilheteria/index', $session['permissoes_usuario'])) {
                    array_push($arrMenu, ['label' => 'Bilheteria', 'items' => [
                        ['label' => 'Ativar ingresso - Temporada', 'url' => ['bilheteria/ativar-ingresso'], 'visible' => in_array('bilheteria/index', $session['permissoes_usuario'])],
                        ['label' => 'Ativar ingresso - Jogos avulsos', 'url' => ['bilheteria/consultar-ingresso'], 'visible' => in_array('bilheteria/index', $session['permissoes_usuario'])],
                        ['label' => 'Venda avulsa', 'url' => ['bilheteria/venda-avulsa'], 'visible' => in_array('bilheteria/index', $session['permissoes_usuario'])],
                    ]]);
                }
                if ($master || in_array('grafica/reservar', $session['permissoes_usuario']) || in_array('ingressos/lote-entrega', $session['permissoes_usuario']) || in_array('ingressos/associar-ingressos', $session['permissoes_usuario']) ||
                        in_array('ingressos/lote-liberar', $session['permissoes_usuario']) || in_array('ingressos/acompanhar', $session['permissoes_usuario']) || in_array('ingressos/consultar', $session['permissoes_usuario']) ||
                        in_array('ingressos/invalidos', $session['permissoes_usuario'])  || in_array('venda/ingressos', $session['permissoes_usuario']) ) {
                    array_push($arrMenu, ['label' => 'Ingressos', 'items' => [
                        ['label' => 'Reservar lote', 'url' => ['grafica/reservar'], 'visible' => in_array('grafica/reservar', $session['permissoes_usuario'])],
                        ['label' => 'Lote "entregue ao Estádio"', 'url' => ['ingressos/lote-entrega'], 'visible' => in_array('ingressos/lote-entrega', $session['permissoes_usuario'])],
                        ['label' => 'Associar ingresso a mifare', 'url' => ['ingressos/associar-ingressos'], 'visible' => in_array('ingressos/associar-ingressos', $session['permissoes_usuario'])],
                        ['label' => 'Lote "liberado para bilheteria"', 'url' => ['ingressos/lote-liberar'], 'visible' => in_array('ingressos/lote-liberar', $session['permissoes_usuario'])],
                        '<li class="divider"></li>',
                        ['label' => 'Acompanhar lotes', 'url' => ['ingressos/acompanhar'], 'visible' => in_array('ingressos/acompanhar', $session['permissoes_usuario'])],
                        ['label' => 'Consultar ingressos', 'url' => ['ingressos/consultar'], 'visible' => in_array('ingressos/consultar', $session['permissoes_usuario'])],
                        ['label' => 'Ingressos inválidos', 'url' => ['ingressos/invalidos'], 'visible' => in_array('ingressos/invalidos', $session['permissoes_usuario'])],
                        '<li class="divider"></li>',
                        ['label' => 'Venda de Ingressos', 'url' => ['venda/ingressos'], 'visible' => $master || in_array('venda/ingressos', $session['permissoes_usuario'])]
                    ]]);
                }
                if (in_array('vendas/reserva', $session['permissoes_usuario'])) {
                    array_push($arrMenu, ['label' => 'Assentos', 'items' => [
                        ['label' => 'Reserva/bloqueio', 'url' => ['vendas/reserva'], 'visible' => in_array('vendas/reserva', $session['permissoes_usuario'])]
                    ]]);
                }
                if (in_array('financeiro/boleto', $session['permissoes_usuario']) || in_array('caixa/operacao', $session['permissoes_usuario'])) {
                    array_push($arrMenu, ['label' => 'Financeiro', 'items' => [
                        ['label' => 'Boleto', 'url' => ['financeiro/boleto'], 'visible' => in_array('financeiro/boleto', $session['permissoes_usuario'])],
                        ['label' => 'Caixa', 'url' => ['caixa/operacao'], 'visible' => in_array('caixa/operacao', $session['permissoes_usuario'])],
                    ]]);
                }
                if (in_array('grafica/lote-producao', $session['permissoes_usuario']) || in_array('grafica/lote-estadio', $session['permissoes_usuario'])) {
                    array_push($arrMenu, ['label' => 'Gráfica', 'items' => [ 
                        ['label' => 'Lote "em Produção"', 'url' => ['grafica/lote-producao'], 'visible' => in_array('grafica/lote-producao', $session['permissoes_usuario'])],
                        ['label' => 'Lote "enviado ao Estádio"', 'url' => ['grafica/lote-estadio'], 'visible' => in_array('grafica/lote-estadio', $session['permissoes_usuario'])],
                    ]]);
                }
                if (in_array('usuarios/cadastro', $session['permissoes_usuario']) || in_array('usuarios/lista-operadores', $session['permissoes_usuario']) || 
                        in_array('usuarios/permissoes', $session['permissoes_usuario']) || in_array('usuarios/listar-permissoes', $session['permissoes_usuario'])) {
                    array_push($arrMenu, ['label' => 'Acessos', 'items' => [
                        ['label' => 'Adicionar operador', 'url' => ['usuarios/cadastro'], 'visible' => in_array('usuarios/cadastro', $session['permissoes_usuario'])],
                        ['label' => 'Gerenciar operadores', 'url' => ['usuarios/lista-operadores'], 'visible' => in_array('usuarios/lista-operadores', $session['permissoes_usuario'])],
                        '<li class="divider"></li>',
                        ['label' => 'Adicionar permissão', 'url' => ['usuarios/permissoes'], 'visible' => in_array('usuarios/permissoes', $session['permissoes_usuario'])],
                        ['label' => 'Gerenciar permissão', 'url' => ['usuarios/listar-permissoes'], 'visible' => in_array('usuarios/listar-permissoes', $session['permissoes_usuario'])],
                    ]]);
                }
                if (in_array('sistema/manutencao', $session['permissoes_usuario'])) {
                    array_push($arrMenu, ['label' => 'Sistema', 'items' => [
                        ['label' => 'Manutenção', 'url' => ['sistema/manutencao'], 'visible' => in_array('sistema/manutencao', $session['permissoes_usuario'])],
                    ]]);
                }
                if (in_array('museu/registra-vendas', $session['permissoes_usuario'])) {
                    array_push($arrMenu, ['label' => 'Museu', 'items' => [
                        ['label' => 'Registra Vendas', 'url' => ['museu/registra-vendas'], 'visible' => in_array('museu/registra-vendas', $session['permissoes_usuario'])],
                         ['label' => 'Consulta Vendas', 'url' => ['museu/vendas'], 'visible' => in_array('museu/vendas', $session['permissoes_usuario'])],
                    ]]);
                }
                
                if (in_array('/admin/caixa', $session['permissoes_usuario']) || in_array('/admin/caixa/relatorio-vendas', $session['permissoes_usuario'])) {
                    array_push($arrMenu, ['label' => 'Caixa', 'items' => [
                        ['label' => 'Abertura/fechamento/sangria', 'url' => ['/admin/caixa'], 'visible' => in_array('/admin/caixa', $session['permissoes_usuario'])],
                        ['label' => 'Relatório de vendas', 'url' => ['/admin/caixa/relatorio-vendas'], 'visible' => in_array('/admin/caixa/relatorio-vendas', $session['permissoes_usuario'])],
                    ]]);
                }
                
                /*
                if ($master || in_array('/caixa', $session['permissoes_usuario'])) {
                    array_push($arrMenu, ['label' => 'Caixa', 'items' => [
                        ['label' => 'Abertura/fechamento/sangria', 'url' => ['index'], 'visible' => $master || in_array('caixa', $session['permissoes_usuario'])],
                        ['label' => 'Relatório de Vendas', 'url' => ['relatorio-vendas'], 'visible' => $master || in_array('relatorio-vendas', $session['permissoes_usuario'])]                        
                    ]]);
                } 
                */               
                array_push($arrMenu, ['label' => 'Logout', 'url' => ['/admin/default/logout']]);
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right'],
                    'items' => $arrMenu
                ]);
                NavBar::end();
            }
            ?>
        </div>

        <?= $content ?>

        <footer style="position:fixed; z-index:100; padding:20px 0px; bottom:0px; width:100%;">
            <div style="text-align:center; color:#fff; margin:0px; padding:0px;">EasyforPay System - © Copyright 2017-<span id="getYear"></span></div>
            <script>
                    var d = new Date();
                    var n = d.getFullYear();
                    document.getElementById("getYear").innerHTML = n;				
            </script>
        </footer>
    <?php $this->endBody() ?>    

    </body>
</html>
<?php $this->endPage() ?>

<!-- Modal - Mensagens -->
<?php Modal::begin([
    'header' => '<h4 align="center">Atenção</h4>',
    'size'   => 'modal-sm',
    'id'     => 'modal-mensagem',
    'options'=> [
        'class' => 'modal-center'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ]
]);
Modal::end(); ?>

<!-- Modal - Imagens -->
<?php Modal::begin([
    'size'   => 'modal-lg',
    'id'     => 'modal-imagem',
    'options'=> [
        'class' => 'modal-center fade'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ]
]);
Modal::end(); ?>
    
<?php Modal::begin([
    'header' => '<h4 align="center">Atenção</h4>',
    'size' => 'modal-sm',
    'id' => 'modal-confirmar',
    'options' => [
        'class' => 'modal-center'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ],
    'footer' => '
       <div class="col-md-12" align="center">
           <button type="button" class="btn btn-danger" id="btn-cancel">Cancelar</button>
           <button type="button" class="btn btn-success" id="btn-confirmar">Confirmar</button>
       </div>'
]); ?>
<?php Modal::end(); ?>
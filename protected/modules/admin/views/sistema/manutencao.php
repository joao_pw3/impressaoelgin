<?php
use yii\widgets\ActiveForm;

$this->title = 'ADM Estádio Mineirão :: Manutenção';
?>

<div class="container" id="caixa">
    <h2 class="tituloGeral"><i class="fa fa-clone"></i> Manutenção</h2>   
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">

                <div class="row">
                    
                    <div class="col-md-12">

                        <div class="alert alert-info>">
                            <ul>
                                <li>Para o site entrar em manutenção digite um texto no campo abaixo e clique em "Site entre em manutenção"</li>
                                <li>Para retornar o site, <strong>não</strong> preencha nada no campo abaixo e clique em "Site saia da manutenção"</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-12">
                    <?php $key = array_keys($message); ?>
                        <div class="alert alert-<?php echo $key[0];?>"><?php echo $message[$key[0]]; ?></div>
                    </div>
                </div>
                
                
                <?php $form = ActiveForm::begin(['id' => 'form-manutencao']); ?>

                <div class="row">
                    <div class="col-md-12">

                        <?= $form->field($manutencao, 'texto')->textArea([
                            'class' => 'form-control',
                            'rows' => 10
                        ]); ?>  
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-primary"><?php echo $info['btn']; ?></button>
                    </div>
                </div>                

                <?php ActiveForm::end();  ?>
            </div>            
        </div>
    </div>
</div>
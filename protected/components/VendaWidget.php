<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Url;
use app\models\ApiCarrinho;
use app\models\ApiAgenda;
use app\models\Mineiraocard;
use app\models\Ingressos;
use app\models\Compras;
use app\models\Carrinho;
use app\models\Conta;
use app\models\Tags;

class VendaWidget extends Widget 
{
   /**
     * Mostrar dados de uma venda
     * @param int $idVenda o id da venda para consulta na API
     * @param string $_scenario distinção se este método está sendo usado para consulta (bliheteria) ou aquisição de ingresso (cliente)
     */
    public static function getVenda($idVenda='',$_scenario='') 
    {        
        $venda = '';
        if (!empty($idVenda) && (int)$idVenda > 0) {
            $compra = (new ApiCarrinho)->consultarVendasFiltro(['codigo' => $idVenda]);
            if ($compra->successo == '1') {
                $carrinho = (new ApiCarrinho)->consultarCarrinho($compra->objeto[0]->id_carrinho);
                if ($carrinho->successo == '1') {
                    $venda .= VendaWidget::getDataVenda($compra->objeto[0],$idVenda);
                    $venda .= VendaWidget::getDataDetalhe($compra->objeto[0], $carrinho, $_scenario);
                } 
            }
        }
        return $venda;
    }
 
    /**
     * Buscar as vendas a partir de um documento.
     * Também localiza o documento na tabela mineiraocard para compras onde o vistante não for o comprador mas está listado como ocupante do ingresso
     * @param string $documento o documento apresentado pelo cliente a ser pesquisado
     * @param string $_scenario distinção se este método está sendo usado para consulta (bliheteria) ou aquisição de ingresso (cliente)
     */
    public static function getVendasDocumento($documento='',$_scenario='') 
    {
        $venda = '';
        if (!empty($documento)) {
            $compras = (new ApiCarrinho)->consultarVendasFiltro(['comprador' => $documento]);
            if ($compras->successo == '1') {
                foreach($compras->objeto as $compra){
                    $venda .= self::getVenda($compra->vendas->codigo,$_scenario);
                } 
            }

            $registroMineiraoCard = Mineiraocard::find()->where(['cpf' => $documento])->all();
            if($registroMineiraoCard!=null){
                foreach ($registroMineiraoCard as $mineiraocard) {
                    $venda .= self::getVenda($mineiraocard->id_compra,$_scenario);
                }
            }            
        }
        return $venda;
    }
 
    /**
     * Dados gerais da compra
     */
    public static function getDataVenda($objCompra,$hash_compra) {
        return '
        <div class="row tableResumo">
            <div class="col-md-5">
                <div class="col-md-3"><strong>Comprador: </strong></div>
                <div class="col-md-9">' . $objCompra->vendas->comprador->nome . '</div>
                <div class="col-md-3"><strong>Documento: </strong></div>
                <div class="col-md-9">' . $objCompra->vendas->comprador->documento . '</div>
                <div class="col-md-3"><strong>Identificador: </strong></div>
                <div class="col-md-9"><strong>' . $objCompra->vendas->codigo . '</strong></div>
            </div>
            <div class="col-md-5">
                <div class="col-md-3"><strong>Data: </strong></div>
                <div class="col-md-9">' . $objCompra->vendas->data . '</div>
                <div class="col-md-3"><strong>Status: </strong></div>
                <div class="col-md-9' . ($objCompra->vendas->status == 'Autorizado' ? ' plus-green' : ' plus-red') . '">
                    ' . $objCompra->vendas->status . '</div>
                <div class="col-md-3"><strong>Valor total: </strong></div>
                <div class="col-md-9">R$ ' . number_format($objCompra->vendas->valor, 2, ',', '.') . '</div>
            </div>
            <div class="col-md-2">
                <a class="btn btn-default btn-lg" href="'.Url::to(['site/resumo-compra','hash_compra'=>$hash_compra,'opc'=>'print']).'" target="_blank">
                    <span class="glyphicon glyphicon-print"></span>
                </a>
            </div>
        </div>
        <hr>';
    }
    
    /**
     * Detalhes da compra - lugares
     */
    public static function getDataDetalhe($objCompra, $carrinho, $_scenario='') {
        $mapa='';
        $detalhe = ' 
        <div class="row tableResumo">
            <div class="col-md-12">
                <strong>Produtos:</strong>                  
            </div>';
        
        if (isset($carrinho->objeto->produtos) && !empty($carrinho->objeto->produtos)) { 
            
            foreach ($carrinho->objeto->produtos as $index => $produto) {
                $unidades=1;
                $agenda = new ApiAgenda();
                $mineiraocardNome='';
                $mineiraocardDoc='';
                $mineiraocardTel='';
                $mineiraocardEmail='';
                $mineiraocardValido=false;
                if(isset($carrinho->objeto->tags)){
                    foreach($carrinho->objeto->tags as $tag){
                        $nome=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_nome');
                        if($nome!=false)
                            $mineiraocardNome=$nome;
                        $doc=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_documento');
                        if($doc!=false)
                            $mineiraocardDoc=$doc;
                        $tel=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_telefone');
                        if($tel!=false)
                            $mineiraocardTel=$tel;
                        $email=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_email');
                        if($email!=false)
                            $mineiraocardEmail=$email;
                    }
                    $mineiraocardValido=$mineiraocardNome!=false&&$mineiraocardDoc!=false;
                }
                if(isset($produto->tags)){
                    foreach ($produto->tags as $index_tag => $tag) {
                        if ($tag->nome == 'matriz') {
                            $evento = $agenda->buscarEvento(json_encode(['tags' => [['nome' => 'apelido', 'valor' => $tag->valor]]]));
                        }
                    }
                }
                if(!isset($produto->tags) && isset($produto->agenda->codigo))
                    $evento = $agenda->buscarEvento(null,$produto->agenda->codigo);

                $nomeEvento='';
                $banner='';
                if ($evento->successo == 1) {
                    $nomeEvento=$evento->objeto[0]->nome;
                    if (isset($evento->objeto[0]->anexos)) {
                        foreach ($evento->objeto[0]->anexos as $index_anexo => $anexo) {
                            if ($anexo->nome == 'bannervitrine' || $anexo->nome == 'imgEvento') {
                                $banner = $anexo->url;
                            }
                        }
                    }
                    if (isset($evento->objeto[0]->tags)) {
                        foreach ($evento->objeto[0]->tags as $index_tags => $tags) {
                            if ($tags->nome == 'assento') {
                                $mapa = $tags->valor;
                            }
                        }
                    }
                }
                while ($unidades <= $produto->unidades) {
                    $ingressoNome = isset($produto->cupom->nome) ? $produto->cupom->nome : $produto->nome;
                    $ingressoCupom = isset($produto->cupom->id) ? $produto->cupom->id : ''; 
                    $detalhe .= '
                    <div class="col-md-9">
                        <div class="tracejado">
                            <div class="col-md-4" style="padding-left:0px;">
                                <strong>'.$nomeEvento.'</strong><br />
                                <img alt="'.$nomeEvento.'" src="' . $banner . '">
                            </div>
                            <div class="col-md-4">
                            ' . $ingressoNome . '<br>';
                                if (isset($produto->cupom->desconto->total) && $produto->cupom->desconto->total>0) {
                                    $detalhe .= '
                                    <span class="strikeDesconto">R$ ' . number_format($produto->valor, 2, ',', '.') . '</span><br>';
                                }
                                $detalhe .= ' R$ ' . number_format($produto->valor_liquido, 2, ',', '.');
                                $detalhe .= '
                            </div>';
                        if($mapa==''){
                            $detalhe .= '<div class="col-md-4">
                                Este é o seu voucher Mineirão. Enquanto o seu cartão personalizado para acesso a toda a temporada está em produção,  apresente este voucher na bilheteria e retire seu ingresso para a partida.
                            </div>';
                        }
                        if($mapa!=''){
                            $detalhe .= '<div class="col-md-4">
                                Este voucher é seu ingresso. Apresente-o para validar sua entrada.
                            </div>';
                        }
                        
                        $detalhe .= '
                            <div class="col-md-12">
                                <p><strong>Dados do ocupante</strong></p>
                            </div>
                            <div class="col-md-12">';
                        if ($_scenario == 'consulta') {
                            $detalhe .= '
                                <label>Nome</label>
                                <span ' . $mineiraocardNome . '</span>
                                <p class="mineiraocard-nome-info"></p>
                                <label>Documento</label>
                                <span>' . $mineiraocardDoc .'</span>
                                <p class="mineiraocard-cpf-info"></p>';
                        } else {
                            if($mapa == 'livre') {
                                $detalhe .= self::encontraOcupante($produto->codigo, $ingressoCupom ,$carrinho->objeto->tags, $unidades);
                            }
                            else {
                                $detalhe .= self::formMineiraocard($index, $produto, (object)['nome'=>$mineiraocardNome,'doc'=>$mineiraocardDoc,'email'=>$mineiraocardEmail,'tel'=>$mineiraocardTel], $objCompra);
                            }
                        }
                        
                        $detalhe .= '
                            </div>
                        </div>
                    </div>';
                    if($mapa != '') {
                        $detalhe .= self::qrCodeIngresso($produto->qrcodes[$unidades-1]);
                    } else {
                        $detalhe .= self::qrCodeVoucher($objCompra->vendas->codigo, $produto, (object)['nome'=>$mineiraocardNome,'doc'=>$mineiraocardDoc]);
                    }
                    $unidades++;
                }
            }
        }
        $detalhe .= '</div>';

        return $detalhe;
    }

    /**
     * Quando os dados não estiverem preenchidos, mostrar formulário para coletá-los
     * @param type $index
     * @param type $produto
     * @param type $registroMineiraoCard
     * @param type $objCompra
     * @return string
     */
    private static function formMineiraoCard($index, $produto, $registroMineiraoCard, $objCompra)
    {
        $mineiraocardNome = '';
        $mineiraocardDoc = '';
        $mineiraocardEmail = '';
        $mineiraocardTelefone = '';
        $mineiraocardId = '';

        if ($registroMineiraoCard != null) {
            if (isset($registroMineiraoCard->nome))
                $mineiraocardNome=$registroMineiraoCard->nome;
            if (isset($registroMineiraoCard->doc))
                $mineiraocardDoc=$registroMineiraoCard->doc;
            if (isset($registroMineiraoCard->email))
                $mineiraocardEmail=$registroMineiraoCard->email;
            if (isset($registroMineiraoCard->tel))
                $mineiraocardTelefone=$registroMineiraoCard->tel;
            if (isset($registroMineiraoCard->id))
                $mineiraocardId=$registroMineiraoCard->id;
        }
        
        if($mineiraocardNome==''||$mineiraocardDoc==''){
            $form ='
                <form class="row divsAuxDoc" method="post" id="formMineiraocard-' . $produto->codigo . '" action="' . Url::to(['site/info-mineirao-card']) . '">
                    <input type="hidden" id="mineiraocard-valido-<?= $produto->codigo ?>" class="Mineiraocard_valido" value="<?=(string)$mineiraocardValido?>">
                    <label>Nome</label>
                    <input type="text" name="Mineiraocard[nome]" style="border:1px solid #bbb;" maxlength="24" value="'.$mineiraocardNome.'"';
            if ($index == 0) { 
                $form .= '"data-toggle=\"popover\"';
            }
            $form .= 'title="" data-content="Por favor preencha os NOMES e os documentos das pessoas de todos dos ingressos comprados." data-placement="top" data-trigger="focus">
                    <p class="mineiraocard-info mineiraocard-info-nome"></p>';

            $form .= '
                    <label style="margin-top:10px;">Documento (apenas números)</label>
                    <input type="text" name="Mineiraocard[documento]" style="border:1px solid #bbb;" maxlength="11" value="'.$mineiraocardDoc.'">
                    <p class="mineiraocard-info mineiraocard-info-documento"></p>';

            $form .= '
                    <label style="margin-top:10px;">E-mail</label>
                    <input type="text" name="Mineiraocard[email]" style="border:1px solid #bbb;" value="'.$mineiraocardEmail.'">
                    <p class="mineiraocard-info mineiraocard-info-email"></p>';

            $form .= '
                    <label style="margin-top:10px;">Telefone</label>
                    <input type="text" name="Mineiraocard[telefone]" style="border:1px solid #bbb;" value="'.$mineiraocardTelefone.'">
                    <p class="mineiraocard-info mineiraocard-info-telefone"></p>';

            $form .= '
                    <input type="hidden" name="Mineiraocard[produto]" value="'.$produto->codigo.'">
                    <input type="hidden" name="Mineiraocard[carrinho]" value="'.$objCompra->id_carrinho.'">
                    <input type="submit" value="Salvar" class="icon-next carrinho mineiraocard-submit" id="mineiraocard-submit-'. $produto->codigo .'"><br />
                    <p class="mineiraocard-retorno"></p>
                </form>
                <br clear="all">';
            return $form;
        }else{
            return '<strong>Nome</strong> '.$mineiraocardNome.' | 
                <strong>Documento</strong> '.$mineiraocardDoc.' | 
                <strong>E-mail</strong> '.$mineiraocardEmail.' | 
                <strong>Telefone</strong> '.$mineiraocardTelefone;
        }
    }

    public static function encontraOcupante($codigo, $cupom, $ocupantes, $contador) 
    {   
        $campos = ['documento','nome','email','telefone'];   
        $ocupantes= (array) (new Tags)->objetoTags($ocupantes);
        $string = null;
        if($cupom != '') {
            $codigo = $codigo.'_'.$cupom;
        }
        foreach ($campos as $value) {
            $keyOcupante = isset($ocupantes[$codigo.'_'.$contador.'_'.$value]) ? $ocupantes[$codigo.'_'.$contador.'_'.$value] : false;

            if($keyOcupante!==false)
                $string.= '<strong>'.ucfirst($value).'</strong> : '. $keyOcupante.'<br/>';
        }
        return $string;
    }    
    
    /**
     * Construir QR Code do voucher
     * @param integer $idVenda Id da venda
     * @param object $produto Dados do produto 
     * @param string $usuario Documento do usuário que usará o produto
     * @param inteiro $contador Contador de unidades para o produto (Cadeira = 1 / Camarote = 1..n)
     */
    public static function qrCodeVoucher ($idVenda, $produto, $usuario, $layout='', $contador=1) 
    {
        if ($idVenda > 0 && $produto->codigo > 0 && !empty($usuario->doc)) {
            $customCode = base64_encode($idVenda . '|' . $produto->codigo . '|' . $usuario->doc . '|' . $contador);
            $qrCode = (new ApiCarrinho)->getQrCode($customCode);
            return $layout == 'print' ?'<img src="' . $qrCode->writeDataUri() . '"/>' :'
        <div class="col-md-3">
            <strong>QR Code do Voucher</strong><br>
            <img src="' . $qrCode->writeDataUri() . '"/>
        </div>';
        } 
        return $layout == 'print' ?' Para ver o QR Code do seu voucher, preencha os dados do ocupante e clique em salvar (preenchimento obrigatório). Este voucher <strong>NÃO é seu ingresso</strong>, mas será utilizado para que você o retire na bilheteria do estádio.' :' 
        <div class="col-md-3">
            <strong>QR Code do Voucher</strong><br>
            Para ver o QR Code do seu voucher, preencha os dados do ocupante e clique em salvar (preenchimento obrigatório). Este voucher <strong>NÃO é seu ingresso</strong>, mas será utilizado para que você o retire na bilheteria do estádio.
        </div>';
    }

    public static function qrCodeIngresso ($qrcode,$layout='') 
    {   
        if($qrcode->qrcode !='' && !empty($qrcode->qrcode)) { 
            $conta=new Conta;
            $compras=new Compras($conta);
            if($qrcode->utilizado !='' || !empty($qrcode->utilizado)) {
                $htmlQrcode = 'O Ingresso já foi utilizado em '.$qrcode->utilizado;
            } else {
                $imgQrCode = (new Ingressos($compras))->getQrCode($qrcode->qrcode)->writeDataUri();
                $htmlQrcode = '<img src="' . $imgQrCode . '"/>';
            }
            
            return $layout == 'print' ? $htmlQrcode :'
        <div class="col-md-3">
            <strong>QR Code do Voucher</strong><br>
                '.$htmlQrcode.'
        </div>';
        } 

        return $layout == 'print' ?' Para ver o QR Code do seu voucher, preencha os dados do ocupante e clique em salvar (preenchimento obrigatório). Este voucher <strong>NÃO é seu ingresso</strong>, mas será utilizado para que você o retire na bilheteria do estádio.' :' 
        <div class="col-md-3">
            <strong>QR Code do Voucher</strong><br>
            Para ver o QR Code do seu voucher, preencha os dados do ocupante e clique em salvar (preenchimento obrigatório). Este voucher <strong>NÃO é seu ingresso</strong>, mas será utilizado para que você o retire na bilheteria do estádio.
        </div>';
    }     
}
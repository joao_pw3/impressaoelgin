<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use app\models\EventosModel;
use app\models\CompraModel;
use app\models\ClienteModel;
use app\models\CartaoModel;
use app\models\ApiCliente;
use app\models\ApiProduto;
use app\models\ApiCartao;
use app\models\ApiCarrinho;
use app\models\ApiMensagem;
use app\components\VendaWidget;
use app\models\Mineiraocard;
use app\models\ApiAgenda;
use app\components\ReCaptcha;
use app\models\ApiKonduto;
use app\modules\admin\models\ApiIngresso;
use app\models\Cliente;
use app\models\Carrinho;
use app\models\Agenda;
use app\models\Tags;
use app\models\Cupom;
use app\models\Ingressos;
use app\models\Compras;
use app\models\Conta;
/**
 * Classe geral controller - define rotas e views para a aplicação. As variáveis $area, $setor e $assento são utilizadas em várias actions e views, portanto são definidas pela classe
 * @var string $area área do estádio
 * @var string $setor setor (camarote ou bloco) na visão atual para seleção de assento
 * @var string $assento lugar / cadeira marcada na seleção de assento
*/
class SiteController extends Controller
{
    public $enableCsrfValidation = false;
    public $area;
    public $setor;
    public $assento;
    private $secret = "6Lecv0YUAAAAACYEIe4EUuA2Kf9XzTMR3IZ0-cnJ"; /* CHAVE SECRETA RECAPTCHA */

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    /**
     * Método chamado antes de cada action (página) desse controller
    */
    public function beforeAction($action){

        $file = file_get_contents(Yii::getAlias('@app').'/data/controles.js');
        $jsonFile = json_decode($file); 

        if($jsonFile->sistema->manutencao->offline === true && $this->getRoute() != 'site/offline')
        {
            return $this->redirect(['/site/offline']);
            exit;
        }

        if($jsonFile->sistema->manutencao->offline === false && $this->getRoute() == 'site/offline')
        {
           return $this->redirect(['/site']);
           exit;
        }
        
        $session = Yii::$app->session;
        $session->open();
        if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
            $dataExpira = date($session['tempoCarrinho']);
            if ($dataExpira < date('Y,m,d,H,i,s')) { 
                $session['tempoCarrinho'] = '';
            }
        }
        // correção de link do voucher
        if (strpos(Yii::$app->request->getUrl(), '?r=site/resumo-compra&hash_compra')>-1) {
            Yii::$app->response->redirect(Url::to(['site/resumo-compra','hash_compra'=>$_GET['hash_compra']]), 301);
            Yii::$app->end();
        }

        //setando token da unidade em sessão
        $tokenUnidade=$session->get('token_unidade',false);
        $docUnidade=$session->get('documento_unidade',false);
        if(!$tokenUnidade || !$docUnidade){
            $objApiCliente = new Cliente;
            $objApiCliente->buscarUnidadePor('user_token',$objApiCliente->getTokenMatriz());
            $session->set('token_unidade',$objApiCliente->user_token);
            $session->set('documento_unidade',$objApiCliente->documento);
        }

        return parent::beforeAction($action);
    }    
    

	/*
     * Listar os eventos pela classe model Eventos
     * Captura exceção com nome "Erro listando eventos"
     * @return Response
     */
    public function actionIndex(){                  

        \Yii::$app->view->registerMetaTag([
            'name' => 'kdt:page',
            'content' => 'index',
        ]);

        try {
            $modelEventos = new EventosModel();
            $eventos = $modelEventos->listaEventos(['ativo'=>'1']);
            return $this->render('index', ['eventos' => $eventos]);
            
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro listando eventos', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Traz as informações de um evento e os lugares disponíveis nas áreas do estádio
     * @param int $id o ID do evento na base de dados
     * @return Response
     */
    public function actionAreas($id){
        try {

            \Yii::$app->view->registerMetaTag([
                'name' => 'kdt:page',
                'content' => 'areas',
            ]);

            $modelEvento = new EventosModel();
            $evento = $modelEvento->infoEventoComAreas($id);
            return $this->render('areas', ['evento' => $evento,'matriz'=>['areasDisponiveis'=>'["Leste","Oeste"]']]);
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro buscando informações de áreas', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Traz as informações de um evento, área selecionada e blocos disponíveis
     * @param int $id o ID do evento na base de dados
     * @param string $area o nome da área selecionada
     * @param string $setor o setor ou camarote selecionado
     * @param string $assento o assento selecionado
     * @return Response
     */
    public function actionBlocos($id, $area, $setor=null, $assento=null){

        \Yii::$app->view->registerMetaTag([
            'name' => 'kdt:bloco-area',
            'content' => $id.'-'.$area,
        ]);     

        try {
            $modelEvento = new EventosModel();
            $this->area=$area;
            $this->setor=$setor;
            $this->assento=$assento;
            $evento = $modelEvento->infoEventoComLugares($id, $area, $area == 'Leste' ? [127, 128, 130] : ['vip106', 'vip107', 108, 109, 110, 111]);
            $session = Yii::$app->session;
            $session->open();
            $carrinho = isset($session['idCarrinho']) ? (new ApiCarrinho)->consultarCarrinho($session['idCarrinho']) : (object)[];
            $session->set('cobrar_taxa',false);
            $tags = new Tags();

            if(isset($carrinho->objeto,$carrinho->objeto->produtos)){
                $_produtos=$carrinho->objeto->produtos;
                $ingressoCopaNoCarrinho=false;
                foreach ($_produtos as $p) {
                    if(!isset($p->tags)){
                        $ingressoCopaNoCarrinho=true;
                        continue;
                    }
                    $_tags=Tags::arrayTags($p->tags);
                    $ingressoCopaNoCarrinho=!isset($_tags['matriz']);
                }
                if($ingressoCopaNoCarrinho){
                    return $this->render('/agenda/compra-em-andamento', [
                        'evento' => $evento['evento'],
                        'tags' => $evento['evento']->tags,
                    ]);
                }
            }
            return $this->render('blocos', [
                'evento' => $evento['evento'], 
                'setores' => $evento['jsonAssentos']->objeto,
                'compra' => new CompraModel,
                'carrinho' => $carrinho,
            ]);
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro buscando informações de blocos', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Página de visitante - efetuar login ou link para cadastro
     */
    public function actionVisitante()
    {
        \Yii::$app->view->registerMetaTag([
            'name' => 'kdt:page',
            'content' => 'login',
        ]);

        return $this->render('visitante', [
            'cliente' => new ClienteModel
        ]);
    }

    /**
     * Página de Frequent Asked Questions (FAQ)
     */
    public function actionFaq()
    {
        return $this->render('faq');
    }

    /**
     * Se o usuário logado não for um comprador, mostrar informações de acesso operador
     */ 
    public function actionAcessoAdministrativo()
    {
        return $this->render('acesso-administrativo');
    }

    /**
     * Página de cliente. 
     * @param $statusConta - se tiver um valor, checa se já concluiu o processo. Senão, manda usuário para passo anterior
     */
    public function verificaClienteLogOn($statusConta=false)
    {
        $session = Yii::$app->session;
        $session->open();
        $clienteApi = isset($session['user_token']) && !empty($session['user_token']) ? (new ApiCliente)->getDadosComprador() : [];    

        
        if(!isset($clienteApi->successo) || $clienteApi->successo!=1){
            $checkTokenOperador=\app\models\User::findIdentityByAccessToken($session['user_token']);            
            if($checkTokenOperador && $checkTokenOperador->operador=='1')
                return $this->redirect(['acesso-administrativo']);

            return $this->redirect(['site/visitante']);
        }
        switch($statusConta){
            case 'CadastroAcesso':
                if(!isset($session['user_token'])){
                    return $this->redirect(['site/cadastro-acesso']);
                }
            break;
            case 'CadastroDadosPessoais':
                if(!isset($session['documento'])){
                    return $this->redirect(['site/cadastro-dados-pessoais']);
                }
            break;
            case 'ClienteAcesso':
                if(!isset($session['user_token'])){
                    return $this->redirect(['site/cadastro-acesso']);
                }
            break;
            case 'ClienteDadosPessoais':
                if(!isset($session['documento'])){
                    return $this->redirect(['site/cliente-dados-pessoais']);
                }
            break;
        }
    }

    public function actionCliente()
    {
        $this->verificaClienteLogOn();

        \Yii::$app->view->registerMetaTag([
            'name' => 'kdt:page',
            'content' => 'cliente',
        ]);

        $this->redirect(['site/cliente-acesso']);
    }

    public function actionClienteAcesso()
    {
        $this->verificaClienteLogOn();

        \Yii::$app->view->registerMetaTag([
            'name' => 'kdt:page',
            'content' => 'cliente-acesso',
        ]);

        $session = Yii::$app->session;
        $session->open();
                
        $clienteApi = isset($session['user_token']) && !empty($session['user_token']) ? (new ApiCliente)->getDadosComprador() : [];

        return $this->render('cliente-acesso', [
            'cliente' => new ClienteModel,
            'clienteApi' => $clienteApi
        ]);
    } 

    public function actionClienteDadosPessoais()
    {
        $this->verificaClienteLogOn('ClienteAcesso');

        \Yii::$app->view->registerMetaTag([
            'name' => 'kdt:page',
            'content' => 'cliente-dados-pessoais',
        ]);

        $session = Yii::$app->session;
        $session->open();

        $clienteApi = isset($session['user_token']) && !empty($session['user_token']) ? (new ApiCliente)->getDadosComprador() : [];

        return $this->render('cliente-dados', [
            'cliente' => new ClienteModel,
            'clienteApi' => $clienteApi
        ]);
    }

    public function actionClientePagamentos()
    {
        $this->verificaClienteLogOn('ClienteDadosPessoais');

        \Yii::$app->view->registerMetaTag([
            'name' => 'kdt:page',
            'content' => 'cliente-pagamentos',
        ]);

        $session = Yii::$app->session;
        $session->open();

        $clienteApi = isset($session['user_token']) && !empty($session['user_token']) ? (new ApiCliente)->getDadosComprador() : [];
        $cartoesApi = isset($session['user_token']) && !empty($session['user_token']) ? (new ApiCartao)->consultarCartoes() : [];

        return $this->render('cliente-pagamento', [
            'clienteApi' => $clienteApi,
            'cartao' => new CartaoModel,
            'cartoesApi' => $cartoesApi,
            'clienteNovo' => false,
        ]);        
    }

    public function actionClienteCreditos()
    {
        $this->verificaClienteLogOn();

        \Yii::$app->view->registerMetaTag([
            'name' => 'kdt:page',
            'content' => 'cliente-creditos',
        ]);

        $session = Yii::$app->session;
        $session->open();

        $saldoContaMineirao=0;
        $erroSaldoContaMineirao=false;
        $apiCliente=new ApiCliente;
        $creditos=$apiCliente->getCreditosUsuario($session['documento']);
        if($creditos->successo)
            $saldoContaMineirao=$creditos->objeto[0]->creditos;
        else
            $erroSaldoContaMineirao=$creditos->erro->mensagem;

        return $this->render('cliente-creditos', [
            'saldo' => 'R$ '.number_format($saldoContaMineirao,2,',','.'),
            'msgErro' => $erroSaldoContaMineirao
        ]);          
    }

    public function actionClienteHistorico()
    {
        $this->verificaClienteLogOn();

        \Yii::$app->view->registerMetaTag([
            'name' => 'kdt:page',
            'content' => 'cliente-historico',
        ]);

        $session = Yii::$app->session;
        $session->open();

        $returnApi = isset($session['documento']) && !empty($session['documento']) ? (new ApiCarrinho)->consultarVendasFiltro(['comprador' => $session['documento']]) : [];

        $historico = new ArrayDataProvider([
            'allModels' => isset($returnApi->successo) && $returnApi->successo == '1' && !empty($returnApi->objeto) ? (array)$returnApi->objeto : [], 
            'pagination' => [
                'pageSize' => 0,
            ],
        ]);

        return $this->render('cliente-historico', [
            'historico' => $historico
        ]);
    }

    public function actionClienteMineiraocard()
    {
        $this->verificaClienteLogOn();

        \Yii::$app->view->registerMetaTag([
            'name' => 'kdt:page',
            'content' => 'cliente-mineiraocard',
        ]);

        $session = Yii::$app->session;
        $session->open();

        $ingressos = isset($session['documento']) && !empty($session['documento']) ? (new ApiIngresso)->consultar(['documento' => $session['documento']]) : [];

        return $this->render('cliente-mineiraocard', [
            'ingressos' => $ingressos
        ]);
    }

    public function actionClienteLogout()
    {
        $this->verificaClienteLogOn();

        \Yii::$app->view->registerMetaTag([
            'name' => 'kdt:page',
            'content' => 'cliente-logout',
        ]);

        return $this->render('cliente-logout');        
    }

    /**
     * Efeturar logout do site
     */
    public function actionLogout()
    {
        $session = Yii::$app->session;
        $session->open();
        $session['user_id'] = '';
        $session['user_token'] = '';
        $session['nome'] = '';
        $session['documento'] = '';
        $session->destroy();
        $session->close();
        return $this->redirect(['site/index']);
    }   
    
    /**
     * Página de cadastro
     */
    public function actionCadastro()
    {
        \Yii::$app->view->registerMetaTag([
            'name' => 'kdt:page',
            'content' => 'visitante-cadastro',
        ]);

        $this->redirect(['cadastro-acesso']);
    }

    public function actionCadastroAcesso()
    {
       \Yii::$app->view->registerMetaTag([
            'name' => 'kdt:page',
            'content' => 'visitante-cadastro-acesso',
        ]);
        
        return $this->render('cliente-acesso', [
            'cliente' => new ClienteModel
        ]);              
    }

    public function actionCadastroDadosPessoais()
    {
        $this->verificaClienteLogOn('CadastroAcesso');

        \Yii::$app->view->registerMetaTag([
            'name' => 'kdt:page',
            'content' => 'visitante-cadastro-dados-pessoais',
        ]);
        
        return $this->render('cliente-dados', [
            'cliente' => new ClienteModel
        ]);              
    }

    public function actionCadastroPagamentos()
    {
        $this->verificaClienteLogOn('CadastroDadosPessoais');

        \Yii::$app->view->registerMetaTag([
            'name' => 'kdt:page',
            'content' => 'visitante-cadastro-pagamentos',
        ]);

        $session = Yii::$app->session;
        $session->open();        

        $clienteApi = isset($session['user_token']) && !empty($session['user_token']) ? (new ApiCliente)->getDadosComprador() : [];   

        $cartoesApi = isset($session['user_token']) && !empty($session['user_token']) ? (new ApiCartao)->consultarCartoes() : [];


        return $this->render('cliente-pagamento', [
            'cartao' => new CartaoModel,
            'clienteApi' => $clienteApi,
            'cartoesApi' => isset($cartoesApi) && !empty($cartoesApi) ? $cartoesApi : [],
            'clienteNovo' => true
        ]);
    }

    /**
     * Rotina para login do cliente no site
     */
    public function actionLogin()
    {
        try {
            if (Yii::$app->request->post()) {
                $post = Yii::$app->request->post('ClienteModel');
                $auth = (new ApiCliente)->autenticarCliente([
                    'user_id' => $post['user_id'], 
                    'senha' => $post['senha']
                ]);
                $response = null;        
                if (isset($_POST['g-recaptcha-response'])) {
                    $reCaptcha = new ReCaptcha($this->secret);
                    $response = $reCaptcha->verifyResponse($_SERVER["REMOTE_ADDR"], Yii::$app->request->post('g-recaptcha-response'));
                }
                Yii::$app->response->format = Response::FORMAT_JSON;
                $session = Yii::$app->session;
                $session->open();
                $redirect = isset($session['redirect']) ? $session['redirect'] : Yii::$app->request->referrer;
                if ($auth->successo == '1' && $response != null && $response->success){
                    $session['tentativas_login'] = 0;
                } else {
                    $session['tentativas_login'] = isset($session['tentativas_login']) ? $session['tentativas_login'] + 1 : 1;
                    $session['erro_login'] = $auth;
                }
                return Yii::$app->request->isAjax ? $auth : $this->redirect($redirect);
            }
            return $this->redirect(['visitante']);
            //return (new ApiCliente)->getApiError('Faltam parâmetros para efetuar o login.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao efetuar login do cliente', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Rotina para cadastro básico de cliente
     */
    public function actionCadastroBasico()
    {
        try {
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $post = Yii::$app->request->post('ClienteModel');
                return (new ApiCliente)->criarConta([
                    'user_id' => $post['user_id'], 
                    'senha' => $post['senha']
                ]);
            }
            return (new ApiCliente)->getApiError('Faltam parâmetros para efetuar o cadastro básico.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao cadastrar cliente - dados básicos', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Rotina para cadastro completo de cliente
     */
    public function actionCadastroCompleto()
    {
        try {
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $post = Yii::$app->request->post('ClienteModel');
                $cliente=new ClienteModel;
                $cliente->load(Yii::$app->request->post());
                if($cliente->validate()){
                    $criarConta=(new ApiCliente)->criarContaCompleta([
                        'documento' => isset($post['documento']) ? preg_replace('/(\D)/','', $post['documento']) : '',
                        'nome' => $post['nome'],
                        'sexo' => $post['sexo'],
                        'data_nasc' => $post['data_nasc'],
                        'ddd' => $post['ddd'],
                        'celular' => preg_replace('/(\D)/','', $post['celular']),
                        'cep' => $post['cep'],
                        'endereco' => $post['endereco'],
                        'num' => $post['num'],
                        'compl' => $post['compl'],
                        'cidade' => $post['cidade'],
                        'bairro' => $post['bairro']
                    ]);
                    $retorno=[];
                    if(isset($criarConta->successo) && $criarConta->successo=='1'){
                        $carrinho=(new ApiCarrinho)->consultarCarrinho();
                        $retorno['successo']='1';
                        if(isset($carrinho->successo) && $carrinho->successo=='1' && $carrinho->objeto->totais->total==0)
                            $retorno['redirect']=Url::to(['/site/pagamento']);
                    }
                    return $retorno;
                }
            }
            return (new ApiCliente)->getApiError('Faltam parâmetros para efetuar o cadastro completo.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao cadastrar cliente - dados básicos', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Cadastro de cartão de crédito do cliente
     */
    public function actionCadastroCartao()
    {
        try {
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $post = Yii::$app->request->post('CartaoModel');
                return (new ApiCartao)->adicionarCartao([
                    'nome_cartao' => $post['nome_cartao'], 
                    'numero_cartao' => str_replace(['_', '.'], ['', ''], $post['numero_cartao']), 
                    'cvv' => $post['cvv'], 
                    'mes_venc' => $post['mes_venc'], 
                    'ano_venc' => $post['ano_venc'], 
                    'senha' => $post['cvv']
                ]);
            }
            return (new ApiCartao)->getApiError('Faltam parâmetros para cadastrar o cartão.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao cadastrar cartão de crédito', 'message' => $e->getMessage()]);
        }   
    }
    
    /**
     * Definir um cartão escolhido pelo cliente como seu favorito
     */
    public function actionCartaoFavorito()
    {
        try {
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return (new ApiCartao)->marcarFavorito(Yii::$app->request->post());
            }
            return (new ApiCartao)->getApiError('Faltam parâmetros para definir este cartão como favorito.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao tornar um cartão favorito', 'message' => $e->getMessage()]);
        }  
    }
    
    /**
     * Remover um cartão selecionado pelo cliente
     */
    public function actionCartaoExcluir()
    {
        try {
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $excluir=(new ApiCartao)->excluirCartao(Yii::$app->request->post());
                if(isset($excluir->successo) && $excluir->successo==1)
                    Yii::$app->session->remove('codigo_cartao');
                return $excluir;
            }
            return (new ApiCartao)->getApiError('Faltam parâmetros para excluir o cartão.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao remover um cartão', 'message' => $e->getMessage()]);
        }  
    }
    
    /**
     * Buscar cep informado e retornar informações relacionadas, se existirem
     */
    public function actionCheckCep() 
    {
        \Yii::$app->view->registerMetaTag([
            'name' => 'kdt:page',
            'content' => 'valida-cep',
        ]);

        try {
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $post = Yii::$app->request->post();
                return (new ApiCliente)->consultarCep($post);
            }
            return (new ApiCliente)->getApiError('Faltam parâmetros para consultar o CEP.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro buscando informações de CEP', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Campos para ocupantes dos assentos. Verifica se cliente está logado verificando dados de sessão e salvando o endereço de redirecionamento caso positivo
     * @todo contador do tempo de carrinho
     */
    public function actionResumo($evento=null, $data=null) 
    {
        $session = Yii::$app->session;
        $session->open();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $objApiCliente->getTokenMatriz());
        $carrinho=(new ApiCarrinho)->consultarCarrinho();
        if(is_null($evento) && isset($carrinho->successo) && $carrinho->successo=='1')
            $evento=$carrinho->objeto->produtos[0]->agenda->codigo;
        $objEvento = new Agenda($objApiCliente);
        $objEvento->codigo=$evento;
        $objEvento->umEvento();
        $tags=Tags::objetoTags($objEvento->tags);
        if(isset($tags,$tags->assento) && $tags->assento=='livre')
            $this->redirect(['compra/ingresso','evento'=>$evento,'data'=>$data]);
        $nivelAcesso=$session['nivelAcesso'];
        $pgtoConciliacao=$nivelAcesso=='super-admin'||$nivelAcesso=='operadorBilheteria';
        /*if (!$pgtoConciliacao && (!isset($session['user_token']) || empty($session['user_token']) || !isset($session['documento']) || empty($session['documento']) || !isset($session['codigo_cartao']) || empty($session['codigo_cartao']))) {
            $session['redirect']=Url::to(['site/resumo']);
            $this->redirect(['site/visitante']);
        }*/
        $clienteApi = isset($session['user_token']) && !empty($session['user_token']) ? (new ApiCliente)->getDadosComprador() : [];
        try {
            $cartao = new ApiCartao;
            
            return $this->render('resumo', [
                'evento' => (object)['objeto'=>[$objEvento]],
                'cliente' => $clienteApi,
                'carrinho' => $carrinho,
                'id_carrinho' => $session['idCarrinho'],
                'cartao' => $cartao->consultarFavorito(),
                'pgtoConciliacao'=>$pgtoConciliacao,
                'mineiraoCard' => new Mineiraocard()
            ]);

            \Yii::$app->view->registerMetaTag([
                'name' => 'kdt:page',
                'content' => 'resumo',
            ]);

        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao carregar o resumo da compra', 'message' => $e->getMessage()]);
        }   
    }
    
    /**
     * Enviar intenção de compra para a API
     * OBS: Preço à vista - deixar vazio (uso dos valores do carrinho, vindos diretamente da api)
     */
    public function actionEnviarCobranca() 
    {
        try {
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $session = Yii::$app->session;
                $session->open();
                $post = Yii::$app->request->post();
                $arrayEnvioCobranca=[
                    'trid' => $post['trid'], 
                    'id_carrinho' => isset($session['idCarrinho']) ? $session['idCarrinho'] : '', 
                    'documento' => $session['documento'], 
                    'email' => $session['user_id'], 
                    'valor' => isset($post['taxa2via']) && !empty($post['taxa2via']) ? $post['taxa2via'] : 
                               ($post['parcelas'] == '1' ? '' : floatval($post['preco-final-parcelado'])),
                    'forma' => $post['parcelas'], 
                    'descricao' => 'Mineirão'
                ];
                if(isset($post['Conciliacao']['tipo']) && $post['Conciliacao']['id_conciliacao']!=''){
                    $arrayEnvioCobranca['id_conciliacao']=$post['Conciliacao']['id_conciliacao'];
                    $arrayEnvioCobranca['tipo']=$post['Conciliacao']['tipo'];
                    $arrayEnvioCobranca['documento']=$post['Conciliacao']['documento'];
                }
                if (isset($post['check-pagar']) && $post['check-pagar']=='pagar-creditos') {
                    $arrayEnvioCobranca['tipo']='Créditos Mineirão';
                } else if (isset($post['taxa2via']) && !empty($post['taxa2via'])) {
                    $arrayEnvioCobranca['tipo'] = 'Taxa - 2ª via cartão mifare';
                }
                $apiCarrinho=new ApiCarrinho;
                $carrinho=$apiCarrinho->consultarCarrinho();
                
                if($carrinho->objeto->totais->total==0){
                    $arrayEnvioCobranca['id_conciliacao']=time();
                    $arrayEnvioCobranca['tipo']='Gratuidade';
                    $arrayEnvioCobranca['documento']=$session->get('documento');
                }
                
                $cobranca=$apiCarrinho->enviarCobranca($arrayEnvioCobranca);
                
                if($cobranca->successo && isset($arrayEnvioCobranca['id_conciliacao']))
                    $apiCarrinho->limparSessionCarrinho($cobranca);

                return $cobranca;
            }
            return (new ApiCarrinho)->getApiError('Faltam parâmetros enviar a cobrança.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao enviar cobrança', 'message' => $e->getMessage()]);
        }  
    }
    
    /**
     * Consultar intenção de compra
     */
    public function actionConsultarCobranca() 
    {
        try {
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $post = Yii::$app->request->post();
                return (new ApiCarrinho)->consultarCobranca(['trid' => $post['trid']]);
            }
            return (new ApiCarrinho)->getApiError('Faltam parâmetros consultar a cobrança.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao consultar cobrança - pré-autorização', 'message' => $e->getMessage()]);
        }  
    }
    
    /**
     * Autorizar pagamento de compra
     */
    public function actionAutorizarPagamento() 
    {
        try 
        {
            $session = Yii::$app->session;
            $session->open();

            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $post = Yii::$app->request->post();
                $arrayEnvioAutorizacao=[
                    'codigo_cartao' => $post['codigo_cartao'],
                    'senha_cartao' => $post['senha'],
                    'id_compra' => $post['id_compra']
                ];

                $paymentType = 'credit';
                
                if(isset($post['Conciliacao']['tipo']) && $post['Conciliacao']['tipo']!='') 
                {
                    $arrayEnvioAutorizacao['Conciliacao']=$post['Conciliacao'];

                    switch ($post['Conciliacao']['tipo']) 
                    {
                        case 'Boleto':
                                $paymentType = 'boleto';
                            break;
                        
                        default:
                                $paymentType = 'transfer';
                            break;
                    }
                }

                $apiCarrinho = new ApiCarrinho();

                //antes de autorizar o Pagamento para limpar o carrinho
                //grava o carrinho no []
                $shoppingCart = [];

                $carrinho = $apiCarrinho->consultarCarrinho();   

                if (isset($carrinho->objeto->produtos)) {
                    foreach ($carrinho->objeto->produtos as $k => $item) 
                    {
                        $discount = 0;

                        if($item->desconto->fixo > 0) 
                            $discount = $item->desconto->fixo;

                        if($item->desconto->percentual > 0) 
                            $discount = $item->valor * ($item->desconto->percentual/100);

                        $shoppingCart[] = [
                            'sku' => $item->codigo,
                            'category' => 300,
                            'name' => $item->nome,
                            'description' => $item->descricao,
                            'unit_cost' => (int) $item->valor,
                            'quantity' => (int) $item->unidades,
                            'discount' => (int) $discount,
                        ];                        
                    }
                }

                //autorização do pagamento
                $autorizarPagamento = $apiCarrinho->autorizarPagamento($arrayEnvioAutorizacao);

                $dadosComprador = (new ApiCliente())->getDadosComprador();

                //envio de dados para API do Konduto
                $kondutoPedido = [
                    'id' => $post['id_compra'],
                    'total_amount' => isset($post['preco-final']) && !empty($post['preco-final']) 
                                            ? (float) number_format($post['preco-final'], 2, '.', '') 
                                            :(
                                                isset($post['taxa2via']) && !empty($post['taxa2via']) 
                                                ? (float)$post['taxa2via'] 
                                                : ''
                                            ),
                    'currency'=> 'BRL',
                    'customer' => [
                        'id' => $dadosComprador->objeto->user_id,
                        'name' => $dadosComprador->objeto->comprador->nome,
                        'tax_id' => $dadosComprador->objeto->comprador->documento,
                        'dob' => (new \DateTime(str_replace('/', '-', $dadosComprador->objeto->comprador->data_nasc)))->format('Y-m-d'),
                        'phone1' => '('.$dadosComprador->objeto->comprador->ddd.') '.$dadosComprador->objeto->comprador->celular,
                        'email' => $session['user_id'],
                    ],
                    'billing' => [  
                        'name' => $dadosComprador->objeto->comprador->nome,
                        'address1' => $dadosComprador->objeto->comprador->endereco,
                        'address2' => $dadosComprador->objeto->comprador->compl,
                        'city' => $dadosComprador->objeto->comprador->cidade_nome,
                        'state' => $dadosComprador->objeto->comprador->uf,
                        'zip' => $dadosComprador->objeto->comprador->cep,
                    ],                         
                ];
                
                $kondutoPedido['shopping_cart'] = $shoppingCart;  

                if($paymentType == 'credit') {

                    $autorizacaoStatus = isset($autorizarPagamento->objeto->status) ? $autorizarPagamento->objeto->status : $autorizarPagamento->erro->codigo;

                    switch ($autorizacaoStatus) 
                    {
                        case 'Autorizado':
                            $paymentStatus = 'approved';
                            break;
                        case 'Não autorizado': case 'Cancelado': case 134: case 137: case 138: 
                            $paymentStatus = 'declined';
                            break;
                        default:
                            $paymentStatus = 'pending';
                            break;
                    }
                
                    $kondutoPedido['installments'] = (int) $post['parcelas'];
                    $kondutoPedido['payment'][0]['type'] = $paymentType;
                    $kondutoPedido['payment'][0]['status'] = $paymentStatus;      
                } 
                else
                {
                    $kondutoPedido['payment'][0]['type'] = $paymentType;  
                }

                if($autorizarPagamento->successo == 1)
                {   
                    $kondutoPedido['purchased_at'] = \DateTime::createFromFormat('d/m/Y H:i:s', $autorizarPagamento->objeto->data_compra)->format('Y-m-d\TH:i:s\Z'); 
                    if(isset($autorizarPagamento->objeto->validade_cartao) && $autorizarPagamento->objeto->validade_cartao != '')   
                     $kondutoPedido['payment'][0]['expiration_date'] = preg_replace('/(\D)/','', $autorizarPagamento->objeto->validade_cartao);

                    if(strlen($autorizarPagamento->objeto->bin) == 6)
                        $kondutoPedido['payment'][0]['bin'] = $autorizarPagamento->objeto->bin; 
                        
                    $kondutoPedido['payment'][0]['last4'] = $autorizarPagamento->objeto->cartao;    
                }

                $file = file_get_contents(Yii::getAlias('@app').'/data/controles.js');
                $jsonFile = json_decode($file);

                $diaAtual = (new \DateTime('now'))->format('d');
                $mesAtual = (new \DateTime('now'))->format('m');
                
                $enviados = $jsonFile->konduto->pedidos->enviados_mensal;

                if($diaAtual == '01')
                    $jsonFile->konduto->pedidos->enviados_mensal = 0;           
                else
                    $jsonFile->konduto->pedidos->enviados_mensal++;

                $jsonFileNovo = json_encode($jsonFile);
                file_put_contents(Yii::getAlias('@app').'/data/controles.js', $jsonFileNovo);  
                /*
                * Informações de disparo de e-mail
                */
                $content = null;
                $apiKonduto = new ApiKonduto();

                if(in_array($enviados, [3500, 3900]))
                {
                    $content .= '<strong>Konduto - Pedidos<strong><br />';
                    $content .= 'O limite enviado do mês '.$mesAtual.' está em '.$enviados; 
                }

                if($enviados < 4001) 
                {
                    $respostaKonduto = $apiKonduto->call('orders', 'post', $kondutoPedido);

                    if ($respostaKonduto['code'] != 200 || $respostaKonduto['code'] != 201) 
                    {
                        $content .= '<strong>Code:</strong>'.$respostaKonduto['code'].'<br />';
                        $content .= '<strong>Message:</strong>:<br />'.$respostaKonduto['message'];
                    }
                }

                if(!is_null($content)) 
                {
                    $from = 'vendas@estadiomineirao.com.br';
                    $to = 'ariel@easyforpay.com.br';
                    $subject = 'Informações da API Konduto';

                    $apiKonduto->enviarEmailSwift($from, $to, $subject, $content);
                }        
                return $autorizarPagamento;
            }
            
            return (new ApiCarrinho)->getApiError('Faltam parâmetros autorizar o pagamento.');

        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao autorizar pagamento', 'message' => $e->getMessage()]);
        }  
    }
    
    /**
     * Após pagamento de taxa, solicitar segunda-via de ingressos
     */
    public function actionSegundaVia () 
    {
        try {
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post('ingressos'))) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $post = Yii::$app->request->post('ingressos');
                $retorno = true;
                foreach($post as $index => $ingresso) {
                    $postSegundaVia=[
                        'id_agenda' => $ingresso['id_agenda'],
                        'qrcode' => $ingresso['qrcode']
                    ];
                    if(isset($ingresso['nome']))
                        $postSegundaVia['nome']=$ingresso['nome'];
                    $returnApi = (new ApiIngresso)->segundavia($postSegundaVia);
                    if ($returnApi->successo == '0') {
                        $retorno = false;
                    }
                }
                if ($retorno) {
                    return (new ApiIngresso)->getApiSuccess('Segunda-via de cartão solicitada com sucesso.');
                }
                return (new ApiIngresso)->getApiError('Falha ao solicitar segunda via de cartão.');
            }
            return (new ApiCarrinho)->getApiError('Faltam parâmetros para solicitar segunda via de ingressos.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao solicitar segunda via de ingressos', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Ao solicitar a segunda via de mifare, enviar e-mail aos responsáveis
     */
    public function actionEmail2via () 
    {
        try {
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post('ingressos'))) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $post = Yii::$app->request->post('ingressos');
                foreach($post as $index => $ingresso) {
                    $post[$index]['dados'] = (new ApiIngresso)->consultar([
                        'id_agenda' => $ingresso['id_agenda'],
                        'qrcode' => $ingresso['qrcode']
                    ]);
                }
                return (new ApiMensagem)->emailSegundaViaMifare($post);
            }
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao enviar e-mail aos responsáveis por segunda via de mifare', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Enviar resumo da compra para o e-mail do cliente
     * @param integer $id_compra id da compra efetuada
     */
    public function actionEmailCompra($id_compra)
    {
        try {
            if (!empty($id_compra) && (int)$id_compra > 0) {               
                $compra = (new ApiCarrinho)->consultarVendasFiltro(['codigo' => $id_compra]);
                $returnApi = (new ApiMensagem)->enviarEmailCompra($compra);
                if ($returnApi->successo === '0') {
                    $returnApi = $this->enviarSmsCompra($compra);
                }
                return $this->redirect(['resumo-compra','hash_compra'=>$returnApi->hash_compra]);
                
            }
            return (new ApiMensagem)->getApiError('Faltam parâmetros enviar o e-mail de compra.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao enviar e-mail de resumo da compra', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Enviar resumo da compra para o e-mail do cliente
     * @param object $compra Resumo da compra
     * @param object $carrinho Carrinho correspondente
     */
    private function enviarSmsCompra($compra)
    {
        try {
            if (!empty($compra)) {
                return (new ApiMensagem)->enviarSmsCompra($compra);
            }
            return (new ApiMensagem)->getApiError('Faltam parâmetros enviar o SMS de compra.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao enviar e-mail de resumo da compra', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Resumo de uma compra
     * @param object $hash_compra Hash da compra efetuada (e-mail ou área de cliente)
     */
    
    /*
    public function actionResumoCompra($hash_compra='',$opc='') 
    {
        try {
            $session = Yii::$app->session;
            $session->open();
            $hashDecript = !empty($hash_compra) ? (new ApiCarrinho)->getHashCompra($hash_compra) : '';
            $hashArray = $hashDecript->successo === '1' ? explode('|' , $hashDecript->objeto->mensagem) : '';
            $detalheVenda = (count($hashArray) == 3) ? VendaWidget::getVenda($hashArray[2]) : '';
            

            $clienteApi = isset($session['user_token']) && !empty($session['user_token']) ? (new ApiCliente)->getDadosComprador() : [];
            $cartoesApi = isset($session['user_token']) && !empty($session['user_token']) ? (new ApiCartao)->consultarCartoes() : [];
            $saldoContaMineirao=0;
            $erroSaldoContaMineirao=false;
            $apiCliente=new ApiCliente;
            $creditos=$apiCliente->getCreditosUsuario($session['documento']);
            if($creditos->successo)
                $saldoContaMineirao=$creditos->objeto[0]->creditos;
            else
                $erroSaldoContaMineirao=$creditos->erro->mensagem;
            if($opc=='print'){
                $this->layout='print';
                $compra = (new ApiCarrinho)->consultarVendasFiltro(['codigo' => $hash_compra]);
                $carrinho;
                if ($compra->successo == '1') {
                    $carrinho = (new ApiCarrinho)->consultarCarrinho($compra->objeto[0]->id_carrinho); 
                }
                return $this->render('resumo-compra-print', [
                    'compra' => $compra,
                    'carrinho' => $carrinho,
                    'clienteApi' => $clienteApi,
                    'cartoesApi' => $cartoesApi,
                    'hash_compra' => $hash_compra,
                    'cliente' => new ClienteModel, 
                    'cartao' => new CartaoModel,                    
                    'erroSaldoContaMineirao'=>$erroSaldoContaMineirao,
                    'saldoContaMineirao'=>$saldoContaMineirao,
                ]);
            }else{
                return $this->render('resumo-compra', [
                    'detalheVenda' => $detalheVenda,
                    'clienteApi' => $clienteApi,
                    'cartoesApi' => $cartoesApi,
                    'hash_compra'=>$hash_compra,
                    'cliente' => new ClienteModel, 
                    'cartao' => new CartaoModel,                    
                    'erroSaldoContaMineirao'=>$erroSaldoContaMineirao,
                    'saldoContaMineirao'=>$saldoContaMineirao,
                ]);
            }
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao carregar resumo da compra', 'message' => $e->getMessage()]);
        }   
    }
    */
    
    public function actionResumoCompra($hash_compra='',$opc='') {
        try {
            $session = Yii::$app->session;
            $session->open();            
            $hashDecript = !empty($hash_compra) ? (new ApiCarrinho)->getHashCompra($hash_compra) : '';
            $hashArray = $hashDecript->successo === '1' ? explode('|' , $hashDecript->objeto->mensagem) : '';
            if(count($hashArray) == 3){
                $compraCheck = (new ApiCarrinho)->consultarVendasFiltro(['codigo' => $hashArray[2]]);
                if(!isset($compraCheck->objeto) || $compraCheck->objeto[0]->vendas->codigo_status!=='1')
                    return $this->redirect(['cliente-historico']);
            }
            $detalheVenda = (count($hashArray) == 3) ? VendaWidget::getVenda($hashArray[2]) : '';
            $clienteApi = isset($session['user_token']) && !empty($session['user_token']) ? (new ApiCliente)->getDadosComprador() : [];
            $cartoesApi = isset($session['user_token']) && !empty($session['user_token']) ? (new ApiCartao)->consultarCartoes() : [];
            $saldoContaMineirao=0;
            $erroSaldoContaMineirao=false;
            $apiCliente=new ApiCliente;
            $creditos=$apiCliente->getCreditosUsuario($session['documento']);
            if($creditos->successo)
                $saldoContaMineirao=$creditos->objeto[0]->creditos;
            else
                $erroSaldoContaMineirao=$creditos->erro->mensagem;            
            if($opc=='print'){
                $this->layout='print';
                $compra = (new ApiCarrinho)->consultarVendasFiltro(['codigo' => $hash_compra]);
                $carrinho;
                if ($compra->successo == '1') {
                    $carrinho = (new ApiCarrinho)->consultarCarrinho($compra->objeto[0]->id_carrinho); 
                }
                return $this->render('resumo-compra-print', [
                    'compra' => $compra,
                    'carrinho' => $carrinho,
                    'clienteApi' => $clienteApi,
                    'cartoesApi' => $cartoesApi,
                    'hash_compra' => $hash_compra,
                    'cliente' => new ClienteModel, 
                    'cartao' => new CartaoModel,                    
                    'erroSaldoContaMineirao'=>$erroSaldoContaMineirao,
                    'saldoContaMineirao'=>$saldoContaMineirao,
                ]);
            } else {
                return $this->render('resumo-compra', [
                    'detalheVenda' => $detalheVenda,
                    'clienteApi' => $clienteApi,
                    'cartoesApi' => $cartoesApi,
                    'hash_compra'=>$hash_compra,
                    'cliente' => new ClienteModel, 
                    'cartao' => new CartaoModel,                    
                    'erroSaldoContaMineirao'=>$erroSaldoContaMineirao,
                    'saldoContaMineirao'=>$saldoContaMineirao
                ]);         
            }   
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao carregar resumo da compra', 'message' => $e->getMessage()]);
        }               
    }
    
    
    /**
     * Registrar inclusão de produto no carrinho
     * @param boolean $relativo define se a quantidade do produto deve ser absoluta ou relativa
     */
    public function actionAddCarrinho($relativo=false)
    {
        try {
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $post = Yii::$app->request->post();

                return (new ApiCarrinho)->atualizarCarrinho(['produto' => [
                    'codigo' => $post['carrinho-codigo'], 
                    'unidades' => $post['carrinho-quantidade'],
                    'relativo' => (int)$relativo
                ]]);

                $content = null;
                $content .= 'Evento' . $post['carrinho-evento'];
                $content .= ' - Bloco ' . $post['carrinho-bloco'];
                $content .= ' - Area ' . $post['carrinho-area'];
                $content .= ' - Codigo ' . $post['carrinho-codigo'];

                \Yii::$app->view->registerMetaTag([
                    'name' => 'kdt:page',
                    'content' => 'bloco-area',
                ]);  

                \Yii::$app->view->registerMetaTag([
                    'name' => 'kdt:produto',
                    'content' => $content,
                ]);        
            } 
            return (new ApiCarrinho)->getApiError('Faltam parâmetros adicionar o item ao carrinho.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao adicionar item ao carrinho', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Retornar tempo de validade do carrinho
     * @return type
     */
    public function actionTempoCarrinho () 
    {
        try {
            if (Yii::$app->request->isAjax) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $session = Yii::$app->session;
                $session->open();
                return isset($session['tempoCarrinho']) ? 
                    ['successo' => '1' , 'tempoCarrinho' => $session['tempoCarrinho']] : 
                    ['successo' => '0' , 'tempoCarrinho' => '0'];
            }
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao verificar o tempo do carrinho', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Remover compra no carrinho
     * @param integer $id_evento Id do evento
     * @param integer $id Id do produto
     */
    public function actionDelCarrinho(){

        \Yii::$app->view->registerMetaTag([
            'name' => 'kdt:page',
            'content' => 'del-carrinho',
        ]);

        try {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                $post = Yii::$app->request->post();
                return (new ApiCarrinho)->atualizarCarrinho(['produto' => ['codigo' => $post['id'], 'unidades' => 0]]);
            } 
            return (new ApiCarrinho)->getApiError('Faltam parâmetros para excluir o item do carrinho');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao remover item do carrinho', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Trazer um produto da API pelo parâmetro GET
     * @param array $id Id de um produto
     * @return json informações do produto
    */
    public function actionProdutoId($id){
        $model=new ApiProduto;        
        try{
            $produtos=$model->buscarProdutoTags(null,$id);
            if (Yii::$app->request->isAjax){
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $produtos;
            }
            return $this->render('listaProdutos',['produtos'=>$produtos]);
        }catch(Exception $e){
            return $this->render('error', ['name' => 'Erro ao consultar produto', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Trazer um produto da API pelo parâmetro GET. Verificar antes se o bloco está ativo
     * @param array $tags array de objetos nome:valor das tags para filtrar
     * @return json informações do produto
    */
    public function actionProdutoPorTag(){
        $model = new ApiProduto;
        $tagsPost = $_POST['post'];
        foreach ($tagsPost as $key => $value) {
            $tagsBusca[$key]=$value;
        }
        try{
            $produtos = $model->buscarProdutoTags(json_encode(["ativo" => "1", "tags" => $tagsBusca]));
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $produtos;
            }
            return $this->render('listaProdutos', ['produtos' => $produtos]);
        } catch (Exception $e){
            return $this->render('error', ['name' => 'Erro ao consultar produto', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Enviar mensagem para cliente sobre troca de senha
     */
    public function actionMensagemEsqueciSenha()
    {
        try {
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $post = Yii::$app->request->post();
                $returnApi = (new ApiMensagem)->enviarMensagemEsqueciSenha($post);
                return $returnApi;
            }
            return (new ApiCarrinho)->getApiError('Faltam parâmetros para enviar mensagem para troca de senha.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao enviar mensagem - esqueci minha senha', 'message' => $e->getMessage()]);
        }
        
    }
    
    /**
     * Se usuário clicar em link recebido em seu e-mail, efetuar a troca de sua senha
     * @param string $hash_senha Hash com informações para gerar nova senha de cliente
     */
    public function actionTrocarSenha($hash_senha='')
    {
        try {
            if (!empty($hash_senha)) {
                $returnApi = (new ApiMensagem)->enviarNovaSenha($hash_senha);
                return $this->render('esqueci-senha', [
                    'retorno' => $returnApi
                ]);
            }
            return (new ApiCarrinho)->getApiError('Faltam parâmetros para trocar a senha de acesso.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao trocar senha de cliente', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Estornar compra do cliente
     */
    public function actionEstornarCompra()
    {
        try {
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $post = Yii::$app->request->post();
                return (new ApiCarrinho)->estornarPagamento($post);
            }
            return (new ApiCarrinho)->getApiError('Faltam parâmetros para estornar a compra.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao estornar compra do cliente', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Ação para renovação de lugares para próxima temporada
     * @param integer $id Id do evento
     * @param string $reserva Identificação da reserva do cliente
     * @param string $usuario Documento do cliente
     * @param string $area Área do estádio
     */
    public function actionTemporada ($id='', $reserva='', $usuario='', $area='') 
    {
        try {
            if ((int)$id > 0 && !empty($reserva) && !empty($usuario) && !empty($area)) {
                $session = Yii::$app->session;
                $session->open();
                $session['temporada_prazo_extra_2018']='true';
                $returnApi = (new ApiCarrinho)->criarCarrinho();
                if ($returnApi->successo == '1') {
                    $returnProdutos = (new ApiProduto)->setProdutosReserva($usuario, $area);
                    if (isset($returnProdutos->successo)) {
                        $carrinho = (new ApiCarrinho)->consultarCarrinho();
                    }
                }
                $evento = (new EventosModel)->infoEventoComLugares($id, $area, $area == 'Leste' ? [128, 130] : [108, 109, 110, 111]);
                return $this->render($area == 'Oeste' ? 'temporadaOeste' : 'temporadaLeste', [
                    'produtosReserva' => $returnProdutos->produtos,
                    'evento' => $evento['evento'], 
                    'setores' => $evento['jsonAssentos']->objeto,
                    'compra' => new CompraModel,
                    'carrinho' => $carrinho,
                ]); 
            }
            return (new ApiCarrinho)->getApiError('Faltam parâmetros para acesso à página de renovação de temporada.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro na página de renovação de temporada', 'message' => $e->getMessage()]);
        } 
    }
    
    /**
     * No caso de compra para renovação de temporada, validar informações do usuário e de produtos
     */
    public function actionValidaTemporada () 
    {
        try {
            if (Yii::$app->request->isAjax) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $session = Yii::$app->session;
                $session->open();
                if ((isset($session['renovacao_temporada']) && $session['renovacao_temporada'] == true && 
                    $this->validarClienteTemporada() && $this->validarProdutosTemporada()) || 
                    (!isset($session['renovacao_temporada']) || $session['renovacao_temporada'] == false)) {
                    return (new ApiCarrinho)->getApiSuccess('Validação de usuário e produtos para renovação de temporada efetuada com sucesso.');
                }
                return (new ApiCarrinho)->getApiError('Este usuário não pode efetuar a compra de renovação de temporada.');
            }
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro na validação de usuário e produtos para renovação - temporada', 'message' => $e->getMessage()]);
        } 
    }
    
    /**
     * Validar se cliente está renovando temporada
     * @return type
     */
    private function validarClienteTemporada ()
    {
        $session = Yii::$app->session;
        $session->open();
        if ($session['area_temporada'] == 'Oeste') {
            return (base64_encode($session['documento']) == $session['documento_temporada']) ? true : false;
        } 
        return ($session['area_temporada'] == 'Leste' && base64_encode($session['user_id']) == $session['documento_temporada']) ? true : false;
    }
    
    /**
     * Validar produtos que podem ser adquiridos na renovação
     * @return boolean
     */
    private function validarProdutosTemporada ()
    {
        $session = Yii::$app->session;
        $session->open();
        $produtosReserva = (new ApiProduto)->getProdutosReserva($session['documento_temporada'], $session['area_temporada']);
        $produtoCompra = true;
        if ($produtosReserva) {
            $arrProdutos = [];
            foreach ($produtosReserva as $index => $produto) {
                if (isset($produto->codproduto) && !empty($produto->codproduto)) {
                    $arrProdutos[] = $produto->codproduto;
                }
            }
            $carrinho = (new ApiCarrinho)->consultarCarrinho();
            if ($carrinho->successo == '1' && !empty($carrinho->objeto->produtos)) {
                foreach ($carrinho->objeto->produtos as $index => $produto) {
                    if (!in_array($produto->codigo, $arrProdutos)) {
                        $produtoCompra = false;
                    }
                }
            }
        }
        return $produtosReserva && $produtoCompra;
    }
    
    /**
     * Validar limite de compras por CPF e evento
     * OBS.: limite de 6 compras por CPF, por evento
     */
    public function actionValidarLimiteCompra () 
    {
        try {
            if (Yii::$app->request->isAjax) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                return (new ApiCarrinho)->validarLimiteCompra();
            }
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro na validação de limite de compra', 'message' => $e->getMessage()]);
        } 
    }
    
    /**
     * Após a renovação da temporada, limpar informações da sessão correspondente
     */
    public function delSessionTemporada()
    {
        $session = Yii::$app->session;
        $session->open();
        if ($session['renovacao_temporada'] == true) {
            $session['renovacao_temporada'] = '';
            $session['documento_temporada'] = '';
            $session['produtos_temporada'] = '';
            unset($session['renovacao_temporada']);
            unset($session['documento_temporada']);
            unset($session['produtos_temporada']);
        }
        $session->close();
    }
    
    /**
     * Salvar nome, cpf, id de produto e id de compra em tabela auxiliar. Método chamado por ajax
     * @return json com o resultado da requisição
     * @todo tratar caso o envio via POST vier vazio (Yii::$app->request->post('Mineiraocard'))
    */
    public function actionInfoMineiraoCard(){
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $post = Yii::$app->request->post('Mineiraocard');
            $produto=$post['produto'];
            $model=new \app\models\Mineiraocard();
            $model->load(Yii::$app->request->post());

            \Yii::$app->view->registerMetaTag([
                'name' => 'kdt:page',
                'content' => 'info-mineirao-card'
            ]);

            \Yii::$app->view->registerMetaTag([
                'name' => 'kdt:detalhe',
                'content' => 'salvando ocupante'
            ]);

            if(!$model->save())
                return ['successo'=>'0','erros'=>$model->getErrors(),'form'=>'formMineiraocard-'.$produto];
            else
                return ['successo'=>'1','mensagem'=>'Registro salvo com sucesso','form'=>'formMineiraocard-'.$produto,'id'=>$model->id];
        }
        
    }
    
    /**
     * Ao acessar a página de cadastro de cartões, verificar se há documento registrado na sessão, para impedir que o
     * cartão fique sem relacionamento com o documento correspondente
     */
    public function actionVerificarDocumento () 
    {
        try {
            if (Yii::$app->request->isAjax) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $session = Yii::$app->session;
                $session->open();
                if (!isset($session['documento']) || empty($session['documento'])) {
                    return (new ApiCarrinho)->getApiError('O cliente ainda não efetuou login.');
                }
                return (new ApiCarrinho)->getApiSuccess('Documento registrado na sessão.');
            }
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro na verificação de documento do cliente na sessão', 'message' => $e->getMessage()]);
        } 
    }
    
    /**
     * À partir de um código fornecido, retornar a imagem do QR Code correspondente
     * @param type $customCode
     * @return type
     */
    public function actionQrCode($customCode='')
    {
        try {
            if (!empty($customCode)) {
                return (new ApiCarrinho)->getQrCode($customCode);
            }
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro na geração de imagem do QR Code do voucher', 'message' => $e->getMessage()]);
        } 
    }

    /**
     * Resumo da compra e autorização de pagamento. Verifica se cliente está logado verificando dados de sessão e salvando o endereço de redirecionamento caso positivo. Verifica se os campos de ocupante foram preechidos
     * Verificar o nível de acesso do usuário. Se for super-admin ou operadorBilheteria, permitir seguir sem cartão e habilitar pagamento com id de conciliação     
     */
    public function actionPagamento($evento=null, $data=null) 
    {

        \Yii::$app->view->registerMetaTag([
            'name' => 'kdt:page',
            'content' => 'pagamento',
        ]);

        $session = Yii::$app->session;
        $session->open();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
        $nivelAcesso=$session['nivelAcesso'];
        $pgtoConciliacao=$nivelAcesso=='super-admin'||$nivelAcesso=='operadorBilheteria';
        $redirect=false;
        $carrinho=(new ApiCarrinho)->consultarCarrinho();
        if (!$pgtoConciliacao){
            if(!isset($session['user_token']) || empty($session['user_token'])){
                $session['redirect']=Url::to(['site/pagamento']);
                $redirect=Url::to(['site/visitante']);
            }
            
            if(!$redirect && (!isset($session['documento']) || empty($session['documento']))){
                Yii::$app->session->setFlash('aviso-cadastro','Informe seus dados');
                $redirect=Url::to(['site/cliente-dados-pessoais']);
            }
            if(isset($carrinho->objeto) && $carrinho->objeto->totais->total > 0){
                if(!$redirect && (!isset($session['codigo_cartao']) || empty($session['codigo_cartao']))){
                    Yii::$app->session->setFlash('aviso-cadastro','Cadastre ao menos um cartão de crédito válido');
                    $redirect=Url::to(['site/cliente-pagamentos']);
                }
            }
            if($redirect!==false)
                return $this->redirect($redirect);
            
        }
        try {
            $cartao=new ApiCartao;
            $mineiraoCard=new Mineiraocard;
            if(isset($carrinho->objeto) && !$mineiraoCard->infoOcupantes($carrinho->objeto)){
                Yii::$app->getSession()->setFlash('info-campos','Você deve preencher os dados dos ocupantes e clicar em OK para cada assento antes de prosseguir. Em seguida clique em PAGAMENTO.');
                $this->redirect(['site/resumo']);
            }
            if(is_null($evento) && isset($carrinho->successo) && $carrinho->successo=='1')
                $evento=$carrinho->objeto->produtos[0]->agenda->codigo;
            $objEvento = new Agenda($objApiCliente);
            $objEvento->codigo=$evento;
            $objEvento->umEvento();
            $parcelamento=true;
            $espaco='';
            $objTags=new Tags;
            $tags=$objTags::arrayTags($objEvento->tags);
            if(isset($tags['parcelamento']))
                $parcelamento=(bool)$tags['parcelamento'];

            return $this->render('pagamento', [
                'evento' => (object)['objeto'=>[$objEvento]],
                'carrinho' => $carrinho,
                'cartao' => $cartao->consultarFavorito(),
                'area'=>$session['area_temporada'],
                'espaco'=>$espaco,
                'pgtoConciliacao'=>$pgtoConciliacao,
                'parcelamento'=>$parcelamento,
                'listaProdutos'=> isset($tags['assento']) && $tags['assento']=='livre' ?$this->geraListaProdutos($objTags,$carrinho):null
            ]);
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao carregar o resumo da compra', 'message' => $e->getMessage()]);
        }   
    }
    
    /**
     * Ler arquivo e adicioná-lo como anexo de um evento
     * @param string $arquivo Local e nome do arquivo a ser adicionado como anexo em um evento 
     * (Exemplos: Exemplos: C:\wamp\www\mineirao\html\images\banner5.jpg OU /var/www/html/images/banner5.jpg)
     * @param integer $idEvento Evento que receberá anexo
     * @param string $nomeAnexo nome do arquivo na API. Exemplos: bannerapp, bannervitrine, banner_carrossel, etc
     * @return type
     */
    public function actionAddAnexo($arquivo='', $idEvento=0, $nomeAnexo='')
    {
        if (!empty($arquivo) && is_file($arquivo) && $idEvento > 0 && !empty($nomeAnexo)) {
            $agenda = new ApiAgenda();
            $returnImg = $agenda->lerArquivo($arquivo);
            if (!empty($returnImg)) {
                return $agenda->adicionarAnexoEvento([
                    'codigo' => $idEvento,
                    'nome' => $nomeAnexo,
                    'tipo' => $returnImg['tipo'],
                    'objeto' => $returnImg['arq']
                ]);
            }
        }
    }

    /**
     * Pesquisar se usuário possui conta no paguetudo
    */
    public function actionCheckConta($user_id){
        if (Yii::$app->request->isAjax)
            Yii::$app->response->format = Response::FORMAT_JSON;
        $auth=(new ApiCliente)->autenticarCliente(['user_id'=>$user_id]);
        return ['successo'=>isset($auth->objeto->conta_usuario)];
    }

    public function actionOffline()
    {
        $file = file_get_contents(Yii::getAlias('@app').'/data/controles.js');
        $jsonFile = json_decode($file); 

        $this->layout = false;
        return $this->render('offline', ['texto' => $jsonFile->sistema->manutencao->texto]);
    }

    /**
     * Iniciar pagamento com boleto. Primeiro, mostra o termo e check de aceite e só depois do submit o carrinho é esvaziado e mostra form para gerar boleto
     */
    public function actionBoleto(){
        $msg='';
//        $apiCarrinho=new ApiCarrinho;
//        $carrinho=$apiCarrinho->consultarCarrinho();
//        if(!$carrinho->successo)
//            return $this->render('error',['name'=>'Carrinho de compras inválido','message'=>'O carrinho de compras não existe ou não possui os valores corretos.']);
                        
        if (Yii::$app->request->post()) {
            $aceite=Yii::$app->request->post('aceite');
            if ($aceite) {
                $cliente = (new ApiCliente)->getDadosComprador();
                if (!$cliente->successo)
                    return $this->render('error', ['name' => 'Conta de usuário inválida','message'=>'Não foi possível recuperar informações de comprador.']);
                return $this->gerarBoleto(Yii::$app->request->post(), $cliente->objeto->comprador);
            } else
                $msg = 'Você precisa concordar com os termos';
        }
        return $this->render('boleto-aceite', ['msg' => $msg]);
    }

    /**
     * Gerar boleto - checa as informações para evitar manipulação do form
     */
    public function gerarBoleto($post,$cliente){
        $_trid=time() . mt_rand();
        $cobranca['trid']=substr($_trid, 8, 11);
        $cobranca['valor'] = $post['total_boleto'];
        $cobranca['mensagem']='Compra de crédito';
        $cobranca['unidades']='1';
        $cobranca['validade']=date('d/m/Y');
        $cobranca['forma']='1';
        $cobranca['creditar']='1';
        $cobranca['tipo']='Boleto';

        $sacado['nome']=$cliente->nome;
        $sacado['documento']=$cliente->documento;
        $sacado['endereco']=$cliente->endereco;
        $sacado['num']=$cliente->num;
        $sacado['compl']=$cliente->compl;
        $sacado['cidade_nome']=$cliente->cidade_nome;
        $sacado['uf']=$cliente->uf;

        $boleto['seunumero']=substr($_trid, 8, 10);
        $boleto['mensagem']='Compra de crédito Mineirão';

        $apiCarrinho=new ApiCarrinho;
//        $checkCompra=$apiCarrinho->verificarInfoCompraBoleto($cobranca,$sacado['documento']);
        
//        if(!$checkCompra)
//            return $this->render('error',['name'=>'Carrinho de compras inválido','message'=>'O carrinho de compras não existe ou não possui os valores corretos.']);

        $enviarCobranca=$apiCarrinho->enviarCobrancaBoleto($cobranca, $sacado, $boleto);
        if(!$enviarCobranca->successo)
            return $this->render('error',['name'=>'Erro ao gerar boleto','message'=>$enviarCobranca->erro->mensagem]);
        
        $compras=(new ApiCliente)->getComprasUsuario();
        if(!$compras->successo)
            return $this->render('error',['name'=>'Erro ao gerar boleto','message'=>$compras->erro->mensagem]);
        
//        $apiCarrinho->forcarLimparSessionCarrinho();
        return $this->render('boleto-gerado',['urlBoleto'=>$compras->objeto[0]->compras->url_boleto]);
    }

    public function actionSaldoUsuario(){
        if (!Yii::$app->request->isAjax)
            echo json_encode((object)['successo'=>'0','erro'=>['mensagem'=>'Requisição inválida']]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $session = Yii::$app->session;
        $session->open();
        $apiCliente=new ApiCliente;
        $clienteApi = isset($session['user_token']) && !empty($session['user_token']) ? $apiCliente->getDadosComprador() : [];
        if(!isset($clienteApi->successo) || $clienteApi->successo!='1')
            echo json_encode((object)['successo'=>'0','erro'=>['mensagem'=>'Usuário não encontrado']]);
        $creditos = $apiCliente->getCreditosUsuario($session['documento']);
        $creditosFormatado = 'R$ ' . number_format(isset($creditos->objeto[0]->creditos) ? $creditos->objeto[0]->creditos : 0,2,',','.');
        echo json_encode((object)['successo'=> isset($creditos->objeto[0]->creditos) ? '1' : '0', 'creditos' => isset($creditos->objeto[0]->creditos) ? $creditos->objeto[0]->creditos : 0, 'creditosFormatado' => $creditosFormatado]);
        exit();
    }

    public function actionPagamentoCreditos(){
        try {
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $post = Yii::$app->request->post();
                $apiCliente=new ApiCliente;
                $session = Yii::$app->session;
                $session->open();
                $creditos = $apiCliente->getCreditosUsuario($session['documento']);
                $creditosFormatado = number_format(isset($creditos->objeto[0]->creditos) ? $creditos->objeto[0]->creditos : 0,2,'.','');
                if($creditosFormatado<$post['preco_final'])
                    return (new ApiCarrinho)->getApiError('Saldo insuficiente');
                
                $qrcodes=$apiCliente->getQrCodes();

                if($qrcodes->successo)
                    return (new ApiCarrinho)->concluirCobrancaCreditos($post['codigo_cobranca'],$qrcodes->objeto[0]->qrcode,999999);
            }
            return (new ApiCarrinho)->getApiError('Faltam parâmetros consultar a cobrança.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao consultar cobrança - pré-autorização', 'message' => $e->getMessage()]);
        }       
    }

    /**
     * Montar a lista de produtos para ingressos e pagamento
     */
    private function geraListaProdutos($objTags,$carrinho){
        $listaProdutos=[];
        if(isset($carrinho->objeto,$carrinho->objeto->produtos)){
            if(isset($carrinho->objeto->tags)){
                $tags = $objTags->arrayTags($carrinho->objeto->tags);             
            }
            foreach ($carrinho->objeto->produtos as $index => $produto) {
                $contagemQtde=1;
                while ($contagemQtde <= $produto->unidades) {
                    $indiceNameForm=$contagemQtde;
                    $cupom=isset($produto->cupom) ?'_'.$produto->cupom->id :'';
                    $linhaProduto = ['codigo'=>$produto->codigo.$cupom.'_'.$indiceNameForm,'indexForm'=>$indiceNameForm,'objeto'=>$produto,'tipo'=>'inteira','nome'=>'','documento'=>'','email'=>'','telefone'=>''];
                    if(isset($tags)){
                        foreach ($tags as $k=>$v) {
                            if($linhaProduto['codigo'].'_nome' == $k)
                                $linhaProduto['nome']=$v;
                            if($linhaProduto['codigo'].'_documento' == $k)
                                $linhaProduto['documento']=$v;
                            if($linhaProduto['codigo'].'_email' == $k)
                                $linhaProduto['email']=$v;
                            if($linhaProduto['codigo'].'_telefone' == $k)
                                $linhaProduto['telefone']=$v;
                        }
                    }
                    if(isset($produto->cupom))
                        $linhaProduto['tipo']=$produto->cupom->id;
                    
                    $listaProdutos[]=$linhaProduto;
                    $contagemQtde++;
                }
            }
        }
        return $listaProdutos;
    }
}
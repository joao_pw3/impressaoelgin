<?php
namespace app\models;
use Yii;

/**
 * This is the model class for table "setores".
 *
 * @property string $id
 * @property string $nome
 * @property string $descricao
 * @property string $lotacao
 * @property string $caracteristicas
 * @property integer $status
 */
class SetoresModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setores';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome', 'descricao', 'lotacao'], 'required'],
            [['descricao', 'caracteristicas'], 'string'],
            [['lotacao', 'status'], 'integer'],
            [['nome'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'descricao' => 'Descricao',
            'lotacao' => 'Lotacao',
            'caracteristicas' => 'Caracteristicas',
            'status' => 'Status',
        ];
    }

    /**
     * @inheritdoc
     * @return SetoresQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SetoresQuery(get_called_class());
    }
}

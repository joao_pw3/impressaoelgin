<?php
use yii\helpers\Url;
use app\models\ApiAgenda;
use app\models\ApiProduto;
use yii\helpers\Html;
use app\models\Mineiraocard;
use app\components\TempoCarrinhoWidget;

$session = Yii::$app->session;
$session->open();
echo TempoCarrinhoWidget::getBreadCrumb('losang4.png');
?>

<div class="resumo" id="resumo">
    
    <input type="hidden" id="url-index" value="<?= Url::to(['site/index']); ?>">
    <input type="hidden" id="url-visitante" value="<?= Url::to(['site/visitante']); ?>">
    <input type="hidden" id="url-del-cart" value="<?= Url::to(['site/del-carrinho']); ?>">
    <input type="hidden" id="url-email-compra" value="<?= Url::to(['site/email-compra']); ?>">
    <input type="hidden" id="url-cobranca" value="<?= Url::to(['site/enviar-cobranca']); ?>">
    <input type="hidden" id="url-consulta" value="<?= Url::to(['site/consultar-cobranca']); ?>">
    <input type="hidden" id="url-autoriza" value="<?= Url::to(['site/autorizar-pagamento']); ?>">
    <input type="hidden" id="url-pagamento-creditos" value="<?= Url::to(['site/pagamento-creditos']); ?>">
    <input type="hidden" id="url-cliente" value="<?= Url::to(['site/cliente']); ?>">
    <input type="hidden" id="url-temporada" value="<?= Url::to(['site/valida-temporada']); ?>">
    <input type="hidden" id="url-limite" value="<?= Url::to(['site/validar-limite-compra']); ?>">
    <input type="hidden" id="id_evento" value="1">
    <input type="hidden" id="id_compra" value="">
    
    <div class="container">

        <?php if (isset($carrinho) && empty($carrinho->objeto->produtos)) { ?>
        <div class="boxMensagem2" style="height:350px; margin-bottom:25px;">
            Seu carrinho de compras está vazio. Acesse a <a href="<?= Url::to(['site/index']); ?>">página de eventos</a> para comprar os ingressos de seu interesse.
        </div>
        <?php } else {
             ?>
            <?php
                $render='/compra/_carrinho-pag';
                if(isset($listaProdutos))
                    $render='/compra/_lista-produtos-pag';
                echo $this->render($render,[
                    'carrinho'=>$carrinho,
                    'evento'=>$evento,
                    'listaProdutos'=>$listaProdutos
                ]);
            ?>
        <br clear="all">
        <span class="botaoVoltar"><i class="fa fa-arrow-left"></i> <a href="<?=Url::to(['site/resumo'])?>">voltar e editar</a></span>
        <br><br>
        <form id="form-pagamento" method="post" 
            <?php if(isset($carrinho->objeto) && $carrinho->objeto->totais->total == 0)
                echo 'class="hidden"';
            ?>
        >
            <div class="col-md-4 wow fadeInRight animated checkoutPgto" data-wow-delay=".5s">
                <div class="checkoutPgto1">
                    <input type="hidden" name="trid" value="<?= substr(time() . mt_rand(), 8, 11); ?>">
                    <p>Formas de Pagamento</p>
                    <?php if (!isset($cartao->ultimos_digitos) && !$pgtoConciliacao) { ?>
                    <p>&nbsp;</p>
                    <p align="left">Por favor, <a href="<?= Url::to(['site/cliente', '#' => 'tab-pagamentos']); ?>">clique aqui</a>, escolha seu cartão favorito ou<br>cadastre um novo cartão.</p>
                    <?php } else { ?>
                    <div class="row">
                        <div class="col-md-8 col-xs-8">
							<label for="check-pagar-cartao text-format-checkout">
								<input type="radio" name="check-pagar" value="pagar-cartao" checked="checked" id="check-pagar-cartao" style="width:15px; padding-top:50px;">Pagar com Cartão de final:
							</label>
						</div>
                        <div class="col-md-4 col-xs-4 text-format-checkout" align="right" style="padding-top:4px;">
                            <?= !empty($cartao->ultimos_digitos) ? $cartao->ultimos_digitos : '-'; ?>
                            <input type="hidden" name="codigo_cartao" id="codigo_cartao"value="<?= !empty($cartao->codigo_cartao) ? $cartao->codigo_cartao : ''; ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-xs-8 lbl-parcelas text-format-checkout" style="padding-left:40px; font-weight:normal;">Bandeira:</div>
                        <div class="col-md-4 col-xs-4" align="right"><?= !empty($cartao->bandeira) ? '<img src="'.Yii::getAlias('@web').'/images/' . $cartao->bandeira . '.png" width="50">' : '-'; ?></div>
                    </div>
                    <?php if($pgtoConciliacao) { ?>
                        <div class="row" id="pgtoConciliacao">
                            <div class="col-xs-4 nopadding">
                                <input id="Conciliacao_doc" size="12" type="text" placeholder="Documento" name="Conciliacao[documento]"></input>
                            </div>
                            <div class="col-xs-4 nopadding">
                                <select id="Conciliacao_tipo" name="Conciliacao[tipo]">
                                    <option value="">Conciliação por:</option>
                                    <option value="Depósito/TED/Doc">Depósito/TED/Doc</option>
                                    <option value="Boleto">Boleto</option>
                                    <option value="Dinheiro">Dinheiro</option>
                                    <option value="POS">POS</option>
                                    <option value="Outros">Outros</option>
                                </select>
                            </div>
                            <div class="col-xs-4 nopadding">
                                <input id="Conciliacao_id" size="12" type="text" placeholder="Identificação" name="Conciliacao[id_conciliacao]"></input>
                            </div>
                            <div class="col-xs-12" id="msgConciliacao"></div>
                        </div>
                    <?php }
                    } ?>
					
                </div>
            </div>
            <div class="col-md-4 wow fadeInRight animated checkoutPgto" data-wow-delay=".5s">
                <div class="checkoutPgto1">
                    <p>Compra Total</p>
                    <div class="row" style="margin-bottom:23px;">
                        <div class="col-md-7 col-xs-7 text-format-checkout">Total de itens:</div>
                        <div class="col-md-5 col-xs-5 alinhaD text-format-checkout"><?= $carrinho->objeto->totais->unidades; ?><input type="hidden" name="unidades-final" value="<?= $carrinho->objeto->totais->total; ?>"></div>
                    </div>
					
                    <div class="row" style="margin-bottom:23px;">
                        <div class="col-md-7 col-xs-7 text-format-checkout">Preço final:</div>
                        <div class="col-md-5 col-xs-5 alinhaD text-format-checkout">
                            R$ <?= number_format($carrinho->objeto->totais->total, 2, ',', '.'); ?>
                            <input type="hidden" name="preco-final" id="preco-final" value="<?= $carrinho->objeto->totais->total; ?>">
                        </div>
                    </div>
					
                    <?php
                    if(!$parcelamento){?>
                        <input type="hidden" name="parcelas" id="parcelas" value="1">
                    <?php }
                    
                    if($parcelamento){ 
                        $parcela = 1;
                        $limiteParcelas = 12;
                        if (isset($area) && $area=='Vermelha') {
                            $limiteParcelas = 10;
                        }
                        ?>
                        <div class="row">
                            <div class="col-md-6 col-xs-7 lbl-parcelas text-format-checkout">Parcele em até<br> <?=$limiteParcelas?> vezes:</div>
                            <div class="col-md-6 col-xs-5" style="text-align:right;">
                                <select name="parcelas" id="parcelas">
                                    <option value="">Parcele em:</option>
                                <?php $parcela=1;
                                while ($parcela <= $limiteParcelas) {
                                    $valorParcela = $carrinho->objeto->totais->total / $parcela;
                                ?>
                                    <option value="<?= $parcela; ?>"><?= $parcela; ?> x R$ <?= number_format($valorParcela, 2, ',', '.'); ?></option>

                                <?php 
                                    $parcela++; 
                                } ?>
                                </select>
                            </div>
                            <input type="hidden" name="preco-final-parcelado" id="preco-final-parcelado">
                        </div>
                    <?php } ?>
                    <br clear="all">
                </div>
            </div>
            <div class="col-md-4 wow fadeInRight animated checkoutPgto" data-wow-delay=".5s">
                <div class="checkoutPgto1">
                    <p>Confirmar pagamento</p>
                    <div class="row">
                        <div class="col-md-7 col-xs-7"><img src="<?php echo Yii::getAlias('@web')?>/images/cvv2.png" width="100%" style="max-width:200px; margin:0px auto 10px auto; display:block;"></div>
                        <div class="col-md-5 col-xs-5 col-sd-5 text-format-checkout">Informe o CVV do seu cartão: <input type="password" name="senha" id="senha" class="font-passwd" maxlength="4"<?= !isset($cartao->ultimos_digitos) ? ' disabled' : ''; ?> style="margin-top:10px;"></div>
                    </div>
                </div>
                <br clear="all">
                <br>
				<br>
                <div class="col-md-11" id="info-card">
                        Pagamentos online de <strong>valores altos</strong> e <strong>não habituais</strong> podem ser recusados pela <strong>área de segurança</strong> do <strong>banco emissor do cartão</strong>. Saiba mais <i class="fa fa-info-circle saiba-mais" data-toggle="popover" data-placement="top" data-content="Caso seu pagamento não seja aprovado <strong>entre em contato com o banco emissor</strong> (o telefone está em seu cartão de crédito), confirme que você deseja realizar o pagamento e tente novamente. Caso o problema persista entre em contato conosco no e-mail <u>contato@easyforpay.com.br</u>." data-html="true"></i>
                </div>
            </div>
			
        </form>
        
        <br clear="all">
		<br>
        <div class="row contato">
            <div class="col-md-3 col-xs-6">
                <a class="carrinho seta" href="<?= Url::to(['site/index']); ?>" style="display:block;">Continuar comprando</a>
            </div>
            <?php if (isset($carrinho->objeto) && $carrinho->objeto->totais->total > 0 && (isset($cartao->ultimos_digitos) || $pgtoConciliacao)) { ?>
                <div class="col-md-9  col-xs-6 text-right carrDest" align="right">
                    <input type="button" data-loading-text="Processando..." value="Autorizar pagamento" id="autorizar-pagamento" class="icon-next carrinho ">
                </div>
            <?php } 
            if(isset($carrinho->objeto) && $carrinho->objeto->totais->total == 0){?>
                <div class="col-md-9  col-xs-6 text-right carrDest" align="right">
                    <input type="button" data-loading-text="Processando..." value="Confirmar" id="autorizar-pagamento" class="icon-next carrinho ">
                </div>
            <?php } ?>
        </div>        
        <?php } ?>
    </div>	
</div>

<?php 
$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/carrinho.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/compra.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    $this->registerJs("deadline = setDataExpira('" . $session['tempoCarrinho'] . "'); initializeClock('clockdiv', deadline);");
}
$this->registerJs("
    mostrarSaldoUsuario('".Url::to(['saldo-usuario'])."');
    ",\yii\web\View::POS_READY);
?>
<?php
namespace app\modules\admin\models;

use yii\base\Model;
use app\models\ApiCarrinho;
use app\models\ApiAgenda;
use app\modules\admin\models\ApiIngresso;
use app\models\Mineiraocard;
use yii\base\Exception;

/**
 * Classe Model para Bilheteria - Easy for Pay - Mineirão
 * Responsável por criar regras de validação e tratar dados da API
 * obs.: parâmetros entre colchetes são opcionais
 */
class BilheteriaModel extends Model
{
    const SCENARIO_ATIVAR = 'ativar';
    const SCENARIO_CONSULTAR = 'consultar';
    
    public $qrcode_voucher;
    public $documento;
    public $localizar;
    public $comprador;
    public $idEvento;
    public $statusIngresso = [
        ''  => 'Escolha um status de ingresso',
        'R' => 'Reservado',
        'P' => 'Em produção',
        'N' => 'Enviado ao estádio', 
        'E' => 'Entregue ao estádio', 
        'A' => 'Associado ao mifare', 
        'L' => 'Liberado para bilheteria',
        'T' => 'Retirado pelo cliente',
        'I' => 'Inválido',
        'B' => 'Bloqueado',
        'S' => 'Segunda via solicitada',
        '2' => 'Segunda via recebida'
    ];
    public $status;
    public $lote;
    public $produto;

    /**
     * Validação 
     * qrcode_voucher e documento para ativação - ao menos um deles deve estar preenchido, qrcode_voucher deve ter um formato específico JSON/array; documento não pode estar vazio
     */
    public function rules()
    {
        return [
            ['qrcode_voucher', 'either', 'params' => ['other' => 'documento'], 'on' => self::SCENARIO_ATIVAR],
            ['qrcode_voucher', 'formatoVoucher'],
            ['documento', 'string', 'length' => ['min' => 1]],
            [['qrcode_voucher', 'documento', 'localizar', 'status', 'lote', 'produto', 'comprador'], 'safe'],
        ];
    }

    public function either($attr, $params)
    {
        $field1 = $this->getAttributeLabel($attr);
        $field2 = $this->getAttributeLabel($params['other']);
        if (empty($attr) || empty($this->{$params['other']})) {
            $this->addError($attr, "{$field1} ou {$field2} deve ser preenchido.");
            return false;
        }

        return true;
    }

    public function formatoVoucher($attr, $params)
    {
        if (!empty($this->qrcode_voucher)) {
            $obj=json_decode($this->qrcode_voucher);
            if(
                empty($obj) ||
                !isset($obj->codigo) ||
                !isset($obj->comprador) ||
                $obj->codigo == '' ||
                $obj->comprador == ''
                )
            $this->addError($attr, 'Formato de QR Code inválido');

            return false;
        }

        return true;
    }
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ATIVAR] = ['qrcode_voucher', 'documento', 'localizar'];
        $scenarios[self::SCENARIO_CONSULTAR] = ['qrcode_voucher', 'documento', 'status', 'lote', 'produto', 'idEvento'];
        return $scenarios;
    }

    /**
     * Iniciar a pesquisa da venda pelo voucher
    */
    public function pesquisa() 
    {
        if(!isset($this->localizar)) $this->addError('localizar', 'Informe uma das opções para localizar a venda');
        $vendaTmp = ($this->localizar == 1) 
          ? [$this->getVenda($this->qrcode_voucher)] 
          : $this->getVendasDocumento($this->documento);
        return !empty($vendaTmp) ? $vendaTmp : [];
    }

    /**
     * Mostrar dados de uma venda
     * @param string $qrcodeVoucher o código do voucher fornecido pelo cliente
     * @throws Exception se o código não estiver no padrão esperado ou se a venda não for localizada
     */
    public function getVenda($qrcodeVoucher='') 
    {
        $venda = [];
        if (!empty($qrcodeVoucher)) {
            $voucherdecode=base64_decode($qrcodeVoucher);
            $vendaArr=explode('|', $voucherdecode);
            if (!is_array($vendaArr) || count($vendaArr) < 3) {//qr-code do voucher fora do padrão
                return [];
            }
            $compra = (new ApiCarrinho)->consultarVendasVoucher($vendaArr);
            $this->documento=$vendaArr[2];
            if ($compra->successo == '1') {
                $carrinho = (new ApiCarrinho)->consultarCarrinho($compra->objeto[0]->id_carrinho);
                if ($carrinho->successo == '1') {
                    $venda = [
                        'voucher'=>$vendaArr,
                        'objeto' => $compra->objeto[0],
                        'carrinho' => $carrinho,
                        'comprador' => $compra->objeto[0]->vendas->comprador->documento,
                        'detalhes' => $this->getDetalhesVenda($compra->objeto[0],$carrinho)
                    ];
                } 
            }
        }
        return $venda;
    }

    /**
     * pegar detalhes dos produtos de uma venda 
     * A partir do código da venda, definir qual lote será passado ao objeto de pesquisa dos ingressos.
     * Fazer uma chamada a ingresso/reservar passando o objeto de pesquisa
     * O objeto de retorno da chamada ingresso/reservar irá mostrar a situação dos ingressos
     * @param mixed $objCompra o objeto retornado após consultado
     * @param mixed $carrinho o objeto carrinho que traz os produtos da venda
     * @return 
     * @todo lançar exceções
     */
    public function getDetalhesVenda($objCompra, $carrinho){
        if (!isset($carrinho->objeto->produtos) || empty($carrinho->objeto->produtos))
            return;
        $detalhe=[];
        foreach ($carrinho->objeto->produtos as $index => $produto) {
            $agenda = new ApiAgenda();
            
            foreach ($produto->tags as $index_tag => $tag) {
                if ($tag->nome == 'matriz') {
                    $evento = $agenda->buscarEvento(json_encode(['tags' => [['nome' => 'apelido', 'valor' => $tag->valor]]]));
                }
            }
            $idEvento='';
            $nomeEvento='';
            $banner='';
            if($evento->successo==1){
                $nomeEvento=$evento->objeto[0]->nome;
                $idEvento=$evento->objeto[0]->codigo;
                foreach ($evento->objeto[0]->anexos as $index_anexo => $anexo) {
                    if ($anexo->nome == 'bannervitrine') {
                        $banner = $anexo->url;
                    }
                }
            }

            $detalhe[]=[
                'evento'=>['id'=>$idEvento,'nome'=>$nomeEvento,'banner'=>$banner],
                'produto'=>$produto,
                'mineiraoCard'=>Mineiraocard::ocupanteIngresso($carrinho,$produto)
            ];
        }
        foreach($detalhe as $k=>$v){
            if(isset($v['mineiraoCard'],$v['mineiraoCard']->doc,$v['mineiraoCard']->doc->valor) && $v['mineiraoCard']->doc->valor==$this->documento)
            {
                $out = array_splice($detalhe, $k, 1);
                array_splice($detalhe, 0, 0, $out);
            }  
        }
        return $detalhe;
    }

    /**
     * Ordenar o array "detalhes da compra" pelo documento pesquisado
    */
    private function docPesquisadoDetalhe($a, $b){
        return $a['mineiraoCard']->doc->valor==$this->documento
            ? 1
            : (
                $b['mineiraoCard']->doc->valor==$this->documento
                ?1 :-1
              );
    }

    /**
     * Buscar as vendas a partir de um documento pelas tags do carrinho e pelo comprador
     * @param string $documento o documento apresentado pelo cliente a ser pesquisado
     * @return array as vendas encontradas de todos os carrinhos
     */
    public function getVendasDocumento($documento='') 
    {
        $venda=[];
        $codVendasEncontradas=[];
        if (!empty($documento)) {
            $documento = preg_replace('/(\W)/','', $documento);
            $apiCarrinho = new ApiCarrinho;
            $compras = $apiCarrinho->consultarVendasFiltro(['comprador' => $documento]);
            if ($compras->successo == '1') {
                foreach ($compras->objeto as $vendaObj) {
                    if ($vendaObj->vendas->codigo_status != 1)
                        continue;
                    if (in_array($vendaObj->vendas->codigo, $codVendasEncontradas))
                        continue;
                    $venda[] = [
                        'objeto' => $vendaObj,
                        'carrinho' => $vendaObj->id_carrinho,
                        'comprador' => $vendaObj->vendas->comprador->documento,
                        'detalhes' => $this->getDetalhesVenda($vendaObj, $apiCarrinho->consultarCarrinho($vendaObj->id_carrinho))
                    ];
                    $codVendasEncontradas[] = $vendaObj->vendas->codigo;
                }
            }
            $carrinhos = $apiCarrinho->consultarCarrinhoTags([['nome'=>'%_documento', 'valor'=>$documento]]);
            if ($carrinhos->successo == '1') {
              foreach($carrinhos->objeto as $carrinho){
                  $objCarrinho=$apiCarrinho->consultarCarrinho($carrinho->id);
                  if($objCarrinho->successo=='1' && isset($objCarrinho->objeto->codigo_cobranca)){
                    $vendaFiltro=$apiCarrinho->consultarVendasFiltro(['codigo'=>$objCarrinho->objeto->codigo_cobranca]);
                    if($vendaFiltro->successo=='1'){
                      foreach($vendaFiltro->objeto as $vendaObj){
                        if($vendaObj->vendas->codigo_status!=1) continue;
                        if(in_array($vendaObj->vendas->codigo, $codVendasEncontradas)) continue;
                        $venda[] = [
                          'objeto' => $vendaObj,
                          'carrinho' => $carrinho->id,
                          'comprador' => $vendaObj->vendas->comprador->documento,
                          'detalhes' => $this->getDetalhesVenda($vendaObj,$objCarrinho)
                        ];
                        $codVendasEncontradas[]=$vendaObj->vendas->codigo;
                      }
                    }
                  }
              } 
            }            
        }
        return $venda;
    }

    /**
     * Habilitar o botão de ingresso com mensagem adicional.
     * @todo Pesquisar se já tem o documento ativado para este evento
     * @param string $docClienteVoucher o documento que consta no voucher ou apresentado pelo cliente
     * @param string $docComprador o documento do comprador retornado pela pesquisa da compra
     * @param string $docOcupante o documento do ocupante salvo na tag do carrinho
     * @param integer $evento ID do evento. Verificar se já foi ativado para este cliente
    */
    public function botaoIngresso($codigoVenda, $docClienteVoucher, $docComprador, $docOcupante, $evento) {
        $checkLiberado = $this->checkStatus($docOcupante, $evento, $codigoVenda);
        if (isset($checkLiberado->status)) {
            if (in_array($checkLiberado->status, ['R', 'P', 'N', 'E', 'A'])) {
                return ['mostrar' => false, 'msg' => 'O cartão está sendo produzido. Entregar avulso.', 'tipo' => 'info', 'ativo' => false, 'qrcode' => $checkLiberado->qrcode];
            } else if ($checkLiberado->status == 'L') {
                return ['mostrar' => true, 'msg' => 'Cartão disponível. Clique em "INGRESSO" para entregá-lo.', 'tipo' => 'success', 'ativo' => false, 'status' => $checkLiberado->status, 'qrcode' => $checkLiberado->qrcode];
            } else if ($checkLiberado->status == 'T') {
                return ['mostrar' => false, 'msg' => 'O cartão foi retirado por '.$checkLiberado->retirado_nome.' ('.$checkLiberado->retirado_documento.') em '.date_create_from_format('Y-m-d H:i:s', $checkLiberado->data_retirado)->format('d/m/Y H:i:s'), 'tipo' => 'success', 'ativo' => true, 'qrcode' => $checkLiberado->qrcode];
            } else if ($checkLiberado->status == 'I') {
                return ['mostrar' => false, 'msg' => 'O cartão foi invalidado. Motivo: ' . $checkLiberado->motivo_invalidado, 'tipo' => 'danger', 'ativo' => false, 'qrcode' => $checkLiberado->qrcode];
            } else if ($checkLiberado->status == 'B') {
                return ['mostrar' => false, 'msg' => 'O cartão está bloqueado', 'tipo' => 'danger', 'ativo' => false, 'qrcode' => $checkLiberado->qrcode];
            } else if ($checkLiberado->status == 'S') {
                return ['mostrar' => false, 'msg' => 'Foi solicitada uma segunda via do cartão', 'tipo' => 'warning', 'ativo' => false, 'qrcode' => $checkLiberado->qrcode];
            } else if ($checkLiberado->status == '2') {
                return ['mostrar' => false, 'msg' => 'A segunda via do cartão foi recebida mas ainda não está liberada', 'tipo' => 'warning', 'ativo' => false, 'qrcode' => $checkLiberado->qrcode];
            }
        }
        return ['mostrar' => false, 'msg' => 'A ser produzido. Entregar avulso.', 'tipo' => 'info', 'ativo' => false];
    }

    /**
     * Verificar o ingresso do ocupante com seus dados
     * @param string $doc Documento do ocupante
     * @param integer $evento ID do evento
     * @param integer $codigo_venda ID da venda
     */
    public function checkStatus($doc, $evento, $codigo_venda) {
        $consulta = (new ApiIngresso)->consultar([
            'id_agenda' => $evento,
            'documento' => $doc,
            'codigo_venda' => $codigo_venda
        ]);
        if ($consulta->successo == '1' && is_array($consulta->objeto)) {
            return $consulta->objeto[count($consulta->objeto)-1];
        } else {
            return false;
        }
    }

    /**
     * Lista de lugares e ocupantes a partir de uma lista de vendas
     */
    public function infoOcupantes($vendas) {
        $produtos = [];
        if (count($vendas) > 0) {
            foreach ($vendas as $venda) {
                $detalhes = $this->getDetalhesVenda($venda, (new ApiCarrinho)->consultarCarrinho($venda['carrinho']));
                if (count($detalhes) > 0) {
                    foreach ($detalhes as $detalhe) {
                        $produtos[] = ['produto' => $detalhe['produto'], 'ocupante' => $detalhe['mineiraoCard']];
                    }
                }
            }
        }
        return $produtos;
    }

}
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Produto - Cadastrar' . (isset($matriz) && $matriz == true ? ' (matriz)' : '');
?>

<div class="container">
	<div class="row">
		<h2 class="tituloGeral"><i class="fa fa-clone"></i> Produto - Cadastrar <?= isset($matriz) && $matriz == true ? ' (matriz)' : ''; ?></h2>
	</div>

	<?php
	$form = ActiveForm::begin(['id' => 'produto-form']); ?>

	<?= $form->field($model, 'id_agenda')->dropDownList(yii\helpers\ArrayHelper::map($eventos, 'id', 'data', 'evento')) ?>

	<?= $form->field($model, 'nome') ?>

	<?= $form->field($model, 'descricao')->textarea(['rows' =>5]) ?>

	<?= $form->field($model, 'valor') ?>

	<?= $form->field($model, 'descontoFixo') ?>

	<?= $form->field($model, 'descontoPercentual') ?>

	<?= $form->field($model, 'ativo') ?>

	<div class="form-group">
		<?= Html::submitButton('Cadastrar', ['class' => 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
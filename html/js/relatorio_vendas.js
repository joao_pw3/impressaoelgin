var codigo;
var meioPagto;
var conciliador;

$(document).ready(function () {
   
    $(document).on('click', '.estorno', function () {
        codigo = $(this).attr('data-estorno');
        unidade = $(this).attr('data-unidade');
        $('#form-estorno #codigo').val(codigo);
        $('#div-codigo').html(codigo);
        $('#unidade').val(unidade);
        $('#modal-venda').modal('hide');
        $('#modal-estorno').modal('show');
    });
    
    $('#btn-confirmar').click(function () {
        if ($('#form-estorno #codigo').val() == '') {
            showMsg('Escolha uma venda para efetuar o estorno.');
            return false;
        }    
        if ($('#motivo').val().length < 10) {
            showMsg('Preencha o motivo do estorno.');
            return false;
        }
        $('#modal-estorno').toggle();
        if(conciliador!='Especie')
            estorno(conciliador);
        else 
            estornar();
    });
    
    $('#btn-cancel').click(function () {
        $('#modal-estorno').modal('hide');
    });
    
    $('.detalhes').click(function() {
        meioPagto = $(this).attr('data-meio-pagto');
        conciliador=$(this).data('conciliador');
        doAjax ($('#url-detalhes').val(), 'POST', {'id_carrinho': $(this).attr('data-detalhe')}, 'JSON', 5000, 'detalheOK', 'detalheErr');
    }); 
    
    $('#btn-print-detalhes').click(function () {
        var conteudo = $('#tabela-vendas-transacoes').clone(true);
        conteudo.find('table th:last-child').remove();
        conteudo.find('table td:last-child').remove();
        printWindow(conteudo.html());
    });

    $(document).on('click', '.btn-estorno-impressao', function () {
        var codigo=$(this).data('codigo');
        console.log(codigo);
        var conteudo=$('#table-caixa [data-id="'+codigo+'"]').children().last().clone(true);
        conteudo=window.atob(conteudo.html());
        printWindow('<pre>'+conteudo+'</pre>');
    });

});

function estornar(data={}) {
    $("#form-estorno #unidade").after('<input type="hidden" value="'+window.btoa(JSON.stringify(data))+'" name="dataCappta" />');
    doAjax ($('#url-estorno').val(), 'POST', $('#form-estorno').serialize(), 'JSON', 5000, 'estornoOK', 'estornoErr');
}

function estornoOK(data) {
    showMsg('Estorno efetuado com sucesso. Acesse novamente o código para impressão, caso precise.');
    reloadPage();
}

function estornoErr(data) {
    showMsg('Erro ao fazer estorno: ' + data.erro.mensagem);
}

function detalheOK(data) {
    var html = 'Código: ' + data.objeto.codigo_cobranca + ' - Status: ' + data.objeto.cobranca.status.descricao + '<hr>';
    var dataCompra = data.objeto.cobranca.data.split(' ');
    var dataHoje = new Date().toISOString().slice(0, 10);
    var utilizado = false;
    var arrayProduto = [];
    var campos = ['nome', 'documento', 'email', 'telefone'];
    var ocupantes = Tags(data.objeto.tags);
    
    dataCompra = dataCompra[0].split('/');
    dataCompra = dataCompra[2] + '-' + dataCompra[1] + '-' + dataCompra[0];
    
    html += "<div class='detalhes-bilheteria'>";
    $.each(data.objeto.produtos, function (i, el) {
        var cupom = typeof el.cupom !== 'undefined' && typeof el.cupom.id !== 'undefined' ? '_' + el.cupom.id : '';
        var produtoCupom = el.codigo + cupom;
        if (typeof arrayProduto[produtoCupom] == 'undefined') {
            arrayProduto[produtoCupom] = 1;
        }
        $.each(el.qrcodes, function (i1, el1) {
            // evento
            html += 'Evento: ' + el.agenda.nome + ' - ' + el.agenda.horario + '<br>';
            // assento
            html += 'Assento: ' + el.descricao + '<br>';
            // valor
            html += 'Ingresso: ' + (typeof el.cupom !== 'undefined' ? el.cupom.nome : 'Inteira') + ' - R$ ' + el.valor_liquido.replace('.', ',') + '<br>';
            // teatro
            html += 'Local: ' + data.objeto.cobranca.vendedor.nome + '<br>';
            var strOcupante = '';
            $.each(campos, function (i2, el2) {
                var keyOcupante = ocupantes[produtoCupom + '_' + arrayProduto[produtoCupom] + '_' + el2] != '' ? ocupantes[produtoCupom + '_' + arrayProduto[produtoCupom] + '_' + el2] : false;
                if (keyOcupante !== false && typeof keyOcupante !== 'undefined') {
                    strOcupante += el2 + ' : ' + keyOcupante + '<br/>';
                }
            });
            arrayProduto[produtoCupom] += arrayProduto[produtoCupom];
            html += 'Ocupante: ' + strOcupante + '<hr>';
        });
    });
    html += '</div>';

    if (utilizado == true){
        html += 'Estorno indisponível: esta compra possui um ou mais vouchers utilizados.';
    } else if (data.objeto.cobranca.status.descricao == 'Estornado') {
        html += 'Estorno efetuado: ' + data.objeto.cobranca.movimento.data + '<br />Motivo: ' + data.objeto.cobranca.movimento.motivo+'<br />';
        if (meioPagto != 'Espécie') {
            html += '<button class="btn btn-primary btn-estorno-impressao" data-codigo="' + data.objeto.codigo_cobranca + '">Impressão recibo</button>';
        }
    } else if (data.objeto.cobranca.status.descricao == 'Autorizado' && (((meioPagto == 'Cartão de débito' || meioPagto == 'Cartão de crédito') && 
        dataCompra == dataHoje) || meioPagto == 'Espécie') && utilizado == false) { 
        if(data.objeto.cobranca.id_conciliacao == 'Especie' || fnEstorno.desabilitado===false) {
            html += '<button class="btn btn-primary estorno" data-estorno="' + data.objeto.codigo_cobranca + '" data-unidade="' + data.objeto.cobranca.vendedor.documento + '">estornar venda</button>';
        } else {
            html += '<p class="alert alert-danger">' + fnEstorno.error.reason + ' (codigo:' + fnEstorno.error.reasonCode + ')</p>';
        }
    } else {
        html += '* Estorno indisponível';
    }
    $('#modal-venda .modal-body').html(html);
    $('#modal-venda').modal('toggle');
}

function detalheErr (data) {
    showMsg('Erro ao consultar informações da venda: ' + data.erro.mensagem);
}

function Tags(tags) {
    var obj = [];
    $.each(tags, function(i, el) {
        obj[el.nome] = el.valor;
    });
    return obj;
}
<?php
use app\models\ApiProduto;
use yii\helpers\Html;
use yii\helpers\Url;

$session = Yii::$app->session;
$session->open(); 
?>

<input type="hidden" id="url-add-cart" value="<?= Url::to(['site/add-carrinho']); ?>">
<input type="hidden" id="url-del-cart" value="<?= Url::to(['site/del-carrinho']); ?>">
<input type="hidden" id="url-resumo" value="<?= Url::to(['site/resumo']); ?>">
<input type="hidden" id="url-visitante" value="<?= Url::to(['site/visitante']); ?>">
<input type="hidden" id="user" value="<?= isset($session['user_token']) && !empty($session['user_token']) ? 1 : 0; ?>">
<input type="hidden" id="documento" name="documento" value="<?= $session['documento']; ?>">
    
<div class="col-md-8 col-sm-12 col-xs-12 nopadding">
    <div id="mapaVermelho">
        <div class="titArea">
			<div class="celTabLeft">
				<h4>Embaixada Mineirão - Setor Vermelho</h4>
				Escolha o BLOCO que você deseja comprar (passe o mouse sobre o mapa e clique para selecionar).<br><br>
			</div>
			<div class="celTabRight">
				<?= Html::img($evento->anexos['bannervitrine']); ?>
			</div>
        </div>

        <div class="fundoEstadio">
            <div class="campoDiv">
                <?= Html::img('@web/images/campo2.jpg', ["class" => "campoP"]); ?>
            </div>
            <?php
            $mapa='<path rel="14292" class="bloqueado bloco" id="bloco_123" d="M569 52c-42,-15 -85,-32 -129,-50l-170 259c1,0 2,1 2,1 5,3 10,5 15,8 51,12 102,23 153,32l128 -250z"/><path rel="14293" class="bloqueado bloco" id="bloco_124" d="M698 94c-42,-13 -85,-27 -129,-43l-128 250c10,2 21,4 31,6 14,2 28,5 41,7 11,2 23,4 34,6 2,0 5,1 7,1 14,2 27,4 41,6l13 2 91 -235z"/><path rel="14294" class="bloqueado bloco" id="bloco_125" d="M827 129c-42,-10 -86,-21 -130,-34l-91 235c9,1 18,3 27,4 13,2 27,4 40,5 13,2 26,3 40,5 13,2 26,3 39,4l0 0 15 2 59 -221z"/><path rel="14295" class="bloqueado bloco" id="bloco_126" d="M959 153c-43,-6 -87,-14 -131,-24l-59 221c8,1 16,2 24,2 13,1 26,2 39,3 13,1 26,2 39,3 13,1 26,2 39,3l18 1 33 -209z"/><path rel="14296" class="bloqueado bloco" id="bloco_127" d="M1091 165c-44,-2 -88,-6 -133,-12l-33 209 21 1c13,1 26,1 38,2 13,1 25,1 38,1 1,0 1,0 2,0l0 -97 0 -3 3 0 58 0 5 -101z"/><path rel="14298" class="bloqueado bloco" id="bloco_129" d="M1152 167c-20,0 -41,0 -61,-1l-5 101 66 0 78 0 -6 -101c-24,1 -48,2 -72,2z"/><path rel="14300" class="bloqueado bloco" id="bloco_131" d="M1357 151c-45,7 -89,11 -133,13l6 101 59 0 3 0 0 3 0 96c12,0 24,-1 36,-2 13,-1 25,-1 38,-2l26 -2 -34 -208z"/><path rel="14301" class="bloqueado bloco" id="bloco_132" d="M1488 126c-44,10 -88,19 -131,25l34 208 13 -1c13,-1 26,-2 39,-3 13,-1 26,-2 39,-3 13,-1 26,-2 39,-4 10,-1 19,-2 29,-3l-61 -219z"/><path rel="14302" class="bloqueado bloco" id="bloco_133" d="M1618 91c-44,14 -87,25 -130,35l61 219 10 -1c13,-1 26,-3 39,-5 13,-2 26,-3 40,-5 13,-2 27,-4 40,-6 11,-2 22,-3 32,-5l-92 -230z"/><path rel="14303" class="bloqueado bloco" id="bloco_134" d="M1747 48c-44,16 -87,31 -129,43l92 233 8 -1c13,-2 27,-4 40,-6 14,-2 27,-5 41,-7 13,-2 26,-5 39,-7 13,-2 26,-5 39,-7l-130 -245z"/><path rel="14304" class="bloqueado bloco" id="bloco_135" d="M1866 1c-41,17 -80,32 -119,46l130 247c53,-10 106,-22 159,-35l-170 -255z"/><path rel="14305" class="bloqueado camarote" id="camarote_C237" d="M440 302c-51,-10 -102,-20 -153,-32 48,25 99,47 151,68 5,2 10,4 16,6l18 -36c-10,-2 -21,-4 -31,-6z"/><path rel="14306" class="bloqueado camarote" id="camarote_C238" d="M512 315c-14,-2 -28,-5 -41,-7l-18 36c13,5 26,10 39,14l20 -43z"/><path rel="14307" class="bloqueado camarote" id="camarote_C239" d="M553 321c-14,-2 -27,-4 -41,-7l-20 43c13,5 26,9 39,13l21 -50z"/><path rel="14308" class="bloqueado camarote" id="camarote_C240" d="M594 328c-14,-2 -27,-4 -41,-6l-21 50c13,4 26,8 40,13l22 -56z"/><path rel="14309" class="bloqueado camarote" id="camarote_C241" d="M634 333c-9,-1 -18,-3 -27,-4 -4,-1 -9,-1 -13,-2l-22 56c13,4 26,8 39,11 0,0 0,0 1,0l23 -62z"/><path rel="14310" class="bloqueado camarote" id="camarote_C242" d="M674 338c-13,-2 -27,-3 -40,-5l-23 62c13,4 27,7 40,11l23 -67z"/><path rel="14311" class="bloqueado camarote" id="camarote_C243" d="M714 343c-13,-2 -26,-3 -40,-5l-23 67c13,3 27,7 40,10l22 -72z"/><path rel="14312" class="bloqueado camarote" id="camarote_C244" d="M753 348c-13,-1 -26,-3 -39,-4l-22 72c13,3 27,6 40,9l21 -77z"/><path rel="14313" class="bloqueado camarote" id="camarote_C245" d="M768 349c-5,-1 -10,-1 -15,-2l-21 77c13,3 27,5 40,8l20 -81c-8,-1 -16,-2 -24,-2z"/><path rel="14314" class="bloqueado camarote" id="camarote_C246" d="M831 355c-13,-1 -26,-2 -39,-3l-20 81c5,1 10,2 16,3 8,1 16,3 25,4l19 -85z"/><path rel="14315" class="bloqueado camarote" id="camarote_C247" d="M870 358c-13,-1 -26,-2 -39,-3l-19 85c13,2 27,4 40,6l17 -88z"/><path rel="14316" class="bloqueado camarote" id="camarote_C248" d="M908 361c-13,-1 -26,-2 -39,-3l-17 88c13,2 27,4 40,6l15 -91z"/><path rel="14317" class="bloqueado camarote" id="camarote_C249" d="M947 363c-7,0 -14,-1 -21,-1 -6,0 -12,-1 -18,-1l-15 91c13,2 27,3 41,5l13 -94z"/><path rel="14318" class="bloqueado camarote" id="camarote_C250" d="M985 365c-13,-1 -26,-1 -38,-2l-13 94c11,1 22,2 33,3 3,0 5,0 8,1l11 -96z"/><path rel="14319" class="bloqueado camarote" id="camarote_C251" d="M1023 366c-13,0 -25,-1 -38,-1l-11 96c13,1 27,2 41,3l9 -97z"/><path rel="14320" class="bloqueado camarote" id="camarote_C252" d="M1061 370c-11,0 -22,-1 -33,-1l-3 0 0 -3 0 0c-1,0 -1,0 -2,0l-9 97c13,1 27,2 41,2l6 -96z"/><path rel="14321" class="bloqueado camarote" id="camarote_C253" d="M1099 371c-13,0 -26,0 -38,-1l-6 96c13,1 27,1 41,1l4 -96z"/><path rel="14322" class="bloqueado camarote" id="camarote_C254" d="M1137 371c-13,0 -26,0 -38,0l-4 96c13,0 27,1 41,1l1 -97z"/><path rel="14323" class="bloqueado camarote" id="camarote_C255" d="M1152 468l0 0zm1 0l0 0zm22 -97c-7,0 -14,0 -21,0 -1,0 -1,0 -2,0 -5,0 -10,0 -15,0l-1 97c5,0 10,0 16,0 0,0 0,0 0,0l0 0 0 0c8,0 16,0 24,0l-2 -97z"/><path rel="14324" class="bloqueado camarote" id="camarote_C256" d="M1213 370c-13,0 -25,1 -38,1l2 97c14,0 27,0 41,-1l-4 -97z"/><path rel="14325" class="bloqueado camarote" id="camarote_C257" d="M1251 369c-13,0 -26,1 -38,1l4 97c14,0 27,-1 41,-2l-7 -96z"/><path rel="14326" class="bloqueado camarote" id="camarote_C258" d="M1289 368c-13,0 -25,1 -37,1l7 96c14,-1 27,-1 41,-2l-9 -95 -1 0z"/><path rel="14327" class="bloqueado camarote" id="camarote_C259" d="M1327 363c-12,1 -24,1 -36,2l0 0 0 3 -2 0 9 95c14,-1 27,-2 40,-3 0,0 0,0 0,0l-12 -97z"/><path rel="14328" class="bloqueado camarote" id="camarote_C260" d="M1366 361c-13,1 -25,1 -38,2l12 97c14,-1 27,-3 41,-4l-14 -95z"/><path rel="14329" class="bloqueado camarote" id="camarote_C261" d="M1391 359c-9,1 -17,1 -26,2l14 95c14,-1 27,-3 40,-5l-16 -92c-4,0 -9,1 -13,1z"/><path rel="14330" class="bloqueado camarote" id="camarote_C262" d="M1443 356c-13,1 -26,2 -39,3l16 92c14,-2 27,-4 40,-6l-18 -90z"/><path rel="14331" class="bloqueado camarote" id="camarote_C263" d="M1481 352c-13,1 -26,2 -39,3l18 90c14,-2 27,-4 40,-7l-20 -86z"/><path rel="14332" class="bloqueado camarote" id="camarote_C264" d="M1520 348c-13,1 -26,3 -39,4l20 86c6,-1 11,-2 17,-3 8,-1 16,-3 24,-4l-21 -83z"/><path rel="14334" class="bloqueado camarote" id="camarote_C266" d="M1549 345c-10,1 -19,2 -29,3l21 83c13,-3 27,-5 40,-8l-22 -79c-3,0 -7,1 -10,1z"/><path rel="14335" class="bloqueado camarote" id="camarote_C267" d="M1600 340c-14,2 -27,3 -41,5l23 79c14,-3 28,-6 42,-9l-24 -74z"/><path rel="14336" class="bloqueado camarote" id="camarote_C268" d="M1638 334c-13,2 -26,3 -40,5l0 0c1,0 1,0 2,0l24 74c13,-3 25,-6 38,-9l-24 -69z"/><path rel="14337" class="bloqueado camarote" id="camarote_C269" d="M1678 329c-13,2 -27,4 -40,6l24 69c11,-3 22,-6 33,-9 2,-1 5,-1 7,-2l-24 -64z"/><path rel="14338" class="bloqueado camarote" id="camarote_C270" d="M1710 324c-11,2 -22,3 -32,5l24 64c13,-4 27,-8 40,-12l-23 -58c-3,0 -5,1 -8,1z"/><path rel="14339" class="bloqueado camarote" id="camarote_C271" d="M1758 316c-13,2 -27,4 -40,6l23 58c13,-4 26,-8 40,-13l-23 -52z"/><path rel="14340" class="bloqueado camarote" id="camarote_C272" d="M1799 309c-14,2 -27,5 -41,7l23 52c13,-4 26,-9 39,-14l-21 -45z"/><path rel="14341" class="bloqueado camarote" id="camarote_C273" d="M1838 302c-13,2 -26,5 -39,7l21 45c12,-4 25,-9 37,-14l-19 -39z"/><path rel="14342" class="bloqueado camarote" id="camarote_C274" d="M1877 295c-13,3 -26,5 -39,7l19 39c4,-1 7,-3 10,-4 58,-23 113,-48 166,-76 1,0 2,-1 2,-1 -53,13 -106,24 -159,35z"/>                <path class="fil4 str1 arquibancada" d="M2 323l110 -158c269,187 636,303 1040,303l0 0 0 0 0 0 0 0c404,0 770,-116 1040,-303l109 157c-800,473 -1566,453 -2300,2z"/>
                ';
            foreach($setores as $a){
                $modelProduto=new ApiProduto;
                if(!isset($a->anexos)) continue;
                $setor=$a->tags['tipo'].'_'.($a->tags['tipo']=='bloco' ?$a->tags['bloco'] :$a->tags['camarote']);
                $anexo=$modelProduto->findAnexo($a->anexos,'svgmapa');
                if($this->context->setor!=null && $this->context->setor==$setor)
                    $anexo=str_replace('class="', 'class="selecionado ', $anexo);
                if($a->ativo==0)
                    $anexo=str_replace('class="', 'class="bloqueado ', $anexo);
                $anexo=str_replace('<path ', '<path rel="'.$a->codigo.'" ', $anexo);
                $mapa.=$anexo;
                $totais=$a->estoque;
                if(isset($a->tags['bloco'])){
                    $modelConsulta=new ApiProduto;
                    $consultaTotais=$modelConsulta->consultarTotaisEstoqueProduto([
                        'tags'=>[
                           [
                                "nome"=>"matriz",
                                "valor"=>$a->tags['matriz']
                            ],
                        
                            [
                                "nome"=>"tipo",
                                "valor"=>"assento"
                            ],
                        
                            [
                                "nome"=>"bloco",
                                "valor"=>$a->tags['bloco']
                            ]
                    
                        ]
                    ]);
                    if($consultaTotais->successo==1)
                        $totais=$consultaTotais->objeto;
                }                
                echo Html::input('hidden','oferecido',number_format(floatval($totais->oferecido), 0),['id'=>'oferecido_'.$setor]);
                echo Html::input('hidden','disponivel',number_format(floatval($totais->disponivel), 0),['id'=>'disponivel_'.$setor]);
            }
            ?>
            <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="100%" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd; margin-left:25px;"
                viewBox="0 0 2480 707"
                xmlns:xlink="http://www.w3.org/1999/xlink" id="lugares" class="leste">
                <?php echo $mapa ?>
                <path class="fil4 str1 arquibancada" d="M2 323l110 -158c269,187 636,303 1040,303l0 0 0 0 0 0 0 0c404,0 770,-116 1040,-303l109 157c-800,473 -1566,453 -2300,2z"/>
                <path class="fil4 str1 arquibancada" d="M1153 468l0 201"/>
                <path class="fil4 str1 arquibancada" d="M179 424l93 -162"/>
                <path class="fil4 str1 arquibancada" d="M362 509l76 -172"/>
                <path class="fil4 str1 arquibancada" d="M551 578l59 -183"/>
                <path class="fil4 str1 arquibancada" d="M746 629l41 -193"/>
                <path class="fil4 str1 arquibancada" d="M944 660l22 -200"/>
                <path class="fil4 str1 arquibancada" d="M1941 503l-74 -166"/>
                <path class="fil4 str1 arquibancada" d="M1752 572l-57 -177"/>
                <path class="fil4 str1 arquibancada" d="M1558 624l-40 -188"/>
                <path class="fil4 str1 arquibancada" d="M1360 657l-21 -197"/>
                <path class="fil4 str1 arquibancada" d="M2124 419l-90 -158"/>
            </svg>
        </div>
        
        <div class="mapa-assentos-leste" style="position:relative;" align="center">
            <?= Html::img('@web/images/bloco127.png', ['class' => 'hidden imgMapa', 'id' => 'assentos-127']); ?>
            <?= Html::img('@web/images/bloco128.png', ['class' => 'hidden imgMapa', 'id' => 'assentos-128']); ?>
			<?= Html::img('@web/images/bloco130.png', ['class' => 'hidden imgMapa', 'id' => 'assentos-130']); ?>
            <a href="javascript:;" id="closeImg" class="hidden btn btn-warning btn-sm" style="position:absolute;right:0px;top:0px;">Fechar mapa</a>
            <ul class="hidden" id="textInstru3">
                <li>1 - Clique no quadro a direita, em "SELECIONE UM PRODUTO" para escolher seu assento.</li>
                <li>2 - Logo em seguida clique no carrinho para adicionar o produto.</li>
                <li>3 - Selecione até 6 produtos por CPF por evento.</li>
            </ul>
        </div>
        
        <div class="titArea lesteInfo">
            <div><?= Html::img('@web/images/mapa_peq2.png', ["class" => "campoP2"]); ?></div>
            <div>
                <p>Está com dúvida de como fazer sua compra? <a href="<?= Url::to(['site/faq']); ?>">Clique Aqui.</a></p>
                <p>Embaixada Mineirão: você mais perto da história, 100% futebol. A Embaixada Mineirão é para quem vai ao campo pra sentir toda a emoção do futebol e não quer perder nenhum lance. Garanta logo sua cadeira em um dos melhores setores do estádio.</p>
            </div>
            <div>
                Vista do<br>Bloco 128<br>
                <i class="fa fa-camera tam30 flipHor" aria-hidden="true" onclick="vistaBL128()"></i>
            </div>
            <div>
                Vista do<br>Bloco 130<br>
                <i class="fa fa-camera tam30 flipHor" aria-hidden="true" onclick="vistaBL130()"></i>
            </div>
        </div>
    </div>
    <br>
</div>

<div class="col-md-4 col-sm-12 col-xs-12 about-right <?= (!isset($this->context->setor)) ? 'wow fadeInRight animated' : ''; ?> textosAprest3" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
    <div class="tabProd2 hidden" id="div-item">
        <form id="form-carrinho" method="post">
            <input type="hidden" name="id_evento" id="id_evento" value="<?= $evento->codigo; ?>">
            <input type="hidden" id="carrinho-codigo" name="carrinho-codigo">
            <input type="hidden" id="carrinho-area" name="carrinho-area">
            <input type="hidden" id="carrinho-bloco" name="carrinho-bloco">
            <input type="hidden" id="carrinho-desconto" name="carrinho-desconto">
            <table class="table table-condensed" id="table-item">
                <thead>
                    <tr>
                        <th align="center" width="50%">Assento</th>
                        <th align="center" width="35%">Preço unitário</th>
                        <th align="center" width="10%">Quantidade</th>
                        <th align="center" width="5%"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="tbl-assento" width="50%"></td>
                        <td width="35%"><input type="text" id="carrinho-preco-unitario" name="carrinho-preco-unitario" readonly></td>
                        <td width="10%"><input type="text" id="carrinho-quantidade" name="carrinho-quantidade" min="1" maxlength="3"></td>
                        <td width="5%" align="center" valign="middle"><i class="fa fa-cart-plus fa-2x plus-white" title="Adicionar ao seu carrinho"></i></td>
                    </tr>
                </tbody>
            </table>
            <span class="msg-carrinho">* Selecione o assento, a quantidade e clique em <i class="fa fa-cart-plus fa-2x"></i></span>
        </form>
    </div>
    <form method="post" id="form-bloco">
        <input type="hidden" name="id_evento" id="id_evento" value="<?= $evento->codigo; ?>">
        <div class="tabProd">
            <div><i class="fa fa-shopping-cart fa-2x"></i> Meu Carrinho:</div>
            <table class="table table-condensed table-stripped">
                <thead id="tbl-header" class="<?= empty($carrinho->objeto->produtos) ? 'hidden' : ''; ?>">
                    <tr>
                        <!-- <th width="15%">Área</th> -->
                        <th width="40%">Setor / Bloco</th>
                        <th width="10%">Itens</th>
                        <th width="30%">Subtotal</th>
                        <th width="5%"></th>
                    </tr>
                </thead>
                <tbody id="linhasLugares">
                <?php 
                // listando produtos do carrinho
                // o for serve também para criar array de checagem para funções JS (permitir somente os produtos que o usuário pode renovar)
                $blocosSelect=[];
                $assentosSelect=[];
                if (!empty($carrinho->objeto->produtos)) {  
                    foreach ($carrinho->objeto->produtos as $index => $produto) {
                        $bloco = '';
                        $area = '';
                        $assentosSelect[]=$produto->codigo;
                        if(isset($produto->tags)){
                            foreach ($produto->tags as $index_tag => $tag) {
                                if ($tag->nome == 'bloco') {
                                    $bloco = $tag->valor;
                                    $blocosSelect[]=$tag->valor;
                                }
                                if ($tag->nome == 'area') {
                                    $area = $tag->valor;
                                }
                            }
                        }
                    ?>
                    <tr id="tr-<?= $produto->codigo; ?>" class="linha-carrinho">
                        <!-- <td width="15%"><?= $area; ?></td> -->
                        <td width="40%"><?= $produto->nome; ?></td>
                        <td width="10%" align="right"><?= number_format($produto->unidades, 0); ?></td>
                        <td width="30%">
                            <?php if (floatval($produto->desconto->fixo) > 0) { ?>
                            R$ <?= number_format(($produto->valor - $produto->desconto->fixo) * $produto->unidades, 2, ',', ''); ?><br>
                            <span class="strikeDesconto">R$ <?= number_format($produto->valor * $produto->unidades, 2, ',', ''); ?></span>
                            <?php } else { ?>
                            R$ <?= number_format($produto->valor * $produto->unidades, 2, ',', ''); ?>
                            <?php } ?>
                        </td>
                        <td width="5%" align="center"><i class="fa fa-trash trash-item plus-red mouseMao" id="trash-<?= $produto->codigo; ?>" data-trash="<?= $produto->codigo; ?>" title="Remover este item do seu carrinho"></i></td>
                        <input type="hidden" name="unidades[]" value="<?= number_format($produto->unidades, 0); ?>" min="0" />
                        <input type="hidden" name="id[]" value="<?= $produto->codigo; ?>">
                        <input type="hidden" name="area[]" value="<?= $area; ?>">
                        <input type="hidden" name="setor[]" value="<?= $bloco; ?>">
                        <input type="hidden" name="preco_unitario[]" value="<?= $produto->valor; ?>">
                        <input type="hidden" name="desconto[]" value="<?= $produto->desconto->fixo; ?>">
                    </tr>                    
                    <?php 
                    } 
                } ?>
                </tbody>
            </table>
        </div>
     </form>   
    <p class="preenche">* Preenchimento obrigatório.</p>
    <div style="margin:0px auto;">
        <br>
        <span class="row contato carrDest" style="width:50%;">
            <input type="submit" value="Finalizar a Compra&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" id="finaliza-compra" class="icon-next carrinho">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="carrinho seta" href="<?= Url::to(['site/index']); ?>">Voltar à página de eventos</a>
        </span>
    </div>
</div>

<p id="infoSetor"></p>
<hr style="padding-top:25px;">
    
<?php $this->registerJs('
    $("#mapaVermelho #lugares .bloco, #mapaVermelho #lugares .camarote").mouseover(function(){
        if($(this).hasClass("bloqueado")) return;
        infoConteudo($(this),"Vermelho");
    });
    $("#mapaVermelho #lugares").mouseout(function(){
		if($(this).hasClass("bloqueado")) return;
        offInfoConteudo();
    });
    $("#mapaVermelho #lugares path").on("click",function(e){
        if($(this).hasClass("bloqueado")) return;
        $("#mapaVermelho #lugares path").removeClass("selecionado");
        if($(this).hasClass("selecionado")) e.preventDefault()
        $(this).addClass("selecionado");
        if($(this).hasClass("camarote")){
            var id=$(this).attr("rel");
            addProduto(id, $(this).attr("id"));
        }else{
            var id=$(this).attr("id");
            listaLugares(id);
        }
    });
', \yii\web\View::POS_END); ?>
<?php
use app\models\ApiProduto;
use yii\helpers\Html;
use yii\helpers\Url;

$session = Yii::$app->session;
$session->open(); 
?>

<input type="hidden" id="url-add-cart" value="<?= Url::to(['site/add-carrinho']); ?>">
<input type="hidden" id="url-del-cart" value="<?= Url::to(['site/del-carrinho']); ?>">
<input type="hidden" id="url-resumo" value="<?= Url::to(['site/resumo']); ?>">
<input type="hidden" id="url-visitante" value="<?= Url::to(['site/visitante']); ?>">
<input type="hidden" id="user" value="<?= isset($session['user_token']) && !empty($session['user_token']) ? 1 : 0; ?>">
    
<div class="col-xs-12">
    <div>
        <div class="titArea">
            <h4>Vendas interrompidas até 21h</h4>
            
        </div>
    </div>
    <br>
</div>
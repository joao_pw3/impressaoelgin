<?php
use yii\helpers\Url;
use app\modules\admin\assets\GraficaAsset;

GraficaAsset::register($this);

$this->title = 'ADM Estádio Mineirão :: Lote "liberado para bilheteria"';
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h2 class="tituloGeral"><i class="fa fa-clone"></i> Lote "liberado para bilheteria"</h2> 
        </div>
        <div class="col-xs-4">
            <form name="form-liberar" method="post" action="<?= Url::to(['ingressos/lote-liberar']); ?>">
                <label for="estadio_evento">Eventos</label>
                <select name="estadio_evento">
                    <?php foreach($eventos as $index => $evento) { ?>
                    <option value="<?= $evento->codigo ; ?>"<?= isset($codigo_evento) && $codigo_evento == $evento->codigo ? ' selected' : ''; ?>><?= $evento->nome; ?></option>
                    <?php } ?>
                </select>
                <input type="submit" class="btn btn-primary" value='Lotes de ingresso "associados ao mifare"'>
            </form>
        </div>
    </div>
    
    <hr style="margin-top: 10px;">
    
    <?php if (isset($lotes) && !empty($lotes)) { ?>
    <input type="hidden" id="url-lote-libera" value="<?= Url::to(['ingressos/lote-liberar']); ?>">
    <input type="hidden" id="url-status-libera" value="<?= Url::to(['ingressos/status-lote-liberar']); ?>">
    <table class="table table-responsive table-striped table-bordered">
    <thead class="table-active">
        <tr>
            <th colspan="4" scope="col" class="aliCentro">Lotes disponíveis para este evento</th>
        </tr>
        <tr>
            <th scope="col" class="aliCentro">Lote Easy For Pay</th>
            <th scope="col" class="aliCentro">Total de ingressos do lote</th>
            <th scope="col" class="aliCentro">Data de associação</th>
            <th scope="col" class="aliCentro">Dados dos ingressos</th>
        </tr>
    </thead>
    <tbody>
    <?php 
    foreach($lotes as $key => $lote) { 
        $date1 = date_create(date('Y-m-d', strtotime($lote['data'])));
        $date2 = date_create(date('Y-m-d', strtotime('now')));
        $diff = date_diff($date2, $date1);
    ?>
        <tr>
            <td><?= $key; ?></td>
            <td><?= $lote['total']; ?></td>
            <td><?= date('d/m/Y H:i', strtotime($lote['data'])) . ' (<span class="' . ($diff->days > 5 ? 'plus-red' : 'plus-green') . '">' . $diff->days . ' dias</span>)'; ?></td>
            <td class="aliCentro"><a href="javascript:;" class="ver-ingresso" id="<?= $key; ?>">Mostrar</a></td>
        </tr>
        <tr id="ingressos-<?= $key; ?>" class="hidden">
            <td colspan="3">
                <table class="table table-responsive table-striped table-bordered">
                    <thead>
                        <th scope="col" class="aliCentro table-info">Qrcode</th>
                        <th scope="col" class="aliCentro table-info">RFID</th>
                        <th scope="col" class="aliCentro table-info">Nome do ocupante</th>
                        <th scope="col" class="aliCentro table-info">Bloco</th>
                        <th scope="col" class="aliCentro table-info">Fileira</th>
                        <th scope="col" class="aliCentro table-info">Cadeira</th>
                    </thead>
                    <tbody>
                    <?php 
                    foreach($ingressos->objeto as $index => $ingresso) { 
                        if ($ingresso->lote_reserva == $key) { 
                            $infoProduto = explode(' ', $ingresso->produto);
                    ?>
                    <tr>
                        <td><?= isset($ingresso->qrcode) ? $ingresso->qrcode : '-'; ?></td>
                        <td><?= isset($ingresso->codigo) ? $ingresso->codigo : '-'; ?></td>
                        <td><?= isset($ingresso->ocupante_nome) ? $ingresso->ocupante_nome : '-'; ?></td>
                        <td><?= isset($infoProduto[3]) && !empty($infoProduto[3]) ? $infoProduto[3] : '-'; ?></td>
                        <td><?= isset($infoProduto[1]) && !empty($infoProduto[1]) ? substr($infoProduto[1], 0, 1) : '-'; ?></td>
                        <td><?= isset($infoProduto[1]) && !empty($infoProduto[1]) ? substr($infoProduto[1], 1, strlen($infoProduto[1])) : '-'; ?></td>
                    </tr>
                    <?php 
                        }
                    } 
                    ?>
                    <tr>
                        <td class="aliCentro" colspan="3">
                            <a href="<?= Url::to(['grafica/exportar-csv', 'lote' => $key, 'evento' => $codigo_evento, 'status' => 'A']); ?>" class="btn btn-primary">Gerar arquivo .csv</a><br>
                        </td>
                        <td colspan="3" align="center">
                            <button class="btn btn-primary btn-libera" data-lote="<?= $key; ?>" data-evento="<?= isset($codigo_evento) ? $codigo_evento : ''; ?>">Registrar lote como "liberado para bilheteria"</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>    
    <?php } ?>
    </tbody>
    </table>
    <?php } else if ($codigo_evento > 0 && empty($lote)){ ?> 
    <div class="row">
        <label>Não há ingressos disponíveis com estas condições.</label>
    </div>
    <?php } ?>
</div>
<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\WsCaixa;
use app\models\WsCarrinho;
use app\models\Financeiro;

/**
 * Classe Model para Caixa - Easy for Pay
 * Responsável por criar regras de validação e tratar dados da API
 * obs.: parâmetros entre colchetes são opcionais
 * Padrões do ending point entsai para o caixa:
 *      saida:1 + ajuste:0 = fechamento 
 *      saida:0 + ajuste:0 = abertura
 *      saida:1 + ajuste:1 = sangria 
 *      saida:0 + ajuste:1 = reforço
 */

class Caixa extends Model 
{
    public $user_id;
    public $senha;
    public $numero_cartao;
    public $relatorio;
    
    /**
     * Regras de validação. 
     */
    public function rules() {
        return [
        ];
    }

    /**
     * definição dos labels de atributos
     */
    public function attributeLabels() {
        return[
        ];
    }
    
    /**
     * Consulta de ações registradas por operador
     */
    public function lerAcaoCaixa($post=[])
    {
        $session = Yii::$app->session;
        $session->open();
        $api = new WsCaixa($session->get('user_token'));
        if (isset($post) && !empty($post)) {
            return $api->lerAcaoCaixa([
                'id' => $post['id'], 
                'dateStart' => isset($post['dateStart']) ? $post['dateStart'] : '', 
                'dateFinish' => isset($post['dateFinish']) ? $post['dateFinish'] : ''
            ]);
        }
        return $api->getApiError('Faltam parâmetros para consultar ações de caixa');
    }
    
    /**
     * Relatório resumido do caixa
     */
    public function resumoCaixa($post=[])
    {
        $session = Yii::$app->session;
        $session->open();
        $api = new WsCaixa($session->get('user_token'));
        if (isset($post) && !empty($post)) {
            $returnApi = $api->lerAcaoCaixa(['id' => $post['id']]);
            if ($returnApi->successo == '1' && $returnApi->objeto) {
                $arrayCaixa = [];
                foreach($returnApi->objeto as $index => $caixa) {
                    $obj = json_decode(base64_decode($caixa->objeto));
                    if ($caixa->saida == 1 && $caixa->ajuste == 0) { // fechamento
                        $arrayCaixa['fechamento']['valor'] = $obj->valor;
                        $arrayCaixa['fechamento']['data'] = $caixa->data;
                        break; //consultar o último movimento, desde o fechamento até a abertura e parar
                    } else if ($caixa->saida == 0 && $caixa->ajuste == 0) { // abertura
                        $arrayCaixa['abertura']['valor'] = $obj->valor;
                        $arrayCaixa['abertura']['data'] = $caixa->data;
                    } else if ($caixa->saida == 1 && $caixa->ajuste == 1) { // sangria
                        $arrayCaixa['sangria'] = isset($arrayCaixa['sangria']) ? $arrayCaixa['sangria'] + $obj->valor : $obj->valor;
                    } else if ($caixa->saida == 0 && $caixa->ajuste == 1) { // reforço
                        $arrayCaixa['reforço'] = isset($arrayCaixa['reforço']) ? $arrayCaixa['reforço'] + $obj->valor : $obj->valor;
                    } 
                }
                return (object)['successo' => '1', 'resumo' => $arrayCaixa];
            }
            return $api->getApiError('Falha ao buscar informações de caixa do operador.');
        }
        return $api->getApiError('Faltam parâmetros para buscar informações de caixa do operador');
    }
    
    /**
     * Buscar resumo de vendas de um operador
     * @param integer $operador Id do operador
     * @param type $dataInicio Data inicial
     * @param type $dataFim Data final
     */
    public function getVendasOperadorResumo($operador, $dataInicio='', $dataFim='')
    {
        $session = Yii::$app->session;
        $session->open();
        $apiCarrinho = new WsCarrinho($session['user_token']);
        $returnApi = $apiCarrinho->consultarVendaFiltro(['operador' => $operador, 'data_ini' => $dataInicio, 'data_fim' => $dataFim]);
        $arrayCaixa = [];
        if ($returnApi->successo == '1') {
            foreach($returnApi->objeto as $index => $venda) {
                $valor = isset($arrayCaixa[$venda->vendas->status][$venda->vendas->meio_de_pagamento->descricao]['valor']) ? $arrayCaixa[$venda->vendas->status][$venda->vendas->meio_de_pagamento->descricao]['valor'] : 0;
                $itens = isset($arrayCaixa[$venda->vendas->status][$venda->vendas->meio_de_pagamento->descricao]['unidades']) ? $arrayCaixa[$venda->vendas->status][$venda->vendas->meio_de_pagamento->descricao]['unidades'] : 0;
                $arrayCaixa[$venda->vendas->status][$venda->vendas->meio_de_pagamento->descricao] = [
                    'valor' => $valor + (float)$venda->vendas->valor,
                    'unidades' => $itens + (int)$venda->vendas->unidades
                ];
            }
            return (object)['successo' => '1', 'vendas' => $arrayCaixa];
        }
        return (object)['successo' => '0', 'vendas' => $arrayCaixa];
    }
    
    /**
     * Buscar vendas de um operador
     * @param integer $operador Id do operador
     * @param type $dataInicio Data inicial
     * @param type $dataFim Data final
     */
    public function getVendasOperador($operador, $dataInicio='', $dataFim='')
    {
        if ($operador > 0) {
            $session = Yii::$app->session;
            $session->open();
            $apiCarrinho = new WsCarrinho($session['user_token']);
            return $apiCarrinho->consultarVendaFiltro(['operador' => $operador, 'data_ini' => $dataInicio, 'data_fim' => $dataFim]);
        }
        return (object)['successo' => '0', 'erro' => ['mensagem' => 'Falha ao buscar vendas do operador']];
    }
    
    /**
     * Fazer operações de caixa (abrir / fechar caixa, registrar informações, etc)
     * @param type $post
     *      id: string (255) (id do caixa)
     *      saida: boolean (0 = false / 1 = true)
     *      objeto: base64_encode(<json>) - qualquer informação relacionada que deva ser registrada
     */
    public function fazerAcaoCaixa($post=[])
    {
        $session = Yii::$app->session;
        $session->open();
        $api = new WsCaixa($session->get('user_token'));
        if (isset($post) && !empty($post)) {
            return $api->fazerAcaoCaixa([
                'id' => 'caixa_' . $post['id_vendedor'],
                'saida' => (int)$post['saida'], 
                'objeto' => base64_encode(json_encode(['valor' => $post['valor']])),
                'ajuste' => (int)$post['ajuste']
            ]);
        }
        return $api->getApiError('Faltam parâmetros para registrar ações de caixa');
    }
    
    /**
     * Relatório de caixa - mix de informações de venda e movimentações do operador
     * @parameter string $dataInicial Início do período
     * @parameter string $dataFinal Fim do período
     */
    public function relatorioCaixa ($dataInicial, $dataFinal) 
    {
        if (!empty($dataInicial) && !empty($dataFinal)) {
            $session = Yii::$app->session;
            $session->open();
            $relatorio = [];
            $vendas = (new WsCarrinho($session->get('token_unidade')))->consultarVendaFiltro([
                'descricao' => 'Mineirão - Bilheteria', 
                'data_ini' => $dataInicial . ' 00:00:00', 
                'data_fim' => $dataFinal . ' 23:59:59'
            ]);
            if (!empty($vendas->objeto)) {
                foreach ($vendas->objeto as $index => $venda) {
                    $data = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $venda->vendas->data)));
                    $relatorio[$data . '_venda'][] = $venda;
                }
            }
            $caixa = $this->lerAcaoCaixa([
                'id' => 'caixa_' . Yii::$app->user->identity->cod_operador, 
                'dateStart' => str_replace('/', '-', $dataInicial), 
                'dateFinish' => str_replace('/', '-', $dataFinal)
            ]);
            if (!empty($caixa->objeto)) {
                foreach ($caixa->objeto as $index => $caixa) {
                    $data = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $caixa->data)));
                    $relatorio[$data . '_caixa'][] = $caixa;
                }
            }
            ksort($relatorio);
        } 
        return $relatorio;
    }
    
    /**
     * Calcular o saldo de caixa em espécie para um operador
     * @param integer $id Id do operador
     * @param array $caixa Informações do caixa do operador
     * @return int
     */
    public function getSaldoOperador($id, $caixa) 
    {
        $saldo = 0;
        if (!empty($id) && (isset($caixa->resumo['abertura']) || isset($caixa->resumo['fechamento']))) {
            $dataFim = isset($caixa->resumo['abertura']) ? str_replace('/', '-', $caixa->resumo['abertura']['data']) : str_replace('/', '-', $caixa->resumo['fechamento']['data']);
            $dataFim = str_replace(' ', 't', date('d-m-Y H:i:s', strtotime('-1minute', strtotime($dataFim))));
            $saldoCaixa = $this->saldoCaixa(['id' => $id, 'data_fim' => $dataFim]);
            if ($saldoCaixa->successo == '1') {
                $saldo += $saldoCaixa->objeto[0]->saldo_fechamento;
            }
            $saldoFinanceiro = (new Financeiro)->consulta(['meioPagto' => 4, 'data_fim' => isset($caixa->resumo['abertura']) ? str_replace(['/', ' '], ['-', 't'], $caixa->resumo['abertura']['data']) : str_replace(['/', ' '], ['-', 't'], $caixa->resumo['fechamento']['data'])]);
            if ($saldoFinanceiro->successo == '1') {
                $saldo += $saldoFinanceiro->objeto[0]->saldo;
            }
        }
        return $saldo;
    }
    
    /**
     * Consultar saldo atual para o operador de caixa
     */
    public function saldoCaixa($post)
    {
        $session = Yii::$app->session;
        $session->open();
        $api = new WsCaixa($session->get('user_token'));
        if (isset($post) && !empty($post)) {
            return $api->saldoCaixa([
                'id' => $post['id'],
                'data_ini' => isset($post['dataInicio']) && !empty($post['dataInicio']) ? $post['dataInicio'] : 'null', 
                'data_fim' => isset($post['dataFim']) && !empty($post['dataFim']) ? $post['dataFim'] : 'null'
            ]);
        }
        return $api->getApiError('Faltam parâmetros para consultar saldo de caixa.');
    }
}
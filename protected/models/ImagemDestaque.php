<?php
namespace app\models;

use Yii;
use app\models\Anexos;

class ImagemDestaque extends Anexos{
	public function __construct(){
		$this->nome='imgDestaque';
		$this->tipo='imagem';
	}

	public function imgBase64($file)
    {
    	parent::imgBase64($file);
    	$this->tipo=$file->extension;
    }

	public function acaoAposSalvar($modelAnexos,$post,$modelAgenda){
		return true;
	}

	/**
     * definição dos labels de atributos
     */
    public function attributeLabels() {
        return[
            'objeto'=>'Imagem para destaque na página inicial (carrossel)'
        ];
    }
}
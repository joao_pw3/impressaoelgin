<?php
use yii\helpers\Url;
?>
<div class="container">
    <input type="hidden" id="url-index" value="<?= Url::to(['site/index']); ?>">
    <h2>Boleto gerado com sucesso</h2>
    <p>A confirmação do pagamento pode levar até 4 dias úteis. Uma vez confirmado, seus créditos estarão disponíveis na sua conta para adquirir produtos Mineirão</p>
    <p><a target="_blank" href="<?php echo $urlBoleto?>">Clique aqui para ver o boleto gerado (abre uma nova janela)</a></p>
    <p>Você pode acompanhar o status do seu pedido na <a href="<?=Url::to(['cliente'])?>">área do cliente</a></p>
</div>

<?php 
$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/carrinho.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/compra.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
?>
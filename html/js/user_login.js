$(document).ready(function() { 
    $('#esqueci-senha').click(function() {
            $('#user_id-esqueci').val($('#user_id').val()).attr('readonly', $('#user_id').val().length == 0 ? false : true);
            $('#modal-senha').modal('toggle');
            konduto('esqueci-senha');
            return false;
        });
        
    $('#btn-esqueci-senha').click(function () {
        if ($('#user_id-esqueci').val() == '') {
            $('#modal-mensagem .modal-body').html('Informe o e-mail para o qual deseja trocar a senha de acesso.');
            $('#modal-mensagem').modal('toggle');
            return false;
        }
        enviarMensagemEsqueciSenha();
    });
})
function enviarMensagemEsqueciSenha () {
    $('.close').trigger('click');
    doAjax ($('#url-mensagem-esqueci').val(), 'POST', {'user_id': $('#user_id-esqueci').val()}, 'JSON', 10000, 'esqueciOK', 'esqueciErr');
    return false;
}

function esqueciOK (data) {
    $('#modal-mensagem .modal-body').html('Verifique a caixa de mensagens do e-mail informado.');
    $('#modal-mensagem').modal('toggle');
    return false;
}

function esqueciErr (data) {
    $('#modal-mensagem .modal-body').html('Falha ao enviar mensagem para troca de senha: ' + data.erro.mensagem);
    $('#modal-mensagem').modal('toggle');
    return false;
}
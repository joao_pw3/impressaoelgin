<table class="uppercase table table-responsive table-striped">
	<thead>
		<tr>
			<th>Data da venda</th>
			<th>Código da venda</th>
			<th>Produto</th>
			<th>Nome do ocupante</th>
			<th>Documento</th>
			<th>Telefone</th>
			<th>Email</th>
			<th>Nome do comprador</th>
			<th>Documento do comprador</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		foreach($produtos->objeto as $produto){
		?>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td><?=$produto->cliente->nome?></td>
			<td><?=$produto->cliente->documento?></td>
			<td><?php 
			if (isset($produto->cliente->ddd,$produto->cliente->celular))
				echo $produto->cliente->ddd.' '.$produto->cliente->celular?></td>
			<td><?php 
			if (isset($produto->cliente->email))
				echo $produto->cliente->email?></td>
			<td></td>
			<td></td>
		</tr>
		<?php }?>
	</tbody>
</table>
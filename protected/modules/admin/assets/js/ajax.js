/**
 * Função Ajax genérica
 * @param string url Url para execução de rotina
 * @param string method Método de envio de informações: POST, PUT, GET
 * @param array form Formulário de dados
 * @param string type Tipo de retorno: HTML, JSON, TEXT
 * @param integer timeout Tempo máximo de execução
 * @param string callSuccess String com nome de função a ser executada quando a função for bem-sucedida
 * @param string callError String com nome de função a ser executada quando a função apresentar algum erro
 * @returns {undefined}
 */
function doAjax (url, method, form, type, timeout, callSuccess, callError) {
    $.ajax({
        type: method, 
        url: url, 
        data: form, 
        dataType: type, 
        timeout: timeout
    }).done(function(data) {
        if (data.successo === '1' || data === '1') {
            var funcName = callSuccess;
            window[funcName](data);
        } else {
            var funcName = callError;
            window[funcName](data);
        }
    }).fail(function(jqXHR, textStatus ) {
        if (textStatus === 'timeout') {     
            showMsg('Limite de tempo de execução atingido. Por favor, tente novamente.'); 
        } else {
            showMsg('<div class="textMod">Falha em requisição: ' + textStatus + '</div><br><button type="button" class="btn btn-default botaoGeral" data-dismiss="modal">Fechar</button>');
        }
        return false;
    });
//    }).always(function() {
//        showMsg('Requisição completa');
//    });
}

function showMsg (msg) {
    $('#modal-mensagem .modal-body').html(msg);
    $('#modal-mensagem').modal('toggle');
}
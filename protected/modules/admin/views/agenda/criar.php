<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Evento - Novo evento' . (isset($matriz) && $matriz == true ? ' (matriz)' : '');
?>

<div class="container">
	<div class="row">
		<h2 class="tituloGeral"><i class="fa fa-clone"></i> Evento - Novo Evento <?= isset($matriz) && $matriz == true ? ' (matriz)' : ''; ?></h2>
	</div>

	<?php
	$form = ActiveForm::begin(['id' => 'agenda-form']); ?>

	<?= $form->field($model, 'nome') ?>

	<?= $form->field($model, 'descricao')->textarea(['rows' => 5]) ?>

	<?= $form->field($model, 'dataInicial') ?>

	<?= $form->field($model, 'dataFinal') ?>

	<?= $form->field($model, 'dataAtivar') ?>

	<?= $form->field($model, 'dataDesativar') ?>

	<?= $form->field($model, 'ativo') ?>

	<div class="form-group">
		<?= Html::submitButton('Cadastrar', ['class' => 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>
	
</div>

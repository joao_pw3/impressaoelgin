<?php
use app\assets\VendaAsset;
use app\assets\CapptaAsset;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\Ingresso;
use app\models\Tags;
use yii\web\View;
use app\models\Mapa;
use kartik\money\MaskMoney;

VendaAsset::register($this);
CapptaAsset::register($this);

$this->title = 'Caixa - Venda de ingressos';
$indexCarrinho = 0;
$produtosCarrinho = [];
?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
        <?php $this->registerJs("capptaInit();", View::POS_READY); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h2 class="tituloGeral"><i class="fa fa-clone"></i> Caixa - Venda de ingressos</h2>
            <p id="cappta-status"></p>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <?php if (!isset($caixa->resumo['abertura'])) { ?>
            <p class="plus-red">O caixa está fechado. Para efetuar vendas de ingressos é necessário abri-lo primeiro.</p>
            <p>Clique <a href="<?= Url::to(['caixa/index']); ?>">aqui</a> para abrir o caixa.</p>
            <?php } else { ?>
                <?php $form = ActiveForm::begin(['id' => 'venda-form']); ?>
                <?php $model->id_agenda = isset($id_agenda) ? $id_agenda : ''; ?>
                <?= $form->field($model, 'id_agenda')->dropDownList(ArrayHelper::map($eventos, 'codigo', 'data', 'evento')); ?>
                <?= Html::submitButton('Carregar mapa', ['class' => 'btn btn-primary', 'id' => 'btn-mapa']) ?>
                <?php ActiveForm::end(); ?>
            <?php } ?>
            <br>
        </div>
    </div>

<?php if (isset($id_agenda) && !empty($id_agenda)) { ?>
    <div class="row">
        <input type="hidden" name="id_vendedor" id="id_vendedor" value="<?= Yii::$app->user->identity->cod_operador; ?>">        
        <input type="hidden" id="id_agenda" value="<?= $id_agenda; ?>">
        <input type="hidden" id="assentos" value="true">
        <input type="hidden" id="url-select-ingresso" value="<?= Url::to(['venda/select-ingresso']); ?>">
        <input type="hidden" id="url-add-produto" value="<?= Url::to(['venda/adicionar-produto-carrinho']); ?>">
        <input type="hidden" id="url-del-produto" value="<?= Url::to(['venda/remover-produto-carrinho']); ?>">  
        <input type="hidden" id="url-carrinho" value="<?= Url::to(['venda/salvar-carrinho', 'tokenOperador' => 'true']); ?>">
        <input type="hidden" id="url-cobranca-conciliada" value="<?= Url::to(['/admin/venda/cobranca-conciliada']); ?>">
        <input type="hidden" id="url-filipeta" value="<?= Url::to(['venda/filipeta']); ?>">
        <div class="col-xs-8">
            <div class="bordArredondBilhet">
                <?= !empty($mapa) ? $mapa : '<p style="margin-top:100px; width:100%; text-align:center; font-size:18px; color:#E10019;">Não há produtos para este mapa</p>'; ?>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="bordArredondBilhet2">
                <div class="row">
                    <input type="text" name="documento_comprador" id="documento_comprador" class="form-control hidden" placeholder="Documento do comprador">
                    <input type="hidden" name="trid" id="trid" value="<?= substr(time() . mt_rand(), 8, 11); ?>">
                </div>
                <form id="form-bilheteria">
                    <div class="row barScroll fundoBilhet" id="venda-selecionados">
                        <?php
                        if (isset($carrinho->objeto->produtos) && !empty($carrinho->objeto->produtos)) {
                            $tags = new Tags;
                            $icone = new Mapa;
                            foreach ($carrinho->objeto->produtos as $index => $produto) {
                                $tagsTmp = $tags->objetoTags($produto->tags);
                                if (isset($tagsTmp->taxa_conveniencia) && $tagsTmp->taxa_conveniencia == 1) {
                                    continue;
                                }
                                ?>
                                <div id="div-<?= $produto->codigo; ?>" class="col-xs-12 selecionado prodBilhet">
                                    <form id="form-<?= $produto->codigo; ?>">
                                        <input type="hidden" name="Ocupante[<?= $index; ?>][produto_codigo]" value="<?= $produto->codigo; ?>">
                                        <div class="col-xs-2 semPad">
                                            <div class="div-circle"><?= $tagsTmp->fileira . '-' . $tagsTmp->assento; ?></div>
                                            <?php if (isset($tagsTmp->especial) && !empty($tagsTmp->especial)) { ?>
                                            <div class="div-especial font-awesome" title="Assento especial - <?= $tagsTmp->especial; ?>"><?= $icone->iconeEspecial[$tagsTmp->especial]; ?></div>
                                            <?php } ?>
                                        </div>
                                        <div class="col-xs-10">
                                            <input type="text" name="Ocupante[<?= $index; ?>][nome]" class="form-control ocupante-nome ocupBilhet" placeholder="Nome do ocupante">
                                            <input type="text" name="Ocupante[<?= $index; ?>][documento]" class="form-control ocupante-doc ocupBilhet" placeholder="Documento do ocupante">
                                            <?= Ingresso::getSelectTipo($produto, $cupons, $index, false, false); ?>
                                            <span id="conv-<?= $produto->codigo; ?>" class="ocupante-conv"></span>
                                        </div>
                                    </form>
                                </div>
                                <?php
                                $indexCarrinho = $index;
                                array_push($produtosCarrinho, $produto->codigo);
                            }
                        }
                        ?>
                    </div>
                </form>
                <div class="row totais hidden div-bilheteria">
                    <hr class="borderSulco">
                    <div class="col-xs-8 corBrancaB" align="right">Total:  R$</div>
                    <div class="col-xs-4 ingresso-total corBrancaB" style="font-size:20px;" align="right"></div>
                    <hr class="borderSulco2">
                    <?php $form = ActiveForm::begin(['options' => ['id' => 'form-venda']]); ?>
                    <input type="hidden" id="url-venda" value="<?= Url::to(['inicia-pedido']) ?>" style="margin-bottom:30px;">
                    <?= $form->field($modelVnB, 'total')->hiddenInput(['id' => 'vendaTotal'])->label(false); ?> 
                    <?php ActiveForm::end(); ?>            
                </div>
                <img src="<?= Yii::getAlias("@web") ?>/images/logo_easy.png" alt="">
                <div class="row div-bilheteria corBrancaB">
                    <div id="confirmaVenda">
                        <?php $form2 = ActiveForm::begin(['options' => ['id' => 'form-pedido']]); ?>
                        <?= $form2->field($modelVnB, 'forma_pagamento', ['labelOptions' => ['style' => 'color:white']])->dropDownList($modelVnB->formasPagamento(), ['id' => 'formaPagamento']); ?>
                        <?= $form2->field($modelVnB, 'parcelas')->dropDownList([], ['id' => 'parcelas']); ?>
                        <?= $form2->field($modelVnB, 'valor_final')->hiddenInput(['id' => 'valorFinal'])->label(false); ?>
                        <div id="especie-mostra">
                            <hr class="borderSulco">
                            <?= $form2->field($modelVnB, 'troco')->widget(MaskMoney::classname(), [
                                'pluginOptions' => [
                                    'prefix' => 'R$ ',
                                    'thousands' => '.',
                                    'decimal' => ',',
                                    'allowNegative' => false,
                                ],
                                'options' => [
                                    'placeholder' => 'R$ 0,00'
                                ]
                            ]);
                            ?>                  
                            <div class="btn-auxiliar-3">
                                <?= Html::button('Calcular Troco', ['id' => 'btn-calcular-troco', 'class' => 'btn btn-primary', 'type' => 'button']); ?>
                            </div>                            
                            <div class="col-xs-8 corBrancaB" align="right">Devolver:  R$</div>
                            <div id="especie-troco" class="col-xs-4 corBrancaB" style="font-size:20px;" align="right">0,00</div>
                            <hr class="borderSulco2">
                        </div>          
                        <div class="btn-auxiliar-2">
                        <?= Html::button('Autorizar Pagamento', ['id' => 'btn-autorizar-pagamento', 'class' => 'btn btn-primary', 'type' => 'button']); ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <div id="div-pagamento-efetuado" class='hidden'>
                    <?= Html::button('Impressão da Filipeta', ['id' => 'imprimir-filipeta', 'class' => 'btn btn-primary', 'type' => 'button']); ?>
                    <a href="<?= Url::to(['venda/ingressos']); ?>" class="btn btn-primary">Próximo Cliente</a>
                </div>
            </div>
        </div>
    </div>
    <div class="hidden" id="mostra-filipeta"></div>
    <br /><br /><br />
    <?php } ?>

    <?php
    $this->registerJs('
        var produtosCarrinho = "' . implode(',', $produtosCarrinho) . '";
	', View::POS_HEAD);
    ?>
</div>
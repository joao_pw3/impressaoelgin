<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            [['username'], 'email'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if(isset($user->erro)){
                switch ($user->erro->codigo) {
                    case '007':
                        $this->addError($attribute,'Senha inválida');
                        break;
                    case '050':
                        $this->addError('username','Usuário não encontrado');
                        break;                    
                    default:
                        $this->addError('username',$user->erro->mensagem);
                        break;
                }
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    /**
     * Finds user by username and password
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username,$this->password);
        }
        return $this->_user;
    }

    /**
     * Logs in a user using token
     * @return bool whether the user is logged in successfully
     */
    public function autologin($token=null)
    {
        if($token==null) return false;
        $this->_user = User::findIdentityByAccessToken($token);        
        if(Yii::$app->user->login($this->_user, $this->rememberMe ? 3600*24*30 : 0))
            return Yii::$app->user;
    }   

    public function attributeLabels()
    {
        return [
            'username' => 'E-mail',
            'password' => 'Senha',
            'rememberMe' => 'Lembrar meu acesso',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pessoa".
 *
 * @property integer $id
 * @property integer $comprador
 * @property string $nome
 * @property string $email
 * @property string $sexo
 * @property integer $cpf
 * @property string $nascimento
 * @property string $telefone
 * @property string $time
 * @property string $cep
 * @property string $logradouro
 * @property integer $numero
 * @property string $complemento
 * @property string $bairro
 * @property string $pais
 * @property integer $estado
 * @property integer $cidade
 * @property integer $status
 */
class Pessoa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pessoa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comprador', 'cpf', 'numero', 'estado', 'cidade', 'status'], 'integer'],
            [['nome', 'email', 'sexo', 'cpf', 'nascimento', 'telefone', 'time', 'cep', 'logradouro', 'numero', 'bairro', 'pais', 'estado', 'cidade', 'status'], 'required'],
            [['sexo'], 'string'],
            [['nascimento'], 'safe'],
            [['nome', 'email', 'logradouro'], 'string', 'max' => 200],
            [['telefone'], 'string', 'max' => 15],
            [['time', 'bairro', 'pais'], 'string', 'max' => 50],
            [['cep'], 'string', 'max' => 10],
            [['complemento'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comprador' => 'Comprador',
            'nome' => 'Nome',
            'email' => 'Email',
            'sexo' => 'Sexo',
            'cpf' => 'Cpf',
            'nascimento' => 'Nascimento',
            'telefone' => 'Telefone',
            'time' => 'Time',
            'cep' => 'Cep',
            'logradouro' => 'Logradouro',
            'numero' => 'Numero',
            'complemento' => 'Complemento',
            'bairro' => 'Bairro',
            'pais' => 'Pais',
            'estado' => 'Estado',
            'cidade' => 'Cidade',
            'status' => 'Status',
        ];
    }
}

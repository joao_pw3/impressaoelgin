<?php
namespace app\components;

use Yii;
use yii\base\Widget;

class TempoCarrinhoWidget extends Widget 
{
   /**
     * Mostrar breadcrumb (passos de compra) + temporizador do carrinho
     * @param string $imagem Nome da imagem para a página em uso
     */
    public static function getBreadCrumb($imagem) 
    {
        $session = Yii::$app->session;
        $session->open();
        return "
        <div class='container bc-div-top' id='div-carrinho'>
            <div class='bc-div'>
                <div class='tabBreadCrumb' id='tabBreadCrumb' style='background: url(" . Yii::getAlias('@web') . "/images/" . (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho']) ? $imagem : 'losang0.png') . ") no-repeat 0px 0px; background-size: 100%;'>
                    <div class='espacoTop2'>
                        " . ($imagem == 'losang1.png' ? '<strong>' : '') . "1 - Escolha do Assento" . ($imagem == 'losang1.png' ? '</strong>' : '') . "
                    </div>
                    <div class='espacoTop2'>
                        " . ($imagem == 'losang2.png' ? '<strong>' : '') . "2 - Cadastro/Login" . ($imagem == 'losang2.png' ? '</strong>' : '') . "
                    </div>
                    <div class='espacoTop2'>
                        " . ($imagem == 'losang3.png' ? '<strong>' : '') . "3 - Resumo</strong>" . ($imagem == 'losang3.png' ? '</strong>' : '') . "
                    </div>
                    <div class='espacoTop2'>
                        " . ($imagem == 'losang4.png' ? '<strong>' : '') . "4 - Pagamento" . ($imagem == 'losang4.png' ? '</strong>' : '') . "
                    </div>
                    <div class='espacoTop3'>
                        <span class='" . (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho']) ? '' : 'hidden') . "' id='relogio-carrinho'>
                            <div class='celulaTab'>
                                > Tempo restante para<br>concluir a compra:
                            </div>
                            <div class='celulaTab'>
                                <div class='relogio' id='clockdiv'>
                                    <div><span class='minutes'></span></div>
                                    <div>:</div>
                                    <div><span class='seconds'></span></div>
                                </div>
                            </div>
                        </span>		
                    </div>
                </div>
            </div>
        </div>
        ";
    }
 
}
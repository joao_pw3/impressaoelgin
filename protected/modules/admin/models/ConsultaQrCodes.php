<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "qrcode_eventos".
 *
 * @property string $documento
 * @property string $ocupante
 * @property string $qrcode
 * @property integer $bloco
 * @property integer $fileira
 * @property integer $cadeira
 */
class ConsultaQrCodes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'consulta-qrcodes';
    }

}

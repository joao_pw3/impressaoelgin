<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
        'css/font-awesome.css',
        'css/site.css',
        'css/styles.css',
        // 'css/bootstrap.css',
        'css/normalize.min.css',
        'css/foundation-flex.css',
        'css/icones.css',
        'css/animate.min.css',
        'css/style.css',
        'css/style_2.css',
    ];
    public $js = [
        'js/konduto.js',
        'js/fontawesome.js',
        'js/wow.min.js',
        'js/app.js',
        //'js/bootstrap.js',
        'js/compra.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    
    public $publishOptions = [
        'forceCopy' => YII_DEBUG
    ];
}

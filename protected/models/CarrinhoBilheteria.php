<?php
namespace app\models;

use Yii;
use app\models\Carrinho;
use app\models\WsCarrinho;

/**
 * Carrinho para o Operador
 * @author User
 */
class CarrinhoBilheteria extends Carrinho
{
    /**
     * Criar carrinho de compras - Operador
     * @return object {'successo': '1', 'id': ''}
     */
    public function criarCarrinho()
    {
        $session = Yii::$app->session;
        $session->open();
        if (!isset($session['idCarrinho']) || $session['idCarrinho'] == '') {
            $this->carrinho = (new WsCarrinho($session->get('user_token')))->criarCarrinho();
            if ($this->carrinho->successo == '1') {
                $this->definirSessaoCarrinho($this->carrinho->objeto->id);
                $this->definirTempoCarrinho();
            }
            return $this->carrinho;
        }
        $this->definirSessaoCarrinho($session['idCarrinho']);
    }
    
    /**
     * Atualizar carrinho de compras - Operador
     * @param $post Dados do carrinho: 
     *      '[produto]': {'codigo': '', 'unidades': ''}, 
     *      '[cupom]': {'id': '', 'unidades': ''}, 
     *      '[tag]': {'nome': '', 'valor': ''}
     * @param boolean $produtoUnico Indica que o cupom é para venda com assento marcado (true) ou com assentos livres (false)
     * @return object {'successo': '1'}
     */
    public function atualizarCarrinho ($post, $produtoUnico=true)
    {
        $session = Yii::$app->session;
        $session->open();
        if (!isset($session['idCarrinho']) || empty($session['idCarrinho'])) {
            $this->criarCarrinho();
        }
        $api = new WsCarrinho($session->get('user_token'));
        if (!empty($post) && isset($session['idCarrinho']) && !empty($session['idCarrinho'])) {
            $object = [];
            if (isset($post['produto_codigo']) && !empty($post['produto_codigo'])) {
                $object['produto'] = [
                    'codigo' => $post['produto_codigo'], 
                    'unidades' => isset($post['produto_unidades']) ? (int)$post['produto_unidades'] : '1'
                ];
            }
            if (isset($post['cupom_codigo']) && !empty($post['cupom_codigo']) && $post['cupom_codigo'] != 'inteira') {
                $object['produto']['unico'] = $produtoUnico;
                $object['cupom'] = ['id' => $post['cupom_codigo'], 'unidades' => isset($post['cupom_unidades']) ? (int)$post['cupom_unidades'] : '1'];
            }
            if (isset($post['tag_nome']) && isset($post['tag_valor']) && !empty($post['tag_nome']) && !empty($post['tag_valor'])) {
                $object['tag'] = ['nome' => $post['tag_nome'], 'valor' => $post['tag_valor']];
            }
            
            $this->carrinho = $api->atualizarCarrinho($object, $session['idCarrinho']);
            $this->definirTempoCarrinho();
            return $this->carrinho;
        }
        return $api->getApiError('Não foi possível atualizar o carrinho de compras - operador.');
    }
    
    /**
     * Consultar carrinho de compras - Operador
     * @param string $idCarrinho Id de um carrinho
     * @return object {'successo': '1', '[produtos]': {...}, '[cupons]': {...}, '[tags]': {...}, '[totais]': {...}}
     */
    public function consultarCarrinho ($idCarrinho='')
    {
        $session = Yii::$app->session;
        $session->open();
        $api = new WsCarrinho($session->get('user_token'));
        $id_carrinho = (!empty($idCarrinho) ? $idCarrinho : (isset($session['idCarrinho']) && !empty($session['idCarrinho']) ? $session['idCarrinho'] : ''));
        if (!empty($id_carrinho)) {
            $this->carrinho = $api->consultarCarrinho($id_carrinho);
            return $this->carrinho;
        }
        return $api->getApiError('Não foi possível consultar o carrinho de compras - bilheteria.');
    }
    
    /**
     * Remover cupom do carrinho.
     * @param boolean $produtoUnico Indica que o cupom é para venda com assento marcado (true) ou com assentos livres (false)
     */
    public function removerCupomCarrinho ($post, $produtoUnico=true) 
    {
        if (!empty($post)) {
            $carrinho = $this->consultarCarrinho();
            foreach($carrinho->objeto->produtos as $index => $produto) {
                if (isset($produto->cupom->id)) {
                    return $this->atualizarCarrinho([
                        'produto_codigo' => $produto->codigo, 
                        'cupom_codigo' => $produto->cupom->id, 
                        'cupom_unidades' => 0
                    ], $produtoUnico);
                }
            }
        }
    }
    
    /**
     * Atualizar tags em lote. As tags anteriores são descartadas
     */
    public function atualizarTagsCarrinhoLote ($post, $idCarrinho)
    {
        $session = Yii::$app->session;
        $session->open();
        $api = new WsCarrinho($session->get('user_token'));
        if (!empty($post)){
            $atualizar = $api->atualizarTagsCarrinhoLote($post, $idCarrinho);
            $this->definirTempoCarrinho();
            return $atualizar;
        }
        return $api->getApiError('Não foi possível salvar informações de ocupante.');
    }    
    
}

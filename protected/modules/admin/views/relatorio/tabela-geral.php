<!-- <pre>	
<?php 	print_r($model) ?>
</pre> -->
<div class="container-fluid">	
	<table id="table-fixed-header" class="table-bordered table-striped table table-condensed">	
		<thead>
			<tr>
					<th>Transação</th>	
					<th>Adquirente</th>	
					<th>Vendedor</th>	
					<th>Comprador</th>	
					<th>Antifraude</th>	
					<th>ERP</th>	
					<th>Usuário</th>	
			</tr>
		</thead>
		<tbody>	
			<?php 	foreach ($model->lista as $key => $value) { ?>
				<tr>
					<td><?php $model->mostrarValores($value->transacao)?></td>	
					<td><?php $model->mostrarValores($value->adquirente)?></td>	
					<td><?php $model->mostrarValores($value->vendedor)?></td>	
					<td><?php $model->mostrarValores($value->comprador)?></td>	
					<td><?php $model->mostrarValores($value->antifraude)?></td>	
					<td><?php $model->mostrarValores($value->erp)?></td>	
					<td><?php $model->mostrarValores($value->usuario)?></td>	
				</tr>
			<?php } ?>
		</tbody>	
	</table>
</div>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="area" id="area">

    <div class="container">

        <div style="margin-bottom:25px;">
            <div class="col-md-6 about-left wow fadeInLeft animated" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                <?= Html::img($evento->anexos['bannervitrine']); ?>
            </div>
            <div class="col-md-4 about-right wow fadeInRight animated textosAprest" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                    <div><?= $evento->nome; ?></div>                    
                        <?php if ($evento->tags['apelido']!='temporada2018') { 
                        $objData=\DateTime::createFromFormat('d/m/Y H:i:s',$evento->data->inicial);
                        $diaSemana=['','Seg','Ter','Qua','Qui','Sex','Sab','Dom'];
                        ?>
                            <div>
                                <?= $diaSemana[$objData->format('N')].' '.$objData->format('d/m') ?> - 
                                Horário: <?= $objData->format('H'); ?>h
                            </div>
                        <?php } ?>                  
                    <?= $evento->descricao; ?>
            </div>
            <hr style="padding-top:25px;">
        </div>

        <div class="row">
            <div class="col-md-8">
                <div id="mapaMineirao" class="mapaDefundo">
                    <!-- o mapa é montado por JS -->
                </div>
            </div>
            <div class="col-md-4 textosAprest2" style="padding-top:30px;">
                <div>
                    O <strong>Mineirão Temporada</strong> oferece opções para todos os gostos e bolsos. Neste momento, você pode garantir seu <strong>Mineirão Tribuna ou Embaixada Mineirão</strong>.
                </div>
                <div>
                    Escolha na imagem ao lado o setor <span style="font-weight:bold; color:red;">Vermelho</span> ou <span style="font-weight:bold; color:#634096">Roxo</span> e clique para selecionar os lugares de seu interesse.<br>
                </div>
                <div>
                    Para compra de Camarotes, entre em contato com nossos consultores pelo telefone <strong>(31) 3499-4333</strong>.<br>
                </div>
                <div>
                    <hr>
                    <span class="row contato">
                        <a class="carrinho seta" href="<?= Url::to(['site/index']); ?>">Voltar à página de eventos</a> 
                    </span>
                </div>
            </div>
        </div>


    </div>  
</div>

<?php
// registrar primeiro o arquivo de funções JS
$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset', 'yii\bootstrap\BootstrapPluginAsset']]);

//chamar método do arquivo acima - o parâmetro depende da matriz do evento, que é definida em PHP
$this->registerJs('
    areas = montarAreas(' . $matriz['areasDisponiveis'] . ',"' . Url::to(['site/blocos', 'id' => $evento->codigo]) . '");
    $.each(areas,function(i,o){
        $("#mapaMineirao").append(o)
    })
'
)
?>
<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\web\View;
use app\components\TempoCarrinhoWidget;

$session = Yii::$app->session;
$session->open();
if (isset($session['user_token']) && !empty($session['user_token'])) {
    header('Location: ' . Url::to(['site/cliente']));
    die;
}
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    echo TempoCarrinhoWidget::getBreadCrumb('losang2.png');
} 
?>

<div class="row contato">
    <input type="hidden" id="url-resumo" value="<?= Url::to(['site/resumo']); ?>">
    <input type="hidden" id="url-cliente" value="<?= Url::to(['site/cliente']); ?>">
    <input type="hidden" id="url-index" value="<?= Url::to(['site/index']); ?>">
    
    <div class="small-4 columns boxRoxo">
        <span>LOGIN</span>
        <div class="formMineiraobox">
            <div class="col-md-6 botVisit">
                <a href="<?= Url::to(['site/cadastro']); ?>" class="linkcAD">Quero me Cadastrar</a>
            </div>
            <div class="col-md-6 botVisit">
                <a href="" class="linkcAD" id="esqueci-senha">Esqueci minha Senha</a>
            </div>

            <br clear="all">
            <br>
            <?php $form = ActiveForm::begin([
                'id' => 'form-login',
                'class' => 'formMineirao wpcf7-form',
                'action' => Url::to(['site/login'])
            ]); ?>
                <?= $form->field($cliente, 'user_id')->textInput([
                    'class' => 'form-control',
                    'placeholder' => 'Digite aqui seu e-mail',
                    'title' => 'Digite aqui seu e-mail',
                    'id' => 'user_id',
                ]); ?>
                
                <?= $form->field($cliente, 'senha')->passwordInput([
                    'class' => 'form-control',
                    'placeholder' => 'Informe sua senha',
                    'title' => 'Informe sua senha. Esta senha serve para recuperar os seus dados cadastrais e de cartão, mas não autoriza pagamentos.',
                    'id' => 'senha',
                    'maxlength' => 10
                ]); ?>
                <input type="hidden" name="finalizar-compra" value="<?= isset($_SERVER['HTTP_REFERER']) && stristr(urldecode($_SERVER['HTTP_REFERER']), 'site/blocos') ? 1 : 0; ?>">
                <input type="submit" value="Enviar" id="login-cliente" class="icon-next">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <?php if (isset($session['tentativas_login']) && $session['tentativas_login'] >= 3) {?>
                <hr class="linhaLogin">
                <div class="g-recaptcha" data-sitekey="6Lecv0YUAAAAABKsy4PkMYctR_tPParhtuJjl5QW" align="center"></div>
                <?php } ?>
            <?php ActiveForm::end(); ?>   
            <br clear="all">
        </div>
    </div>
    
</div>
<div class="container">
	<div class="row" style="width:100%;">
		<div class="botVisit" style="width:120px; margin:0px auto; display:block;">
			<a href="<?= Url::to(['site/index']); ?>" class="linkcAD"><i class="fa fa-home"></i> Go Home</a>
		</div>
	</div>
</div>

<!-- Modal - Esqueci minha senha -->
<?php Modal::begin([
    'header' => '<h4 align="center">Esqueci minha senha</h4>',
    'size'   => 'modal-sm',
    'id'     => 'modal-senha',
    'options'=> [
        'class' => 'modal-center'
    ]
]); ?>
<form id="form-esqueci-senha">
    <input type="hidden" id="url-mensagem-esqueci" value="<?= Url::to(['site/mensagem-esqueci-senha']); ?>">
    <div class="row">
        <div class="col-md-12">
            <label for="user_id-esqueci">E-mail:</label>
            <input type="text" name="user_id" id="user_id-esqueci" class="form-control" readonly>
            <p>Caso este e-mail esteja cadastrado, receberá uma mensagem com instruções para a troca de sua senha.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 contato" align="center">
            <input type="button" value="Enviar" id="btn-esqueci-senha" class="icon-next esqueci-senha">
        </div>
    </div>
</form>
<?php Modal::end(); ?>

<?php 
if (isset($session['erro_login']) && !empty($session['erro_login'])) {
    $this->registerJs('
        var tentativas_login = ' . (isset($session['tentativas_login']) && !empty($session['tentativas_login']) ? $session['tentativas_login'] : 0) . ';
        var erro_login = "' . (isset($session['erro_login']->erro->mensagem) ? 
            $session['erro_login']->erro->mensagem : 
            (isset($session['erro_login']->erro['mensagem']) ? 
            $session['erro_login']->erro['mensagem'] : '')) . '";', View::POS_HEAD);
    unset($session['erro_login']);
}
$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/carrinho.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset']]); 
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    $this->registerJs("deadline = setDataExpira('" . $session['tempoCarrinho'] . "'); initializeClock(deadline);");
}
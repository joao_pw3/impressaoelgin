var codigo;
var urlCalcularTaxa;
var selectIngressoAtual;
var linhaDelProduto;
var indice;
var redirecionarParaPagamento=true;
var tiposIngresso;
var preventBeforeUnload=false;
var btnModalOpen;

$(document).ready(function () {
    
    $('.del-carrinho').click(function () {
        linhaDelProduto=$(this).attr('id').replace('btn-del','row');
        $("#"+linhaDelProduto).remove();
        preventBeforeUnload=true;
        if($(".telaOcupantes").length <= 0)
            salvarOcupantesVoltarParaEvento();
    });
    
    $('#btn-pagar').click(function () {
        if (erroOcupantes()) {
            showMsg('Preencha os campos obrigatórios (nome e documento).');
            return false;
        }
        if(!erroOcupantes())
            preventBeforeUnload=false;
    });
   
    // alterar valores de taxa de conveniência ao alterar o produto selecionado
    $(".exibirValoresIngressos > select").change(function(k){
        selectIngressoAtual=$(this);
        taxaOk();
        preventBeforeUnload=true;
    });

    $("#btn-continuar-compra").click(function(){
        salvarOcupantesVoltarParaEvento();
        return false;
    })

    $("input[name*='Ocupante']").on('input',function(e){
        if(!preventBeforeUnload)
            preventBeforeUnload=true;
    })

    $("#consulta-cupom").click(function(){
        var idCupom=$("#input-cupom").val();
        if(idCupom==undefined || idCupom=='') return;
        $(this).button('loading');
        doAjax(urlValidarCupom, 'POST', {cupom:idCupom,evento:$(btnModalOpen).data('idevento'),data:$(btnModalOpen).data('iddata')}, 'JSON', 10000, 'consultaCupomOk', 'consultaCupomErr');
    })
});

function calcularTipos(){
    tiposIngresso={};
    $(".cupom-codigo").each(function(i,o){
        var tipo=$(o).val();
        if(!tiposIngresso[tipo])
            tiposIngresso[tipo]=0;
        tiposIngresso[tipo]+=1;
    });
}

function erroOcupantes() {
    var erro = false;
    $('.nomeOc').each(function () {
        if ($(this).val() == '') {
            erro = true;
        }
    });
    $('.docOc').each(function () {
        if ($(this).val() == '') {
            erro = true;
        }
    });    
    return erro;
}

function ocupanteOK (data) {
    doAjax ($('#url-cupom').val(), 'POST', $('#form-ingresso').serialize(), 'JSON', 10000, 'cupomOK', 'cupomErr');
}

function ocupanteErr (data) {
    showMsg('Erro ao registrar ocupante: ' + data.erro.mensagem);
}

function delProdutoOK (data) {
    var linhas = $(".telaOcupantes");
    if(linhas.length > 0){
        var i=parseInt(indice);
        while(i<=linhas.length){
            console.log([i+1,i])
            elementosLinhaProduto(i+1,i);
            i++
        }
    }
    $('#'+linhaDelProduto).remove();
    $('.infoUnidades-'+codigo).val($('.infoUnidades-'+codigo).val()-1);
    redirecionarParaPagamento=false
    doAjax ($('#url-ocupante').val(), 'POST', $('#form-ingresso').serialize(), 'JSON', 10000, 'ocupanteOK', 'ocupanteErr');
    showMsg('Produto removido do carrinho com sucesso.');
}

function delProdutoErr (data) {
    showMsg('Erro ao remover produto do carrinho: ' + data.erro.mensagem);
}

function cupomOK (data) {
    if(redirecionarParaPagamento)
        window.location.href = $('#url-pagar').val() + '?evento=' + $('#evento').val() + '&data=' + $('#data_evento').val();
}

function cupomErr (data) {
    showMsg('Erro ao adicionar ingressos ao carrinho: ' + data.erro.mensagem);
}

function taxaOk(){
    var selTaxa=$(selectIngressoAtual).attr('id');
    var spanTaxa = $("#"+selTaxa).next();
    var strValor = 'R$ '+ $('#' + selTaxa + ' option:selected').attr('data-valor').replace('.', ',');
    var strTaxa = 'R$ '+ parseFloat($('#' + selTaxa + ' option:selected').attr('data-conv')).toFixed(2).replace('.', ',');
	var mensaDoc = $('#' + selTaxa + ' option:selected').attr('data-descricao');
	if (mensaDoc==undefined) {
		mensaDoc='Apresentar documento de identidade com foto.';
	}	
    var infoTaxa = '<small style="line-height:14px; display:block; margin-top:5px; position:relative; margin-left:5px;">Ingresso '+strValor+'<div class="informaPos">' + mensaDoc + '</div></small>';
    $(spanTaxa).html(infoTaxa)
    var _indice=$(selectIngressoAtual).data('indice');
    $("#ocupante-"+_indice+"-produto_unidades").attr('data-cupom',$(selectIngressoAtual).val());
}

function taxaErro(data){

}

function calcularQtd(){
    $(".campoQtde").each(function(e,o){
        _val=tiposIngresso[$(o).attr('data-cupom')];
        $(o).val(_val);
    })
}

function elementosLinhaProduto(de,para){
    //encontra a linha
    var divRow=$("#row-"+codigo+"-"+de)
    //encontra as divs de cada campo
    var campos=divRow.children();
    //em cada div, encontra os elementos a serem trocados
    var tagsOcupantes=['nome','documento','email','telefone'];
    var camposProduto=['produto_codigo','tag_ocupante','produto_unidades'];
    $.each(campos, function(i,campo){
        //alterar select de cupons e taxa
        var select = $(campo).find('#ingresso-'+codigo+'-'+de);
        var spanTaxa = $(campo).find('#taxa-'+codigo+'-'+de)
        $(select).attr('id','ingresso-'+codigo+'_'+para);
        $(select).attr('name','Ocupante['+para+'][cupom_codigo]');
        $(spanTaxa).attr('id','#taxa-'+codigo+'-'+para);
        
        var o=$(campo).children();
        $.each(tagsOcupantes, function(y,x){
            //encontrar a tag container do campo, label e input
            var containerClass='.field-'+x+'-'+codigo+'_'+de;
            var label = $(containerClass).find('label[for="'+x+'-'+codigo+'_'+de+'"]');
            var input = $(containerClass).find('#'+x+'-'+codigo+'_'+de);
            var labelSelect = $(campo).find('label[for="ingresso-'+codigo+'-'+de+'"]');
            
            //alterar atributos
            if($(o).hasClass('field-'+x+'-'+codigo+'_'+de))
                $(o).attr('class',$(o).attr('class').replace('field-'+x+'-'+codigo+'_'+de,'field-'+x+'-'+codigo+'_'+para));
            $(label).attr('for',x+'-'+codigo+'_'+para);
            $(input).attr('id',x+'-'+codigo+'_'+para);
            $(input).attr('name','Ocupante['+para+']['+x+']');
            $(labelSelect).attr('for','ingresso-'+codigo+'_'+para);
        })

        $.each(camposProduto, function(a,b){
            var containerClass='.field-ocupante-'+de+'-'+b;
            var input = $(containerClass).find('#ocupante-'+de+'-'+b);
            containerClassAttr=$(containerClass).attr('class');
            if(containerClassAttr!=undefined){
                $(containerClass).attr('class',containerClassAttr.replace('field-ocupante-'+de+'-'+b,'field-ocupante-'+para+'-'+b));
                $(input).attr('id','ocupante-'+para+'-'+b);
                $(input).attr('name','Ocupante['+para+']['+b+']');
            }
        })
    })
    // alterar botão de excluir
    var btDel=$(divRow).find('.div-trash2').children();
    $(btDel).attr('data-indice',para);
    $(btDel).attr('id','btn-del-'+codigo+'-'+para);
    //alterar id da linha depois de tudo
    $(divRow).attr('id',"row-"+codigo+"-"+para);
}

function continuarCompraOk(data){
    window.location.assign($("#url-evento").val());
}

function continuarCompraErr(data){

}

function salvarOcupantesVoltarParaEvento(){
    var separacaoGetVar=window.location.search=='' ?'?' :'&';
    doAjax ($('#form-ingresso').attr('action')+separacaoGetVar+'acao=continuar-compra', 'POST', $('#form-ingresso').serialize(), 'JSON', 10000, 'continuarCompraOk', 'continuarCompraErr');
    preventBeforeUnload=false;
}

window.onbeforeunload = function(evt){
    if(preventBeforeUnload)
        return 'Você tem alterações não salvas. Deseja sair?';
    else
        return null;
}

function consultaCupomOk(data){
    $("#consulta-cupom").button('reset');
    var selCupom=$("#"+$(btnModalOpen).data('produtoselect'));
    var valorInteira=$(selCupom).find('option:eq(0)').data('valor');
    valorDesconto = valorInteira-data.objeto.cupom.desconto_fixo;
    if(data.objeto.cupom.desconto_percentual>0)
        valorDesconto=valorInteira-(valorInteira*data.objeto.cupom.desconto_percentual/100);
    $(selCupom).append('<option data-valor="'+valorDesconto.toFixed(2)+'" data-descricao="'+data.objeto.cupom.descricao+'" value="'+data.objeto.cupom.id+'">'+data.objeto.cupom.nome+'</option>');
    $(selCupom).val(data.objeto.cupom.id);
    $(selCupom).trigger('change');
    $("#modal-cupom").modal('hide');
}

function consultaCupomErr(data){
    $("#consulta-cupom").button('reset');
    $("#consulta-cupom-retorno").text(data.erro.mensagem);
    $("#consulta-cupom-retorno").removeClass("hidden");
}
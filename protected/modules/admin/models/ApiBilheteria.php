<?php
namespace app\modules\admin\models;

use app\models\Api;
use app\models\ApiCarrinho;

/**
 * Classe para Bilheteria - Easy for Pay - Mineirão
 * obs.: parâmetros entre colchetes são opcionais
 */
class ApiBilheteria extends Api 
{
    public $logs;
    
    /**
     * Registrar qrcode
     * @param type $post
     * @return type
     */
    public function registrarQrCode($post=[])
    {
        $this->setLog('Registrar QR Code');
        if (!empty($post) && is_array($post)) {
            $this->logs->log('Registro de QR Code', $post);
            $returnApi  = json_decode($this->call('qrcode/', 'POST', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao registrar QR Code' : 'QR Code registrado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Registro de QR Code inválido.');
        return $this->getApiError('Não foi possível registrar o QR Code. Faltam informações para efetuar o procedimento.');
    }
    
    /**
     * Bloquear cartão de cliente
     * @param array $post documento (bloqueio de todos os qr-codes do portador do documento informado) / qrcode (bloqueia apenas o qrcode informado)
     */
    public function bloquearQrCode($post=[])
    {
        $this->setLog('Bloquear QR Code');
        if (!empty($post) && is_array($post)) {
            $this->logs->log('Bloqueio de QR Code', $post);
            $returnApi = json_decode('qrcode/' . (isset($post['documento']) ? $post['documento'] : $post['qrcode'] . '/' . $post['qrcode']), 'DELETE', $post);
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao bloquear QR Code' : 'QR Code bloqueado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Bloqueio de QR Code inválido.');
        return $this->getApiError('Não foi possível bloquear o QR Code. Faltam informações para efetuar o procedimento.');
    }    
    
    /**
     * Identificar todos os qr-codes registrados para um cliente
     * @param type $post
     * @return type
     */
    public function listarClienteQrCode($post=[])
    {
        $this->setLog('Listar QR Codes de um cliente');
        if (!empty($post) && is_array($post)) {
            $this->logs->log('Lista de QR Codes', $post);
            $returnApi = json_decode($this->call('qrcode/' . $post['user_id'] . '/lista', 'GET', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao listar QR Codes' : 'QR Codes identificados com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Lista de QR Codes inválida.');
        return $this->getApiError('Não foi possível identificar os QR Codes deste cliente. Faltam informações para efetuar o procedimento.');
    }
    
    /**
     * Validar um qr-code
     * @todo verificar em tabela "repositório" ou do evento
     * @param type $post
     */
    public function validarQrCode($post)
    {
        $this->setLog('Validar QR Code');
        if (!empty($post) && is_array($post)) {
            $this->logs->log('Validação de QR Code', $post);
            $returnApi = json_decode($this->call('qrcode/' . base64_encode($post['qrcode']), 'GET', NULL, false, 'A'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao validar QR Code' : 'QR Code validado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Validação de QR Code inválida.');
        return $this->getApiError('Não foi possível validar o QR Code informado. Faltam informações para efetuar o procedimento.');
    }
    
    /**
     * Validar ingresso
     * @todo checar com Roth diferença deste endpoint e do qrcode/
     * @param type $post
     * @return type
     */
    public function validarIngresso($post=[])
    {
        $this->setLog('Validar ingresso');
        if (!empty($post) && is_array($post)) {
            $this->logs->log('Validação de ingresso', $post);
            $returnApi = json_decode($this->call('validar/' . $post['qrcode'], 'GET', null, true, 'A'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao validar ingresso' : 'Ingresso validado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Validação de ingresso inválida.');
        return $this->getApiError('Não foi possível validar ingresso. Faltam informações para efetuar o procedimento.');
    }
    
    /**
     * Marcar ingresso como utilizado
     * @todo salvar em tabela evento
     * @param type $post
     */
    public function marcarIngresso($post=[])
    {
        $this->setLog('Marcar ingresso');
        if (!empty($post) && is_array($post)) {
            $this->logs->log('Marcação de ingresso', $post);
            $returnApi = json_decode($this->call('validar/' . $post['qrcode'], 'POST', null, true, 'A'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao marcar ingresso' : 'Ingresso marcado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Marcação de ingresso inválida.');
        return $this->getApiError('Não foi possível marcar ingresso. Faltam informações para efetuar o procedimento.');
    }
    
    /**
     * Ao ler um qr-code ou informar um documento, consultar e validar compra correspondente
     */
    public function checkCompra ($post=[]) 
    {
        $this->setLog('Check compra');
        if (!empty($post) && isset($post['documento']) && !empty($post['documento'])) {
            $returnApi = (new ApiCarrinho)->consultarVendasFiltro(['comprador' => $post['documento'], 'codigo' => (isset($post['id_venda']) ? $post['id_venda'] : '')]);
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao checar compra' : 'Compra checada com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Check de compra inválida.');
        return $this->getApiError('Não foi possível checar compra. Faltam informações para efetuar o procedimento.');
    }
}
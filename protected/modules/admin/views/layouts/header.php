<?php
use yii\helpers\Url;
use yii\helpers\Html;

$session = Yii::$app->session;
$session->open();
?>
<div class="barrSecAdm">
	<div>
            <a href="<?= Url::to(['//admin'])?>" class="logoTop">
                <?= Html::img('@web/images/logoMineirapB.png', ['alt' => 'Estádio Mineirão', 'title' => 'Estádio Mineirão', 'border' => '0', 'class' => 'logobranco']); ?>
            </a>
	</div>
	<div>
            <h3><?=$this->title?></h3>
	</div>
</div>
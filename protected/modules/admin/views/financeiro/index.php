<?php
use app\models\Mineiraocard;

$this->title='ADM Estádio Mineirão :: Financeiro';
?>
<div class="container-fluid">
	<ul class="nav nav-tabs" role="tabsFinanceiro">
		<li role="presentation" class="active">
			<a href="#financeiro_sumarioExecutivo" aria-controls="financeiro_sumarioExecutivo" role="tab" data-toggle="tab">Sumário Executivo</a>
		</li>
		<li role="presentation">
			<a href="#financeiro_visaoGeral" aria-controls="financeiro_visaoGeral" role="tab" data-toggle="tab">Visão Geral</a>
		</li>
	</ul>
	<div class="tab-content tabRelato">
		<div role="tabpanel" class="tab-pane active" id="financeiro_sumarioExecutivo">
			<table id="tabelaSumarioTipos" class="table table-bordered table-condensed table-responsive table-hover">
				<thead>
					<tr>
						<th>VENDAS BRUTAS POR TIPO DE TRANSAÇÃO</th>
						<?php foreach ($periodos as $key => $value) {
							echo '<th>'.$value.'</th>';
						}?>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Transações por cartão</td>
						<?php foreach ($periodos as $key => $value) {
							echo '<td>R$ '.number_format($sumario['tipos'][$value]['cc']['receita'],2,',','.').'</td>';
						}?>
					</tr>
					<tr>
						<td>Transações por boletos</td>
						<?php foreach ($periodos as $key => $value) {
							echo '<td>R$ '.number_format($sumario['tipos'][$value]['boleto']['receita'],2,',','.').'</td>';
						}?>
					</tr>
					<tr>
						<td>Transações por depósito/TED/DOC</td>
						<?php foreach ($periodos as $key => $value) {
							echo '<td>R$ '.number_format($sumario['tipos'][$value]['deposito']['receita'],2,',','.').'</td>';
						}?>
					</tr>
					<tr>
						<td>Outros</td>
						<?php foreach ($periodos as $key => $value) {
							echo '<td>R$ '.number_format($sumario['tipos'][$value]['outros']['receita'],2,',','.').'</td>';
						}?>
					</tr>
				</tbody>
			</table>

			<table id="tabelaSumarioProdutos" class="table table-bordered table-condensed table-responsive table-hover">
				<thead>
					<tr>
						<th colspan="2">VENDAS BRUTAS POR PRODUTO</th>
						<?php foreach ($periodos as $key => $value) {
							echo '<th>'.$value.'</th>';
						}?>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($model->produtos as $area=>$produtos) {?>
						<?php foreach ($produtos as $produto=>$info) {?>
							<tr>
								<td rowspan="8"><p class="text_medium"><?=$produto?></p></td>
							</tr>
							<tr>
								<td>Valor</td>
								<?php foreach ($periodos as $key => $value) {
									echo '<td>R$ '.number_format($sumario['produtos'][$value][$area][$produto]['valor'],2,',','.').'</td>';
								}?>
							</tr>
							<tr>
								<td>Assentos Vendidos</td>
								<?php foreach ($periodos as $key => $value) {
									echo '<td>'.$sumario['produtos'][$value][$area][$produto]['vendidos'].'</td>';
								}?>
							</tr>
							<tr>
								<td>Vendas Acumuladas</td>
								<?php foreach ($periodos as $key => $value) {
									echo '<td>'.$sumario['produtos'][$value][$area][$produto]['vendas'].'</td>';
								}?>
							</tr>
							<tr>
								<td>Capacidade do Setor</td>
								<?php foreach ($periodos as $key => $value) {
									echo '<td>'.$sumario['produtos'][$value][$area][$produto]['capacidade'].'</td>';
								}?>
							</tr>
							<tr>
								<td>Bloqueados para Venda</td>
								<?php foreach ($periodos as $key => $value) {
									echo '<td>'.$sumario['produtos'][$value][$area][$produto]['bloqueados'].'</td>';
								}?>
							</tr>
							<tr>
								<td>Disponíveis para Venda</td>
								<?php foreach ($periodos as $key => $value) {
									echo '<td>'.$sumario['produtos'][$value][$area][$produto]['disponiveis'].'</td>';
								}?>
							</tr>
							<tr>
								<td>Taxa de Ocupação %</td>
								<?php foreach ($periodos as $key => $value) {
									echo '<td>'.$sumario['produtos'][$value][$area][$produto]['ocupacao'].'</td>';
								}?>
							</tr>
						<?php }?>
					<?php }?>
				</tbody>
			</table>
		</div>
		<div role="tabpanel" class="tab-pane" id="financeiro_visaoGeral">
			<?php
			foreach ($vendas as $mesAno=>$transacoes) {?>
				<h3><?php
				$mes=DateTime::createFromFormat('m-Y',$mesAno);
				echo $mes->format('M Y')?></h3>
				<table id="tabelaGeralFinanceiro" class="table table-bordered table-condensed table-responsive table-hover">
					<thead>
						<tr>
							<th>Código venda</th>
							<th>Data da vendas</th>
							<!--<th>codigo_status</th>-->
							<th>status</th>
							<!--<td>valor</th>
							<th>unidades</th>-->
							<th>total</th>
							<!--<th>tipo</th>
							<th>validade</th>-->
							<th>Parcelas</th>
							<th>Origem da venda</th>
							<!--<th>produtos</th>-->
							<th>ID de Conciliação</th>
							<th>Nome do comprador</th>
							<th>Documento do comprador</th>
							<th>E-mail do comprador</th>
							<th>Telefone do comprador</th>
							<th>CEP do comprador</th>
							<th>Endereço do comprador</th>
							<th>Número</th>
							<th>Complemento</th>
							<th>Bairro</th>
							<th>Cidade</th>
							<th>Estado</th>
							<th>Bandeira do cartão</th>
							<!--<th>deposito_data</th>
							<th>deposito_valor</th>
							<th>id_carrinho</th>-->
						</tr>
					</thead>
					<tbody>
						<?php foreach ($transacoes as $v) {?>				
							<tr class="<?= $v->vendas->codigo_status == 1 ?'success' :'warning'?>">
								<td><?=$v->vendas->codigo?></td>
								<td><?=$v->vendas->data?></td>
								<!--<td><?=$v->vendas->codigo_status?></td>-->
								<td><?=$v->vendas->codigo_status?> - <strong><?=$v->vendas->status?></strong></td>
								<!--<td><?=$v->vendas->valor?></td>
								<td><?=$v->vendas->unidades?></td>-->
								<td>R$ <?=number_format($v->vendas->total,2,'.',',')?></td>
								<!--<td><?=$v->vendas->tipo?></td>
								<td><?=$v->vendas->validade?></td>-->
								<td><?=$v->vendas->forma?></td>
								<td><?=$v->vendas->descricao?></td>
								<!--<td><?=$v->vendas->produtos?></td>-->
								<td><?=$v->vendas->id_conciliacao?></td>
								<td><?=$v->vendas->comprador->nome?></td>
								<td><?=$v->vendas->comprador->documento?></td>
								<td><?=$v->vendas->comprador->email?></td>
								<td><?=$v->vendas->comprador->telefone?></td>
								<td><?=$v->vendas->comprador->cep?></td>
								<td><?=$v->vendas->comprador->endereco?></td>
								<td><?=$v->vendas->comprador->numero?></td>
								<td><?=$v->vendas->comprador->complemento?></td>
								<td><?=$v->vendas->comprador->bairro?></td>
								<td><?=$v->vendas->comprador->cidade?></td>
								<td><?=$v->vendas->comprador->uf?></td>
								<td><?=$v->vendas->comprador->cartao->bandeira?></td>
								<!--<td><?=$v->deposito->data?></td>
								<td><?=$v->deposito->valor?></td>					
								<td><?=$v->id_carrinho?></td>-->					
							</tr>					
							<?php if($v->carrinho->successo) {?>				
								<?php foreach($v->carrinho->objeto[0]->produtos as $produto) {
									$ocupante=Mineiraocard::ocupanteIngressoObjetoProduto($produto);
									?>
									<tr class="active">
										<td>&nbsp;</td>
										<td colspan="2">Código do produto: <?=$produto->codigo?></td>
										<td colspan="4">Nome do produto: <?=$produto->nome?></td>
										<td colspan="12">Ocupante: 
											<strong>Nome: </strong><?=$ocupante->nome?>, 
											<strong>E-mail: </strong><?=$ocupante->email?>, 
											<strong>Documento: </strong><?=$ocupante->doc?>, 
											<strong>Telefone: </strong><?=$ocupante->tel?>
										</td>
									</tr>				
								<?php }	?>
							<?php }	?>
						<?php }	?>
					</tbody>
				</table>
			<?php }	?>
		</div>
	</div>
</div>
<br>
<br>
<br>
<br>
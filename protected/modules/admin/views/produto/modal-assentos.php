<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use yii\bootstrap\Modal;

Modal::begin([
    'header' => '<h4 align="center" id="title-assento">Assentos</h4>',
    'size'   => 'modal-md',
    'id'     => 'modal-assento',
    'options'=> [
        'class' => 'modal-center'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ]
]);

$tags->scenario = 'create';

$form = ActiveForm::begin([
    'id' => 'assento-form', 
    'action' => Url::to(['produto/novo-assento'])
]); ?>

<?php $model->ativo = '1'; ?>

<?= $form->field($model, 'id_agenda')->hiddenInput(['id' => 'id_agenda'])->label(false); ?>
<?= $form->field($model, 'nome')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'valor')->hiddenInput(['value' => '0'])->label(false); ?>
<?= $form->field($model, 'descricao')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'ativo')->radioList(['1' => 'Ativo', '0' => 'Inativo'])->label('Status'); ?>
<?= $form->field($tags, '[0]nome')->hiddenInput(['value' => 'matriz'])->label(false); ?>
<?= $form->field($tags, '[0]valor')->hiddenInput(['id' => 'tag-matriz'])->label(false); ?>
<?= $form->field($tags, '[1]nome')->hiddenInput(['value' => 'setor'])->label(false); ?>
<?= $form->field($tags, '[1]valor')->hiddenInput(['id' => 'tag-setor'])->label(false); ?>
<?= $form->field($tags, '[2]nome')->hiddenInput(['value' => 'fileira'])->label(false); ?>
<?= $form->field($tags, '[2]valor')->hiddenInput(['id' => 'tag-fileira'])->label(false); ?>
<?= $form->field($tags, '[3]nome')->hiddenInput(['value' => 'tipo'])->label(false); ?>
<?= $form->field($tags, '[3]valor')->hiddenInput(['value' => 'assento'])->label(false); ?>

<div class="row">
    <div class="col-xs-6">
        <?= $form->field($tags, '[4]nome')->hiddenInput(['value' => 'assento'])->label(false); ?>
        <?= $form->field($tags, '[4]valor')->textInput()->widget(MaskedInput::className(), [
            'mask' => '99',
            'options' => [
                'class' => 'form-control',
                'placeholder' => 'Número inicial do assento',
                'title' => 'Número inicial do assento',
                'id' => 'assento-ini'
            ],
            'clientOptions' => [
                'removeMaskOnSubmit' => true
            ]
        ])->label('do assento:');?>
    </div>
    <div class="col-xs-6">
        <?= $form->field($tags, '[5]nome')->hiddenInput(['value' => 'assento'])->label(false); ?>
        <?= $form->field($tags, '[5]valor')->textInput()->widget(MaskedInput::className(), [
            'mask' => '99',
            'options' => [
                'class' => 'form-control',
                'placeholder' => 'Número final do assento',
                'title' => 'Número final do assento',
                'id' => 'assento-fim'
            ],
            'clientOptions' => [
                'removeMaskOnSubmit' => true
            ]
        ])->label('até assento:');?>
    </div>
</div>

<?= Html::submitButton('Cadastrar', ['class' => 'btn btn-primary', 'id' => 'btn-novo-assento']); ?>

<?php ActiveForm::end(); ?>

<?php Modal::end(); ?>
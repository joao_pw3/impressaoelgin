<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

Modal::begin([
    'header' => '<h4 align="center">Setor</h4>',
    'size'   => 'modal-md',
    'id'     => 'modal-setor',
    'options'=> [
        'class' => 'modal-center'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ]
]);

    $tags->scenario = 'create';

    $form = ActiveForm::begin([
        'id' => 'setor-form', 
        'action' => Url::to(['produto/novo-setor'])
    ]); ?>

    <?php $model->ativo = '1'; ?>

    <?= $form->field($model, 'id_agenda')->hiddenInput(['id' => 'id_agenda'])->label(false); ?>
    <?= $form->field($model, 'nome'); ?>
    <?= $form->field($model, 'valor')->hiddenInput(['value' => '0'])->label(false); ?>
    <?= $form->field($model, 'descricao')->textarea(['rows' =>5]); ?>
    <?= $form->field($model, 'ativo')->radioList(['1' => 'Ativo', '0' => 'Inativo'])->label('Status'); ?>
    <?= $form->field($tags, '[0]nome')->hiddenInput(['value' => 'matriz'])->label(false); ?>
    <?= $form->field($tags, '[0]valor')->hiddenInput(['id' => 'tag-matriz'])->label(false); ?>
    <?= $form->field($tags, '[1]nome')->hiddenInput(['value' => 'tipo'])->label(false); ?>
    <?= $form->field($tags, '[1]valor')->hiddenInput(['value' => 'setor'])->label(false); ?>
    <?= $form->field($tags, '[2]nome')->hiddenInput(['value' => 'setor'])->label(false); ?>
    <?= $form->field($tags, '[2]valor')->textInput(['id' => 'id-setor'])->label('Identificador do setor'); ?>

    <?= Html::submitButton('Cadastrar', ['class' => 'btn btn-primary', 'id' => 'btn-novo-setor']); ?>

    <?php ActiveForm::end(); ?>

<?php Modal::end(); ?>
var formulario = ''; 
var bloco = '';

$(document).ready(function () {
    
    $('#blocos').change(function () {
        if ($('#blocos option:selected').hasClass("camarote")) {
            addProduto($('#blocos option:selected').val(), $('#blocos option:selected').prop("id"));
        } else {
            $('#boxAssentos').removeClass('esconde');
            listaLugaresReserva($('#blocos option:selected').prop("id"));
        }        
    });
    
    $(document).on('click', '.reserva-assento', function () {
        $('.historico').addClass('hidden');
        $('#reserva-codigo').val($(this).attr('id'));
        $('#reserva-produto').val($(this).attr('data-produto'));
        $('#reserva-status').val($(this).attr('data-ativo'));
        $('#reserva-disponivel').val($(this).attr('data-disponivel'));
        $('#reserva-reservado').val($(this).attr('data-reservado'));
        $('#reserva-vendido').val($(this).attr('data-vendido'));
        $('#reserva-documento').val($(this).attr('data-documento'));
        $('.res-produto').html($(this).attr('data-produto') + ' - ' + $('#blocos option:selected').prop('id').replace('_', ' '));
        $('#res-oferecido').html($(this).attr('data-oferecido'));
        $('#res-disponivel').html($(this).attr('data-disponivel'));
        $('#res-vendido').html($(this).attr('data-vendido'));
        $('#res-reservado').html($(this).attr('data-reservado'));
        $('#res-status').html($(this).attr('data-ativo') == 1 ? 'desbloqueado' : 'bloqueado');
        if ($(this).attr('data-reserva') != '') {
            $('#res-cliente').html($(this).attr('data-reserva'));
            $('#res-documento').val($(this).attr('data-reserva')).attr('readonly', true);
            $('#reserva-documento').val($(this).attr('data-reserva'));
            $('#btn-reservar').html('Remover reserva');
        } else {
            $('#res-cliente').html('');
            $('#res-documento').attr('readonly', false);
            $('#reserva-documento').val('');
            $('#btn-reservar').html('Reservar assento');
        }
        $('#bloq-' + $(this).attr('id')).removeClass('hidden');
        $('#res-' + $(this).attr('id')).removeClass('hidden');
        $('#btn-reservar').attr('disabled', $(this).attr('data-disponivel') == '0' || $(this).attr('data-reservado') == '1' ? true : false);
        $('#btn-status').attr('disabled', $(this).attr('data-reservado') == '1' ? true : false);
        $('#reserva-justificativa').attr('disabled', $(this).attr('data-disponivel') == '0' || $(this).attr('data-reservado') == '1' ? true : false);
        $('#res-documento').attr('disabled', $(this).attr('data-disponivel') == '0' || $(this).attr('data-reservado') == '1' ? true : false);
        $('#modal-reserva').modal('toggle');
    });
    
    $('#btn-status').click(function () {
        if ($('#reserva-justificativa').val() == '') {
            showMsg('Por favor, justifique a operação.');
            return false;
        }
        mudarStatus('reserva');
    });
    
    $('#btn-reservar').click(function () {
        if ($('#reserva-justificativa').val() == '') {
            showMsg('Por favor, justifique a operação.');
            return false;
        } 
        if ($('#reserva-disponivel').val() == '0') {
            showMsg('Não há produto disponível para reserva.');
            return false;
        }
        $('#res-documento').val($('#res-documento').val().replace(/[^0-9]/gi, ''));
        if ($('#res-documento').val() == '') {
            showMsg('Por favor, preencha o documento do cliente');
            return false;
        }
        if ($('#res-documento').val().length == 11) {
            doAjax ($('#url-check-user').val(), 'POST', {'user_id': $('#res-documento').val()}, 'json', 10000, 'userOK', 'userErro');
        } else {
            fazerReserva();
        }
    });
    
    $(document).on('click', '.reserva-bloco', function() {
        $('.historico-bloco').addClass('hidden');
        $('#bloco-codigo').val($(this).attr('id'));
        $('#span-produto').html($(this).attr('data-produto'));
        $('#span-status').html($(this).attr('data-ativo') == '0' ? 'inativo' : 'ativo');
        $('#bloco-status').val($(this).attr('data-ativo'));
        $('#bloc-bloq-' + $(this).attr('id')).removeClass('hidden');
        $('#modal-blocos').modal('toggle');
    });
    
    $('#btn-bloco-status').click(function () {
        if ($('#bloco-justificativa').val() == '') {
            showMsg('Por favor, justifique a operação.');
            return false;
        }
        mudarStatus('bloco');
    });
    
    $(document).on('click', '#btn-cancel', function () {
        $('#modal-confirmar').modal('hide');
        return false;
    });
    
    $(document).on('click', '#btn-continue', function (e) {
        e.preventDefault();
        fazerReserva();
    });
    
    $("#vendas-mostrar-tudo-btn").click(function(){
        if($(this).hasClass('mostrando-nao-autorizadas')){
            $("#tabela-vendas-transacoes tr.nao-processada").addClass("hidden");
            $(this).text('Mostrar transações não autorizadas').removeClass('mostrando-nao-autorizadas');
        } else {
            $("#tabela-vendas-transacoes tr.nao-processada").removeClass("hidden");
            $(this).text('Esconder transações não autorizadas').addClass('mostrando-nao-autorizadas');
        }
    });
});

/**
 * buscar os assentos de um bloco pela API para fazer a reserva
 * @param string id do bloco
 */ 
function listaLugaresReserva(id) {
    if (id == undefined || id == null || id == '') { 
        $('#div-assento').html('Escolha um bloco/camarote acima.');
        return false;
    }
    splId = id.split('_');
    var matriz = $('#matriz-produtos').val();
    var area = $('#area-estadio').val();
    var tipo = 'assento';
    bloco = splId[1];
    var post = [{'nome':'matriz', 'valor':matriz}, {'nome':'area', 'valor':area}, {'nome':'tipo', 'valor':tipo}, {'nome':'bloco', 'valor':bloco}];
    doAjax ($('#url-get-produto-tags').val(), 'POST', {post:post}, 'json', 10000, 'assentosReserva', 'erroProdutoTabela');
}

function assentosReserva (data) {
    if (data.successo != 1) return;
    var fileira = '';
    var strSvg = '';
    var y = 20;
    var x = 60;
    var fileiraYPlus = 40;
    var l = data.objeto.length;
    if (bloco == '128' || bloco == '111' || bloco == '130' || bloco == '110' || bloco == '109' || bloco == '108' || bloco == '106' || bloco == 'vip106') { 
        $('#map-svg').html('');
        $('#map-div').load($('#url-map').val() + bloco + '.html', function() {
            for (i = 0; i < l; i++) {
                var nomeTmp = data.objeto[i].nome.split(' ');
                var id = nomeTmp[3] + nomeTmp[1];
                var isReserva = data.objeto[i].estoque.reserva_exclusiva == '' ? '' : data.objeto[i].estoque.reserva_exclusiva;
                var corAssento = (parseInt(data.objeto[i].estoque.disponivel) == 0 && parseInt(data.objeto[i].estoque.vendido) > 0 ? 'fillRed' : 
                    (parseInt(data.objeto[i].estoque.disponivel) == 0 && parseInt(data.objeto[i].estoque.reservado) > 0 ? 'fillYellow' : 
                    (isReserva != '' ? 'fillOrange' : 
                    (data.objeto[i].ativo == '0' ? 'fillGray' : 'fillGreen'))));
                var corTexto = corAssento == 'fillGray' || corAssento == 'fillGreen' ? 'fntWhite' : 'fntBold';
                var strTitle = corAssento == 'fillRed' ? 'Produto vendido' : 
                        (corAssento == 'fillGray' ? 'Assento bloqueado' : 
                        (corAssento == 'fillYellow' ? 'Produto reservado (carrinho)' : 
                        (corAssento == 'fillOrange' ? 'Produto reservado (cliente)' : 'Produto disponível')));
                $('#' + id + ' circle').removeClass('fillWhite').addClass(corAssento);
                $('#' + id + ' circle').after('<title id="title-' + id + '">' + strTitle + '</title>');
                var content = $('#' + id)[0].outerHTML;
                $('#' + id).html('');
                content = '<a xlink:href="javascript:;" id="' + data.objeto[i].codigo + '" class="reserva-assento" data-ativo="' + data.objeto[i].ativo + '" \n\
                        data-disponivel="' + parseInt(data.objeto[i].estoque.disponivel) + '" data-oferecido="' + parseInt(data.objeto[i].estoque.oferecido) + '" \n\
                        data-reservado="' + parseInt(data.objeto[i].estoque.reservado) + '" data-vendido="' + parseInt(data.objeto[i].estoque.vendido) + '" \n\
                        data-produto="' + data.objeto[i].tags.fileira + data.objeto[i].tags.cadeira + '" data-reserva="' + isReserva + '" \n\
                        data-unico="' + id + '">\n\ ' +
                        content + '</a>';
                $('#' + id).html(content);
                identificarHistorico(data.objeto[i].codigo, data.objeto[i].tags);
            }  
        });
    } else {
        $('#map-div').html('');
        for (i = 0; i < l; i++) {
            if (fileira != data.objeto[i].tags.fileira) {
                yColuna = 25 + fileiraYPlus;
                y = y + fileiraYPlus;
                x = 60;
                strSvg += '<g><rect x="5" y="' + (y - 18) + '" width="30" height="30" fill="#0e4d8e" stroke-width="1" stroke="#0e4d8e" />\n\
                    <title id="title-' + data.objeto[i].tags.fileira + '">Fileira ' + data.objeto[i].tags.fileira + '</title>\n\
                    <text x="15" y="' + (y + 5) + '" fill="#fff">' + data.objeto[i].tags.fileira + '</text></g>';
                fileira = data.objeto[i].tags.fileira;
            }
            var isReserva = data.objeto[i].estoque.reserva_exclusiva == '' ? '' : data.objeto[i].estoque.reserva_exclusiva;
            var corAssento = (parseInt(data.objeto[i].estoque.disponivel) == 0 && parseInt(data.objeto[i].estoque.vendido) > 0 ? 'red' : 
                (parseInt(data.objeto[i].estoque.disponivel) == 0 && parseInt(data.objeto[i].estoque.reservado) > 0 ? 'yellow' : 
                (isReserva != '' ? 'orange' : 
                (data.objeto[i].ativo == '0' ? 'gray' : 'green'))));
            var corTexto = corAssento == 'gray' || corAssento == 'green' ? 'white' : 'black';
            var strTitle = corAssento == 'red' ? 'Produto vendido' : 
                    (corAssento == 'gray' ? 'Assento bloqueado' : 
                    (corAssento == 'yellow' ? 'Produto reservado (carrinho)' : 
                    (corAssento == 'orange' ? 'Produto reservado (cliente)' : 'Produto disponível')));
            strSvg += '<g id="assento-' + data.objeto[i].codigo + '">\n\
                <circle fill="' + corAssento +  '" stroke="#777" stroke-width="1" cx="' + x  + '" cy="' + y + '" r="15"/>\n\
                <title id="title-' + data.objeto[i].codigo + '">' + strTitle + '</title>\n\
                <a xlink:href="javascript:;" id="' + data.objeto[i].codigo + '" class="reserva-assento" data-ativo="' + data.objeto[i].ativo + '" \n\
                    data-disponivel="' + parseInt(data.objeto[i].estoque.disponivel) + '" data-oferecido="' + parseInt(data.objeto[i].estoque.oferecido) + '" \n\
                    data-reservado="' + parseInt(data.objeto[i].estoque.reservado) + '" data-vendido="' + parseInt(data.objeto[i].estoque.vendido) + '" \n\
                    data-produto="' + data.objeto[i].tags.fileira + data.objeto[i].tags.cadeira + '" data-reserva="' + isReserva + '">\n\
                    <text x="' + (x - 8) + '" y="' + (y + 5) + '" fill="' + corTexto + '">' + data.objeto[i].tags.cadeira + '</text>\n\
                </a>\n\
                </g>';
            x += fileiraYPlus;
            identificarHistorico(data.objeto[i].codigo, data.objeto[i].tags);
        }
        $('#map-svg').html(strSvg);
    }
    $('#boxAssentos').removeClass('esconde');
}

/**
 * Identificar histórico de ativações/bloqueios e reservas/liberações
 * @param integer produto Id do produto
 * @param array tags Tags registradas no produto
 * @returns {Boolean}
 */
function identificarHistorico(produto, tags) {
    var bloqueio = '<div class="titHistorico">Histórico de Bloqueio:</div><hr class="ricoBloqueio">';
    var reserva = '<div class="titHistorico">Histórico de Reserva:</div><hr class="ricoBloqueio">';
    var dataOld = '';
    $.each(tags, function(key, value) {
        if (key.indexOf('reservar_') > -1 || key.indexOf('reservarjust_') > -1 || key.indexOf('liberar_') > -1 || key.indexOf('liberarjust_') > -1){
            var arrTmp = key.split('_');
            var dataLog = formatarData(arrTmp[1]); 
            reserva += dataOld == '' ? '' : (dataOld == dataLog ? '<br>' : '<hr class="ricoBloqueio">');
            reserva += dataLog + ' - ' + (arrTmp[0] == 'reservarjust' || arrTmp[0] == 'liberarjust' ? 'justificativa' : arrTmp[0]) + 
                       ':<br>' + value;
            dataOld = dataLog;
        } else if (key.indexOf('bloqueio_') > -1 || key.indexOf('desbloqueio_') > -1) {
            var arrTmp = key.split('_');
            bloqueio += formatarData(arrTmp[1]) + ' - ' + arrTmp[0] + ':<br>' + value + '<hr class="ricoBloqueio">';
        }
    });
    if (reserva != '<div class="titHistorico">Histórico de Reserva:</div><hr class="ricoBloqueio">') {
        $('#historico-reserva').append('<div class="hidden select-lugares historico boxHistoria" id="res-' + produto + '">' + reserva + '</div>');
    }
    if (bloqueio != '<div class="titHistorico">Histórico de Bloqueio:</div><hr class="ricoBloqueio">') {
        $('#historico-bloqueio').append('<div class="hidden select-lugares historico boxHistoria" id="bloq-' + produto + '">' + bloqueio + '</div>');
    }
}

/**
 * formatar data de logs no padrão brasileiro: dd/mm/YYYY HH:ii:ss
 * @param string data
 * @returns string
 */
function formatarData (data) {
    var dataFormatada = '';
    if (data != '') {
        dataFormatada = data.substr(6, 2) + '/' + data.substr(4, 2) + '/' + data.substr(0, 4) + ' ' + data.substr(8, 2) + ':' + data.substr(10, 2) + ':' + data.substr(12, 2);
    }
    return dataFormatada;
}

function mudarStatus (formName) {
    formulario = formName;
    doAjax ($('#url-status').val(), 'POST', $('#form-' + formulario).serialize(), 'json', 10000, 'statusOK', 'statusErro');
}

function statusOK (data) {
    showMsg('Status alterado com sucesso.');
    if (formulario == 'reserva') {
        $('#reserva-status').val($('#reserva-status').val() == 1 ? 0 : 1);
        $('#res-status').html($('#res-status').html() == 'bloqueado' ? 'desbloqueado' : 'bloqueado');
        $('#modal-mensagem').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            $('#modal-reserva').modal('hide');
        });
        
    } else {
        $('#modal-mensagem').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            $('#modal-blocos').modal('hide');
        });
    }
    $('#modal-reserva, #modal-blocos').on('hidden.bs.modal', function (e) {
        e.preventDefault();
        window.location.reload();
    });
    return false;
}

function statusErro (data) {
    showMsg('Erro ao alterar status. Erro: ' + data.erro.mensagem);
    return false;
}

function fazerReserva () {
    $('#modal-confirmar').modal('hide');
    doAjax ($('#url-reserva').val(), 'POST', $('#form-reserva').serialize(), 'json', 5000, 'reservaOK', 'reservaErro');
}

function reservaOK (data) {
    showMsg('Reserva ' + ($('#reserva-documento').val() == '' ? 'efetuada' : 'liberada') + ' com sucesso.');
    $('#modal-mensagem').on('hidden.bs.modal', function (e) {
        e.preventDefault();
        $('#modal-reserva').modal('hide');
    });
    $('#modal-reserva').on('hidden.bs.modal', function (e) {
        e.preventDefault();
        window.location.reload();
    });
    return false;
}

function reservaErro (data) {
    showMsg('Erro ao fazer reserva. Erro: ' + data.erro.mensagem);
    return false;
}

function userOK (data) {
    fazerReserva();
    return false;
} 

function userErro (data) {
    if (data.hasOwnProperty('objeto') && data.objeto.nome_comprador != '' && data.erro.codigo == '007') {
        $('#modal-confirmar .modal-body').html('Este procedimento será feito para:<br><span class="reserva-text">Nome: ' + data.objeto.nome_comprador + '<br>E-mail: ' + data.objeto.conta_usuario + '</span>');
    } else {
        $('#modal-confirmar .modal-body').html('Não há cliente registrado no sistema com este documento. O seu portador deverá cadastrar-se no sistema para utilizar esta reserva. Confirma?');
    }
    $('#modal-confirmar').modal('toggle');
}
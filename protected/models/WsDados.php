<?php
namespace app\models;

use app\models\Api;

/**
 * Classe para Dados - Easy for Pay
 * Repositório de dados via API
*/

class WsDados extends Api
{
    /**
     * Método construtor - obrigado a passar o token do vendedor para separação de unidades
     */
    public function __construct($tokenVendedor='')
    {
        if (empty($tokenVendedor)) {
            throw new \Exception("Token vendedor não foi declarado", 1);        
        }
        $this->setToken($tokenVendedor, 'V');
    }

    /**
     * 1 - Incluir / alterar: POST .../dados (Token empresa / operador)
        {
          "id":"X(128)",
          "tipo":"X(128)",
          "valor":"T(64k)",
          "permissao":"X(64)"
        }
    */
    public function salvar($post=[]) {
        return json_decode($this->call('dados/', 'POST', $post));
    }

    /**
     * 2 - Consultar: PUT .../dados (Token empresa / operador)
        {
          "id":"X(128)",
          "tipo":"X(128)"
        }
    */
    public function consultar($post=[]) {
        return json_decode($this->call('dados/', 'PUT', $post));
    }
}
<?php
use app\models\ApiProduto;
use yii\helpers\Html;
use yii\helpers\Url;

$session = Yii::$app->session;
$session->open(); 
?>

<input type="hidden" id="url-add-cart" value="<?= Url::to(['site/add-carrinho']); ?>">
<input type="hidden" id="url-del-cart" value="<?= Url::to(['site/del-carrinho']); ?>">
<input type="hidden" id="url-resumo" value="<?= Url::to(['site/resumo']); ?>">
<input type="hidden" id="url-visitante" value="<?= Url::to(['site/visitante']); ?>">
<input type="hidden" id="user" value="<?= isset($session['user_token']) && !empty($session['user_token']) ? 1 : 0; ?>">
<input type="hidden" id="documento" name="documento" value="<?= $session['documento']; ?>">

  <div class="col-md-8 col-sm-12 col-xs-12 nopadding">
    <div id="mapaRoxo">
        <div class="titArea">
			<div class="celTabLeft">
				<h4>Mineirão Tribuna – Setor Roxo</h4>
				Escolha o BLOCO que você deseja comprar (passe o mouse sobre o mapa e clique para selecionar).<br><br>
			</div>
			<div class="celTabRight">
				<?= Html::img($evento->anexos['bannervitrine']); ?>
			</div>
        </div>

        <div class="fundoEstadio">
            <div class="campoDiv">
                <?= Html::img('@web/images/campo2.jpg', ["class" => "campoP"]); ?>
            </div>
            <?php
            $mapa='<path rel="14214" class="bloqueado bloco" id="bloco_101" d="M350 212c20,9 40,17 60,25 7,3 14,5 20,8l25 -53 81 -172c-17,-7 -34,-14 -51,-21l-169 151 34 60z"/><path rel="14215" class="bloqueado bloco" id="bloco_102" d="M536 21l-81 172c11,3 22,6 33,9 15,4 30,8 46,11 15,4 31,7 46,11 10,2 20,5 30,7l61 -164c-46,-14 -90,-29 -134,-45z"/><path rel="14216" class="bloqueado bloco" id="bloco_103" d="M670 66l-61 164 16 4c15,3 31,7 46,10 15,3 31,6 46,9 15,3 31,6 46,8 12,2 23,4 35,6l40 -159c-58,-12 -115,-26 -170,-42z"/><path rel="14217" class="bloqueado bloco" id="bloco_104" d="M840 108l-40 159 12 2c16,2 31,5 47,7 16,2 31,4 47,6 16,2 32,4 47,6 16,2 31,3 47,5l21 -157c-62,-7 -122,-16 -181,-28z"/><path rel="14218" class="bloqueado bloco" id="bloco_105" d="M1000 293l0 0c49,5 100,8 154,9l7 -157c-47,-2 -94,-5 -140,-10l-21 157z"/><path rel="14219" class="bloqueado bloco" id="bloco_106" d="M1161 146l-7 157c62,3 120,2 172,0l-7 -156c-15,1 -30,1 -45,1l0 51 -33 0 -2 0 -33 0 0 -51c-15,0 -30,-1 -45,-1z"/><path rel="14220" class="bloqueado bloco" id="bloco_107" d="M1319 146l7 156c56,-2 107,-5 154,-10l-21 -156c-46,5 -93,8 -140,10z"/><path rel="14225" class="bloqueado bloco" id="bloco_vip105" d="M1000 293l-12 91c13,2 25,3 38,4 13,1 27,3 40,4 12,1 25,2 37,3 12,1 23,1 35,2 0,0 0,0 0,0l11 0c0,0 0,0 0,0l4 -95c-53,-1 -104,-4 -154,-10z"/><path rel="14226" class="bloqueado bloco" id="bloco_vip106" d="M1153 304l-4 93c6,0 13,1 19,1 23,1 47,1 70,1 1,0 1,0 2,0 24,0 47,0 70,-1 6,0 13,0 19,-1l-4 -93c-59,3 -117,3 -173,0z"/><path rel="14227" class="bloqueado bloco" id="bloco_vip107" d="M1326 303l4 95c28,-1 56,-3 83,-5 13,-1 27,-2 40,-4 13,-1 25,-3 38,-4l-12 -92 0 -1c-52,6 -103,9 -154,10z"/><path rel="14228" class="bloqueado bloco" id="bloco_vip201" d="M1149 398c-4,0 -7,0 -11,-1 4,0 7,0 11,1zm-83 -5l-8 84c35,3 71,5 107,7l3 -84c-6,0 -13,0 -19,-1 -4,0 -7,0 -11,-1 -24,-1 -48,-3 -72,-5z"/><path rel="14229" class="bloqueado bloco" id="bloco_vip202" d="M1240 484l0 0zm1 0l0 0zm0 -84c-1,0 -1,0 -2,0 1,0 1,0 2,0zm-72 -1l-3 84c25,1 49,1 74,1l0 0 0 0c25,0 49,0 73,-1l-3 -84c-23,1 -47,1 -70,1l-2 0c-24,0 -47,0 -70,-1z"/><path rel="14230" class="bloqueado bloco" id="bloco_vip203" d="M1311 398l3 84c36,-1 72,-3 107,-7l-8 -84c-28,2 -56,4 -83,5 -6,0 -13,1 -19,1z"/><path rel="14231" class="bloqueado camarote" id="camarote_C101" d="M463 258l25 -56c-11,-3 -22,-6 -33,-9l-25 53c5,2 10,4 16,6 6,2 12,4 17,7z"/><path rel="14232" class="bloqueado camarote" id="camarote_C102" d="M508 274l26 -61c-15,-4 -31,-8 -46,-11l-25 56 0 0 19 7c0,0 0,0 0,0 9,3 17,6 26,9z"/><path rel="14233" class="bloqueado camarote" id="camarote_C103" d="M554 289l26 -66c-15,-4 -31,-7 -46,-11l-26 61 11 4c12,4 23,8 35,12z"/><path rel="14234" class="bloqueado camarote" id="camarote_C104" d="M600 304l26 -70c-5,-1 -11,-2 -16,-4 -10,-2 -20,-4 -30,-7l-26 66 2 1c12,4 25,8 37,12l7 2z"/><path rel="14235" class="bloqueado camarote" id="camarote_C105" d="M646 317l25 -74c-15,-3 -31,-6 -46,-10l-26 70c10,3 21,6 31,9l15 4 0 0z"/><path rel="14236" class="bloqueado camarote" id="camarote_C106" d="M693 330l25 -77c-16,-3 -31,-6 -46,-9l-25 74c8,2 15,4 23,6 8,2 16,4 24,6z"/><path rel="14237" class="bloqueado camarote" id="camarote_C107" d="M741 341l24 -80c-16,-3 -31,-6 -46,-8l-25 77 14 4c11,3 22,5 33,8z"/><path rel="14238" class="bloqueado camarote" id="camarote_C108" d="M789 352l23 -83c-4,-1 -8,-1 -12,-2 -12,-2 -23,-4 -35,-6l-24 80 5 1c13,3 26,6 39,9l3 1z"/><path rel="14239" class="bloqueado camarote" id="camarote_C109" d="M837 361l21 -86c-16,-2 -31,-5 -47,-7l-23 83c12,3 24,5 36,7l12 2z"/><path rel="14240" class="bloqueado camarote" id="camarote_C110" d="M886 370l20 -88c-16,-2 -31,-4 -47,-6l-21 86c9,2 18,3 28,5 7,1 14,2 21,4z"/><path rel="14241" class="bloqueado camarote" id="camarote_C111" d="M934 378l19 -90c-16,-2 -32,-4 -47,-6l-20 88 19 3c10,2 20,3 30,4z"/><path rel="14242" class="bloqueado camarote" id="camarote_C112" d="M987 384l12 -92c-16,-1 -31,-3 -47,-5l-19 90 11 2c14,2 28,4 42,5z"/><path rel="14243" class="bloqueado camarote" id="camarote_C113" d="M1480 293l12 92c14,-2 28,-3 42,-5 4,0 7,-1 11,-1l-18 -90c-16,2 -31,3 -47,5z"/><path rel="14244" class="bloqueado camarote" id="camarote_C114" d="M1594 370l-20 -88c-16,2 -32,4 -47,6l18 90c10,-1 20,-3 30,-4l19 -3 0 0c0,0 0,0 0,0z"/><path rel="14245" class="bloqueado camarote" id="camarote_C115" d="M1643 361l-21 -86c-16,2 -31,4 -47,6l20 88c7,-1 14,-2 21,-4 9,-2 18,-3 28,-5z"/><path rel="14246" class="bloqueado camarote" id="camarote_C116" d="M1621 276l21 86c4,-1 8,-2 12,-2 12,-2 24,-5 36,-7l-23 -83c-16,2 -31,5 -47,7z"/><path rel="14247" class="bloqueado camarote" id="camarote_C117" d="M1715 261c-12,2 -23,4 -35,6 -4,1 -8,1 -12,2l23 83c1,0 2,0 3,-1 13,-3 26,-6 39,-9 2,0 4,-1 5,-1l-24 -80z"/><path rel="14248" class="bloqueado camarote" id="camarote_C118" d="M1715 261l24 80c11,-3 22,-5 33,-8 5,-1 9,-2 14,-4l-25 -77c-15,3 -31,6 -47,8z"/><path rel="14249" class="bloqueado camarote" id="camarote_C119" d="M1761 253l25 77c8,-2 16,-4 24,-6 8,-2 15,-4 23,-6l-25 -73c-15,3 -31,6 -46,9z"/><path rel="14250" class="bloqueado camarote" id="camarote_C120" d="M1854 234c-15,3 -31,7 -46,10l25 73c5,-1 10,-3 15,-4 10,-3 21,-6 31,-9l-26 -70z"/><path rel="14251" class="bloqueado camarote" id="camarote_C121" d="M1854 234l26 70c2,-1 4,-1 7,-2 13,-4 25,-8 37,-12 1,0 1,0 2,-1l-26 -66c-10,2 -20,5 -30,7 -5,1 -11,2 -16,4z"/><path rel="14252" class="bloqueado camarote" id="camarote_C122" d="M1971 274l-26 -61c-15,4 -30,7 -46,11l26 66c12,-4 23,-8 35,-12 4,-1 7,-2 11,-4z"/><path rel="14253" class="bloqueado camarote" id="camarote_C123" d="M2017 257l-25 -56c-15,4 -30,8 -46,11l26 61c9,-3 17,-6 26,-9 6,-2 13,-5 19,-7z"/><path rel="14254" class="bloqueado camarote" id="camarote_C124" d="M2024 193c-11,3 -22,6 -33,9l25 56c6,-2 12,-4 17,-7 5,-2 10,-4 15,-6l-25 -52z"/><path rel="14255" class="bloqueado camarote" id="camarote_C201" d="M384 290l26 -53c-20,-8 -40,-16 -60,-25l-24 47c19,11 39,21 58,31z"/><path rel="14256" class="bloqueado camarote" id="camarote_C202" d="M445 251c-5,-2 -10,-4 -15,-6l0 0c-7,-3 -14,-5 -20,-8l-26 53c12,6 24,12 36,18l26 -57c0,0 0,0 0,0z"/><path rel="14257" class="bloqueado camarote" id="camarote_C203" d="M446 251l-26 57c12,6 24,11 37,17l26 -60c-6,-2 -13,-5 -19,-7l0 0 0 0 -17 -7 0 0c0,0 0,0 0,0z"/><path rel="14258" class="bloqueado camarote" id="camarote_C204" d="M482 264l-26 60c12,5 25,11 37,16l25 -63c-4,-1 -7,-2 -11,-4 -9,-3 -17,-6 -26,-9z"/><path rel="14259" class="bloqueado camarote" id="camarote_C205" d="M519 277l-25 63c12,5 25,10 38,15l25 -66c-1,0 -1,0 -2,-1 -12,-4 -23,-8 -35,-12z"/><path rel="14260" class="bloqueado camarote" id="camarote_C206" d="M556 290l-25 66c13,5 25,10 38,14l24 -68c-13,-4 -25,-8 -37,-12z"/><path rel="14261" class="bloqueado camarote" id="camarote_C207" d="M593 302l-24 68c13,5 26,9 39,14l23 -70c-10,-3 -21,-6 -31,-9 -2,-1 -4,-1 -7,-2z"/><path rel="14262" class="bloqueado camarote" id="camarote_C208" d="M631 313l-23 70c13,4 26,9 39,13l22 -73c-8,-2 -15,-4 -23,-6 -5,-1 -10,-3 -15,-4z"/><path rel="14263" class="bloqueado camarote" id="camarote_C209" d="M669 323l-22 73c13,4 26,8 40,12l21 -75c-5,-1 -9,-2 -14,-4 -8,-2 -16,-4 -24,-6z"/><path rel="14264" class="bloqueado camarote" id="camarote_C210" d="M707 333l-21 75c13,4 27,7 40,11l19 -76c-2,0 -4,-1 -5,-1 -11,-3 -22,-5 -33,-8z"/><path rel="14265" class="bloqueado camarote" id="camarote_C211" d="M746 342l-19 76c13,4 27,7 41,10l18 -78c-13,-3 -26,-6 -39,-9z"/><path rel="14266" class="bloqueado camarote" id="camarote_C212" d="M785 351l-18 78c14,3 27,6 41,9l16 -79c-12,-2 -24,-5 -36,-7 -1,0 -2,0 -3,-1z"/><path rel="14267" class="bloqueado camarote" id="camarote_C213" d="M825 359l-16 79c14,3 28,6 41,9l15 -80c-9,-2 -18,-3 -28,-5 -4,-1 -8,-2 -12,-2z"/><path rel="14268" class="bloqueado camarote" id="camarote_C214" d="M864 366l-15 80c14,3 28,5 42,8l13 -81c-6,-1 -13,-2 -19,-3 -7,-1 -14,-2 -21,-4z"/><path rel="14269" class="bloqueado camarote" id="camarote_C215" d="M905 373l-13 81c14,2 28,5 42,7l11 -82c-4,0 -7,-1 -11,-2 -10,-1 -20,-3 -30,-4z"/><path rel="14270" class="bloqueado camarote" id="camarote_C216" d="M976 467l11 -83c-14,-2 -28,-3 -42,-5l-11 82c14,2 28,4 42,6z"/><path rel="14271" class="bloqueado camarote" id="camarote_C217" d="M1015 472l10 -83c-13,-1 -25,-3 -38,-4l-11 83c13,2 26,3 39,5z"/><path rel="14272" class="bloqueado camarote" id="camarote_C218" d="M1058 476l8 -84c-13,-1 -27,-2 -40,-4l-10 83c14,2 28,3 43,4z"/><path rel="14273" class="bloqueado camarote" id="camarote_C219" d="M1414 392l8 84c14,-1 29,-3 43,-4l-10 -83c-13,1 -27,3 -40,4z"/><path rel="14274" class="bloqueado camarote" id="camarote_C220" d="M1454 389l10 83c13,-1 26,-3 39,-5l-11 -83c-13,2 -25,3 -38,4z"/><path rel="14275" class="bloqueado camarote" id="camarote_C221" d="M1492 384l11 83c14,-2 28,-4 42,-6l-11 -82c-14,2 -28,4 -42,5z"/><path rel="14276" class="bloqueado camarote" id="camarote_C222" d="M1535 379l11 82c14,-2 28,-4 42,-7l-13 -81c-10,2 -20,3 -30,4 -4,1 -7,1 -11,1z"/><path rel="14277" class="bloqueado camarote" id="camarote_C223" d="M1575 373l13 81c14,-2 28,-5 42,-8l-15 -80c-7,1 -14,2 -21,4 -6,1 -13,2 -19,3z"/><path rel="14278" class="bloqueado camarote" id="camarote_C224" d="M1615 366l15 80c14,-3 28,-6 41,-8l-16 -79c-4,1 -8,2 -12,2 -9,2 -18,3 -28,5z"/><path rel="14279" class="bloqueado camarote" id="camarote_C225" d="M1655 359l16 79c14,-3 27,-6 41,-9l-18 -78c-1,0 -2,0 -3,1 -12,3 -24,5 -36,7z"/><path rel="14280" class="bloqueado camarote" id="camarote_C226" d="M1694 351l18 78c14,-3 27,-7 41,-10l-19 -76c-13,3 -26,6 -39,9z"/><path rel="14281" class="bloqueado camarote" id="camarote_C227" d="M1733 342l19 76c13,-4 27,-7 40,-11l-21 -75c-11,3 -22,5 -33,8 -2,0 -4,1 -5,1z"/><path rel="14282" class="bloqueado camarote" id="camarote_C228" d="M1772 333l21 75c13,-4 27,-8 40,-12l-22 -73c-8,2 -16,4 -24,6 -5,1 -9,2 -14,4z"/><path rel="14283" class="bloqueado camarote" id="camarote_C229" d="M1810 323l22 73c13,-4 26,-8 39,-13l-23 -71c-5,1 -10,3 -15,4 0,0 0,0 0,0 -8,2 -15,4 -23,6z"/><path rel="14284" class="bloqueado camarote" id="camarote_C230" d="M1849 313l23 71c13,-4 26,-9 39,-14l-24 -68c-2,1 -4,1 -7,2 -10,3 -21,6 -31,9z"/><path rel="14285" class="bloqueado camarote" id="camarote_C231" d="M1886 302l24 68c13,-5 26,-9 38,-14l-25 -66c-12,4 -25,8 -37,12z"/><path rel="14286" class="bloqueado camarote" id="camarote_C232" d="M1924 290l25 66c13,-5 25,-10 38,-15l-25 -63c-12,4 -23,8 -35,12 -1,0 -1,0 -2,1z"/><path rel="14287" class="bloqueado camarote" id="camarote_C233" d="M1961 277l25 63c12,-5 25,-10 37,-16l-26 -60c-9,3 -17,6 -26,9 -4,1 -7,2 -11,4z"/><path rel="14288" class="bloqueado camarote" id="camarote_C234" d="M1998 264l26 60c12,-5 24,-11 37,-17l-26 -57c0,0 0,0 0,0 -6,2 -11,4 -17,7 -6,2 -13,5 -19,7z"/><path rel="14289" class="bloqueado camarote" id="camarote_C235" d="M2034 251l26 57c12,-6 24,-12 36,-18l-26 -53c-7,3 -14,5 -21,8 -5,2 -10,4 -15,6l0 0z"/><path rel="14290" class="bloqueado camarote" id="camarote_C236" d="M2070 237l26 53c20,-10 39,-20 58,-31l-25 -48c-20,9 -40,17 -60,25z"/>                <path class="fil4 str1 arquibancada" d="M89 339l110 -158c270,187 636,303 1040,303l0 0 0 0 0 0 0 0c404,0 770,-116 1040,-303l109 157c-801,473 -1566,453 -2301,2z"/>';
            foreach($setores as $a){
                $modelProduto=new ApiProduto;
                if(!isset($a->anexos)) continue;
                $setor=$a->tags['tipo'].'_'.($a->tags['tipo']=='bloco' ?$a->tags['bloco'] :$a->tags['camarote']);
                $anexo=$modelProduto->findAnexo($a->anexos,'svgmapa');
                if($this->context->setor!=null && $this->context->setor==$setor)
                    $anexo=str_replace('class="', 'class="selecionado ', $anexo);
                if($a->ativo==0 && !$modelProduto->checkReserva($a))
                    $anexo=str_replace('class="', 'class="bloqueado ', $anexo);
                $anexo=str_replace('<path ', '<path rel="'.$a->codigo.'" ', $anexo);
                $mapa.=$anexo;
                $totais=$a->estoque;
                if(isset($a->tags['bloco'])){
                    $modelConsulta=new ApiProduto;
                    $consultaTotais=$modelConsulta->consultarTotaisEstoqueProduto([
                        'tags'=>[
                            [
                                "nome"=>"matriz",
                                "valor"=>$a->tags['matriz']
                            ],
                        
                            [
                                "nome"=>"tipo",
                                "valor"=>"assento"
                            ],
                        
                            [
                                "nome"=>"bloco",
                                "valor"=>$a->tags['bloco']
                            ]
                    
                        ]
                    ]);
                    if($consultaTotais->successo==1)
                        $totais=$consultaTotais->objeto;
                }
                echo Html::input('hidden','oferecido',number_format(floatval($totais->oferecido), 0),['id'=>'oferecido_'.$setor]);
                echo Html::input('hidden','disponivel',number_format(floatval($totais->disponivel), 0),['id'=>'disponivel_'.$setor]);
            }
            ?>
            <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="100%" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                 viewBox="0 0 2480 707"
                 xmlns:xlink="http://www.w3.org/1999/xlink" id="lugares" class="oeste">  
                <path class="fil0 str0" d="M1240 490l-1 0 -1 0 0 0c-345,0 -663,-85 -916,-226l27 -52 -34 -60 169 -151c213,90 458,143 721,147l0 51 33 0 2 0 33 0 0 -51c263,-4 508,-57 721,-147l169 151 -34 60 27 52c-253,142 -571,226 -916,226l0 0z"/>
                <?php echo $mapa ?>
                <path class="fil4 str1 arquibancada" d="M89 339l110 -158c270,187 636,303 1040,303l0 0 0 0 0 0 0 0c404,0 770,-116 1040,-303l109 157c-801,473 -1566,453 -2301,2z"/>
                <path class="fil3 str1 arquibancada" d="M1240 484l0 201"/>
                <path class="fil3 str1 arquibancada" d="M266 439l93 -162"/>
                <path class="fil3 str1 arquibancada" d="M449 525l76 -172"/>
                <path class="fil3 str1 arquibancada" d="M638 594l59 -183"/>
                <path class="fil3 str1 arquibancada" d="M833 645l41 -193"/>
                <path class="fil3 str1 arquibancada" d="M1032 675l22 -200"/>
                <path class="fil3 str1 arquibancada" d="M2028 519l-74 -167"/>
                <path class="fil3 str1 arquibancada" d="M1840 588l-57 -177"/>
                <path class="fil3 str1 arquibancada" d="M1646 639l-40 -188"/>
                <path class="fil3 str1 arquibancada" d="M1448 672l-21 -197"/>
                <path class="fil3 str1 arquibancada" d="M2212 435l-90 -158"/>
            </svg>
        </div>
        
        <div class="mapa-assentos-oeste text-center" style="position:relative;">			
			<div class="hidden" id="textInstru2">Mapa apenas para visualização dos assentos.</div>
            <?= Html::img('@web/images/bloco108.png', ['class' => 'hidden imgMapa', 'id' => 'assentos-108']); ?>
            <?= Html::img('@web/images/bloco109.png', ['class' => 'hidden imgMapa', 'id' => 'assentos-109']); ?>
            <?= Html::img('@web/images/bloco110.png', ['class' => 'hidden imgMapa', 'id' => 'assentos-110']); ?>
            <?= Html::img('@web/images/bloco111.png', ['class' => 'hidden imgMapa', 'id' => 'assentos-111']); ?>
            <a href="javascript:;" id="closeImg" class="hidden btn btn-warning btn-sm" style="position:absolute;right:0px;top:-20px;">Fechar mapa</a>
			
			<ul class="legMap hidden" id="legMap">
				<li>Obeso</li>
				<li>Cadeirante</li>
				<li>Acompanhante</li>
				<li>Mobilidade Reduzida</li>
			</ul>
			
			<ul class="hidden" id="textInstru">
				<li>1 - Clique no quadro a direita, em "SELECIONE UM PRODUTO" para escolher seu assento.</li>
				<li>2 - Logo em seguida clique no carrinho para adicionar o produto.</li>
				<li>3 - Selecione até 6 produtos por CPF por evento.</li>
			</ul>			
        </div>
        
        <div class="titArea oesteInfo">
            <div><?= Html::img('@web/images/mapa_peq.png', ["class" => "campoP2"]); ?></div>

            <div>
                
                <p>Está com dúvida de como fazer sua compra? <a href="<?= Url::to(['site/faq']); ?>">Clique Aqui.</a></p>

                <p>Mineirão Tribuna: muito mais que futebol. Área exclusiva com serviço diferenciado e entretenimento. O Mineirão Tribuna é para quem valoriza o aquecimento, pra quem acha que futebol e música tem tudo a ver, pra quem acha que não precisa esperar a final do campeonato para festejar no estádio. Garanta logo sua cadeira em um dos melhores setores do estádio.</p>
            </div>
            <div>
                Vista lado<br>Bl 108 e 109<br>
                <i class="fa fa-camera tam30 flipHor" aria-hidden="true" onclick="vistaOesteEsquerda()"></i>
            </div>
            <div>
                Vista lado<br>Bl 110 e 111<br>
                <i class="fa fa-camera tam30 flipHor" aria-hidden="true" onclick="vistaOesteDireita()"></i>
            </div>
        </div>
    </div>
    <br>
</div>

<div class="col-md-4 col-sm-12 col-xs-12 about-right <?= (!isset($this->context->setor)) ? 'wow fadeInRight animated' : ''; ?> textosAprest3" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
    <div class="tabProd2 hidden" id="div-item">
        <form id="form-carrinho" method="post">
            <input type="hidden" name="id_evento" id="id_evento" value="<?= $evento->codigo; ?>">
            <input type="hidden" id="carrinho-codigo" name="carrinho-codigo">
            <input type="hidden" id="carrinho-area" name="carrinho-area">
            <input type="hidden" id="carrinho-bloco" name="carrinho-bloco">
            <input type="hidden" id="carrinho-desconto" name="carrinho-desconto">
            <table class="table table-condensed" id="table-item">
                <thead>
                    <tr>
                        <th align="center" width="50%">Assento</th>
                        <th align="center" width="35%">Preço unitário</th>
                        <th align="center" width="10%">Quantidade</th>
                        <th align="center" width="5%"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="tbl-assento" width="50%"></td>
                        <td width="35%"><input type="text" id="carrinho-preco-unitario" name="carrinho-preco-unitario" readonly></td>
                        <td width="10%"><input type="text" id="carrinho-quantidade" name="carrinho-quantidade" min="1" maxlength="3"></td>
                        <td width="5%" align="center" valign="middle"><i class="fa fa-cart-plus fa-2x plus-white" title="Adicionar ao seu carrinho"></i></td>
                    </tr>
                </tbody>
            </table>
            <span class="msg-carrinho">* Selecione o assento, a quantidade e clique em <i class="fa fa-cart-plus fa-2x"></i></span>
        </form>
    </div>
    <form method="post" id="form-bloco">
        <input type="hidden" name="id_evento" id="id_evento" value="<?= $evento->codigo; ?>">
        <div class="tabProd">
            <div><i class="fa fa-shopping-cart fa-2x"></i> Meu Carrinho:</div>
            <table class="table table-condensed table-stripped">
                <thead id="tbl-header" class="<?= empty($carrinho->objeto->produtos) ? 'hidden' : ''; ?>">
                    <tr>
                        <!-- <th width="20%">Área</th> -->
                        <th width="50%">Setor / Bloco</th>
                        <th width="10%">Itens</th>
                        <th width="15%">Subtotal</th>
                        <th width="5%"></th>
                    </tr>
                </thead>
                <tbody id="linhasLugares">
                <?php 
                if (!empty($carrinho->objeto->produtos)) {  
                    foreach ($carrinho->objeto->produtos as $index => $produto) {
                        $bloco = '';
                        $area = '';
                        if(isset($produto->tags)){
                            foreach ($produto->tags as $index_tag => $tag) {
                                if ($tag->nome == 'bloco') {
                                    $bloco = $tag->valor;
                                }
                                if ($tag->nome == 'area') {
                                    $area = $tag->valor;
                                }
                            }
                        }
                    ?>
                    <tr id="tr-<?= $produto->codigo; ?>" class="linha-carrinho">
                        <!-- <td width="15%"><?= $area; ?></td> -->
                        <td width="40%"><?= $produto->nome; ?></td>
                        <td width="10%" align="right"><?= number_format($produto->unidades, 0); ?></td>
                        <td width="30%">
                            <?php if (floatval($produto->desconto->fixo) > 0) { ?>
                            R$ <?= number_format(($produto->valor - $produto->desconto->fixo) * $produto->unidades, 2, ',', ''); ?><br>
                            <span class="strikeDesconto">R$ <?= number_format($produto->valor * $produto->unidades, 2, ',', ''); ?></span>
                            <?php } else { ?>
                            R$ <?= number_format($produto->valor * $produto->unidades, 2, ',', ''); ?>
                            <?php } ?>
                        </td>
                        <td width="5%" align="center"><i class="fa fa-trash trash-item plus-red mouseMao" id="trash-<?= $produto->codigo; ?>" data-trash="<?= $produto->codigo; ?>" title="Remover este item do seu carrinho"></i></td>
                        <input type="hidden" name="unidades[]" value="<?= number_format($produto->unidades, 0); ?>" min="0" />
                        <input type="hidden" name="id[]" value="<?= $produto->codigo; ?>">
                        <input type="hidden" name="area[]" value="<?= $area; ?>">
                        <input type="hidden" name="setor[]" value="<?= $bloco; ?>">
                        <input type="hidden" name="preco_unitario[]" value="<?= $produto->valor; ?>">
                        <input type="hidden" name="desconto[]" value="<?= $produto->desconto->fixo; ?>">
                    </tr>
                    <?php 
                    } 
                } ?>
                </tbody>
            </table>
        </div>
    </form>
	<p class="preenche">* Preenchimento obrigatório.</p>
    <div style="margin:0px auto;">
		<br>
        <span class="row contato carrDest" style="width:50%;">
            <input type="submit" value="Finalizar a Compra" id="finaliza-compra" class="icon-next carrinho">
            <a class="carrinho seta" href="<?= Url::to(['site/index']); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Voltar a Home</a>
        </span>
    </div>
</div>

<p id="infoSetor"></p>
<hr style="padding-top:25px;">
    
<?php $this->registerJs('
    $("#mapaRoxo #lugares .bloco, #mapaRoxo #lugares .camarote").mouseover(function(){
        if($(this).hasClass("bloqueado")) return;
        infoConteudo($(this),"Roxo");
    });
    $("#mapaRoxo #lugares").mouseout(function(){
        if($(this).hasClass("bloqueado")) return;
        offInfoConteudo();
    });
    $("#mapaRoxo #lugares path").on("click",function(e){
        if($(this).hasClass("bloqueado")) return;
        $("#mapaRoxo #lugares path").removeClass("selecionado");
		if($(this).hasClass("selecionado")) e.preventDefault()
        $(this).addClass("selecionado");
        if($(this).hasClass("camarote")){
            var id=$(this).attr("rel");
            addProduto(id, $(this).attr("id"));
        }else{
            var id=$(this).attr("id");
            listaLugares(id);
        }
    });
	$("[data-toggle=\"tooltip\"]").tooltip();
', \yii\web\View::POS_END); ?>
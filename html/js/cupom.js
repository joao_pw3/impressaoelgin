var cupom;

$(document).ready(function () {
    
    atualizarDesconto();
    $('input[name="desconto"]').click(function() {
        atualizarDesconto();
    });
    
    $('#cupom-form').on('beforeSubmit', function (e) {
        return validarForm();
    });
    
    $('.del-cupom').click(function () {
        cupom = $(this).attr('data-cupom');
        showConfirm('Confirma a remoção deste cupom?');
    });
    
    $('#btn-cancel').click(function () {
        $('#modal-confirmar').modal('hide');
        return false;
    });
    
    $('#btn-confirmar').click(function () {
        $('#modal-confirmar').modal('hide');
        doAjax ($('#url-del-cupom').val(), 'POST', {'Cupom': {'id': cupom}}, 'JSON', 10000, 'delCupomOk', 'delCupomErr');
    });
    
    $('#btn-estoque').click(function () {
        if ($('#estoque').val() == '') {
            showMsg('Informe o total de unidades para este cupom.');
            return false;
        }
        doAjax ($('#url-movimento').val(), 'POST', {'Cupom': {'id': $('#cupom').val(), 'estoque': $('#estoque').val(), 'tipo': $('input[name="Cupom[tipo]"]:checked').val()}}, 'JSON', 10000, 'estoqueOk', 'estoqueErr');
    });
    
});

function validarForm() {
    if ($('#nome').val() == '') {
        showMsg('Preencha o nome do cupom.');
        return false;
    }
    if ($('#desconto_fixo').val() == '' && $('#desconto_percentual').val() == '') {
        showMsg('Preencha o desconto fixou OU o desconto percentual');
        return false;
    }
    if (($('#desconto_fixo').val() != '' && isNaN($('#desconto_fixo').val())) || ($('#desconto_percentual').val() != '' && isNaN($('#desconto_percentual').val()))) {
        showMsg('O desconto deve ser numérico.');
        return false;
    }
}

function atualizarDesconto () {
    var desconto = $('input[name="desconto"]:checked').val();
    if (desconto == 'desconto_fixo') {
        $('#desconto_percentual').attr('disabled', true);
        $('#cupom-desconto_percentual').attr('disabled', true);
        $('#desconto_fixo').attr('disabled', false);
        $('#cupom-desconto_fixo').attr('disabled', false);
    } else {
        $('#desconto_percentual').attr('disabled', false);
        $('#cupom-desconto_percentual').attr('disabled', false);
        $('#desconto_fixo').attr('disabled', true);
        $('#cupom-desconto_fixo').attr('disabled', true);
    }
}

function delCupomOk(data) {
    showMsg('Cupom removido com sucesso.');
    reloadPage();
}

function delCupomErr(data) {
    showMsg('Falha ao remover cupom: ' + data.erro.mensagem);
}

function estoqueOk(data) {
    showMsg('Estoque do cupom definido com sucesso.');
    reloadPage();
}

function estoqueErr(data) {
    showMsg('Falha ao definir estoque do cupom: ' + data.erro.mensagem);
}
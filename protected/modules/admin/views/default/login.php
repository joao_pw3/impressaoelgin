<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Modal;

$this->title = 'Login Administrativo';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row contato">

    <div class="small-4 columns boxRoxo">
        <span><strong>ADM</strong></span>
        <div class="formMineiraobox">
                <?php $form = ActiveForm::begin([
                    'id' => 'form-login',
                    'class' => 'formMineirao wpcf7-form',
                    
                ]); ?>

                <?= $form->field($model, 'username')->textInput([
                    'class' => 'form-control',
                    'placeholder' => 'Digite aqui seu e-mail',
                    'title' => 'Digite aqui seu e-mail',
                    'id' => 'user_id',
                ]) ?>
                
                <?= $form->field($model, 'password')->passwordInput([
                    'class' => 'form-control',
                    'placeholder' => 'Informe sua senha',
                    'title' => 'Informe sua senha. Esta senha serve para recuperar os seus dados cadastrais e de cartão, mas não autoriza pagamentos.',
                    'id' => 'senha',
                    'maxlength' => 10
                ]); ?>
            <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            <?php ActiveForm::end(); ?>   
            <hr class="linhaLogin">
                <a href="" class="linkcAD" id="esqueci-senha">Esqueci minha Senha</a>
            <br clear="all">
        </div>
    </div>
    
</div>

<!-- Modal - Esqueci minha senha -->
<?php Modal::begin([
    'header' => '<h4 align="center">Esqueci minha senha</h4>',
    'size'   => 'modal-sm',
    'id'     => 'modal-senha',
    'options'=> [
        'class' => 'modal-center'
    ]
]); ?>
<form id="form-esqueci-senha">
    <input type="hidden" id="url-mensagem-esqueci" value="<?= Url::to(['//site/mensagem-esqueci-senha']); ?>">
    <div class="row">
        <div class="col-md-12">
            <label for="user_id-esqueci">E-mail:</label>
            <input type="text" name="user_id" id="user_id-esqueci" class="form-control" readonly>
            <p>Caso este e-mail esteja cadastrado, receberá uma mensagem com instruções para a troca de sua senha.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 contato" align="center">
            <input type="button" value="Enviar" id="btn-esqueci-senha" class="icon-next esqueci-senha">
        </div>
    </div>
</form>
<?php Modal::end();?>

<!-- Modal - Mensagens -->
<?php Modal::begin([
    'header' => '<h4 align="center">Atenção</h4>',
    'size'   => 'modal-sm',
    'id'     => 'modal-mensagem',
    'options'=> [
        'class' => 'modal-center'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ]
]);
Modal::end(); ?>
<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="container">
    <h1>Continuar o acesso</h1>
    <p>Você fez o login como operador do sistema, portanto as funções relacionadas a cliente não pode ser acessada.</p>
    <p>Se você quiser ir para a área administrativa, <a href="<?=Url::to(['admin/default'])?>">clique aqui</a></p>
    <p>Caso prefira fazer logout, <a href="<?=Url::to(['site/logout'])?>">clique aqui</a></p>
</div>
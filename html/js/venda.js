var codigo;
var produto;
var indexOcupante = 1;

$(document).ready(function () {
    
    if ($('.selecionado').length > 0) {
        $('#documento_comprador').removeClass('hidden');
    }
    
    $('.assento').click(function () {
        var id = $(this).attr('id');
        codigo = id;
        produto = $(this).attr('data-produto');
        if ($('#' + id + ' circle').hasClass('fillRed') || ($('#' + id + ' circle').hasClass('fillOrange') && produtosCarrinho != '' && produtosCarrinho.indexOf(id) == -1)) {
            showMsg('Este produto está indisponível. Por favor, escolha outro.');
            return false;
        }
        if ($('.circle-' + id).hasClass('fillPurple') || $('.circle-' + id).hasClass('fillOrange')) {
            doAjax ($('#url-del-produto').val(), 'POST', {'produto_codigo': codigo,'produto_unidades': 0}, 'JSON', 10000, 'delProdutoOK', 'delProdutoErr');
        } else {
            doAjax ($('#url-add-produto').val(), 'POST', {'produto_codigo': codigo, 'assentos': true}, 'JSON', 10000, 'addProdutoOK', 'addProdutoErr');
        }
    });
    
    $(document).on('change', '.cupom-codigo', function() {
        calcularTotal();    
    });
    
    $('#btn-autorizar-pagamento').click(function () {
        var ocupantes = parametrosOcupantesOk();
        if (ocupantes) 
            salvarCarrinho();
    });

    $('#btn-calcular-troco').click(function(){
        var troco=0;
        var recebido=$('#vendabilheteria-troco-disp').val().replace('R$ ','').replace(',','').replace('.','');
        var valor=$('#valorFinal').val().replace(',','').replace('.','');
        console.log([recebido,valor]);
        if(parseInt(recebido)>parseInt(valor))
            troco=(recebido-valor)/100;
        else {
            showMsg('Não há troco para esta compra.');
            return false;
        }

        $('#especie-troco').html(troco.toFixed(2).replace('.',','));
    });    
    calcularTotal();
});

var parametrosOcupantesOk = function(){
    var retorno=true;
    if ($('#documento_comprador').val() == '') {
        showMsg('Por favor, informe o documento do comprador.');
        retorno=false;
    }
    $('.ocupante-nome').each(function () {
        if ($(this).val() == '') {
            $(this).val('Baixe nosso app.');
        }
    });
    $('.ocupante-doc').each(function () {
        if ($(this).val() == '') {
            $(this).val($('#documento_comprador').val());
        }
    });    
    return retorno;
};

var prosseguirVendaOk=function(){
    // console.log('prosseguirVendaOk');
    var forma=parseInt($("#formaPagamento").val());
    var valor=$("#valorFinal").val();
    var parcelas=parseInt($("#parcelas").val());
    if(capptaObj instanceof Object == false) {
        showMsg('O pinpad não está conectado ou está com problemas. Conecte-o e recarregue a página');
        return false;
    }
    if(isNaN(forma)) {
        console.log('forma');
        showMsg('Selecione a forma de pagamento');        
        return false;
    }
    if(!isNaN(valor) || valor<0 || !parseFloat(valor)) {
        console.log('valor');
        showMsg('O total da compra é inválido. Reinicie a operação soma.');
        return false;
    }
    if(forma==1 && (isNaN(parcelas) || !parcelas>0 || isNaN(parcelas))) {
        console.log('parcelas');
        showMsg('Selecione a quantidade de parcelas');
        return false;
    }
    return {'ok':true,'forma': forma, 'valor': valor, 'parcelas': parcelas};
};


function delCupomOK (data) {
    showMsg('Tipo de ingresso escolhido com sucesso (inteira).');
    calcularTotal();
}

function delCupomErr (data) {
    showMsg('Erro ao escolher tipo de ingresso (inteira): ' + data.erro.mensagem);
}

function addProdutoOK (data) {
    doAjax ($('#url-select-ingresso').val(), 'POST', {'produto_codigo': codigo, 'id_agenda': $('#id_agenda').val(), 'index': indexOcupante}, 'JSON', 10000, 'ingressoOK', 'ingressoErr');
}

function addProdutoErr (data) {
    showMsg('Falha ao adicionar produto. Talvez você não tenha permissão para essa operação:<span class="mensErro1"> ' + data.erro.mensagem + '</span>');
}

function delProdutoOK (data) {
    $('.circle-' + codigo).removeClass('fillPurple').removeClass('fillOrange').addClass('fillGray');
    $('#div-' + codigo).remove();
    calcularTotal();
}

function delProdutoErr (data) {
    showMsg('Erro ao remover produto do carrinho: ' + data.erro.mensagem);
}

function ingressoOK (data) {
    $('.circle-' + codigo).addClass('fillPurple');
    var divIngresso = '\
    <div id="div-' + codigo + '" class="col-xs-12 selecionado prodBilhet">\n\
        <input type="hidden" name="Ocupante[produto_codigo][' + indexOcupante + ']" value="' + codigo + '">\n\
        <div class="col-xs-2 semPad">\n\
            <div class="div-circle">' + produto + '</div>\n\ ';
    if ($('#' + codigo + ' text').hasClass('font-awesome')) {
        divIngresso += '\
            <div class="div-especial font-awesome" title="Assento especial - ' + $('#' + codigo + ' title').text() + '">' + $('#' + codigo + ' text').text() + '</div>\n\ ';
    }
    divIngresso += '\
        </div>\n\
        <div class="col-xs-10">\n\
            <input type="text" name="Ocupante[nome][' + indexOcupante + ']" class="form-control ocupante-nome ocupBilhet" placeholder="Nome do ocupante">\n\
            <input type="text" name="Ocupante[documento][' + indexOcupante + ']" class="form-control ocupante-doc ocupBilhet" placeholder="Documento do ocupante">\n\
            <input type="text" name="Ocupante[email][' + indexOcupante + ']" class="form-control ocupante-email ocupBilhet" placeholder="Email do ocupante">\n\
            <input type="text" name="Ocupante[telefone][' + indexOcupante + ']" class="form-control ocupante-tel ocupBilhet" placeholder="Telefone do ocupante">\n\
            ' + data.select + '\n\
        </div>\n\
    </div>';
    $('#documento_comprador').removeClass('hidden');
    $('#venda-selecionados').append(divIngresso);
    indexOcupante ++;
    calcularTotal();
}

function ingressoErr (data) {
    return '';
}

function calcularTotal () {
    var total = 0;
    $('.cupom-codigo').each(function () {
        var id = $(this).attr('id');
        total += parseFloat($('#' + id + ' option:selected').attr('data-valor'));
        $('.div-bilheteria').removeClass('hidden');
        $('.btn-auxiliar-1').removeClass('hidden');
    });
    var totalReal=total.toFixed(2).replace('.', ',');
    $('.ingresso-total').text(total.toFixed(2).replace('.', ','));
    $('#vendaTotal').val(totalReal);
    $('#valorFinal').val(totalReal);
    $('.field-parcelas').hide();
    $('#confirmaVenda #formaPagamento option').first().prop('selected',true);
    if (total == 0) {
        $('.div-bilheteria').addClass('hidden');
        $('.btn-auxiliar-1').addClass('hidden');
    }
    if ($('.prodBilhet').length==0) {
        $('#documento_comprador').val('').addClass('hidden');
    }
}

function salvarCarrinho() {
    doAjax ($('#url-carrinho').val(), 'POST', $('#form-bilheteria').serialize(), 'JSON', 10000, 'carrinhoOk', 'carrinhoErr');
}

function carrinhoOk(data) {
    var prosseguir = prosseguirVendaOk();
    if (prosseguir.ok) {
        pagamento(prosseguir);
    }
}

function carrinhoWrong(data) {
    console.log(data);
}

function cobrancaOK (data) {
    $('.bordArredondBilhet').addClass('esconde-mapa');
    showMsg('Compra efetuada com sucesso.');
}

function cobrancaErr (data) {
    showMsg('Erro ao efetuar compra: ' + data.erro.mensagem);
}
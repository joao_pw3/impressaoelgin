<?php
use app\models\Tags;
$tagsTmp = new Tags();
foreach ($listaProdutos as $index => $_produto) {
    $produto=$_produto['objeto'];
    $indexForm=intval($_produto['indexForm']);
    $ingresso = isset($produto->cupom) ? $produto->cupom->nome : 'Inteira';
    $cupom = isset($produto->cupom) ? $produto->cupom->id : 'inteira';
    $tags=null;
    if(isset($produto->tags))
        $tags = $tagsTmp->objetoTags($produto->tags);
    ?>
    
    <div class="row telaPagamento" id="produto-<?= $produto->codigo.'-'.$indexForm; ?>">
        <div class="col-xs-2">
            <p><?= $produto->agenda->nome ?><br /><?= $produto->agenda->horario ?></p>
        </div>
        <div class="col-xs-2">
            <p><?= $_produto['nome']; ?></p>
        </div>
        <div class="col-xs-2">
            <p><?= $_produto['documento']; ?></p>
        </div>
        <div class="col-xs-2">
            <p><?= $ingresso; ?></p>
        </div>
        <div class="col-xs-2" align="right">
            <p>R$ <?= number_format($produto->valor_liquido,2,',','.'); ?></p>
        </div>
        <div class="col-xs-2">
            <?php if(isset($tags)) {?>
                <div class="div-circle div-ingresso">
                    <span title="Fileira <?= $tags->fileira; ?> - Assento <?= $tags->assento; ?>"><?= $tags->fileira . '-' . $tags->assento; ?></span>
                </div>
            <?php }?>
            <?php if (isset($tags, $tags->especial)) { ?>
            <div class="div-especial">
                <span class="font-awesome" title="<?= $tags->especial == 'cadeirante' ? 'Cadeirante' : ($tags->especial == 'mobilidade' ? 'Mobilidade reduzida' : ($tags->especial == 'obeso' ? 'Obeso' : 'Acompanhante')); ?>">
                    <?= $tags->especial == 'cadeirante' ? '&#xf193;' : ($tags->especial == 'mobilidade' ? '&#xf29d' : ($tags->especial == 'obeso' ? '&#xf234;' : '&#xf067')); ?>
                </span>
            </div>
            <?php } ?>
        </div>
    </div>
   
<?php } ?>
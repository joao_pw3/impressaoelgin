<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\WsFinanceiro;

class Financeiro extends Model
{
    public $transacao;
    public $adquirente;
    public $vendedor;
    public $comprador;
    public $antifraude;
    public $erp;
    public $usuario;

    public $unidade;
    public $lista;

    public function tabelaFinanceira()
    {
    	$ws = new WsFinanceiro($this->unidade->user_token);
    	$tabela = $ws->tabela(['rede'=>'0']);
    	if (!isset($tabela->successo)){
            $this->addError('lista','Não foi possível realizar a consulta');
            return;
    	}
    	if (!$tabela->successo) {
            $this->addError('lista',$tabela->erro->mensagem);
            return;
    	}
    	if ($tabela->successo) {
            $this->lista = [];
            foreach ($tabela->objeto as $objeto) {
                $model = new Financeiro;
                $model->loadFromApi($objeto);
                $this->lista[] = $model;
            }
    	}
    }
    
    public function consulta($post)
    {
        $session = Yii::$app->session;
        $session->open();
        $api = new WsFinanceiro($session->get('user_token'));
        if (!empty($post)) {
            $object = [];
            if (isset($post['meioPagto'])) {
                $object['meio_pagamento'] = $post['meioPagto'];
            }
            if (isset($post['dataInicio'])) {
                $object['data_ini'] = $post['dataInicio'];
            }
            if (isset($post['dataFim'])) {
                $object['data_fim'] = $post['dataFim'];
            }
            if (isset($post['rede'])) {
                $object['rede'] = $post['rede'];
            }
            return $api->consulta($object);
        }
        return $api->getApiError('Faltam parâmetros para consultar vendas de operador');
    }

    /**
     * Atribuir valores retornado pela API às propriedades
     */
    public function loadFromApi($obj) {
        if (!isset($obj->transacao))
            return false;

        $this->transacao = $obj->transacao;
        $this->adquirente = $obj->adquirente;
        $this->vendedor = $obj->vendedor;
        $this->comprador = $obj->comprador;
        $this->antifraude = $obj->antifraude;
        $this->erp = $obj->erp;
        $this->usuario = $obj->usuario;
    }

    /**
     * Mostrar os valores de cada objeto
     */
    public function mostrarValores($obj)
    {
    	$a = (array)$obj;
    	foreach ($a as $key => $value) {
            if (is_object($value))
                $this->mostrarValores($value);
            else
                echo '<p><strong>'.$key.'</strong>: '.$value.'</p>';
    	}
    }
}
<?php
namespace app\models;
use yii\base\Model;

/**
 * CartaoModel é o modelo para cartão de crédito do cliente
 *
 */
class CartaoModel extends Model
{
    public $numero_cartao;
    public $cvv;
    public $nome_cartao;
    public $mes_venc;
    public $ano_venc;
    public $senha;
    public $ultimos_digitos;
    public $codigo_cartao;
    public $bandeira;
    public $forma;
    public $parcela;
    public $bin;
    public $valorReferencia = [
        1 => 150,
        2 => 159.1,
        3 => 162.3, 
        4 => 165.56, 
        5 => 168.95,
        6 => 172.38,
        7 => 175.81,
        8 => 179.24,
        9 => 182.67,
        10 => 186.10,
        11 => 189.53,
        12 => 192.96
    ];
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['nome_cartao', 'numero_cartao', 'cvv', 'mes_venc', 'ano_venc', 'senha'], 'required'],
            [['nome_cartao'], 'string', 'max' => 64],
            [['cvv', 'ano_venc', 'senha'], 'integer'],
            [['mes_venc'], 'integer', 'min' => 1, 'max' => 12],
            [['numero_cartao'], 'string', 'min' => 13, 'max' => 20],
            [['cvv'], 'string', 'min' => 3, 'max' => 4],
            [['mes_venc'], 'string', 'min' => 2, 'max' => 2],
            [['ano_venc'], 'string', 'min' => 4, 'max' => 4],
            [['senha'], 'string', 'min' => 3, 'max' => 4],
            [['ultimos_digitos', 'codigo_cartao', 'bandeira', 'forma', 'parcela'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'numero_cartao' => 'Número do cartão',
            'cvv' => 'CVV (3 ou 4 dígitos)',
            'nome_cartao' => 'Nome conforme cartão',
            'mes_venc' => 'Mês de validade (MM)',
            'ano_venc' => 'Ano de validade (AAAA)',
            'senha' => 'CVV (3 ou 4 dígitos)',
        ];
    }
    
}
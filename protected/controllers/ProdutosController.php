<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Produto;
use app\models\Cliente;

class ProdutosController extends Controller
{


    /**
     * Buscar um produto a partir do ID da data. Somente AJAX, somente POST
     */
    public function actionBuscarJs()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()){
            $post=Yii::$app->request->post();
            $session = Yii::$app->session;
            $session->open();
            $objApiCliente = new Cliente;
            $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
            $model=new Produto($objApiCliente);           
            $model->selecionarProdutos($post);
            if($model->hasErrors('lista'))
                echo json_encode(['successo'=>'0','erro'=>['mensagem'=>$model->getErrors('lista')]]);
            echo json_encode(['successo'=>'1','produtos'=>$model->lista]);
        }
        exit(); 
    }

}

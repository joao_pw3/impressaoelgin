<?php
namespace app\models;
use app\models\Ws;

/**
 * Classe para Cliente - Easy for Pay
 * obs.: parâmetros entre colchetes são opcionais
 */
class WsConta extends Ws
{
    public function _getConta($token)
    {   
        return json_decode($this->call('conta/'.$token, 'GET', null, false, 'A')); 
    }

    public function _post($post)
    {	
        return json_decode($this->call('conta/', 'POST', $post, true, 'A')); 
    }

    public function _put($token, $post)
    {	
        $this->setToken($token, 'U');
        return json_decode($this->call('cadastro/' . $token, 'PUT', $post, true, 'U')); 
    }    

    public function _get($token)
    {
        return json_decode($this->call('cadastro/' . $token, 'GET', NULL, false, 'A'));
    }    

    public function _novaSenha($hash, $message)
    {
        return json_decode($this->call('novasenha/' . $hash, 'PUT', $message, true, 'A'));        
    }
}
<?php 
$this->title = 'Caixa - Relatório';

if (!empty($caixa->objeto)) { ?>
<div class="row">
    <h1><span style="font-weight:200;">Caixa</span> - Relatório</h1>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th scope="col">Data</th>
                <th scope="col">Tipo</th>
                <th scope="col">Valor R$</th>
            </tr>
        </thead>
        <tbody>
    <?php 
    foreach ($caixa->objeto as $index => $operacao) { 
        $objeto = json_decode(base64_decode($operacao->objeto)); 
        $objeto->valor = empty($objeto->valor) ? 0 : $objeto->valor;
        if ($operacao->saida == 1 && $operacao->ajuste == 0) {
    ?>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
    <?php        
        }
    ?>
            <tr>
                <td><?= $operacao->data; ?></td>
                <td<?= $operacao->saida == 1 && $operacao->ajuste == 0 ? " align='right'" : '' ;?>><?= $operacao->ajuste == 1 ? ($operacao->operacao == 'E' ? 'Reposição' : 'Sangria') : ($operacao->operacao == 'E' ? 'Abertura' : 'Fechamento'); ?></td>
                <td align="right"><?= number_format($objeto->valor, 2, ',', '.'); ?></td>
            </tr>
    <?php } ?>
        </tbody>
    </table>
</div>
<?php } else { ?>
<div class="row">
    <h1><span style="font-weight:200;">Caixa</span> - Relatório</h1>
    <p>Não há movimentação de caixa para este operador.</p>
</div>
<?php } ?>
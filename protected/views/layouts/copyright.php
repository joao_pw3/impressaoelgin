<?php 
use yii\helpers\Html;
?>
<div style="width:100px; text-align:left;">
</div>
<div class="columns text-center">
    <h6>Copyright Minas Arena <span id="getYear"></span> | Todos os direitos reservados</h6>
</div>
<div class="shrink columns text-right">
    <a href="http://www.easyforpay.com.br/" target="_blank">
        <?= Html::img('@web/images/logo-easypay3.png', ['alt' => 'Easyforpay', 'title' => 'Easyforpay', 'border' => '0', 'width' => '100']); ?>
    </a>
</div>

<script>
    var d = new Date();
    var n = d.getFullYear();
    document.getElementById("getYear").innerHTML = n;				
</script>
<?php
use app\modules\admin\assets\VendasAsset;
use yii\bootstrap\Modal;

VendasAsset::register($this);
?>
<div class="col-md-12 col-sm-12 col-xs-12 nopadding">
    <div id="mapaVermelho">
        <div class="titArea row">
            <div class="col-md-8 zeraesp">
                <h4>Blocos Disponíveis - Setor <?= $area == '' ? 'Indefinido' : ($area == 'Leste' ? 'Vermelho' : 'Roxo'); ?></h4>
            </div>
            <div class="col-md-4 zeraesp">
				<div class="legendaBl2">
					<div><span></span></div>
					<div>Bloqueado (fora do site)</div>
					<div><span></span></div>
					<div>Disponível</div>
				</div>
            </div>
        </div>
        <div class="row">
            <?php
            $historicoBloqueio = '';
            foreach($blocos->objeto as $index => $bloco) { 
            ?>
            <div class="col-md-2 <?= $bloco->ativo == '0' ? 'back-preto' : 'back-green'; ?> reserva-bloco-box" align="center">
                <a href="javascript:;" id="<?= $bloco->codigo; ?>" class="reserva-bloco" data-ativo="<?= $bloco->ativo; ?>" data-produto="<?= $bloco->nome; ?>"><?= $bloco->nome; ?></a>
            </div>
            
            <?php
                $tagBloq = '';
                foreach($bloco->tags as $key => $value) {
                    if (stristr($key, 'bloqueio')) {
                        $arrTmp = explode('_', $key);
                        $tagBloq .= substr($arrTmp[1], 6, 2) . '/' . substr($arrTmp[1], 4, 2) . '/' . substr($arrTmp[1], 0, 4) . ' ' . 
                                    substr($arrTmp[1], 8, 2) . ':' . substr($arrTmp[1], 10, 2) . ':' . substr($arrTmp[1], 12, 2) . ' - ' . 
                                    $arrTmp[0] . ':<br>' . $value . '<hr class="ricoBloqueio">';
                    }
                }
                $historicoBloqueio .= (!empty($tagBloq)) ? '<div class="hidden select-lugares historico-bloco" id="bloc-bloq-' . $bloco->codigo . '"><hr class="ricoBloqueio"><label>Histórico de Bloqueio:</label><hr class="ricoBloqueio">' . $tagBloq . '</div>' : '';
            } 
            ?>
        </div>
    </div>
</div>

<!-- Modal - Blocos -->
<?php Modal::begin([
    'header' => '<h4 align="center"><span id="span-produto"></span></h4>',
    'size'   => 'modal-md',
    'id'     => 'modal-blocos',
    'options'=> [
        'class' => 'modal-center'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ]
]); ?>
<form id="form-bloco" method="post">
    <input type="hidden" id="bloco-codigo" name="codigo">
    <input type="hidden" id="bloco-status" name="status">
    <div class="content">
        <!-- <label align="center"><span id="span-produto"></span></label>
        <hr>-->
        <div class="row">
            <div class="col-md-12" style="text-align:center;">
                <label>Status: <span id="span-status"></span></label>
            </div>
        </div>
        <div class="boxCz2">
			<div class="row">
				<div class="col-md-12">
					<label>Justificativa</label>
					<textarea name="justificativa" id="bloco-justificativa" rows="3" cols="80" class="boxHistoria"></textarea>
					<button type="button" class="btn btn-primary cli-cartao" id="btn-bloco-status">Bloquear / Desbloquear</button>
				</div>
				<div class="col-md-12 blocos-div" id="historico-bloc-bloqueio"><?= $historicoBloqueio; ?></div>
			</div>
		</div>
    </div>
</form> 
<?php Modal::end(); ?>
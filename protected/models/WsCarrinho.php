<?php
namespace app\models;

use app\models\Ws;

/**
 * Classe para Carrinho de compras - Easy for Pay
 * obs.: parâmetros entre colchetes são opcionais
 */
class WsCarrinho extends Ws
{
    /**
     * Método construtor - obrigado a passar o token do vendedor para separação de unidades
     */
    public function __construct($tokenVendedor=''){
        if (empty($tokenVendedor)) {
            throw new \Exception("Token vendedor não foi declarado", 1);        
        }
        $this->setToken($tokenVendedor, 'V');
    }

    /**
     * Criar carrinho de compras
     * @return object {'successo': '1', 'id': ''}
     */
    public function criarCarrinho()
    {
        return json_decode($this->call('carrinho/', 'GET'));
    }
    
    /**
     * Atualizar carrinho de compras
     * @param array $post Dados do carrinho: 
     *          '[produto]': {'codigo': '', 'unidades': ''}, 
     *          '[cupom]': {'id': '', 'unidades': ''}, 
     *          '[tag]': {'nome': '', 'valor': ''}
     * @param string $idCarrinho id do carrinho
     * @return object {'successo': '1'}
     */
    public function atualizarCarrinho($post, $idCarrinho)
    {
        return json_decode($this->call('carrinho/' . $idCarrinho, 'POST', $post));
    }
    
    /**
     * Limpar carrinho de compras
     * @param string $idCarrinho Id do carrinho
     * @return object {'successo': '1', 'removido': '1'}
     */
    public function limparCarrinho($idCarrinho)
    {
        return json_decode($this->call('carrinho/' . $idCarrinho, 'DELETE'));
    }

    /**
     * Consultar carrinho de compras
     * @param string $idCarrinho Id do carrinho
     * @return object {'successo': '1', '[produtos]': {...}, '[cupons]': {...}, '[tags]': {...}, '[totais]': {...}}
     */
    public function consultarCarrinho($idCarrinho)
    {
        return json_decode($this->call('carrinho/' . $idCarrinho, 'PUT'));
    }

    /**
     * Consultar carrinho de compras do comprador pelo id do carrinho;
     * @param string $idCarrinho Id do carrinho
     * @return object {'successo': '1', '[produtos]': {...}, '[cupons]': {...}, '[tags]': {...}, '[totais]': {...}}
     */
    public function consultarCarrinhoCliente($idCarrinho)
    {
        return json_decode($this->call('carrinho/' . $idCarrinho, 'PUT', null, true, 'U'));
    }    

    /**
     * Enviar cobrança
     * @param $post Dados de cobrança: trid, id_carrinho, documento, [email], [tipo], [validade], [forma], [descricao]
     * @return object 
     */
    public function enviarCobranca($post)
    {
        return json_decode($this->call('cobranca/', 'POST', $post));
    }
    
    /**
     * Identifica dados de uma cobrança
     * @param string $trid Id da transação
     */
    public function consultarCobranca($trid)
    {
        return json_decode($this->call('cobranca/' . $trid, 'GET'));
    }   
    
    /**
     * Autorizar pagamento. Não chamar o endpoint se for uma conciliação (venda por depósito, boleto ou TEF)
     * @param array $post Array de dados para autorizar pagamento: codigo (codigo_cobrança), codigo_cartao, senha_cartao
     * @return object
     */
    public function autorizarPagamento($post)
    {
        return json_decode($this->call('cobranca/' . $post['id_compra'], 'PUT', [
            'codigo_cartao' => $post['codigo_cartao'], 
            'senha_cartao' => $post['senha_cartao']
        ], true, 'U'));
    }
      
    /**
     * Consultar vendas por período
     * @param string $dateStart Início do período
     * @param string $dateEnd Fim do período
     * @param string $token Token: 'U' = Usuário (cliente específico) / 'V' = Vendedor
     * @return object
     */
    public function consultarVenda($dateStart, $dateEnd)
    {
        return json_decode($this->call('vendas/' . (empty($dateStart) && empty($dateEnd) ? 
                date('d-m-Yt00:00:00', strtotime('-30days')) . '/' . date('d-m-Yt23:59:59') :
                $dateStart . '/' . $dateEnd),
            'GET'));
    }
    
    /**
    * Consultar vendas com filtros
    * @param array $post Filtros para busca: data_ini, data_fim, descricao, operador, total, parcelas, trid, comprador, codigo
    * @param string $token V - loja
    * @return object
    */
    public function consultarVendaFiltro($post)
    {
        return json_decode($this->call('vendas/', 'POST', $post));
    }    
    
    /**
    * Consultar vendas por período trazendo ocupantes de assentos
    * @param string $dateStart Início do período
    * @param string $dateEnd Fim do período
    * @return object
    */
    public function consultarVendaOcupantes($dateStart, $dateEnd)
    {
        return json_decode($this->call('mineirao/vendas/' . (empty($dateStart) && empty($dateEnd) ? 
                date('d-m-Yt00:00:00', strtotime('-30days')) . '/' . date('d-m-Yt23:59:59') :  
                $dateStart . '/' . $dateEnd),
            'GET', NULL, true, 'V')
        );
    }

    /**
     * Consultar produtos de uma venda por período
     * @param integer $idVenda Código da venda
     * @return produtos a partir do carrinho
     */
    public function consultarProdutosVenda($idVenda)
    {
        $returnVenda = $this->consultarVendasFiltro(['codigo' => $idVenda]);
        return json_decode($this->call('carrinho/' . $returnVenda->objeto[0]->id_carrinho, 'PUT'));
    }

    /**
     * Consultar carrinho por tags
     * @param array $post array de tags para consulta
     * @param string $tagsDe carrinho ou produto
     * @return object IDs de carrinho que possuam as tags pesquisadas
     */
    public function consultarCarrinhoTags($post, $tagsDe='carrinho')
    {
        return json_decode($this->call('carrinho/', 'PUT', ['cobranca' => ['status' => '1'], 'tags' => [$tagsDe => $post]]));
    }

    /**
     * Consultar carrinho pelo comprador
     * @param string $documento Documento do comprador
     * @return object IDs de carrinho que possuam as tags pesquisadas
     */
    public function consultarCarrinhoComprador($documento)
    {
        return json_decode($this->call('carrinho/', 'PUT', ['cobranca' => ['status' => '1'], 'comprador' => ['documento' => $documento]]));
    }

    
    /**
     * Consultar carrinho pelo código do produto
     * @param integer $codigo Código do produto
     * @return object Informações do carrinho referente ao produto informado
     */
    public function consultarCarrinhoProduto($codigo)
    {
        return json_decode($this->call('carrinho/', 'PUT', ['produto' => ['codigo' => $codigo]]));
    }
     
    /**
     * Trazer somente dados de produtos vendidos em um carrinho
     * @param integer $idCarrinho Id do carrinho
     * @return object
     */
    public function dadosProdutos($idCarrinho){
        $return = $this->consultarCarrinho($idCarrinho);
        if ($return->successo == '1' && isset($return->objeto)) {
            return $return->objeto;
        }
    }

    /**
     * Consultar carrinho pelo código da compra, trazendo informações de produtos e ocupantes
     * @param string $compra código da compra
     * @return object IDs de carrinho que possuam as tags pesquisadas
     */
    public function consultarCarrinhoPorCodigoCompra($compra)
    {
        return json_decode($this->call('carrinho/', 'PUT', ['cobranca' => ['codigo' => $compra]]));
    }
    
    /**
    * Consultar vendas com filtros
    * @param $post Filtros para busca: data_ini, data_fim, descricao, operador, total, parcelas, trid, comprador, codigo
    * @param $token V - loja
    * @return type
    */
    public function consultarVendasFiltro($post=[])
    {
        return json_decode($this->call('vendas/', 'POST', $post));
    }

    /**
     * Clonar carrinho de compras
     * @param string $post
     */
    public function clonarCarrinho($post)
    {
        return json_decode($this->call('carrinho/' . $post['id_origem'] . '/duplicar', 'POST', ['id_destino' => $post['id_destino']]));
    }

    /**
     * Atualizar tags (remove as anteriores)
     * @param string $post
     */
    public function atualizarTagsCarrinhoLote($post,$idCarrinho)
    {
        return json_decode($this->call('carrinho/' . $idCarrinho . '/tags', 'POST', $post));
    }

    /**
     * Atualizar carrinho completo, com cupons e tags
     * @param string $post
     */
    public function salvarCarrinhoCompleto($post,$idCarrinho)
    {
        return json_decode($this->call('carrinho/' . $idCarrinho . '/completo', 'POST', $post));
    }
}
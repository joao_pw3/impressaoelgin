<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Url;
use app\models\ApiCarrinho;

class MenuWidget extends Widget 
{
    public static function getMenuVisitante()
    {
        $html = null;

        $html .= '
            <div class="col-md-2 borderRedond">
                <ul class="nav nav-pills nav-stacked verificaPagina" id="navPills" data-nav="cadastro" id="cadastro">
                    <li><a href="'.Url::to(['site/cadastro-acesso']).'" class="pesoNormal"><i class="fa fa-lock"></i> Acesso</a></li>
                    <li id="li-personal" class="disabled"><a id="dados-pessoais-vertical" href="'.Url::to(['site/cadastro-dados-pessoais']).'" title="Cadastro de dados pessoais. Aba liberada quando os dados de acesso são cadastrados." class="pesoNormal"><i class="fa fa-user"></i> Dados pessoais</a></li>
                    <li id="li-payment" class="disabled"><a id="meus-cartoes-vertical" href="'.Url::to(['site/cadastro-pagamentos']).'" title="Cadastro de dados de pagamento. Aba liberada quando os dados de acesso e pessoais são cadastrados." class="pesoNormal"><i class="fa fa-credit-card"></i> Pagamentos</a></li>
                </ul>
            </div>';

            return $html;
    }

    public static function getMenuLateral()
    {   
        $html = null;

        $html .= '
        <div class="col-md-2 borderRedond">
            <ul class="nav nav-pills nav-stacked verificaPagina" data-nav="cliente" id="liCliente">
                <li><a href="'.Url::to(['site/cliente-acesso']).'"><i class="fa fa-lock"></i> Acesso</a></li>
                <li><a href="'.Url::to(['site/cliente-dados-pessoais']).'"><i class="fa fa-user"></i> Dados pessoais</a></li>
                <li><a href="'.Url::to(['site/cliente-pagamentos']).'"><i class="fa fa-credit-card"></i> Meus cartões</a></li>
                <li><a href="'.Url::to(['site/cliente-creditos']).'"><i class="fa fa-qrcode"></i> Meus créditos</a></li>
                <li><a href="'.Url::to(['site/cliente-historico']).'"><i class="fa fa-money"></i> Minhas compras</a></li>
                <li><a href="'.Url::to(['site/cliente-mineiraocard']).'" title="Meus cartões Mineirão Card / 2º Via"><i class="fa fa-credit-card"></i> Mineirão Card</a></li>';

        if (ApiCarrinho::carrinhoAberto())
            $html .= 
                '<li><a id="finalizar-compra-vertical" href="'.Url::to(['site/pagamento']).'"><i class="fa fa-usd"></i> Finalizar compra</a></li>';

        $html .= 
                '<li><a href="'.Url::to(['site/cliente-logout']).'"><i class="fa fa-unlock"></i> Logout</a></li>
            </ul>
        </div>';

    	return $html;
    }

    public static function getMenuAtalhos()
    {
    	$session = Yii::$app->session;
        $session->open();

        $html = null;

        $html .= 
        '<ul class="nav nav-pills">
            <li class="active"><a href="'.Url::to(['site/cliente-dados-pessoais']).'"><i class="fa fa-user"></i> Dados pessoais</a></li>
            <li class="active"><a href="'.Url::to(['site/cliente-pagamentos']).'"><i class="fa fa-credit-card"></i> Meus cartões</a></li>
            <li class="active"><a href="'.Url::to(['site/cliente-historico']).'"><i class="fa fa-money"></i> Minhas compras</a></li>';


        if (isset($session['idCarrinho']) && !empty($session['idCarrinho'])) 
            $html .= 
        	'<li class="active"><a id="finalizar-compra-horizontal" href="'.Url::to(['site/pagamento']).'"><i class="fa fa-usd"></i> Finalizar compra</a></li>';

        if (isset($session['nivelAcesso']) && $session['nivelAcesso'] == 'super-admin')
        {
        	$html .= 
        	'<li class="active"><a id="compra-avulsa" href="'.Url::to(['admin/vendas/reserva']).'"><i class="fa fa-usd"></i> Reservar/Liberar lugares</a></li>';
        }

        $html .= 
        '</ul>';

    	return $html;
    }

    public static function getUrls()
    {
        return '
            <div class"urls">
                <input type="hidden" id="url-index" value="'.Url::to(['site/index']).'">
                <input type="hidden" id="url-cadastro" value="'.Url::to(['site/cadastro']).'">
                <input type="hidden" id="url-cep" value="'.Url::to(['site/check-cep']).'">
                <input type="hidden" id="url-logout" value="'.Url::to(['site/logout']).'">
                <input type="hidden" id="url-cliente" value="'.Url::to(['site/cliente-acesso']).'">
                <input type="hidden" id="url-cartao" value="'.Url::to(['site/cadastro-cartao']).'">
                <input type="hidden" id="url-resumo" value="'.Url::to(['site/resumo']).'">
                <input type="hidden" id="url-cadastro-completo" value="'.Url::to(['site/cadastro-completo']).'">
                <input type="hidden" id="url-verificar-documento" value="'.Url::to(['site/verificar-documento']).'">
            </div>';
    } 

    public static function getUrlsVisitante()
    {

        return '
            <div class"urls">
                <input type="hidden" id="url-cadastro" value="'.Url::to(['site/cadastro-basico']).'">
                <input type="hidden" id="url-login" value="'.Url::to(['site/login']).'">
                <input type="hidden" id="url-cadastro-completo" value="'.Url::to(['site/cadastro-completo']).'">
                <input type="hidden" id="url-cartao" value="'.Url::to(['site/cadastro-cartao']).'">
                <input type="hidden" id="url-cep" value="'.Url::to(['site/check-cep']).'">
                <input type="hidden" id="url-resumo" value="'.Url::to(['site/resumo']).'">
                <input type="hidden" id="url-visitante" value="'.Url::to(['site/visitante']).'">
                <input type="hidden" id="url-cliente" value="'.Url::to(['site/cliente']).'">
                <input type="hidden" id="url-index" value="'.Url::to(['site/index']).'">
                <input type="hidden" id="url-verificar-documento" value="'.Url::to(['site/verificar-documento']).'">
            </div>';
    }    
}
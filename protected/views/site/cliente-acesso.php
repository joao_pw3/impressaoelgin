<?php 
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use app\components\MenuWidget;
use app\components\TempoCarrinhoWidget;

$session = Yii::$app->session;
$session->open();

if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    echo TempoCarrinhoWidget::getBreadCrumb('losang2.png');
}

$cliente->user_id = isset($clienteApi->objeto->user_id) ? $clienteApi->objeto->user_id : '';
?>
<div class="container">
    <div class="row">

        <?php
        if($cliente->user_id) {
            echo MenuWidget::getUrls();
            echo MenuWidget::getMenuLateral(); 
        }
        else {
            echo MenuWidget::getUrlsVisitante();      
            echo MenuWidget::getMenuVisitante(); 
        }
        ?>
        <div class="col-md-10">
            <div class="tab-content">
                <div class="boxDados">

                	<?php if (!empty($cliente->user_id)) { ?>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="lead">Seja Bem-Vindo<?= !empty($_SESSION['nome']) ? ', ' . $_SESSION['nome'] : '.'; ?></div>       
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="arred"><i class="fa fa-user aumentuser" aria-hidden="false"></i></div>
                            </div>
                        </div>
                        <div class="row aument">
                            <p>Este é o espaço cliente Mineirão. Aqui você pode consultar e alterar seus dados pessoais e as informações dos seus cartões. Além disso, você pode consultar suas compras.</p> 
                        </div> 
                    </div>        
                    <?php } ?>
                	<hr class="linhFina">

                    <?php $form = ActiveForm::begin(['id' => 'form-cadastro']); ?>

                        <div class="row">          
                            <div class="col-md-6 maisMargem">
                    		
                                <?= $form->field($cliente, 'user_id')->textInput([
                                    'class' => 'form-control form-user-id',
                                    'placeholder' => 'Digite aqui seu e-mail',
                                    'title' => 'Por favor, digite seu e-mail',
                                    'id' => 'user_id',
                                    'disabled' => !empty($cliente->user_id) ? true : false
                                ]); ?>
                            </div>
                        </div>
                        
                        <?php if (!$cliente->user_id) { ?>
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($cliente, 'senha')->passwordInput([
                                    'class' => 'form-control form-user-id',
                                    'placeholder' => 'Informe sua senha',
                                    'id' => 'senha',
                                    'title' => 'Informe sua senha. Esta senha serve para recuperar os seus dados cadastrais e de cartão, mas não autoriza pagamentos.',
                                    'maxlength' => 10
                                ]); ?>
                            </div>        
                            <div class="col-md-6">
                                <?= $form->field($cliente, 'senha_repeat')->passwordInput([
                                    'class' => 'form-control form-user-id',
                                    'placeholder' => 'Confirme sua senha',
                                    'id' => 'senha_repeat',
                                    'title' => 'Confirme sua senha.',
                                    'maxlength' => 10
                                ]); ?>
                            </div>
                        </div>
                    	<div class="row">
                    		<div class="col-md-12">
                    			<br>
                                <?= $form->field($cliente, 'cliente_aceite')->checkbox(['id' => 'cliente-aceite', 'label' => 'Aceito os termos do <a href="#null" id="btn-termos">REGULAMENTO MINEIRÃO TRIBUNA 2018 E MINEIRÃO EMBAIXADA 2018</a>']); ?>
								<br>
								
								CONHEÇA NOSSA <a href="#null" id="btn-politica">POLÍTICA DE PRIVACIDADE</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="margin-top:50px;">
                                <span class="contato">
                                    <input type="button" value="Prosseguir" id="btn-cli-acesso" class="icon-next carrinho areas">
                                </span>
                            </div>
                        </div>
                    <?php }

                    ActiveForm::end();
                    
                    if (!empty($cliente->user_id)) { 

                    ?>        
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 borderRedond">

                                <?php echo MenuWidget::getMenuAtalhos(); ?>
                            </div>    
                        </div>
                    </div>    
                    <?php } ?>
                    
                    <!-- Modal - Esqueci minha senha -->
                    <?php Modal::begin([
                        'header' => '<h4 align="center">Trocar senha</h4>',
                        'size'   => 'modal-sm',
                        'id'     => 'modal-senha',
                        'options'=> [
                            'class' => 'modal-center'
                        ]
                    ]); ?>
                    <form id="form-esqueci-senha" name="form-esqueci-senha" method="POST">
                        <input type="hidden" id="url-mensagem-esqueci" value="<?= Url::to(['site/mensagem-esqueci-senha']); ?>">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="user_id-esqueci">E-mail:</label>
                                <input type="text" name="user_id" id="user_id-esqueci" class="form-control" readonly>
                                <p>Será enviada uma mensagem para este e-mail com instruções para a troca de sua senha.</p>
                            </div>
                			
                        </div>
                        <div class="row">
                            <div class="col-md-12 contato" align="center">
                                <input type="button" value="Enviar" id="btn-esqueci-senha" class="icon-next esqueci-senha">
                            </div>
                        </div>
                    </form>
                    <?php Modal::end(); ?>
                	
                    <!-- Modal - Termos -->
                    <?php Modal::begin([
                        'header' => '<h4 align="center">REGULAMENTO MINEIRÃO TRIBUNA 2018 e EMBAIXADA  2018</h4>',
                        'size'   => 'modal-lg',
                        'id'     => 'modal-termos',
                        'options'=> [
                            'class' => 'modal-center'
                        ]
                    ]); ?>    
                	<div class="row">
                            <div class="col-md-12">
                                    <div class="scrollTermos">
                			<h2>REGULAMENTO MINEIRÃO TRIBUNA 2018</h2><br>
                					
                            <p>Este regulamento descreve o Mineirão Tribuna 2018, disponibilizado aos Torcedores que queiram efetuar ou tenham efetuado a compra deste produto (“Torcedores”), e estabelece os benefícios, as regras e as condições gerais aplicáveis aos compradores do Passaporte (“Regulamento”).</p>

                            <p>1. ABRANGÊNCIA DO MINEIRÃO TRIBUNA 2018</p>

                            <p>1.1. O Mineirão Tribuna 2018 considera apenas assentos localizados no setor roxo inferior no Estádio Governador Magalhães Pinto (“Mineirão”), em blocos a serem indicados exclusivamente pelo Mineirão, para uso exclusivamente em Jogos Oficiais.</p>
                            <p>1.1.1. Para fins do Mineirão Tribuna 2018, “Jogos Oficiais” são as partidas de futebol disputadas no Mineirão pelas Equipes Profissionais dos Clubes de Futebol de Minas Gerais em que tenham o mando de campo, em todo e qualquer campeonato de futebol local, regional, nacional e internacional.</p>
                            <p>1.1.2. Desta forma, não se incluem, mas não se limitando, no conceito de Jogos Oficiais, os jogos de futebol realizados no Mineirão:<br />
                            (i) Pela Seleção Brasileira de Futebol e as demais seleções de futebol de outros países.<br />
                            (ii) Pelos Clubes de Futebol de outros Estados do Brasil e Distrito Federal, em quaisquer campeonatos. <br />
                            (iii) Organizados pela Federação Internacional de Futebol - FIFA e pelo Comitê Organizador dos Jogos Olímpicos.<br />
                            (iv) Considerados como partidas de futebol amistosas ou festivas.</p>
                            <p>1.1.3. Nos casos de “rodada-dupla” de Jogos Oficiais, ou seja, a ocorrência de Jogos Oficiais com intervalo no mesmo dia de realização, estas serão consideradas como apenas um Jogo Oficial.</p>
                            <p>1.1.4. O número de cadeiras disponibilizadas será critério exclusivo de definição pelo Mineirão.</p>
                            <p>1.1.5. O setor roxo inferior do Mineirão é destinado a torcida mista, isto é, tanto para o time mandante, como para o time visitante. Contudo, a critério do Mineirão, em Jogos Oficiais específicos, tais como clássicos e finais de campeonatos, o referido setor poderá sofrer alteração para ser destinado apenas à torcida do time mandante. Neste caso, recomendamos que qualquer vestimenta ou adereço relacionado ao time visitante seja evitado pelo Torcedor.</p>
                            <p>1.2. O Torcedor que adquirir o Mineirão Tribuna 2018 poderá assistir a todos os Jogos Oficiais até o final da temporada de futebol de 2018, que se dará em 31 de dezembro de 2018.</p>
                            <p>1.3. As regras e horários dos Jogos Oficiais são determinados pelos organizadores dos campeonatos de futebol. O Mineirão não se responsabiliza por eventuais alterações que ocorram durante este período.</p>
                            <p>1.4. Os horários e as datas dos Jogos Oficiais, e de acessos ao Mineirão, serão divulgados no sítio eletrônico estadiomineirao.com.br para informação e acompanhamento dos Torcedores que adquirirem o Mineirão Tribuna 2018.</p>
                            <p>1.5. Não acarretará qualquer tipo de multa, ressarcimento ou indenização ao Torcedor que adquirir o Mineirão Tribuna 2018, a não realização e/ou cancelamento, por quaisquer motivos, de qualquer Jogo Oficial e/ou campeonato de futebol previstos e/ou confirmados no Mineirão, tais como, mas não se limitando: (i) não classificação dos Clubes de Futebol de Minas Gerais em campeonatos, (ii) Jogos Oficiais sem torcida, (iii) eventuais penalidades aplicadas pelo Tribunal de Justiça Desportiva, e (iv) fatos supervenientes, fortuitos, ou de força maior.</p>

                            <p>2. PASSAPORTE MINEIRÃO TRIBUNA 2018</p>

                            <p>2.1. O Torcedor receberá um cartão mifare que lhe dará acesso ao Mineirão, por meio de leitura nas catracas existentes no Mineirão, para assistir aos Jogos Oficiais (“Passaporte”).</p>  
                            <p>2.1.1. O Passaporte será disponibilizado ao Torcedor que o adquiriu em até 15 (quinze) dias úteis da respectiva confirmação de pagamento enviada pelo Mineirão, nos dias da realização de Jogos Oficiais.</p>
                            <p>2.1.2. O Passaporte deverá ser retirado, pelo Torcedor que o adquiriu, pessoalmente na Bilheteria Sul do Mineirão, no dia da realização de Jogos Oficiais, munido de: (i) voucher emitido pelo sítio eletrônico no qual realizou o cadastro, (ii) o cartão de crédito que realizou a compra e a sua cópia impressa, (iii) e o documento de identidade válido em todo o território nacional, onde conste foto recente e número do CPF do comprador, nos seguintes dias e horários: no dia de Jogo Oficial, entre as 10h00min e até os primeiros 10 (dez) minutos do segundo tempo do Jogo Oficial. Se for o caso de terceiro que efetuará a retirada do Passaporte, este terceiro deverá estar munido de todos os documentos acima listados, mais uma procuração com firma reconhecida em cartório, com o seu respectivo documento de identidade válido em todo o território nacional, onde conste foto recente e número do CPF.</p>
                            <p>2.1.2.1. Como medida de segurança, fica autorizada no ato da retirada a cópia (decalque) dos 06 (seis) primeiros dígitos e dos 04(quatro) últimos dígitos do cartão utilizado no pagamento ou foto dessa mesma sequência de dígitos, no protocolo de entrega que deverá ser assinado, caso não seja apresentada uma cópia impressa do cartão.</p>
                            <p>2.1.3. O acesso ao Mineirão, pelo Torcedor, só poderá ocorrer mediante a utilização do Passaporte, sem prejuízo da apresentação de outros documentos que se façam necessários nos termos da lei e deste Regulamento. Caso haja a realização de Jogos Oficiais no período em que o Passaporte não tenha sido disponibilizado, pelo Mineirão, ao Torcedor, o Torcedor que o adquiriu deverá retirar o ingresso impresso referente ao Jogo Oficial, pessoalmente na Bilheteria Sul do Mineirão, munido de: (i) voucher emitido pelo sítio eletrônico no qual realizou o cadastro, (ii) o cartão de crédito que realizou a compra, (iii) e o documento de identidade válido em todo o território nacional, onde conste foto recente e número do CPF do comprador, nos seguintes dias e horários: no dia de Jogo Oficial, entre as 10h00min e até os primeiros 10 (dez) minutos do segundo tempo do Jogo Oficial. Se for o caso de terceiro que efetuará a retirada do ingresso impresso, este terceiro deverá estar munido de todos os documentos acima listados, mais uma procuração com firma reconhecida em cartório, com o seu respectivo documento de identidade válido em todo o território nacional, onde conste foto recente e número do CPF.</p>
                            <p>2.2. Para cada Torcedor/CPF válido cadastrado, será limitada a compra de até 06 (seis) Passaportes.</p>
                            <p>2.3. Cada Passaporte corresponderá a um assento determinado.</p>
                            <p>2.4. O Passaporte é individual e intransferível, não sendo permitida a sua comercialização a terceiros, inclusive a realização de ações promocionais com ingressos, sob pena de, conforme o caso, a comercialização, incluindo a prática de atos de agenciamento e revenda do ingresso, ser caracterizada ilícito penal e/ou cível.</p>
                            <p>2.5. Em caso de perda, extravio, furto ou roubo do Passaporte (“Perda”), o Torcedor deverá imediatamente contatar o Mineirão no contato constante deste Regulamento (vide item 7.2 abaixo), e solicitar a emissão de um novo Passaporte para ter acesso ao Mineirão em dias de Jogos Oficiais.</p>
                            <p>2.5.1. Após o contato do Torcedor, o Passaporte será imediatamente cancelado.</p>
                            <p>2.5.2.  Para a emissão de novo Passaporte, o Torcedor deverá preencher requisição específica para a emissão de novo Passaporte e, após, efetuar o pagamento de uma taxa correspondente a R$60,00 (sessenta reais).</p>
                            <p>2.5.3. O pagamento da taxa deverá ocorrer por meio de transferência bancária identificada para a conta corrente de número 08381-1, de titularidade do Mineirão, no Banco Itaú, Agência 2001. Após o pagamento, o Mineirão deverá ser imediatamente informado para verificar a quitação do valor.</p>
                            <p>2.5.4. Até a confecção de novo Passaporte, e somente após o pagamento/quitação da taxa constante do item 2.5.2 acima, o Torcedor poderá retirar ingressos impressos nos termos do item 2.1.3 acima, caso pretendam assistir aos Jogos Oficiais.</p>
                            <p>2.5.5. Caso o Mineirão não seja informado da Perda de novo Passaporte, o Mineirão não será em hipótese alguma responsabilizado por eventual utilização do Passaporte por terceiro, sendo vetado o acesso do Torcedor, ao Mineirão, em dias de Jogos Oficiais sem a utilização do Passaporte, nos termos do item 2.1.3 acima.</p>  
                            <p>2.6. Extinto ou suspenso o direito ao uso do Passaporte, independentemente do motivo, nos termos deste Regulamento, o Torcedor deverá restituir o Passaporte ao Mineirão, sob pena de pagamento da taxa constante do item 2.5.2 acima.</p>

                            <p>3. CADASTRO</p>

                            <p>3.1. Para adquirir o Passaporte, os Torcedores deverão acessar o sítio eletrônico estadiomineirao.com.br e efetuar o seu cadastro.</p>
                            <p>3.2. A compra do Passaporte ocorrerá exclusivamente através do sítio eletrônico estadiomineirao.com.br, mediante cadastro prévio e concordância com o presente termo e Regulamento.</p>
                            <p>3.3. O cadastro no sítio eletrônico estadiomineirao.com.br estará disponível apenas para os Torcedores que possuem número de CPF válido.</p>
                            <p>3.4. O Torcedor poderá se cadastrar uma única vez com seu próprio CPF válido (1 cadastro por nome e CPF).</p>
                            <p>3.5. Serão confirmados somente os cadastros dos Torcedores que preencherem todos os campos obrigatórios, com informações exatas e verídicas.</p>
                            <p>3.6. A incorreção das informações prestadas pelo Torcedor será de sua total responsabilidade, respondendo este pela veracidade e autenticidade de seus dados cadastrais, assumindo o dever de manter atualizadas essas informações no ambiente virtual do sítio eletrônico.</p>
                            <p>3.7. Qualquer cadastro realizado com informações errôneas e/ou inverídicas será cancelado, com a consequente perda do histórico de compras de jogos e pagamentos, se houver, não sendo devido ao Torcedor qualquer tipo de multa, ressarcimento ou indenização.</p>
                            <p>3.8. Para atualização e/ou regularização do cadastro, o Torcedor deverá entrar em contato por meio do endereço eletrônico atendimento@estadiomineirao.com.br, informar os dados corretos e solicitar a atualização.</p>
                            <p>3.9. O Torcedor deverá, no momento de seu cadastro, escolher um login (nome de usuário sem espaço e/ou caracteres) e uma senha (sem espaço e/ou caracteres). Estes dados deverão ser utilizados para acesso no sítio eletrônico estadiomineirao.com.br, através do qual o Torcedor poderá comprar os Passaportes.</p>
                            <p>3.10. Caso seja identificada fraude no cadastro Torcedor, os dados serão enviados aos órgãos competentes.</p>

                            <p>4. COMPRA E FORMAS DE PAGAMENTO</p>

                            <p>4.1. O valor do Passaporte para a temporada de 2018 será aquele indicado do sítio eletrônico responsável pela comercialização do Mineirão Tribuna 2018 quando da respectiva data de compra pelo Torcedor, e poderá ser pago em até 12 (doze) parcelas iguais e fixas através de cartões de crédito (Visa ou Mastercard).</p> 
                            <p>4.2. Para a compra do Passaporte não serão válidas as condições de meia entrada.</p>
                            <p>4.3. Não é possível realizar reserva de Passaporte.</p>
                            <p>4.4. Para dúvidas relacionadas ao processamento do pagamento, entre em contato com o Plantão de Atendimento da EasyForPay por telefone ou whatsApp (11) 97190-8834, ou através do e-mail do contato@easyforpay.com.br. A EasyForPay é a empresa de pagamentos dos ingressos do Mineirão.</p>

                            <p>5. CANCELAMENTO</p>

                            <p>5.1. Uma vez efetivada a compra do Passaporte, não serão aceitos quaisquer devoluções e/ou cancelamentos ao Torcedor.</p>
                            <p>5.2. Sem prejuízo do item 5.1, poderá ser cancelado o Passaporte cujo Torcedor não tenha acessado a nenhum Jogo Oficial.</p>

                            <p>6. DISPOSIÇÕES GERAIS</p>

                            <p>6.1. A compra está sujeita à observância dos termos e condições aqui descritos, com os quais o Torcedor manifesta expressa concordância ao adquirir seu Passaporte.</p>
                            <p>6.2. O ingresso do Torcedor no Mineirão somente será permitido àqueles devidamente possuidores dos Passaportes, e demais documentos exigidos pelo Mineirão, conforme o caso.</p>
                            <p>6.3. A aquisição do Passaporte não garante acesso irrestrito às dependências do Mineirão.</p>
                            <p>6.4. Todas as áreas, dependências, instalações e equipamentos do Mineirão, bem como atividades desenvolvidas no Mineirão, sempre estarão sujeitos à disciplina, segurança e administração do Mineirão, que as exercerá sem qualquer limitação ou restrição, segundo seu exclusivo critério ou mediante determinações das autoridades competentes, seja diretamente ou por pessoas ou empresas por ele indicadas ou contratadas para tanto, de modo a garantir a ordem e a segurança do Mineirão.</p>
                            <p>6.5. Todos os Torcedores que adentrarem no Mineirão deverão consentir com a revista pessoal de prevenção e segurança, sendo condição de acesso e permanência do Torcedor no Mineirão.</p>
                            <p>6.6. Sempre que solicitado, o Torcedor deverá apresentar seu Passaporte, bem como documento de identidade válido em todo território nacional que contenha foto recente.</p>
                            <p>6.6.1. Será facultado ao Mineirão estabelecer, a seu exclusivo critério, novos mecanismos de acesso.</p>
                            <p>6.7. O ingresso e a permanência de incapazes no Mineirão, a exemplo de crianças e adolescentes, deverão ocorrer sempre em consonância com as normas aplicáveis, e dependerá, nas hipóteses em que a legislação assim determinar, da presença dos pais, do responsável legal ou da existência de autorização judicial própria, observadas, em especial, as regras emitidas pelas Varas de Infância e Juventude da Comarca de Belo Horizonte e a respectiva classificação etária do Jogo Oficial.</p>
                            <p>6.8. Os incapazes também deverão portar Passaporte ou ingresso válido, observada, em todo o caso, a necessidade de acompanhamento pelo responsável legal, o qual também deverá portar Passaporte ou ingresso válido.</p>
                            <p>6.9. São vedados a entrada, o porte e a utilização no Mineirão, inclusive nos assentos, de objetos proibidos pelas autoridades competentes, por legislação federal, estadual ou municipal ou que coloquem em risco os Torcedores ou terceiros presentes no Mineirão, a qualquer título, em especial:<br />
                            I. Fogos de artifício, objetos explosivos, sinalizadores ou quaisquer outros engenhos pirotécnicos ou produtores de efeitos análogos.<br />
                            II. Substâncias ilícitas, de qualquer natureza e entorpecentes.<br />
                            III. Objetos, bebidas ou substâncias proibidas ou suscetíveis de gerar ou possibilitar a prática de atos de violência, em especial:<br />
                            a) Armas de qualquer tipo e qualquer objeto que possa ser usada como arma e outros objetos que sejam proibidos pela política de segurança do Mineirão.<br >
                            b) Instrumentos que produzam volume excessivo de barulho, tais como: megafones, sirenes ou  buzinas a gás, inclusive instrumentos de sopro.<br />
                            <p>6.10. Ressalvados os alimentos e bebidas comercializados no Mineirão e autorizados por si, são vedados o acesso e a permanência nos assentos com bebida, líquidos ou alimentos de qualquer tipo.</p>
                            <p>6.11. Em consonância com o disposto no item anterior, é especialmente vedado o porte, o acesso, a venda ou o consumo de bebidas alcoólicas durante a realização de Jogos Oficiais. A proibição de que trata este item poderá, a exclusivo critério do Mineirão, ser flexibilizada na hipótese de alteração das normas ou determinações das autoridades competentes, a respeito do consumo de bebida alcoólica no Mineirão.</p>
                            <p>6.12. O porte de bandeiras no Mineirão e nos assentos deverá observar as regras de segurança estabelecidas pelo Mineirão e as entidades competentes, em especial a Polícia Militar do Estado de Minas Gerais, bem como os acordos de patrocínio eventualmente firmados pelo Mineirão com terceiros.</p>
                            <p>6.13. O consumo de fumígenos de qualquer espécie, tais como cigarros, charutos e outros, não será permitido no Mineirão.</p>
                            <p>6.14. O Torcedor deverá guardar e se responsabilizar pelos seus bens levados ao Mineirão, não podendo ser atribuída ao Mineirão qualquer responsabilidade pela guarda, ou por casos de quebra ou extravio de bens pessoais do Torcedor, seja durante a realização de Jogos Oficiais, ou mesmo após tais jogos.</p>
                            <p>6.15.  O Torcedor que adquirir um Passaporte se compromete, desde já, a assumir uma conduta respeitosa e ética, em relação aos demais torcedores, funcionários, atletas, membros da comissão técnica e dirigentes dos Clubes de Futebol, abstendo-se de praticar quaisquer atos que venham a prejudicar a imagem do Mineirão, a qualquer momento, dentro e fora do âmbito da arena, sob pena de sofrer as sanções de suspensão temporária ou exclusão e demais sanções permitidas em Lei.</p>
                            <p>6.16. As seguintes condutas são expressamente proibidas ao Torcedor, sem prejuízo de outras proibições de legislação ou deste Regulamento:<br />
                            I.  Arremessar objeto, de qualquer natureza, no interior do Mineirão, em especial das cadeiras e em direção a outra pessoa ou local, tal como nas cadeiras inferiores e no campo.<br />
                            II. Subir ou apoiar-se em estruturas e instalações não planejadas para tal uso, em especial, mas não se restringindo a fachadas, cercas, muros, grades, alambrados, cadeiras, postes de luz, barreiras ou guarda-corpo.<br />
                            III. Praticar conduta que possa atrapalhar o aproveitamento do Jogo Oficial por outros espectadores, ou dar causa à sua interrupção ou não realização.<br />
                            IV. Incitar ou praticar atos de violência física ou moral no Mineirão.<br />
                            V. Criar qualquer ameaça para a vida ou para a segurança de si próprio ou de terceiros.<br />
                            VI. Entoar cânticos, portar ou ostentar mensagens, cartazes, bandeiras, símbolos ou outros sinais com mensagens de que sejam ofensivas, inclusive de caráter discriminatório, racista ou xenófobo, ou que de qualquer modo incitem a violência e a desordem.<br />
                            VII. Escrever, pintar ou afixar qualquer coisa nos elementos estruturais, instalações ou passagens do Mineirão, bem como realizar qualquer conduta que possa provocar danos às estruturas e instalações do Mineirão.<br />
                            VIII. Invadir ou incitar a invasão, de qualquer forma, da área restrita aos competidores, à equipe técnica ou operacional do Mineirão ou de áreas fechadas ao público em geral.<br />
                            IX. Restringir ou bloquear passagens, entradas e saídas de pessoas ou veículos no Mineirão.<br />
                            X. Ficar de pé nos assentos, em especial nos assentos, nas áreas de espectadores ou obstruir a visão de outros espectadores de forma não razoável.<br />
                            XI. Entrar ou permanecer em traje de banho ou sem camisa nas dependências do Mineirão, inclusive nas dos assentos.<br />
                            XII. Entrar com animais no Mineirão e, por consequência acessar os assentos, à exceção de cães guia que auxiliem deficientes visuais, nos termos e condições da Lei Federal nº 11.126, de 27 de junho de 2005, e do Decreto nº 5.904, de 27 de setembro de 2006, ou outra norma que as complemente ou substitua.<br />
                            <p>6.17. O desrespeito às regras previstas neste Regulamento, e sem prejuízo de eventuais perdas e danos a serem arcados pelo Torcedor:<br />
                            I. Ensejará o recolhimento imediato, pelo Mineirão, de todos os materiais irregulares eventualmente identificados, sem que seja devida a devolução ou a indenização em relação aos materiais recolhidos, sem prejuízo, da notificação das autoridades públicas competentes quando cabível.<br />
                            II. Implicará, quando necessário e a critério do Mineirão, a impossibilidade de ingresso temporário ou definitivo do Torcedor no Mineirão, ou, se for o caso, o seu afastamento imediato do Mineirão, sem prejuízo de outras sanções contratuais, administrativas, civis ou penais eventualmente aplicáveis.<br />
                            III. Ao imediato pagamento, pelo Torcedor, ao Mineirão, de valor correspondente ao conserto do dano praticado, caso acarretado por culpa do Torcedor.<br />
                            <p>6.18. Este Regulamento não gera nenhum contrato de sociedade, mandato, franquia, intermediação, agência ou relação de trabalho entre o Mineirão e o Torcedor.</p>
                            <p>6.19. Este Regulamento será regido pela legislação brasileira, notadamente pelo Código Civil, Código de Defesa do Consumidor e Estatuto do Torcedor.</p>
                            <p>6.20. As regras deste Regulamento são válidas apenas para os Jogos Oficiais realizados no ano de 2018. </p>

                            <p>7. CONTATO</p>

                            <p>7.1. Todas as comunicações do Mineirão ao Torcedor ocorrerão por meio de correspondência eletrônica, o que se torna importante a atualização imediata do cadastro do Torcedor sempre que houver alguma alteração no endereço eletrônico apresentado quando do cadastro do Torcedor.</p> 
                            <p>7.2. Endereço do Mineirão: Avenida Antônio Abrahão Caram, nº 1.001, Bairro São José, CEP 31.275-000, Belo Horizonte, MG. Horário de atendimento: das 09h00min às 18h00min em dias úteis comerciais. Telefone: (31) 3499-4333. E-mail: <a href="mailto:atendimento@estadiomineirao.com.br">atendimento@estadiomineirao.com.br</a></p><br><br><hr>			
                			
                			
                			
                			<h2>REGULAMENTO MINEIRÃO EMBAIXADA 2018</h2><br>
                			
                			<p>Este regulamento descreve o Mineir&atilde;o
                Embaixada 2018, disponibilizado aos Torcedores que queiram efetuar ou
                tenham efetuado a compra deste produto (&ldquo;Torcedores&rdquo;), e
                estabelece os benef&iacute;cios, as regras e as condi&ccedil;&otilde;es
                gerais aplic&aacute;veis aos compradores do Passaporte
                (&ldquo;Regulamento&rdquo;).</p>
                			
                			
                			<p>1.1. O&nbsp;Mineir&atilde;o Embaixada
                2018&nbsp;considera apenas&nbsp;assentos&nbsp;localizados no setor
                vermelho inferior no Est&aacute;dio Governador Magalh&atilde;es Pinto
                (&ldquo;Mineir&atilde;o&rdquo;), em blocos e assentos a serem
                indicados exclusivamente pelo Mineir&atilde;o, para uso
                exclusivamente em Jogos Oficiais.</p>
                <p>1.1.1. Para fins do Mineir&atilde;o
                Embaixada 2018, &ldquo;Jogos Oficiais&rdquo; s&atilde;o as partidas
                de futebol disputadas no Mineir&atilde;o pelas Equipes Profissionais
                dos Clubes de Futebol de Minas Gerais em que tenham o mando de campo,
                em todo e qualquer campeonato de futebol local, regional, nacional e
                internacional.</p>
                <p>1.1.2. Desta forma, n&atilde;o se
                incluem, mas n&atilde;o se limitando, no conceito de Jogos Oficiais,
                os jogos de futebol realizados no Mineir&atilde;o:</p>
                <p>(i) Pela Sele&ccedil;&atilde;o
                Brasileira de Futebol e as demais sele&ccedil;&otilde;es de futebol
                de outros pa&iacute;ses.</p>
                <p>(ii) Pelos Clubes de Futebol de outros
                Estados do Brasil e Distrito Federal, em quaisquer campeonatos.</p>
                <p>(iii) Organizados pela Federa&ccedil;&atilde;o
                Internacional de Futebol - FIFA e pelo Comit&ecirc; Organizador dos
                Jogos Ol&iacute;mpicos.</p>
                <p>(iv) Considerados como partidas de
                futebol amistosas ou festivas.</p>
                <p>1.1.3. Nos casos de &ldquo;rodada-dupla&rdquo;
                de Jogos Oficiais, ou seja, a ocorr&ecirc;ncia de Jogos Oficiais com
                intervalo no mesmo dia de realiza&ccedil;&atilde;o, estas ser&atilde;o
                consideradas como apenas um Jogo Oficial.</p>
                <p>1.1.4. O n&uacute;mero de cadeiras
                disponibilizadas ser&aacute; crit&eacute;rio exclusivo de defini&ccedil;&atilde;o
                pelo Mineir&atilde;o.</p>
                <p>1.1.5. O setor vermelho inferior do
                Mineir&atilde;o &eacute; destinado &agrave; torcida do time mandante.
                Contudo, a crit&eacute;rio do Mineir&atilde;o, em Jogos Oficiais o
                referido setor poder&aacute; sofrer altera&ccedil;&atilde;o para ser
                destinado &agrave; torcida mista do time mandante.&nbsp;Neste caso,
                recomendamos que qualquer vestimenta ou adere&ccedil;o relacionado ao
                time visitante seja evitado pelo Torcedor.</p>
                <p>1.2. O&nbsp;Torcedor&nbsp;que adquirir
                o Mineir&atilde;o Embaixada 2018 poder&aacute; assistir a todos os
                Jogos Oficiais de janeiro de 2018 at&eacute; o final da temporada de
                futebol de 2018, que se dar&aacute; em 31 de dezembro de 2018.</p>
                <p>1.3. As regras e hor&aacute;rios dos
                Jogos Oficiais s&atilde;o determinados pelos organizadores dos
                campeonatos de futebol. O Mineir&atilde;o n&atilde;o se
                responsabiliza por eventuais altera&ccedil;&otilde;es que ocorram
                durante este per&iacute;odo.</p>
                <p>1.4. Os hor&aacute;rios e as datas dos
                Jogos Oficiais, e de acessos ao Mineir&atilde;o, ser&atilde;o
                divulgados no s&iacute;tio eletr&ocirc;nico&nbsp;estadiomineirao.com.br
                para informa&ccedil;&atilde;o e acompanhamento dos Torcedores que
                adquirirem o Mineir&atilde;o Embaixada 2018.</p>
                <p>1.5. N&atilde;o acarretar&aacute;
                qualquer tipo de multa, ressarcimento ou indeniza&ccedil;&atilde;o ao
                Torcedor que adquirir o Mineir&atilde;o Embaixada 2018, a n&atilde;o
                realiza&ccedil;&atilde;o e/ou cancelamento, por quaisquer motivos, de
                qualquer Jogo Oficial e/ou campeonato de futebol previstos e/ou
                confirmados no Mineir&atilde;o, tais como, mas n&atilde;o se
                limitando: (i) n&atilde;o classifica&ccedil;&atilde;o dos Clubes de
                Futebol de Minas Gerais em campeonatos, (ii) Jogos Oficiais sem
                torcida, (iii) eventuais penalidades aplicadas pelo Tribunal de
                Justi&ccedil;a Desportiva, e (iv) fatos supervenientes, fortuitos, ou
                de for&ccedil;a maior.</p>
                <p>
                </p>
                <p>2. PASSAPORTE MINEIR&Atilde;O EMBAIXADA
                2018</p>
                <p>
                </p>
                <p>2.1. O Torcedor receber&aacute; um
                cart&atilde;o mifare que lhe dar&aacute; acesso ao Mineir&atilde;o,
                por meio de leitura nas catracas existentes no Mineir&atilde;o, para
                assistir aos Jogos Oficiais (&ldquo;Passaporte&rdquo;).  
                </p>
                <p>2.1.1. O Passaporte ser&aacute;
                disponibilizado ao Torcedor que o adquiriu em at&eacute; 15 (quinze)
                dias &uacute;teis da respectiva confirma&ccedil;&atilde;o de
                pagamento enviada pelo Mineir&atilde;o, nos dias da realiza&ccedil;&atilde;o
                de Jogos Oficiais.</p>
                <p>2.1.2. O Passaporte dever&aacute; ser
                retirado, pelo Torcedor que o adquiriu, pessoalmente na Bilheteria
                Sul do Mineir&atilde;o, no dia da realiza&ccedil;&atilde;o de Jogos
                Oficiais, munido de: (i) voucher emitido pelo s&iacute;tio eletr&ocirc;nico
                no qual realizou o cadastro, (ii) o cart&atilde;o de cr&eacute;dito
                que realizou a compra e a sua c&oacute;pia impressa, (iii) e o
                documento de identidade v&aacute;lido em todo o territ&oacute;rio
                nacional, onde conste foto recente e n&uacute;mero do CPF do
                comprador, nos seguintes dias e hor&aacute;rios: no dia de Jogo
                Oficial, entre as 10h00min e at&eacute; os primeiros 10 (dez) minutos
                do segundo tempo do Jogo Oficial. Se for o caso de terceiro que
                efetuar&aacute; a retirada do Passaporte, este terceiro dever&aacute;
                estar munido de todos os documentos acima listados, mais uma
                procura&ccedil;&atilde;o com firma reconhecida em cart&oacute;rio,
                com o seu respectivo documento de identidade v&aacute;lido em todo o
                territ&oacute;rio nacional, onde conste foto recente e n&uacute;mero
                do CPF.</p>
                <p>2.1.2.1. Como medida de seguran&ccedil;a,
                fica autorizada no ato da retirada a c&oacute;pia (decalque) dos 06
                (seis) primeiros d&iacute;gitos e dos 04(quatro) &uacute;ltimos
                d&iacute;gitos do cart&atilde;o utilizado no pagamento ou foto dessa
                mesma sequ&ecirc;ncia de d&iacute;gitos, no protocolo de entrega que
                dever&aacute; ser assinado, caso n&atilde;o seja apresentada uma
                c&oacute;pia impressa do cart&atilde;o.</p>
                <p>2.1.3. O acesso ao Mineir&atilde;o,
                pelo Torcedor, s&oacute; poder&aacute; ocorrer mediante a utiliza&ccedil;&atilde;o
                do Passaporte, sem preju&iacute;zo da apresenta&ccedil;&atilde;o de
                outros documentos que se fa&ccedil;am necess&aacute;rios nos termos
                da lei e deste Regulamento. Caso haja a realiza&ccedil;&atilde;o de
                Jogos Oficiais no per&iacute;odo em que o Passaporte n&atilde;o tenha
                sido disponibilizado, pelo Mineir&atilde;o, ao Torcedor, o Torcedor
                que o adquiriu dever&aacute; retirar o ingresso impresso referente ao
                Jogo Oficial, pessoalmente na Bilheteria Sul do Mineir&atilde;o,
                munido de: (i) voucher emitido pelo s&iacute;tio eletr&ocirc;nico no
                qual realizou o cadastro, (ii) o cart&atilde;o de cr&eacute;dito que
                realizou a compra, (iii) e o documento de identidade v&aacute;lido em
                todo o territ&oacute;rio nacional, onde conste foto recente e n&uacute;mero
                do CPF do comprador, nos seguintes dias e hor&aacute;rios: no dia de
                Jogo Oficial, entre as 10h00min e at&eacute; os primeiros 10 (dez)
                minutos do segundo tempo do Jogo Oficial. Se for o caso de terceiro
                que efetuar&aacute; a retirada do ingresso impresso, este terceiro
                dever&aacute; estar munido de todos os documentos acima listados,
                mais uma procura&ccedil;&atilde;o com firma reconhecida em cart&oacute;rio,
                com o seu respectivo documento de identidade v&aacute;lido em todo o
                territ&oacute;rio nacional, onde conste foto recente e n&uacute;mero
                do CPF.</p>
                <p>2.2. Para cada Torcedor/CPF v&aacute;lido
                cadastrado, ser&aacute; limitada a compra de at&eacute; 06 (seis)
                Passaportes.</p>
                <p>2.3. Cada Passaporte corresponder&aacute;
                a um assento determinado.</p>
                <p>2.4. O Passaporte &eacute; individual e
                intransfer&iacute;vel, n&atilde;o sendo permitida a sua
                comercializa&ccedil;&atilde;o a terceiros, inclusive a realiza&ccedil;&atilde;o
                de a&ccedil;&otilde;es promocionais com ingressos, sob pena de,
                conforme o caso, a comercializa&ccedil;&atilde;o, incluindo a pr&aacute;tica
                de atos de agenciamento e revenda do ingresso, ser caracterizada
                il&iacute;cito penal e/ou c&iacute;vel.</p>
                <p>2.5. Em caso de perda, extravio, furto
                ou roubo do Passaporte (&ldquo;Perda&rdquo;), o Torcedor dever&aacute;
                imediatamente contatar o Mineir&atilde;o no contato constante deste
                Regulamento (vide item 7.2 abaixo), e solicitar a emiss&atilde;o de
                um novo Passaporte para ter acesso ao Mineir&atilde;o em dias de
                Jogos Oficiais.</p>
                <p>2.5.1. Ap&oacute;s o contato do
                Torcedor, o Passaporte ser&aacute; imediatamente cancelado.</p>
                <p>2.5.2.  Para a emiss&atilde;o de novo
                Passaporte, o Torcedor dever&aacute; preencher requisi&ccedil;&atilde;o
                espec&iacute;fica para a emiss&atilde;o de novo Passaporte e, ap&oacute;s,
                efetuar o pagamento de uma taxa correspondente a R$60,00 (sessenta
                reais).</p>
                <p>2.5.3. O pagamento da taxa dever&aacute;
                ocorrer por meio de transfer&ecirc;ncia banc&aacute;ria identificada
                para a conta corrente de n&uacute;mero 08381-1, de titularidade do
                Mineir&atilde;o, no Banco Ita&uacute;, Ag&ecirc;ncia 2001. Ap&oacute;s
                o pagamento, o Mineir&atilde;o dever&aacute; ser imediatamente
                informado para verificar a quita&ccedil;&atilde;o do valor.</p>
                <p>2.5.4. At&eacute; a confec&ccedil;&atilde;o
                de novo Passaporte, e somente ap&oacute;s o pagamento/quita&ccedil;&atilde;o
                da taxa constante do item 2.5.2 acima, o Torcedor poder&aacute;
                retirar ingressos impressos nos termos do item 2.1.3 acima, caso
                pretendam assistir aos Jogos Oficiais.</p>
                <p>2.5.5. Caso o Mineir&atilde;o n&atilde;o
                seja informado da Perda de novo Passaporte, o Mineir&atilde;o n&atilde;o
                ser&aacute; em hip&oacute;tese alguma responsabilizado por eventual
                utiliza&ccedil;&atilde;o do Passaporte por terceiro, sendo vetado o
                acesso do Torcedor, ao Mineir&atilde;o, em dias de Jogos Oficiais sem
                a utiliza&ccedil;&atilde;o do Passaporte, nos termos do item 2.1.3
                acima.  
                </p>
                <p>2.6. Extinto ou suspenso o direito ao
                uso do Passaporte, independentemente do motivo, nos termos deste
                Regulamento, o Torcedor dever&aacute; restituir o Passaporte ao
                Mineir&atilde;o, sob pena de pagamento da taxa constante do item
                2.5.2 acima.</p>
                <p>
                </p>
                <p>3. CADASTRO</p>
                <p>
                </p>
                <p>3.1. Para adquirir o Passaporte, os
                Torcedores dever&atilde;o acessar o s&iacute;tio
                eletr&ocirc;nico&nbsp;estadiomineirao.com.br e efetuar o seu
                cadastro.</p>
                <p>3.2. A compra do Passaporte ocorrer&aacute;
                exclusivamente atrav&eacute;s do s&iacute;tio
                eletr&ocirc;nico&nbsp;estadiomineirao.com.br, mediante cadastro
                pr&eacute;vio e concord&acirc;ncia com o presente termo e
                Regulamento.</p>
                <p>3.3. O cadastro no s&iacute;tio
                eletr&ocirc;nico&nbsp;estadiomineirao.com.br&nbsp;estar&aacute;
                dispon&iacute;vel apenas para os Torcedores que possuem n&uacute;mero
                de CPF v&aacute;lido.</p>
                <p>3.4. O Torcedor poder&aacute; se
                cadastrar uma &uacute;nica vez com seu pr&oacute;prio CPF v&aacute;lido
                (1 cadastro por nome e CPF).</p>
                <p>3.5. Ser&atilde;o confirmados somente
                os cadastros dos Torcedores que preencherem todos os campos
                obrigat&oacute;rios, com informa&ccedil;&otilde;es exatas e
                ver&iacute;dicas.</p>
                <p>3.6. A incorre&ccedil;&atilde;o das
                informa&ccedil;&otilde;es prestadas pelo Torcedor ser&aacute; de sua
                total responsabilidade, respondendo este pela veracidade e
                autenticidade de seus dados cadastrais, assumindo o dever de manter
                atualizadas essas informa&ccedil;&otilde;es no ambiente virtual do
                s&iacute;tio eletr&ocirc;nico.</p>
                <p>3.7. Qualquer cadastro realizado com
                informa&ccedil;&otilde;es err&ocirc;neas e/ou inver&iacute;dicas ser&aacute;
                cancelado, com a consequente perda do hist&oacute;rico de compras de
                jogos e pagamentos, se houver, n&atilde;o sendo devido ao Torcedor
                qualquer tipo de multa, ressarcimento ou indeniza&ccedil;&atilde;o.</p>
                <p>3.8. Para atualiza&ccedil;&atilde;o
                e/ou regulariza&ccedil;&atilde;o do cadastro, o Torcedor dever&aacute;
                entrar em contato por meio do endere&ccedil;o
                eletr&ocirc;nico&nbsp;atendimento@estadiomineirao.com.br, informar os
                dados corretos e solicitar a atualiza&ccedil;&atilde;o.</p>
                <p>3.9. O Torcedor dever&aacute;, no
                momento de seu cadastro, escolher um login (nome de usu&aacute;rio
                sem espa&ccedil;o e/ou caracteres) e uma senha (sem espa&ccedil;o
                e/ou caracteres). Estes dados dever&atilde;o ser utilizados para
                acesso no s&iacute;tio eletr&ocirc;nico&nbsp;estadiomineirao.com.br,&nbsp;atrav&eacute;s
                do qual o Torcedor poder&aacute; comprar os Passaportes.</p>
                <p>3.10. Caso seja identificada fraude no
                cadastro Torcedor, os dados ser&atilde;o enviados aos &oacute;rg&atilde;os
                competentes.</p>
                <p>
                </p>
                <p>4. COMPRA E FORMAS DE PAGAMENTO</p>
                <p>
                </p>
                <p>4.1. O valor do Passaporte para a
                temporada de 2018 ser&aacute; aquele indicado do s&iacute;tio
                eletr&ocirc;nico respons&aacute;vel pela comercializa&ccedil;&atilde;o
                do Mineir&atilde;o Embaixada 2018 quando da respectiva data de compra
                pelo Torcedor, e poder&aacute; ser pago em quantidade de parcelas
                iguais e fixas conforme apontado no s&iacute;tio eletr&ocirc;nico,
                atrav&eacute;s de cart&otilde;es de cr&eacute;dito (Visa ou
                Mastercard). 
                </p>
                <p>4.2. Para a compra do Passaporte n&atilde;o
                ser&atilde;o v&aacute;lidas as condi&ccedil;&otilde;es de meia
                entrada.</p>
                <p>4.3. N&atilde;o &eacute; poss&iacute;vel
                realizar reserva de Passaporte.</p>
                <p>4.4. Para d&uacute;vidas relacionadas
                ao processamento do pagamento, entre em contato com o Plant&atilde;o
                de Atendimento da EasyForPay por telefone ou whatsApp (11)
                97190-8834, ou atrav&eacute;s do e-mail do contato@easyforpay.com.br.
                A EasyForPay &eacute; a empresa de pagamentos dos ingressos do
                Mineir&atilde;o.</p>
                <p>
                </p>
                <p>5. CANCELAMENTO</p>
                <p>
                </p>
                <p>5.1. Uma vez efetivada a compra do
                Passaporte, n&atilde;o ser&atilde;o aceitos quaisquer devolu&ccedil;&otilde;es
                e/ou cancelamentos ao Torcedor.</p>
                <p>5.2. Sem preju&iacute;zo do item 5.1,
                poder&aacute; ser cancelado o Passaporte cujo Torcedor n&atilde;o
                tenha acessado a nenhum Jogo Oficial.</p>
                <p>
                </p>
                <p>6. DISPOSI&Ccedil;&Otilde;ES GERAIS</p>
                <p>
                </p>
                <p>6.1. A compra est&aacute; sujeita &agrave;
                observ&acirc;ncia dos termos e condi&ccedil;&otilde;es aqui
                descritos, com os quais o Torcedor manifesta expressa concord&acirc;ncia
                ao adquirir seu Passaporte.</p>
                <p>6.2. O ingresso do Torcedor no Mineir&atilde;o
                somente ser&aacute; permitido &agrave;queles devidamente possuidores
                dos Passaportes, e demais documentos exigidos pelo Mineir&atilde;o,
                conforme o caso.</p>
                <p>6.3. A aquisi&ccedil;&atilde;o do
                Passaporte n&atilde;o garante acesso irrestrito &agrave;s
                depend&ecirc;ncias do Mineir&atilde;o.</p>
                <p>6.4. Todas as &aacute;reas,
                depend&ecirc;ncias, instala&ccedil;&otilde;es e equipamentos do
                Mineir&atilde;o, bem como atividades desenvolvidas no Mineir&atilde;o,
                sempre estar&atilde;o sujeitos &agrave; disciplina, seguran&ccedil;a
                e administra&ccedil;&atilde;o do Mineir&atilde;o, que as exercer&aacute;
                sem qualquer limita&ccedil;&atilde;o ou restri&ccedil;&atilde;o,
                segundo seu exclusivo crit&eacute;rio ou mediante determina&ccedil;&otilde;es
                das autoridades competentes, seja diretamente ou por pessoas ou
                empresas por ele indicadas ou contratadas para tanto, de modo a
                garantir a ordem e a seguran&ccedil;a do Mineir&atilde;o.</p>
                <p>6.5. Todos os Torcedores que adentrarem
                no Mineir&atilde;o dever&atilde;o consentir com a revista pessoal de
                preven&ccedil;&atilde;o e seguran&ccedil;a, sendo condi&ccedil;&atilde;o
                de acesso e perman&ecirc;ncia do Torcedor no Mineir&atilde;o.</p>
                <p>6.6. Sempre que solicitado, o Torcedor
                dever&aacute; apresentar seu Passaporte, bem como documento de
                identidade v&aacute;lido em todo territ&oacute;rio nacional que
                contenha foto recente.</p>
                <p>6.6.1. Ser&aacute; facultado ao
                Mineir&atilde;o estabelecer, a seu exclusivo crit&eacute;rio, novos
                mecanismos de acesso.</p>
                <p>6.7. O ingresso e a perman&ecirc;ncia
                de incapazes no Mineir&atilde;o, a exemplo de crian&ccedil;as e
                adolescentes, dever&atilde;o ocorrer sempre em conson&acirc;ncia com
                as normas aplic&aacute;veis, e depender&aacute;, nas hip&oacute;teses
                em que a legisla&ccedil;&atilde;o assim determinar, da presen&ccedil;a
                dos pais, do respons&aacute;vel legal ou da exist&ecirc;ncia de
                autoriza&ccedil;&atilde;o judicial pr&oacute;pria, observadas, em
                especial, as regras emitidas pelas Varas de Inf&acirc;ncia e
                Juventude da Comarca de Belo Horizonte e a respectiva classifica&ccedil;&atilde;o
                et&aacute;ria do Jogo Oficial.</p>
                <p>6.8. Os incapazes tamb&eacute;m dever&atilde;o
                portar Passaporte ou ingresso v&aacute;lido, observada, em todo o
                caso, a necessidade de acompanhamento pelo respons&aacute;vel legal,
                o qual tamb&eacute;m dever&aacute; portar Passaporte ou ingresso
                v&aacute;lido.</p>
                <p>6.9. S&atilde;o vedados a entrada, o
                porte e a utiliza&ccedil;&atilde;o no Mineir&atilde;o, inclusive nos
                assentos, de objetos proibidos pelas autoridades competentes, por
                legisla&ccedil;&atilde;o federal, estadual ou municipal ou que
                coloquem em risco os Torcedores ou terceiros presentes no Mineir&atilde;o,
                a qualquer t&iacute;tulo, em especial:</p>
                <p>I. Fogos de artif&iacute;cio, objetos
                explosivos, sinalizadores ou quaisquer outros engenhos pirot&eacute;cnicos
                ou produtores de efeitos an&aacute;logos.</p>
                <p>II. Subst&acirc;ncias il&iacute;citas,
                de qualquer natureza e entorpecentes.</p>
                <p>III. Objetos, bebidas ou subst&acirc;ncias
                proibidas ou suscet&iacute;veis de gerar ou possibilitar a pr&aacute;tica
                de atos de viol&ecirc;ncia, em especial:</p>
                <p>a) Armas de qualquer tipo e qualquer
                objeto que possa ser usada como arma e outros objetos que sejam
                proibidos pela pol&iacute;tica de seguran&ccedil;a do Mineir&atilde;o.</p>
                <p>b) Instrumentos que produzam volume
                excessivo de barulho, tais como: megafones, sirenes ou&nbsp; buzinas
                a g&aacute;s, inclusive instrumentos de sopro.</p>
                <p>6.10. Ressalvados os alimentos e
                bebidas comercializados no Mineir&atilde;o e autorizados por si, s&atilde;o
                vedados o acesso e a perman&ecirc;ncia nos assentos com bebida,
                l&iacute;quidos ou alimentos de qualquer tipo.</p>
                <p>6.11. Em conson&acirc;ncia com o
                disposto no item anterior, &eacute; especialmente vedado o porte, o
                acesso, a venda ou o consumo de bebidas alco&oacute;licas durante a
                realiza&ccedil;&atilde;o de Jogos Oficiais. A proibi&ccedil;&atilde;o
                de que trata este item poder&aacute;, a exclusivo crit&eacute;rio do
                Mineir&atilde;o, ser flexibilizada na hip&oacute;tese de altera&ccedil;&atilde;o
                das normas ou determina&ccedil;&otilde;es das autoridades
                competentes, a respeito do consumo de bebida alco&oacute;lica no
                Mineir&atilde;o.</p>
                <p>6.12. O porte de bandeiras no Mineir&atilde;o
                e nos assentos dever&aacute; observar as regras de seguran&ccedil;a
                estabelecidas pelo Mineir&atilde;o e as entidades competentes, em
                especial a Pol&iacute;cia Militar do Estado de Minas Gerais, bem como
                os acordos de patroc&iacute;nio eventualmente firmados pelo Mineir&atilde;o
                com terceiros.</p>
                <p>6.13. O consumo de fum&iacute;genos de
                qualquer esp&eacute;cie, tais como cigarros, charutos e outros, n&atilde;o
                ser&aacute; permitido no Mineir&atilde;o.</p>
                <p>6.14. O Torcedor dever&aacute; guardar
                e se responsabilizar pelos seus bens levados ao Mineir&atilde;o, n&atilde;o
                podendo ser atribu&iacute;da ao Mineir&atilde;o qualquer
                responsabilidade pela guarda, ou por casos de quebra ou extravio de
                bens pessoais do Torcedor, seja durante a realiza&ccedil;&atilde;o de
                Jogos Oficiais, ou mesmo antes ou ap&oacute;s tais jogos.</p>
                <p>6.15. &nbsp;O Torcedor que adquirir um
                Passaporte se compromete, desde j&aacute;, a assumir uma conduta
                respeitosa e &eacute;tica, em rela&ccedil;&atilde;o aos demais
                torcedores, funcion&aacute;rios, atletas, membros da comiss&atilde;o
                t&eacute;cnica e dirigentes dos Clubes de Futebol, abstendo-se de
                praticar quaisquer atos que venham a prejudicar a imagem do Mineir&atilde;o,
                a qualquer momento, dentro e fora do &acirc;mbito da arena, sob pena
                de sofrer as san&ccedil;&otilde;es de suspens&atilde;o tempor&aacute;ria
                ou exclus&atilde;o e demais san&ccedil;&otilde;es permitidas em Lei.</p>
                <p>6.16. As seguintes condutas s&atilde;o
                expressamente proibidas ao Torcedor, sem preju&iacute;zo de outras
                proibi&ccedil;&otilde;es de legisla&ccedil;&atilde;o ou deste
                Regulamento:</p>
                <p>I. &nbsp;Arremessar objeto, de qualquer
                natureza, no interior do Mineir&atilde;o, em especial das cadeiras e
                em dire&ccedil;&atilde;o a outra pessoa ou local, tal como nas
                cadeiras inferiores e no campo.</p>
                <p>II. Subir ou apoiar-se em estruturas e
                instala&ccedil;&otilde;es n&atilde;o planejadas para tal uso, em
                especial, mas n&atilde;o se restringindo a fachadas, cercas, muros,
                grades, alambrados, cadeiras, postes de luz, barreiras ou
                guarda-corpo.</p>
                <p>III. Praticar conduta que possa
                atrapalhar o aproveitamento do Jogo Oficial por outros espectadores,
                ou dar causa &agrave; sua interrup&ccedil;&atilde;o ou n&atilde;o
                realiza&ccedil;&atilde;o.</p>
                <p>IV. Incitar ou praticar atos de
                viol&ecirc;ncia f&iacute;sica ou moral no Mineir&atilde;o.</p>
                <p>V. Criar qualquer amea&ccedil;a para a
                vida ou para a seguran&ccedil;a de si pr&oacute;prio ou de terceiros.</p>
                <p>VI. Entoar c&acirc;nticos, portar ou
                ostentar mensagens, cartazes, bandeiras, s&iacute;mbolos ou outros
                sinais com mensagens de que sejam ofensivas, inclusive de car&aacute;ter
                discriminat&oacute;rio, racista ou xen&oacute;fobo, ou que de
                qualquer modo incitem a viol&ecirc;ncia e a desordem.</p>
                <p>VII. Escrever, pintar ou afixar
                qualquer coisa nos elementos estruturais, instala&ccedil;&otilde;es
                ou passagens do Mineir&atilde;o, bem como realizar qualquer conduta
                que possa provocar danos &agrave;s estruturas e instala&ccedil;&otilde;es
                do Mineir&atilde;o.</p>
                <p>VIII. Invadir ou incitar a invas&atilde;o,
                de qualquer forma, da &aacute;rea restrita aos competidores, &agrave;
                equipe t&eacute;cnica ou operacional do Mineir&atilde;o ou de &aacute;reas
                fechadas ao p&uacute;blico em geral.</p>
                <p>IX. Restringir ou bloquear passagens,
                entradas e sa&iacute;das de pessoas ou ve&iacute;culos no Mineir&atilde;o.</p>
                <p>X. Ficar de p&eacute; nos assentos, em
                especial nos assentos, nas &aacute;reas de espectadores ou obstruir a
                vis&atilde;o de outros espectadores de forma n&atilde;o razo&aacute;vel.</p>
                <p>XI. Entrar ou permanecer em traje de
                banho ou sem camisa nas depend&ecirc;ncias do Mineir&atilde;o,
                inclusive nas dos assentos.</p>
                <p>XII. Entrar com animais no Mineir&atilde;o
                e, por consequ&ecirc;ncia acessar os assentos, &agrave; exce&ccedil;&atilde;o
                de c&atilde;es guia que auxiliem deficientes visuais, nos termos e
                condi&ccedil;&otilde;es da Lei Federal n&ordm; 11.126, de 27 de junho
                de 2005, e do Decreto n&ordm; 5.904, de 27 de setembro de 2006, ou
                outra norma que as complemente ou substitua.</p>
                <p>6.17. O desrespeito &agrave;s regras
                previstas neste Regulamento, e sem preju&iacute;zo de eventuais
                perdas e danos a serem arcados pelo Torcedor:</p>
                <p>I. Ensejar&aacute; o recolhimento
                imediato, pelo Mineir&atilde;o, de todos os materiais irregulares
                eventualmente identificados, sem que seja devida a devolu&ccedil;&atilde;o
                ou a indeniza&ccedil;&atilde;o em rela&ccedil;&atilde;o aos materiais
                recolhidos, sem preju&iacute;zo, da notifica&ccedil;&atilde;o das
                autoridades p&uacute;blicas competentes quando cab&iacute;vel.</p>
                <p>II. Implicar&aacute;, quando necess&aacute;rio
                e a crit&eacute;rio do Mineir&atilde;o, a impossibilidade de ingresso
                tempor&aacute;rio ou definitivo do Torcedor no Mineir&atilde;o, ou,
                se for o caso, o seu afastamento imediato do Mineir&atilde;o, sem
                preju&iacute;zo de outras san&ccedil;&otilde;es contratuais,
                administrativas, civis ou penais eventualmente aplic&aacute;veis.</p>
                <p>III. Ao imediato pagamento, pelo
                Torcedor, ao Mineir&atilde;o, de valor correspondente ao conserto do
                dano praticado, caso acarretado por culpa do Torcedor.</p>
                <p>6.18. Este Regulamento n&atilde;o gera
                nenhum contrato de sociedade, mandato, franquia, intermedia&ccedil;&atilde;o,
                ag&ecirc;ncia ou rela&ccedil;&atilde;o de trabalho entre o Mineir&atilde;o
                e o Torcedor.</p>
                <p>6.19. Este Regulamento ser&aacute;
                regido pela legisla&ccedil;&atilde;o brasileira, notadamente pelo
                C&oacute;digo Civil, C&oacute;digo de Defesa do Consumidor e Estatuto
                do Torcedor.</p>
                <p>6.20. As regras deste Regulamento s&atilde;o
                v&aacute;lidas apenas para os Jogos Oficiais realizados no ano de
                2018. 
                </p>
                <p>
                </p>
                <p>7. CONTATO</p>
                <p>
                </p>
                <p>7.1. Todas as comunica&ccedil;&otilde;es
                do Mineir&atilde;o ao Torcedor ocorrer&atilde;o por meio de
                correspond&ecirc;ncia eletr&ocirc;nica, o que se torna importante a
                atualiza&ccedil;&atilde;o imediata do cadastro do Torcedor sempre que
                houver alguma altera&ccedil;&atilde;o no endere&ccedil;o eletr&ocirc;nico
                apresentado quando do cadastro do Torcedor. 
                </p>
                <p>7.2. Endere&ccedil;o do Mineir&atilde;o:
                Avenida Ant&ocirc;nio Abrah&atilde;o Caram, n&ordm; 1.001, Bairro S&atilde;o
                Jos&eacute;, CEP 31.275-000, Belo Horizonte, MG. Hor&aacute;rio de
                atendimento: das 09h00min &agrave;s 18h00min em dias &uacute;teis
                comerciais. Telefone: (31) 3499-4333.
                E-mail: <a href="mailto:atendimento@estadiomineirao.com.br">atendimento@estadiomineirao.com.br</a> 
                </p>
                			
                			

                                    </div>
                            </div>
                	</div>
                    <?php Modal::end(); ?>
					
					
					
					<!-- Modal - Politica -->
                    <?php Modal::begin([
                        'header' => '<h4 align="center">POLÍTICA DE PRIVACIDADE</h4>',
                        'size'   => 'modal-lg',
                        'id'     => 'modal-politica',
                        'options'=> [
                            'class' => 'modal-center'
                        ]
                    ]); ?>    
                	<div class="row">
						<div class="col-md-12">
							<div class="scrollTermos">
								
								
								
								
								<h2>POLÍTICA DE PRIVACIDADE&nbsp;</h2>
								<p>A EasyPay sabe o quanto sua seguran&ccedil;a e sua privacidade s&atilde;o importantes e se preocupa muito com isso. Para garantir a sua tranquilidade, a EasyPay divulga uma s&eacute;rie de normas que todo usu&aacute;rio deve seguir e os compromissos que a EasyPay estabelece para garantir sua privacidade.</p>
								<p>Estas normas poder&atilde;o ser alteradas a qualquer momento pela EasyPay, que se compromete a disponibilizar as altera&ccedil;&otilde;es aos seus clientes com anteced&ecirc;ncia.</p>
								<p><strong>Quais s&atilde;o os compromissos que a Easypay estabelece com o usu&aacute;rio, para garantir sua seguran&ccedil;a e privacidade?</strong></p>
								<ul style="font-size: 0.875rem;">
								<li>N&atilde;o divulgar a terceiros, sem pr&eacute;via autoriza&ccedil;&atilde;o, em hip&oacute;tese alguma, o e-mail do internauta que se cadastrar em p&aacute;ginas que requerem preenchimento de dados pessoais;</li>
								<li>N&atilde;o enviar mensagens ao correio eletr&ocirc;nico do internauta se esta manifestar seu desejo de n&atilde;o as receber, salvo as de confirma&ccedil;&atilde;o de compras que s&atilde;o &uacute;teis para o combate a fraudes;</li>
								<li>Possibilitar ao internauta, a qualquer momento, a suspen&ccedil;&atilde;o envio de material informativo ou publicit&aacute;rio que n&atilde;o seja relativo &agrave; presta&ccedil;&atilde;o do servi&ccedil;o por ele contratado;</li>
								<li>Preservar a identidade do internauta, mantendo sigilo enquanto o mesmo navega nas p&aacute;ginas da Easypay ou via App de celular da EasyPay;</li>
								<li>Manter em absoluto sigilo o n&uacute;mero do cart&atilde;o de cr&eacute;dito do internauta, respeitando as recomenda&ccedil;&otilde;es PCI (Payment Card Industry);</li>
								<li>Utilizar cookies, beacons e outros identificadores somente para controle de audi&ecirc;ncia, de navega&ccedil;&atilde;o, do uso de produtos e servi&ccedil;os, de seguran&ccedil;a e de publicidade. Cookies s&atilde;o informa&ccedil;&otilde;es enviadas pelo servidor da Easypay ao computador do usu&aacute;rio, para identific&aacute;-lo. Voc&ecirc; n&atilde;o precisa aceitar cookies para navegar na Easypay, mas alguns de nossos servi&ccedil;os podem n&atilde;o funcionar corretamente se os cookies forem desabilitados;</li>
								<li>Coletar informa&ccedil;&otilde;es de navega&ccedil;&atilde;o do usu&aacute;rio, tais como: informa&ccedil;&otilde;es de visita ao site, intera&ccedil;&atilde;o com o conte&uacute;do etc., atrav&eacute;s de sistemas pr&oacute;prios ou atrav&eacute;s de terceiros autorizados pela Easypay, respeitadas as quest&otilde;es de seguran&ccedil;a e privacidade de dados pessoais do usu&aacute;rio. Em hip&oacute;tese alguma, ser&atilde;o coletados ou transmitidos dados pessoais de usu&aacute;rios por terceiros sem conhecimento do usu&aacute;rio, salvo para quest&otilde;es operacionais e de seguran&ccedil;a e controle de fraude, hip&oacute;tese em que os dados ser&atilde;o utilizados livremente para garantir a regularidade, fluidez e efic&aacute;cia das transa&ccedil;&otilde;es;</li>
								<li>Garantir seguran&ccedil;a e privacidade de identidade aos internautas que fazem compras na Easypay. Dados cadastrados - como nome, endere&ccedil;o e n&uacute;mero de cart&atilde;o de cr&eacute;dito - s&atilde;o protegidos por sistemas avan&ccedil;ados de criptografia enquanto s&atilde;o enviados e mantidos em sigilo em servidores de seguran&ccedil;a da Easypay ou em outros autorizados, sob as mesmas condi&ccedil;&otilde;es, pela Easypay.</li>
								<li>Realizar parcerias com empresas id&ocirc;neas para a utiliza&ccedil;&atilde;o de dados de clientes com o fim de direcionamento de mensagens promocionais, respeitando sempre o direito de o cliente n&atilde;o receber este tipo de mensagem caso manifeste este desejo.</li>
								</ul>
								<p><strong>&nbsp;</strong></p>
								<p><strong>Atividades que n&atilde;o ser&atilde;o toleradas e gerar&atilde;o a proibi&ccedil;&atilde;o de o cliente operar utilizando os meios de pagamento EasyPay, com os quais o cliente concorda ao aderir a esta pol&iacute;tica:</strong></p>
								<ul style="font-size: 0.875rem;">
								<li>Forjar ou fornecer com inexatid&atilde;o quaisquer dados cadastrais, endere&ccedil;os de m&aacute;quinas, de rede ou de correio eletr&ocirc;nico, na tentativa de responsabilizar terceiros ou ocultar a identidade ou autoria de transa&ccedil;&otilde;es financeiras e outras opera&ccedil;&otilde;es;</li>
								<li>Violar a privacidade de outros usu&aacute;rios;</li>
								<li>Destruir ou corromper dados e informa&ccedil;&otilde;es de outros usu&aacute;rios;</li>
								<li>Distribuir, via correio eletr&ocirc;nico, grupos de discuss&atilde;o, f&oacute;runs e formas similares de comunica&ccedil;&atilde;o mensagens n&atilde;o solicitadas do tipo 'corrente', publicidade e mensagens em massa, comerciais ou n&atilde;o;</li>
								<li>Utilizar nossos sistemas e ferramentas com prop&oacute;sitos ilegais;</li>
								<li>Propagar v&iacute;rus de computador, programas invasivos (worms) ou outras formas de programas de computador, auto-replicantes ou n&atilde;o, que causem danos permanentes ou tempor&aacute;rios nos equipamentos do destinat&aacute;rio;</li>
								<li>Transmitir tipos ou quantidades de dados que causem falhas em servi&ccedil;os ou equipamentos na rede da Easypay ou na Internet;</li>
								<li>Usar a rede para tentar e/ou realizar acesso n&atilde;o autorizado a dispositivos de comunica&ccedil;&atilde;o, informa&ccedil;&atilde;o ou computa&ccedil;&atilde;o;</li>
								</ul>
								<p>&nbsp;</p>
								<p><strong>O que acontece a quem desobedece a uma dessas normas?</strong></p>
								<p>Pode receber advert&ecirc;ncias ou, at&eacute; mesmo, ter o contrato de assinatura encerrado de imediato, dependendo &agrave; gravidade da infra&ccedil;&atilde;o que ser&aacute; avaliada discricionariamente pela EasyPay. Se houver a caracteriza&ccedil;&atilde;o de atividade criminosa ou suspeita fundada sobre o caso, a conta do usu&aacute;rio ser&aacute; imediatamente cancelada, os dados preservados e disponibilizados &agrave;s autoridades mediante ordem judicial.</p>
								<p>Lembre-se, a EasyPay jamais solicita informa&ccedil;&otilde;es relativas a pagamentos via e-mail, sendo toda a nossa comunica&ccedil;&atilde;o ativa feita via site ou liga&ccedil;&otilde;es de ou para nossa central de atendimento, que confirmar&aacute; seus dados antes de prestar qualquer tipo de atendimento.</p>
								
								
								
								

							</div>
						</div>
                	</div>
                    <?php Modal::end(); ?>
					
					
					
					
                </div>  
            </div>            
        </div>
    </div>           
</div>

<?php
if($session['user_token'] && $cliente->user_id) 
{
    $this->registerJs("
        var customer = '".$cliente->user_id."'
        kondutoCustomer(customer);
    ", \yii\web\View::POS_END);    
}

$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/carrinho.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/cadastro.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset', 'yii\bootstrap\BootstrapPluginAsset']]);
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    $this->registerJs("deadline = setDataExpira('" . $session['tempoCarrinho'] . "'); initializeClock();");
}
?>
<?php
namespace app\models;

use app\models\Ws;

class WsCompra extends Ws
{
    /**
    * Consultar vendas com filtros
    * @param $post Filtros para busca: data_ini, data_fim, descricao, operador, total, parcelas, trid, comprador, codigo
    * @param $token V - loja
    * @return type
    */
    public function consultarCompraFiltro($post=[])
    {
        return json_decode($this->call('compras/', 'POST', $post, true, 'U'));
    }

}
<?php
use yii\helpers\Url;
use app\modules\admin\assets\UsuariosAsset;
UsuariosAsset::register($this);
?>
<div class="container row">
    <div class="col-md-12">		
        <div>
            <h2 class="tituloGeral"><i class="fa fa-clone"></i> Ficha de usuário</h2>
            <section>
                <?php
                foreach ($model->attributes as $key => $value) {
                    if($value!='')
                        echo '<p><strong>'.$key.':</strong> '.$value.'<br />';
                }
                ?>
            </section>
            <a class="btn btn-default" href="javascript:history.back()">Voltar</a>
            <a class="btn btn-primary" href="<?=Url::to(['atualizar','codigo'=>$model->codigo])?>">Alterar dados</a>
            <button class="btn btn-warning" id="excluirOperador">Excluir operador</button>
        </div>
    </div>
</div>
<?php
yii\bootstrap\Modal::begin([
   'size'   => 'modal-md',
   'id'     => 'modal-confirmar',
   'options'=> [
       'class' => 'modal-center'
   ],
   'clientOptions' => [
       'keyboard' => false,
       'backdrop' => 'static',
   ],
   'footer' => '
       <div class="col-md-12" align="center">
           <button type="button" class="btn btn-danger" id="btn-cancel">Cancelar</button>
           <button type="button" class="btn btn-success" id="btn-continue">Continuar</button>
       </div>'
]);
yii\bootstrap\Modal::end();

$this->registerJs('
	$("#excluirOperador").click(function(){
		$("#alertExcluir").remove();
		$("#modal-confirmar .modal-header").html("<h3 align=\"center\">Excluir operador</h3>");
		$("#modal-confirmar .modal-body").html("Tem certeza que deseja excluir este usuário ('.$model->email.')? Esta ação não pode ser desfeita.");
		$("#modal-confirmar").show();
		$("#btn-cancel").click(function(){$("#modal-confirmar").hide();})
		$("#btn-continue").click(function(){
			$(this).button("loading");
			doAjax("'.Url::to(['excluir']).'", "POST", {"PermissoesModelForm[email]":"'.$model->email.'"}, "JSON", 5000, "ajaxExcluirModalOk", "ativarExcluirPermissaoErro"
			);
		})
	})

	function ajaxExcluirModalOk(){
		$("#btn-continue").button("reset");
		$("#modal-confirmar").hide();
		$("#modal-mensagem .modal-body").html("<p>Operador removido</p>");
		$("#modal-mensagem .modal-body").append("<p class=\"text-center\"><button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal-mensagem\" aria-hidden=\"true\">Fechar</button></p>");
		$("#modal-mensagem").show();
		$("#modal-mensagem .modal-body .btn, #modal-mensagem .modal-header .close").on("click",function(){window.location.assign("'.Url::to(['lista-operadores']).'")});
	}

	function ativarExcluirPermissaoErro(data){
		$("#btn-continue").button("reset");
		$("#modal-confirmar").hide();
		$("#excluirOperador").after("<div id=\"alertExcluir\" class=\"alert alert-danger\"><p><strong>Erro ao tentar remover o operador</strong></p><p>Informações do erro:<br />"+data.erro.mensagem+"</p></div>");
	}
',\yii\web\View::POS_END);
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Produto - Estoque';
?>
<h1><span style="font-weight:200;">Produto</span> - Estoque</h1>
<h2><?=$produto->nome?></h2>
<p><?=$produto->descricao?></p>
<p>oferecido: <?=$produto->oferecido?><br />
vendido: <?=$produto->vendido?><br />
reservado: <?=$produto->reservado?><br />
disponivel: <?=$produto->disponivel?></p>

<?php
$form = ActiveForm::begin(['id' => 'produto-form']); ?>
<?= $form->field($produto, 'tipoMovimentoEstoque')->radioList(['adicionar'=>'Adicionar', 'remover'=>'Remover']) ?>
<?= $form->field($produto, 'unidadesEstoque', ['inputOptions'=>['class'=>'form-control', 'type'=>'number']]) ?>
<div class="form-group">
    <?= Html::submitButton('Salvar', ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>
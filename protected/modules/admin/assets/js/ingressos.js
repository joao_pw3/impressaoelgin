var linhaAssociacaoAtual;
var urlAssociacaoIngresso;
var urlInvalidarIngresso;
var urlStatusIngresso;
var qrcode;
var tabelaOrigem;
var idCampoAutoFocoModal;

$(document).ready(function () {
    $('#modal-mensagem-associar-ingresso').on('shown.bs.modal', function () {
        $('#'+idCampoAutoFocoModal).focus();
        listenEnterKey();
    })

    $('#modal-mensagem-associar-ingresso').on('hidden.bs.modal', function () {
        stopListenEnterKey();
        $('#form-associacao-ingressos-associar-btn').button('reset');
        $('.table-ingressos-associacao tr').removeClass('linhaAssociacaoAtual');
        $("#associar-ingresso-msg").removeClass('alert alert-warning');
        $('input[id="associar-ingresso-busca-qrcode"]').focus();
        prepararModalAssociarIngresso();
    });
    
    $('#form-associacao-ingressos-associar-btn').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        
        var campoQrcode = $('#form-associacao-ingressos-qrcode');
        var campoCodigo = $('#form-associacao-ingressos-codigo');
        var campoIdAgenda = $('#form-associacao-ingressos-idagenda');
        
        $('#associar-ingresso-msg').removeClass('associarIngressoInfoErro').html('');
        var formValido = true;
        if (campoQrcode.val() == '')
            formValido = msgErroAssociacao('Informe o QR Code');
        else if (campoQrcode.val() != qrcode)
            formValido = msgErroAssociacao('O QR Code lido não é igual ao gravado em base. Verifique os dados.');
        if (campoCodigo.val() == '')
            formValido = msgErroAssociacao('Informe o RFID');

        if (formValido)
            associarIngresso(campoQrcode.val(), campoCodigo.val(), campoIdAgenda.val());
    });
    
    $(document).on('click', '#form-associacao-ingressos-invalidar-btn', function () {
        stopListenEnterKey();
        $('#holderFormAssociar').hide();
        $('#holderFormInvalidar').show();
    });
    
    $(document).on('click', '#form-invalidar-ingressos-voltar-btn', function () {
        listenEnterKey();
        prepararModalAssociarIngresso();
    });

    $("#form-invalidar-ingressos").submit(function(e){e.preventDefault();e.stopPropagation()})

    $('#form-invalidar-ingressos-invalidar-btn').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var campoQrcode = $("#form-invalidar-ingressos-qrcode");
        var campoMotivo = $("#form-invalidar-ingressos-motivo");
        var campoIdAgenda = $("#form-invalidar-ingressos-idagenda");
        $("#invalidar-ingresso-msg").removeClass('invalidarIngressoInfoErro').html('');

        var formValido = true;
        if (campoQrcode.val() == '')
            formValido = msgErroInvalidar("Informe o QR Code");
        else if (campoQrcode.val() != qrcode)
            formValido = msgErroInvalidar("O QR Code lido não é igual ao gravado em base. Verifique os dados.");
        if (campoMotivo.val() == '')
            formValido = msgErroInvalidar("Informe o Motivo");

        if (formValido)
            invalidarIngresso(campoQrcode.val(), campoMotivo.val(), campoIdAgenda.val());
    });
    
    $("#associar-ingresso-busca-qrcode").on('keyup',function(e){
        if($("#associar-ingresso-busca-qrcode").val()!='' && (e.key == 'Enter' || e.keyCode==13))
            buscarQrCode($("#associar-ingresso-busca-qrcode").val());
    });
    $("#associar-ingresso-busca-qrcode-btn").on('click',function(){
        if($("#associar-ingresso-busca-qrcode").val()!='')
            buscarQrCode($("#associar-ingresso-busca-qrcode").val());
    });
    $("#associar-ingresso-busca-qrcode").on('focus',function(){
        $(this).val('');
    });
});

function clickLinhaTabelaAssociarIngresso(tds) {
    linhaAssociacaoAtual = $(tds).parent();
    tabelaOrigem=linhaAssociacaoAtual.parent().parent().attr('id');
    var valores = valoresLinhaAssociacaoAtual();
    resetModalAssociacao();
    resetFormInvalidarIngresso();
    modalAssociacaoIngresso(valores.qrcode, valores.produto, valores.ocupante, valores.comprador);
}

function valoresLinhaAssociacaoAtual() {
    var tds = $(linhaAssociacaoAtual).find('td');
    return {
        qrcode: tds[0].innerText,
        produto: tds[1].innerText,
        ocupante: tds[2].innerText,
        comprador: tds[3].innerText
    };
}

function modalAssociacaoIngresso(qrcodeFunc, produto, ocupante, comprador) {
    $(linhaAssociacaoAtual).addClass('linhaAssociacaoAtual');
    $("#associar-ingresso-produto").html(produto);
    $("#associar-ingresso-comprador").html(comprador);
    qrcode = qrcodeFunc;
    switch(tabelaOrigem){
        case 'associar-ingressos':
            $("#associar-ingresso-header-modal").html('Associar Ingresso');
        break;
        case 'lista-ingressos-associados':
            $("#associar-ingresso-header-modal").html('<strong>INGRESSO ASSOCIADO</strong>');
            $("#associar-ingresso-msg").html('Este ingresso JÁ ESTÁ ASSOCIADO. Você pode invalida-lo ou mudar o RFID preenchendo os campos novamente.').addClass('alert alert-warning');
        break;
    }
    $("#modal-mensagem-associar-ingresso").modal('toggle');
}

function prepararModalAssociarIngresso() {
    $("#holderFormInvalidar").hide();
    $("#holderFormAssociar").show();
}

function msgErroAssociacao(msg) {
    var pMsg = $("#associar-ingresso-msg");
    if (!pMsg.hasClass('associarIngressoInfoErro')) {
        pMsg.append('<hr >Ocorreram erros na associação:<br /><br />')
        pMsg.addClass('associarIngressoInfoErro');
    }
    pMsg.append('&bull; ' + msg + '<br />');
    return false;
}

function associarIngresso(qrcode, rfid, id_agenda) {
    if (qrcode == '' || rfid == '' || id_agenda == '')
        return false;
    $('#form-associacao-ingressos-associar-btn').button('loading');
    doAjax(urlAssociacaoIngresso, 'POST', {'qrcode': qrcode, 'rfid': rfid, 'id_agenda': id_agenda}, 'JSON', 5000, 'associarIngressoOk', 'associarIngressoErro');
}

function associarIngressoOk(data) {
    $("#associar-ingresso-msg").removeClass('associarIngressoInfoErro').addClass('associarIngressoInfoSucesso').html('Ingresso associado');
    $(linhaAssociacaoAtual).removeClass('linhaAssociacaoAtual').addClass("associado");
    $("#form-associacao-ingressos-associar-btn,#form-associacao-ingressos-invalidar-btn").addClass('hidden');
    if(tabelaOrigem=='associar-ingressos')
        $("#lista-ingressos-associados tbody").append(linhaAssociacaoAtual);
    setTimeout(function(){$("#modal-mensagem-associar-ingresso").modal('hide');},1500);
}

function resetModalAssociacao() {
    $("#form-associacao-ingressos-associar-btn,#form-associacao-ingressos-invalidar-btn").removeClass('hidden')
    $("#form-associacao-ingressos-proximo-btn").addClass('hidden');
    $('.table-ingressos-associacao tr').removeClass('linhaAssociacaoAtual');
    $("#associar-ingresso-produto").html('');
    $("#associar-ingresso-comprador").html('');
    $("#form-associacao-ingressos-codigo").val('');
    $("#form-associacao-ingressos-qrcode").val('');
    $("#associar-ingresso-msg").removeClass('associarIngressoInfoErro').removeClass('associarIngressoInfoSucesso').html('');
    $("#modal-mensagem-associar-ingresso").modal('hide');
}

function associarIngressoErro(data) {
    $('#form-associacao-ingressos-associar-btn').button('reset');
    $("#associar-ingresso-msg").removeClass('associarIngressoInfoErro').html('');
    msgErroAssociacao(data.erro.mensagem);
}

function resetFormInvalidarIngresso() {
    $("#form-invalidar-ingressos-qrcode").val('');
    $("#form-invalidar-ingressos-motivo").val('');
    $("#invalidar-ingresso-msg").removeClass('invalidarIngressoInfoErro').html('');
    $("#form-invalidar-ingressos-voltar-btn").off();
    $("#form-invalidar-ingressos :submit").off();
    $("#form-invalidar-ingressos-msg-confirmacao").removeClass('hidden');
    $("#form-invalidar-ingressos-invalidar-btn").removeClass('hidden');
    $("#form-invalidar-ingressos-voltar-btn").removeClass('hidden');
}

function msgErroInvalidar(msg) {
    var pMsg = $("#invalidar-ingresso-msg");
    if (!pMsg.hasClass('invalidarIngressoInfoErro')) {
        pMsg.append('<hr >Ocorreram erros ao invalidar:<br /><br />')
        pMsg.addClass('invalidarIngressoInfoErro');
    }
    pMsg.append('&bull; ' + msg + '<br />');
    return false;
}

function invalidarIngresso(qrcode, motivo, id_agenda) {
    if (qrcode == '' || motivo == '' || id_agenda == '')
        return false;
    $("#form-invalidar-ingressos-invalidar-btn").button('loading')
    doAjax(urlInvalidarIngresso, 'POST', {'qrcode': qrcode, 'motivo': motivo, 'id_agenda': id_agenda}, 'JSON', 5000, 'invalidarIngressoOk', 'invalidarIngressoErro');
}

function invalidarIngressoOk(data) {
    $("#invalidar-ingresso-msg").removeClass('invalidarIngressoInfoErro').addClass('invalidarIngressoInfoSucesso').html('Ingresso invalidado');
    $(linhaAssociacaoAtual).remove();
    $("#form-invalidar-ingressos-msg-confirmacao").addClass('hidden');
    $("#form-invalidar-ingressos-invalidar-btn").addClass('hidden');
    $("#form-invalidar-ingressos-voltar-btn").addClass('hidden');
    setTimeout(function(){$("#modal-mensagem-associar-ingresso").modal('hide');},1500);
}

function invalidarIngressoErro(data) {
    $("#invalidar-ingresso-msg").removeClass('invalidarIngressoInfoErro').html('');
    $("#form-invalidar-ingressos-invalidar-btn").button('reset');
    msgErroInvalidar(data.erro.mensagem);
}

function buscarQrCode(qrcode){
    var busca=$(".qrcode-table:contains('"+qrcode+"')");
    if(busca.length==1){
        idCampoAutoFocoModal="form-associacao-ingressos-codigo";
        clickLinhaTabelaAssociarIngresso($(busca).parent().find("td"));
        $("#form-associacao-ingressos-qrcode").val(qrcode);
    }
    if(busca.length>1){
        showMsg("Parece que há mais de um ingresso com este código na tabela. Verifique");
        $(busca).addClass("alert alert-danger");
    }
    if(busca.length<=0)
        showMsg("O ingresso com este código não está nessa lista. <a target=\"_blank\" href=\""+urlStatusIngresso+"\">Consulte o status aqui</a> ou passe para o próximo.");
}

function listenEnterKey(){
    $("#form-associacao-ingressos").on('keyup',function(e){
        if(e.key == 'Enter' || e.keyCode==13)
            $('#form-associacao-ingressos-associar-btn').trigger('click');
    });
}

function stopListenEnterKey(){
    $("#form-associacao-ingressos").off('keyup');
}
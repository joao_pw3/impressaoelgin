$(document).ready(function () {
    
    $('#testes-mineirao').click(function () {
        doAjax ($('#url-check-cartao').val(), 'POST', $('#form-ativar-ingresso').serialize(), 'JSON', 10000, 'checkVendaOK', 'checkVendaErr');
    });   
    
    $('#cancelar-qrcode').click(function () {
        location.reload();
    });    

    $(".botao-registro").on('click',function() {
        var vlrAtual = parseFloat(document.getElementById("valores").value.replace(",", "."));
        $('#numero_cartao').focus();
        var vlrData = parseFloat($(this).attr('data-preco'));

        var vlrTotal = vlrAtual+vlrData;
        document.getElementById("valores").value = vlrTotal;       
    });

    $("#limpar-valores").on('click', function(){
        $('#numero_cartao').focus();
        document.getElementById("valores").value = 0;
    });

    $(".botao-tipo-pagamento").on('click',function(){
        $('#numero_cartao').focus();
        var vlrData = $(this).attr('data-tipo-pagamento');
        console.log(vlrData);
    });
    
    $('#btn-operacao').click(function () {
        if ($('#caixa-valor').val() == '' || $('#caixa-valor').val() == 0 || isNaN($('#caixa-valor').val())) {
            showMsg('O valor deve ser um número maior que zero.');
            $('#caixa-valor').focus();
            return false;
        }
        $('#ajuste').val('0');
        acaoCaixa();
    });
    
    $('#btn-sangria').click(function () {
        if (typeof $('input[type=radio]:checked').val() == 'undefined') {
            showMsg('Selecione a ação a ser efetuada: sangria ou reposição.');
            return false;
        }
        if ($('#caixa-sangria').val() == '' || $('#caixa-sangria').val() == 0 || isNaN($('#caixa-sangria').val())) {
            showMsg('O valor deve ser um número maior que zero.');
            $('#caixa-sangria').focus();
            return false;
        }
        $('#ajuste').val('1');
        $('#saida').val($('input[type=radio]:checked').val());
        acaoSangria(); 
    });
    
});

function acaoCaixa () {
    doAjax ($('#url-acao').val(), 'POST', $('#form-caixa').serialize(), 'JSON', 10000, 'acaoCaixaOK', 'acaoCaixaErr');
    return false;
}

function acaoCaixaOK (data) {
    showMsg('Ação efetuada com sucesso.');
    $('#modal-mensagem').on('hidden.bs.modal', function (e) {
        window.location.href = $('#url-operacao').val();
    });
    return false;
}

function acaoCaixaErr (data) {
    showMsg('Falha ao executar ação. Erro: ' + data.erro.mensagem);
    return false;
}

function acaoSangria () {
    doAjax ($('#url-acao').val(), 'POST', $('#form-caixa').serialize(), 'JSON', 10000, 'sangriaOK', 'sangriaErr');
    return false;
}

function sangriaOK (data) {
    showMsg('Ação efetuada com sucesso.');
    $('#modal-mensagem').on('hidden.bs.modal', function (e) {
        window.location.href = $('#url-operacao').val();
    });
    return false;
}

function sangriaErr (data) {
    showMsg('Falha ao executar ação. Erro: ' + data.erro.mensagem);
    return false;
}

function checkVendaOK (data) {
    if (data.conteudo != '') { 
        $('#data-venda').html(data.conteudo);
    } else {
        $('#modal-mensagem .modal-body').html('Não há dados para o ID informado.');
        $('#modal-mensagem').modal('toggle');
        $('#ativar-ingresso').attr('disabled', false);
    }
}

function checkVendaErr (data) {
    $('#modal-mensagem .modal-body').html('Verifique o ID da compra e tente novamente.');
    $('#modal-mensagem').modal('toggle');
    $('#ativar-ingresso').attr('disabled', false);
    return false;
}

(function($) {
    $.fn.keyboard = function(options) {
        // Settings
        var settings = $.extend({
            numpadLayout: [
                [
                    ['7'],
                    ['8'],
                    ['9']
                ],
                [
                    ['4'],
                    ['5'],
                    ['6']
                ],
                [
                    ['1'],
                    ['2'],
                    ['3']
                ],
                [
                    ['del'],
                    ['0'],
                    ['.']
                ]
            ],
            telLayout: [
                [
                    ['1'],
                    ['2'],
                    ['3']
                ],
                [
                    ['4'],
                    ['5'],
                    ['6']
                ],
                [
                    ['7'],
                    ['8'],
                    ['9']
                ],
                [
                    ['del'],
                    ['0'],
                    ['.']
                ]
            ],
            layout: false,
            type: false,
            btnTpl: '<button type=\"button\" tabindex=\"-1\">',
            btnClasses: 'btn btn-default',
            btnActiveClasses: 'active btn-primary',
            initCaps: false,
            placement: 'bottom',
            container:'body',
            trigger: 'focus'
        }, options);
        if (!settings.layout) {
            if (($(this).attr('type') === 'tel' && $(this).hasClass('keyboard-numpad')) || settings.type === 'numpad') {
                settings.layout = settings.numpadLayout;
            } else if (($(this).attr('type') === 'tel') || settings.type === 'tel') {
                settings.layout = settings.telLayout;
            } else {
                settings.layout = settings.keyboardLayout;
            }
        }
        // Keep track of shift status
        var keyboardShift = false;

        // Listen for keypresses
        var onKeypress = function(e) {
            $(this).addClass(settings.btnActiveClasses);
            var keyContent = $(this).attr('data-value' + (keyboardShift ? '-alt' : ''));
            var parent = $('[aria-describedby=' + $(this).closest('.popover').attr('id') + ']');
            var currentContent = parent.val();
            switch (keyContent) {
                case 'space':
                    currentContent += ' ';
                    break;
                case 'shift':
                    keyboardShift = !keyboardShift;
                    keyboardShiftify();
                    break;
                case 'del':
                    currentContent = currentContent.substr(0, currentContent.length - 1);
                    break;
                case 'enter':
                    parent.closest('form').submit();
                    break;
                default:
                    currentContent += keyContent;
                    keyboardShift = false;
            }
            parent.val(currentContent);
            keyboardShiftify();
            parent.focus();
        };
        $(document).off('touchstart', '.jqbtk-row .btn');
        $(document).on('touchstart', '.jqbtk-row .btn', onKeypress);

        $(document).off('mousedown', '.jqbtk-row .btn');
        $(document).on('mousedown', '.jqbtk-row .btn',function(e){
            onKeypress.bind(this,e)();
            var parent = $('[aria-describedby=' + $(this).closest('.popover').attr('id') + ']');
            parent.focus();
            e.preventDefault();
        });

        // All those trouble just to prevent clicks on the popover from cancelling the focus
        $(document).off('mouseup', '.jqbtk-row .btn');
        $(document).on('mouseup', '.jqbtk-row .btn',function(e){
            $(this).removeClass(settings.btnActiveClasses);
            var parent = $('[aria-describedby=' + $(this).closest('.popover').attr('id') + ']');
            parent.focus();
        });

        $(document).on('click', '.jqbtk-row .btn',function(e){
            var parent = $('[aria-describedby=' + $(this).closest('.popover').attr('id') + ']');
            parent.focus();
        });
        $(document).on('touchend', '.jqbtk-row .btn', function() {
            $(this).removeClass(settings.btnActiveClasses);
            var parent = $('[aria-describedby=' + $(this).closest('.popover').attr('id') + ']');
            parent.focus();
        });
        $(document).on('touchend', '.jqbtk-row', function(e) {
            e.preventDefault();
            var parent = $('[aria-describedby=' + $(this).closest('.popover').attr('id') + ']');
            parent.focus();

        });
        // Update keys according to shift status
        var keyboardShiftify = function() {
            $('.jqbtk-container .btn').each(function() {
                switch ($(this).attr('data-value')) {
                    case 'shift':
                    case 'del':
                    case 'space':
                    case 'enter':
                        break;
                    default:
                        $(this).text($(this).attr('data-value' + (keyboardShift ? '-alt' : '')));
                }
            });
        };
        var container = this.data('container');
        if(container!=undefined)
        {
          container = '#'+container;
          settings.container = container;
          settings.placement = 'in';
          settings.trigger = 'manual';
          $(container).addClass('keyboard-container');
        }
        // Set up a popover on each of the targeted elements
        return this.each(function() {
            $(this).popover({
                content: function() {
                    // Optionally set initial caps
                    if (settings.initCaps && $(this).val().length === 0) {
                        keyboardShift = true;
                    }
                    // Set up container
                    var content = $('<div class=\"jqbtk-container\" tabIndex=\"-1\">');
                    $.each(settings.layout, function() {
                        var line = this;
                        var lineContent = $('<div class=\"jqbtk-row\">');
                        $.each(line, function() {
                            var btn = $(settings.btnTpl).addClass(settings.btnClasses);
                            btn.attr('data-value', this[0]).attr('data-value-alt', this[1]);
                            switch (this[0]) {
                                case 'shift':
                                    btn.addClass('jqbtk-shift').html('<span class=\"glyphicon glyphicon-arrow-up\"></span>');
                                    break;
                                case 'space':
                                    btn.addClass('jqbtk-space').html('&nbsp;');
                                    break;
                                case 'del':
                                    btn.addClass('jqbtk-del').html('<span class=\"glyphicon glyphicon-arrow-left\"></span>');
                                    break;
                                case 'enter':
                                    btn.addClass('jqbtk-enter').html('Enter');
                                    break;
                                default:
                                    btn.text(btn.attr('data-value' + (keyboardShift ? '-alt' : '')));
                            }
                            lineContent.append(btn);
                        });
                        content.append(lineContent);
                    });
                    return content;
                },
                html: true,
                placement: settings.placement,
                trigger: settings.trigger,
                container:settings.container,
                viewport: settings.container
            });
            if(settings.trigger == 'manual')
            {
              $(this).popover('show');
            }
        });
    };

}(jQuery));

$(function(){
    $('#keypad-numbers').keyboard();
});

$('#inline').keyboard();

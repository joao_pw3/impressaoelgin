<?php
$this->title = 'ADM Estádio Mineirão :: Atualizar cadastro';
?>
<div class="container row">
    <h2 class="tituloGeral"><i class="fa fa-clone"></i> Atualizar cadastro</h2>
    <div class="col-md-12">		
        <div>
            <?= $this->render('form', ['model' => $model]); ?>
        </div>
    </div>
</div>
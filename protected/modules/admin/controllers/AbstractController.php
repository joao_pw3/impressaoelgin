<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\models\LoginForm;
use app\models\ApiPermissoes;

/**
 * Classe abstrata para controllers admin
 */
abstract class AbstractController extends Controller
{
    protected $permissoes;

    /**
     * @inheritdoc
     * Método que regula o acesso de usuários às diferentes partes do sistema
     * Qualquer usuário operador E não-cliente pode acessar logout e index
     * usuário comprador não pode acessar as actions deste controller
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except'=>['login'],
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            if(Yii::$app->user->isGuest)
                                return false;//$this->redirect(['/admin/default/login']);
                            if(Yii::$app->user->identity->comprador)
                                return false;//$this->redirect(['/site']);
                            // método que checa a permissão do usuário para cada ação
                            return $this->permitirAcessoPara($action);
                        },
                        'denyCallback' => function ($rule, $action) {
                            return $this->redirect(['/site/error']);
                        }
                    ],
                ],
            ]
        ];
    }

    /**
     * Regras de acesso para ações específicas
     * Permite o acesso dos níveis de acordo com o switch/case com o e-mail do usuário
     * @param $action objeto yii\base\InlineAction traz informações da action requisitada
     * @todo mudar usuário de e-mails para níveis de acesso retornados pelo Web Service
     */
    public function permitirAcessoPara($action){
        try{
            $controle=$action->controller->id;
            $acao=$action->id;
            $session=self::sessaoActionsPermissoes();
            if(in_array('*|*', $session['permissoes_usuario'])) return true;
            if($controle.'/'.$acao == 'default/index' || $controle.'/'.$acao == 'default/logout') return true;
            $usuario=Yii::$app->user->identity;
            return in_array($controle.'/'.$acao, $session['permissoes_usuario']);
        }catch(Exception $e){
            return $this->redirect(['/site/error'],['message'=>$e->getMessage()]);
        }
    }

    /**
     * Método chamado antes de cada action (página) desse controller
     * Verificar se existe token em sessão para manter usuário logado
    */
    public function beforeAction($action){

        $model = new LoginForm();
        $model->autologin($this->getTokenSessao());
        $this->enableCsrfValidation=false;
        return parent::beforeAction($action);
    }

    /**
     * Abrir sessão, verificando primeiro se já está aberta
     */
    protected static function _sessionOpen(){
        $session = Yii::$app->session;
        if (!$session->isActive)
            $session->open();
        return $session;
    }

    /**
     * Recuperar o token do usuário logado que fica gravado em sessão após o login
     */
    protected function getTokenSessao(){
        $session = $this->_sessionOpen();
        return $session->get('user_token');
    }

    /**
     * Gravar o token do usuário em sessão após o login
     */
    protected function setTokenSessao($token){
        $session = $this->_sessionOpen();
        return $session->set('user_token',$token);
    }

    protected static function sessaoActionsPermissoes($reset=false){
        $session=self::_sessionOpen();
        if($reset) $session['permissoes_usuario']=null;
        if(is_null($session['permissoes_usuario'])){
            $permissoesUsuario=(new ApiPermissoes)->consultarPermissoes(Yii::$app->user->identity->conta_usuario);
            $permissoesAtivas=[];
            if($permissoesUsuario->successo)
                foreach($permissoesUsuario->objeto as $o){
                    if($o->inativo==0)
                        $permissoesAtivas[]=$o->permissao;
                }
            $session['permissoes_usuario']=$permissoesAtivas;
        }
        return $session;
    }

    public function getPermissoes(){
        $this->permissoes=self::sessaoActionsPermissoes();
        return $this->permissoes;
    }
}

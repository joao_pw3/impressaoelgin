<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Produto - Editar';
?>


<div class="container">
	<div class="row">
		<h2 class="tituloGeral"><i class="fa fa-clone"></i> Produto - Editar</h2>
	</div>

	<?php
	$form = ActiveForm::begin(['id' => 'produto-form']); ?>
	
	<div class="col-md-6 sempadL">
		<?= $form->field($model, 'id_agenda')->dropDownList(yii\helpers\ArrayHelper::map($eventos, 'id', 'data', 'evento')) ?>
	</div>
	<div class="col-md-6 sempadR">
		<?= $form->field($model, 'nome') ?>
	</div>
	
	<?= $form->field($model, 'descricao')->textarea(['rows' =>5]) ?>

	<div class="col-md-3 sempadL">
		<?= $form->field($model, 'valor') ?>
	</div>
	<div class="col-md-3 sempadR">
		<?= $form->field($model, 'descontoFixo') ?>
	</div>
	<div class="col-md-3 sempadR">
		<?= $form->field($model, 'descontoPercentual') ?>
	</div>
	<div class="col-md-3 sempadR">
		<?= $form->field($model, 'ativo') ?>
	</div>

	<div class="form-group">
		<?= Html::submitButton('Salvar', ['class' => 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>
	
</div>
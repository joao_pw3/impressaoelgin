<?php
use yii\helpers\Html;

$this->title = 'ADM Estádio Mineirão';
?>
<div class="container-fluid" align="center">
    <div class="row">
        <?= Html::img('@web/images/logoMineirap.png', ['alt' => 'Adm Mineirão', 'title' => 'Adm Mineirão', 'class' => 'img-adm']); ?>
    </div>
    <div class="row aument">
        <div class="col-xs-12">
            <p>Olá, <?= Yii::$app->user->identity->usuario; ?>.<br>Seja bem-vindo ao espaço administrativo do Mineirão.</p> 
        </div>
    </div> 
</div>

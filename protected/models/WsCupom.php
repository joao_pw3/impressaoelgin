<?php
namespace app\models;
use app\models\Ws;

/**
 * Classe para Cupons - Easy for Pay
 * obs.: parâmetros entre colchetes são opcionais
 */
class WsCupom extends Ws
{
    
    /**
     * Método construtor - obrigado a passar o token do vendedor para separação de unidades
     */
    public function __construct($tokenVendedor=''){
        if (empty($tokenVendedor)) {
            throw new \Exception("Token vendedor não foi declarado", 1);
        }
        $this->setToken($tokenVendedor, 'V');
    }
    
    /**
     * Criar cupom
     * @param array $post Dados do cupom: id, nome, desconto ([fixo], [percentual]), [validade] ([inicial], [final]), cumulativo, ativo
     * @return object
     */
    public function criarCupom($post)
    {
        return json_decode($this->call('cupom/', 'POST', $post));
    }
    
    /**
     * Alterar cupom
     * @param string $id Id do cupom
     * @param array $post Dados do cupom: nome, desconto ([fixo], [percentual]), [validade] ([inicial], [final]), cumulativo, ativo
     * @return object
     */
    public function alterarCupom($id, $post)
    {
        return json_decode($this->call('cupom/' . $id, 'PUT', $post));
    }
    
    /**
     * Excluir cupom
     * @param array $post Dados do cupom: id
     * @return object
     */
    public function excluirCupom($post)
    {
        return json_decode($this->call('cupom/' . $post['id'], 'DELETE'));
    }
    
    /**
     * Consultar cupom
     * @param array $post Dados do cupom: id
     * @return object
     */
    public function consultarCupom($post=[])    
    {
        return json_decode($this->call('cupom/buscar', 'PUT', $post));
    }
    
    /**
     * Adicionar estoque a cupom
     * @param array $post Dados do cupom: id, unidades, tid
     * @return object
     */
    public function adicionarEstoqueCupom($post)
    {
        return json_decode($this->call('cupom/' . $post['id'] . '/estoque', 'POST', [
            'unidades' => $post['unidades'], 
            'tid' => isset($post['tid']) ? $post['tid'] : substr(time() . mt_rand(), 8, 11)
        ]));
    }

    /**
     * Consultar estoque de cupom
     * @param $post Dados do cupom: id
     * @return object
     */
    public function consultarEstoqueCupom($post)
    {
        return json_decode($this->call('cupom/' . $post['id'] . '/estoque', 'GET'));
    }
    
    /**
     * Clonar cupom
     * @param array $post Dados de cupons para clonagem
     */
    public function clonarCupom($post) 
    {
        return json_decode($this->call('cupom/clonar/' . $post['id_origem'] . '/' . $post['id_destino'], 'PUT'));
    }
    
}

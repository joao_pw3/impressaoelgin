<?php
namespace app\models;

use Yii;


/**
 * Classe geral de acesso à API Pague Tudo
 * Token Vendedor da conta ti@easyforpay.com.br
 * login: {"user_id":"ti@easyforpay.com.br","senha":"577847"}
 */
class Ws
{
    private $token = [
        //token administrativo
        'A' => 'a9c73940a8ab6eb919b671a5d3a339e80212cd3fdbb3cf16d4850cc70754613aef0302926af83598fbde4fbd6fd8dd8f2a98c2ce30ddb0fedf49578297549f1c',
        //token vendedor
        'V' => '',
        //token Usuario (cliente)
        'U' => '',
        //token Custom
        'C' => ''
    ];

    //HOMOLOGAÇÃO
    public $url = 'http://54.94.144.200/sandbox/paguetudo/v1/'; 
    // public $url = 'https://paguetudo.easyp.com.br/api/paguetudo/v1/'; 

    /**
     * Acesso à API Easy for Pay
     * 
     * @param string $endpoint URL para o ending point utilizado
     * @param string $method Método utilizado
     * @param array/null $data Dados para a API
     * @param string $parse Tipo de retorno
     * @param string $key Tipo de token: A = Administrador / U = User / V = Vendedor
     * @return objeto
     */
    public function call($endpoint, $method, $data = null, $parse = true, $key = 'V') 
    {
        // Yii::debug(['Objeto enviado ao webservice'=>[$endpoint, $method, $data, $parse, $this->getToken(), $key]]);

        if (!$endpoint) {
            return false;
        }
        $data = $parse ? $this->parse($data) : $data;
        $headers = [
            'Accept: application/json', 
            'Content-Type: application/json', 
            'Authorization: Custom ' . $this->getToken($key), 
            strtoupper($method) == 'PUT' ? 'Content-Length:' . strlen($data) : ''
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url . $endpoint);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if (!empty($data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if (!$result = curl_exec($ch)) {
            return curl_error($ch);
        } else {
            // Yii::debug(['Retorno do webservice'=>$result]);
            return $result;
        }
    }
    
    /**
     * Converter informação em UTF8
     * 
     * @param array $param Parâmetros para conversão em UTF8
     * @return array
     */
    public function utf8ize($param) 
    {
        if (is_array($param)) {
            foreach ($param as $k => $v) {
                $param[$k] = $this->utf8ize($v);
            }
        } else if (is_string($param) && mb_detect_encoding($param) === 'ISO-8859-1') {
            return utf8_encode($param);
        }
        return $param;
    }

    /**
     * Converter informação em UTF8 e codificá-la em JSON
     * 
     * @param type $data Informação para conversão
     * @return type
     */
    function parse($data) 
    {
        if (!$data) {
            return null;
        }
        return json_encode($this->utf8ize($data));
    }
    
    /**
     * Retornar objeto com mensagem de erro
     */
    public function getApiError($errorMsg) 
    {
        return (object)['successo' => '0', 'erro' => (object)['mensagem' => $errorMsg]];
    }
    
    /**
     * Retornar objeto com mensagem de sucesso
     */
    public function getApiSuccess($successMsg) 
    {
        return (object)['successo' => '1', 'mensagem' => $successMsg];
    }
    
    /**
     * Registrar log para operações do site
     * 
     * @param string $logNome Identificação do log
     */
    public function setLog($logNome) 
    {
        $this->logs = new Logs($logNome, Yii::getAlias('@app') . '/logs/');
    }   
    
    /**
     * Setar valor de URL da Api
     */
    public function setUrl($url=''){
        $this->url=$url;
    }   
    
    /**
     * Retornar a URL da Api
     */
    public function getUrl(){
        return $this->url;
    }
    
    /**
     * Criptografar informação
     * @param array $post mensagem para ser criptografada
     * @return type
     */
    public function setCriptoHash($post=[]) 
    {
        $session = Yii::$app->session;
        $session->open();
        if (is_array($post) && !empty($post) && isset($session['user_token']) && !empty($session['user_token'])) {
            return json_decode($this->call('cripto/', 'POST', $post));
        }
        return $this->getApiError('Faltam parâmetros para criptografar informação.');
    }
        
    /**
     * Descriptografar informação
     * @param string $post Mensagem para ser descriptografada
     * @return type
     */
    public function getCriptoHash($post=[]) 
    {
        if (is_array($post) && !empty($post)) {
            return json_decode($this->call('cripto/', 'PUT', $post));
        }
        return $this->getApiError('Faltam parâmetros para descriptografar informação.');
    }

    /**
     * Setter de $token. Pode enviar apenas um valor ou todos
     * @param $token um valor de um token ou array com os índices A, V, C e U
     * @param $indice o índice do array $token
     */
     public function setToken($token,$indice=''){
        if(is_array($token))
            return $this->token=$token;
        if(array_key_exists($indice, $this->token))
            return $this->token[$indice]=$token;
     }

    /**
     * Getter de $token. retorna todo o array ou apenas o índice desejado
     * @param $token um valor de um token ou array com os índices A, V, C e U
     */
     public function getToken($indice=''){
        if($indice!='')
            return $this->token[$indice];
        return $this->token;
     }    
}
<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="container">
	<div class="row ">
        <div class="col-md-12 col-xs-12">
			<img src="<?php echo Yii::getAlias('@web')?>/images/bannerFaq.jpg" alt="" style="width:100%;">
			<br>
			<br>
            <div id="accordion" role="tablist">
                <div class="card">
                    <div class="card-header" role="tab" id="headingSix">
                        <h5 class="mb-0">
                            <a class="collapsed" data-toggle="collapse" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                    Posso fazer a compra com o cadastro do ano de 2016?
                            </a>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse espacoLinha" role="tabpanel" aria-labelledby="headingSix" data-parent="#accordion">
                        <div class="card-body">
                        Para comprar os ingressos da temporada 2018 é necessário fazer um novo cadastro. Os dados podem ser os mesmos, porém não é possível pular a etapa de cadastro.
                        </div>
                    </div>
                </div>     
                <div class="card">
                    <div class="card-header" role="tab" id="headingThree">
                        <h5 class="mb-0">
                             <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Não consigo acessar meu cadastro
                             </a>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse espacoLinha" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                        Verifique se você está digitando o seu e-mail de forma correta.
                        O e-mail cadastrado é o mesmo para o qual foi enviado o convite para a renovação.
                        O cadastro utilizado no ano passado para fazer a renovação não é mais válido. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" role="tab" id="headingOne">
                        <h5 class="mb-0">
                             <a data-toggle="collapse" href="#collapseTwo" data-toggle="collapse" aria-expanded="false" aria-controls="collapseTwo">
                                 Quero fazer meu pagamento com mais de um cartão. Como fazer?
                            </a>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse espacoLinha" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">No momento, só é possível utilizar um cartão por compra. Se você está recebeu o link por e-mail, os seus assentos estarão já no carrinho ao acessar a URL que enviamos ao seu e-mail. Nesse caso você pode seguir estes passos:
                            <ul><br>
                                <li>Clique no ícone da Área de Cliente (<i class="fa fa-user"></i>) na parte superior para acessar sua conta. Caso ainda não possua conta, faça seu cadastro <?= Html::a('clicando aqui', Url::to(['site/cadastro'])); ?>. Importante: seu cadastro da temporada de 2017 não poderá ser reutilizado pois estamos com um novo sistema;</li>
                                <li>Cadastre seus cartões na opção "Meus cartões". Você pode cadastrar quantos cartões desejar, mas apenas um deles será o seu favorito, utilizado para efetuar o pagamento. Este cartão é identificado com o ícone de um coração verde (<span class="fa fa-heart plus-green"></span>). Para mudar o seu cartão favorito, basta clicar no ícone do coração (<i class="fa fa-heart-o"></i>) ao lado do número do cartão desejado;</li>
                                <li>Prossiga para o carrinho de compras e deixe apenas os produtos que você quer pagar com o cartão favorito atual, apagando os outros. Não se preocupe, seus assentos estão reservados apenas para você até dia 02/01/2017;</li> 
                                <li>Prossiga normalmente com a compra. Após confirmar que o pagamento foi aprovado, basta acessar novamente o link de renovação que vão aparecer os produtos que ainda não foram renovados. Acesse novamente a página "Meus cartões", marque outro cartão como favorito e prossiga para a compra como no passo anterior e pronto!</li>
                            </ul>                            
                        </div>
                    </div>
                </div>   
                <div class="card">
                    <div class="card-header" role="tab" id="headingOne">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" href="#collapseOne" data-toggle="collapse" aria-expanded="false" aria-controls="collapseOne">
                                O que é o código de verificação do meu CVV?
                            </a>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse espacoLinha" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                         CVV é um código de segurança de 3 ou 4 dígitos, impresso no verso de cartões de crédito. 
                         Em alguns cartões, como American Express, você encontra o CVV na frente do cartão. 
                         Em nosso sistema, este código é utilizado para a confirmação do pagamento.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" role="tab" id="headingOne">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" href="#collapseNA" data-toggle="collapse" aria-expanded="false" aria-controls="collapseOne">
                                Minha compra não foi autorizada! Por quê?
                            </a>
                        </h5>
                    </div>
                    <div id="collapseNA" class="collapse espacoLinha" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                         Isso pode acontecer por vários motivos - limite, liberação para compras online, compras com valor alto, etc. Somente o <strong>banco emissor do cartão</strong> sabe dessas condições e pode liberar o uso do cartão. Entre em contato com seu banco e explique que está fazendo uma compra online em nosso site. Geralmente em questão de minutos você poderá repetir a operação.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" role="tab" id="headingFive">
                        <h5 class="mb-0">
                            <a class="collapsed" data-toggle="collapse" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                    Como vejo os ingressos que comprei?
                            </a>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse espacoLinha" role="tabpanel" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                        Um link para acessar os ingressos é enviado para o e-mail utilizado em seu cadastro. O envio pode levar até 24 horas.
                        Além disso, é possível ver os ingressos no item "Minhas Compras", localizado em sua Área de Cliente.
                        </div>
                    </div>
                </div>
            </div>
	    </div>
    </div>
</div>
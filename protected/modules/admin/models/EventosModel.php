<?php
namespace app\modules\admin\models;

use yii\base\Model;
use app\modules\admin\models\QrcodeRepositorio;
use app\modules\admin\models\QrcodeEventos;

/**
 * Classe Model para Eventos - Easy for Pay - Mineirão
 * Criar eventos e carga de ingressos
 */
class EventosModel extends Model
{

    /**
     * Criar novos ingressos - buscar $qtde ingressos da tabela qrcode_repositorio e salvar em planilha para envio à gráfica
     * @param qtde a quantidade total de ingressos
     * @return boolean true se salvar a mesma quantidade de ingressos solicitada, false em caso de falha
     * @throws Exception falha ao buscar, sem qtde necessária de ingressos na base ou parâmetro incorreto
     * @todo Gerar a planilha e aplicar mecânica para gravação após recebimento dos ingressos com o lote
    */
    public function novosIngressos($qtde){
        if(!is_integer($qtde) || $qtde<=0) throw new \Exception('Quantidade inválida',null);
        $repositorio=QrcodeRepositorio::find()
            ->where(['exportado'=>0])
            ->limit($qtde)
            ->all();
        if($repositorio==null) throw new \Exception('Falha ao buscar códigos no repositório',null);
        if(count($repositorio)!==$qtde) throw new \Exception('Quantidade de ingressos insuficiente',null);

        // gerar a planilha com o resultado acima

    }

    /**
     * Carga de ingressos - buscar $qtde ingressos da tabela qrcode_repositorio e salvar na tabela qrcode_eventos
     * @param qtde a quantidade total de ingressos
     * @return boolean true se salvar a mesma quantidade de ingressos solicitada, false em caso de falha
     * @throws Exception erro de importação, sem qtde necessária de ingressos na base ou parâmetro incorreto; Erro salvando na tabela de eventos
    */
    public function cargaIngressos($qtde, $evento, $lote){
        if(!is_integer($qtde) || $qtde<=0) throw new \Exception('Quantidade inválida',0);
        $repositorio=QrcodeRepositorio::find()
            ->where(['exportado'=>0])
            ->limit($qtde)
            ->all();
        foreach ($repositorio as $rep) {
            $qr=new QrcodeEventos;
            $qr->idqrcode=$rep->id;
            $qr->evento=$evento;
            $qr->datageracao=time();
            $qr->lote=$lote;
            $qr->ingresso=$rep->qrcode;
            if(!$qr->validate()) print_r($qr->getErrors());
            if(!$qr->save()) throw new \Exception('Erro salvando model QrcodeEventos',1);
            $update=QrcodeRepositorio::findOne($rep->id);
            $update->exportado=1;
            if(!$update->save()) throw new \Exception('Erro atualizando model QrcodeRepositorio',2);
        }
    }
      
}
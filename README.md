Instalação do projeto

1. git clone https://aeasy@bitbucket.org/joao_pw3/mineirao.git
2. php composer.phar create-project yiisoft/yii2-app-basic basic 2.0.14
3. Copia a pasta vendor para protected/vendor
4. Adicione no protected/config/web.php
    'modules' => [
           'admin' => [
               'class' => 'app\modules\admin\Module',
           ],
       ],   
5. Adicione no /protected/config/test.php
        'request' => [
            'enableCsrfValidation' => false,
        ],
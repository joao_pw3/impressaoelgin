<?php
namespace app\models;

use yii\base\Model;

class Tags extends Model{
	public $nome;
	public $valor;
	public $lista;

	public function rules(){
		return [
			[['nome'],'required'],
			[['valor'],'required', 'on'=>'create']
		];
	}

	public function adicionar($apiObject, $codigo){
		if($this->validate())
			return $apiObject->adicionarTag(['codigo'=>$codigo,'nome'=>$this->nome,'valor'=>$this->valor]);
	}

	public function excluir($apiObject,$codigo){
		if($this->validate())
			return $apiObject->excluirTag(['codigo'=>$codigo,'nome'=>$this->nome]);
	}

	public static function stringTags($tags){
		if(is_array($tags))
			foreach ($tags as $tag)
				echo ' '.$tag->nome.':'.$tag->valor.', ';
	}

	public static function objetoTags($tags){
        $obj=[];
		if(is_array($tags)){
	        foreach ($tags as $key => $value) {
	            $obj[$value->nome]=$value->valor;
	        }
	    }
        return (object)$obj;
	}

	public static function arrayTags($tags){
        $obj=[];
        if(is_array($tags)){
            foreach ($tags as $key => $value) {
                $obj[$value->nome]=$value->valor;
            }
        }
        return $obj;
	}

	public function listFromObjTags($tags){
		foreach ($tags as $key => $value) {
			$obj=new Tags;
            $obj->nome=$value->nome;
            $obj->valor=$value->valor;
            $this->lista[]=$obj;
        }
        return (object) $obj;
    }

}

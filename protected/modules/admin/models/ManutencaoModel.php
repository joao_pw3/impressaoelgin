<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;

/**
 *
 */
class ManutencaoModel extends Model
{
    public $texto;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // fields are safe
            [['texto'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'texto' => 'Digite o texto para o site entrar em manutenção'
        ];
    }    
}

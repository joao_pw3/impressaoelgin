<?php
namespace app\models;

use Yii;
use yii\base\Model;

class Anexos extends Model{
	public $nome;
	public $tipo;
	public $objeto;
	public $url;
	public $lista;

	public function rules(){
		return [
			[['nome','tipo'],'required'],
			[['objeto'],'file', 'extensions'=>'jpg, jpeg, png, gif', 'mimeTypes' => 'image/jpeg, image/png, image/gif'],
			[['objeto'],'required', 'on'=>'create']
		];
	}

	public function adicionar($apiObject,$codigo){
		if($this->validate())
			return $apiObject->adicionarAnexo(['codigo'=>$codigo, 'nome'=>$this->nome, 'tipo'=>$this->tipo, 'objeto'=>$this->objeto]);
	}

	public function excluir($apiObject,$codigo){
		if($this->validate())
			return $apiObject->excluirAnexo(['codigo'=>$codigo, 'nome'=>$this->nome]);
	}

	public static function stringAnexos($anexos){
		if(is_array($anexos))
			foreach ($anexos as $a)
				echo ' '.$a->nome.': <a target="_blank" href="'.$a->url.'">'.$a->tipo.'</a>, ';
	}

	public static function objetoAnexos($anexos,$retornarObjeto=true){
		$obj=[];
        if(is_array($anexos))
            foreach ($anexos as $a) {
                $obj[$a->nome]=$a->url;
            }
        return $retornarObjeto ?(object)$obj :$obj;
	}

	public function imgBase64($file)
    {
    	$erro=$this->tratarUpload($file);
    	if($erro!='')
    		return $this->addError('objeto','Houve um erro ao tentar salvar a imagem: '.$erro);	
    	$path=Yii::getAlias('@app').DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR;
    	if(!$file->saveAs($path . $file->baseName . '.' . $file->extension))
    		return $this->addError('objeto','Houve um erro ao tentar salvar a imagem. Código do erro: '.$file->error);
    	$this->objeto=base64_encode(file_get_contents($path . $file->baseName . '.' . $file->extension));
    }

	public function listFromObjAnexos($anexos){
		foreach ($anexos as $key => $value) {
			$obj=new Anexos;
            $obj->nome=$value->nome;
            $obj->tipo=$value->tipo;
            $obj->url=$value->url;
            $this->lista[]=$obj;
        }
	}
	
	 /**
	  * Mensagens de erro para tratar o upload de arquivo
	  * @param UploadedFile $file objeto criado a partir do upload
	  * @return string mensagem de erro. Vazia se não houver erro
	  */
	private function tratarUpload($file){
    	$upload_max_filezise=ini_get('upload_max_filesize');
		switch($file->error){
			case '0':
				return '';
			break;
			case '1':
				return 'O arquivo é maior do que o permitido. Favor enviar arquivos de até '.$upload_max_filezise;
			break;
			case '2':
				return 'O arquivo é maior do que o permitido neste formulário';
			break;
			case '3':
				return 'O arquivo foi enviado parcialmente e descartado. Tente outro arquivo com tamanho menor';
			break;
			case '4':
				return 'Nenhum arquivo enviado';
			break;
			case '6':
				return 'Falha de servidor (pasta temporária não localizada)';
			break;
			case '7':
				return 'Falha de servidor (erro ao escrever no disco)';
			break;
			case '8':
				return 'Falha de servidor (uma extensão interrompeu o upload)';
			break;
			default:
				return 'Erro desconhecido';
			break;
		}
	}

	public function acaoAposSalvar($modelAnexos,$post,$modelAgenda){
		return Yii::$app->getResponse()->redirect(['/admin/agenda/registro-evento','evento'=>$modelAgenda->codigo]);
	}
}
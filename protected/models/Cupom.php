<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\WsCupom;

/**
 * Classe que gerencia cupons do sistema
 */
class Cupom extends Model 
{
    public $id;
    public $unidade;
    public $nome;
    public $descricao;
    public $desconto_fixo;
    public $desconto_percentual;
    public $validade_inicial;
    public $validade_final;
    public $cumulativo;
    public $ativo;
    public $estoque;
    public $lista;
    public $tipo;
    public $id_agenda;
    
    /**
     * Regras para validação dos campos
     * @return type
     */
    public function rules() 
    {
    	return [
            [['id'], 'required', 'on' => 'delete'],
            [['id', 'nome', 'id_agenda'], 'required'],
            [['id_agenda'], 'string', 'max' => 8],
            [['desconto_fixo', 'desconto_percentual'], 'double'],
//            ['desconto_fixo', 'either', 'params' => ['other' => 'desconto_percentual'], 'skipOnEmpty' => false],
            [['id', 'nome'], 'string', 'max' => 128],
            [['descricao'], 'string', 'max' => 10240],
            [['cumulativo', 'ativo'], 'integer'],
            [['validade_inicial', 'validade_final', 'estoque', 'tipo'], 'safe']
    	];
    }

    /**
     * definição dos labels de atributos
     */
    public function attributeLabels()
    {
        return[
            'id' => 'ID',
            'nome' => 'Nome do cupom',
            'descricao' => 'Descrição (documentos necessários)',
            'desconto_fixo' => 'Desconto fixo',
            'desconto_percentual' => 'Desconto percentual',
            'validade_inicial' => 'Válido de',
            'validade_final' => 'Válido até',
            'cumulativo' => 'Cumulativo',
            'ativo' => 'Ativo',
            'estoque' => 'Estoque',
            'id_agenda' => 'Evento',
            'tipo' => 'Tipo de cupom'
        ];
    } 
    
    /**
     * Desconto - pode ser fixo ou percentual, mas um deles é obrigatório
     */
    public function either($attr, $params)
    {
        if (empty($attr) || empty($this->{$params['other']})) {
            $this->addError($attr, 'Informe o desconto (fixo ou percentual)');
            return false;
        }
        return true;
    }

    /**
     * Método construtor sempre pede a unidade atual
     */
    public function __construct(Cliente $unidade){
        $this->unidade = $unidade;
    }
    
    /**
     * Salvar cupom
     * @Override
    */
    public function save($runValidation=true, $attributeNames=null) 
    {
        if (!$this->validate()) {
            return false;
        }
        $api = new WsCupom($this->unidade->user_token);
        $cupom = [
            'id' => $this->id,
            'nome' => $this->nome,
            'descricao' => $this->descricao,
            'desconto' => [
                'fixo' => $this->desconto_fixo,
                'percentual' => $this->desconto_percentual
            ],
            'validade' => [
                'inicial' => $this->validade_inicial, 
                'final' => $this->validade_final, 
            ],
            'cumulativo' => (int)$this->cumulativo,
            'ativo' => (int)$this->ativo
        ];
        $returnApi = $api->criarCupom($cupom);
        
        if ($returnApi->successo == '0') {
            return $this->addError('lista', $returnApi->erro->mensagem);
        }
        return true;
    }
    
    /**
     * Atualizar cupom
     * @Override
     */
    public function update($runValidation = true, $attributeNames = null)
    {
        $api = new WsCupom($this->unidade->user_token);
        $cupom = [];
        if (!empty($this->nome)) {
            $cupom['nome'] = $this->nome; 
        }
        if (!empty($this->descricao)) {
            $cupom['descricao'] = $this->descricao; 
        }
        if (!empty($this->desconto_fixo) && $this->desconto_fixo > 0) {
            $cupom['desconto']['fixo'] = (string)number_format($this->desconto_fixo, 2);
            $cupom['desconto']['percentual'] = "0.00";
        }
        if (!empty($this->desconto_percentual) && $this->desconto_percentual > 0) {
            $cupom['desconto']['fixo'] = "0.00";
            $cupom['desconto']['percentual'] = (string)number_format($this->desconto_percentual, 2);
        }
        if (!empty($this->validade_inicial)) {
            $cupom['validade']['inicial'] = $this->validade_inicial;
        }
        if (!empty($this->validade_final)) {
            $cupom['validade']['final'] = $this->validade_final;
        }
        if ((int)$this->cumulativo >= 0) {
            $cupom['cumulativo'] = (int)$this->cumulativo;
        }
        if ((int)$this->ativo >= 0) {
            $cupom['ativo'] = (int)$this->ativo;
        }
        $returnApi = $api->alterarCupom($this->id, $cupom);
        return $returnApi->successo == '1' ? true : false;
    }
    
    /**
     * Remover cupom
     */
    public function delete()
    {
        $api = new WsCupom($this->unidade->user_token);
        return $api->excluirCupom(['id' => $this->id]);
    }
    
    /**
     * Buscar cupons
     */
    public function buscarCupom($post=[]) 
    {
        $session = Yii::$app->session;
        $session->open();
        $objApiCliente=new Cliente;
        $this->unidade=$objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));    
        $api = new WsCupom($this->unidade->user_token);
        $cupons = $api->consultarCupom($post);
        if ($cupons->successo == '0') {
            return $this->addError('lista', $cupons->erro->mensagem);
        }
        $this->lista=[];
        foreach ($cupons->objeto as $cupom) {
            $model = new Cupom($objApiCliente);
            $model->loadFromApi($cupom);
            $this->lista[] = $model;
        }
    }
    
    /**
     * Buscar histórico de movimentação de estoque de cupom
     */
    public function buscarHistoricoEstoque($post=[]) 
    {
        $api = new WsCupom($this->unidade->user_token);
        return $api->consultarEstoqueCupom($post);
    }
    
    /**
     * Adicionar estoque de cupom
     */
    public function movimentarEstoque() 
    {
        $api = new WsCupom($this->unidade->user_token);
        return $api->adicionarEstoqueCupom([
            'id' => $this->id, 
            'unidades' => ($this->tipo == 1 ? '' : '-') . $this->estoque
        ]);
    }
    
    /**
     * Clonagem de cupons
     * @param string $idOrigem Id do evento origem - padrão idEvento_idData
     * @param string $idDestino Id do evento destino - padrão idEvento_idData
     */
    public function clonarCupom($idOrigem, $idDestino)
    {
        if (!empty($idOrigem) && !empty($idDestino)) {
            $this->buscarCupom(['id' => '%_' . $idOrigem . '_%']);
            $return = true;
            $msg = '';
            $api = new WsCupom($this->unidade->user_token);
            if ($this->lista) {
                foreach ($this->lista as $index => $cupom) {
                    $prefixo=substr($cupom->id,0,strpos($cupom->id,'_'));                    
                    $returnApi = $api->clonarCupom(['id_origem' => $cupom->id, 'id_destino' => $this->getCupomId($prefixo, $idDestino)]);
                    if ($returnApi->successo == '0') {
                        $return = false;
                        $msg .= '<br>id: ' . $cupom->id . ' - ' . $returnApi->erro->mensagem;
                    }
                }
            } else {
                $return = false;
                $msg = 'Não há registros para serem clonados. Origem: ' . $idOrigem . ', Destino: ' . $idDestino;
            }
            return $return ? (object)['successo' => '1'] : (object)['successo' => '0', 'erro' => ['mensagem' => $msg]];
        }
        return (object)['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros para clonar cupoms.']];
    }
    
    /**
     * Atribuir valores retornado pela API às propriedades
     */
    public function loadFromApi($objeto) {
        if (!isset($objeto->id)) {
            return false;
        }
        $this->id = $objeto->id;
        $this->nome = $objeto->nome;
        $this->descricao = $objeto->descricao;
        $this->desconto_fixo = $objeto->desconto->fixo;
        $this->desconto_percentual = $objeto->desconto->percentual;
        $this->validade_inicial = $objeto->validade->inicial;
        $this->validade_final = $objeto->validade->final;
        $this->cumulativo = (int)$objeto->cumulativo;
        $this->ativo = (int)$objeto->ativo;
        $this->estoque = $objeto->estoque;
        $this->tipo = 'publico';
        if(stristr($objeto->id, 'PRIV_') !== false)
            $this->tipo='privado';
        return true;
    }
    
    /**
     * Criar e retornar id de cupom à partir de informações fornecidas
     * @param string $tipoCupom Tipo de cupom: PRIV (privado) ou PUB (público)
     * @param string $idAgendaData Identificação de evento e data, no formato: idEvento_idData
     */
    public function getCupomId($tipoCupom, $idAgendaData)
    {
        return $tipoCupom . '_' . $idAgendaData . '_' . date('YmdHi') . substr(time() . mt_rand(), 8, 11);
    }
    
    /**
     * Consultar id de evento e data de um cupom a partir do seu ID
     */
    public static function infoEventoData($idCupom)
    {
        $split=explode('_', $idCupom);
        return ['evento'=>$split[1],'data'=>$split[2]];
    }

}
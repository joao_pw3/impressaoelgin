<?php
namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\controllers\AbstractController;
use yii\web\Response;
use app\models\Caixa;
use app\models\Cobranca;
use app\models\Carrinho;
use app\models\WsDados;
use app\models\Cliente;

/**
 * Controller de bilheteria. Funções para operador de bilheteria
 * Padrões do ending point entsai para o caixa:
 *      saida:1 + ajuste:0 = fechamento 
 *      saida:0 + ajuste:0 = abertura
 *      saida:1 + ajuste:1 = sangria 
 *      saida:0 + ajuste:1 = reforço
 */
class CaixaController extends AbstractController 
{
    /**
     * Ações para operação de caixa: abertura / fechamento / sangria
     */
    public function actionIndex() 
    {
        $apiCaixa = new Caixa();
        $caixa = $apiCaixa->resumoCaixa(['id' => 'caixa_' . Yii::$app->user->identity->cod_operador]);
        $saldo = $apiCaixa->getSaldoOperador('caixa_' . Yii::$app->user->identity->cod_operador, $caixa);
        $vendas = $caixa->successo == '1' && isset($caixa->resumo['abertura']['data']) ? 
            $apiCaixa->getVendasOperadorResumo(Yii::$app->user->identity->cod_operador, $caixa->resumo['abertura']['data'], '') : 
            [];
        $dataControle = '';
        if (isset($caixa->resumo['abertura']['data']) && !empty($caixa->resumo['abertura']['data'])) {
            $dataControle = date('YmdHis', strtotime('+12 hours', strtotime(str_replace('/', '-', $caixa->resumo['abertura']['data']))));
        }
        return $this->render('index', [
            'saldo' => $saldo,
            'caixa' => $caixa,
            'vendas' => $vendas,
            'dataControle' => $dataControle
        ]);
    } 
    
    /**
     * Ao fechar o caixa, salvar o relatório
     */
    public function actionSalvarRelatorio()
    {
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                $post = Yii::$app->request->post();
                $session = Yii::$app->session;
                $session->open();
                $api = new WsDados($session->get('user_token'));
                return $api->salvar([
                    'id' => Yii::$app->user->identity->cod_operador . '_' . date('YmdHis'),
                    'tipo' => 'fechamento_caixa',
                    'valor' => base64_encode(json_encode($post))
                ]);
            }
            return (object)['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros para salvar o relatório do caixa.']];
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao salvar o relatório do caixa.', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Efetuar ações de caixa
     */
    public function actionAcaoCaixa()
    {
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                $post = Yii::$app->request->post();
                return (new Caixa)->fazerAcaoCaixa($post);
            }
            return (object)['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros para efetuar ação de caixa.']];
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao fazer ação de caixa.', 'message' => $e->getMessage()]);
        }
    }
    
     /**
     * Renders the relatorios view for the module
     * @return string
     */
    public function actionRelatorio() 
    {
        $caixa = (new Caixa)->lerAcaoCaixa([
            'id' => 'caixa_' . Yii::$app->user->identity->cod_operador, 
            'dateStart' => date('d-m-Y', strtotime('-10days')), 
            'dateFinish' => date('d-m-Y')
        ]);
        return $this->render('relatorio', [
            'caixa' => $caixa
        ]);
    }
    
    /**
     * Relatório de vendas do operador, com opção de estorno e movimentações de caixa
     */
    public function actionRelatorioVendas()
    {
        $relatorio = [];
        $firstDay = new \DateTime('today');
        $lastDay = new \DateTime('today');
        $lastDay->setTime(23, 59, 59);
        $dataDe = $firstDay->format('d/m/Y H:i:s');
        $dataAte = $lastDay->format('d/m/Y H:i:s');
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $dataDe = $post['de'] . ' 00:00:00';
            $dataAte = $post['ate'] . ' 23:59:59';
            $relatorio = (new Caixa)->relatorioCaixa($post['de'], $post['ate']);
        }
        return $this->render('relatorio-vendas', [
            'relatorio' => $relatorio,
            'de' => \DateTime::createFromFormat('d/m/Y H:i:s', $dataDe),
            'ate' => \DateTime::createFromFormat('d/m/Y H:i:s', $dataAte)
        ]);
    } 
    
    /**
     * Consultar carrinho à partir do seu id
     * @return type
     */
    public function actionDetalhes()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $session = $this->_sessionOpen();
            $api = new Cliente;
            $api->buscarUnidadePor('user_token', $session->get('token_unidade'));
            return (new Carrinho($api))->consultarCarrinho(Yii::$app->request->post('id_carrinho'));
        }
        return (object)['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros para consultar o carrinho.']];
    }
    
    /**
     * Efetuar estorno de venda efetuada por operador
     * @return string
     */
    public function actionEstorno() 
    {
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                $post = Yii::$app->request->post();
                $session = Yii::$app->session;
                $session->open();
                $api = new Cliente;
                $api->buscarUnidadePor('user_token', $session->get('token_unidade'));
                $cobranca = new Cobranca($api);

                if ($post['dataCappta'])
                    $dataCappta = json_decode(base64_decode($post['dataCappta']));

                $arrayEstorno = [
                    'id' => isset($dataCappta->guid) ? $dataCappta->conciliador->administrativeCode : 'Especie',
                    'motivo' => $post['motivo']
                ];

                if (isset($dataCappta->guid)) {
                    $arrayEstorno['objeto'] = $post['dataCappta'];
                }                
                return $cobranca->estornarPagamentoBilheteria($post['codigo'], $arrayEstorno);
            }
            return (object)['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros para efetuar estorno - conciliação.']];
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao efetuar estorno - conciliação.', 'message' => $e->getMessage()]);
        }
    }  
    
    /**
     * Exportar conteúdo para CSV - Resumo Caixa
    */
    public function actionExportarCsvResumo()
    {
        $apiCaixa = new Caixa();
        $caixa = $apiCaixa->resumoCaixa(['id' => 'caixa_' . Yii::$app->user->identity->cod_operador]);
        $vendas = $caixa->successo == '1' && isset($caixa->resumo['abertura']['data']) ? 
            $apiCaixa->getVendasOperadorResumo(Yii::$app->user->identity->cod_operador, $caixa->resumo['abertura']['data'], '') : 
            [];
        
        $total = (isset($caixa->resumo['abertura']['valor']) ? $caixa->resumo['abertura']['valor'] : 0) 
                + (isset($caixa->resumo['reforço']) ? $caixa->resumo['reforço'] : 0)  
                - (isset($caixa->resumo['sangria']) ? $caixa->resumo['sangria'] : 0)
                + (isset($vendas->vendas['Autorizado']['Espécie']['valor']) ? $vendas->vendas['Autorizado']['Espécie']['valor'] : 0)
                - (isset($vendas->vendas['Estornado']['Espécie']['valor']) ? $vendas->vendas['Estornado']['Espécie']['valor'] : 0);

        ini_set('max_execution_time', 300);
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=caixa_operador_' . Yii::$app->user->identity->cod_operador . '_' . date('d-m-Y_H-i-s') . '.csv');
        $output = fopen('php://output', 'w');
        fputcsv($output, [utf8_decode('Operação'), 'Valor atual', 'Detalhes'], ';'); 
        
        fputcsv($output, ['Abertura', 'R$ ' . (isset($caixa->resumo['abertura']['valor']) ? number_format($caixa->resumo['abertura']['valor'], 2, ',', '.') : '0,00')], ';');
        fputcsv($output, [utf8_decode('Reforço'), 'R$ ' . (isset($caixa->resumo['reforço']) ? number_format($caixa->resumo['reforço'], 2, ',', '.') : '0,00')], ';');
        fputcsv($output, ['Sangria', 'R$ ' . (isset($caixa->resumo['sangria']) ? number_format($caixa->resumo['sangria'], 2, ',', '.') : '0,00')], ';');
        fputcsv($output, ['Vendas', 'R$ ' . (isset($vendas->vendas['Autorizado']['Espécie']) ? number_format($vendas->vendas['Autorizado']['Espécie']['valor'], 2, ',', '.') : '0,00'), isset($vendas->vendas['Autorizado']['Espécie']) ? $vendas->vendas['Autorizado']['Espécie']['unidades'] . ' unidade' . ($vendas->vendas['Autorizado']['Espécie']['unidades'] > 1 ? 's' : '') : utf8_decode('Não há vendas')], ';');
        fputcsv($output, ['Estornos', 'R$ ' . (isset($vendas->vendas['Estornado']['Espécie']) ? number_format($vendas->vendas['Estornado']['Espécie']['valor'], 2, ',', '.') : '0,00'), isset($vendas->vendas['Estornado']['Espécie']) ? $vendas->vendas['Estornado']['Espécie']['unidades'] . ' unidade' . ($vendas->vendas['Estornado']['Espécie']['unidades'] > 1 ? 's' : '') : utf8_decode('Não há estornos')], ';');
        fputcsv($output, ['Total', 'R$ ' . number_format($total, 2, ',', '.')], ';');
        fputcsv($output, [utf8_decode('Vendas - Crédito'), isset($vendas->vendas['Autorizado']['Cartão de crédito']) ? 
                        'R$ ' . number_format($vendas->vendas['Autorizado']['Cartão de crédito']['valor'], 2, ',', '.') . ' - ' . 
                        $vendas->vendas['Autorizado']['Cartão de crédito']['unidades'] . ' unidade' . 
                        ($vendas->vendas['Autorizado']['Cartão de crédito']['unidades'] > 1 ? 's' : '') : 'Nenhuma'], ';');
        fputcsv($output, [utf8_decode('Vendas - Débito'), isset($vendas->vendas['Autorizado']['Cartão de débito']) ? 
                        'R$ ' . number_format($vendas->vendas['Autorizado']['Cartão de débito']['valor'], 2, ',', '.') . ' - ' . 
                        $vendas->vendas['Autorizado']['Cartão de débito']['unidades'] . ' unidade' . 
                        ($vendas->vendas['Autorizado']['Cartão de débito']['unidades'] > 1 ? 's' : '') : 'Nenhuma'], ';');
        die();
    }
    
    /**
     * Exportar conteúdo para CSV - Vendas
     */
    public function actionExportarCsvVendas($data_ini, $data_fim)
    {
        if (!empty($data_ini) && !empty($data_fim)) {
            $relatorio = (new Caixa)->relatorioCaixa($data_ini, $data_fim);
            
            ini_set('max_execution_time', 300);
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=vendas_caixa_' . date('d-m-Y_H-i-s') . '.csv');
            $output = fopen('php://output', 'w');
            fputcsv($output, ['Tipo', 'Data', utf8_decode('Código'), 'Status', 'Forma pagto.', 'Valor', 'Acumulado'], ';'); 
            
            if (!empty($relatorio)) {
                $total = 0;
                foreach ($relatorio as $data_tipo => $registro) {
                    foreach ($registro as $index => $dados) {
                        if (stristr($data_tipo, '_venda')) {
                            $total = $dados->vendas->status == 'Autorizado' ? $total + $dados->vendas->valor : $total - $dados->vendas->valor;
                            fputcsv($output, ['Venda', $dados->vendas->data, $dados->vendas->codigo, utf8_decode($dados->vendas->status), utf8_decode($dados->vendas->meio_de_pagamento->descricao), 'R$ ' . number_format($dados->vendas->valor, 2, ',', '.'), 'R$ ' . number_format($total, 2, ',', '.')], ';');
                        } else {
                            $objeto = json_decode(base64_decode($dados->objeto)); 
                            $objeto->valor = empty($objeto->valor) ? 0 : $objeto->valor;
                            $total = ($dados->ajuste == 1 && $dados->operacao == 'E') || ($dados->ajuste == 0 && $dados->operacao == 'E') ? $total + $objeto->valor : $total - $objeto->valor;
                            fputcsv($output, [$dados->ajuste == 1 ? ($dados->operacao == 'E' ? 'Reposição' : 'Sangria') : ($dados->operacao == 'E' ? 'Abertura' : 'Fechamento'), $dados->data, '', '', '', 'R$ ' . number_format($objeto->valor, 2, ',', '.'), 'R$ ' . number_format($total, 2, ',', '.')], ';');
                        }
                    }
                }
                die;
            }
        }
    }

}
 <?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use app\modules\admin\assets\IngressosAsset;
IngressosAsset::register($this);

$this->title = 'ADM Estádio Mineirão :: Associar ingressos';
?>
<div class="container">
    <div class="row">
        <h2 class="tituloGeral"><i class="fa fa-clone"></i> Associar ingressos</h2>
    </div>
    <?php
    ActiveForm::begin(['id' => 'form-eventos-associar-ingressos']);
    echo Html::label('Selecione o evento','id_agenda');
    echo Html::dropDownList('id_agenda',null,ArrayHelper::map($eventos,'codigo','nome'));
    echo Html::submitButton('Carregar QR Codes',['class'=>'btn btn-primary']);
    ActiveForm::end();
    echo $msg==''  ?'' :'<p>'.$msg.'</p>';
    if(count($recebidos)>0){?>
        <br /><hr />
        <p class="form form-inline">
            <label>Leia um código para buscar na tabela</label>
            <input class="form-control" type="text" name="associar-ingresso-busca-qrcode" id="associar-ingresso-busca-qrcode" autofocus tabindex="1" />
            <button id="associar-ingresso-busca-qrcode-btn" class="form-control btn btn-primary">OK</button>
        </p>
        <p>ou selecione uma linha abaixo</p>
        <table class="table table-striped table-hover table-responsive table-ingressos-associacao" id="associar-ingressos">
            <thead>
                <tr>
                    <th>QRCode</th>
                    <th>Lote</th>
                    <th>Produto</th>
                    <th>Ocupante</th>
                    <th>Comprador</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($recebidos as $ingresso) { ?>
                <tr>
                    <td class="qrcode-table"><?=$ingresso->qrcode?></td>
                    <td><?=$ingresso->lote_reserva?></td>
                    <td><?=$ingresso->produto?></td>
                    <td><?=$ingresso->ocupante_nome.' - '.$ingresso->documento?></td>
                    <td><?=$ingresso->comprador.' - '.$ingresso->comprador_documento?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <p>Ingressos associados</p>
        <table class="table table-striped table-hover table-responsive table-ingressos-associacao" id="lista-ingressos-associados">
            <thead>
                <tr>
                    <th>QRCode</th>
                    <th>Lote</th>
                    <th>Produto</th>
                    <th>Ocupante</th>
                    <th>Comprador</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($associados as $ingresso) { ?>
                <tr>
                    <td class="qrcode-table"><?=$ingresso->qrcode?></td>
                    <td><?=$ingresso->lote_reserva?></td>
                    <td><?=$ingresso->produto?></td>
                    <td><?=$ingresso->ocupante_nome.' - '.$ingresso->documento?></td>
                    <td><?=$ingresso->comprador.' - '.$ingresso->comprador_documento?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php } ?>

</div>

<?php Modal::begin([
    'header' => '<h4 align="center" id="associar-ingresso-header-modal">Associar ingresso</h4>',
    'size'   => 'modal-md',
    'id'     => 'modal-mensagem-associar-ingresso',
    'options'=> [
        'class' => 'modal-center'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ]
]);
echo Html::tag('div',
        Html::tag('div',
            Html::tag('p','',['id'=>'associar-ingresso-produto'])
            . Html::tag('p','',['id'=>'associar-ingresso-comprador'])
            . Html::tag('p','',['id'=>'associar-ingresso-msg']),
        ['class'=>'col-sm-6'])

        . Html::beginForm('','POST',['id' => 'form-associacao-ingressos','class'=>'col-sm-6'])
        . Html::label('QR Code Impresso no cartão','qrcode',['id'=>'form-associacao-ingressos-qrcode-label'])
        . Html::textInput('qrcode',null,['id'=>'form-associacao-ingressos-qrcode','tabindex'=>'2'])
        . Html::label('RFID','rfid')
        . Html::textInput('rfid',null,['id'=>'form-associacao-ingressos-codigo','tabindex'=>'3'])
        . Html::hiddenInput('id_agenda',$id_agenda,['id'=>'form-associacao-ingressos-idagenda'])
        . Html::button('Associar',['class'=>'btn btn-primary','id'=>'form-associacao-ingressos-associar-btn','tabindex'=>'4', 'data-loading-text'=>'Enviando...'])
        . Html::button('Invalidar',['class'=>'btn btn-warning','id'=>'form-associacao-ingressos-invalidar-btn','tabindex'=>'5'])
        . Html::button('Próximo',['class'=>'btn btn-primary hidden','id'=>'form-associacao-ingressos-proximo-btn'])
        . Html::endForm()

        . Html::tag('p','',['class'=>'clearfix']),
['class'=>'row','id'=>'holderFormAssociar']);

echo Html::tag('div',
    Html::beginForm('','POST',['class'=>'col-sm-12','id' => 'form-invalidar-ingressos'])
    . Html::label('QR Code Impresso no cartão','qrcode',['id'=>'form-invalidar-ingressos-qrcode-label'])
    . Html::textInput('qrcode',null,['id'=>'form-invalidar-ingressos-qrcode'])
    . Html::label('Motivo','motivo')
    . Html::textarea('motivo',null,['id'=>'form-invalidar-ingressos-motivo'])
    . Html::hiddenInput('id_agenda',$id_agenda,['id'=>'form-invalidar-ingressos-idagenda'])
    . Html::tag('p','',['id'=>'invalidar-ingresso-msg'])
    . Html::tag('span','Confira antes de invalidar, essa ação não poder ser revertida &nbsp; ',['id'=>'form-invalidar-ingressos-msg-confirmacao','class'=>'small'])
    . Html::button('Confirmar inválido',['class'=>'btn btn-warning','id'=>'form-invalidar-ingressos-invalidar-btn', 'data-loading-text'=>'Enviando...'])
    . Html::button('Voltar',['class'=>'btn btn-primary','id'=>'form-invalidar-ingressos-voltar-btn'])
    . Html::endForm(),
['class'=>'row','id'=>'holderFormInvalidar']);

Modal::end();

$this->registerJs('
    urlInvalidarIngresso="'.Url::to(['invalidar-ingresso']).'";
    urlAssociacaoIngresso="'.Url::to(['associar-ingressos']).'";
    urlStatusIngresso="'.Url::to(['consultar']).'";
    $(".table-ingressos-associacao tr").click(function(){
        idCampoAutoFocoModal="form-associacao-ingressos-qrcode";
        clickLinhaTabelaAssociarIngresso($(this).find("td"));
    })

    ',\yii\web\View::POS_END);

?>
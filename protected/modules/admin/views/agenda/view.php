<?php
use \yii\helpers\Url;
$this->title = 'ADM Easy for Pay';
?>

<div class="container">
	<div class="row">
		<h2 class="tituloGeral"><i class="fa fa-clone"></i> Agenda - Detalhes do evento</h2>
	</div>	
	
	<div class="boxArredondado" style="margin-top:30px;">
		<h2 style="margin-top:0px; text-align:center;"><?=$evento->nome?></h2>
		<div div class="col-md-5" style="padding-left:0px;">
			<span class="labelForms">Descrição:</span><br><?=$evento->descricao?>
		</div>
		<div div class="col-md-5" style="padding-left:0px;">
			<span class="labelForms">De:</span><br><?=$evento->dataInicial?> <strong>a:<br></strong> <?=$evento->dataFinal?>
		</div>
		<div div class="col-md-2" style="padding-left:0px;">
			<span class="labelForms">Status:</span><br><?=$evento->ativo ?'Ativo' :'Inativo'?>
		</div>
		<br clear="all">
	</div>
	<br>

	<div div class="col-md-4" style="padding-left:0px;">
		<div class="evetDet barScroll"><span class="labelForms">Datas:</span> <br>
		<?= isset($evento->datas) ? $evento->stringDatas($evento->datas).'</div><br><a href="'.Url::to(['remover-datas','evento'=>$evento->codigo]).'" class="btn btn-primary btn-pagar bot-pagarAlin">Remover datas</a>' :'Nenhum</div>'?>
	</div>
	<div div class="col-md-4" style="padding-left:0px;">
		<div class="evetDet barScroll"><span class="labelForms">Tags:</span><br>
		<?= isset($evento->tags) ?\app\models\Tags::stringTags($evento->tags).'</div><br><a href="'.Url::to(['remover-tags','evento'=>$evento->codigo]).'" class="btn btn-primary btn-pagar bot-pagarAlin">Remover tags</a>'	 :'Nenhum</div>'?>
	</div>
	<div div class="col-md-4" style="padding-left:0px;">
		<div class="evetDet barScroll"><span class="labelForms">Anexos:</span><br> 
		<?= isset($evento->anexos) ?\app\models\Anexos::stringAnexos($evento->anexos).'</div><br><a href="'.Url::to(['remover-anexos','evento'=>$evento->codigo]).'" class="btn btn-primary btn-pagar bot-pagarAlin">Remover anexos</a>' :'Nenhum</div>'?>
	</div>
	<br clear="all">
	<br>
	<br>

	<div style="text-align:center;">
		<a href="<?=Url::to(['adicionar-tag','evento'=>$evento->codigo])?>">Adicionar tag</a> |
		<a href="<?=Url::to(['adicionar-anexo','evento'=>$evento->codigo])?>">Adicionar anexo</a> |
		<a href="<?=Url::to(['adicionar-data','evento'=>$evento->codigo])?>">Adicionar data</a> |
		<a href="<?=Url::to(['editar-evento','evento'=>$evento->codigo])?>">Editar evento</a> |	
		<a href="<?=Url::to(['excluir','evento'=>$evento->codigo])?>">Excluir evento</a>
	</div>
	<br>
	<br>
</div>

<?php
use \app\assets\LightboxAsset;
use \app\assets\SlickAsset;
use \app\models\Agenda;
use app\models\Tags;
use app\assets\CarrouselSimplesAsset; 

CarrouselSimplesAsset::register($this);
LightboxAsset::register($this);
SlickAsset::register($this);

?>

<div class="container">
    <div class="row">
        <div class="boxPeca">
            <div class="boxPeca2">
                <div class="col-md-8 semPad" style="background:none;">
                    <span class="title-line second-line" style="display:block; width:100%; font-size:30px;"><?= $evento->nome ?></span>
                </div>
                <div class="col-md-4 semPad" style="background:none;">
                    <div class="tableCirculo3">
                        <?php if (!empty($evento->sinopse)) { ?>
                        <div id="ico1" class="circulo3 fundoSvg1" title="Ficha Técnica"></div>
                        <?php } ?>
                        <?php if (!empty($evento->atores)) { ?>
                        <div id="ico2" class="circulo3 fundoSvg2" title="Elenco"></div>
                        <?php } ?>
                        <?php if (!empty($evento->premios)) { ?>
                        <div id="ico3" class="circulo3" title="Prêmios"><i class="material-icons">star</i></div>
                        <?php } ?>
                        <?php if (!empty($evento->duracao)) { ?>
                        <div id="ico4" class="circulo3" title="Duração"><i class="material-icons">access_time</i></div>
                        <?php } ?>
                        <?php if (!empty($evento->classificacao)) { ?>
                        <div id="ico5" class="circulo3" title="Classificação"><i class="material-icons">pan_tool</i></div>
                        <?php } ?>
                    </div>
                </div>
                <br clear="all">
            </div>
            <div class="col-md-5 semPad semMargin">
                <div class="imagemPeca" style="background-image: url(<?= isset($evento->anexos['imgEvento']) ? $evento->anexos['imgEvento'] : '' ?>); background-size:cover;"></div>
            </div>
            <div class="col-md-7 boxPrecos" style="margin:0px; padding:0px;">
                <?php if (!empty($evento->sinopse)) { ?>
                <div id="tela1" class="barScroll">
                    <h4 style="font-size:18px; margin-top:20px; text-align:center;">Ficha técnica</h4>
					<hr>
                    <p><strong>Sinopse:</strong><br><?= nl2br($evento->sinopse) ?></p>
                    <p><strong>Direção:</strong><br> <?= nl2br($evento->direcao) ?></p>
                </div>
                <?php } ?>
                <?php if (!empty($evento->atores)) { ?>
                <div id="tela2" class="boxPrecosHide barScroll">
					<h4 style="font-size:18px; margin-top:20px; text-align:center;">Elenco</h4>
					<hr>
                    <p><?= nl2br($evento->atores); ?></p>
                </div>
                <?php } ?>
                <?php if (!empty($evento->premios)) { ?>
                <div id="tela3" class="boxPrecosHide barScroll">
					<h4 style="font-size:18px; margin-top:20px; text-align:center;">Prêmios</h4>
					<hr>
                    <p><?= $evento->premios; ?></p>
                </div>
                <?php } ?>
                <?php if (!empty($evento->duracao)) { ?>
                <div id="tela4" class="boxPrecosHide barScroll">
                    <h4 style="font-size:18px; margin-top:20px; text-align:center;">Duração</h4>
					<hr>
                    <p>
                        <?php
                        $tempo=explode(':', $evento->duracao);
                        $horas = $tempo[0]*60;
                        $tempoEmMinutos=$horas+$tempo[1];
                        echo $tempoEmMinutos.' minutos';
                        ?>
                    </p>
                </div>
                <?php } ?>
                <?php if (!empty($evento->classificacao)) { ?>
                <div id="tela5" class="boxPrecosHide barScroll">
                    <h4 style="font-size:18px; margin-top:20px; text-align:center;">Classificação</h4>
					<hr>
                    <p><?= $evento->classificacao; ?></p>
                </div>
                <?php } ?>
            </div>
            <br clear="all">
        </div>

        <div class="row">
            <div class="mapaAssentos">
                <div class="blocoPrecoGeral" style="position:relative;">
                    <?php
                    if (isset($evento->datas) && count($evento->datas) > 0) {
                        $dias = [];
                        ?>
                        <!-- <div class="multiple-items"> -->
						<!-- aqui começa o carrossel das datas dos eventos, é usado o carrouselSimples.js -->
						<?php 
						if (count($horarios)>=7) { 
						?>
							<a href="#null" id="arrowL" class="slider-arrow2 sa-left2">&#xf104;</a>
						<?php
						}
						?>
						<div id="scrollbar" style="overflow:hidden; width:937px; height:110px; white-space: nowrap;">
							<div style="width:<?= count($horarios)*156 ?>px;">
								<?php
								foreach ($horarios as $dia => $h) {
								?>
								<div class="col-md-2 semPad semMargin dataEvento dataEvento-assento<?=$tipoAssento?>" style="width:156px; float:left; position:relative;" <?=count($h['horarios']) == 1 ?'data-dataevento="'.$h['horarios'][0]['id'].'"' :''?>>
									<div class="blocoPreco blocoPrecoNomal">
										<div class="blocoPreco-box">
											<p><?= Agenda::dataEventoFormato($dia, 'd/m/Y', 'EEEE') ?></p>
											<h3><?= Agenda::dataEventoFormato($dia, 'd/m/Y', "dd 'de' MMM") ?></h3>
											<?php
											if (count($h['horarios']) == 1)
												echo '<h4>' . Agenda::dataEventoFormato($dia . ' ' . $h['horarios'][0]['hora'], 'd/m/Y H:i:s', "HH'h'mm") . '</h4>';
											if (count($h['horarios']) > 1) {
												?>
												<select id="data-evento-ingressos" style="padding:0px 35px 0px 5px; outline:none;">
													<?php
													foreach ($h['horarios'] as $hora) {
														echo '<option value="' . $hora['id'] . '">' . Agenda::dataEventoFormato($dia . ' ' . $hora['hora'], 'd/m/Y H:i:s', "HH'h'mm") . '</option>';
													}
													?>                                        
												</select>
											<?php } ?>
										</div>
									</div>
									<span id="setaCent" class="setas blocoPrecoNomal hidden"></span>
								</div>
								<?php } ?>
							</div>
						</div>
						<?php 
						if (count($horarios)>=7) { 
						?>
							<a href="#null" id="arrowR" class="slider-arrow2 sa-right2">&#xf105;</a>
						<?php
							}
						?>
					<?php 
					} 
					?>
				</div>
            </div>
            <?php if($outroEvento->successo=='0') {?>
                <div class="row" style="padding: 0px 0px 0px 0px">
                    <div class="col-sm-12 semPad semMargin">
                        <div class="alert alert-warning" style="font-size:16px; text-align:center;">
                            <p><span class="font-awesome" style="color:#ffba00; font-size:30px;">&#xf071;</span> 
                            <?=$outroEvento->erro['mensagem'];?></p>
                            <p>&nbsp;</p>
                            <div class="row">
                                <p class="col-xs-6 text-right"><a class="btn btn-primary btn-lg" href="<?=\yii\helpers\Url::to(['/compra/concluir-compra'])?>">Concluir compra</a></p>
                                <p class="col-xs-6 text-left"><a class="btn btn-primary btn-lg" href="<?=\yii\helpers\Url::to(['/compra/nova-compra'])?>">Iniciar nova compra</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }
            echo $this->render('selecaoProdutosLivre', [
                'evento' => $evento,
                'carrinho' => $carrinho,
                'tags' => $tags
            ]);
            ?>
            <div class="col-md-12 semPad semMargin">
                <div class="blocoIngr3 tableCirculo">
                    <div style="display:table-cell; width:100%; vertical-align:middle; min-height:50%;">
                        <div class="circulo" title="Fotos do Teatro" data-toggle="modal" data-target="#myModalMapa2"><i class="material-icons"><i class="material-icons">camera_alt</i></i></div>
                        <div class="circulo" title="Como chegar" data-toggle="modal" data-target="#myModalMapa"><i class="material-icons">place</i></div>
                    </div>
                </div>		
            </div>
        </div>
        <br>
        <br>
    </div>
	</div>
</div>

<div id="myModalMapa" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mapa</h4>
            </div>
            <div class="modal-body">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15035.457827120901!2d-46.9311361!3d-19.5903068!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1d29823c54e10742!2sSENAI+CFP+Arax%C3%A1+Djalma+Guimar%C3%A3es!5e0!3m2!1sen!2sbr!4v1524146897662" width="100%" height="370" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="myModalMapa2" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Fotos do Teatro</h4>
            </div>
            <div class="modal-body">
                <div class="imagemPeca" style="background-image: url(<?= isset($evento->anexos['imgEvento']) ? $evento->anexos['imgEvento'] : '' ?>); background-size:cover;"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs('
    urlGetMapaAssento="' . \yii\helpers\Url::to(['mapa/mapa']) . '";
    urlGetProdutoData="' . \yii\helpers\Url::to(['produtos/buscar-js']) . '";
    tipoAssento="'.$tipoAssento.'";
    '
);

<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\Tags;

$this->title = 'ADM Easy for Pay' . (isset($matriz) && $matriz == true ? ' (matriz)' : ''); 
?>

<div class="container">
	<div class="row">
		<h2 class="tituloGeral"><i class="fa fa-clone"></i> Produtos - Consultar</h2>
	</div>
	
	<?php $form = ActiveForm::begin(['id' => 'produto-form']); ?>
	<?= $form->field($produto, 'matriz')->hiddenInput(['value' => $matriz])->label(false); ?>
	<?= $form->field($produto, 'id_agenda')->dropDownList(ArrayHelper::map($eventos, 'id', 'data', 'evento')) ?>
	<div class="form-group" style="text-align:center; margin-top:30px;">
		<?= Html::submitButton('Consultar', ['class' => 'btn btn-primary']) ?>
	</div>
	<?php ActiveForm::end(); ?>

	
	<div class="gridAgenda5Out">
		<div class="gridAgenda5 gridAgendaHead">
			<div>Nome</div>
			<div>Descrição</div>
			<div>Valor</div>
			<div>Desc.Fixo</div>
			<div>Desc.Perc.</div>
			<div>Status</div>
			<div>Tags</div>
		</div>
		<?php
		if (!$produto->hasErrors() && $produto->lista) {
			foreach ($produto->lista as $p) { 
		?>
			<div class="gridAgenda5">
				<div><h3><a href="<?=Url::to(['registro-produto','produto'=>$p->codigo])?>"><?=$p->nome?></a></h3></div>
				<div><p><?=$p->descricao?></p></div>
				<div><p>R$ <?=number_format($p->valor,2,'.',',')?></p></div>
				<div><p>Desconto fixo: <?=$p->descontoFixo?></p></div>
				<div><p>Desconto percentual: <?=$p->descontoPercentual?></p></div>
				<div><p>Status: <?=$p->ativo?></p></div>
				<div><p>Tags:<?= isset($p->tags) ? Tags::stringTags($p->tags) : 'nenhuma'; ?></p></div>
			</div>
			<?php
			}
		}
		?>
	</div>
</div>

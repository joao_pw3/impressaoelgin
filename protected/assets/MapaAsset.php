<?php
namespace app\assets;

use yii\web\AssetBundle;

class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
        'css/font-awesome.css',
        'css/site.css',
        'css/styles.css',
        'css/bootstrap.css',
        'css/normalize.min.css',
        'css/foundation-flex.css',
        'css/icones.css',
        'css/animate.min.css',
        'css/admStyle.css',
        'css/style_2.css',
    ];
    public $js = [
        'js/fontawesome.js',
        'js/wow.min.js',
        'js/app.js',
        'js/ajax.js',
        'js/compra.js',
        'js/konduto.js',
        'js/user_login.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    
    public $publishOptions = [
        'forceCopy' => true
    ];
}

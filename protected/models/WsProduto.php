<?php
namespace app\models;
use app\models\Ws;

/**
 * Classe para Produtos - Easy for Pay
 * obs.: parâmetros entre colchetes são opcionais
 */
class WsProduto extends Ws
{
    
    /**
     * Método construtor - obrigado a passar o token do vendedor para separação de unidades
    */
    public function __construct($tokenVendedor=''){
        if(empty($tokenVendedor))
            throw new \Exception("Token vendedor não foi declarado", 1);        
        $this->setToken($tokenVendedor,'V');
    }
    
    /**
     * Criar produto permitindo verificar a existência
     * @param type $post Dados do produto: nome, [descricao], valor, [desconto] ([fixo], [percentual]), [validade] ([inicial], [final]), ativo
     * @return object {'successo': '1', 'codigo': <id do produto criado>}
     */
    public function criarProduto($post=[])
    {
        return json_decode($this->call('produtos/', 'POST', $post));
    }
    
    /**
     * Alterar produto
     * @param type $post Dados do produto: codigo, [nome], [descricao], [valor], [desconto] ([fixo], [percentual]), [validade] ([inicial], [final]), ativo
     * @return object {'successo': '1', 'alterado': '1'}
     */
    public function alterarProduto($post=[])
    {
        return json_decode($this->call('produtos/' . $post['codigo'], 'PUT', $post));
    }
    
    /**
     * Alterar produto em lote
     * @param type $post Dados do produto: o array precisa de "filtro" e "alterar", obrigatoriamente
     *      "filtro": {
                "[codigo]: {N(11), N(11), N(11), ..., N(11)},
                "[nome]": "X(128)",
                "[descricao]": "X(128)",
                "[valor]": "F(9,2)",
                "[desconto]": {
                    "[fixo]": "F(9,2)",
                    "[percentual]": "F(9,2)"
                },
                "[ativo]":"N(1)",
                "[tags]": [
                    {
                        "[nome]": "X(128)",
                        "[valor]": "X(128)"
                    }
                ],
                "[anexos]":[
                    {
                        "[nome]": "X(128)",
                        "[tipo]": "X(128)"
                    }
                ],
                "[estoque]": "F(9,2)"
            },
            "alterar": {
                "[nome]": "X(128)",
                "[descricao]": "X(10240)",
                "[valor]": "F(9,2)",
                "[desconto]": {
                    "[fixo]": "F(9,2)",
                    "[percentual]": "F(9,2)"
                },
                "[validade]": {
                    "[inicial]": "DD/MM/AAAATHH:MM:SS",
                    "[final]": "DD/MM/AAAATHH:MM:SS"
                },
                "[ativo]":"N(1)"
            }
     * @return object {'successo': '1', 'alterado': '1'}
     */
    public function alterarProdutoEmLote($post=[])
    {
        return json_decode($this->call('produtos/', 'PUT', $post));
    }
    
    /**
     * Excluir produto
     * @param type $post Dados do produto: codigo
     * @return object {'successo': '1', 'removido': '1'}
     */
    public function excluirProduto($post=[])
    {
        return json_decode($this->call('produtos/' . $post['codigo'], 'DELETE'));
    }
    
    /**
     * Listar produto
     * @param json $post Parâmetros para busca
     * @param boolean $cache Buscar registros com ou sem cache
     * @return object {'successo': '1', 'produtos': {...}}
     */
    public function listarProdutos($post=null, $cache=true)
    {
        return json_decode($this->call('produtos/buscar' . ($cache == true ? '' : 'semcache') . '/', 'PUT',  $post));
    }
    
    /**
     * Consultar produto
     * @param integer $id ID de um produto
     * @param boolean $cache Buscar registros com ou sem cache
     * @param integer $registro Número do registro inicial para a busca (uso em paginação) - Sem $pagina, a busca retorna 600 registros
     * @return object {'successo': '1', 'produtos': {...}}
     */
    public function buscarProduto($id=null, $cache=true, $registro='')
    {
        $busca = $registro != '' ? ['registro_inicial' => $registro] : [];
        return json_decode($this->call('produtos/buscar' . ($cache == true ? '' : 'semcache') . '/' . $id, 'PUT', $busca));
    }
    
    /**
     * Adicionar tag para produto
     * @param integer $codigo Código do produto
     * @param type $post Dados da tag: nome, [valor]
     * @return object
     */
    public function adicionarTag($post)
    {
        return json_decode($this->call('produtos/' . $post['codigo'] . '/tag', 'POST', $post));
    }
    
    /**
     * Excluir tag para produto
     * @param type $post Dados da tag: nome
     * @return object
     */
    public function excluirTag($post=[])
    {
        return json_decode($this->call('produtos/' . $post['codigo'] . '/tag', 'DELETE', $post));
    }
    
    /**
     * Adicionar anexo para produto
     * @param type $post Dados de anexo: nome, tipo, objeto
     * @return object
     */
    public function adicionarAnexo($post=[])
    {
        return json_decode($this->call('produtos/' . $post['codigo'] . '/anexo', 'POST', $post));
    }

    /**
     * Excluir anexo para produto
     * @param type $post Dados de anexo: nome
     * @return object
     */
    public function excluirAnexo($post=[])
    {
        return json_decode($this->call('produtos/' . $post['codigo'] . '/anexo', 'DELETE', $post));
    }    
    
    /**
     * Adicionar estoque para produto
     * @param type $post Dados para estoque: unidades
     * @return object
     */
    public function alterarEstoque($post=[])
    {
        return json_decode($this->call('produtos/' . $post['codigo'] . '/estoque', 'POST', [
            'tid' => $post['tid'], 
            'unidades' => $post['unidades']
        ]));
    } 
    
    /**
     * Consultar estoque de produto
     * @return object {'successo': '1', 'data': 'DD/MM/AAAA HH:MM:SS', 'unidades': '+-F(9,2)'}
     */
    public function consultarEstoque($post=[])
    {
        return json_decode($this->call('produtos/' . $post['codigo'] . '/estoque', 'GET', $post));
    } 
    
    
    /**
     * Consultar totais de produto
     * @return object {'successo': '1', 'objeto': Array}
     */
    public function consultarTotaisEstoque($post=[])
    {
        return json_decode($this->call('produtos/buscar/0/totais', 'PUT', $post));
    } 
    
    /**
     * Clonagem de produto por tag
     * @param array $post Array de informações: tag, nova_tag
     * @return object {'successo': '1', 'produtos_clonados': 'N(11)'}
     */
    public function clonarProduto($post=[])
    {
        return json_decode($this->call('produtos/clonar/' . $post['tag'] . '/' . $post['nova_tag'], 'PUT', $post));
    } 
    
    /**
     * Clonagem de produto por id de agenda
     * @param array $post Array de informações: de, para
     * @return object {'successo': '1', 'produtos_clonados': 'N(11)'}
     */
    public function clonarProdutoAgenda($post=[])
    {
        return json_decode($this->call('produtos/clonaragenda/' . $post['de'] . '/' . $post['para'], 'PUT', $post));
    }
            
}
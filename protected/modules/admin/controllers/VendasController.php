<?php
namespace app\modules\admin\controllers;

use Yii;
use yii\web\Response;
use app\modules\admin\models\Transacoes;
use app\models\EventosModel;
use app\models\CompraModel;
use app\models\ApiCarrinho;
use app\models\ApiProduto;
use app\models\ApiCliente;
use app\models\ApiAgenda;
use app\modules\admin\controllers\AbstractController;
use yii\helpers\Url;
use SimpleXMLElement;

/**
 * Controller de vendas.
 */
class VendasController extends AbstractController
{
    public $area;
    public $setor;
    public $assento;
        
    /**
     * Relatório de vendas. Trazer as vendas com taxas e cálculo de datas
    */
    public function actionIndex(){
        $firstDay=new \DateTime('today');
        $lastDay=new \DateTime('today');
        $lastDay->setTime(23,59,59);
        $dataDe = $firstDay->format('d/m/Y H:i:s');
        $dataAte = $lastDay->format('d/m/Y H:i:s');
        $post=Yii::$app->request->post();
        if($post){
            $dataDe=$post['de'].' 00:00:00';
            $dataAte=$post['ate'].' 23:59:59';
        }
        $transacoes = new Transacoes();
        $transacoes->dataInicial = \DateTime::createFromFormat('d/m/Y H:i:s',$dataDe);
        $transacoes->dataFinal =  \DateTime::createFromFormat('d/m/Y H:i:s',$dataAte);
        $transacoes->transacoesPeriodo(true);
        $session = Yii::$app->session;
        $session->open();
        $session['admVendasTransacoes']=$transacoes->transacoes;
        //<atualização>
        //return $this->render('//site/atualizando-servidor');
        //</atualização>
        return $this->render('index',[
            'de'=>$transacoes->dataInicial,
            'ate'=>$transacoes->dataFinal,
            'transacoes'=>$transacoes->transacoes,
            'total'=>count($transacoes->transacoes)
        ]);
    }

    /**
     * Exportar para CSV com informações adicionais - produtos da compra e se é usuário novo
    */
    public function actionExportarCsv(){
        $session = Yii::$app->session;
        $session->open();        
        if($session['admVendasTransacoes']==null) exit('Nenhum dado foi enviado');
        $registros=$session['admVendasTransacoes'];
        ini_set('max_execution_time', 300);
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=vendas-site-mineirao_'.date('d-m-Y_H-i-s').'.csv');
        $output = fopen('php://output', 'w');
        fputcsv($output, ['Data da compra','Nome completo','CPF',utf8_decode('Endereço completo'),'Cidade','Estado','Bairro','CEP','Telefone de contato','E-mail','Nome do produto vendido','Valor','Forma de pagamento','Bandeira',utf8_decode('Conciliação'),utf8_decode('Condições de pagamento'),'Taxa','Primeira compra'],';');
        foreach($registros as $r){
            if($r['codigo_status']!=1) continue;
            $venda=\app\modules\admin\models\Transacoes::dadosVenda($r);
            foreach($venda as $linha)
                fputcsv($output, $linha,';');
        }
        die();
    }

    public function actionBordero()
    {
        $transacoes = new Transacoes();
        
        /* PARA O JOGO DE 17/01/2018 */
//        $transacoes->dataInicial = \DateTime::createFromFormat('d/m/Y H:i:s','01/11/2017 00:00:00');
//        $transacoes->dataFinal =  \DateTime::createFromFormat('d/m/Y H:i:s','17/01/2018 22:00:00');
        
        /* PARA O JOGO DE 24/01/2018 */
        $transacoes->dataInicial = \DateTime::createFromFormat('d/m/Y H:i:s','17/01/2018 22:00:01');
        $transacoes->dataFinal =  \DateTime::createFromFormat('d/m/Y H:i:s', '24/01/2018 22:00:00');
        
        $transacoes->transacoesPeriodo();
        $somaQtdeTribunas=0;
        $somaValoresTribunas=0;
        $somaQtdeEmbaixadas=0;
        $somaValoresEmbaixadas=0;
        foreach($transacoes->transacoes as $r){
            $venda=\app\modules\admin\models\Transacoes::dadosVendaBruto($r);
            foreach($venda as $linha){
                $produto=$linha[0];
                if(strpos($produto, 'Cadeira')>-1){
                    $somaQtdeTribunas+=1;
                    $somaValoresTribunas+=$linha[1];
                } else {
                    $somaQtdeEmbaixadas+=1;
                    $somaValoresEmbaixadas+=$linha[1];
                }
            }
            
        }
        return $this->render('bordero',[
            'somaQtdeTribunas'=>$somaQtdeTribunas,
            'somaValoresTribunas'=>$somaValoresTribunas,
            'somaQtdeEmbaixadas'=>$somaQtdeEmbaixadas,
            'somaValoresEmbaixadas'=>$somaValoresEmbaixadas,
        ]);
    }
    
    /**
     * Página principal para vendas avulsas - disponíveis apenas para super-admin
     */
    public function actionReserva() 
    {
        try {
            $modelEventos = new EventosModel();
            $eventos = $modelEventos->listaEventos(['ativo' => '1']);
            return $this->render('reserva', [
                'eventos' => $eventos
            ]);
            
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro listando eventos disponíveis', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Traz as informações de um evento, área selecionada e blocos disponíveis
     * @param int $id o ID do evento na base de dados
     * @param string $area o nome da área selecionada
     * @param string $setor o setor ou camarote selecionado
     * @param string $assento o assento selecionado
     * @return Response
     */
    public function actionEstadio($id, $area, $setor=null, $assento=null){
        try {
            $modelEvento = new EventosModel();
            $this->area = $area;
            $this->setor = $setor;
            $this->assento = $assento;
            $evento = $modelEvento->infoEventoComLugares($id, $area);
            $blocos = (new ApiProduto())->buscarProdutoTags(json_encode(['tags' => 
                [
                    ['nome' => 'area', 'valor' => $area],
                    ['nome' => 'tipo', 'valor' => 'bloco'],
                    ['nome' => 'matriz', 'valor' => $evento['evento']->tags['apelido']],
                ]
            ]), NULL, false);
            return $this->render('estadio', [
                'evento' => $evento['evento'], 
                'setores' => isset($evento['jsonAssentos']->objeto) ? $evento['jsonAssentos']->objeto : '',
                'compra' => new CompraModel,
                'carrinho' => (new ApiCarrinho)->criarCarrinho(),
                'blocos' => $blocos
            ]);
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro buscando informações de blocos', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Traz as informações de um evento, área selecionada e blocos disponíveis
     * @param int $id o ID do evento na base de dados
     * @param string $area o nome da área selecionada
     * @param string $setor o setor ou camarote selecionado
     * @param string $assento o assento selecionado
     * @return Response
     */
    public function actionEstadioTeste($id, $area, $setor=null, $assento=null){
        try {
            $modelEvento = new EventosModel();
            $this->area = $area;
            $this->setor = $setor;
            $this->assento = $assento;
            $evento = $modelEvento->infoEventoComLugares($id, $area);
            $blocos = (new ApiProduto())->buscarProdutoTags(json_encode(['tags' => 
                [
                    ['nome' => 'area', 'valor' => $area],
                    ['nome' => 'tipo', 'valor' => 'bloco'],
                    ['nome' => 'matriz', 'valor' => $evento['evento']->tags['apelido']],
                ]
            ]), NULL, false);
            return $this->render('estadioTeste', [
                'evento' => $evento['evento'], 
                'setores' => isset($evento['jsonAssentos']->objeto) ? $evento['jsonAssentos']->objeto : '',
                'compra' => new CompraModel,
                'carrinho' => (new ApiCarrinho)->criarCarrinho(),
                'blocos' => $blocos
            ]);
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro buscando informações de blocos', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Busca de produtos para fazer reserva
     * @param array $tags array de objetos nome:valor das tags para filtrar
     * @return json informações do produto
    */
    public function actionProdutoPorTag()
    {
        $model = new ApiProduto;
        $tagsBusca = [];
        $tagsPost = $_POST['post'];
        if ($tagsPost) {
            foreach ($tagsPost as $key => $value) {
                $tagsBusca[$key] = $value;
            }
        }
        try {
            $produtos = $model->buscarProdutoTags(json_encode(['tags' => $tagsBusca]), NULL, false);
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $produtos;
            }
            return $this->render('/site/listaProdutos', [
                'produtos' => $produtos
            ]);
        } catch (Exception $e){
            return $this->render('error', ['name' => 'Erro ao consultar produto - admin', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Mudar status de produto - Se produto estiver ativo, bloqueia / Se produto estiver bloqueado, ativa
     */
    public function actionMudarStatus ()
    {
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                $post = Yii::$app->request->post();
                $returnApi = (new ApiProduto)->alterarProduto([
                    'codigo' => $post['codigo'],
                    'ativo' => $post['status'] == 1 ? 0 : 1
                ]);
                if ($returnApi->successo == '1') {
                    $teste = (new ApiProduto)->adicionarTagProduto([
                        'codigo' => $post['codigo'],
                        'nome' => ($post['status'] == 1 ? 'bloqueio' : 'desbloqueio') . '_' . date('YmdHis'),
                        'valor' => $post['justificativa']
                    ]);
                }
                return $returnApi;
            }
            return (new ApiProduto)->getApiError('Faltam parâmetros para alterar status do produto');
        } catch (Exception $e){
            return $this->render('error', ['name' => 'Erro ao mudar status de produto - admin', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Reservar produto para um cliente
     */
    public function actionReservarProduto ()
    {
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                $post = Yii::$app->request->post();
                $dtReserva = date('YmdHis');
                $returnApi = (new ApiProduto)->adicionarTagProduto([
                    'codigo' => $post['codigo'],
                    'nome' => ($post['documento'] == '' ? 'reservar' : 'liberar') . '_' . $dtReserva,
                    'valor' => $post['reserva-documento']
                ]);          
                if ($returnApi->successo == '1') {
                    return (new ApiProduto)->adicionarTagProduto([
                        'codigo' => $post['codigo'],
                        'nome' => ($post['documento'] == '' ? 'reservarjust' : 'liberarjust') . '_' . $dtReserva,
                        'valor' => $post['justificativa']
                    ]); 
                }
                return $returnApi;
            }
            return (new ApiProduto)->getApiError('Faltam parâmetros para reservar produto');
        } catch (Exception $e){
            return $this->render('error', ['name' => 'Erro ao reservar produto - admin', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Iniciar venda conciliada
     */
    public function actionConciliar() 
    {
        try {
            $modelEventos = new EventosModel();
            $eventos = $modelEventos->listaEventos(['ativo' => '1']);
            return $this->render('conciliar', [
                'eventos' => $eventos
            ]);
            
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro listando eventos disponíveis', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Checar se documento refere-se a um cliente cadastrado
     */
    public function actionChecarDocumento ()
    {
        try {
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return (new ApiCliente)->autenticarCliente(Yii::$app->request->post(), true);
            }
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro checando documento de cliente', 'message' => $e->getMessage()]);
        }
    }

}

var loadImage = false;

$(document).ready(function () {

    /*
     CRIAÇÃO MENUS
     ------------------------------*/
    $('#main-menu li.current-menu-parent li.level-1, #main-menu li.current-menu-ancestor li.level-1, #main-menu li.current-menu-item li.level-1').each(function () {
        classes = $(this).attr('class');
        a = $(this).find('a.level-1').clone();
        div = $("<div class='small-12 medium-expand columns " + classes + "'></div>").html(a);
        $('#menu-2nd-level').append(div);
    });

    $('#main-menu li.current-menu-parent li.level-2, #main-menu li.current-menu-ancestor li.level-2, #main-menu li.current-menu-item li.level-2').each(function () {
        classes = $(this).attr('class');
        a = $(this).find('a.level-2').clone();
        div = $("<div class='small-12 medium-expand columns " + classes + "'></div>").html(a);
        $('#menu-3nd-level').append(div);
    });

    $(".swipebox").swipebox();

    var owl_slide = $(".slide");
    owl_slide.owlCarousel({
        singleItem: true,
        //height : 300,
    })
    $(".slide-next").click(function () {
        owl_slide.trigger('owl.next');
    });
    $(".slide-prev").click(function () {
        owl_slide.trigger('owl.prev');
    });


});

function shareFacebook(url, title, description, picture) {

    var facebook_appID = '266905050323834'
    url = "https://www.facebook.com/dialog/feed?"
            + "app_id=" + facebook_appID
            + "&link=" + url
            + "&name=" + title
            + "&description=" + description
            + "&picture=" + picture
            + "&redirect_uri=https://www.facebook.com";
    window.open(url, null, "height=200,width=600,status=yes,toolbar=no,menubar=no,location=no");

}
;

function getParameterByName(name, url) {
    if (!url)
        url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

window.fbAsyncInit = function () {
    FB.init({
        appId: '266905050323834',
        cookie: true, // This is important, it's not enabled by default
        version: 'v2.3'
    });
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function loginFB(method) {

    params = arguments;

    FB.login(function (e) {
        e.authResponse ? FB.api("/me", function (e) {
            $.ajax({
                url: ajaxurl,
                method: 'post',
                data: {
                    action: 'facebook_login_callback',
                },
                xhrFields: {
                    withCredentials: true
                }
            }).success(function (data) {
                console.log("User logged.");
                method && method(params[1], params[2]);
            }).error()
        }) : console.log("User cancelled login or did not fully authorize.")
    }, {
        scope: "email"
    });


}

function interactions(step) {
    $('.conte-sua-historia').hide();
    $('.conte-sua-historia.passo-' + step).show();
    //$('.conte-sua-historia').fadeOut(function() {
    //    $('.conte-sua-historia.passo-' + step).fadeIn();
    //});
}

function sendAddHistory(form) {

    if ($('textarea[name=historia]').val().length == 0) {
        $('.errors').show();
        return;
    }

    $('.errors').hide();

    var data = form.serializeArray();
    data.push({
        name: 'action',
        value: 'add_history'
    });

    $('.conte-sua-historia').hide();
    $('.conte-sua-historia.carregando').show();

    $.ajax({
        url: ajaxurl,
        method: 'post',
        data: data,
        xhrFields: {
            withCredentials: true
        }
    }).success(function (data) {
        $.ajax({
            url: ajaxurl,
            method: 'post',
            data: {
                action: 'histories',
                event_id: $('#event_id').val()
            }
        }).success(function (data) {
            $('#historias').html(data);
            $('#history_photo_url').val('');
            $('#history_youtube_url').val('');
            $('.prev-foto').hide();
            $('.prev-video').hide();
            $('.escolha-foto-video').show();
            $('textarea[name=historia]').val('');
            interactions(1);
        });
    });

}

function went(event_id) {

    $.ajax({
        url: ajaxurl,
        method: 'post',
        data: {
            action: 'events_went',
            event_id: event_id
        },
        xhrFields: {
            withCredentials: true
        }
    }).success(function (data) {
        data = $.parseJSON(data);
        data.went ? $('.bt-fixa.fui').addClass('active') : $('.bt-fixa.fui').removeClass('active');
        $('.numero-foram').html(data.went_count == 1 ? '1 pessoa confirmou presença' : data.went_count + ' pessoas confirmaram presença');
    });

}

function loadImageFacebook() {

    loadImage || FB.login(function (t) {
        t.authResponse && FB.api('/me/albums', function (e) {
            albumsCarousel = $('#albumsFacebook');
            $.each(e.data, function (e, n) {
                div = $('<div/>', {
                    id: 'photo-' + n.id,
                    onclick: "listPhotos('" + n.id + "'); interactions(4);",
                    'class': 'foto item'
                });
                figure = $('<figure/>').appendTo(div);
                elementAlbum = $('<img/>', {
                    src: 'https://graph.facebook.com/' + n.id + '/picture?access_token=' + t.authResponse.accessToken
                }).appendTo(figure);
                div.append('<h5>' + n.name + '</h5>');
                albumsCarousel.append(div);
                loadImage = true;
            });
            albumsCarousel.owlCarousel({
                items: 4,
                itemsDesktop: [1000, 4],
                itemsDesktopSmall: [900, 3],
                itemsTablet: [600, 2],
            });
        })
    }, {
        scope: 'user_photos'
    });

}

function listPhotos(albumId) {

    $("#imagesFacebook").html("");

    FB.api("/" + albumId + "/photos?limit=500&fields=images", function (images_data) {

        $("<div/>", {
            id: "fbPhotos"
        }).appendTo("#imagesFacebook");

        photosCarousel = $('#fbPhotos');

        $.each(images_data.data, function (i, image_data) {

            if ("undefined" != typeof image_data.images[1]) {
                image_url = image_data.images[1].source;
                div = $('<div/>', {
                    id: 'photo-' + image_data.id,
                    onclick: "addPhotoFacebook('" + image_data.id + "', '" + image_url + "'); interactions(2);",
                    class: 'foto item'
                });
                figure = $('<figure/>').appendTo(div);
                elementAlbum = $('<img/>', {
                    src: image_url
                }).appendTo(figure);
                photosCarousel.append(div);
            }

        });

        photosCarousel.owlCarousel({
            items: 4,
            itemsDesktop: [1000, 4],
            itemsDesktopSmall: [900, 3],
            itemsTablet: [600, 2],
        });

    });

}

function addPhotoFacebook(image_id, image_url) {
    $("#history_youtube_url").val("");
    $(".escolha-foto-video").hide();
    $(".prev-video").hide();
    $(".prev-foto").show();
    $("#history_photo_url").val(image_url);
    $(".prev-foto img").attr('src', image_url);
    $(".photo-" + image_id).addClass("selected");
}

function getYoutubeId() {
    var youtube_url = $("#youtube_url_input").val();
    regex = youtube_url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
    if (regex != null) {
        $(".video-error").hide();
        $("#history_photo_url").val("");
        $(".escolha-foto-video").hide();
        $(".prev-foto").hide();
        $(".prev-video").show();
        $(".prev-video img").attr('src', "http://img.youtube.com/vi/" + regex[1] + "/0.jpg");
        $("#history_youtube_url").val(regex[1]);
        interactions(2);
    } else {
        alert('nok');
        $(".video-error").show();
    }
}

function reload() {
    window.location.reload()
}

function loadHistory(history_id) {

    $.ajax({
        url: ajaxurl,
        method: 'post',
        data: {
            action: 'history',
            history_id: history_id
        }
    }).success(function (data) {
        $('#historias').hide();
        $('#historia').html(data);
    });

}

function closeHistory() {
    $('#historias').show();
    $('#historia').html('');
}
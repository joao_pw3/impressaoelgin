<?php
namespace app\widgets;

use Yii;
use \yii\bootstrap\Widget;
use app\models\Cliente;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/**
 * Widget para alterar a unidade atual
 * Dentro de ADM permite alternar somente se o usuário for o da matriz
 * Na aplicação pública, alerna em qualquer lugar do site
 */
class SelectUnidades extends Widget
{
    public $modulo; //adm ou cliente
    public $alternarUnidade; // boolean para alternar a unidade atual na sessão
    private $unidade; // info da unidade com base no token atual em sessão
    private $token; // token atual em sessão
    public $options;
    public $id;
    public $action;
    public $documento_unidade;

    public function init(){
        parent::init();
        $session = Yii::$app->session;
        $this->token=$session->get('token_unidade',false);
        $this->alternarUnidade=true;
        $session->set('podeAlternarUnidade',$this->alternarUnidade);
        $this->id='selectUnidades';

        // guardar o documento da unidade atual para troca pelo cliente
        $this->documento_unidade = $session->get('documento_unidade',false);
        
        // Definir atributos do select para exibir 
        if(!isset($this->options))
            $this->options=['id' => $this->id,'prompt'=>'Escolha uma unidade'];
    }

    public function run(){
        $session = Yii::$app->session;
        if(!$this->token) return '';
        $objApiCliente = new Cliente();

        if(!$this->alternarUnidade)
            return $objApiCliente->nome;
        
        if($this->alternarUnidade)        
            $objApiCliente->unidadeAtual($objApiCliente->buscarUnidadePor('user_token', $this->token));
        
        // para o cliente, caso documento_unidade esteja vazio não usar a unidade matiz como se tivesse selecionada
        $selecaoMatriz=$session->get('selecao_matriz',false);
        if($this->modulo=='cliente' && $objApiCliente->user_token==$objApiCliente->getTokenMatriz() && !$selecaoMatriz)
            $objApiCliente->resetUnidade();
        
        $listaUnidadesSelect=[];
        if($this->modulo=='cliente')
            $listaUnidadesSelect[]=['documento'=>'rede','nome'=>'Toda a rede'];
        foreach ($objApiCliente->unidades as $unidade) {
            $listaUnidadesSelect[] = ['documento' => $unidade->documento, 'nome' => $unidade->nome];
        }

        ob_start();
            $form = ActiveForm::begin(['action' => $this->action]);
            echo $form->field($objApiCliente, 'documento')->dropDownList(ArrayHelper::map($listaUnidadesSelect, 'documento', 'nome'), $this->options)->label(false);
            ActiveForm::end();
            $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
}

<?php 


?>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/devel.css">
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/devel.js"></script>
    </head>
    <body>
        <form name="form-grid">
            <div class="col-xs-4">
                <div class="row">
                    <div class="col-xs-4"><label for="nome">Nome:</label></div>
                    <div class="col-xs-6"><input type="text" name="nome" id="nome" class="form-control"></div>
                </div>                
                <div class="row">
                    <div class="col-xs-4"><label for="colunas">Colunas:</label></div>
                    <div class="col-xs-6"><input type="text" name="colunas" id="colunas" class="form-control"></div>
                </div>
                <div class="row">
                    <div class="col-xs-4"><label for="colunas">Linhas:</label></div>
                    <div class="col-xs-6"><input type="text" name="linhas" id="linhas" class="form-control"></div>
                </div>
                <div class="row">
                    <div class="col-xs-4"><label for="colunas">Fileiras:</label></div>
                    <div class="col-xs-6">
                        <input type="radio" name="fileira" id="fileira" value="cima" checked> de cima para baixo<br> 
                        <input type="radio" name="fileira" id="fileira" value="baixo"> da baixo para cima
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4"><label for="colunas">Palco / Campo:</label></div>
                    <div class="col-xs-6">
                        <input type="radio" name="palco" id="palco" value="embaixo" checked> embaixo<br>
                        <input type="radio" name="palco" id="palco" value="cima"> em cima<br>
                        <label for="palco">Texto: </label>
                        <input type="text" name="texto" id="texto" value="" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-5" align="center"><input type="button" id="btn-grid" value="Gerar grid"></div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="row">
                    <div class="col-xs-4"><label for="colunas">Números dos assentos:</label></div>
                    <div class="col-xs-6">
                        <input type="radio" name="numeracao" id="numeracao" value="esquerda" checked> da esquerda para direita<br> 
                        <input type="radio" name="numeracao" id="numeracao" value="direita"> da direita para esquerda
                    </div>
                </div>
                <div class="row" id="">
                    <div class="col-xs-7" align="center"><input type="button" id="btn-assentos" value="Gerar assentos"></div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="row">
                    <a href="javascript:void(0)" id="btn-exportar"><button>Salvar mapas</button></a>
                    <a href="javascript:void(0)" id="btn-exportar-2" style="display: none"><button>Salvar mapa</button></a>
                </div>
            </div>
        </form>
        <hr>
        <p>&nbsp;</p>
        <div class="col-xs-12" id="grid"></div>
    </body>
</html>
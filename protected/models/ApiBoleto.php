<?php
namespace app\models;
use app\models\Api;
use yii\base\Exception;

/**
 * Classe geral de acesso à API Boleto
 */
class ApiBoleto extends Api
{
    public $url = YII_ENV_DEV ? 'https://www.zpointz.com.br/sandbox/boleto/v1/' : 'https://www.zpointz.com.br/api/boleto/v1/';
}
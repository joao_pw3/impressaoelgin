<?php
use yii\helpers\Url;
?>

    <div class="content" id="cli-cartoes" style="margin-top:20px;">
        
        <input type="hidden" id="url-cartao-favorito" value="<?= Url::to(['site/cartao-favorito']); ?>">
        <input type="hidden" id="url-cartao-excluir" value="<?= Url::to(['site/cartao-excluir']); ?>">
        
        <div class="row cli-cartao">
            
        <?php if (isset($cartoesApi->objeto) && !empty($cartoesApi->objeto)) {
            foreach($cartoesApi->objeto as $index => $cartao) {
                if ($index != 0 && $index % 4 == 0) { ?>
        </div>
        <div class="row cli-cartao">
            <?php } ?>
            <div class="col-md-12 separaCard">
                <div class="comoTabela">
                    <div><?= !empty($cartao->bandeira) ? '<img src="' . Yii::getAlias('@web') . '/images/' . $cartao->bandeira . '.png" width="50">' : '-'; ?></div>
                    <div>Final: <?= $cartao->ultimos_digitos; ?></div>
                    <div><i class="fa <?= $cartao->favorito == 1 ? 'fa-heart plus-green' : 'fa-heart-o'; ?> cartao-favorito" title="<?= $cartao->favorito == 1 ? 'Cartão Favorito' : 'Clique para tornar este cartão o seu favorito'; ?>" data-cartao="<?= $cartao->codigo_cartao; ?>"></i></div>
                    <div><i class="fa fa-trash cartao-excluir" title="Clique para remover este cartão" data-cartao="<?= $cartao->codigo_cartao; ?>"></i></div>
                </div>
            </div>
        <?php }
        } ?>
        </div>
    </div>

<?php 
$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/compra.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);

$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/carrinho.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/cadastro.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset', 'yii\bootstrap\BootstrapPluginAsset']]);

if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    $this->registerJs("deadline = setDataExpira('" . $session['tempoCarrinho'] . "'); initializeClock();");
}
?>
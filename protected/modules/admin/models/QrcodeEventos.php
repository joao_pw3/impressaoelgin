<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "qrcode_eventos".
 *
 * @property integer $id
 * @property integer $idqrcode
 * @property integer $evento
 * @property string $datageracao
 * @property string $dataativacao
 * @property string $cliente
 * @property integer $produto
 * @property string $lote
 * @property string $portao
 * @property string $ingresso
 * @property integer $vip
 * @property integer $camarote
 * @property string $bloco
 * @property string $fila
 * @property integer $assento
 * @property string $formato
 * @property string $tipo
 * @property integer $valor
 * @property integer $desconto
 */
class QrcodeEventos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'qrcode_eventos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idqrcode', 'evento', 'lote', 'datageracao'], 'required'],
            [['idqrcode', 'evento', 'produto', 'vip', 'camarote', 'assento', 'valor', 'desconto'], 'integer'],
            [['cliente', 'dataativacao', 'lote', 'portao', 'ingresso', 'bloco', 'fila', 'formato', 'tipo', 'valor', 'desconto'], 'safe'],
            [['lote', 'ingresso'], 'string', 'max' => 20],
            [['cliente'], 'string', 'max' => 50],
            [['portao'], 'string', 'max' => 5],
            [['bloco'], 'string', 'max' => 4],
            [['fila'], 'string', 'max' => 2],
            [['formato', 'tipo'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idqrcode' => 'Idqrcode',
            'evento' => 'Evento',
            'datageracao' => 'Datageracao',
            'dataativacao' => 'Dataativacao',
            'cliente' => 'Cliente',
            'produto' => 'Produto',
            'lote' => 'Lote',
            'portao' => 'Portao',
            'ingresso' => 'Ingresso',
            'vip' => 'Vip',
            'camarote' => 'Camarote',
            'bloco' => 'Bloco',
            'fila' => 'Fila',
            'assento' => 'Assento',
            'formato' => 'Formato',
            'tipo' => 'Tipo',
            'valor' => 'Valor',
            'desconto' => 'Desconto',
        ];
    }

    /**
     * Verificar se ingresso consta na tabela para poder ser validado
     * @throws Exception se houver um erro durante a execução - mais de um ingresso na tabela (crítico)
     * @return mixed bool successo, object resultado da busca ou string de mensagem em caso de erro
     * @todo Validar lote
     * @todo Gravar logs das ações
     * @todo Reportar casos mais críticos por e-mail
    */
    public static function verificarIngresso($post){
        $verifica=self::find()->where(['ingresso'=>$post['QrcodeEventos']['ingresso']])->all();
        if($verifica==null)
            return (object)['successo' => '0', 'erro' => ['mensagem'=>'Ingresso não encontrado']];
        if(count($verifica)>1)
            throw new \Exception('Ingresso com repetição!',1);
        $verifica=$verifica[0];
        if($verifica->cliente!=null)
            return (object)['successo' => '0', 'erro' => ['mensagem'=>'Este ingresso já foi ativado - '.date('d/m/Y H:i:s',$verifica->dataativacao).' para o documento '.$verifica->cliente]];
        return (object)['successo'=>'1', 'objeto'=>$verifica];
    }
}

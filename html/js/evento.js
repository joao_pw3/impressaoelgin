var urlGetMapaAssento;
var urlGetProdutoData;
var codigo;
var selecionados = new Array();
var idDataEventoAtual;
var tipoAssento;

$(document).ready(function () {

    $('.multiple-items').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        nextArrow: '<span style="color:black;" class="font-awesome">&#xf190;</span>',
        prevArrow: '<span style="color:black;" class="font-awesome">&#xf18e;</span>',        
    });

    resetEventos();
    marcarSelecionados();    
    adicionarOuvinteClickAssento();    
    
    btnAssentos();
    if($("#qtdeIngressos").length > 0)
        $("#qtdeIngressos").change(function(){btnAssentos()})

    $('#btn-assentos').click(function () {
        if(tipoAssento=='livre'){
            //$(this).button('loading');
            doAjax(urlGetProdutoData, 'POST', {'filtro':{'id_agenda':idDataEventoAtual}}, 'JSON', 10000, 'getProdutoOK', 'getProdutoErro')
        }
        if(tipoAssento=='marcado')
            window.location.href = $('#url-ocupantes').val() + '?evento=' + $('#evento').val() + '&data=' + $('#data').val();
    });
    
    $(".dataEvento .data-evento-ingressos").change(function(){
        idDataEventoAtual=$(this).val()
        mudarIdDataBotao();
        if($(this).hasClass('dataEvento-assentomarcado')){
            carregaMapaEvento();
            mostrarSelecionadosDoDia();
        }
    });
    
    $('.dataEvento').click(function () {
        if($(this).data('dataevento')!=undefined){
            idDataEventoAtual=$(this).data('dataevento');
            mudarIdDataBotao();
            if($(this).hasClass('dataEvento-assentomarcado')){
                carregaMapaEvento();
                mostrarSelecionadosDoDia();
            }
        }
    });

    //mostrar qtd de produtos no carrinho
    idDataEventoAtual=$('.dataEvento:first-child').data('dataevento');
    qtdeProdutosDataEvento();

    // funções usando regra de nomeação - NÃO ALTERAR SE NÃO SOUBER O QUE ESTÁ FAZENDO!!
    var primeiroDiaEvento=$('.dataEvento:first')
    $(primeiroDiaEvento).find('.setas').addClass('ativa blocoPrecoAtive').removeClass('hidden');
    $(primeiroDiaEvento).find('.blocoPreco').addClass('ativa blocoPrecoAtive');

    if($(primeiroDiaEvento).find("#data-evento-ingressos").length>0)
        idDataEventoAtual=$(primeiroDiaEvento).find("#data-evento-ingressos").val();
    
    if($(primeiroDiaEvento).data('dataevento')!=undefined)
        idDataEventoAtual=$(primeiroDiaEvento).data('dataevento');

    mostrarSelecionadosDoDia();
    atualizarQtdeNoCarrinho();
});

function marcarSelecionados(){
     $('.selecionado').each(function() {
        var codigo = $(this).attr('id').replace('div-', '');
        $('.circle-' + codigo).addClass('fillOrange').removeClass('fillRed').addClass('minhaSelecao');
        if ($.inArray(codigo, selecionados) == -1) {
            selecionados.push(codigo);
        }
    });
}

function mostrarSelecionadosDoDia(){
    $("#assentos-selecionados .selecionado").hide();
    $("#assentos-selecionados .idData-"+idDataEventoAtual).show();
}

function adicionarOuvinteClickAssento(){
    $('.assento').on('click', function () {
        codigo = $(this).attr('id');
        produto = $('#' + codigo).attr('data-produto');
        if (!$('#' + codigo + ' circle').hasClass('fillGray') && !$('#' + codigo + ' circle').hasClass('minhaSelecao')) {
            showMsg('Este assento está indisponível. Por favor, selecione outro.');
            return false;
        }
        if ($('#' + codigo + ' circle').hasClass('fillOrange') && $.inArray(codigo, selecionados) != -1) {
            selecionados.splice($.inArray(codigo, selecionados), 1);
            doAjax ($('#url-del-produto').val(), 'POST', {'produto_codigo': codigo,'produto_unidades':'0'}, 'JSON', 10000, 'delProdutoOK', 'delProdutoErr');
        } else if ($.inArray(codigo, selecionados) == -1){
            selecionados.push(codigo);
            doAjax ($('#url-add-produto').val(), 'POST', {'produto_codigo': codigo}, 'JSON', 10000, 'addProdutoOK', 'addProdutoErr');
        }
        btnAssentos(); 
    });
}

function resetEventos(){
     $('.assento').off('click');
}

function estoqueOK (data) {
    window.location.href = $('#url-ingresso').val() + '?evento=' + $('#evento').val() + '&data=' + $('#data').val();
}

function estoqueErr (data) {
    showMsg('Falha ao selecionar produtos: ' + data.erro.mensagem);
}

function carregaMapaEvento(){ 
    $("#holder-mapa > svg").remove();
    resetEventos();
    $('.div-loading').show();
    doAjax (urlGetMapaAssento+'?id_data_evento='+idDataEventoAtual, 'GET', {}, 'JSON', 30000, 'mapaOk', 'mapaErr');
        return false;
}

function mapaOk(data){
    $("#holder-mapa").append(data.objeto);
    acaoAposCarregarMapa();
    $('.div-loading').hide();
}

function mapaErr(data){
    $("#holder-mapa > .mapa-de-assentos").html('<p class="alert alert-danger">Falha ao carregar o mapa</p>');
    $('.div-loading').hide();
}

function addProdutoOK (data) {
    $('#' + codigo + ' circle').addClass('fillOrange').addClass('minhaSelecao');
    var div = '<div id="div-' + codigo + '" class="col-xs-3 selecionado idData-'+idDataEventoAtual+'" style="display:block"><div class="div-circle">' + produto + '</div></div>';
    $('#assentos-selecionados').append(div);
    atualizarQtdeNoCarrinho();
    btnAssentos();
}

function addProdutoErr (data) {
    showMsg('Erro ao adicionar produto ao carrinho: ' + data.erro.mensagem);
}

function delProdutoOK (data) {
    $('#' + codigo + ' circle').removeClass('fillOrange').removeClass('minhaSelecao').addClass('fillGray');
    $('#div-' + codigo).remove();
    btnAssentos();
    atualizarQtdeNoCarrinho();
}

function delProdutoErr (data) {
    showMsg('Erro ao remover produto do carrinho: ' + data.erro.mensagem);
}

function atualizarQtdeNoCarrinho(){
    $("#total-itens-carrinho .itensNoCarrinho").html($("#assentos-selecionados .selecionado").length + ' ite' + ($("#assentos-selecionados .selecionado").length <= 1 ? 'm' : 'ns'));
}

function mudarIdDataBotao(){
    $("#data").val(idDataEventoAtual);
    qtdeProdutosDataEvento();
}

function btnAssentos(){
    var selecionados = $('.selecionado').length > 0;
    if($("#qtdeIngressos").length > 0)
        selecionados = $("#qtdeIngressos").val() > 0;
    $('#btn-assentos').prop('disabled', !selecionados);
}

function getProdutoOK(data){
    var produto = data.produtos[0];
    if(produto.ativo != '1' || produto.oferecido <= 0){
        showMsg("Esse produto não está disponível");
        return;
    }
    var qtde = $("#qtdeIngressos").val()
    if(parseInt(produto.oferecido) < parseInt(qtde)){
        showMsg("Só existe(m) "+produto.oferecido+" ingresso(s) no estoque! Informe a quantidade igual ou menor do que "+produto.oferecido);
        return;
    }
    doAjax ($('#url-add-produto').val(), 'POST', {'produto_codigo': produto.codigo,'produto_unidades': qtde}, 'JSON', 10000, 'addProdutoLivreOK', 'addProdutoErr');
    //$("#btn-assentos").button('reset');
}

function getProdutoErro(data){

    //$("#btn-assentos").button('reset');
}

function addProdutoLivreOK (data) {
    //$("#btn-assentos").button('loading');
    window.location.href = $('#url-ocupantes').val() + '?evento=' + $('#evento').val() + '&data=' + $('#data').val();
}

function qtdeProdutosDataEvento(){
    $("#qtdeIngressos").attr('disabled','disabled');
    var v=$('#url-qtd-produto').val();
    if(v!=undefined)
        doAjax (v, 'GET', {'data': idDataEventoAtual}, 'JSON', 10000, 'atualizarQtdProdutos', 'atualizarQtdProdutosErr');
}

function atualizarQtdProdutos(data){
    var q=parseInt(data.objeto.qtde);
    $("#qtdeIngressos").val(q).removeAttr('disabled');
}
function atualizarQtdProdutosErr(data){
    $("#qtdeIngressos").val(1).removeAttr('disabled');
}
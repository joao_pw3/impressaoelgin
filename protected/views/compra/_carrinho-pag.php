<?php
use app\models\ApiAgenda;
use app\models\ApiProduto;
use yii\helpers\Html;
use app\models\Mineiraocard;
$tituloEvento = '';
foreach ($carrinho->objeto->produtos as $index => $produto) {
        $apiProduto=new ApiProduto();
        $produtoValor=$apiProduto->descontoProduto($produto->codigo);
        $bloco = '';
        $area = '';
        $agenda=new ApiAgenda();
        $evento;
        $banner = '';
        if(isset($produto->tags)){
            foreach ($produto->tags as $index_tag => $tag) {
                if ($tag->nome == 'area') {
                    switch($tag->valor){
                        case 'Leste':
                            $area='Vermelha';
                            break;
                        case 'Oeste':
                            $area='Roxa';
                            break;
                    }
                }
                if ($tag->nome == 'matriz') {
                    $evento = $agenda->buscarEvento(json_encode(['tags'=>[['nome'=>'apelido','valor'=>$tag->valor]]]));
                }
            }
        }
        foreach ($evento->objeto[0]->anexos as $index_anexo => $anexo) {
            if (isset($anexo->nome) && $anexo->nome == 'bannervitrine') {
                $banner = $anexo->url;
            }                  
        }
        
        $mineiraocardNome='';
        $mineiraocardDoc='';
        $mineiraocardTel='';
        $mineiraocardEmail='';
        $mineiraocardValido=false;
        
        if(isset($carrinho->objeto->tags)){
            foreach($carrinho->objeto->tags as $tag){
                $nome=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_nome');
                if($nome!=false)
                    $mineiraocardNome=$nome;
                $doc=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_documento');
                if($doc!=false)
                    $mineiraocardDoc=$doc;
                $tel=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_telefone');
                if($tel!=false)
                    $mineiraocardTel=$tel;
                $email=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_email');
                if($email!=false)
                    $mineiraocardEmail=$email;
            }
            $mineiraocardValido=$mineiraocardNome!=false&&$mineiraocardDoc!=false;
        }
        if ($tituloEvento != $produto->agenda->nome . ' - ' . $produto->agenda->horario) { ?>
            <div class="row about-left wow fadeInLeft animated div-<?= $produto->codigo; ?>" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                <div class="col-md-12 col-xs-12 tarjaCz">
                    <?php echo $evento->objeto[0]->nome; ?>
                </div>
            </div>
            <br>
        <?php
            $tituloEvento = $produto->agenda->nome . ' - ' . $produto->agenda->horario;
        }?>
        <div class="row">           
            <div class="col-md-12 zeraesp">
                <div class="tabProds">
                    <div>
                        <label>Itens</label>
                        <?= number_format($produto->unidades, 0); ?>
                    </div>
                    <div>
                        <?php echo Html::img($banner, ["class" => "imgBanner"]); ?>
                    </div>
                    <div>
                        <label>Produto</label>
                        <?= $produto->nome; ?>
                    </div>
                    <div>
                        <label>Área</label>
                        <?= $area; ?>
                    </div>
                    <div>
                        <label>Nome do Ocupante</label>
                        <?= $mineiraocardNome ?>
                    </div>
                    <div>
                        <label>Documento</label>
                        <?= $mineiraocardDoc ?>
                    </div>
                    <div>
                        <label>Subtotal</label>
                        <?php
                        if ($produtoValor == $produto->valor) {
                            echo 'R$ ' . number_format($produtoValor, 2, ',', '.');
                        } else {
                            echo '<span class="strikeDesconto">R$ ' . number_format($produto->valor, 2, ',', '.') . '</span><br> ';
                            echo ' R$ ' . number_format($produtoValor, 2, ',', '.');
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        
        <?php 
}
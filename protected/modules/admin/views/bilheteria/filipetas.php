<?php
use yii\helpers\Html;

$count = -1;
$tagControl = 0;
$tagOcupante = [];
$ocupantes = [];
if (isset($produtos->tags) && !empty($produtos->tags)) {
    foreach ($produtos->tags as $tag) {
        $arrTag = isset($tag->nome) ? explode('_', $tag->nome) : explode('_', $tag->documento);
        if ($tagControl != $arrTag[0]) {
            $count ++;
            $tagControl = $arrTag[0];
            $tagOcupante[$count]['produto'] = $tagControl;
        }
        $tagOcupante[$count][$arrTag[1]] = $tag->valor;
    }
    foreach($tagOcupante as $index => $ocupante) {
        $ocupantes[$ocupante['produto']] = $ocupante;
    }
}
?>
<page size="filipetas">
    <?php 
    foreach($produtos->produtos as $index => $produto) { 
        if ($tipo == 'venda' || ($tipo == 'produto' && $produtoCodigo == $produto->codigo)) {
    ?>
    <div class="filipeta filipeta-<?= $produto->codigo ?>">
        <?= Html::img('@web/images/logoMineirap.png', ['style' => 'width:130px']); ?>
        <hr style="border:2px groove #000; margin:5px 0px;">
        <p style="font-size:19px;"><strong><?= $evento->objeto[0]->nome; ?></strong></p> 
        <p style="font-size:19px;"><strong><?= $ocupantes[$produto->codigo]['nome']; ?> (Documento: <?= $ocupantes[$produto->codigo]['documento']; ?>) </strong></p>
        <p><strong><?= $produto->nome ?></strong></p>
        <!--<p>Portão A</p>-->
        <p>Nº do seguro: 409876 - Mapfre Seguros</p>
        <hr style="border:2px groove #aaa; margin:5px 0px;">
        <p style="font-size:11px;">Seja bem vindo ao gigante! Esta filipeta faz parte do seu ingresso com informações do seu assento.<br>Estádio Mineirão - www.estadiomineirao.com.br</p>
        <hr style="border:2px groove #000; margin:5px 0px;">
    </div>
    <?php 
        }
    } 
    ?>
</page>
<?php
namespace app\modules\admin\models;

use app\models\Api;

/**
 * Classe para Caixa - Easy for Pay - Mineirão
 * obs.: parâmetros entre colchetes são opcionais
 */
class ApiCaixa extends Api 
{
    public $logs;
    
    /**
     * Fazer operações de caixa (abrir / fechar caixa, registrar informações, etc)
     * @param type $post
     *      id: string (255) (id do caixa)
     *      saida: boolean (0 = false / 1 = true)
     *      objeto: base64_encode(<json>) - qualquer informação relacionada que deva ser registrada
     */
    public function fazerAcaoCaixa ($post=[]) {
        $this->setLog('Fazer acao de caixa');
        if (!empty($post) && is_array($post)) {
            $this->logs->log('Efetuar ação de caixa', $post);
            $returnApi = json_decode($this->call('entsai/', 'POST', $post, true, 'A'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao fazer ação de caixa' : 'Ação de caixa efetuada com sucesso', $returnApi);
            return $returnApi;
        } 
        $this->logs->log('Ação de caixa inválida. Formulário vazio.');
        return $this->getApiError('Não foi possível fazer ação de caixa. Faltam informações para efetuar o procedimento.');
    }
    
    /**
     * Ler operações de caixa
     * @param type $post
     * @param string $dateOperation
     * @return type
     */
    public function lerAcaoCaixa ($post=[], $dateStart='', $dateFinish='') {
        $this->setLog('Ler acao de caixa');
        if (!empty($post) && is_array($post)) {
            $this->logs->log('Leitura de ação de caixa', $post);
            $returnApi = json_decode($this->call('entsai/' . $post['id'] . '/' . (empty($dateStart) && empty($dateFinish) ? date('d-m-Yt00:00:00') . '/' . date('d-m-Yt23:59:59') : $dateStart . 't00:00:00/' . $dateFinish . 't23:59:59'), 'GET', NULL, false, 'A'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao ler ação de caixa' : 'Ação de caixa lida com sucesso', $returnApi);
            return $returnApi;
        } 
        $this->logs->log('Leitura de ação de caixa inválida.');
        return $this->getApiError('Não foi possível ler ação de caixa. Faltam informações para efetuar o procedimento.');
    }
    
}
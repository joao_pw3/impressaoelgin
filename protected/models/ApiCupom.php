<?php
namespace app\models;
use app\models\Api;

/**
 * Classe para Cupons - Easy for Pay - Mineirão
 * obs.: parâmetros entre colchetes são opcionais
 */
class ApiCupom extends Api
{
    public $logs;
    
    /**
     * Criar cupom
     * @param $post Dados do cupom: id, nome, desconto ([fixo], [percentual]), [validade] ([inicial], [final]), cumulativo, ativo
     * @return object {'successo': '1'}
     */
    public function criarCupom($post=[])
    {
        $this->setLog('Criação de cupom');
        if (!empty($post) && is_array($post)) {
            $this->logs->log('Criar cupom', $post);
            $returnApi = json_decode($this->call('cupom/', 'POST', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao criar cupom' : 'Cupom criado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Criação de cupom inválido. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível criar o cupom.');
    }
    
    /**
     * Alterar cupom
     * @param $post Dados do cupom: nome, desconto ([fixo], [percentual]), [validade] ([inicial], [final]), cumulativo, ativo
     * @return object {'successo': '1', 'alterado': '1'}
     */
    public function alterarCupom($post=[])
    {
        $this->setLog('Alteração de cupom');
        if (!empty($post) && is_array($post) && isset($post['id']) && $post['id'] > 0) {
            $this->logs->log('Alterar cupom', $post);
            $returnApi = json_decode($this->call('cupom/' . $post['id'], 'PUT', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao alterar cupom' : 'Cupom alterado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Alteração de cupom inválido. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível alterar o cupom.');
    }
    
    /**
     * Excluir cupom
     * @param $post Dados do cupom: id
     * @return object {'successo': '1', 'removido': '1'}
     */
    public function excluirCupom($post=[])
    {
        $this->setLog('Exclusão de cupom');
        if (isset($post['id']) && $post['id'] > 0) {
            $this->logs->log('Excluir cupom', $post);
            $returnApi = json_decode($this->call('cupom/' . $post['id'], 'DELETE', NULL));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao excluir cupom' : 'Cupom excluído com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Exclusão de cupom inválido. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível excluir o cupom.');
    }
    
    /**
     * Consultar cupom
     * @param $post Dados do cupom: id
     * @return object {'successo': '1', 'nome': '', 'desconto': {'fixo': '', 'percentual'}, 'cumulativo': '', 'ativo': '', 'estoque': ''}
     */
    public function consultarCupom($post=[])    
    {
        $this->setLog('Consulta de cupom');
        if (isset($post['id']) && $post['id'] > 0) {
            $this->logs->log('Consultar cupom', $post);
            $returnApi = json_decode($this->call('cupom/' . $post['id'], 'GET', NULL));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar cupom' : 'Cupom consultado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Consulta de cupom inválido. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível consultar o cupom.');
    }
    
    /**
     * Adicionar estoque a cupom
     * @param $post Dados do cupom: codigo, unidades, tid
     */
    public function adicionarEstoqueCupom($post=[])
    {
        $this->setLog('Adição de estoque a cupom');
        if (isset($post['codigo']) && $post['codigo'] > 0) {
            $this->logs->log('Adicionar estoque a cupom', $post);
            $returnApi = json_decode($this->call('cupom/' . $post['codigo'] . '/estoque', 'POST', ['unidades' => $post, 'tid' => substr(time() . mt_rand(), 8, 11)]));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao adicionar estoque a cupom' : 'Estoque adicionado a cupom com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Adição de estoque a cupom inválido. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível adicionar estoque a cupom.');
    }
    
    /**
     * Baixar estoque de cupom
     * @param $post Dados do cupom: codigo, unidades
     */
    public function removerEstoqueCupom($post=[])
    {
        $this->setLog('Baixa de estoque de cupom');
        if (isset($post['codigo']) && $post['codigo'] > 0) {
            $this->logs->log('Baixar estoque de cupom', $post);
            $returnApi = json_decode($this->call('cupom/' . $post['codigo'] . '/estoque', 'DELETE', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao baixar estoque de cupom' : 'Estoque baixado de cupom com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Baixa de estoque de cupom inválido. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível baixar estoque de cupom.');
    }

    /**
     * Consultar estoque de cupom
     * @param $post Dados do cupom: codigo
     * @return object
     */
    public function consultarEstoqueCupom($post=[])
    {
        $this->setLog('Consulta de estoque de cupom');
        if (isset($post['codigo']) && $post['codigo'] > 0) {
            $this->logs->log('Consultar estoque de cupom', $post);
            $returnApi = json_decode($this->call('cupom/' . $post['codigo'] . '/estoque', 'GET'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar estoque de cupom' : 'Consulta de estoque de cupom efetuada com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Consulta de estoque de cupom inválido. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível consultar estoque de cupom.');
    }
    
}
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin(['id' => 'form-tag']); ?>

<div div class="col-md-6" style="padding-left:0px;">
	<?= $form->field($model, 'nome') ?>
</div>
<div div class="col-md-6" style="padding-left:0px;">
	<?= $form->field($model, 'valor') ?>
</div>

<div class="form-group" style="text-align:center;">
    <?= Html::submitButton($model->scenario=='create'? 'Cadastrar' :'Salvar', ['class' => 'btn btn-primary btn-pagar']) ?>
</div>

<?php ActiveForm::end(); ?>
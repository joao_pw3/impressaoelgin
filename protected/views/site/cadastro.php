<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\components\TempoCarrinhoWidget;

$session = Yii::$app->session;
$session->open();
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    echo TempoCarrinhoWidget::getBreadCrumb('losang2.png');
} 
?>

<div class="cadastro" id="cadastro">
    <div class="container">
        <input type="hidden" id="url-cadastro" value="<?= Url::to(['site/cadastro-basico']); ?>">
        <input type="hidden" id="url-login" value="<?= Url::to(['site/login']); ?>">
        <input type="hidden" id="url-cadastro-completo" value="<?= Url::to(['site/cadastro-completo']); ?>">
        <input type="hidden" id="url-cartao" value="<?= Url::to(['site/cadastro-cartao']); ?>">
        <input type="hidden" id="url-cep" value="<?= Url::to(['site/check-cep']); ?>">
        <input type="hidden" id="url-resumo" value="<?= Url::to(['site/resumo']); ?>">
        <input type="hidden" id="url-visitante" value="<?= Url::to(['site/visitante']); ?>">
        <input type="hidden" id="url-cliente" value="<?= Url::to(['site/cliente']); ?>">
        <input type="hidden" id="url-index" value="<?= Url::to(['site/index']); ?>">
        <input type="hidden" id="url-verificar-documento" value="<?= Url::to(['site/verificar-documento']); ?>">
   
    <?php $form = ActiveForm::begin([
        'id' => 'form-cadastro',
    ]); ?>
        
    <div class="row">
        <?php echo MenuWidget::getMenuVisitante(); ?>
        <div class="col-md-10">
            <div class="tab-content">
                <div id="tab-acesso" class="tab-pane fade in active">
                    <?= $this->render('cliente-acesso', [
                        'cliente' => $cliente,
                        'form' => $form
                    ]); ?>
                </div>
                <div id="tab-dados" class="tab-pane fade">
                    <?= $this->render('cliente-dados', [
                        'cliente' => $cliente,
                        'form' => $form
                    ]); ?>
                </div>
                <div id="tab-pagamentos" class="tab-pane fade">
                    <?= $this->render('cliente-pagamento', [
                        'clienteApi' => $clienteApi,
                        'cartao' => $cartao,
                        'cartoesApi' => isset($cartoesApi) && !empty($cartoesApi) ? $cartoesApi : [],
                        'form' => $form,
                        'clienteNovo' => true,
                    ]); ?>
                </div>
            </div>            
        </div>
    </div>
    
    <?php ActiveForm::end(); ?>
        
    </div>
    
</div>
    
<?php 
$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/cadastro.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset', 'yii\bootstrap\BootstrapPluginAsset']]);
$this->registerJsFile('@web/js/carrinho.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    $this->registerJs("deadline = setDataExpira('" . $session['tempoCarrinho'] . "'); initializeClock();");
}
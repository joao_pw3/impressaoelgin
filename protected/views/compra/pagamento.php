<?php

use app\assets\PagamentoAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Tags;

PagamentoAsset::register($this);

$session = Yii::$app->session;
$session->open();
$this->title = 'Pagamento';
$this->params['breadcrumbs'][] = ['label' => 'Mapa de Assentos', 'url' => ['/agenda', 'evento' => $evento]];
$this->params['breadcrumbs'][] = ['label' => 'Ocupantes', 'url' => ['ingresso', 'evento' => $evento, 'data' => $data]];
$this->params['breadcrumbs'][] = $this->title;
if (!empty($session['user_id']) && isset($carrinho->objeto->produtos) && !empty($carrinho->objeto->produtos)) {
    ?>
    <?php $form = ActiveForm::begin([
        'id' => 'form-pagamento'
    ]); ?>
    <div class="container">
        <div class="row">
            <input type="hidden" id="url-ocupante" value="<?= Url::to(['compra/salvar-ocupante']); ?>">
            <input type="hidden" id="url-cobranca" value="<?= Url::to(['compra/enviar-cobranca']); ?>">
            <input type="hidden" id="url-consulta" value="<?= Url::to(['compra/consultar-cobranca']); ?>">
            <input type="hidden" id="url-autoriza" value="<?= Url::to(['compra/autorizar-cobranca']); ?>">
            <input type="hidden" id="url-pos-venda" value="<?= Url::to(['compra/pos-venda']); ?>">
            <input type="hidden" id="url-compras" value="<?= Url::to(['cliente/compras']); ?>">
            <input type="hidden" id="url-del-produto-carrinho" value="<?= Url::to(['compra/remover-produto-carrinho']); ?>">
            <div class="row">
                <?= $form->field($cobranca, 'trid')->hiddenInput(['value' => substr(time() . mt_rand(), 8, 11)])->label(false); ?>
                <?= $form->field($cobranca, 'id_carrinho')->hiddenInput(['value' => $session['idCarrinho']])->label(false); ?>
                <?= $form->field($cobranca, 'documento')->hiddenInput(['value' => $session['documento']])->label(false); ?>
                <?= $form->field($cobranca, 'email')->hiddenInput(['value' => $session['user_id']])->label(false); ?>
                <?= $form->field($cobranca, 'descricao')->hiddenInput(['value' => 'Easy for Pay - Site'])->label(false); ?>
                <?= $form->field($cobranca, 'id_venda')->hiddenInput(['id' => 'id_venda'])->label(false); ?>
                <?php
                $totalGeral = 0;
                $tagsTmp = new Tags();
                foreach ($listaProdutos as $index => $_produto) {
                    $produto=$_produto['objeto'];
                    $indexForm=intval($_produto['indexForm']);
                    $ingresso = isset($produto->cupom) ? $produto->cupom->nome : 'Inteira';
                    $cupom = isset($produto->cupom) ? $produto->cupom->id : 'inteira';
                    $tags=null;
                    if(isset($produto->tags))
                        $tags = $tagsTmp->objetoTags($produto->tags);
                    ?>
                    
                    <div class="row telaPagamento" id="produto-<?= $produto->codigo.'-'.$indexForm; ?>">
                        <div class="col-xs-2">
                            <p><?= $produto->agenda->nome ?><br /><?= $produto->agenda->horario ?></p>
                        </div>
                        <div class="col-xs-2">
                            <p><?= $_produto['nome']; ?></p>
                            <?= $form->field($cobranca, "[$indexForm]tag_nome")->hiddenInput(['value' => $_produto['nome']])->label(false); ?>
                            <?= $form->field($cobranca, "[$indexForm]tag_telefone")->hiddenInput(['value' => $_produto['telefone']])->label(false); ?>
                            <?= $form->field($cobranca, "[$indexForm]tag_email")->hiddenInput(['value' => $_produto['email']])->label(false); ?>
                            <?= $form->field($cobranca, "[$indexForm]cupom_codigo")->hiddenInput(['value' => $cupom])->label(false); ?>
                        </div>
                        <div class="col-xs-2">
                            <p><?= $_produto['documento']; ?></p>
                            <?= $form->field($cobranca, "[$indexForm]tag_documento")->hiddenInput(['value' => $_produto['documento']])->label(false); ?>
                        </div>
                        <div class="col-xs-2">
                            <p><?= $ingresso; ?></p>
                        </div>
                        <div class="col-xs-2" align="right">
                            <p>R$ <?= number_format($produto->valor_liquido,2,',','.'); ?></p>
                            <small>
                                Ingresso R$ <?=number_format($produto->valor_desconto,2,',','.')?> <br /> 
                                Conveniência R$ <?=number_format(($produto->valor_liquido-$produto->valor_desconto),2,',','.')?>
                            </small>
                        </div>
                        <div class="col-xs-2">
                            <?php if(isset($tags)) {?>
                                <div class="div-circle div-ingresso">
                                    <span title="Fileira <?= $tags->fileira; ?> - Assento <?= $tags->assento; ?>"><?= $tags->fileira . '-' . $tags->assento; ?></span>
                                </div>
                            <?php }?>
                            <?php if (isset($tags, $tags->especial)) { ?>
                            <div class="div-especial">
                                <span class="font-awesome" title="<?= $tags->especial == 'cadeirante' ? 'Cadeirante' : ($tags->especial == 'mobilidade' ? 'Mobilidade reduzida' : ($tags->especial == 'obeso' ? 'Obeso' : 'Acompanhante')); ?>">
                                    <?= $tags->especial == 'cadeirante' ? '&#xf193;' : ($tags->especial == 'mobilidade' ? '&#xf29d' : ($tags->especial == 'obeso' ? '&#xf234;' : '&#xf067')); ?>
                                </span>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                   
                <?php } ?>
                <?= $form->field($cobranca, 'valor')->hiddenInput(['id' => 'valor', 'value' => $carrinho->objeto->totais->total])->label(false); ?> 
                <?= $form->field($cobranca, 'forma')->hiddenInput(['value' => 1])->label(false); ?> 
                <div class="row">
                    <div class="col-xs-8 fontSize16">Total geral:</div>
                    <div class="col-xs-2 fontSize16" align="right">R$ <?= number_format($carrinho->objeto->totais->total, 2, ',', '.'); ?></div>
                </div>
                <hr>
                <div class="row">
                    <?php if (!isset($session['documento']) || empty($session['documento'])) { ?>
                    <div class="col-xs-6">
                        <p>&nbsp;</p>
                        <p align="left">Para finalizar sua compra, <a href="<?= Url::to(['/cliente/index/meus-dados', 'menu' => true]); ?>">clique aqui</a> e finalize seu cadastro.</p>
                    </div>
                    <?php } else if (empty($cartoes->objeto)) { ?>
                    <div class="col-xs-6">
                        <p>&nbsp;</p>
                        <p align="left">Para finalizar sua compra, <a href="<?= Url::to(['cliente/cartao']); ?>">clique aqui</a> e cadastre um cartão.</p>
                    </div>
                    <?php } else {
                        $items = [];
                        foreach ($cartoes->objeto as $indexForm => $cartao) {
                            $items[$cartao->codigo_cartao] = $cartao->ultimos_digitos . (!empty($cartao->bandeira) ? ' - ' . $cartao->bandeira : ' -');
                            if ($cartao->favorito == 1) {
                                $cobranca->codigo_cartao = $cartao->codigo_cartao;
                            }
                        }
                        ?>
                        <div class="col-xs-4">
                            <?= $form->field($cobranca, 'codigo_cartao')->dropDownList($items, ['id' => 'codigo_cartao'])->label('Pagar com o cartão de final:'); ?>
                            <p align="left">Cadastre outro cartão, <a href="<?= Url::to(['cliente/cartao']); ?>">clicando aqui</a>.</p>
                        </div>
                        <div class="col-xs-4">
                            <?= $form->field($cobranca, 'cvv')->passwordInput(['id' => 'cvv', 'maxlength' => 4])->label('CVV deste cartão:'); ?>
                        </div>
                        <div class="col-xs-4">
                            <input type="button" class="btn btn-primary btn-pagar" data-loading-text="Processando..." value="Autorizar o pagamento" id="btn-autorizar">
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <br>
            <br>
        </div>
    </div>
    <?php
    ActiveForm::end();
} 

if (!isset($carrinho->objeto->produtos) || empty($carrinho->objeto->produtos)) {
    ?>
    <div class="container">
        <div class="row">
            <p>Seu carrinho de compras expirou.</p>
            <p><?= Html::a('Fazer nova compra', Url::to(['/'])); ?></p>
        </div>
    </div>
    <?php
}
?>
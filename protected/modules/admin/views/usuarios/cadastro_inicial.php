<?php
$this->title = 'ADM Estádio Mineirão :: Adicionar operador';
?>
<div class="container row">
    <div class="col-md-12">		
        <div>
            <h2 class="tituloGeral"><i class="fa fa-clone"></i> Adicionar operador</h2>
            <?= $this->render('form',['model' => $model])?>
        </div>
    </div>
</div>
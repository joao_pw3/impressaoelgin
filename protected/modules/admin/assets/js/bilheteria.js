var btnIngresso;
var capptaObj;
var qrcode;

$(document).ready(function() {

    $('.bilheteria_opc_localizar').each(function () {
        if ($(this).is(':checked') && $(this).val() == '1') {
            $('#qrcode_voucher').prop('disabled', false);
            $('#qrcode_voucher').focus();
            $('#documento').prop('disabled', true);
        } else if ($(this).is(':checked') && $(this).val() == '2') {
            $('#documento').prop('disabled', false);
            $('#documento').focus();
            $('#qrcode_voucher').prop('disabled', true);
        }
    });
    
    $('.bilheteria_opc_localizar').change(function () {
        if ($(this).val() == '1') {
            $('#qrcode_voucher').prop('disabled', false);
            $('#qrcode_voucher').focus();
            $('#documento').prop('disabled', true);
        } else {
            $('#documento').prop('disabled', false);
            $('#documento').focus();
            $('#qrcode_voucher').prop('disabled', true);
        }
    });

    $("input#qrcode_voucher, input#documento").focus(function(){$(this).val('')})

    $('#ativar-ingresso').click(function () {
        if ($('#qrcode_voucher').val() == '' && $('#documento').val() == '') {
            $('#modal-mensagem .modal-body').html('Por favor, leia o QR Code ou preencha o documento do cliente.');
            $('#modal-mensagem').modal('toggle');
            return false;
        }
    });

    $('.salvar-ingresso').click(function () {
        btnIngresso = $(this);
        var id = $(this).attr('id').replace('salvar-', '');
        var form = $('#form-ingresso-' + id);
        form.removeClass('hidden');
        form.find('fieldset').removeAttr('disabled');
        $('#modal-' + id).modal('toggle');
        $('#form-ingresso-' + id + '-qrcode').focus();
    });
    
    $('.btn-salvar').click(function () {
        var id = $(this).attr('id');
        var form = $('#form-ingresso-' + id);
        if (typeof $('#form-ingresso-' + id + ' #retira-nome') != 'undefined' && ($('#form-ingresso-' + id + ' #retira-nome').val() == '' || $('#form-ingresso-' + id + ' #retira-documento').val() == '')) {
            showMsg('Preencha o nome e o documento de quem está retirando o ingresso.');
            $('#modal-mensagem').on('hidden.bs.modal', function (e) {
                e.preventDefault();
            });
            return false;
        }
        if ($('#form-ingresso-' + id + '-qrcode').val() == '' ) {
            showMsg('Por favor, leia o qrcode.');
            return false;
        }
        
        var tmp1 = new String($('#form-ingresso-' + id + ' #qrcode_check').val());
        var tmp2 = new String($('#form-ingresso-' + id + '-qrcode').val());
        if (tmp1.valueOf() != tmp2.valueOf()) {
            showMsg('O Qrcode lido não é deste ocupante. Por favor, confira os dados do cartão e tente novamente.');
            return false;
        }
        
        var qrCodeVal = $('#form-ingresso-' + id + "-qrcode").val().trim();
        if (qrCodeVal == '') {
            showMsg('O Qrcode é obrigatório.');
            $('#modal-mensagem').on('hidden.bs.modal', function (e) {
                e.preventDefault();
            });
            return false;
        }
        enviaFormIngresso(form);
    });
    
    $('#btn-restart').click(function () {
        window.location.href = $('#url-ativar-ingresso').val();
    });
    
    $('#teste-etiqueta').click(function () {
        print_etiqueta();
    });

    // submit de pagamento avulso
    $("#venda-avulsa-checkout-form :submit").click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var tipo=$("#venda-avulsa-checkout-form input[name='tipo']:checked").val();
        var total=$("#venda-avulsa-checkout-form input[name='total']").val();
        var parcelas=$("#venda-avulsa-checkout-form select[name='parcelas']").val();
        var prosseguir=parametrosVendaOk(capptaObj,tipo,total,parcelas);
        if(prosseguir.ok){
            $("#cappta-checkout-iframe").removeClass('hidden');
            switch(tipo){
                case 'debito':
                    pagamentoDebito(capptaObj,parseFloat(total));
                    break;
                case 'credito':
                    pagamentoCredito(capptaObj,parseFloat(total),parcelas);
                    break;
            }
        }
        if(prosseguir.erro)
            showMsg(prosseguir.mensagem);
    }); 
    
    // mostrar/esconder campo de parcelamento
    $("#venda-avulsa-checkout-form input[name='tipo']").click(function(e){
        if(e.target.value=='credito')
            $("#venda-avulsa-checkout-form select[name='parcelas']").removeClass('hidden');
        else
            $("#venda-avulsa-checkout-form select[name='parcelas']").addClass('hidden');
    }); 
    
    $('.ingresso-detalhe').click(function () {
        var id = $(this).attr('id');
        if ($('#tr-' + id).hasClass('hidden')) {			
            $('.seta' + id).css('-webkit-transform','rotate(180deg)'); 
            $('.seta' + id).css('-moz-transform','rotate(180deg)');
            $('.seta' + id).css('transform','rotate(180deg)')
            $('#tr-' + id).fadeIn("slow");
            $('#tr-' + id).removeClass('hidden');
        } else {
            $('.seta' + id).css('-webkit-transform','rotate(0deg)'); 
            $('.seta' + id).css('-moz-transform','rotate(0deg)');
            $('.seta' + id).css('transform','rotate(0deg)')
            $('#tr-' + id).fadeOut();
            $('#tr-' + id).addClass('hidden');
        }
    });
	
	/*Abre e fecha barra de filtros*/
	$('#fechaboxFilter').click(function () {
		$('#boxBorder').removeClass("semboxBorder");
		$('#boxBorder').addClass("boxBorder");
		$('.boxBorder2int').slideDown();
		$('#fechaboxFilter').hide();
		$('#abreboxFilter').show();
		//$('#boxBorder').hide();		
		//$('.consBusca2').slideDown("slow");		
		//$('#boxFilter').animate({height: 'toggle'});
	});
	$('#abreboxFilter').click(function () {
		$('#boxBorder').addClass("semboxBorder");
		$('.boxBorder2int').slideUp();
		$('#abreboxFilter').hide();
		$('#fechaboxFilter').show();
		//$('.consBusca2').slideUp();		
		//$('.consBusca1').slideDown();
		//$('#fechaboxFilter').show();
		//$('#boxFilter').animate({height: 'toggle'});
	});
    
    $('.btn-bloq').click(function () {
        qrcode = $(this).attr('data-qrcode');
        evento = $(this).attr('data-evento');
        $('#modal-confirmar .modal-body').html('Confirma bloqueio de ingresso?');
        $('#btn-continue').removeClass('2via').addClass('bloq');
        $('#modal-confirmar').modal('toggle');
    });
    
    $('.btn-2, .btn-seg-via').click(function () {
        qrcode = $(this).attr('data-qrcode');
        evento = $(this).attr('data-evento');
        $('#modal-confirmar .modal-body').html('Confirma solicitação de segunda via de ingresso?');
        $('#btn-continue').removeClass('bloq').addClass('2via');
        $('#modal-confirmar').modal('toggle');
    });
    
    $('#btn-cancel').click(function () {
        $('#modal-confirmar').modal('hide');
        return false;
    });
    
    $('#btn-continue').click(function () {
        $('#modal-confirmar').modal('hide');
        if ($(this).hasClass('bloq')) {
            doAjax($('#url-bloq-ingresso').val(), 'POST', {'id_agenda': evento, 'qrcode': qrcode}, 'JSON', 5000, 'bloqueioIngressoOk', 'bloqueioIngressoErr');
        } else {
            doAjax($('#url-2via-ingresso').val(), 'POST', {'id_agenda': evento, 'qrcode': qrcode}, 'JSON', 5000, 'segundaviaIngressoOk', 'segundaviaIngressoErr');
        }
        return false; 
    });
    
    $('.btn-invalidar').click(function () {
        qrcode = $(this).attr('data-qrcode');
        evento = $(this).attr('data-evento');
        $('#modal-invalidar').modal('toggle');
        return false;
    });
    
    $('#btni-cancel').click(function () {
        $('#modal-invalidar').modal('hide');
        return false;
    });
    
    $('#btni-continuar').click(function () {
        if ($('#motivo').val().length <= 5) {
            showMsg('Por favor, informe o motivo para invalidar o cartão do cliente.');
            return false;
        }
        $('#modal-invalidar').modal('hide');
        doAjax($('#url-invalidar-ingresso').val(), 'POST', {'id_agenda': evento, 'qrcode': qrcode, 'motivo': $('#motivo').val()}, 'JSON', 5000, 'invalidarIngressoOk', 'invalidarIngressoErr');
        return false;
    });
    
    $('.radio-retira').click(function() {
        var id = $(this).parent().attr('id');
        var form = id.replace('retira-', '');
        if ($('#' + form + ' input[name="retira"]:checked').val() == 'o') {
            $('#' + form + ' #retira-nome').val($('#' + form + ' #ocupante-nome').val());
            $('#' + form + ' #retira-documento').val($('#' + form + ' #ocupante-documento').val());
        } else if ($('#' + id + ' input[name="retira"]:checked').val() == 'c') {
            $('#' + form + ' #retira-nome').val($('#' + form + ' #comprador-nome').val());
            $('#' + form + ' #retira-documento').val($('#' + form + ' #comprador-documento').val());
        } else {
            $('#' + form + ' #retira-nome').val('');
            $('#' + form + ' #retira-documento').val('');
        }
    });
    

});

function enviaFormIngresso(form){
    var action = form.attr('action');
    doAjax(action, 'POST', form.serialize(), 'JSON', 5000, 'retirarIngressoOk', 'retirarIngressoErr');
}

function retirarIngressoOk(data) {
    var blocoIngresso = $(btnIngresso).parent();
    $(btnIngresso).remove();
    var id = btnIngresso.attr('id').replace('salvar-', '');
    $('.msg-' + id).removeClass('alert-info').removeClass('alert-warning').removeClass('alert-danger').addClass('alert-success').html(data.mensagem);
    $('.btn-salvar-' + id).remove();
    if ($('input[class^=\"btn-salvar-\"]').length == 0) {
        $('#div-next').removeClass('hidden');
    }
    $('#modal-' + id).modal('hide');
}

function retirarIngressoErr(data) {
    var id = btnIngresso.attr('id').replace('salvar-', '');
    $('.msg-' + id).removeClass('alert-info').removeClass('alert-success').removeClass('alert-warning').addClass('alert-danger').html(data.erro.mensagem);
}

function checkVenda () {
    doAjax ($('#url-check-venda').val(), 'POST', $('#form-ativar-ingresso').serialize(), 'JSON', 10000, 'checkVendaOK', 'checkVendaErr');
}

function checkVendaOK (data) {
    if (data.conteudo != '') { 
        $('#data-venda').html(data.conteudo);
    } else {
        $('#modal-mensagem .modal-body').html('Não há dados para o ID informado.');
        $('#modal-mensagem').modal('toggle');
        $('#ativar-ingresso').attr('disabled', false);
    }
}

function checkVendaErr (data) {
    $('#modal-mensagem .modal-body').html('Verifique o ID da compra e tente novamente.');
    $('#modal-mensagem').modal('toggle');
    $('#ativar-ingresso').attr('disabled', false);
    return false;
}

function bloqueioIngressoOk (data) {
    $('#btnb-' + qrcode).prop('disabled', 'disabled');
    $('#btn2-' + qrcode).prop('disabled', 'disabled');
    $('#modal-mensagem .modal-body').html('Ingresso bloqueado com sucesso.');
    $('#modal-mensagem').modal('toggle');
    return false;
}

function bloqueioIngressoErr (data) {
    $('#modal-mensagem .modal-body').html('Falha ao bloquear ingresso. Erro: ' + data.erro.mensagem);
    $('#modal-mensagem').modal('toggle');
    return false;
}

function segundaviaIngressoOk (data) {
    $('#btnb-' + qrcode).prop('disabled', 'disabled');
    $('#btn2-' + qrcode).prop('disabled', 'disabled');
    $('#modal-mensagem .modal-body').html('Segunda via de ingresso solicitada com sucesso.');
    $('#modal-mensagem').modal('toggle');
    return false;
}

function segundaviaIngressoErr (data) {
    $('#modal-mensagem .modal-body').html('Falha ao solicitar segunda via de ingresso. Erro: ' + data.erro.mensagem);
    $('#modal-mensagem').modal('toggle');
    return false;
}

function invalidarIngressoOk (data) {
    $('#btni-' + qrcode).prop('disabled', 'disabled');
    $('#modal-mensagem .modal-body').html('O ingresso foi invalidado com sucesso.');
    $('#modal-mensagem').modal('toggle');
    return false;
}

function invalidarIngressoErr (data) {
    $('#modal-mensagem .modal-body').html('Falha ao invalidar ingresso. Erro: ' + data.erro.mensagem);
    $('#modal-mensagem').modal('toggle');
    return false;
}

function print_etiqueta () {
    var texto = "N\n"; //  (limpa o buffer de impressão, para ser iniciado um novo arquivo BPLB)
    texto.concat("D13\n");  //(configura a Densidade ou aquecimento da cabeça de impressão para o valor 9)
    texto.concat("S1\n");  //(configura a Velocidade de impressão para 3 pol./seg)
    texto.concat("JF\n");  //(habilita o “backfeed” para que ao final da impressão, o espaço entre etiquetas pare na serrilha)
    texto.concat("Q400,140\n");  //(largura: 400/8 = 50mm - altura: 140/8 = 25mm)
    texto.concat("ZT\n");  //(indica que a impressão deve inciar a partir do topo, ou seja, de cabeça para baixo)
    texto.concat("A50,110,0,3,1,1,N," + '\"TESTE DE IMPRESSÃO DE ETIQUETA\"' + "\\n");
    texto.concat("P1\n");

    var printWindow = window.open('', 'Filipeta', 'left=0,top=5,right=0,height=300,width=100');
    printWindow.document.open('text/html');
    printWindow.document.write(texto);
    printWindow.document.close();
    printWindow.focus();
    printWindow.print();
    printWindow.close();    
}
//CAPPTA

var capptaInit=function(){
    capptaObj=CapptaCheckout.authenticate(capptaAuth, capptaAuthSuccess, capptaAuthError);
};

var capptaAuth = {
    authenticationKey: '795180024C04479982560F61B3C2C06E',
    merchantCnpj: '34555898000186',
    checkoutNumber: 185
};

var capptaAuthSuccess = function (response) {
    $("#cappta-status").html('Autenticado com sucesso' + '<br>' + 'Checkout GUID: ' + response.merchantCheckoutGuid);
    $("#cappta-checkout-iframe").addClass('hidden');
};

var capptaAuthError = function (error) {
    $("#cappta-status").html('Código: ' + error.reasonCode + '<br>' + error.reason);
};

var onPendingPayments = function (response) {
    console.log(response);        
};

var parametrosVendaOk=function(obj,tipo,total,parcelas){
    var retorno={'ok':true};
    if(obj instanceof Object == false)
        retorno={'erro':true,'mensagem':'O pinpad não está conectado ou está com problemas. Conecte-o e recarregue a página'};
    if(tipo!='debito' && tipo!='credito')
        retorno={'erro':true,'mensagem':'Selecione a forma de pagamento'};
    if(isNaN(total) || !total>0 || isNaN(parseFloat(total)))
        retorno={'erro':true,'mensagem':'O total da compra é inválido. Reinicie a operação'};
    if(tipo=='credito' && (isNaN(parcelas) || !parcelas>0))
        retorno={'erro':true,'mensagem':'Selecione a quantidade de parcelas'};
    return retorno;
}

var pgtoOk=function(r){
    console.log('pagamento ok');
    console.log(r.administrativeCode);
    console.log(r.receipt.merchantReceipt);
};

var pgtoErr=function(r){
    //showMsg(r.reason);
};

var pagamentoDebito=function(objCappta,valor){
    objCappta.debitPayment({amount:valor}, pgtoOk, pgtoErr);
};

var pagamentoCredito=function(objCappta,valor,parcelas){
    objCappta.creditPayment({
        amount:valor,
        installments:parcelas,
        installmentsType:1
    }, pgtoOk, pgtoErr);
};
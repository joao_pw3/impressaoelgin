<?php
use yii\helpers\Url;
use app\models\Tags;
use app\assets\ProdutoAsset;

ProdutoAsset::register($this);

$this->title = 'Produtos - Matriz de assentos';
?>
<div class="content">
    <h1><span style="font-weight:200;">Produtos</span> - Matriz (assentos marcados)</h1>
    <input type="hidden" id="url-setor" value="<?= Url::to(['setor']); ?>">
    <input type="hidden" id="url-fileira" value="<?= Url::to(['fileira']); ?>">
    <input type="hidden" id="url-assento" value="<?= Url::to(['assento']); ?>">
    <input type="hidden" id="url-salvar-assentos" value="<?= Url::to(['salvar-assentos']); ?>">
    
    <div class="row">
        <div class="col-xs-2">
            1 - Matriz:
        </div>
        <div class="col-xs-6">
            <select id="matriz" style="width:100%;">
            <?php if (!empty($matrizes)) { ?>
                <option>Escolha uma matriz</option>
                <?php 
                foreach ($matrizes as $index => $matriz) { 
                    $tmpTag = (new Tags)->objetoTags($matriz->tags);
                ?>
                <option value="<?= $matriz->codigo; ?>" data-matriz="<?= $tmpTag->matriz; ?>" data-agenda="<?= $matriz->id_agenda; ?>"><?= $matriz->nome; ?></option>
                <?php }?>
            <?php } else { ?>
                <option>Cadastre matriz</option>
            <?php } ?>
            </select>
        </div>
        <div class="col-xs-4" style="text-align:center;">
            <input type="button" id="btn-matriz" class="btn btn-default" value="Cadastrar matriz" style="width:150px;">
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs-2">
            2 - Setor:
        </div>
        <div class="col-xs-6">
            <select id="setor" style="width:100%;" disabled>
                <option>Escolha uma matriz</option>
            </select>
        </div>
        <div class="col-xs-4" style="text-align:center;">
            <input type="button" id="btn-setor" class="btn btn-default" value="Cadastrar setor" disabled style="width:150px;">
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs-2">
            3 - Fileiras:
        </div>
        <div class="col-xs-6">
            <select id="fileira" style="width:100%;" disabled>
                <option>Escolha um setor</option>
            </select>
        </div>
        <div class="col-xs-4" style="text-align:center;">
            <input type="button" id="btn-fileiras" class="btn btn-default" value="Cadastrar fileiras" disabled style="width:150px;">
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs-2">
            4 - Assentos:
        </div>
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-12" id="div-assentos"></div>
            </div>
        </div>
        <div class="col-xs-4" style="text-align:center;">
            <input type="button" id="btn-assentos" class="btn btn-default" value="Cadastrar assentos" disabled style="width:150px;">
        </div>
    </div>
</div>

<?= $this->render('modal-matriz', [
    'model' => $model,
    'eventos' => $eventos,
    'tags' => $tags
]); ?>

<?= $this->render('modal-setor', [
    'model' => $model,
    'tags' => $tags
]); ?>

<?= $this->render('modal-fileira', [
    'model' => $model,
    'tags' => $tags
]); ?>

<?= $this->render('modal-assentos', [
    'model' => $model,
    'tags' => $tags
]); ?>

<br>
<br>
<br>
<br>
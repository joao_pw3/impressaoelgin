<?php
use \yii\helpers\Url;
$this->title = 'ADM Easy for Pay';
?>

<div class="container">
	<div class="row">
		<h2 class="tituloGeral"><i class="fa fa-clone"></i> Remover Anexos: <?=$evento->nome?></h2>
	</div>
	<div class="gridAgenda4Out">
		<div class="gridAgenda4 gridAgenda3Head">		
			<div>Nome</div>
			<div>URL (ver o anexo)</div>
			<div>Tipo</div>
			<div>Ação</div>
		</div>
		<?php
		foreach ($anexos->lista as $model) {?>
			<div class="gridAgenda4">
				<div><?=$model->nome?></div>
				<div><a href="<?=$model->url?>" target="_blank">Abrir em nova janela</a></div>
				<div><?=$model->tipo?></div>
				<div><a href="<?=Url::to(['remover-anexos', 'evento'=>$evento->codigo, 'nomeAnexo'=>$model->nome, 'tipoAnexo'=>$model->tipo])?>" class="fontA botUnic">&#xf1f8;</a></div>
			</div>
		<?php }
		?>
	</div>
</div>
<?php
namespace app\assets;
use yii\web\AssetBundle;

class RelatorioCaixaAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
    ];
    
    public $js = [
        'js/ajax.js',
        'js/relatorio_vendas.js',
        'js/print.js'
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    
    public $publishOptions = [
        'forceCopy' => true
    ];
}

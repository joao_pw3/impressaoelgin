<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "museu_vendas".
 *
 * @property int $id
 * @property string $adq_afiliacao_key
 * @property string $adq_codigo_autorizacao
 * @property string $adquirente
 * @property string $administrative_code
 * @property string $merchant_checkout_guid
 * @property string $cartao_bandeira
 * @property string $cartao_bandeira_codigo
 * @property string $cartao
 * @property string $pagamento_tipo
 * @property double $pagamento_valor
 * @property int $pagamento_parcelas
 * @property int $sequencia_unica
 * @property string $autorizacao_data
 * @property string $recibo_consumidor
 * @property string $recibo_estabelecimento
 * @property string $recibo_simples
 * @property string $status
 * @property string $user_id
 */
class MuseuVendas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'museu_vendas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adq_afiliacao_key', 'adquirente', 'administrative_code', 'merchant_checkout_guid', 'cartao_bandeira', 'cartao_bandeira_codigo', 'cartao', 'pagamento_tipo', 'pagamento_valor', 'pagamento_parcelas', 'sequencia_unica', 'autorizacao_data', 'recibo_consumidor', 'recibo_estabelecimento', 'recibo_simples', 'status', 'user_id'], 'required'],
            [['pagamento_valor'], 'number'],
            [['sequencia_unica'], 'integer'],
            [['recibo_consumidor', 'recibo_estabelecimento', 'recibo_simples', 'status'], 'string'],
            [['adq_afiliacao_key', 'adq_codigo_autorizacao', 'adquirente', 'administrative_code', 'merchant_checkout_guid', 'cartao_bandeira', 'cartao', 'user_id'], 'string', 'max' => 255],
            [['cartao_bandeira_codigo'], 'string', 'max' => 6],
            [['pagamento_tipo'], 'string', 'max' => 45],
            [['pagamento_parcelas'], 'string', 'max' => 1],
            [['autorizacao_data'], 'string', 'max' => 19],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'adq_afiliacao_key' => 'Adq Afiliacao Key',
            'adq_codigo_autorizacao' => 'Adq Codigo Autorizacao',
            'adquirente' => 'Adquirente',
            'administrative_code' => 'Administrative Code',
            'merchant_checkout_guid' => 'Merchant Checkout Guid',
            'cartao_bandeira' => 'Cartao Bandeira',
            'cartao_bandeira_codigo' => 'Cartao Bandeira Codigo',
            'cartao' => 'Cartao',
            'pagamento_tipo' => 'Pagamento Tipo',
            'pagamento_valor' => 'Pagamento Valor',
            'pagamento_parcelas' => 'Pagamento Parcelas',
            'sequencia_unica' => 'Sequencia Unica',
            'autorizacao_data' => 'Autorizacao Data',
            'recibo_consumidor' => 'Recibo Consumidor',
            'recibo_estabelecimento' => 'Recibo Estabelecimento',
            'recibo_simples' => 'Recibo Simples',
            'status' => 'Status',
            'user_id' => 'User ID',
        ];
    }
}

<?php
namespace app\modules\admin\controllers;

use Yii;
use yii\web\Response;
use app\modules\admin\models\BilheteriaModel;
use app\modules\admin\models\ApiIngresso;
use app\models\ApiCliente;
use app\models\ApiCarrinho;
use app\components\VendaWidget;
use app\models\EventosModel;
use app\modules\admin\controllers\AbstractController;
use app\models\Bilheteria;
use app\models\Cliente;
use app\models\Agenda;

/**
 * Controller de bilheteria. Funções para operador de bilheteria do Mineirão
 * ApiBilheteria é o model relacionado
 */
class BilheteriaController extends AbstractController
{
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Registrar entrega de ingresso
     * @todo Gravar logs das ações
     * @todo Reportar casos mais críticos por e-mail
     */
    public function actionAtivarIngresso($idEvento='') {
        try{
            $modelBilheteria = new BilheteriaModel;
            if ($idEvento != '') $modelBilheteria->idEvento = $idEvento;
            $resultadoBusca = [];
            if (Yii::$app->request->post() && $modelBilheteria->load(Yii::$app->request->post())) {
                $resultadoBusca = $modelBilheteria->pesquisa();
            }
            $post = Yii::$app->request->post('BilheteriaModel');
            $radioList = \yii\helpers\Html::radioList('BilheteriaModel[localizar]', '1',
                [
                    '1' => 'Leia o QR Code do voucher',
                    '2' => 'Digite o documento do cliente',
                ],
                [
                    'tag' => false,
                    'separator' => '|',
                    'item' => function ($index, $label, $name, $checked, $value) use ($post) {
                        $radio = "<label><input class=\"bilheteria_opc_localizar\" name=\"$name\" value=\"$value\" type=\"radio\"";
                        $_checked = $index == 0 && (!isset($post['localizar']) || (isset($post['localizar']) && $post['localizar'] == '1')) ? 'checked="checked"' : 
                                    ($index == 1 && isset($post['localizar']) && $post['localizar'] == 2) ? 'checked="checked"' : '';
                        $_tabindex = $index == 0 ? '1' : '3';
                        $radio .= $_checked." tabindex=\"$_tabindex\"> $label</label>";
                        return $radio;
                    }
                ]
            );
            //atualização
            //return $this->render('//site/atualizando-servidor');
            //</atualização>
            
            return $this->render('ativar', [
                'model' => $modelBilheteria,
                'resultadoBusca' => $resultadoBusca,
                'radioList' => explode('|', $radioList)
            ]);
        }catch (Exception $e){
            return $this->render('error', ['name' => 'Erro ao fazer checagem da venda', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Checar venda
     * Usar o valor de "localizar" passado via POST para pesquisar pelo documento ou pelo id da venda.
     * Mostrar resultado na tela para operador prosseguir
     */
    public function actionCheckVenda() {
        try {
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $post = Yii::$app->request->post('BilheteriaModel');
                
                $pesquisa=$post['localizar']==1 ?VendaWidget::getVenda($post['qrcode_voucher'],'consulta') :VendaWidget::getVendasDocumento($post['documento'],'consulta');
                return (object)['successo' => '1', 'conteudo' => $pesquisa];
            }
            return (new ApiCliente)->getApiError('Faltam parâmetros para checagem da venda.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao fazer checagem da venda', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Registrar retirada do cartão Mifare do cliente
     * Antes de salvar, verifica se o código não foi salvo anteriormente e se faz parte do lote correto
     * @return mixed array com boolean se foi salvo e mensagem adicional
     * @todo exportar para arquivo da catraca após salvar
    */
    public function actionRetirarCartao(){
        try {
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $post = Yii::$app->request->post();
                $statusIngresso = (new ApiIngresso)->consultar(['qrcode' => $post['qrcode'], 'id_agenda' => $post['id_agenda']]);
                if ($statusIngresso->successo == '1' && $statusIngresso->objeto[0]->status == 'L') {
                    $retirar = (new ApiIngresso)->retirar([
                        'id_agenda' => $post['id_agenda'],
                        'qrcode' => $post['qrcode'],
                        'nome' => $post['retira-nome'],
                        'documento' => $post['retira-documento']                   
                    ]);
                    if ($retirar->successo == 1) {
                        return (new ApiCliente)->getApiSuccess('Ingresso retirado com sucesso.');
                    }
                    return (new ApiCliente)->getApiError($retirar->erro->mensagem);
                } else {
                    return (new ApiCliente)->getApiError('Este ingresso já foi ativado. Por favor, verifique.');
                }
            }
            return (new ApiCliente)->getApiError('Faltam parâmetros para registrar retirada do cartão.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao registrar retirada do cartão', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Imprimir filipeta de um produto ou todas de uma venda
    */
    public function actionFilipetas($codigo=null, $tipo='produto', $produto='', $documento='')
    {
        $this->layout = '//print';
        $busca = $codigo > 0 ? (new ApiCarrinho)->buscarProdutosVenda($codigo) : '';
        $produtos = $busca->successo == '1' ? $produtos = $busca->objeto : '';
        $evento = $produtos->produtos[0]->tags ? (new EventosModel)->getEventoByApelido($produtos->produtos[0]->tags) : '';
        return $this->render('filipetas', [
            'produtos' => $produtos,
            'evento' => $evento,
            'tipo' => $tipo,
            'codigoVenda' => $codigo,
            'produtoCodigo' => isset($produto) ? $produto : '',
            'documentoOcupante' => isset($documento) ? $documento : ''
        ]);
    }

    public function actionVendaAvulsa($cancelar=false){
        $carrinho=new ApiCarrinho;
        $parcelas=[''=>'Parcelas'];
        if($cancelar){
            $carrinho->limparCarrinho();
            $this->redirect('venda-avulsa');
        }
        $objCarrinho=$carrinho->consultarCarrinho();
        $erro=false;
        if (Yii::$app->request->post()) {
            $post=Yii::$app->request->post();
            $adicionar=$carrinho->atualizarCarrinho(['produto'=>['codigo'=>$post['produto'],'unidades'=>1]]);
            if(!$adicionar->successo)
                $erro=$adicionar->erro;
            $objCarrinho=$carrinho->consultarCarrinho();
            $maxParcelas=12;
            $initParcela=1;
            while($initParcela<=$maxParcelas){
                $label=$initParcela>1 ?' parcelas ' :' parcela ';
                $parcelas[$initParcela]=$initParcela.$label.'de R$ '.number_format($objCarrinho->objeto->totais->total/$initParcela,2,',','.');
                $initParcela++;
            }
        }
        return $this->render('venda-avulsa',['carrinho'=>$objCarrinho,'erro'=>$erro,'parcelas'=>$parcelas]);
    }

    /* Os métodos abaixo são para nova versão de consulta, utilizada em ingressos da copa e avulsos*/



    /**
     * Método para pesquisa do qr code do ingresso. O formulário envia um código e o model deverá utiliza-o para verificar e criar um objeto Ingresso. Em seguida, o form confirma o ingresso, marcando-o como utilizado na action ValidarIngresso
     */
    public function actionConsultarIngresso() 
    {
        $session = $this->_sessionOpen();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
        $model = new Bilheteria;
        $model->scenario = 'consultar-ingresso';
        if (Yii::$app->request->post('Bilheteria') && $model->load(Yii::$app->request->post())) {
            $consulta = $model->procurarIngresso();
            if (!isset($consulta->successo))
                $model->addError('qrcode', 'Não foi possível tentar localizar o código.');
            if (!$consulta->successo)
                $model->addError('qrcode', $consulta->erro->mensagem);
            if (isset($consulta->successo) && $consulta->successo == '1')
                $model->objConsulta = $consulta->objeto;
            $model->dadosIngressos();
        }
        $agenda = new Agenda($objApiCliente);
        $agenda->listaTodosEventos('MATRIZ');
        $datasEventos = $agenda->listaDatasEventos('MATRIZ');
        return $this->render('validar-entrada', [
            'model' => $model,
            'eventos' => $datasEventos,
        ]);
    }

    /**
    * Método para validar um ingresso, marcando-o como utilizado
    * @todo somente POST
    */
    public function actionValidarIngresso()
    {
        $post=Yii::$app->request->post();
        $model=new Bilheteria;
        if($post) {
            $consulta=$model->marcarIngressoUtilizado($post['id_agenda'],$post['qrcode'],date('d/m/Y H:i:s'));
            if(!isset($consulta->successo))
                $model->addError('qrcode','Não foi possível validar o código.');
            if(!$consulta->successo)
                $model->addError('qrcode',$consulta->erro->mensagem);
            if($model->hasErrors('qrcode'))
                return $this->render('/default/error', ['name' => 'Erro ao tentar marcar o ingresso como utilizado', 'message' => $model->getErrors('qrcode')[0]]);
            if(isset($consulta->successo) && $consulta->successo=='1')
                Yii::$app->session->setFlash('sucesso','Validado com sucesso');
        }
        return $this->redirect(['consultar-ingresso']);
    }
}

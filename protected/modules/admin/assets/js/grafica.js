$(document).ready(function () {
    
    $('.ver-ingresso').click(function () {
        $(this).html($(this).html() == 'Mostrar' ? 'Esconder' : 'Mostrar');
        $('#ingressos-' + $(this).attr('id')).toggleClass('hidden');
    });
    
    $('.btn-producao').click(function () {
        doAjax ($('#url-status').val(), 'POST', {'evento': $(this).attr('data-evento'), 'lote': $(this).attr('data-lote')}, 'json', 10000, 'loteprodOK', 'loteprodErro'); 
        return false;
    });
    
    $('.btn-estadio').click(function () {
        if ($('#lote-grafica-' + $(this).attr('data-lote')).val().length == 0 ||  $('#lote-grafica-' + $(this).attr('data-lote')).val() == '') {
            showMsg('Por favor, informe o identificador do lote da gráfica.');
            $('#lote-grafica-' + $(this).attr('data-lote')).focus();
            return false;
        }
        doAjax ($('#url-status-estadio').val(), 'POST', {'evento': $(this).attr('data-evento'), 'lote': $(this).attr('data-lote'), 'lote_grafica': $('#lote-grafica-' + $(this).attr('data-lote')).val()}, 'json', 10000, 'loteestadioOK', 'loteestadioErro');
        return false;
    });
    
    $('.btn-entrega').click(function () {
        doAjax ($('#url-status-entrega').val(), 'POST', {'evento': $(this).attr('data-evento'), 'lote': $(this).attr('data-lote')}, 'json', 10000, 'loteentregueOK', 'loteentregueErro');
        return false;
    });
    
    $('.btn-libera').click(function () {
        doAjax ($('#url-status-libera').val(), 'POST', {'evento': $(this).attr('data-evento'), 'lote': $(this).attr('data-lote')}, 'json', 10000, 'loteliberadoOK', 'loteliberadoErro');
        return false;
    });
    
    
});

function loteprodOK (data) {
    showMsg('Status do lote modificado para "em Produção".');
    $('#modal-mensagem').on('hidden.bs.modal', function (e) {
        e.preventDefault();
        window.location.href = $('#url-lote-producao').val();
    });
    return false;
}

function loteprodErro (data) {
    showMsg('Falha ao modificar status do lote. Erro: ' + data.erro.mensagem);
    return false;
} 

function loteestadioOK (data) {
    showMsg('Status do lote modificado para "enviado ao Estádio".');
    $('#modal-mensagem').on('hidden.bs.modal', function (e) {
        e.preventDefault();
        window.location.href = $('#url-lote-estadio').val();
    });
    return false;
}

function loteestadioErro (data) {
    showMsg('Falha ao modificar status do lote. Erro: ' + data.erro.mensagem);
    return false;
}

function loteentregueOK (data) {
    showMsg('Status do lote modificado para "entregue ao Estádio".');
    $('#modal-mensagem').on('hidden.bs.modal', function (e) {
        e.preventDefault();
        window.location.href = $('#url-lote-entrega').val();
    });
    return false;
}

function loteentregueErro (data) {
    showMsg('Falha ao modificar status do lote. Erro: ' + data.erro.mensagem);
    return false;
}

function loteliberadoOK (data) {
    showMsg('Status do lote modificado para "liberado para bilheteria".');
    $('#modal-mensagem').on('hidden.bs.modal', function (e) {
        e.preventDefault();
        window.location.href = $('#url-lote-libera').val();
    });
    return false;
}

function loteliberadoErro (data) {
    showMsg('Falha ao modificar status do lote. Erro: ' + data.erro.mensagem);
    return false;
}
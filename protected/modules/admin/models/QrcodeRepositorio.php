<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "qrcode_repositorio".
 *
 * @property integer $id
 * @property string $qrcode
 * @property integer $exportado
 */
class QrcodeRepositorio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'qrcode_repositorio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['qrcode'], 'required'],
            [['exportado'], 'integer'],
            [['qrcode'], 'string', 'max' => 16],
            [['qrcode'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'qrcode' => 'Qrcode',
            'exportado' => 'Exportado',
        ];
    }
}

<?php
namespace app\modules\admin\assets;
use yii\web\AssetBundle;

class MuseuAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/assets';
    
    public $css = [
    ];
    
    public $js = [
        'js/ajax.js',
        YII_ENV_DEV ? 'js/cappta_init_dev.js' : 'js/cappta_init.js',
        'js/museu.js',
        'https://s3.amazonaws.com/cappta.api/v2/dist/cappta-checkout.js'
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];
}

<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\models\ApiCarrinho;
use app\components\MenuWidget;

$session = Yii::$app->session;
$session->open(); 
?>
<div class="container" id="resumo-compra">

    <input type="hidden" id="url-cadastro" value="<?= Url::to(['site/cadastro']); ?>">
    <input type="hidden" id="url-cep" value="<?= Url::to(['site/check-cep']); ?>">
    <input type="hidden" id="url-logout" value="<?= Url::to(['site/logout']); ?>">
    <input type="hidden" id="url-cliente" value="<?= Url::to(['site/cliente']); ?>">
    <input type="hidden" id="url-cartao" value="<?= Url::to(['site/cadastro-cartao']); ?>">
    <input type="hidden" id="url-resumo" value="<?= Url::to(['site/resumo']); ?>">
    <input type="hidden" id="url-cadastro-completo" value="<?= Url::to(['site/cadastro-completo']); ?>">
        
    <div class="row">
        <?php echo MenuWidget::getMenuLateral(); ?>
        
        <div class="col-md-10">
            <div class="tab-content">
                
                <div id="tab-historico">
                    <?= $this->render('resumo-compra-detalhe', [
                        'detalheVenda' => $detalheVenda
                    ]); ?>
                </div>
                
            </div>            
        </div>
    </div>        
</div>

<?php 
$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/cadastro.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset', 'yii\bootstrap\BootstrapPluginAsset']]);

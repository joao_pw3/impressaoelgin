<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\WsAuth;
use app\models\Vendedor;
use app\models\WsConta;

class Cliente extends Model {

    public $unidades;
    public $mapa;
    public $user_token;

    public $user_id;
    public $senha;
    public $senha_repeat;
    public $nome;
    public $razao;
    public $documento;

    const TOKEN_MATRIZ_DEV = '57fa266619b1529ceb39fbc61371fcd3ca599f592d5755f00bfa7c715d3faf8f23f7fc8922fd905c29490140a9ff9203d8b560602790ba331ae60e53d8688f38';
    const TOKEN_MATRIZ_PROD = '57fa266619b1529ceb39fbc61371fcd3ca599f592d5755f00bfa7c715d3faf8f23f7fc8922fd905c29490140a9ff9203d8b560602790ba331ae60e53d8688f38';

    public function attributeLabels(){
        return[
            'user_id'=>'E-mail do gerente',
            'senha'=>'Senha',
            'senha_repeat'=>'Confirme sua Senha',
            'nome'=>'Nome',
            'razao'=>'Razão Social',
            'documento'=>'CNPJ'
        ];
    }

    /**
     * Traz todas as unidades para módulo adm.
     */
    public function listaTodasUnidades() {
        $session = Yii::$app->session;
        if (!$session->isActive)
            $session->open();
        $lista_unidades=$session->get('lista_unidades',false);
        if($lista_unidades){
            $this->unidades=$lista_unidades;
            return;
        }
        /*$ws=new WsAuth;
        $ws->setToken(self::getTokenMatriz(),'V');
        $listar=$ws->listarUnidades();*/
        $ws=new WsConta;
        $listar=$ws->_get(self::getTokenMatriz());
        $this->unidades=[];
        if(!isset($listar->successo) || $listar->successo != '1')
            $this->addError('unidades','Não foi possível trazer a lista de unidades');
        if(!$this->hasErrors('unidades')){
            //foreach ($listar->objeto as $obj) {
                $unidade=new Vendedor;
                $unidade->loadFromApiUnico($listar->objeto);
                $this->unidades[]=$unidade;
            //}
            $session->set('lista_unidades',$this->unidades);
        }
    }

    /**
     * Retorna uma unidade da array pelo índice
     */
    public function getUnidade($ix) {
        return $this->unidades[$ix];
    }

    /**
     * Procurar uma unidade por qq atributo do cadastro (user ou vendedor)
     */
    public function buscarUnidadePor($atributo, $valor) {
        $this->listaTodasUnidades();
        foreach ($this->unidades as $unidade) {
            if ($unidade->$atributo == $valor) {
                $this->unidadeAtual($unidade);
                return $unidade;
            }
        }
        return false;
    }

    /**
     * Cadastrar uma unidade junto com o gerente responsável. Primeiro, cria a conta para o usuário com e-mail e senha e após o retorno do WS com sucesso, atualiza o cadastro do vendedor com nome e CNPJ
     */
    public function criarUnidade($gerente,$unidade){
        $gerente->criarContaGerente();
        if($gerente->hasErrors())
            $this->addErrors($gerente->getErrors());
        if(!$this->hasErrors()){
            $unidade->alterarCadastro($gerente->user_token);
            if(!$this->hasErrors())
                return true;
        }

        return false;
    }

    /**
     * Consultar uma unidade para alternar na aplicação, mudando o que estiver em sessão
     * @param string $documento o documento da unidade a ser retornada
    */
    public function alternarUnidade($documento){
        $this->listaTodasUnidades();
        foreach ($this->unidades as $unidade) {
            if ($unidade->documento == $documento) {
                return $unidade;
            }

        }
    }

    /**
     * Dados da unidade atual com base no vendedor
     * @param Vendedor $vendedor
    */
    public function unidadeAtual($vendedor){        
        $this->user_token=$vendedor->user_token;
        $this->user_id=$vendedor->user_id;
        $this->nome=$vendedor->nome;
        $this->razao=$vendedor->razao;
        $this->documento=$vendedor->documento;
    }

    /**
     * Remover atributos da unidade desta classe
     * @param Vendedor $vendedor
    */
    public function resetUnidade(){        
        $this->user_token='';
        $this->user_id='';
        $this->nome='';
        $this->razao='';
        $this->documento='';
    }
    
    /**
     * Retornar o token de matriz, respeitando o ambiente
     */
    public static function getTokenMatriz()
    {
        return YII_ENV_DEV ? self::TOKEN_MATRIZ_DEV : self::TOKEN_MATRIZ_PROD;
    }

    /**
     * Verificar se a permissão $permissao existe na array $listaPermissoes
     * @param string $permissao a permissão solicitada
     * @param array $listaPermissoes lista de permissões do usuário grava em sessão
     */
    public static function permissaoPara($permissao, $listaPermissoes){

        return array_search($permissao, $listaPermissoes) !== false;
    }

    /**
     * Verificar se tem ao menos um item de um grupo de permissões para exibir aba principal do menu
     * @param string $permissao a permissão solicitada
     * @param array $listaPermissoes lista de permissões do usuário grava em sessão
     */
    public static function grupoPermissao($permissao, $listaPermissoes){
        $return=false;
        foreach ($listaPermissoes as $item) {
            if(stripos($item, $permissao)!== false && stripos($item,$permissao) < stripos($item,'|'))
                $return=true;
        }

        return $return;
    }
}

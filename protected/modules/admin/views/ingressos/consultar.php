<?php

use yii\widgets\ActiveForm;
use app\modules\admin\assets\BilheteriaAsset;
use yii\helpers\Url;
use yii\bootstrap\Modal;

BilheteriaAsset::register($this);

$this->title = 'ADM Estádio Mineirão :: Consultar ingressos';
$session = Yii::$app->session;
$session->open();
?>
<div class="container">

    <div class="row">
        <h2 class="tituloGeral"><i class="fa fa-clone"></i> Consultar ingressos</h2>
    </div>

    <div id="boxFilter" class="boxLer2">
        <?php
        $form = ActiveForm::begin([
            'id' => 'form-consultar-ingresso',
        ]);
        ?>
        <div id="localizar" class="row consBusca2">
            <div class="ingresso col-md-3">
                <div class="semboxBorder" id="boxBorder">
                    <label for="idEvento">Eventos ativos no estádio</label>
                    <select name="BilheteriaModel[idEvento]">
                    <?php foreach ($eventos as $index => $evento) { ?>
                        <option value="<?= $evento->codigo; ?>"<?= isset($post['idEvento']) && $post['idEvento'] == $evento->codigo ? ' selected' : ''; ?>><?= $evento->nome; ?></option>
                    <?php } ?>
                    </select>
                </div>
            </div>
            <div class="ingresso col-md-9">
                <div class="ingresso col-md-4" id="boxBorder2">
                    <div class="boxBorder2int">
                        <?= $form->field($model, 'produto')->textInput(['id' => 'produto', 'value' => isset($post['produto']) && !empty($post['produto']) ? $post['produto'] : '']); ?>
                    </div>
                    <div class="boxBorder2int">
                        <?php if (isset($post['status']) && !empty($post['status'])) {
                            $model->status = $post['status'];
                        } ?>
                        <?= $form->field($model, 'status')->dropdownList($model->statusIngresso, ['id' => 'status']); ?>
                    </div>
                </div>
                <div class="ingresso col-md-4">
                    <div class="boxBorder2int">
                        <?= $form->field($model, 'qrcode_voucher')->textInput(['id' => 'qrcode_voucher', 'value' => isset($post['qrcode_voucher']) && !empty($post['qrcode_voucher']) ? $post['qrcode_voucher'] : '']); ?>
                    </div>
                    <div class="boxBorder2int">					
                        <?= $form->field($model, 'lote')->textInput(['id' => 'lote', 'value' => isset($post['lote']) && !empty($post['lote']) ? $post['lote'] : '']); ?>
                    </div>
                </div>
                <div class="ingresso col-md-4">	
                    <div class="boxBorder2int">
                        <?= $form->field($model, 'documento')->textInput(['id' => 'documento', 'value' => isset($post['documento']) && !empty($post['documento']) ? $post['documento'] : '']); ?>
                    </div>
                    <div class="boxBorder2int">					
                        <?= $form->field($model, 'comprador')->textInput(['id' => 'comprador', 'value' => isset($post['comprador']) && !empty($post['comprador']) ? $post['comprador'] : '']); ?>
                    </div>
                </div>
                <button type="submit" id="consultar-ingresso" class="btn btn-success btn-consulta"><i class="fa fa-check"></i> Buscar</button>  
                <i class="fa fa-angle-double-down boxTopRight" id="fechaboxFilter"></i>
                <i class="fa fa-angle-double-up boxTopRight esconde" id="abreboxFilter"></i>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

    <hr class="bordaPont2">

    <div class="row">
        <div class="col-md-12" id="data-venda">
<?php
if (!empty($resultadoBusca) && $resultadoBusca->successo == '1') {
    if (count($resultadoBusca->objeto) > 0) {
?>
            <input type="hidden" id="url-invalidar-ingresso" value="<?= Url::to(['ingressos/invalidar-ingresso']); ?>">
            <input type="hidden" id="url-2via-ingresso" value="<?= Url::to(['ingressos/segunda-via-ingresso']); ?>"> 
            <?php if($btExportarCsv) {?>
                <p class="text-right">
                    <a class="btn btn-primary" href="<?=Url::to(['grafica/exportar-csv-sem-lote', 'evento'=>$post['idEvento'], 'status'=>$post['status']])?>">Exportar CSV</a>
                </p>
            <?php }?>
            <table class="table table-responsive tableInfo">
                <thead>
                    <tr>
                        <th>Qrcode</th>
                        <th>Ativo</th>
                        <th>Status</th>
                        <th>Lote</th>
                        <th>Lote gráfica</th>
                        <th>Produto</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>            
        <?php
        foreach ($resultadoBusca->objeto as $index => $ingresso) {
            if (!isset($post['produto']) || empty($post['produto']) || (isset($post['produto']) && !empty($post['produto']) && stristr($ingresso->produto, $post['produto']))) {
        ?>
                    <tr>
                        <td><?= $ingresso->qrcode; ?></td>
                        <td class="uppercase"><?= $ingresso->bloqueado == 0 ? 'Sim' : 'Não'; ?></td>
                        <td class="uppercase"><?= $model->statusIngresso[$ingresso->status]; ?></td>
                        <td><?= $ingresso->lote_reserva; ?></td>
                        <td><?= $ingresso->lote_entrega; ?></td>
                        <td class="uppercase"><?= $ingresso->produto; ?></td>
                        <td><a href="javascript:;" class="ingresso-detalhe" id="<?= $ingresso->qrcode; ?>"><i class="fa fa-caret-down setaDown seta<?= $ingresso->qrcode; ?>"></i></a></td>
                    </tr>
                    <tr class="uppercase hidden ingresso-linha" id="tr-<?= $ingresso->qrcode; ?>">
                        <td colspan="7">
                            <div class="row boxLerIngresso2">
                                <div class="col-xs-6">
                                    <label>Ocupante</label>
                                    <div class="col-xs-6">Nome</div>
                                    <div class="col-xs-6"><?= !empty($ingresso->ocupante_nome) ? $ingresso->ocupante_nome : '-'; ?></div>
                                    <div class="col-xs-6">Documento</div>
                                    <div class="col-xs-6"><?= !empty($ingresso->documento) ? $ingresso->documento : '-'; ?></div>
                                </div>
                                <div class="col-xs-6">
                                    <label for="">Retirada</label>
                                    <div class="col-xs-6">Nome</div>
                                    <div class="col-xs-6"><?= !empty($ingresso->retirado_nome) ? $ingresso->retirado_nome : '-'; ?></div>
                                    <div class="col-xs-6">Documento</div>
                                    <div class="col-xs-6"><?= !empty($ingresso->retirado_documento) ? $ingresso->retirado_documento : '-'; ?></div>
                                </div>
                                <div class="col-xs-6">
                                    <label for="">Venda</label>
                                    <div class="col-xs-6">Identificador</div>
                                    <div class="col-xs-6"><?= $ingresso->codigo_venda; ?></div>
                                    <div class="col-xs-6">Data</div>
                                    <div class="col-xs-6"><?= $ingresso->data_venda; ?></div>
                                    <div class="col-xs-6">Comprador</div>
                                    <div class="col-xs-6"><?= $ingresso->comprador; ?></div>
                                    <div class="col-xs-6">Documento</div>
                                    <div class="col-xs-6"><?= $ingresso->comprador_documento; ?></div>
                                </div>
                                <div class="col-xs-6">
                                    <label for="">Histórico</label>
                                    <div class="col-xs-6">Reservado</div>
                                    <div class="col-xs-6"><?= !empty($ingresso->data_reserva) ? date('d/m/Y H:i:s', strtotime($ingresso->data_reserva)) : '-'; ?></div>
                                    <div class="col-xs-6">Em produção</div>
                                    <div class="col-xs-6"><?= !empty($ingresso->data_producao) ? date('d/m/Y H:i:s', strtotime($ingresso->data_producao)) : '-'; ?></div>
                                    <div class="col-xs-6">Enviado ao estádio</div>
                                    <div class="col-xs-6"><?= !empty($ingresso->data_envio) ? date('d/m/Y H:i:s', strtotime($ingresso->data_envio)) : '-'; ?></div>
                                    <div class="col-xs-6">Entregue ao estádio</div>
                                    <div class="col-xs-6"><?= !empty($ingresso->data_entrega) ? date('d/m/Y H:i:s', strtotime($ingresso->data_entrega)) : '-'; ?></div>
                                    <div class="col-xs-6">Associado ao mifare</div>
                                    <div class="col-xs-6"><?= !empty($ingresso->data_associacao) ? date('d/m/Y H:i:s', strtotime($ingresso->data_associacao)) : '-'; ?></div>
                                    <div class="col-xs-6">Liberado para bilheteria</div>
                                    <div class="col-xs-6"><?= !empty($ingresso->data_liberado) ? date('d/m/Y H:i:s', strtotime($ingresso->data_liberado)) : '-'; ?></div>
                                    <div class="col-xs-6">Retirado pelo cliente</div>
                                    <div class="col-xs-6"><?= !empty($ingresso->data_retirado) ? date('d/m/Y H:i:s', strtotime($ingresso->data_retirado)) : '-'; ?></div>
                                    <div class="col-xs-6">Inválido</div>
                                    <div class="col-xs-6"><?= !empty($ingresso->data_invalidado) ? date('d/m/Y H:i:s', strtotime($ingresso->data_invalidado)) : '-'; ?><?= !empty($ingresso->motivo_invalidado) ? '<br>Motivo: ' . $ingresso->motivo_invalidado : ''; ?></div>
                                    <div class="col-xs-6">Segunda via solicitada</div>
                                    <div class="col-xs-6"><?= !empty($ingresso->data_segunda_via) ? date('d/m/Y H:i:s', strtotime($ingresso->data_segunda_via)) : '-'; ?></div>
                                </div>

                                <?php if ($ingresso->status == 'T' && (in_array('ingressos/invalidar-ingresso', $session['permissoes_usuario']) || in_array('ingressos/segunda-via-ingresso', $session['permissoes_usuario']))) { ?>
                                <div class="col-xs-12" align="center">
                                    <?php if (in_array('ingressos/invalidar-ingresso', $session['permissoes_usuario'])) { ?>
                                    <button class="btn btn-primary btn-invalidar" data-qrcode="<?= $ingresso->qrcode; ?>" data-evento="<?= $post['idEvento']; ?>" id="btni-<?= $ingresso->qrcode; ?>">Invalidar ingresso</button>
                                    <?php } ?>
                                    <?php if (in_array('ingressos/segunda-via-ingresso', $session['permissoes_usuario'])) { ?>
                                    <button class="btn btn-primary btn-seg-via" data-qrcode="<?= $ingresso->qrcode; ?>" data-evento="<?= $post['idEvento']; ?>" id="btns-<?= $ingresso->qrcode; ?>">Solicitar 2ª via</button>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                            </div>
                        </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                        </tbody>
                    </table>  
            <?php } else { ?>
                <div class="alert alert-warning">Não há compra para as informações fornecidas.</div>
            <?php }
            } else if (!empty(!empty($resultadoBusca)) && $resultadoBusca->successo == '0') {
            ?>
                <div class="alert alert-warning"><?= $resultadoBusca->erro->mensagem; ?>.</div>
<?php } else { ?>
                <div class="alert alert-warning">Preencha os filtros acima para efetuar a busca.</div>
<?php } ?>
        </div>
    </div>

</div>

<?php
Modal::begin([
    'header' => '<h4 align="center">Invalidar ingresso</h4>',
    'size' => 'modal-md',
    'id' => 'modal-invalidar',
    'options' => [
        'class' => 'modal-center'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ]
]);
?>
<div class="content">
    <div clas="row">
        <form method="post" name="form-invalidar">
            <label for="motivo">Motivo:</label>
            <textarea name="motivo" id="motivo" rows="3" cols="60" autofocus></textarea>
            <input type="hidden" name="id_agenda" value="<?= isset($post['idEvento']) && !empty($post['idEvento']) ? $post['idEvento'] : ''; ?>">
            <div class="col-xs-12" align="center">
                <button type="button" class="btn btn-danger" id="btni-cancel">Cancelar</button>
                <button type="button" class="btn btn-primary btnAzEscuro" id="btni-continuar">Invalidar</button>
            </div>
        </form>        
    </div>
</div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'header' => '<h4 align="center">Atenção</h4>',
    'size' => 'modal-md',
    'id' => 'modal-confirmar',
    'options' => [
        'class' => 'modal-center'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ],
    'footer' => '
       <div class="col-md-12" align="center">
           <button type="button" class="btn btn-danger" id="btn-cancel">Cancelar</button>
           <button type="button" class="btn btn-success" id="btn-continue">Continuar</button>
       </div>'
]); ?>
<?php Modal::end(); ?>
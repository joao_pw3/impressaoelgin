<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin();
echo $form->field($model, 'nome')->textInput(['maxlength' => true]);
//alternar os atributos do campo e-mail para ser readonly no update
$emailAttr=$model->scenario=='update' ?['maxlength' => true, 'readonly'=>'readonly'] :['maxlength' => true];
echo $form->field($model, 'email')->textInput($emailAttr);
echo $form->field($model, 'senha')->passwordInput(['maxlength' => true, 'placeholder'=>$model->scenario=='update' ?'Preencha somente se quiser alterar':'']);
//mensagem somente se estiver em default, que nesse caso é novo cadastro
if($model->scenario=='default')
	echo $form->field($model, 'mensagem')->textArea(['placeholder'=>'Olá! Você foi cadastrado como operador. Utilize o seu email ([EMAIL]) como usuário, sua senha de acesso é [SENHA].'])->label('Mensagem (ao preencher, utilize as palavras-chave [EMAIL] e [SENHA] conforme o exemplo. A mensagem será enviada ao e-mail cadastrado)');
?>
<div class="form-group">
    <?= Html::submitButton('Continuar' , ['class'=>'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>
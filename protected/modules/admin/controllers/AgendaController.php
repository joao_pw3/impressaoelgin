<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\controllers\AbstractController;
use app\models\Agenda;
use app\models\Tags;
use app\models\Anexos;
use yii\web\UploadedFile;
use app\models\Cliente;
use app\models\Produto;
use app\models\ImagemEvento;
use app\models\ImagemMosaico;
use app\models\ImagemDestaque;
use app\models\ImagemDestaqueApp;
use app\models\Cupom;

/**
 * Controle de agenda
 */
class AgendaController extends AbstractController
{
    
    /**
     * Renders the index view for the module
     * @param boolean $matriz Boolean indicando se é uma consulta para matriz ou não
     * @return string
     */
    public function actionIndex($matriz=false)
    {
        $session = $this->_sessionOpen();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $objApiCliente->getTokenMatriz());
        $model = new Agenda($objApiCliente);
        (boolean)($matriz) ? $model->buscarEventosPorFiltro(['nome' => 'MATRIZ%']) : $model->listaTodosEventos('MATRIZ');
        return $this->render('index', ['agenda' => $model, 'matriz' => $matriz]);
    }

    /**
     * Criar novo evento usando ActiveForm e a classe model correspondente - método apenas cria o básico da agenda, não usar a não ser em último caso
     */
    public function actionNovoEventoSimples()
    {
        $session = $this->_sessionOpen();
        $objApiCliente=new Cliente;
        $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
        $model=new Agenda($objApiCliente);
        $model->scenario='novoEvento';
        if (Yii::$app->request->post('Agenda')) {
            $model->load(Yii::$app->request->post());
            if($model->criarEvento($model->unidade->user_token))
                return $this->redirect(['index']);
        }
        return $this->render('criar', ['model' => $model, 'matriz' => Yii::$app->request->get('matriz')]);
    }

    /**
     * Criar novo evento usando ActiveForm e a classe model correspondente
     * @todo clonarCupom: CUPOM BASE DE CÓPIA ESTÁ HARDCODED - IDENTIFICAR VIA PROGRAMAÇÃO
     */
    public function actionNovoEvento()
    {
        $session = $this->_sessionOpen();
        $objApiCliente=new Cliente;
        $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
        $model=new Agenda($objApiCliente);
        $model->scenario='novoEventoCompleto';
        $model->ativo='0';
        $model->listaEspacos();
        $modelImgEvento=new ImagemEvento;
        $modelImgDestaque=new ImagemDestaque;
        $modelImgDestaqueApp=new ImagemDestaqueApp;

        if (Yii::$app->request->post('Agenda')) {
            $model->load(Yii::$app->request->post());
            $model->imgEvento= Yii::$app->request->post('ImagemEvento') ?'upload' :null;
            if($model->criarEvento()){
                //salvar tag de destaque
                $model->salvarTagDestaque();
                //salvar imagens como anexo
                if(!$this->salvarAnexo($modelImgEvento,Yii::$app->request->post(),$model))
                    $model->addError('imgEvento', 'Erro salvando imagem! Verifique o arquivo enviado');
                if($model->destaque && !$this->salvarAnexo($modelImgDestaque,Yii::$app->request->post(),$model))
                    $model->addError('imgDestaque', 'Erro salvando imagem! Verifique o arquivo enviado');
                if($model->destaque && !$this->salvarAnexo($modelImgDestaqueApp,Yii::$app->request->post(),$model))
                    $model->addError('imgDestaqueApp', 'Erro salvando imagem! Verifique o arquivo enviado');
                //salvar a data inicial como primeira data, clonando o mapa de assentos e setando os preços
                $addData=$model->adicionarDataEvento($model->dataInicial);
                
                if(!$addData->successo)
                    $model->addError('dataInicial',$addData->erro->mensagem);
                if($addData->successo){
                    
                    $tmp = new Agenda($objApiCliente);
                    $filtro = Yii::$app->request->post('Agenda')['tipoEvento'] == '1' ? ['nome' => 'MATRIZ-MARCADO%'] : ['nome' => 'MATRIZ-LIVRE%'];
                    $filtro['tags'] = [['nome' => 'Espaço', 'valor' => $model->espaco]];
                    $tmp->buscarEventosPorFiltro($filtro);
                    if (!isset($tmp->lista[0]->datas[0]->id)) {
                        die('Não há evento matriz para clonar produto.');
                    }
                    
                    $tmpTags = $tmp->clonarTags(['de' => $tmp->lista[0]->codigo, 'para' => $model->codigo]);
                    if ($tmpTags->successo == '0') {
                        $model->addError('tags', 'Falha ao clonar tags do evento matriz');
                    }
                    if ($tmpTags->successo == '1') {
                        $produtosModel = new Produto($objApiCliente);
                        $clonar = $produtosModel->clonarProdutoAgenda($tmp->lista[0]->datas[0]->id, $addData->objeto->codigo);

                        if(!$clonar->successo)
                            $model->addError('dataInicial',$clonar->erro->mensagem);
                        if($clonar->successo){
                            $cupom = new Cupom($objApiCliente);
                            $clonarCupom = $cupom->clonarCupom($tmp->lista[0]->codigo . '_' . $tmp->lista[0]->datas[0]->id, $model->codigo . '_' . $addData->objeto->codigo);

                            if ($clonarCupom->successo == '0') {
                                $model->addError('cupom', $clonarCupom->erro['mensagem']);
                            }
                            if ($clonarCupom->successo == '1') {
                                $post=[
                                    'filtro'=>[
                                        'id_agenda'=>$addData->objeto->codigo
                                    ],
                                    'alterar'=>[
                                        'valor'=>$model->preco
                                    ]
                                ];
                                $alterar=$produtosModel->alterarEmLoteSemValidacao($post);

                                if(!$alterar->successo)
                                    $model->addError('preco',$alterar->erro->mensagem);                     
                            }
                        }
                    }
                }
                if(!$model->hasErrors())
                    $this->redirect(['registro-evento','evento'=>$model->codigo]);
            }
        }
        return $this->render('criar-completo',['model'=>$model,'modelImgEvento'=>$modelImgEvento,'modelImgDestaque'=>$modelImgDestaque,'modelImgDestaqueApp'=>$modelImgDestaqueApp]);
    }

    /**
     * load model - trazer um registro em model a partir de um parâmetro
     * @param int $evento o código do evento a ser buscado
     */
    private function loadModel($evento){
        $session = $this->_sessionOpen();
        $objApiCliente=new Cliente;
        $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
        $model=new Agenda($objApiCliente);
        $model->codigo=$evento;
        $model->buscarEvento();
        return $model;
    }

    /**
     * Mostrar um registro de um evento
     * @param int $evento o código do evento
     * acessar: localhost/cliente/web/admin/agenda/registro-evento?evento=[num]
     */
    public function actionRegistroEvento($evento){
        $model=$this->loadModel($evento);
        if($model->hasErrors())
            return $this->render('/default/error',['name'=>'Erro buscando evento','message'=>'O evento não pôde ser encontrado com este ID ('.$evento.')']);
        return $this->render('view',['evento'=>$model]);
    }

    /**
     * Mostrar um registro de um evento
     * @param int $evento o código do evento
     */
    public function actionEditarEvento($evento){
        $model=$this->loadModel($evento);
        $model->scenario='update';
        if($model->hasErrors())
            return $this->render('/default/error',['name'=>'Erro buscando evento','message'=>'O evento não pôde ser encontrado com este ID ('.$evento.')']);
        if (Yii::$app->request->post('Agenda')) {
            $model->load(Yii::$app->request->post());
            if($model->atualizarEvento($model->unidade->user_token))
                return $this->redirect(['registro-evento','evento'=>$evento]);
        }
        return $this->render('editar',['model'=>$model]);
    }

    /**
     * Remover um evento
     */
    public function actionExcluir($evento){
        $model=$this->loadModel($evento);
        if($model->hasErrors())
            return $this->render('/default/error',['name'=>'Erro buscando evento','message'=>'O evento não pôde ser encontrado com este ID ('.$evento.')']);
        if (Yii::$app->request->post('Agenda')) {
            $ex=$model->excluirEvento($model->unidade->user_token);
            if(!$ex->successo)
                $model->addError('codigo','Não foi possível excluir o evento - '.$ex->erro->mensagem);
            else return $this->redirect(['index']);
        }
        return $this->render('excluir',['evento'=>$model]);
    }

    /**
     * Adicionar tags a um evento na agenda
     * @param int $evento o código do evento
     */
    public function actionAdicionarTag($evento){
        $model=$this->loadModel($evento);
        if($model->hasErrors())
            return $this->render('/default/error',['name'=>'Erro buscando evento','message'=>'O evento não pôde ser encontrado com este ID ('.$evento.')']);
        $modelTags=new Tags;
        $modelTags->scenario='create';
        $modelTags->load(Yii::$app->request->post());
        if(Yii::$app->request->post('Tags') && $modelTags->adicionar($model->getWs(),$model->codigo))
            return $this->redirect(['registro-evento','evento'=>$evento]);
        
        return $this->render('add-tag',['evento'=>$model,'model'=>$modelTags]);
    }

    /**
     * Remover tags a um evento na agenda
     * @param int $evento o código do evento
     * @param string $nomeTag o nome da tag a ser removida
     */
    public function actionRemoverTags($evento,$nomeTag=false){
        $model=$this->loadModel($evento);
        if($model->hasErrors())
            return $this->render('/default/error',['name'=>'Erro buscando evento','message'=>'O evento não pôde ser encontrado com este ID ('.$evento.')']);
        if($nomeTag){
            if($model->removerTag($nomeTag,$model->unidade->user_token))
                return $this->redirect(['registro-evento','evento'=>$evento]);
        }
        $modelTags=new Tags;
        $modelTags->scenario='lista';
        $modelTags->listFromObjTags($model->tags);

        return $this->render('remover-tags',['tags'=>$modelTags, 'evento'=>$model]);
    }

    /**
     * Adicionar anexos (upload) a um evento na agenda
     * @param int $evento o código do evento
     */
    public function actionAdicionarAnexo($evento){
        $model=$this->loadModel($evento);
        if($model->hasErrors())
            return $this->render('/default/error',['name'=>'Erro buscando evento','message'=>'O evento não pôde ser encontrado com este ID ('.$evento.')']);
        $objAnexo=new Anexos;
        if(Yii::$app->request->post('Anexos'))
            $this->salvarAnexo($objAnexo, Yii::$app->request->post(), $model);
        return $this->render('add-anexo',['evento'=>$model,'model'=>$objAnexo]);
    }

    /**
     * Salvar um anexo enviado por form relacionado a um evento
     * @param object $modelAnexos a classe model do anexo
     * @param array $post objeto enviado por formulário
     * @param object $modelAgenda a classe model do evento
     */
    private function salvarAnexo($modelAnexos,$post,$modelAgenda){
        $modelAnexos->load($post);
        if(UploadedFile::getInstance($modelAnexos, 'objeto')==null) return false;
        $modelAnexos->imgBase64(UploadedFile::getInstance($modelAnexos, 'objeto'));            
        if($modelAnexos->validate()){
            $addAnexo=$modelAnexos->adicionar($modelAgenda->getWs(),$modelAgenda->codigo);
            if($addAnexo->successo){
                return $modelAnexos->acaoAposSalvar($modelAnexos,$post,$modelAgenda);
                /*echo "<pre>";
                echo 'salvarAnexo <br />';
                print_r($modelAnexos);
                echo "</pre>";*/
                
            } else $modelAnexos->addError('nome',$addAnexo->erro->mensagem);
        }
    }

    /**
     * Remover anexos a um evento na agenda
     * @param int $evento o código do evento
     * @param string $nomeAnexo o nome do anexo a ser removido
     * @param string $tipoAnexo o tipo do anexo a ser removido
     */
    public function actionRemoverAnexos($evento,$nomeAnexo=false,$tipoAnexo=false){
        $model=$this->loadModel($evento);
        if($model->hasErrors())
            return $this->render('/default/error',['name'=>'Erro buscando evento','message'=>'O evento não pôde ser encontrado com este ID ('.$evento.')']);
        if($nomeAnexo&&$tipoAnexo){
            if($model->removerAnexo($nomeAnexo,$tipoAnexo,$model->unidade->user_token))
                return $this->redirect(['registro-evento','evento'=>$evento]);
        }
        $modelAnexos=new Anexos;
        $modelAnexos->scenario='lista';
        $modelAnexos->listFromObjAnexos($model->anexos);

        return $this->render('remover-anexos',['anexos'=>$modelAnexos, 'evento'=>$model]);
    }

    /**
     * Gravar uma nova data para um evento
     * @param int $evento o código do evento
     */
    public function actionAdicionarData($evento){
        $model=$this->loadModel($evento);
        if($model->hasErrors())
            return $this->render('/default/error',['name'=>'Erro buscando evento','message'=>'O evento não pôde ser encontrado com este ID ('.$evento.')']);
        $model->scenario='novaData';        
        if(Yii::$app->request->post('Agenda')){
            $post=Yii::$app->request->post('Agenda');
            $addData = $model->adicionarDataEvento($post['data'],$model->unidade->user_token);
            if($addData->successo){
                $tmp = new Agenda($model->unidade);
                $filtro=Yii::$app->request->post('Agenda')['tipoEvento'] == '1' ? ['nome' => 'MATRIZ-MARCADO%'] : ['nome' => 'MATRIZ-LIVRE%'];
                $filtro['tags']=[['nome'=>'Espaço','valor'=>$model->espaco]];
                $tmp->buscarEventosPorFiltro($filtro);
                if (!isset($tmp->lista[0]->datas[0]->id)) {
                    die('Não há evento matriz para clonar produto.');
                }
                
                $produtosModel = new Produto($model->unidade);
                $clonar = $produtosModel->clonarProdutoAgenda($tmp->lista[0]->datas[0]->id, $addData->objeto->codigo);
                if ($clonar->successo) {
                    $cupom = new Cupom($model->unidade);
                    $clonarCupom = $cupom->clonarCupom($tmp->lista[0]->codigo . '_' . $tmp->lista[0]->datas[0]->id, $model->codigo . '_' . $addData->objeto->codigo);
                    if ($clonarCupom->successo == '0') {
                        $model->addError('cupom', $clonarCupom->erro->mensagem);
                    }
                    //clonar tags do evento matriz para o novo evento
                    $tmpTags = $tmp->clonarTags(['de' => $tmp->lista[0]->codigo, 'para' => $model->codigo]);
                    if ($tmpTags->successo == '0') {
                        $model->addError('tags', $tmpTags->erro->mensagem);
                    }
                    return $this->redirect(['registro-evento', 'evento' => $evento]);
                }
                $model->addError('data', $clonar->erro->mensagem);
            }
            if(!$addData->successo)
                $model->addError('data',$addData->erro->mensagem);
        }
        return $this->render('add-data',['model' => $model]);
    }

    /**
     * Remover datas de um evento na agenda
     * @param int $evento o código do evento
     * @param string $data a data a ser removida
     */
    public function actionRemoverDatas($evento,$data=false){
        $model=$this->loadModel($evento);
        if($model->hasErrors())
            return $this->render('/default/error',['name'=>'Erro buscando evento','message'=>'O evento não pôde ser encontrado com este ID ('.$evento.')']);
        if($data){
            if($model->removerData($data))
                return $this->redirect(['registro-evento','evento'=>$evento]);
        }

        return $this->render('remover-datas',['evento'=>$model]);
    }
}

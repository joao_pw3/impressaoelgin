<?php
namespace app\modules\admin\assets;
use yii\web\AssetBundle;
use yii\web\View;

class VendasAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/assets';
    
    public $css = [
    ];
    
    public $jsOptions = [
        'position' => View::POS_END
    ];
    
    public $js = [
        'js/ajax.js',
        'js/vendas.js'
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    
    public $publishOptions = [
        'forceCopy' => true
    ];
    
}

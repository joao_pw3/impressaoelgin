<?php 
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\widgets\MaskedInput;
use yii\helpers\Url;
use kartik\money\MaskMoney;

$this->title = 'ADM Estádio Mineirão :: Boleto';
?>

<div class="container">
    <div class="row">
        <h2 class="tituloGeral"><i class="fa fa-clone"></i> Boleto</h2>
    </div>
	<?php if(isset($mensagem)) { ?>
		<div class="alert"><?php echo $mensagem; ?></div>
	<?php } else { 
	
		$form = ActiveForm::begin([
	        'id' => 'form-boleto',
	    ]); 
	    ?>

			<div class="row">

				<div class="col-md-12">

					<h2>Dados do Sacado</h2>
				</div>
			</div>

			<div class="row">

				<div class="col-md-5">
					
		            <?= $form->field($boleto, 'nome')->textInput([
		                'class' => 'form-control'
		            ]); ?>
				</div>

				<div class="col-md-3">

		            <?= $form->field($boleto, 'documento')->textInput()
		                ->widget(MaskedInput::className(), [
		                    'mask' => ['999.999.999-99', '99.999.999/9999-99'],
		                    'options' => [
		                        'class' => 'form-control',
		                        'clean' => false,
		                        'placeholder' => 'CPF ou CNPJ',
		                    ]
		            ]); ?>		
				</div>

				<div class="col-md-4">

		            <?= $form->field($boleto, 'email')->textInput([
		                'class' => 'form-control'
		            ]); ?>					
				</div>			
			</div>
			
			<div class="row">

				<div class="col-md-3">
		            <?= $form->field($boleto, 'cep')->textInput()
		                ->widget(MaskedInput::className(), [
		                    'mask' => '99999-999',
		                    'options' => [
		                        'class' => 'form-control',
		                        'placeholder' => 'CEP',
		                        'id' => 'cep'
		                    ]
		            ]); ?>
				</div>

			</div>

			<div class="row">

				<div class="col-md-6">

		            <?= $form->field($boleto, 'endereco')->textInput([
		                'class' => 'form-control',
		                'id' => 'endereco'
		            ]); ?>	
				</div>

				<div class="col-md-3">

		            <?= $form->field($boleto, 'numero')->textInput([
		                'class' => 'form-control'
		            ]); ?>	
				</div>

				<div class="col-md-3">

		            <?= $form->field($boleto, 'complemento')->textInput([
		                'class' => 'form-control'
		            ]); ?>	

				</div>
			</div>

			<div class="row">

				<div class="col-md-4">

		            <?= $form->field($boleto, 'bairro')->textInput([
		                'class' => 'form-control',
		                'id' => 'bairro'
		            ]); ?>	
				</div>

				<div class="col-md-6">

		            <?= $form->field($boleto, 'municipio')->textInput([
		                'class' => 'form-control',
		                'id' => 'cidade_nome'
		            ]); ?>	
				</div>

				<div class="col-md-2">

		            <?= $form->field($boleto, 'uf')->textInput([
		                'class' => 'form-control',
		                'id' => 'uf'
		            ]); ?>	
				</div>

			</div>

			<div class="row">

				<div class="col-md-12">

					<h2>Dados do Boleto</h2>
				</div>
			</div>

			<div class="row">

				<div class="col-md-4">

					<?php echo $form->field($boleto, 'valor')->textInput()
							->widget(MaskMoney::classname(), [
						    	'pluginOptions' => [
			                        'prefix' => 'R$ ',
			                        'thousands' => '.',
			                        'decimal' => ',',
			                        'allowNegative' => false
						    	],
			                   'options' => [
			                        'placeholder' => 'R$ 0,00'
			                    ]						    	
							]);
					?>
				</div>

				<div class="col-md-4">		

	            	<?= $form->field($boleto, 'vencimento')->textInput()
	                		->widget(DatePicker::className(), [
	                    		'dateFormat' => 'php:d/m/Y',
	                    		'clientOptions' => [
	                        	'changeMonth' => true,
	                        	'changeYear' => true,
	                    	]
	                	])
	                ->widget(MaskedInput::className(), [
	                    'mask' => '99/99/9999',
	                    'options' => [
	                        'class' => 'form-control',
	                    ],
	            	]); ?>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">

		            <?= $form->field($boleto, 'mensagem')->textArea([
		                'class' => 'form-control',
		                'rows' => 5
		            ]); ?>	
				</div>
			</div>

			<div class="row">

				<input type="hidden" id="url-cep" value="<?= Url::to(['/site/check-cep']); ?>">
			</div>

			<div class="row">

				<div class="col-md-3">
					<button type="submit" class="btn btn-primary">Enviar</button>
				</div>
			</div>
		<?php ActiveForm::end(); 
 	} ?>
</div>




<?php

$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);

$this->registerJs("
	$(document).on('input', '#cep', function () {
		if ($(this).val().indexOf('_') == -1) {
        	checkCep(this.value);
    	}
    });
    
   function checkCep(cep) {
       doAjax($('#url-cep').val(), 'POST', {
           'cep': cep
        }, 'JSON', 5000, 'checkCepOK', 'checkCepErr');
        return false;
  	}
    
    function checkCepErr(data) {
       $('#modal-mensagem .modal-body').html('Este CEP não foi localizado. Por favor, verifique.');
        $('#modal-mensagem').modal('toggle');
    }

	function checkCepOK(data) {
	    $('#endereco').val(data.objeto.endereco).prop('readonly', data.objeto.endereco == '' ? false : true);
	    $('#bairro').val(data.objeto.bairro).prop('readonly', data.objeto.bairro == '' ? false : true);
	    $('#cidade_nome').val(data.objeto.cidade_nome);
	    $('#uf').val(data.objeto.uf);
	    if (data.objeto.cidade === '') {
	        checkCepErr(data);
	    }
	}

	function checkCepErr(data) {
	    $('#modal-mensagem .modal-body').html('Este CEP não foi localizado. Por favor, verifique.');
	    $('#modal-mensagem').modal('toggle');
	}    
", \yii\web\View::POS_END); ?>

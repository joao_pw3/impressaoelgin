var mapa=[
    {
        "codigo": "15",
        "nome": "Oeste",
        "descricao": "",
        "valor": "0.00",
        "desconto": {
            "fixo": "0.00",
            "percentual": "0.00"
        },
        "estoque": {
            "oferecido": "",
            "vendido": "0.00",
            "reservado": "0.00",
            "disponivel": ""
        },
        "ativo": "1",
        "tags": [
            {
                "nome": "cor",
                "valor": "roxa"
            },
            {
                "nome": "tipo",
                "valor": "area"
            },
            {
                "nome": "matriz",
                "valor": "base"
            }
        ],
        "blocos": [
            {
                "codigo": "16",
                "nome": "Bloco 101 Oeste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "189.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "189.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Oeste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "101"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-136.txt"
                    }
                ],
                "assentos": [
                    {
                        "codigo": "617",
                        "nome": "Tribuna A1 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "3.00",
                            "reservado": "0.00",
                            "disponivel": "-2.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "A"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "1"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "618",
                        "nome": "Tribuna A2 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "4.00",
                            "reservado": "0.00",
                            "disponivel": "-3.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "A"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "2"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "619",
                        "nome": "Tribuna A3 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "A"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "3"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "620",
                        "nome": "Tribuna A4 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "A"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "4"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "621",
                        "nome": "Tribuna A5 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "A"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "5"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "622",
                        "nome": "Tribuna A6 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "A"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "6"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "623",
                        "nome": "Tribuna A7 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "A"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "7"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "624",
                        "nome": "Tribuna B1 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "1.00",
                            "reservado": "0.00",
                            "disponivel": "0.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "B"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "1"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "625",
                        "nome": "Tribuna B2 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "1.00",
                            "reservado": "0.00",
                            "disponivel": "0.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "B"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "2"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "626",
                        "nome": "Tribuna B3 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "1.00",
                            "reservado": "0.00",
                            "disponivel": "0.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "B"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "3"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "627",
                        "nome": "Tribuna B4 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "B"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "4"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "628",
                        "nome": "Tribuna B5 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "B"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "5"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "629",
                        "nome": "Tribuna B6 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "B"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "6"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "630",
                        "nome": "Tribuna B7 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "B"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "7"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "631",
                        "nome": "Tribuna B8 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "B"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "8"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "632",
                        "nome": "Tribuna C1 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "C"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "1"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "633",
                        "nome": "Tribuna C2 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "C"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "2"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "634",
                        "nome": "Tribuna C3 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "C"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "3"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "635",
                        "nome": "Tribuna C4 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "C"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "4"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "636",
                        "nome": "Tribuna C5 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "C"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "5"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "637",
                        "nome": "Tribuna C6 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "C"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "6"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "638",
                        "nome": "Tribuna C7 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "C"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "7"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "639",
                        "nome": "Tribuna C8 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "C"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "8"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "640",
                        "nome": "Tribuna C9 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "C"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "9"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "641",
                        "nome": "Tribuna D1 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "D"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "1"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "642",
                        "nome": "Tribuna D2 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "D"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "2"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "643",
                        "nome": "Tribuna D3 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "D"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "3"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "644",
                        "nome": "Tribuna D4 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "D"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "4"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "645",
                        "nome": "Tribuna D5 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "D"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "5"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "646",
                        "nome": "Tribuna D6 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "D"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "6"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "647",
                        "nome": "Tribuna D7 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "D"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "7"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "648",
                        "nome": "Tribuna D8 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "D"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "8"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "649",
                        "nome": "Tribuna D9 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "D"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "9"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "650",
                        "nome": "Tribuna E1 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "E"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "1"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "651",
                        "nome": "Tribuna E2 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "E"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "2"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "652",
                        "nome": "Tribuna E3 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "E"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "3"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "653",
                        "nome": "Tribuna E4 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "E"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "4"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "654",
                        "nome": "Tribuna E5 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "E"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "5"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "655",
                        "nome": "Tribuna E6 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "E"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "6"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "656",
                        "nome": "Tribuna E7 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "E"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "7"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "657",
                        "nome": "Tribuna E8 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "E"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "8"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "658",
                        "nome": "Tribuna E9 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "E"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "9"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "659",
                        "nome": "Tribuna E10 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "E"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "10"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "660",
                        "nome": "Tribuna F1 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "F"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "1"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "661",
                        "nome": "Tribuna F2 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "F"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "2"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "662",
                        "nome": "Tribuna F3 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "F"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "3"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "663",
                        "nome": "Tribuna F4 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "F"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "4"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "664",
                        "nome": "Tribuna F5 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "F"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "5"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "665",
                        "nome": "Tribuna F6 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "F"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "6"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "666",
                        "nome": "Tribuna F7 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "F"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "7"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "667",
                        "nome": "Tribuna F8 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "F"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "8"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "668",
                        "nome": "Tribuna F9 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "F"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "9"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "669",
                        "nome": "Tribuna F10 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "F"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "10"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "670",
                        "nome": "Tribuna G1 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "G"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "1"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "671",
                        "nome": "Tribuna G2 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "G"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "2"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "672",
                        "nome": "Tribuna G3 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "G"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "3"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "673",
                        "nome": "Tribuna G4 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "G"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "4"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "674",
                        "nome": "Tribuna G5 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "G"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "5"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "675",
                        "nome": "Tribuna G6 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "G"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "6"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "676",
                        "nome": "Tribuna G7 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "G"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "7"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "677",
                        "nome": "Tribuna G8 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "G"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "8"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "678",
                        "nome": "Tribuna G9 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "G"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "9"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "679",
                        "nome": "Tribuna G10 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "G"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "10"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "680",
                        "nome": "Tribuna G11 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "G"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "11"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "681",
                        "nome": "Tribuna H1 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "H"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "1"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "682",
                        "nome": "Tribuna H2 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "H"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "2"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "683",
                        "nome": "Tribuna H3 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "H"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "3"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "684",
                        "nome": "Tribuna H4 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "H"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "4"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "685",
                        "nome": "Tribuna H5 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "H"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "5"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "686",
                        "nome": "Tribuna H6 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "H"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "6"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "687",
                        "nome": "Tribuna H7 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "H"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "7"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "688",
                        "nome": "Tribuna H8 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "H"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "8"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "689",
                        "nome": "Tribuna H9 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "H"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "9"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "690",
                        "nome": "Tribuna H10 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "H"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "10"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "691",
                        "nome": "Tribuna H11 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "H"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "11"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "692",
                        "nome": "Tribuna H12 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "H"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "12"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "693",
                        "nome": "Tribuna I1 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "I"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "1"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "694",
                        "nome": "Tribuna I2 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "I"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "2"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "695",
                        "nome": "Tribuna I3 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "I"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "3"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "696",
                        "nome": "Tribuna I4 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "I"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "4"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "697",
                        "nome": "Tribuna I5 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "I"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "5"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "698",
                        "nome": "Tribuna I6 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "I"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "6"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "699",
                        "nome": "Tribuna I7 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "I"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "7"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "700",
                        "nome": "Tribuna I8 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "I"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "8"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "701",
                        "nome": "Tribuna I9 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "I"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "9"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "702",
                        "nome": "Tribuna I10 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "I"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "10"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "703",
                        "nome": "Tribuna I11 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "I"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "11"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "704",
                        "nome": "Tribuna I12 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "I"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "12"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "705",
                        "nome": "Tribuna J1 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "J"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "1"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "706",
                        "nome": "Tribuna J2 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "J"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "2"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "707",
                        "nome": "Tribuna J3 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "J"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "3"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "708",
                        "nome": "Tribuna J4 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "J"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "4"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "709",
                        "nome": "Tribuna J5 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "J"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "5"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "710",
                        "nome": "Tribuna J6 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "J"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "6"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "711",
                        "nome": "Tribuna J7 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "J"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "7"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "712",
                        "nome": "Tribuna J8 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "J"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "8"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "713",
                        "nome": "Tribuna J9 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "J"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "9"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "714",
                        "nome": "Tribuna J10 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "J"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "10"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "715",
                        "nome": "Tribuna J11 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "J"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "11"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "716",
                        "nome": "Tribuna J12 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "J"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "12"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "717",
                        "nome": "Tribuna J13 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "J"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "13"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "718",
                        "nome": "Tribuna K1 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "K"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "1"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "719",
                        "nome": "Tribuna K2 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "K"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "2"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "720",
                        "nome": "Tribuna K3 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "K"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "3"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "721",
                        "nome": "Tribuna K4 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "K"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "4"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "722",
                        "nome": "Tribuna K5 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "K"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "5"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "723",
                        "nome": "Tribuna K6 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "K"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "6"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "724",
                        "nome": "Tribuna K7 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "K"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "7"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "725",
                        "nome": "Tribuna K8 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "K"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "8"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "726",
                        "nome": "Tribuna K9 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "K"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "9"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "727",
                        "nome": "Tribuna K10 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "K"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "10"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "728",
                        "nome": "Tribuna K11 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "K"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "11"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "729",
                        "nome": "Tribuna K12 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "K"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "12"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "730",
                        "nome": "Tribuna K13 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "K"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "13"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "731",
                        "nome": "Tribuna L1 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "L"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "1"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "732",
                        "nome": "Tribuna L2 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "L"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "2"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "733",
                        "nome": "Tribuna L3 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "L"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "3"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "734",
                        "nome": "Tribuna L4 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "L"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "4"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "735",
                        "nome": "Tribuna L5 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "L"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "5"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "736",
                        "nome": "Tribuna L6 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "L"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "6"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "737",
                        "nome": "Tribuna L7 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "L"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "7"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "738",
                        "nome": "Tribuna L8 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "L"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "8"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "739",
                        "nome": "Tribuna L9 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "L"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "9"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "740",
                        "nome": "Tribuna L10 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "L"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "10"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "741",
                        "nome": "Tribuna L11 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "L"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "11"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "742",
                        "nome": "Tribuna L12 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "L"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "12"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "743",
                        "nome": "Tribuna L13 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "L"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "13"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "744",
                        "nome": "Tribuna L14 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "L"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "14"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "745",
                        "nome": "Tribuna M1 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "M"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "1"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "746",
                        "nome": "Tribuna M2 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "M"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "2"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "747",
                        "nome": "Tribuna M3 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "M"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "3"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "748",
                        "nome": "Tribuna M4 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "M"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "4"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "749",
                        "nome": "Tribuna M5 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "M"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "5"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "750",
                        "nome": "Tribuna M6 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "M"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "6"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "751",
                        "nome": "Tribuna M7 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "M"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "7"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "752",
                        "nome": "Tribuna M8 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "M"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "8"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "753",
                        "nome": "Tribuna M9 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "M"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "9"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "754",
                        "nome": "Tribuna M10 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "M"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "10"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "755",
                        "nome": "Tribuna M11 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "M"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "11"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "756",
                        "nome": "Tribuna M12 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "M"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "12"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "757",
                        "nome": "Tribuna M13 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "M"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "13"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "758",
                        "nome": "Tribuna M14 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "M"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "14"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "759",
                        "nome": "Tribuna M15 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "M"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "15"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "760",
                        "nome": "Tribuna N1 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "N"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "1"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "761",
                        "nome": "Tribuna N2 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "N"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "2"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "762",
                        "nome": "Tribuna N3 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "N"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "3"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "763",
                        "nome": "Tribuna N4 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "N"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "4"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "764",
                        "nome": "Tribuna N5 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "N"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "5"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "765",
                        "nome": "Tribuna N6 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "N"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "6"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "766",
                        "nome": "Tribuna N7 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "N"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "7"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "767",
                        "nome": "Tribuna N8 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "N"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "8"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "768",
                        "nome": "Tribuna N9 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "N"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "9"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "769",
                        "nome": "Tribuna N10 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "N"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "10"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "770",
                        "nome": "Tribuna N11 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "N"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "11"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "771",
                        "nome": "Tribuna N12 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "N"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "12"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "772",
                        "nome": "Tribuna N13 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "N"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "13"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "773",
                        "nome": "Tribuna N14 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "N"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "14"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "774",
                        "nome": "Tribuna N15 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "N"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "15"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "775",
                        "nome": "Tribuna O1 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "O"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "1"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "776",
                        "nome": "Tribuna O2 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "O"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "2"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "777",
                        "nome": "Tribuna O3 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "O"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "3"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "778",
                        "nome": "Tribuna O4 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "O"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "4"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "779",
                        "nome": "Tribuna O5 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "O"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "5"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "780",
                        "nome": "Tribuna O6 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "O"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "6"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "781",
                        "nome": "Tribuna O7 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "O"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "7"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "782",
                        "nome": "Tribuna O8 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "O"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "8"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "783",
                        "nome": "Tribuna O9 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "O"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "9"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "784",
                        "nome": "Tribuna O10 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "O"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "10"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "785",
                        "nome": "Tribuna O11 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "O"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "11"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "786",
                        "nome": "Tribuna O12 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "O"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "12"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "787",
                        "nome": "Tribuna O13 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "O"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "13"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "788",
                        "nome": "Tribuna O14 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "O"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "14"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "789",
                        "nome": "Tribuna O15 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "O"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "15"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "790",
                        "nome": "Tribuna O16 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "O"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "16"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "791",
                        "nome": "Tribuna P1 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "P"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "1"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "792",
                        "nome": "Tribuna P2 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "P"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "2"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "793",
                        "nome": "Tribuna P3 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "P"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "3"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "794",
                        "nome": "Tribuna P4 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "P"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "4"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "795",
                        "nome": "Tribuna P5 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "P"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "5"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "796",
                        "nome": "Tribuna P6 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "P"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "6"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "797",
                        "nome": "Tribuna P7 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "P"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "7"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "798",
                        "nome": "Tribuna P8 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "P"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "8"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "799",
                        "nome": "Tribuna P9 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "P"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "9"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "800",
                        "nome": "Tribuna P10 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "P"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "10"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "801",
                        "nome": "Tribuna P11 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "P"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "11"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "802",
                        "nome": "Tribuna P12 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "P"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "12"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "803",
                        "nome": "Tribuna P13 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "P"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "13"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "804",
                        "nome": "Tribuna P14 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "P"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "14"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    },
                    {
                        "codigo": "805",
                        "nome": "Tribuna P15 bloco 101",
                        "descricao": "",
                        "valor": "0.00",
                        "desconto": {
                            "fixo": "0.00",
                            "percentual": "0.00"
                        },
                        "estoque": {
                            "oferecido": "1.00",
                            "vendido": "0.00",
                            "reservado": "0.00",
                            "disponivel": "1.00"
                        },
                        "ativo": "1",
                        "tags": [
                            {
                                "nome": "tipo",
                                "valor": "assento"
                            },
                            {
                                "nome": "bloco",
                                "valor": "101"
                            },
                            {
                                "nome": "fileira",
                                "valor": "P"
                            },
                            {
                                "nome": "cadeira",
                                "valor": "15"
                            },
                            {
                                "nome": "area",
                                "valor": "Oeste"
                            },
                            {
                                "nome": "matriz",
                                "valor": "base"
                            }
                        ]
                    }
                ]
            },
            {
                "codigo": "18",
                "nome": "Bloco 102 Oeste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "302.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "302.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Oeste"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "bloco",
                        "valor": "102"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-137.txt"
                    }
                ]
            },
            {
                "codigo": "19",
                "nome": "Bloco 103 Oeste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "242.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "242.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Oeste"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "bloco",
                        "valor": "103"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-138.txt"
                    }
                ]
            },
            {
                "codigo": "20",
                "nome": "Bloco 104 Oeste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "273.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "273.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Oeste"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "bloco",
                        "valor": "104"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-139.txt"
                    }
                ]
            },
            {
                "codigo": "21",
                "nome": "Bloco 105 Oeste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "190.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "190.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Oeste"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "bloco",
                        "valor": "105"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-140.txt"
                    }
                ]
            },
            {
                "codigo": "22",
                "nome": "Bloco 106 Oeste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "225.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "225.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Oeste"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "bloco",
                        "valor": "106"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-141.txt"
                    }
                ]
            },
            {
                "codigo": "23",
                "nome": "Bloco 107 Oeste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "191.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "191.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "area",
                        "valor": "Oeste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "107"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-142.txt"
                    }
                ]
            },
            {
                "codigo": "34",
                "nome": "Bloco 108 Oeste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "270.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "270.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Oeste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "108"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-143.txt"
                    }
                ]
            },
            {
                "codigo": "35",
                "nome": "Bloco 109 Oeste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "241.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "241.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Oeste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "109"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-144.txt"
                    }
                ]
            },
            {
                "codigo": "36",
                "nome": "Bloco 110 Oeste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "300.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "300.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Oeste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "110"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-145.txt"
                    }
                ]
            },
            {
                "codigo": "37",
                "nome": "Bloco 111 Oeste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "187.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "187.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Oeste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "111"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-146.txt"
                    }
                ]
            },
            {
                "codigo": "38",
                "nome": "Bloco VIP Inferior 105 Oeste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "87.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "87.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Oeste"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "vip",
                        "valor": ""
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    },
                    {
                        "nome": "bloco",
                        "valor": "vip105"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-147.txt"
                    }
                ]
            },
            {
                "codigo": "39",
                "nome": "Bloco VIP Inferior 106 Oeste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "115.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "115.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Oeste"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "vip",
                        "valor": ""
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    },
                    {
                        "nome": "bloco",
                        "valor": "vip106"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-148.txt"
                    }
                ]
            },
            {
                "codigo": "40",
                "nome": "Bloco VIP Inferior 107 Oeste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "87.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "87.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Oeste"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "vip",
                        "valor": ""
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    },
                    {
                        "nome": "bloco",
                        "valor": "vip107"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-149.txt"
                    }
                ]
            },
            {
                "codigo": "41",
                "nome": "Bloco VIP Superior 201 Oeste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "42.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "42.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Oeste"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "vip",
                        "valor": ""
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    },
                    {
                        "nome": "bloco",
                        "valor": "vip201"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-150.txt"
                    }
                ]
            },
            {
                "codigo": "42",
                "nome": "Bloco VIP Superior 202 Oeste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "126.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "126.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Oeste"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "vip",
                        "valor": ""
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    },
                    {
                        "nome": "bloco",
                        "valor": "vip202"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-151.txt"
                    }
                ]
            },
            {
                "codigo": "43",
                "nome": "Bloco VIP Superior 203 Oeste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "42.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "42.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Oeste"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "vip",
                        "valor": ""
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    },
                    {
                        "nome": "bloco",
                        "valor": "vip203"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-152.txt"
                    }
                ]
            }
        ]
    },
    {
        "codigo": "236",
        "nome": "Leste",
        "descricao": "",
        "valor": "0.00",
        "desconto": {
            "fixo": "0.00",
            "percentual": "0.00"
        },
        "estoque": {
            "oferecido": "",
            "vendido": "0.00",
            "reservado": "0.00",
            "disponivel": ""
        },
        "ativo": "1",
        "tags": [
            {
                "nome": "cor",
                "valor": "vermelha"
            },
            {
                "nome": "tipo",
                "valor": "area"
            },
            {
                "nome": "matriz",
                "valor": "base"
            }
        ],
        "blocos": [
            {
                "codigo": "250",
                "nome": "Bloco 123 Leste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "514.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "514.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Leste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "123"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-467.txt",
                        "svg": "<path class=\"bloco\" id=\"bloco_123\" d=\"M569 52c-42,-15 -85,-32 -129,-50l-170 259c1,0 2,1 2,1 5,3 10,5 15,8 51,12 102,23 153,32l128 -250z\"/>"
                    }
                ]
            },
            {
                "codigo": "251",
                "nome": "Bloco 124 Leste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "298.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "298.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Leste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "124"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-468.txt",
                        "svg": "<path class=\"bloco\" id=\"bloco_124\" d=\"M698 94c-42,-13 -85,-27 -129,-43l-128 250c10,2 21,4 31,6 14,2 28,5 41,7 11,2 23,4 34,6 2,0 5,1 7,1 14,2 27,4 41,6l13 2 91 -235z\"/>"
                    }
                ]
            },
            {
                "codigo": "252",
                "nome": "Bloco 125 Leste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "220.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "220.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Leste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "125"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-469.txt",
                        "svg": "<path class=\"bloco\" id=\"bloco_125\" d=\"M827 129c-42,-10 -86,-21 -130,-34l-91 235c9,1 18,3 27,4 13,2 27,4 40,5 13,2 26,3 40,5 13,2 26,3 39,4l0 0 15 2 59 -221z\"/>"
                    }
                ]
            },
            {
                "codigo": "253",
                "nome": "Bloco 126 Leste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "469.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "469.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Leste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "126"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-470.txt"
                    }
                ]
            },
            {
                "codigo": "254",
                "nome": "Bloco 127 Leste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "442.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "442.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Leste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "127"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-471.txt"
                    }
                ]
            },
            {
                "codigo": "255",
                "nome": "Bloco 128 Leste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "101.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "101.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Leste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "128"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-472.txt"
                    }
                ]
            },
            {
                "codigo": "256",
                "nome": "Bloco 129 Leste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "282.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "282.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Leste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "129"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-473.txt"
                    }
                ]
            },
            {
                "codigo": "257",
                "nome": "Bloco 130 Leste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "109.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "109.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Leste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "130"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-474.txt"
                    }
                ]
            },
            {
                "codigo": "258",
                "nome": "Bloco 131 Leste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "443.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "443.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Leste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "131"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-475.txt"
                    }
                ]
            },
            {
                "codigo": "259",
                "nome": "Bloco 132 Leste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "463.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "463.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Leste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "132"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-476.txt"
                    }
                ]
            },
            {
                "codigo": "260",
                "nome": "Bloco 133 Leste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "228.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "228.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Leste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "133"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-477.txt"
                    }
                ]
            },
            {
                "codigo": "261",
                "nome": "Bloco 134 Leste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "293.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "293.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Leste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "134"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-478.txt"
                    }
                ]
            },
            {
                "codigo": "262",
                "nome": "Bloco 135 Leste",
                "descricao": "",
                "valor": "0.00",
                "desconto": {
                    "fixo": "0.00",
                    "percentual": "0.00"
                },
                "estoque": {
                    "oferecido": "522.00",
                    "vendido": "0.00",
                    "reservado": "0.00",
                    "disponivel": "522.00"
                },
                "ativo": "1",
                "tags": [
                    {
                        "nome": "area",
                        "valor": "Leste"
                    },
                    {
                        "nome": "bloco",
                        "valor": "135"
                    },
                    {
                        "nome": "tipo",
                        "valor": "bloco"
                    },
                    {
                        "nome": "matriz",
                        "valor": "base"
                    }
                ],
                "anexos": [
                    {
                        "nome": "svgmapa",
                        "tipo": "txt",
                        "url": "http://www.zpointz.com.br/sandbox/paguetudo/v1/data/anexo-479.txt"
                    }
                ]
            }
        ]
    }
]

//////////

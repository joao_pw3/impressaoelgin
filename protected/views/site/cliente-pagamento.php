<?php

use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use yii\jui\DatePicker;
use app\components\MenuWidget;
use app\components\TempoCarrinhoWidget;

$session = Yii::$app->session;
$session->open();
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    echo TempoCarrinhoWidget::getBreadCrumb('losang2.png');
}

?>
<div class="container">
    <div class="row">
        <?php
        if($clienteNovo == true) {
            echo MenuWidget::getUrlsVisitante();      
            echo MenuWidget::getMenuVisitante();
        }
        else {
            echo MenuWidget::getUrls();
            echo MenuWidget::getMenuLateral(); 
        }
        ?>
        <div class="col-md-10">
            <?php
            if(Yii::$app->session->hasFlash('aviso-cadastro')){?>
                <p class="alert alert-warning"><?=Yii::$app->session->getFlash('aviso-cadastro');?></p>
            <?php } ?>
            <div class="tab-content">

                <?php $form = ActiveForm::begin(['id' => 'form-cadastro']); ?>

                <div class="pagamento" id="pagamento">
                    
                    <ul class="nav nav-tabs">
                        <?php if ((!isset($clienteNovo) || $clienteNovo == false) && isset($cartoesApi->objeto)) { ?>
                        <li class="active"><a data-toggle="tab" href="#tab-cartoes"><i class="fa fa-credit-card-alt"></i> Cartões cadastrados</a></li>
                        <?php } ?>
                        <li<?= ($clienteNovo == true || !isset($cartoesApi->objeto)) ? ' class="active"' : ''; ?>><a data-toggle="tab" href="#tab-cartao"><i class="fa fa-credit-card"></i> Cadastrar cartão</a></li>
                    </ul>

                    <div class="tab-content">
                        <?php if ((!isset($clienteNovo) || $clienteNovo == false) && isset($cartoesApi->objeto)) { ?>
                        <div id="tab-cartoes" class="tab-pane fade in active">
                            <?= $this->render('cliente-cartoes', [
                                'cartao' => $cartao,
                                'cartoesApi' => $cartoesApi,
                                'form' => $form
                            ]); ?>
                        </div>
                        <?php } ?>
                        <div id="tab-cartao" class="tab-pane fade<?= ($clienteNovo == true || !isset($cartoesApi->objeto)) ? ' in active' : ''; ?>">
                            <?= $this->render('cliente-cartao-novo', [
                                'cartao' => $cartao,
                                'clienteApi' => $clienteApi,
                                'form' => $form
                            ]); ?>
                        </div>
                    </div>
                    
                </div>

                <?php ActiveForm::end(); ?> 
            </div>
        </div>
    </div>
</div>
<?php

$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/cadastro.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset', 'yii\bootstrap\BootstrapPluginAsset']]);
$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset', 'yii\bootstrap\BootstrapPluginAsset']]);
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    $this->registerJs("deadline = setDataExpira('" . $session['tempoCarrinho'] . "'); initializeClock();");
}
?>
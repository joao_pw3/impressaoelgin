<?php
use yii\helpers\Url;
use yii\web\View;
use app\modules\admin\assets\MuseuAsset;

MuseuAsset::register($this);
?>

<div class="container">

	<p id="cappta-status"></p>
    <?php 
    if($todas == 0) {
    	$this->registerJs("capptaInit();",View::POS_READY);
    ?>
    <div class="col-sm-12">
    	<div class="alert alert-warning">
    		Só é possível o cancelamento de pagamentos do dia vigente.
    	</div>
    </div>
    <?php } ?>

    <div class="col-sm-6">
    	<a class="btn btn-<?=$todas == 0 ? 'default' : 'primary';?>" href="<?=Url::to(['vendas','todas'=>0])?>">Cancelar Pagamento</a>
    	<a class="btn btn-<?=$todas == 1 ? 'default' : 'primary';?>" href="<?=Url::to(['vendas','todas'=>1])?>">Consultar Pagamento</a>
    	<a class="btn btn-warning" href="<?=\yii\helpers\Url::to(['exportar-csv'])?>">Exportar para CSV</a>
    </div>

    <div class="col-sm-6 impressao_estorno hide">
        <button id="reciboConsumidor" class="btn btn-success" data-recibo onclick="printRecibo(this)">Cancelamento Consumidor <span class="glyphicon glyphicon-print"></span></button>
        <button id="reciboEstabelecimento" class="btn btn-warning" data-recibo onclick="printRecibo(this)">Cancelamento Estabelecimento <span class="glyphicon glyphicon-print"></span></button>
    </div>

	<div class="col-sm-12">
		<input type="hidden" id="controle" name="controle">
		<table class="table" id="cappta">
		 	<thead>
		    	<tr>
		    		<th scope="col">Data</th>
		     		<th scope="col">Controle</th>
		      		<th scope="col">Cartão</th>
		      		<th scope="col">Tipo</th>
		      		<th scope="col">Valor</th>
		      		<th scope="col">Operador</th>
		      		<th scope="col"><?= $todas == 0 ? '#' : 'Status'; ?></th>
		    	</tr>
		  	</thead>
		  	<tbody>
		  	<?php
		  	if(sizeof($vendas) > 0) {
		  	foreach ($vendas as $row) { 
		  		$date = implode('/',array_reverse(explode('-',substr($row->autorizacao_data, 0, 10))));
		  		$time = substr($row->autorizacao_data, 11);
		  		?>
		  		<tr>
		  			<td><?= $date.' '.$time; ?></td>	
		      		<td><?= $row->administrative_code; ?></td>
		      		<td><?= $row->cartao; ?></td>
		      		<td><?= $row->pagamento_tipo == 'debito' ? 'Débito' : 'Crédito'; ?></td>
		      		<td><?= number_format($row->pagamento_valor, 2,',','.'); ?></td>
		      		<td><?= $row->user_id; ?></td>
		      		<?php if($todas == 0) { ?>
		      		<td style="cursor:pointer;" class="cancelamento" data-numero=<?= $row->administrative_code; ?>>Cancelar</td>
		  	  		<?php } else { ?>
		  	  		<td><?= $row->status; ?></td>
		  	  		<?php } ?>
		    	</tr>
		  	<?php } 		  		
		  	} else { ?>
		  		<tr>
		      		<td colspan="5" align="center">Não há vendas para o dia vigente.</td>
		      	</tr>
		  	<?php } ?>
		  	</tbody>
		</table>
	</div>
</div>

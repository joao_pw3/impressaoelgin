<?php echo '<table class="table table-striped"><thead><tr>
            <th>ID</th>
            <th>Nome</th>
            <th>E-mail</th>
            <th>CPF</th>
            <th>CPF base64</th>
            <th>Bloco</th>
            <th>Fileira</th>
            <th>Assento</th>
            <th>Produto</th>
            <th>Produto base64</th>
            <th>URL</th>
            </tr>
            </thead>
            <tbody>
            ';
        foreach ($lista as $r) {
            echo '<tr>
            <td>'.$r->id.'</td>
            <td>'.$r->nome.'</td>
            <td>'.$r->email.'</td>
            <td>'.$r->cpf.'</td>
            <td>'.$r->cpfbase64.'</td>
            <td>'.$r->bloco.'</td>
            <td>'.$r->fileira.'</td>
            <td>'.$r->assento.'</td>
            <td>'.$r->codproduto.'</td>
            <td>'.$r->produtobase64.'</td>
            <td>'.$r->url_compra.'</td>
            </tr>
            ';
        }

        echo '</tbody></table>';
<?php
use yii\grid\GridView;
use app\models\ApiCarrinho;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\models\ApiCliente;
use app\components\MenuWidget;
use app\components\TempoCarrinhoWidget;

$session = Yii::$app->session;
$session->open();
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    echo TempoCarrinhoWidget::getBreadCrumb('losang2.png');
}

echo MenuWidget::getUrls();
?>
<div class="container">
    <div class="row">
        <?php echo MenuWidget::getMenuLateral(); ?>
        <div class="col-md-10">
            <div class="tab-content">
                <div id="historico" class="boxDados">
                    <?= GridView::widget([
                        'dataProvider' => $historico,
                        'id' => 'grid-historico',
                        'layout' => " <div class=''><div class='pull-right'>{summary}</div><div class='pull-left'>{pager}</div>\n{items}</div>",
                        'columns' => [
                            [
                                'attribute' => 'nome',
                                'label' => 'Dados da compra',
                                'format' => 'html',
                                'headerOptions' => [
                                    'class' => 'text-center',
                                ],
                                'contentOptions' => [
                                    'class' => 'col-md-3'
                                ],
                                'value' => function ($historico) {
                                    return '
                                    ' . $historico->vendas->comprador->nome . '<br>
                                    <p>
                                        ' . $historico->vendas->data . '<br>
                                        R$ ' . number_format($historico->vendas->valor, 2, ',', '.') . '
                                    </p>';
                                }
                            ],
                            [
                                'attribute' => 'status',
                                'label' => 'Pagamento',
                                'format' => 'html',
                                'headerOptions' => [
                                    'class' => 'text-center',
                                ],
                                'contentOptions' => [
                                    'class' => 'text-center col-md-2 vert-center',
                                ],
                                'value' => function ($historico) {
                                    if($historico->vendas->status=='Iniciado' && $historico->vendas->tipo=='Boleto' && $historico->vendas->id_conciliacao=='')
                                        return (new ApiCliente)->clienteLinkBoleto($historico->vendas->codigo);
                                    return '<i class="fa ' . (in_array($historico->vendas->status, ['Expirado', 'Cancelado']) ? 'fa-remove plus-red' : 
                                        ($historico->vendas->status == 'Autorizado' ? 'fa-check plus-green': 
                                        ($historico->vendas->status == 'Estornado' ? 'fa-check-square-o plus-red' : 
                                        ($historico->vendas->status == 'Iniciado' ? 'fa-info-circle' : 'fa-exclamation-circle')))) . '  
                                    fa-2x ' . 
                                    ($historico->vendas->status == 'Autorizado' ? 'icon-green' : ($historico->vendas->status == 'Estornado' ? 'icon-orange' : 'icon-red')) .'"
                                    title="' . $historico->vendas->status . '"></i>';
                                }
                            ],
                            [
                                'attribute' => 'comprovante',
                                'label' => '#',
                                'format' => 'raw',
                                'headerOptions' => [
                                    'class' => 'text-center',
                                ],
                                'contentOptions' => [
                                    'class' => 'text-center col-md-2 vert-center',
                                ],
                                'value' => function ($historico) {
                                    if ($historico->vendas->status=='Autorizado' && $historico->vendas->tipo=='Taxa - 2ª via cartão mifare') {
                                        return '<i class="fa fa-money fa-2x plus-blue pgto-taxa-segunda-via" title="Taxa - 2ª via de mifare"></i><p><a class="pgto-taxa-segunda-via">Para retirar a segunda via, apresente seu documento com foto na bilheteria sul</a></p>';
                                    }
                                    if($historico->vendas->status=='Autorizado' && $historico->vendas->tipo=='Boleto' && $historico->vendas->id_conciliacao=='')
                                        return '<i class="fa fa-qrcode fa-2x plus-blue" title="Creditado na sua conta Mineirão"></i>';
                                    if ($historico->vendas->status !== 'Autorizado') {
                                        return '<i class="fa fa-times fa-2x plus-red" title="Indisponível"></i>';
                                    }
                                    $hash = (new ApiCarrinho)->setHashCompra($historico->vendas->data, $historico->vendas->comprador->documento, $historico->vendas->codigo);//                    
                                    return '<a href="' . Url::to(['site/resumo-compra','hash_compra'=>$hash]) . '" title="Clique aqui para ver o seu voucher.">' .
                                        '<i class="fa fa-ticket fa-2x plus-green"></i>' .
                                        '</a>';
                                }
                            ],
                        ]
                    ]); ?>  
                </div>

                <?php Modal::begin([
                    'header' => '<h4 align="center">Reembolso</h4>',
                	'size'   => 'modal-sm',
                    'id'     => 'modal-reembolso',
                    'options'=> [
                        'class' => 'modal-center'
                    ],
                    'clientOptions' => [
                        'backdrop' => 'static', 
                        'keyboard' => false
                    ] 
                ]); 
                ?>

                <form id="form-reembolso">
                    <input type="hidden" name="id_compra" id="id_compra" value="">
                    <input type="hidden" name="trid" id="trid" value="">
                    <input type="hidden" name="tipoTaxa" id="tipoTaxa" value="">
                    <input type="hidden" id="url-estorno" value="<?= Url::to(['site/estornar-compra']); ?>">

                    <div class="row">
                		<div class="col-md-12">
                			<label for="user_id-esqueci">Total da compra:</label>
                			<span class="form-control">R$ <span id="reemb-total"></span></span><br>
                			<p>Aqui você poderá solicitar o seu reeembolso.</p>
                		</div>
                    </div>
                    
                <!--    <div class="row">
                        <div class="col-md-4">* Taxa:</div>
                        <div class="col-md-4"><input type="hidden" name="taxa" id="reemb-taxa" value="" class="form-control margin-reemb"></div>
                    </div>-->
                    
                <!--    <div class="row div-table-color1">
                        <div class="col-md-4">CVV:</div>
                        <div class="col-md-4"><?php /*$form->field($card, 'cvv')->passwordInput()
                            ->widget(MaskedInput::class, [
                                'mask' => '999[9]',
                                'options' => [
                                    'class' => 'form-control margin-reemb',
                                    'placeholder' => 'CVV (3 ou 4 dígitos)',
                                    'title' => 'CVV (3 ou 4 dígitos)',
                                    'id' => 'cvv'
                                ]
                            ])->label(false); */?></div>
                    </div>-->
                    
                    <!--<span>* Taxas bancárias e custo de processamento: 10% (dez por cento) do valor reembolsado</span>-->
                            
                    <div class="row" id="reembolso">
                        <div class="col-md-12" align="center">
                            <!-- <span class="contato">
                                <button type="button" id="btn-reembolso-cancel" title="Cancelar processo de reembolso."><span class="modal-reembolso">Parar</span></button>
                            </span> -->
                            <span class="contato">
                                <button type="button" id="btn-reembolso-confirm" title="Efetuar procedimento de reembolso."><span class="modal-reembolso">Confirmar</span></button>
                            </span>
                        </div>
                    </div>

                </form>

                <?php Modal::end(); ?>

                <?php
                $this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
                $this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);            
                $this->registerJsFile('@web/js/historico.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
                ?>
            </div>
        </div>
    </div>
</div>

<?php


$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    $this->registerJs("deadline = setDataExpira('" . $session['tempoCarrinho'] . "'); initializeClock();");
}
?>
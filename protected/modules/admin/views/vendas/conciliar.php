<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="compra-avulsa" id="compra-avulsa">
    <div class="container">
        <?php 
        foreach ($eventos as $index => $evento) {
            if ($evento->tags['apelido'] == 'temporada2018') {
        ?>
        <div style="margin-bottom:25px;">
            <div class="col-md-6 about-left wow fadeInLeft animated" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                <?= Html::img($evento->anexos['bannervitrine_2']); ?>
            </div>
            <div class="col-md-4 about-right wow fadeInRight animated textosAprest" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                <div>Mineirão Tribuna</div>                    
                Muito mais do que futebol. Garanta já o seu lugar, compre aqui!
            </div>
            <div class="col-md-2 about-right wow fadeInRight animated" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">               
                <span class="row contato">               
                    <a class="areas seta" href="<?= Url::to(['/admin/vendas/blocos', 'id' => $evento->codigo, 'area' => 'Oeste']); ?>">Comprar</a> 
                </span>
            </div>
            <hr style="padding-top:25px;">
        </div>
        <div style="margin-bottom:25px;">
            <div class="col-md-6 about-left wow fadeInLeft animated" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                <?= Html::img($evento->anexos['bannervitrine_1']); ?>
            </div>
            <div class="col-md-4 about-right wow fadeInRight animated textosAprest" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                <div>Mineirão Embaixada</div>                    
                Você mais perto da história. Garanta já o seu lugar, compre aqui!
            </div>
            <div class="col-md-2 about-right wow fadeInRight animated" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">               
                <span class="row contato">               
                    <a class="areas seta" href="<?= Url::to(['/admin/vendas/blocos', 'id' => $evento->codigo, 'area' => 'Leste']); ?>">Comprar</a> 
                </span>
            </div>
            <hr style="padding-top:25px;">
        </div>
        <div style="margin-bottom:25px;">
            <div class="col-md-6 about-left wow fadeInLeft animated" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                <?= Html::img($evento->anexos['bannervitrine_0']); ?>
            </div>
            <div class="col-md-4 about-right wow fadeInRight animated textosAprest" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                <div>Mineirão Camarote</div>                    
                Garanta já o seu lugar, compre aqui!
            </div>
            <div class="col-md-2 about-right wow fadeInRight animated" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">               
                <span class="row contato">               
                    <a class="areas seta" href="<?= Url::to(['/admin/vendas/areas', 'id' => $evento->codigo]); ?>">Comprar</a> 
                </span>
            </div>
            <hr style="padding-top:25px;">
        </div>
    <?php 
            } else {
    ?>
        <div style="margin-bottom:25px;">
            <div class="col-md-6 about-left wow fadeInLeft animated" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                <?= isset($evento->anexos['bannervitrine']) ? Html::img($evento->anexos['bannervitrine']) : '-'; ?>
            </div>
            <div class="col-md-4 about-right wow fadeInRight animated textosAprest" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                <div><?= $evento->nome; ?></div>                    
                <?php
                $objData = \DateTime::createFromFormat('d/m/Y H:i:s', $evento->data->inicial);
                $diaSemana = ['', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab', 'Dom'];
                ?>
                <div>
                <?= $diaSemana[$objData->format('N')] . ' ' . $objData->format('d/m') ?> - 
                    Horário: <?= $objData->format('H'); ?>h
                </div>
                <?= $evento->descricao; ?>
            </div>
            <div class="col-md-2 about-right wow fadeInRight animated" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">               
                <span class="row contato">               
                    <a class="areas seta" href="<?= Url::to(['/admin/areas', 'id' => $evento->codigo]); ?>">Comprar</a> 
                </span>
            </div>
            <hr style="padding-top:25px;">
        </div>
    <?php 
            } 
        } 
    ?>
    </div>
</div>

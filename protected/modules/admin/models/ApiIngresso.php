<?php
namespace app\modules\admin\models;

use app\models\Api;

/**
 * Classe Model para Ingressos - Easy for Pay - Mineirão
 * Carga e ativação de ingressos
 */
class ApiIngresso extends Api
{
	public $logs;

    /**
     * Reservar (Reserva uma quantidade N de ingressos para um evento específico) 
     * @param array $post Dados para a carga de ingresso: id_agenda, lote_origem, codigo_compra_limite
     */
    public function reservar($post = []){
        $this->setLog('Carga de ingresso');
        if (!empty($post) && is_array($post)) {
            $returnApi = json_decode($this->call('ingresso/reservar', 'PUT', $post));
            $this->logs->log('Dados enviados: ',$post);
            $this->logs->log($returnApi->successo === '0' ? 'Falha na carga de ingresso' : 'Carga de ingresso efetuada com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Carga de ingresso inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível fazer a carga de ingresso.');
    }

    /**
     * Consultar ingressos
     * @param array $post Dados para consulta: id_agenda, status, bloqueado, documento, lote_origem, lote_entrega, codigo_venda, nome, qrcode
     */
    public function consultar($post = []) {
        $this->setLog('Consulta ingressos');
        if (!empty($post) && is_array($post)) {
            $returnApi = json_decode($this->call('ingresso', 'POST', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha na consulta de ingressos' : 'Consulta de ingressos efetuada com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Consulta de ingressos inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível fazer a consulta de ingressos.');
    }
    
    /**
     * Identificar lotes e totalizar registros - identificar data de evento
     * @param array $registros Registros para serem separados e contados
     * @param string $data Data do evento: reserva, producao, envio, entrega, associacao, liberado, retirado, invalidado, segunda_via
     */
    public function registrosLote($registros=[], $data="reserva")
    {
        $lotes = [];
        if (!empty($registros)) {
            foreach ($registros->objeto as $index => $ingresso) {
                $lotes[$ingresso->lote_reserva]['total'] = isset($lotes[$ingresso->lote_reserva]['total']) ? $lotes[$ingresso->lote_reserva]['total'] + 1 : 1;
                $lotes[$ingresso->lote_reserva]['data'] = $ingresso->{'data_' . $data};
            } 
        }
        return $lotes;
    }
    
    /**
     * Identificar todos os lotes, totalizar registros e identificar datas por etapa
     * @param array $registros Registros para serem separados e contados
     */
    public function registrosLoteConsolidado ($registros=[])
    {
        $lotes = [];
        if (!empty($registros->objeto)) {
            $arrDatas = ['reserva', 'producao', 'envio', 'entrega', 'associacao', 'liberado'];
            foreach ($registros->objeto as $index => $ingresso) {
                $lotes[$ingresso->lote_reserva]['total'] = isset($lotes[$ingresso->lote_reserva]['total']) ? $lotes[$ingresso->lote_reserva]['total'] + 1 : 1;
                $lotes[$ingresso->lote_reserva]['corte_inferior'] = !isset($lotes[$ingresso->lote_reserva]['corte_inferior']) ? 
                                                            $ingresso->codigo_venda : 
                                                            ($ingresso->codigo_venda < $lotes[$ingresso->lote_reserva]['corte_inferior'] ? 
                                                            $ingresso->codigo_venda : 
                                                            $lotes[$ingresso->lote_reserva]['corte_inferior']);
                $lotes[$ingresso->lote_reserva]['corte_superior'] = !isset($lotes[$ingresso->lote_reserva]['corte_superior']) ? 
                                                            $ingresso->codigo_venda : 
                                                            ($ingresso->codigo_venda > $lotes[$ingresso->lote_reserva]['corte_superior'] ? 
                                                            $ingresso->codigo_venda : 
                                                            $lotes[$ingresso->lote_reserva]['corte_superior']);
                foreach($arrDatas as $data) {
                    if (!empty($ingresso->{'data_' . $data}) && !isset($lotes[$ingresso->lote_reserva]['data_' . $data])) {
                        $lotes[$ingresso->lote_reserva]['data_' . $data] = $ingresso->{'data_' . $data};
                    }
                }
                $lotes[$ingresso->lote_reserva]['lote_grafica']=$ingresso->lote_entrega;
            }
        }
        return $lotes;
    }

    /**
     * Registrar entrada de lote em produção
     * @param $post Dados do lote: id_agenda, lote_origem
     */
    public function producao($post = []) {
        $this->setLog('Registrar lote em produção');
        if (!empty($post) && is_array($post)) {
            $returnApi = json_decode($this->call('ingresso/producao', 'PUT', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao registrar lote em produção' : 'Registro correto de lote em produção', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Registro de lote em produção inválido. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível registrar lote em produção.');
    }
    
    /**
     * Registrar envio de lote da gráfica para o estádio
     * @param $post Dados do lote: id_agenda, lote_origem, lote_entrega
     */
    public function enviar($post = []) 
    {
        $this->setLog('Registrar envio de lote da gráfica para o estádio');
        if (!empty($post) && is_array($post)) {
            $returnApi = json_decode($this->call('ingresso/enviar', 'PUT', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao registrar envio de lote da gráfica para o estádio' : 'Registro correto de envio de lote da gráfica para o estádio', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Registro de envio de lote da gráfica para o estádio inválido. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível registrar envio de lote da gráfica para o estádio.');
    }

    /**
     * Registrar recebimento de lote de ingressos no estádio
     * @param $post Dados do lote: id_agenda, lote_origem
     */
    public function entregar($post = []) 
    {
        $this->setLog('Registrar entrega de lote de ingressos no estádio');
        if (!empty($post) && is_array($post)) {
            $returnApi = json_decode($this->call('ingresso/entregar', 'PUT', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao registrar entrega de lote de ingressos no estádio' : 'Registro correto de entrega de lote de ingressos no estádio', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Registro de entrega de lote de ingressos no estádio inválido. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível registrar entrega de lote de ingressos no estádio.');
    }

    /**
     * Associar (Vincula um ingresso a uma compra de um cliente, está será a operação de troca de voucher por ingresso)
     * @deprecated ATENÇAÕ: Este ending point vai deixar de ser usado, devido à melhorias no fluxo do ingresso
     */
    public function associar($post = []) 
    {
        $this->setLog('Associar ingresso');
        if (!empty($post) && is_array($post)) {
            $returnApi = json_decode($this->call('ingresso/associar/', 'PUT', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao associar ingresso' : 'Associação de ingresso efetuada com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Associação de ingresso inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível associar o ingresso.');
    }
    
    /**
     * Associar RFID a um ingresso
     * @param $post Dados: id_agenda, qrcode, rfid
     */
    public function associa($post = []) 
    {
        $this->setLog('Associar RFID a um ingresso');
        if (!empty($post) && is_array($post)) {
            $returnApi = json_decode($this->call('ingresso/associa', 'PUT', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao associar RFID a ingresso' : 'Associação de RFID a ingresso correta', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Associação de RFID a ingresso inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível associar RFID a ingresso.');
    }
    
    /**
     * Registrar liberação de ingresso(s) para a bilheteria
     * (lote ou individual)
     * @param $post Dados: id_agenda, qrcode, lote
     */
    public function liberar($post = []) 
    {
        $this->setLog('Registrar liberação de ingresso para bilheteria');
        if (!empty($post) && is_array($post)) {
            $returnApi = json_decode($this->call('ingresso/liberar', 'PUT', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao registrar liberação de ingresso para bilheteria' : 'Registro de liberação de ingresso para bilheteria correto', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Registro de liberação de ingresso para bilheteria inválido. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível registrar liberação de ingresso para bilheteria.');
    }
    
    /**
     * Registrar retirada de ingresso na bilheteria
     * @param $post Dados: id_agenda, qrcode, nome, documento
     */
    public function retirar($post = []) 
    {
        $this->setLog('Registrar retirada de ingresso na bilheteria');
        if (!empty($post) && is_array($post)) {
            $returnApi = json_decode($this->call('ingresso/retirar', 'PUT', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao registrar retirada de ingresso na bilheteria' : 'Registro de retirada de ingresso na bilheteria correto', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Registro de retirada de ingresso na bilheteria inválido. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível registrar retirada de ingresso na bilheteria.');
    }

    /**
     * Registrar a utilização de um ingresso em um evento
     * @param $post Dados: id_agenda, qrcode, data
     */
    public function utilizar($post = []) 
    {
        $this->setLog('Registrar a utilização de um ingresso em um evento');
        if (!empty($post) && is_array($post)) {
            $returnApi = json_decode($this->call('ingresso/utilizar', 'PUT', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao registrar a utilização de um ingresso em um evento' : 'Registro de utilização de um ingresso em um evento correto', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Registro de utilização de um ingresso em um evento inválido. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível registrar a utilização de um ingresso em um evento.');
    }
    
    /**
     * Invalidar um ingresso
     * @param $post Dados: id_agenda, qrcode, motivo
     */
    public function invalidar($post = []) 
    {
        $this->setLog('Invalidar um ingresso');
        if (!empty($post) && is_array($post)) {
            $returnApi = json_decode($this->call('ingresso/invalidar', 'PUT', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao invalidar um ingresso' : 'Ingresso invalidado corretamente', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Invalidação de ingresso incorreta. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível invalidar ingresso.');
    } 
    
    /**
     * Bloquear / desbloquear ingresso
     * @param $post Dados: id_agenda, qrcode, bloqueado
     */
    public function bloquear($post = []) 
    {
        $this->setLog('Bloquear ou desbloquear ingresso');
        if (!empty($post) && is_array($post)) {
            $returnApi = json_decode($this->call('ingresso/bloquear', 'PUT', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao bloquear/desbloquear ingresso' : 'Ingresso bloqueado/desbloqueado corretamente', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Bloqueio/desbloqueio de ingresso incorreto. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível bloquear/desbloquear ingresso.');
    } 
    
    /**
     * Solicitar segunda via de ingresso
     * @param $post Dados: id_agenda, qrcode
     */
    public function segundavia($post = []) 
    {
        $this->setLog('Solicitação de segunda via de ingresso');
        if (!empty($post) && is_array($post)) {
            $returnApi = json_decode($this->call('ingresso/segundavia', 'PUT', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao solicitar segunda via de ingresso' : 'Segunda via de ingresso solicitado corretamente', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Solicitação de segunda via de ingresso incorreta. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível solicitar segunda via de ingresso.');
    } 
    
    /**
     * Consultar totais - mesmos filtros de consulta de ingressos
     * @param array $post Dados para consulta: id_agenda, status, bloqueado, documento, lote_origem, lote_entrega, codigo_venda
     */
    public function consultarTotais($post = []) {
        $this->setLog('Consulta ingressos - Totais');
        if (!empty($post) && is_array($post)) {
            $returnApi = json_decode($this->call('ingresso/totais', 'POST', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha na consulta de totais de ingressos' : 'Consulta de totais ingressos efetuada com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Consulta de totais de ingressos inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível fazer a consulta de totais de ingressos.');
    }
    
}
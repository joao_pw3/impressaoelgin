<?php
namespace app\models;
use yii\base\Model;

/**
 * ClienteModel é o modelo para o formulário de cliente
 *
 */
class ClienteModel extends Model
{
    public $user_id;
    public $documento;
    public $nome;
    public $apelido;
    public $sexo;
    public $sexo_nome;
    public $data_nasc;
    public $ddd;
    public $celular;
    public $cep;
    public $endereco;
    public $num;
    public $compl;
    public $cidade;
    public $cidade_nome;
    public $cidade_uf;
    public $uf;
    public $bairro;
    public $url_foto;
    public $senha;
    public $senha_repeat;
    public $retornoConsulta;
    public $cliente_aceite;
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['documento', 'nome', 'data_nasc', 'ddd', 'celular', 'cep', 'endereco', 'num', 'bairro', 'cidade_uf'], 'required'],
            [['senha', 'senha_repeat'],'required','on'=>'cadastro-conta'],
            [['user_id'], 'required', 'message' => 'O e-mail não pode ficar em branco','on'=>'cadastro-conta'],
            [['cliente_aceite'], 'required', 'requiredValue' => '1', 'message' => 'Leia e aceite o regulamento para continuar','on'=>'cadastro-conta'],
            [['user_id'], 'email'],
            [['documento'], 'string', 'min' => 14, 'max' => 18],
            [['nome'], 'string', 'min' => 7, 'max' => 64],
            [['apelido'], 'string', 'max' => 20],
            [['sexo'], 'string', 'max' => 1],
            [['sexo_nome'], 'string', 'max' => 9],
            [['data_nasc'], 'date', 'format' => 'php:d/m/Y'],
            [['ddd'], 'integer'],
            [['ddd'], 'string', 'max' => 2],
            [['celular'], 'string', 'min' => 9, 'max' => 10],
            [['senha', 'senha_repeat'], 'string', 'min' => 4, 'max' => 10],
            [['cep'], 'string', 'max' => 9],
            [['endereco'], 'string', 'max' => 128],
            [['num'], 'string', 'max' => 20],
            [['compl'], 'string', 'max' => 20],
            [['cidade'], 'integer'],
            [['cidade'], 'string', 'max' => 11],
            [['cidade_nome'], 'string', 'max' => 64],
            [['uf'], 'string', 'max' => 2],
            [['bairro'], 'string', 'max' => 32],
            [['url_foto'], 'string', 'max' => 255],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'nome' => 'Nome',
            'user_id' => 'E-mail',
            'documento' => 'CPF/CNPJ',
            'data_nasc' => 'Data de Nascimento',
            'sexo' => 'Gênero',
            'ddd' => 'DDD',
            'celular' => 'Celular',
            'cep' => 'CEP',
            'endereco' => 'Endereço',
            'num' => 'Número',
            'compl' => 'Complemento',
            'bairro' => 'Bairro',
            'cidade_nome' => 'Cidade',
            'senha_repeat' => 'Confirme sua Senha',
            'cliente_aceite' => 'Leia e aceite os temos de uso e as políticas de privacidade'
        ];
    }
   
}
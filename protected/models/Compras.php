<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Conta;
use app\models\Cliente;
use app\models\WsCarrinho;
use app\models\Ingressos;

/**
 * Classe que gerencia compras de um usuário
 */
class Compras extends Model 
{

    public $codigo;
    public $data;
    public $codigo_status;
    public $status;
    public $valor;
    public $unidades;
    public $total;
    public $tipo;
    public $validade;
    public $forma;
    public $descricao;
    public $produtos;
    public $id_conciliacao;
    public $conciliacao;
    public $deposito;
    public $id_carrinho;
    public $comprador;
    public $lista;
    public $carrinho;
    public $ingresso;
    public $id_pdv;
    public $parcelas;
    public $retorno_conciliacao;

    /**
     * Toda compra precisa ter um comprador
     */
    public function __construct(Conta $comprador) {
        $this->comprador = $comprador;
    }

    /**
     * Buscar e listar objetos Compra do usuário atual. Pesquisa o usuário pelo documento;
     * @return boolean/void addError em caso de falha na consulta, o método adiciona o erro e retorna. Void se consulta foi realizada com sucesso
     */
    public function listaComprasUsuario() {
        $session = Yii::$app->session;
        $session->open();

        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
        $ws = new WsCarrinho($objApiCliente->user_token);
        $vendas = $ws->consultarVendaFiltro(['rede' => '1', 'comprador' => $this->comprador->documento]);
        $this->lista = [];
        if (!$vendas->successo)
            return $this->addError('lista', $vendas->erro->mensagem);
        foreach ($vendas->objeto as $compra) {
            $objCompra = new Compras($this->comprador);
            $objCompra->loadFromApi($compra);
            $this->lista[] = $objCompra;
        }
    }

    /**
     * Buscar uma compra pelo código e também os produtos adquiridos pelo carrinho
     */
    public function umaCompra() {
        $session = Yii::$app->session;
        $session->open();

        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
        $ws = new WsCarrinho($objApiCliente->user_token);
        $consulta = $ws->consultarVendaFiltro(['codigo' => $this->codigo]);
        if (!$consulta->successo)
            return $this->addError('codigo', $consulta->erro->mensagem);
        $this->loadFromApi($consulta->objeto[0]);
    }

    /**
     * Traz o objeto do carrinho de uma compra - desde que tenha um carrinho ativo, ou seja, para pagamentos à API sem produto no carrinho, esse método não deve ser chamado
     */
    public function infoCarrinho() 
    {
        if ($this->tipo == '' && $this->id_carrinho != '') {
            $session = Yii::$app->session;
            $session->open();
            $objApiCliente = new Cliente;
            $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
            $ws = new WsCarrinho($objApiCliente->user_token);
            $carrinho = $ws->consultarCarrinho($this->id_carrinho);
            if (!$carrinho->successo)
                return $this->addError('carrinho', $carrinho->erro->mensagem);

            $this->carrinho = $carrinho->objeto;
        }
    }
    
    /**
     * Gerar informações de vouchers para uma compra
     * @param integer $idCompra Id de uma compra
     */
    public function getVouchers ($idCompra)
    {
        $this->codigo = $idCompra;
        $this->umaCompra();
        $this->infoCarrinho();
        /*
        $ingressosModel = new Ingressos($this);
        $ingressosModel->qrcodes();
        return $ingressosModel;
        */
    }

    /**
     * Comprador
     * Buscar uma compra pelo código e também os produtos adquiridos pelo carrinho
     */
    public function umaCompraComprador() {
        $session = Yii::$app->session;
        $session->open();
        $ws= new WsCompra;
        $ws->setToken($session->get('user_token'),'U');
        $consulta = $ws->consultarCompraFiltro(['codigo' => $this->codigo]);
        if (!$consulta->successo)
            return $this->addError('codigo', $consulta->erro->mensagem);

        $this->loadFromApiComprador($consulta->objeto[0]);
    }

    /**
     * Comprador
     * Traz o objeto do carrinho de uma compra - desde que tenha um carrinho ativo, ou seja, para pagamentos à API sem produto no carrinho, esse método não deve ser chamado
     */
    public function infoCarrinhoComprador() 
    {
        if ($this->tipo == '' && $this->id_carrinho != '') {
            $session = Yii::$app->session;
            $session->open();
            $objApiCliente = new Cliente;
            $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
            $ws = new WsCarrinho($objApiCliente->user_token);
            $ws->setToken($session->get('user_token'),'U');
            $carrinho = $ws->consultarCarrinhoCliente($this->id_carrinho);
            if (!$carrinho->successo)
                return $this->addError('carrinho', $carrinho->erro->mensagem);

            $this->carrinho = $carrinho->objeto;
        }
    }
    
    /**
     * Comprador
     * Gerar informações de vouchers para uma compra
     * @param integer $idCompra Id de uma compra
     */
    public function getVouchersComprador($idCompra)
    {
        $this->codigo = $idCompra;
        $this->umaCompraComprador();
        $this->infoCarrinhoComprador();
        /*
        print '<pre>';
        print_r($this);
        exit;
        $ingressosModel = new Ingressos($this);
        $ingressosModel->qrcodes();
        return $ingressosModel;
        */
    }    

    /**
     * Atribuir valores retornado pela API às propriedades
     */
    public function loadFromApi($obj) {
        if (!isset($obj->vendas, $obj->vendas->comprador) || 
            ((!isset(Yii::$app->user->identity->operador) || 
             (isset(Yii::$app->user->identity->operador) && Yii::$app->user->identity->operador == 0)) &&
             $obj->vendas->comprador->documento != $this->comprador->documento)) {
            return false;
        }

        $this->codigo = $obj->vendas->codigo;
        $this->data = $obj->vendas->data;
        $this->codigo_status = $obj->vendas->codigo_status;
        $this->status = $obj->vendas->status;
        $this->valor = $obj->vendas->valor;
        $this->unidades = $obj->vendas->unidades;
        $this->total = $obj->vendas->total;
        $this->tipo = $obj->vendas->tipo;
        $this->validade = $obj->vendas->validade;
        $this->forma = $obj->vendas->forma;
        $this->descricao = $obj->vendas->descricao;
        $this->produtos = $obj->vendas->produtos;
        $this->id_conciliacao = $obj->vendas->id_conciliacao;
        $this->deposito = $obj->deposito;
        $this->id_carrinho = $obj->id_carrinho;
        $this->id_pdv = $obj->vendas->id_pdv;
        $this->parcelas = $obj->vendas->parcelas;
        $this->retorno_conciliacao = json_decode(base64_decode($obj->vendas->conciliacao->retorno));
    }

    public function loadFromApiComprador($obj) {
        if (!isset($obj->compras)) {
            return false;
        }
        $this->codigo = $obj->compras->codigo;
        $this->data = $obj->compras->data;
        $this->codigo_status = $obj->compras->status->codigo;
        $this->status = $obj->compras->status->descricao;
        $this->valor = $obj->compras->valor;
        $this->unidades = $obj->compras->unidades;
        $this->total = $obj->compras->total;
        $this->tipo = $obj->compras->tipo;
        $this->validade = $obj->compras->validade;
        $this->forma = $obj->compras->forma_pagamento;
        $this->descricao = $obj->compras->descricao;
        $this->produtos = $obj->compras->produtos;
        $this->id_conciliacao = $obj->compras->id_conciliacao;
        //$this->deposito = $obj->deposito;
        $this->id_carrinho = $obj->id_carrinho;
        //$this->id_pdv = $obj->compras->id_pdv;
        $this->parcelas = $obj->compras->parcelas;
        //$this->retorno_conciliacao = json_decode(base64_decode($obj->vendas->conciliacao->retorno));
    }    

}

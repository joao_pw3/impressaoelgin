<?php
namespace app\modules\admin\models;

use yii\base\Model;

/**
 * Classe Model para Caixa - Easy for Pay - Mineirão
 * Responsável por criar regras de validação e tratar dados da API
 * obs.: parâmetros entre colchetes são opcionais
 */

class CaixaModel extends Model {
    public $user_id;
    public $senha;
    public $numero_cartao;
    public $relatorio;
}
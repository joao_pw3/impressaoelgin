<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
USE app\models\Cliente;

class ClienteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Alterar a cidade/unidade a partir da URL
     *
     * @return string
     */
    public function actionUnidade()
    {
        $session=Yii::$app->session;
        $alternar=$session->get('podeAlternarUnidade',false);
        if($alternar && Yii::$app->request->post()){
            $post=Yii::$app->request->post();
            $objApiCliente=new Cliente();
            $doc=$post['documento'];
            //selecionou a matriz explicitamente ou esta é utilizada para mostrar toda a rede? Inicia com false (não selecionou matriz)
            $selectMatriz=false;
            $unidadeMatriz=$objApiCliente->buscarUnidadePor('user_token',$objApiCliente->getTokenMatriz());
            
            //caso o documento enviado pelo post for o da matriz, então mostra só os eventos da matriz mantendo-a selecionada
            if($doc==$unidadeMatriz->documento)
                $selectMatriz=true;

            // se enviou "rede" pelo post, significa que devemos usar a matriz porém será pesquisada em toda a rede
            if($doc=='rede')
                $doc=$unidadeMatriz->documento;
            $session->set('selecao_matriz',$selectMatriz);
           
            $unidade=$objApiCliente->alternarUnidade($doc);
            $session->set('token_unidade',$unidade->user_token);
            $session->set('nome_unidade', $unidade->nome);
            if(Yii::$app->request->isAjax){
                echo json_encode(['successo' => '1']);
                exit();
            }
        }
    }
}
<?php
namespace app\modules\admin\controllers;

use Yii;
use yii\web\Response;
use app\modules\admin\controllers\AbstractController;
use app\modules\admin\models\ApiIngresso;
use app\models\ApiCarrinho;
use app\models\EventosModel;

/**
 * Controller de gráfica
 */
class GraficaController extends AbstractController
{
    public $enableCsrfValidation = false;
    
    /**
     * Página principal para a gráfica
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Reservar um conjunto de QR-Codes para um determinado evento
     */
    public function actionReservar()
    {
        try {
            $retorno = '';
            $vendas = [];
            $totais = [];
            if (Yii::$app->request->post()) {
                $ingresso = new ApiIngresso();
                if (Yii::$app->request->post('idEvento') && !Yii::$app->request->post('codigoCompra')) {
                    $totais = (new ApiIngresso)->consultarTotais(['id_agenda' => Yii::$app->request->post('idEvento')]);
                    if ($totais->successo == '1' && isset($totais->objeto->totais->ultima_compra) && !empty($totais->objeto->totais->ultima_compra)) {
                        $ultimaCompra = (new ApiCarrinho)->consultarVendasFiltro(['codigo' => $totais->objeto->totais->ultima_compra]);
                        $vendas = (new ApiCarrinho)->consultarVendasFiltro(['data_ini' => date('d-m-Y H:i:s', strtotime('+10 seconds', strtotime(str_replace('/', '-', $ultimaCompra->objeto[0]->vendas->data))))]);
                    } else {
                        $vendas = (new ApiCarrinho)->consultarVendasFiltro(['data_ini' => '27-11-2017']);
                    }
                }
                if (Yii::$app->request->post('idEvento') && Yii::$app->request->post('codigoCompra')) {
                    $reserva = $ingresso->reservar([
                        'id_agenda' => Yii::$app->request->post('idEvento'), 
                        'lote_origem' => 'mineirao_'.time(),
                        'codigo_compra_limite' => Yii::$app->request->post('codigoCompra'), 
                    ]);
                    if ($reserva->successo == '1') {
                        $retorno = ['tipo' => 'success', 'msg' => 'Reserva: ' . $reserva->objeto->msg . '. Alterados: ' . $reserva->objeto->alterados];
                    }
                    if ($reserva->successo == 0)
                        $retorno = ['tipo' => 'warning', 'msg' => $reserva->erro->mensagem];
                }
            }
            $eventos = (new EventosModel())->listaEventos(['ativo' => '1']);
            return $this->render('lote-reservar', [
                'eventos' => $eventos,
                'retorno' => $retorno,
                'vendas' => $vendas,
                'ultimo_corte' => isset($totais->objeto->totais->ultima_compra) ? $totais->objeto->totais->ultima_compra : '0'
            ]);
            
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro - Reserva de ingressos', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Registrar lote de cartões como "em Produção"
     */
    public function actionLoteProducao()
    {
        try {
            $eventos = (new EventosModel())->listaEventos(['ativo' => '1']);
            $ingressos = '';
            $lotes = '';
            if (Yii::$app->request->post()) {
                $ingresso = new ApiIngresso();
                $ingressos = $ingresso->consultar([
                    'id_agenda' => Yii::$app->request->post('producao_evento'), 
                    'status' => 'R'
                ]);
                if ($ingressos->successo == '1') {
                    $lotes = $ingresso->registrosLote($ingressos);
                }
            }
            return $this->render('lote-producao', [
                'codigo_evento' => Yii::$app->request->post('producao_evento') != null ? Yii::$app->request->post('producao_evento') : '',
                'eventos' => $eventos,
                'ingressos' => $ingressos, 
                'lotes' => $lotes,
            ]);
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro - Lotes em Produção', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Exportar informações para arquivo CSV
     * @param integer $evento Id do evento a que se refere o ingresso
     * @param string $lote Identificador do lote a ser gerado
     * @param string $status Identificador da situação dos ingressos a serem pesquisados
    */
    public function actionExportarCsv($evento='', $lote='', $status='')
    {
        try {
            if (empty($evento) || empty($lote) || empty($status)) {
                exit('Faltam informações para gerar o arquivo .csv do lote.');
            }
            $ingressos = (new ApiIngresso())->consultar([
                'id_agenda' => $evento,
                'lote_origem' => $lote, 
                'status' => $status,
            ]);
            if ($ingressos->successo == '1') {
                header('Content-Type: text/csv; charset=utf-8');
                header('Content-Disposition: attachment; filename=lote_ingresso_' . date('d-m-Y_H-i-s') . '.csv');
                $output = fopen('php://output', 'w');
                $arrCabecalho=['Qrcode', 'Nome do ocupante', 'Bloco', 'Fileira', 'Cadeira'];
                if($status=='A')
                    $arrCabecalho=['Qrcode', 'RFID', 'Nome do ocupante', 'Bloco', 'Fileira', 'Cadeira'];
                fputcsv($output, $arrCabecalho, ';');
                foreach ($ingressos->objeto as $index => $ingresso) {
                    $infoProduto = !empty($ingresso->produto) ? explode(' ', $ingresso->produto) : [];
                    $arrRow=[
                        isset($ingresso->qrcode) && !empty($ingresso->qrcode) ? $ingresso->qrcode : '-', 
                        isset($ingresso->ocupante_nome) && !empty($ingresso->ocupante_nome) ? utf8_decode($ingresso->ocupante_nome) : '-', 
                        isset($infoProduto[3]) && !empty($infoProduto[3]) ? $infoProduto[3] : '-', 
                        isset($infoProduto[1]) && !empty($infoProduto[1]) ? substr($infoProduto[1], 0, 1) : '-', 
                        isset($infoProduto[1]) && !empty($infoProduto[1]) ? substr($infoProduto[1], 1, strlen($infoProduto[1])) : '-'
                    ];
                    if($status=='A')
                        $arrRow=[
                            isset($ingresso->qrcode) && !empty($ingresso->qrcode) ? $ingresso->qrcode : '-', 
                            isset($ingresso->codigo) && !empty($ingresso->codigo) ? $ingresso->codigo : '-', 
                            isset($ingresso->ocupante_nome) && !empty($ingresso->ocupante_nome) ? utf8_decode($ingresso->ocupante_nome) : '-', 
                            isset($infoProduto[3]) && !empty($infoProduto[3]) ? $infoProduto[3] : '-', 
                            isset($infoProduto[1]) && !empty($infoProduto[1]) ? substr($infoProduto[1], 0, 1) : '-', 
                            isset($infoProduto[1]) && !empty($infoProduto[1]) ? substr($infoProduto[1], 1, strlen($infoProduto[1])) : '-'
                        ];
                    fputcsv($output, $arrRow, ';');
                }
                die();
            }
            exit('Não há ingressos disponíveis para as informações fornecidas.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro - Geração de arquivo .csv', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Método alterantivo para exportar informações para arquivo CSV - não pede o lote e adiciona coluna do código RFID
     * @param integer $evento Id do evento a que se refere o ingresso
     * @param string $status Identificador da situação dos ingressos a serem pesquisados
    */
    public function actionExportarCsvSemLote($evento='', $status='')
    {
        try {
            if (empty($evento) || empty($status)) {
                exit('Faltam informações para gerar o arquivo .csv do lote.');
            }
            $ingressos = (new ApiIngresso())->consultar([
                'id_agenda' => $evento,
                'status' => $status,
            ]);
            if ($ingressos->successo == '1') {
                header('Content-Type: text/csv; charset=utf-8');
                header('Content-Disposition: attachment; filename=lote_ingresso_' . date('d-m-Y_H-i-s') . '.csv');
                $output = fopen('php://output', 'w');
                fputcsv($output, ['Qrcode', 'Codigo', 'Nome do ocupante', 'Bloco', 'Fileira', 'Cadeira'], ';');
                foreach ($ingressos->objeto as $index => $ingresso) {
                    $infoProduto = !empty($ingresso->produto) ? explode(' ', $ingresso->produto) : [];
                    fputcsv($output, [
                        isset($ingresso->qrcode) && !empty($ingresso->qrcode) ? $ingresso->qrcode : '-', 
                        isset($ingresso->codigo) && !empty($ingresso->codigo) ? $ingresso->codigo : '-', 
                        isset($ingresso->ocupante_nome) && !empty($ingresso->ocupante_nome) ? utf8_decode($ingresso->ocupante_nome) : '-', 
                        isset($infoProduto[3]) && !empty($infoProduto[3]) ? $infoProduto[3] : '-', 
                        isset($infoProduto[1]) && !empty($infoProduto[1]) ? substr($infoProduto[1], 0, 1) : '-', 
                        isset($infoProduto[1]) && !empty($infoProduto[1]) ? substr($infoProduto[1], 1, strlen($infoProduto[1])) : '-'
                    ], ';');
                }
                die();
            }
            exit('Não há ingressos disponíveis para as informações fornecidas.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro - Geração de arquivo .csv', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Mudar status de lote para "em Produção"
     */
    public function actionStatusLoteProducao ()
    {
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                $post = Yii::$app->request->post();
                return (new ApiIngresso)->producao([
                    'id_agenda' => $post['evento'], 
                    'lote_origem' => $post['lote']
                ]);
            }
            return (new ApiIngresso)->getApiError('Faltam parâmetros para mudar o status do lote.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro - Mudança de status de lote para "em Produção"', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Registrar lote de cartões como "enviado da Gráfica ao Estádio"
     */
    public function actionLoteEstadio()
    {
        try {
            $eventos = (new EventosModel())->listaEventos(['ativo' => '1']);
            $ingressos = '';
            $lotes = '';
            if (Yii::$app->request->post()) {
                $ingresso = new ApiIngresso();
                $ingressos = $ingresso->consultar([
                    'id_agenda' => Yii::$app->request->post('estadio_evento'), 
                    'status' => 'P'
                ]);
                if ($ingressos->successo == '1') {
                    $lotes = $ingresso->registrosLote($ingressos, 'producao');
                }
            }
            return $this->render('lote-estadio', [
                'codigo_evento' => Yii::$app->request->post('estadio_evento') != null ? Yii::$app->request->post('estadio_evento') : '',
                'eventos' => $eventos,
                'ingressos' => $ingressos, 
                'lotes' => $lotes,
            ]);
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro - Envio de Lotes para o Estádio', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Mudar status de lote para "enviado ao Estádio"
     */
    public function actionStatusLoteEstadio ()
    {
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                $post = Yii::$app->request->post();
                return (new ApiIngresso)->enviar([
                    'id_agenda' => $post['evento'], 
                    'lote_origem' => $post['lote'],
                    'lote_entrega' => $post['lote_grafica']
                ]);
            }
            return (new ApiIngresso)->getApiError('Faltam parâmetros para mudar o status do lote.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro - Mudança de status de lote para "enviado ao Estádio"', 'message' => $e->getMessage()]);
        }
    }
    
}
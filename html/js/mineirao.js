var areasMineirao=['Oeste','Leste','Norte','Sul'];
var idLinha = '';
var deadline;
var ingressos = new Array();
var idProdutoLivre;

$(document).ready(function() { 

    var pathname = window.location.pathname,
        ondeEstou = pathname.substr(pathname.indexOf("site/") + 5);

    $('.verificaPagina li').each(function(i)
    {
        var url = $(this).find('a').attr('href'),
            link = url.substr(url.indexOf("site/") + 5);

            if (link == ondeEstou)
                $(this).addClass('active');
            else if(ondeEstou == 'resumo-compra')
                $("#liCliente li:nth-child(5)").addClass('active');
            else if(ondeEstou == 'cadastro-acesso')
                $("#cadastro li:nth-child(1)").addClass('active');
            else if(ondeEstou == 'cadastro-dados-pessoais')
                $("#cadastro li:nth-child(1)").addClass('active');
            else if(ondeEstou == 'cadastro-pagamentos')
                $("#cadastro li:nth-child(2)").addClass('active');
            else 
                $(this).removeClass('active');

    });    
    
    $('#finaliza-compra').click(function() {
        if ($('.linha-carrinho').length > 0) {
            window.location.assign($('#url-resumo').val());
        } else {
            $('#modal-mensagem .modal-body').html('Não há produtos em seu carrinho.');
            $('#modal-mensagem').modal('toggle');
            return false;
        }
    });
	
    $('#botContinua').click(function () {
        if (!$('#form-aceite-checkbox').is(':checked')) {
            $('#form-aceite-checkbox').focus();
            $('#modal-mensagem .modal-body').html('Leia os termos e clique no aceite para continuar.');
            $('#modal-mensagem').modal('toggle');
            return false;
        }
        if ($('#total-geral').val() == '' || $('#total-geral').val() == 0) {
            showMsg('Escolha as quantidades para os produtos que deseja.');
            return false;
        }
        $('#total_boleto').val($('#total-geral').val());
    });

    $('#confirmar-dados').click(function() {
        window.location.href = $('#url-resumo').val();
    });
    
    $('#nova-compra').click(function() {
       window.location.href = $('#url-index').val();
    });
	
    $('#home-mineirao').click(function() {
       window.location.href = 'http://estadiomineirao.com.br/#';
    });
    
    $(document).on('click', '.trash-item, .icon-trash', function () {
        var trash = $(this).attr('id').split('-');
        idLinha = trash[1];
        delCart($(this).attr('data-trash'));
    });
    
    $('.plus-white').click(function () {
        if (($('.select-lugares').length>0 && ($('.select-lugares option:selected').val() == '' || $('.select-lugares option:selected').val() == 'Selecione um produto')) || ($('.select-lugares').length<=0 && $("#carrinho-quantidade").val()<=0)) {
            $('#modal-mensagem .modal-body').html('Escolha um assento.');
            $('#modal-mensagem').modal('toggle');
            return false;
        }
        if ($('#carrinho-quantidade').val() == '' || isNaN($('#carrinho-quantidade').val())) {
            $('#modal-mensagem .modal-body').html('Escolha uma quantidade de assentos.');
            $('#modal-mensagem').modal('toggle');
            return false;
        }
        var max = parseInt($('#carrinho-quantidade').attr('max'));
        var _val = parseInt($('#carrinho-quantidade').val());
        if (_val > max) {
            $('#carrinho-quantidade').val(max);
            $('#modal-mensagem .modal-body').html('Este camarote possui um limite de ' + max + ' lugares disponíveis.');
            $('#modal-mensagem').modal('toggle');
            return false;
        }
        if (checkCarrinho()) {
            $('#modal-mensagem .modal-body').html('O assento selecionado já está no seu carrinho.');
            $('#modal-mensagem').modal('toggle');
            return false;
        }
        adicionarItemCarrinho();
    });
    
    $(document).on('change', '.select-lugares', function () {
        var produto = $('#carrinho-codigo').val($('.select-lugares option:selected').val());
        konduto(produto,'seleciona-produto');
    });
    
    $('#esqueci-senha').click(function() {
        $('#user_id-esqueci').val($('#user_id').val()).attr('readonly', $('#user_id').val().length == 0 ? false : true);
        $('#modal-senha').modal('toggle');
        konduto('esqueci-senha');
        return false;
    });
    
    $('#btn-esqueci-senha').click(function () {
        if ($('#user_id-esqueci').val() == '') {
            $('#modal-mensagem .modal-body').html('Informe o e-mail para o qual deseja trocar a senha de acesso.');
            $('#modal-mensagem').modal('toggle');
            return false;
        }
        enviarMensagemEsqueciSenha();
    });
    
    $('#index-renova').click(function () {
        window.location.href = 'http://estadiomineirao.com.br/contato/';
    });

    $('a.infoParcelas').click(function(){
        var a = $(this);
        var info = infoObjs(a)
        if(a.hasClass('mostrar')){
            a.removeClass('mostrar')
            $(info[0]).removeClass('hidden');
            $(info[1]).removeClass('hidden');
            a.text('[-]')
        }
        else{
            a.addClass('mostrar')
            $(info[0]).addClass('hidden');
            $(info[1]).addClass('hidden');
            a.text('[+]');
        }
    
    });

    $('.mineiraocard-submit').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var id = $(this).attr('id').replace('mineiraocard-submit-', '');
        var documento = $('#formMineiraocard-' + id + ' input[name="Mineiraocard[documento]"]').val();
        $('#formMineiraocard-' + id + ' input[name="Mineiraocard[documento]"]').val(documento.replace(/[^a-z0-9]/gi, ''));
        $('#formMineiraocard-' + id).yiiActiveForm('validate');
        
        if ($('#formMineiraocard-' + id + ' input[name="Mineiraocard[nome]"]').val() == '' || 
            $('#formMineiraocard-' + id + ' input[name="Mineiraocard[nome]"]').val().length < 6) {
            showMsg('O <strong>nome</strong> deve ser preenchido com pelo menos 6 letras.');
            return false;
        }

        if ($('#formMineiraocard-' + id + ' input[name="Mineiraocard[documento]"]').val() == '' || 
            ($('#formMineiraocard-' + id + ' input[name="Mineiraocard[documento]"]').val().length == 11 && 
             !isNaN($('#formMineiraocard-' + id + ' input[name="Mineiraocard[documento]"]').val()) &&
             !isCPFValid($('#formMineiraocard-' + id + ' input[name="Mineiraocard[documento]"]').val()))
           ) {
            showMsg('O <strong>documento</strong> é obrigatório. <br />Se for CPF, deve conter 11 números, sem caracteres.');
            return false;
        }

        if ($('#formMineiraocard-' + id + ' input[name="Mineiraocard[email]"]').val() != '') {
			var email=$('#formMineiraocard-' + id + ' input[name="Mineiraocard[email]"]').val();
			if (email.indexOf("@")==-1 || email.indexOf(".")==-1) {
            showMsg('O <strong>e-mail</strong> não é obrigatório, mas se for preenchido deve ser válido.');
			return false;
            }            
        }
        
        $('#formMineiraocard-' + id).find('.mineiraocard-info').html('');
        doAjax ($('#formMineiraocard-'+id).attr('action'), 'POST', $('#formMineiraocard-'+id).serialize(), 'JSON', 7000, 'nomesMineiraoCardOk', 'nomesMineiraoCardErr');
    });

    $('#ir-para-pagamento').click(function() {
        camposOcupanteOk=new Array();
        $('.form-mineiraocard-ocupantes .Mineiraocard_valido').each(function(i,o){
            camposOcupanteOk.push($(o).val());
        });
        if(camposOcupanteOk.indexOf('')>-1){
            $('#modal-mensagem .modal-body').html('Você deve preencher os dados dos ocupantes e clicar em OK para cada assento antes de prosseguir. Em seguida clique em PAGAMENTO.');
            $('#modal-mensagem').modal('toggle');
        }
        else
            window.location.href = $('#url-pagamento').val();
    });
    
    $('.bloco').click(function() {
        if ($(this).attr('id') == 'bloco_108' || $(this).attr('id') == 'bloco_109' || $(this).attr('id') == 'bloco_110' || $(this).attr('id') == 'bloco_111' || $(this).attr('id') == 'bloco_128' || $(this).attr('id') == 'bloco_130') {
            konduto('bloco', 'selecao');
            $('.imgMapa').addClass('hidden');
            $('#closeImg').removeClass('hidden');
            $('#textInstru').removeClass('hidden');
            $('#textInstru2').removeClass('hidden');
            $('#textInstru3').removeClass('hidden');
            var idBloco = $(this).attr('id').replace('bloco_', '');
            $('#assentos-' + idBloco).removeClass('hidden');
            $('#legMap').addClass('hidden');
            if ($(this).attr('id') == 'bloco_111'){
                $('#legMap').removeClass('hidden');
            }
        }
    });
	
    $('.imgMapa').click(function () {
        $('#modal-mensagem .modal-body').html('Para a escolha do assento, clique no quadro à direita em "Selecione um Produto".');
        $('#modal-mensagem').modal('toggle');
    });
    
    $('#closeImg').click(function () {
        $('.imgMapa').addClass('hidden');
        $('#closeImg').addClass('hidden');
        $('#textInstru').addClass('hidden');
        $('#textInstru2').addClass('hidden');
        $('#textInstru3').addClass('hidden');
        $('#legMap').addClass('hidden');
    });
    
    if (typeof erro_login != 'undefined' && erro_login != '') {
        loginErro(erro_login);
        mensagem = '';
    }    
    
    $('#form-login').on('beforeSubmit', function (e) {
        if (typeof tentativas_login != 'undefined' && tentativas_login >= 3 && grecaptcha.getResponse().length == 0) {
            showMsg('Confirme que não é um robô.');
            return false;
        }
    });
    
    $('.check-segundavia').click(function () {
        var total = $('.check-segundavia:checked').length * 60;
        $('#taxa').val('R$ ' + parseFloat(total).toFixed(2).replace('.', ','));
        $('#botao-segvia1').prop('disabled', $('.check-segundavia:checked').length > 0 ? '' : 'disabled');
    });
    
    $('#modal-segvia1').on("show.bs.modal",function(){
        infoCartoes=dadosSegundaVia();
        var divInfoCartao='';
        $.each(infoCartoes, function(i,o){
            divInfoCartao+="<div class='col-xs-12'>"+o.produto+"</div>";
            divInfoCartao+="<p class='col-xs-5 small'>Nome a ser impresso no cartão:</p>";
            divInfoCartao+="<p class='col-xs-7'><input type='text' id='ocupante-"+o.qrcode+"-nome' value='"+o.nome+"' /></p>";
            divInfoCartao+="<p class='clearfix'></p>";
        })
        $("#info-mineiraocard").append(divInfoCartao);
    })

     $('#modal-segvia1').on("hide.bs.modal",function(){
        $("#info-mineiraocard").html('');
        $('#btn-segvia1').button('reset');
    })

    $('#botao-segvia1').click(function () {
        if ($('.check-segundavia:checked').length > 0) {
            $('#taxa2viaStr').val($('#taxa').val());
            $('#taxa2via').val($('#taxa').val().replace('R$ ', '').replace(',', '.'));
            $('#modal-segvia1').modal('toggle');
        }
        return false;
    });
    
    $('#btn-segvia1').click(function () {
        if ($('#taxa2via').val() == '') {
            showMsg('Escolha os cartões para os quais deseja solicitar 2ª via.');
            return false;
        }
        if ($('#cvv2via').val() == '') {
            showMsg('Informe o CVV do cartão cadastrado para autorizar o pagamento.');
            return false;
        }
        $(this).button('loading');
        var formTaxa = getFormTaxa();
        doAjax ($('#url-cobranca').val(), 'POST', formTaxa, 'JSON', 5000, 'cobrarTaxaOK', 'cobrarTaxaErro');
    });
	
    $('[data-toggle="tooltip"]').tooltip();
    $("#btn-assentos-assento-livre").click(function(){
        if ($('.linha-carrinho').length == 0) {
            $('#modal-mensagem .modal-body').html('Não há produtos em seu carrinho.');
            $('#modal-mensagem').modal('toggle');
            return false;
        }
        window.location.assign($("#url-ocupantes").val());
    });
});

function getFormTaxa (codigoVenda) {
    var formTaxa = {
        'trid': $('#tridTaxa').val(),
        'codigo_cartao': $('#cartaoTaxa').val(), 
        'taxa2via': $('#taxa2via').val(), 
        'senha': $('#cvv2via').val(),
        'parcelas': $('#parcelaTaxa').val()
    };
    if (codigoVenda != '') {
        formTaxa['id_compra'] = codigoVenda;
    }
    return formTaxa;
}

function cobrarTaxaOK (data) {
    var formTaxa = getFormTaxa();
    doAjax ($('#url-consulta').val(), 'POST', formTaxa, 'JSON', 5000, 'consultarTaxaOK', 'consultarTaxaErro');
    return false;
}

function cobrarTaxaErro (data) {
    showMsg('Erro ao enviar a cobrança da taxa: ' + data.erro.mensagem);
    $('#btn-segvia1').button('reset');
    return false;
}

function consultarTaxaOK (data) {
    if (data.hasOwnProperty('objeto') && data.objeto.hasOwnProperty('codigo') && data.objeto.codigo != '') {
        var formTaxa = getFormTaxa(data.objeto.codigo);
        doAjax ($('#url-autoriza').val(), 'POST', formTaxa, 'JSON', 50000, 'autorizarTaxaOK', 'autorizarTaxaErro');
        return false;
    }
    showMsg('Erro ao consultar a cobrança da taxa.');
    $('#btn-segvia1').button('reset');
    return false;
}

function consultarTaxaErro (data) {
    showMsg('Erro ao consultar a cobrança da taxa: ' + data.erro.mensagem);
    $('#btn-segvia1').button('reset');
    return false;
}

function dadosSegundaVia(){
    var _ingressos=[];
    var indexPost = 0;
    $('.check-segundavia').each(function (index, element) {
        if ($(this).is(":checked")) {
            _ingressos[indexPost] = {
                'qrcode': $(this).attr('data-qrcode'),
                'id_agenda': $(this).attr('data-evento'),
                'produto': $(this).attr('data-produto'),
                'nome': $(this).attr('data-nomecartao')
            };
            indexPost++;
        }
    });
    return _ingressos;
}

function autorizarTaxaOK (data) {
    ingressos=dadosSegundaVia();
    $.each(ingressos, function(i,o){
        var nomeCampoModal=$("#ocupante-"+o.qrcode+"-nome").val();
        var nomeAntigo = o.nome
        if(nomeAntigo == nomeCampoModal || nomeCampoModal == '')
            delete o.nome;        
        if(nomeAntigo != nomeCampoModal && nomeCampoModal != '')
            o.nome = nomeCampoModal;
        
        delete o.produto;
    })
    doAjax ($('#url-segunda-via').val(), 'POST', {'ingressos': ingressos}, 'JSON', 90000, 'segundaviaOK', 'segundaviaErro');
    return false;
}

function autorizarTaxaErro (data) {
    showMsg('Erro ao autorizar a cobrança da taxa: ' + data.erro.mensagem);
    $('#btn-segvia1').button('reset');
    return false;
}

function segundaviaOK (data) {
    emailSegundaVia();
    return false;
}

function segundaviaErro (data) {
    showMsg('Erro ao solicitar segunda via de cartões: ' + data.erro.mensagem);
    $('#btn-segvia1').button('reset');
    return false;
}

function emailSegundaVia () {
    doAjax ($('#url-email-2via').val(), 'POST', {'ingressos': ingressos}, 'JSON', 100000, 'email2viaOK', 'email2viaErro');
    return false;
}

function email2viaOK (data) {
    showMsg('Segunda via de cartões solicitada com sucesso.');
    $('#modal-mensagem').on('hidden.bs.modal', function (e) {
        e.preventDefault();
        window.location.href = $('#url-cliente').val();
    });
}

function email2viaErro (data) {
    
}

function checkCarrinho () {
    var existe = false;
    $('input[name="id[]"]').each(function (index, element) {
        if ($('#carrinho-codigo').val() == element.value && tipoAssento!='livre') {
            existe = true;
        }
    }); 
    return existe;
}

function delCart(id) {
    doAjax ($('#url-del-cart').val(), 'POST', {'id_evento': $('#id_evento').val(), 'id': id}, 'JSON', 10000, 'delCartOK', 'delCartErr');
    return false;
}

function delCartOK (data) {
    if ($('#tr-' + idLinha).length > 0) {
        $('#tr-' + idLinha).remove();
        $('.select-lugares > option').each(function (index, element) {
            if (element.value === idLinha) {
                $(this).attr('title', '').attr('disabled', false).removeClass('carrinho-selected');
                return false;
            }
        });
    }
    if ($('.div-' + idLinha).length > 0) {
        window.location.reload(); 
    } else {
        mostrarRelogioCarrinho();
    }
    return false;
}

function delCartErr(data) {
    $('#modal-mensagem .modal-body').html('Erro ao remover compra no carrinho');
    $('#modal-mensagem').modal('toggle');
}

function loginErro (mensagem) {
    showMsg(mensagem);
    return false;
}

function enviarMensagemEsqueciSenha () {
    $('.close').trigger('click');
    doAjax ($('#url-mensagem-esqueci').val(), 'POST', {'user_id': $('#user_id-esqueci').val()}, 'JSON', 10000, 'esqueciOK', 'esqueciErr');
    return false;
}

function esqueciOK (data) {
    $('#modal-mensagem .modal-body').html('Verifique a caixa de mensagens do e-mail informado.');
    $('#modal-mensagem').modal('toggle');
    return false;
}

function esqueciErr (data) {
    $('#modal-mensagem .modal-body').html('Falha ao enviar mensagem para troca de senha: ' + data.erro.mensagem);
    $('#modal-mensagem').modal('toggle');
    return false;
}

//montar elementos HTML do mapa com base na disponibilidade do evento usando o @param areasEvento
//cada área disponível recebe um link com href base igual ao @param href
function montarAreas(areasEvento,href){
    var output=new Array();
    for(var i=0 ; i<areasMineirao.length ; i++){
        var d=document.createElement('DIV');
        var a=document.createElement('A');
        d.setAttribute('class','areaEvento');
        if(areasEvento.indexOf(areasMineirao[i])!=-1){
            d.setAttribute('class','areaEvento disponivel');
            a.setAttribute('href',href+'&area='+areasMineirao[i]);
            d.appendChild(a);
        }
        d.setAttribute('id','area'+areasMineirao[i]);
        output.push(d);
    }
    return output;
}

function extrairInfoProduto(spl,setor){
    var produto;
    var nome;
    if(spl[0]=='bloco' && setor=='Roxo')
        produto='Mineirão Tribuna';
    
    if(spl[1]=='vip201' || spl[1]=='vip202' || spl[1]=='vip203')
        produto='Camarote Brahma';
    
    if(spl[0]=='bloco' && setor=='Vermelho')
        produto='Mineirão Embaixada';

    if(spl[0]=='camarote')
        produto='Camarote';

    nome = spl[0]=='bloco' ?'Bloco ' :'';
    nome += produto=='Camarote Brahma' ?spl[1].replace('vip','') :spl[1];

    if(spl[1]=='vip105' || spl[1]=='vip106' || spl[1]=='vip107')
        nome = 'Bloco '+spl[1].replace('vip','')+' superior';

    return {produto:produto,nome:nome};
}

//usar as informações em um elemento fora do mapa (para ficar acima do z-index)
function infoConteudo(elm,setor){
    var id=elm.attr('id');
    var spl=id.split('_');
    var extrairInfo=extrairInfoProduto(spl,setor);
    var oferecido=$("#oferecido_"+id).val();
    var disponivel=$("#disponivel_"+id).val();
    //boxInfo("<p><strong>"+extrairInfo.produto+"</strong> "+"<br />"+extrairInfo.nome+"<br />Capacidade: "+oferecido+" lugares<br />Disponível: "+disponivel+" lugares</p>");
    boxInfo("<p><strong>"+extrairInfo.produto+"</strong> "+"<br />"+extrairInfo.nome+"<br />Capacidade: "+oferecido+" lugares<br /></p>");
}

function boxInfo(conteudo){
    $("#infoSetor").html(conteudo);
    $("#infoSetor").show();
    $("#mapaRoxo, #mapaVermelho").mousemove(function(event){
        event = event || window.event;
        $("#infoSetor").css('left',(event.pageX)+'px')
        $("#infoSetor").css('top',(event.pageY - $("#infoSetor").outerHeight() - 10)+'px')
    });
}

//retirar a visualização do elemento infoSetor
function offInfoConteudo(){
    $("#infoSetor").hide();
    $("#infoSetor").html('');
}

// adicionar produto para tabela de pedido. O @param id é o id do elemento clicado
function addProduto(id, setor) {
    if(id == undefined || id == null) return false;
    var idSetor = setor.split('_');
    if ($('#disponivel_camarote_' + idSetor[1]).val() == 0) {
        $('#modal-mensagem .modal-body').html('Este camarote não possui mais lugares disponíveis.');
        $('#modal-mensagem').modal('toggle');
        return false;
    }
    doAjax ($('#url-get-produto').val(), 'GET', {id:id}, 'json', 10000, 'produtoTabelaPedido', 'erroProdutoTabela');
}

// buscar os assentos de um bloco pela API para adicionar no carrinho
function listaLugares(id){
    if(id == undefined || id == null) return false;
    splId=id.split('_');
    var matriz=$("#matriz-produtos").val();
    var area=$("#area-estadio").val();
    var tipo='assento';
    var bloco=splId[1];
    var post=[{'nome':'matriz','valor':matriz},{'nome':'area','valor':area},{'nome':'tipo','valor':tipo},{'nome':'bloco','valor':bloco}];
    doAjax ($('#url-get-produto-tags').val(), 'POST', {post:post}, 'json', 10000, 'selecionarAssento', 'erroProdutoTabela');
}

function produtoTabelaPedido (data) {
    var d = data.objeto[0];
    var setor = d.tags.tipo == 'bloco' ? d.tags.bloco : d.tags.camarote;
    
    $('#tbl-assento').html(d.tags.tipo + ' ' + setor);
    $('#carrinho-preco-unitario').val(parseFloat(d.valor).toFixed(2).replace('.', ','));
    $('#div-item').removeClass('hidden');
    $('#carrinho-quantidade').attr('readonly', false).attr('max', parseInt(data.objeto[0].estoque.disponivel)).val('1');
    $('#carrinho-desconto').val(d.desconto.fixo);
    $('#carrinho-codigo').val(d.codigo);
    $('#carrinho-area').val(d.tags.area);
    $('#carrinho-bloco').val(setor);
}

function selecionarAssento(data) {
    if(data.successo != 1) return;
    var d=data.objeto[0];
    var sel=document.createElement('SELECT');
    sel.setAttribute('class', 'select-lugares');
    var i=0;
    var l=data.objeto.length;
    var carrinho = [];
    $('input[name="id[]"]').each(function(index, element) {
        carrinho.push(element.value);
    }); 
    
    var opt = document.createElement('OPTION');
    opt.innerHTML = 'Selecione um produto';
    sel.appendChild(opt);
    
    for (i = 0; i < l; i++) {
        var opt = document.createElement('OPTION');
        opt.innerHTML = data.objeto[i].nome;
        opt.setAttribute('value', data.objeto[i].codigo);
        if (reservaAssento(data.objeto[i]) == false) {
            opt.setAttribute('title','Este item está reservado');
            opt.setAttribute('class', 'carrinho-selected');
            opt.disabled = true;
        } else {
            isReserva = isReservaCliente(data.objeto[i].tags);
            if (isReserva['reserva'] && isReserva['documento'] == $('#documento').val() && parseInt(data.objeto[i].estoque.disponivel) > 0) {
                opt.setAttribute('title', 'Este item está reservado para você.');
                opt.setAttribute('class', 'carrinho-reserva-cliente');
            } else if (parseInt(data.objeto[i].estoque.disponivel) == 0 || (isReserva['reserva'] && isReserva['documento'] != $('#documento').val())) { 
                opt.setAttribute('title', 'Este item está indisponível.');
                opt.setAttribute('class', parseInt(data.objeto[i].estoque.reservado) > 0 ? 'carrinho-vendido' : 'carrinho-selected');
                opt.disabled = true;
            } else {
                opt.setAttribute('title', $.inArray(data.objeto[i].codigo, carrinho) != -1 ? 'Este item já está no seu carrinho.' : '');
                opt.setAttribute('class', $.inArray(data.objeto[i].codigo, carrinho) != -1 ? 'carrinho-selected' : '');
                opt.disabled = $.inArray(data.objeto[i].codigo, carrinho) != -1 ? true : false;
            }
        }
        sel.appendChild(opt);
    }
    $('#tbl-assento').html(sel.outerHTML);
    $('#carrinho-preco-unitario').val(parseFloat(d.valor).toFixed(2).replace('.', ','));
    $('#div-item').removeClass('hidden');
    $('#carrinho-quantidade').attr('max', '1').attr('readonly', '1').val('1');
    $('#carrinho-desconto').val(d.desconto.fixo);
    $('#carrinho-codigo').val(d.codigo);
    $('#carrinho-area').val(d.tags.area);
    $('#carrinho-bloco').val(d.tags.bloco);
}

function reservaAssento(produto){
    if(typeof assentosSelect == "undefined" || typeof blocosSelect == "undefined") return true;
    return assentosSelect.indexOf(produto.codigo) > -1;
}

function isReservaCliente (tags) {
    var returnTag = false;
    var documento = '';
    $.each(tags, function(key, value) {
        if (key.indexOf('reservar_') > -1 && key.indexOf('reservar_just_') == -1) {
            documento = value;
            returnTag = true;
        } else if (key.indexOf('liberar_') > -1 && key.indexOf('liberar_just_') == -1) {
            documento = '';
            returnTag = false;
        }
    });
    return {'reserva': returnTag, 'documento': documento};
}

function adicionarItemCarrinho() {
    idProdutoLivre=$("#carrinho-codigo").val();
    doAjax ($('#url-add-cart').val(), 'POST', $('#form-carrinho').serialize(), 'JSON', 10000, 'addCartOK', 'addCartErr');

}

function addCartOK (data) {
    var subtotal = parseFloat($('#carrinho-preco-unitario').val().replace(',', '.')) * parseInt($('#carrinho-quantidade').val());
    var subtotalDesc = parseFloat($('#carrinho-desconto').val()) > 0 ? 
        (parseFloat($('#carrinho-preco-unitario').val().replace(',', '.')) - parseFloat($('#carrinho-desconto').val())) * parseInt($('#carrinho-quantidade').val()) : 
        0;
    if ($('.select-lugares').length > 0) {
        $('.select-lugares option:selected').attr('title', 'Este item já está no seu carrinho.').attr('disabled', true).addClass('carrinho-selected');
    } 
    //procurar produto do mesmo id - caso encontre, aumenta só a qtd
    var idProdutoNoCarrinho=$('#tr-'+idProdutoLivre);
    if(idProdutoNoCarrinho.length>0){
        var qtdTotal=parseInt($(idProdutoNoCarrinho).find('td:nth-child(2)').text())+parseInt($('#carrinho-quantidade').val())
        $(idProdutoNoCarrinho).find('td:nth-child(2)').text(qtdTotal);
        var precoTotal=parseFloat($('#carrinho-preco-unitario').val().replace(',', '.')) * qtdTotal;
        $(idProdutoNoCarrinho).find('td .precoProduto').text(precoTotal.toFixed(2).replace('.',','));
    }
    if(idProdutoNoCarrinho.length<=0){
        var tr = 
            '<tr id="tr-' + $('#carrinho-codigo').val() + '" class="linha-carrinho">' + 
                // '<td>' + $('#carrinho-area').val() + '</td>' + 
                '<td>' + ($('.select-lugares').length > 0 ? $('.select-lugares option:selected').text() : $('#tbl-assento').html()) + '</td>' + 
                '<td style="text-align:right;">' + $('#carrinho-quantidade').val() + '</td>' + 
                '<td>' + (subtotalDesc > 0 ? 
                    'R$ <span class="precoProduto">' + subtotalDesc.toFixed(2).replace('.', ',') + '</span><br>' +
                    '<span class="strikeDesconto">R$ ' + subtotal.toFixed(2).replace('.', ',')  + '</span>' : 
                    'R$ <span class="precoProduto">' + subtotal.toFixed(2).replace('.', ',') + '</span>') +
                '</td>' + 
                '<td align="center"><i class="fa fa-trash trash-item plus-red" id="trash-' + $('#carrinho-codigo').val() + '" data-trash="' + $('#carrinho-codigo').val() +  '" title="Remover este item do seu carrinho"></i></td>' + 
                '<input type="hidden" name="unidades[]" value="' + $('#carrinho-quantidade').val() + '" min="0" />' + 
                '<input type="hidden" name="id[]" value="' + $('#carrinho-codigo').val() +  '">' +
                '<input type="hidden" name="area[]" value="' + $('#carrinho-area').val() + '">' +
                '<input type="hidden" name="setor[]" value="' + $('#carrinho-bloco').val() +  '">' +
                '<input type="hidden" name="preco_unitario[]" value="' + $('#carrinho-preco-unitario').val() +  '">' +
                '<input type="hidden" name="desconto[]" value="' + $('#carrinho-desconto').val() +  '">' +
            '</tr>';
        $('#linhasLugares').append(tr);
    }
    $('#tbl-header').removeClass('hidden');
    //$('#tabBreadCrumb').css('background-image', 'url(../images/losang1.png)');
    mostrarRelogioCarrinho();
}

function setDataExpira (expira) {
    var arrExpira = expira.split(',');
    var dataExpira = new Date(arrExpira[0], (arrExpira[1] == '11' ? 0 : arrExpira[1]-1), arrExpira[2], arrExpira[3], arrExpira[4], arrExpira[5]);
    return dataExpira;
}

function mostrarRelogioCarrinho() {
    doAjax ($('#url-tempo-carrinho').val(), 'POST', {}, 'JSON', 2000, 'relogioOK', 'relogioErr');
    return false;
}

function relogioOK (data) {
    deadline = setDataExpira(data.tempoCarrinho);
    initializeClock(deadline);
    return false;
}

function relogioErr (data) {return false;}

function addCartErr (data) {
    $('#modal-mensagem .modal-body').html('Não foi possível adicionar este item ao carrinho. Erro: ' + data.erro.mensagem);
    $('#modal-mensagem').modal('toggle');
    return false;
}

function erroProdutoTabela(data){
    $('#modal-mensagem .modal-body').html('Não existem lugares disponíveis por enquanto nesse setor.');
    $('#modal-mensagem').modal('toggle');
    return false;
}

function vistaOesteEsquerda(data){
    $('#modal-imagem .modal-body').html('<img src="images/vista_oeste_lado_esquerdo.jpg">');
    $('#modal-imagem').modal('toggle');
    return false;
}

function vistaOesteDireita(data){
    $('#modal-imagem .modal-body').html('<img src="images/vista_oeste_lado_direito.jpg">');
    $('#modal-imagem').modal('toggle');
    return false;
}

function vistaBL130(data){
    $('#modal-imagem .modal-body').html('<img src="images/vista_lest_bl130.jpg">');
    $('#modal-imagem').modal('toggle');
    return false;
}

function vistaBL128(data){
    $('#modal-imagem .modal-body').html('<img src="images/vista_lest_bl128.jpg">');
    $('#modal-imagem').modal('toggle');
    return false;
}

$(document).on("pjax:end", function(e) {
    console.log("Pjax ended");
});
$(document).on("pjax:error", function(event, xhr) {
    console.log("Pjax failed");
    console.log(xhr.responseText);
    event.preventDefault();
});

function lugaresRenovacao(assentosSelect){
    console.log($(".select-lugares").html());
}

function nomesMineiraoCardOk(data){
    var form = data.form;
    $('#'+data.form).find('.Mineiraocard_valido').val('1');
    $('#'+data.form).find('.mineiraocard-retorno').removeClass('alert alert-warning').html(data.mensagem);
    $('#'+data.form).find('input[name="Mineiraocard[id]"]').val(data.id);
    mostrarRelogioCarrinho();
}

/**
 * @todo manter valores antigos nos campos em caso de erro
*/
    
function nomesMineiraoCardErr(data){
    $('#'+data.form).find('.mineiraocard-retorno').html('');

    if(data.erros.nome!=undefined){
        var infoNome=data.erros.nome[0];
        $('#'+data.form).find('.mineiraocard-info-nome').html(infoNome)
    }
    if(data.erros.documento!=undefined){
        var infoDoc=data.erros.documento[0];
        $('#'+data.form).find('.mineiraocard-info-documento').html(infoDoc)
    }
    if(data.erros.telefone!=undefined){
        var infoTel=data.erros.telefone[0];
        $('#'+data.form).find('.mineiraocard-info-telefone').html(infoTel)
    }

    if(data.successo == 0) {
        var messages = '';

        $.each(data.erros, function(index, value) {
            if(value[0]!=undefined) 
                messages += value[0]+'<br />';
        });
        showMsg(messages);
        return false;
    } 
}

function infoObjs(a) {
   var id = a.attr('id');
   var info = $('#'+id.replace('linhaCompra','headerInfo'));
   var infoId = $(info).attr('id').replace('headerInfo','bodyInfo')
   var infoBody = $('tr[id*="'+infoId +'"]')
   return [info,infoBody]
}

function isCPFValid(strCPF) {
    var Soma = 0;
    var Resto;
    strCPF = $.trim(strCPF.replace(/\./g, '').replace('-', ''));

    if (strCPF.length !== 11) {
        return true;
    }
    if (isNaN(strCPF) || 
        strCPF === '00000000000' || strCPF === '11111111111' || strCPF === '22222222222' || strCPF === '33333333333' || 
        strCPF === '44444444444' || strCPF === '55555555555' || strCPF === '66666666666' || strCPF === '77777777777' || 
        strCPF === '88888888888' || strCPF === '99999999999') {
        return false;
    }

    for (i = 1; i <= 9; i++) {
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    }
    Resto = (Soma * 10) % 11;
    if ((Resto === 10) || (Resto === 11)) {
        Resto = 0;
    }
    if (Resto !== parseInt(strCPF.substring(9, 10))) {
        return false;
    }

    Soma = 0;
    for (i = 1; i <= 10; i++) {
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    }
    Resto = (Soma * 10) % 11;

    if ((Resto === 10) || (Resto === 11)) {
        Resto = 0;
    }
    if (Resto !== parseInt(strCPF.substring(10, 11))) {
        return false;
    }
    return true;
}
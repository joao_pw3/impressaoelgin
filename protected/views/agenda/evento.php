<?php
use app\assets\LightboxAsset;
use app\assets\SlickAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\CarrouselSimplesAsset; 
use app\components\TempoCarrinhoWidget;

CarrouselSimplesAsset::register($this);
LightboxAsset::register($this);
SlickAsset::register($this);

$session = Yii::$app->session;
$session->open(); 
$dataInicial = date('YmdHis', strtotime(str_replace('/', '-', $evento->dataAtivar)));
$dataFinal = date('YmdHis', strtotime(str_replace('/', '-', $evento->dataDesativar)));
$eventoNoAr = ($dataInicial <= date('YmdHis') && $dataFinal >= date('YmdHis') && $evento->ativo == 1);
?>
<input type="hidden" id="url-add-cart" value="<?= Url::to(['site/add-carrinho','relativo'=>1]); ?>">
<input type="hidden" id="url-del-cart" value="<?= Url::to(['site/del-carrinho']); ?>">
<input type="hidden" id="url-resumo" value="<?= Url::to(['site/resumo']); ?>">
<input type="hidden" id="url-visitante" value="<?= Url::to(['site/visitante']); ?>">
<input type="hidden" id="user" value="<?= isset($session['user_token']) && !empty($session['user_token']) ? 1 : 0; ?>">
<input type="hidden" id="documento" name="documento" value="<?= $session['documento']; ?>">
<input type="hidden" id="url-index" value="<?= Url::to(['site/index']); ?>">
<input type="hidden" id="url-get-produto" value="<?= Url::to(['site/produto-id']); ?>">
<input type="hidden" id="url-get-produto-tags" value="<?= Url::to(['site/produto-por-tag']); ?>">
<input type="hidden" id="url-bloco-assento" value="<?= Url::to(['site/get-bloco-assento']); ?>">
<input type="hidden" id="url-tempo-carrinho" value="<?= Url::to(['site/tempo-carrinho']); ?>">
<div class="bloco" id="bloco">
    <div class="container">
        <?php if ($eventoNoAr) { ?>
        <?= TempoCarrinhoWidget::getBreadCrumb('losang1.png'); ?>
        <?php } ?>
        <div id="mapaPjax">
            <div class="col-md-8 col-sm-12 col-xs-12 nopadding">

                <div id="mapaVermelho" style="min-height:315px;">
                    <?= Html::img($evento->anexos['imgEvento']); ?>
                    <br>
                    <br>
                    <div style="margin-left:20px;">
                        <h4 style="font-size:20px; color:#000;"><?= $evento->nome ?></h4>
                        <?=$evento->dataInicial?><br><br>
                        <?php if ($eventoNoAr) { ?>
                        <p>Está com dúvida de como fazer sua compra? <a href="<?= Url::to(['site/faq']); ?>">Clique Aqui.</a></p>
                        <?php } ?>
                    </div>
                </div>
                <br>
            </div>

            <div class="col-md-4 col-sm-12 col-xs-12 about-right <?= (!isset($this->context->setor)) ? 'wow fadeInRight animated' : ''; ?> textosAprest3" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                <?php if (!$eventoNoAr) { ?>
                <div>
                    <br><br><br><br>
                    <span class="row contato carrDest" style="width: 80%;">
                        <p>Este evento não está mais disponível para compra.</p>
                        <a class="carrinho seta" href="<?= Url::to(['site/index']); ?>">Voltar à página de eventos</a>
                    </span>
                </div>
                <?php } else { ?>
                <div class="tabProd2" id="div-item">
                    <form id="form-carrinho" method="post">
                        <input type="hidden" id="url-add-produto" value="<?= Url::to(['compra/adicionar-produto-carrinho']); ?>">
                        <input type="hidden" id="url-del-produto" value="<?= Url::to(['compra/remover-produto-carrinho']); ?>">
                        <input type="hidden" id="url-ocupantes" value="<?= Url::to(['compra/ingresso']); ?>">
                        <input type="hidden" id="url-qtd-produto" value="<?= Url::to(['agenda/qtde-produtos']); ?>">
                        <input type="hidden" id="evento" value="<?= $evento->codigo; ?>">
                        <input type="hidden" id="data" value="<?=$evento->datas[0]->id?>">
                        <input type="hidden" name="id_evento" id="id_evento" value="<?= $evento->codigo; ?>">
                        <input type="hidden" id="carrinho-codigo" name="carrinho-codigo" value="<?=$produtos[0]->codigo?>">
                        <input type="hidden" id="carrinho-area" name="carrinho-area">
                        <input type="hidden" id="carrinho-bloco" name="carrinho-bloco">
                        <input type="hidden" id="carrinho-desconto" name="carrinho-desconto">
                        <table class="table table-condensed" id="table-item">
                            <thead>
                                <tr>
                                    <th align="center" width="50%">Setor</th>
                                    <th align="center" width="35%">Preço unitário</th>
                                    <th align="center" width="10%">Quantidade</th>
                                    <th align="center" width="5%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td id="tbl-assento" width="50%"><?=$tags['Espaço']?></td>
                                    <td width="35%"><input type="text" id="carrinho-preco-unitario" name="carrinho-preco-unitario" readonly value="<?=number_format($produtos[0]->valor,2,',','.')?>"></td>
                                    <td width="10%"><input type="number" autofocus id="carrinho-quantidade" name="carrinho-quantidade" min="1" maxlength="3" value="1"></td>
                                    <td width="5%" align="center" valign="middle"><i class="fa fa-cart-plus fa-2x plus-white" title="Adicionar ao seu carrinho"></i></td>
                                </tr>
                            </tbody>
                        </table>
                        <span class="msg-carrinho">* Informe a quantidade e clique em <i class="fa fa-cart-plus fa-2x"></i></span>
                    </form>
                </div>

                <form method="post" id="form-bloco">
                    <input type="hidden" name="id_evento" id="id_evento" value="<?= $evento->codigo; ?>">
                    <div class="tabProd">
                        <div><i class="fa fa-shopping-cart fa-2x"></i> Meu Carrinho:</div>
                        <table class="table table-condensed table-stripped">
                            <thead id="tbl-header" class="<?= empty($carrinho->objeto->produtos) ? 'hidden' : ''; ?>">
                                <tr>
                                    <!-- <th width="15%">Área</th> -->
                                    <th width="40%">Setor / Bloco</th>
                                    <th width="10%">Itens</th>
                                    <th width="30%">Subtotal</th>
                                    <th width="5%"></th>
                                </tr>
                            </thead>
                            <tbody id="linhasLugares">
                            <?php 
                            // listando produtos do carrinho
                            // o for serve também para criar array de checagem para funções JS (permitir somente os produtos que o usuário pode renovar)
                            $blocosSelect=[];
                            $assentosSelect=[];
                            if (!empty($carrinho->objeto->produtos)) {  
                                foreach ($carrinho->objeto->produtos as $index => $produto) {
                                    $bloco = '';
                                    $area = '';
                                    $assentosSelect[]=$produto->codigo;
                                    if(isset($produto->tags)){
                                        foreach ($produto->tags as $index_tag => $tag) {
                                            if ($tag->nome == 'bloco') {
                                                $bloco = $tag->valor;
                                                $blocosSelect[]=$tag->valor;
                                            }
                                            if ($tag->nome == 'area') {
                                                $area = $tag->valor;
                                            }
                                        }
                                    }
                                ?>
                                <tr id="tr-<?= $produto->codigo; ?>" class="linha-carrinho">
                                    <!-- <td width="15%"><?= $area; ?></td> -->
                                    <td width="40%"><?= $tags['Espaço']; ?></td>
                                    <td width="10%" align="right"><?= number_format($produto->unidades, 0); ?></td>
                                    <td width="30%">
                                        <?php if (floatval($produto->desconto->fixo) > 0) { ?>
                                        R$ <span class="precoProduto"><?= number_format(($produto->valor - $produto->desconto->fixo) * $produto->unidades, 2, ',', ''); ?></span><br>
                                        <span class="strikeDesconto">R$ <?= number_format($produto->valor * $produto->unidades, 2, ',', ''); ?></span>
                                        <?php } else { ?>
                                        R$ <span class="precoProduto"><?= number_format($produto->valor * $produto->unidades, 2, ',', ''); ?></span>
                                        <?php } ?>
                                    </td>
                                    <td width="5%" align="center"><i class="fa fa-trash trash-item plus-red mouseMao" id="trash-<?= $produto->codigo; ?>" data-trash="<?= $produto->codigo; ?>" title="Remover este item do seu carrinho"></i></td>
                                    <input type="hidden" name="unidades[]" value="<?= number_format($produto->unidades, 0); ?>" min="0" />
                                    <input type="hidden" name="id[]" value="<?= $produto->codigo; ?>">
                                    <input type="hidden" name="area[]" value="<?= $area; ?>">
                                    <input type="hidden" name="setor[]" value="<?= $bloco; ?>">
                                    <input type="hidden" name="preco_unitario[]" value="<?= $produto->valor; ?>">
                                    <input type="hidden" name="desconto[]" value="<?= $produto->desconto->fixo; ?>">
                                </tr>                    
                                <?php 
                                } 
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </form>   
                <div style="margin:0px auto;">
                    <br>
                    <span class="row contato carrDest" style="width:50%;">
                        <input type="button" value="Finalizar a Compra&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" id="btn-assentos-assento-livre" class="icon-next carrinho btn-assentos">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a class="carrinho seta" href="<?= Url::to(['site/index']); ?>">Voltar à página de eventos</a>
                    </span>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<!--  -->

<div id="myModalMapa" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mapa</h4>
            </div>
            <div class="modal-body">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15035.457827120901!2d-46.9311361!3d-19.5903068!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1d29823c54e10742!2sSENAI+CFP+Arax%C3%A1+Djalma+Guimar%C3%A3es!5e0!3m2!1sen!2sbr!4v1524146897662" width="100%" height="370" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="myModalMapa2" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Fotos do Teatro</h4>
            </div>
            <div class="modal-body">
                <div class="imagemPeca" style="background-image: url(<?= isset($evento->anexos['imgEvento']) ? $evento->anexos['imgEvento'] : '' ?>); background-size:cover;"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs('
    urlGetMapaAssento="' . \yii\helpers\Url::to(['mapa/mapa']) . '";
    urlGetProdutoData="' . \yii\helpers\Url::to(['produtos/buscar-js']) . '";
    tipoAssento="'.$tipoAssento.'";
    function afterReveal (el) {
        el.addEventListener("animationend", function () {
            $("#carrinho-quantidade").focus()
        });
    }
    new WOW({ callback: afterReveal }).init();
    '
);
$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/carrinho.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    $this->registerJs("deadline = setDataExpira('" . $session['tempoCarrinho'] . "'); initializeClock(deadline);");
}

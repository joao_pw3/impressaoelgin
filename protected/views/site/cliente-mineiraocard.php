<?php
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\components\MenuWidget;
use app\components\TempoCarrinhoWidget;

$session = Yii::$app->session;
$session->open();

if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    echo TempoCarrinhoWidget::getBreadCrumb('losang2.png');
}

echo MenuWidget::getUrls();
?>
<div class="container">
    <div class="row">
        <?php echo MenuWidget::getMenuLateral(); ?>
        <div class="col-md-10">
            <div class="tab-content">
                <div id="tab-acesso" class="tab-pane fade active in">
                    <div class="boxDados">
                        <div class="container-fluid">
                            <div class="box-mineiraocard">
                                <div class="content" id="cli-mineiraocard">    
                                    <div class="row cli-creditos">
                                        <div class="lead">Meus cartões Mineirão Card</div>
                                        <p style="margin:20px 0px;">Aqui você poderá verificar todos os seus cartões Mineirão Card e a situação de cada um. Também é possível solicitar a 2ª via de seus cartões.</p> 
                                        <br>
                                        <?php if (!empty($ingressos->objeto)) { ?>
                                        <div class="mineiraocard-scroll">
                                            <div class="tabela-mineiraocard head-mineiraocard">
                                                <div>Ocupante</div>
                                                <div>Documento</div>
                                                <div>Assentos</div>
                                                <div>Comprado</div>
                                                <div>Status</div>
                                                <div>2ªvia</div>
                                            </div>
                                            <?php 
                                            foreach($ingressos->objeto as $index => $ingresso) { 
                                                if (in_array($ingresso->status, ['T', 'B', 'S'])) { 
                                            ?>
                                            <div class="tabela-mineiraocard">
                                                <div><?= $ingresso->ocupante_nome; ?></div>
                                                <div><?= $ingresso->documento; ?></div>
                                                <div><?= $ingresso->produto; ?></div>
                                                <div><?= $ingresso->data_venda; ?></div>
                                                <div>
                                                    <?= $ingresso->status == 'T' ? 'Ativo' : 
                                                        ($ingresso->status == 'S' ? '2ª via solicitada' : 'Bloqueado'); ?>
                                                </div>
                                                <div>
                                                    <?= $ingresso->status == 'T' ? 
                                                        '<input type="checkbox" class="check-segundavia" '
                                                        . 'data-evento="' . $ingresso->id_agenda . '" '
                                                        . 'data-nomecartao="' . $ingresso->ocupante_nome . '" '
                                                        . 'data-produto="' . $ingresso->produto . '" '
                                                        . 'data-qrcode="' . $ingresso->qrcode . '">' : 
                                                        '-'; ?>
                                                </div>
                                            </div>
                                            <?php } 
                                            }
                                            ?>
                                            <div class="col-xs-8" align="right" style="margin-top:10px">
                                                    <p>Taxa: </p>
                                            </div>
                                            <div class="col-xs-2" style="margin-top:10px">
                                                    <input type="text" class="input-taxa" name="taxa" id="taxa" value="R$ 0,00" readonly>
                                            </div>
                                            <div class="col-xs-2" align="right" style="margin-top:10px">
                                                    <button class="btn btn-primary" id="botao-segvia1" disabled>Solicitar 2ª via</button>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <br>
                                        <hr class="linhFina2">
                                        <br>
                                        <div class="alert alert-warning" style="width:100%;">
                                            • Para o pedido de 2ª via do seu cartão, será cobrado o valor de R$ 60,00 por cartão. O prazo para produção do cartão é de até 15 dias úteis.<br> 
                                            • Após produzido o cartão deverá ser retirado na Bilheteria Sul do Mineirão, conforme regulamento.<br>
                                            • A solicitação da 2ª via implica no bloqueio imediato do cartão anterior.<br>
                                            • Dúvidas e informações: <a href="mailto:atendimento@estadiomineirao.com.br" class="linkdeemail">atendimento@estadiomineirao.com.br</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal - Solicitação de 2º via -->
            <?php
            Modal::begin([
                'header' => '<h4 align="center">Atenção - 2ª via de cartão</h4>',
                'size' => 'modal-lg',
                'id' => 'modal-segvia1',
                'options' => [
                    'class' => 'modal-center modal-segvia'
                ]
            ]);
            ?>
            <form id="form-segvia1" name="form-segvia1" method="POST">
                <input type="hidden" id="url-cobranca" value="<?= Url::to(['site/enviar-cobranca']); ?>">
                <input type="hidden" id="url-consulta" value="<?= Url::to(['site/consultar-cobranca']); ?>">
                <input type="hidden" id="url-autoriza" value="<?= Url::to(['site/autorizar-pagamento']); ?>">
                <input type="hidden" id="url-segunda-via" value="<?= Url::to(['site/segunda-via']); ?>">
                <input type="hidden" id="url-email-2via" value="<?= Url::to(['site/email2via']); ?>">
                <input type="hidden" id="url-cliente" value="<?= Url::to(['site/cliente']); ?>">
                <input type="hidden" name="tridTaxa" id="tridTaxa" value="<?= substr(time() . mt_rand(), 8, 11); ?>">
                <input type="hidden" name="cartaoTaxa" id="cartaoTaxa" value="<?= $session['codigo_cartao']; ?>">
                <input type="hidden" name="taxa2via" id="taxa2via">
                <input type="hidden" name="parcelaTaxa" id="parcelaTaxa" value="1">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="font-14px">
                            <li>Para o pedido de 2ª via do seu cartão, será cobrado o valor de R$ 60,00 por cartão. O prazo para produção do cartão é de até 15 dias úteis.</li>
                            <li>Após produzido o cartão deverá ser retirado na Bilheteria Sul do Mineirão, conforme regulamento.</li>
                            <li>A solicitação da 2ª via implica no bloqueio imediato do cartão anterior.</li>
                            <li>Dúvidas e informações: <a href="mailto:atendimento@estadiomineirao.com.br" class="linkdeemail">atendimento@estadiomineirao.com.br</a></li>
                        </ul>
                        <hr>
                    </div>
                    <div id="info-mineiraocard">
                        
                    </div>
                    <div class="teb2via">
                        <div>
                            <p>Taxa:</p>
                        </div>
                        <div>
                            <input type="text" name="taxa2viaStr" class="input-taxa" maxlength="10" id="taxa2viaStr" readonly>
                        </div>
                        <div>
                            <p>CVV do seu cartão:</p>
                        </div>
                        <div>
                            <input type="password" name="cvv2via" id="cvv2via" class="font-passwd input-taxa" maxlength="4" style="width:50px; display:inline;">
                        </div>
                    </div>

                    <div class="col-md-12 contato" align="center">
                        <input type="button" id="btn-segvia1" class="icon-next carrinho" data-loading-text="Processando... Aguarde..." value="Autorizar pagamento">
                    </div>
                </div>
            </form>
            <?php Modal::end(); ?>
        </div>
    </div>
</div>

<?php

$this->registerJsFile('@web/js/ajax.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/carrinho.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/mineirao.js', ['depends' => ['yii\web\YiiAsset','yii\bootstrap\BootstrapAsset','yii\bootstrap\BootstrapPluginAsset',]]);
$this->registerJsFile('@web/js/cadastro.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset', 'yii\bootstrap\BootstrapPluginAsset']]);
if (isset($session['tempoCarrinho']) && !empty($session['tempoCarrinho'])) {
    $this->registerJs("deadline = setDataExpira('" . $session['tempoCarrinho'] . "'); initializeClock();");
}
?>
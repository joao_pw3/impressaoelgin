var urlCarrinho;
$(document).ready(function () {

    $(document).on('change', '#senha, #senha_repeat', function () {
        if ($('#senha').val() !== '' && $('#senha_repeat').val() !== '' && $('#senha').val() !== $('#senha_repeat').val()) {
            $('#modal-mensagem .modal-body').html('As senhas não são iguais. Tente novamente.');
            $('#modal-mensagem').modal('toggle');
            $('#senha').val('');
            $('#senha_repeat').val('');
        }
    });

    $(document).on('input', '#cep', function () {
        if ($(this).val().indexOf('_') == -1) {
            checkCep(this.value);
        }
    });

    $('#btn-cli-acesso').click(function () {
        if (!$('#cliente-aceite').is(':checked')) {
            $('#cliente-aceite').focus();
            $('#modal-mensagem .modal-body').html('Leia e aceite o regulamento para continuar.');
            $('#modal-mensagem').modal('toggle');
            return false;
        }
        doCadastro();
    });

    $('#btn-cli-dados').click(function () {
        doCadastroCompleto();
    });

    $('#btn-cartao').click(function () {
        $('#btn-cartao').button('loading');
        var dataCartao = $('#ano_venc').val() + $('#mes_venc').val();
        var dataCompleta = new Date();
        var hoje = '' + dataCompleta.getUTCFullYear() + (dataCompleta.getUTCMonth() < 9 ? '0' : '') + (dataCompleta.getUTCMonth() + 1);
        if (dataCartao < hoje) {
            $('#modal-mensagem .modal-body').html('Este cartão está vencido.');
            $('#modal-mensagem').modal('toggle');
            $('#btn-cartao').button('reset');
            return false;
        }
        doCadastroCartao();
    });

    $(document).on('change', '#check-same-name', function () {
        $('#nome_cartao').val($(this).is(':checked') ? $('#nome').val() : '');
    });

    $('#btn-logout').click(function () {
        window.location.href = $('#url-logout').val();
    });

    $('.cartao-favorito').click(function () {
        fazerCartaoFavorito($(this).attr('data-cartao'));
    });

    $('.cartao-excluir').click(function () {
        excluirCartao($(this).attr('data-cartao'));
    });

    jumpTab();

    $('#btn-termos').click(function () {
        $('#modal-termos').modal('toggle');
        return false;
    });

    $('#btn-politic').click(function () {
        $('#modal-politic').modal('toggle');
        return false;
    });
	
	$('#btn-politica').click(function () {
        $('#modal-politica').modal('toggle');
        return false;
    });

    $(document).on('click', 'a[href^="#"][data-toggle="tab"]', function (event) {
        event.preventDefault();
        history.pushState({}, '', this.href);
        jumpTab();
    });

    $("#btn-ir-para-carrinho").click(function(){
        window.location.assign(urlCarrinho)
    })
});

function doCadastro() {
    doAjax($('#url-cadastro').val(), 'POST', $('#form-cadastro').serialize(), 'JSON', 8000, 'cadastroOK', 'cadastroErr');
    return false;
}

function cadastroOK(data) {
    doAjax($('#url-login').val(), 'POST', $('#form-cadastro').serialize(), 'JSON', 8000, 'loginOK', 'loginErr');
    return false;
}

function cadastroErr(data) {
    if (data.successo === '0' && data.erro.codigo === '056') {
        $('#modal-mensagem .modal-body').html('Este e-mail já está cadastrado. Caso não saiba sua senha de acesso, <a href="' + $('#url-visitante').val() + '" target="_top">clique aqui</a> e utilize o link <strong>Esqueci minha senha</strong> para receber uma nova senha de acesso.');
        $('#modal-mensagem').modal('toggle');
        return false;
    }
    $('#modal-mensagem .modal-body').html('Erro ao efetuar cadastro do cliente: ' + data.erro.mensagem);
    $('#modal-mensagem').modal('toggle');
    return false;
}

function loginOK(data) {
    $('#modal-mensagem .modal-body').html('Cadastro inicial e login bem sucedido.');
    $('#modal-mensagem').modal('toggle');
    $('#modal-mensagem').on('hidden.bs.modal', function (e) {
        e.preventDefault();
        $('#li-personal').removeClass('disabled');
        var pagina = $('ul#navPills').find('#li-personal a').attr('href');
        if(pagina !== undefined)
            window.location.href = pagina;
        //$('a[href="#tab-dados"]').attr('data-toggle', 'tab').trigger('click');
    });
    return false;
}

function loginErr(data) {
    $('#modal-mensagem .modal-body').html('Erro ao efetuar login do cliente: ' + data.erro.mensagem);
    $('#modal-mensagem').modal('toggle');
    return false;
}

function doCadastroCompleto() {
    doAjax($('#url-cadastro-completo').val(), 'POST', $('#form-cadastro').serialize(), 'JSON', 8000, 'cadastroCompletoOK', 'cadastroCompletoErr');
    return false;
}

function cadastroCompletoOK(data) {
    $('#modal-mensagem .modal-body').html('Cadastro completo efetuado com sucesso.');
    $('#modal-mensagem').modal('toggle');
    $('#modal-mensagem').on('hidden.bs.modal', function (e) {
        e.preventDefault();
        $('#li-payment').removeClass('disabled');
        if(data.redirect!=undefined&&data.redirect!=''){
            window.location.assign(data.redirect);
            return;
        }
        window.location.assign('cliente-pagamentos');
        /*var pagina = $('ul#navPills').find('#li-payment a').attr('href');
        if(pagina !== undefined)
            window.location.href = pagina;
        $('a[href="#tab-pagamentos"]').attr('data-toggle', 'tab').trigger('click');*/
    });
}

function cadastroCompletoErr(data) {
    $('#modal-mensagem .modal-body').html('Erro ao efetuar cadastro completo do cliente: ' + data.erro.mensagem);
    $('#modal-mensagem').modal('toggle');
    return false;
}

function doCadastroCartao() {
    doAjax($('#url-cartao').val(), 'POST', $('#form-cadastro').serialize(), 'JSON', 8000, 'cartaoOK', 'cartaoErr');
    return false;
}

function cartaoOK(data) {
    $('#btn-cartao').button('reset');
    $('#modal-mensagem .modal-body').html('Cadastro de cartão efetuado com sucesso.');
    $('#modal-mensagem').modal('toggle');
    $('#modal-mensagem').on('hidden.bs.modal', function () {
        redirCartoes();
    });
}

function cartaoErr(data) {
    $('#btn-cartao').button('reset');
    $('#modal-mensagem .modal-body').html('Erro ao efetuar cadastro de cartão do cliente: ' + data.erro.mensagem);
    $('#modal-mensagem').modal('toggle');
    return false;
}

function fazerCartaoFavorito(cartao) {
    doAjax($('#url-cartao-favorito').val(), 'POST', {'codigo_cartao': cartao}, 'JSON', 8000, 'cartaoFavoritoOK', 'cartaoFavoritoErr');
    return false;
}

function cartaoFavoritoOK(data) {
    $('#modal-mensagem .modal-body').html('Este cartão foi definido como favorito!');
    $('#modal-mensagem').modal('toggle');
    $('#modal-mensagem').on('hidden.bs.modal', function () {
        redirCartoes();
    });
}

function cartaoFavoritoErr(data) {
    $('#modal-mensagem .modal-body').html('Falha ao definir cartão como favorito: ' + data.erro.mensagem);
    $('#modal-mensagem').modal('toggle');
}

function excluirCartao(cartao) {
    doAjax($('#url-cartao-excluir').val(), 'POST', {
        'codigo_cartao': cartao
    }, 'JSON', 8000, 'excluirCartaoOK', 'excluirCartaoErr');
    return false;
}

function excluirCartaoOK(data) {
    $('#modal-mensagem .modal-body').html('Este cartão foi removido!');
    $('#modal-mensagem').modal('toggle');
    $('#modal-mensagem').on('hidden.bs.modal', function () {
        redirCartoes();
    });
}

function excluirCartaoErr(data) {
    $('#modal-mensagem .modal-body').html('Falha ao remover cartão: ' + data.erro.mensagem);
    $('#modal-mensagem').modal('toggle');
}

function redirCartoes () {
    var urlRedir=$('#url-cliente').val();
    var linkCompraCarrinho=$("#finalizar-compra-vertical");
    if(linkCompraCarrinho.length>0)
        urlRedir=linkCompraCarrinho.attr('href');
    window.location.assign(urlRedir);
}

function checkCep(cep) {
    doAjax($('#url-cep').val(), 'POST', {
        'cep': cep
    }, 'JSON', 5000, 'checkCepOK', 'checkCepErr');
    return false;
}

function checkCepOK(data) {
    $('#endereco').val(data.objeto.endereco).prop('readonly', data.objeto.endereco == '' ? false : true);
    $('#bairro').val(data.objeto.bairro).prop('readonly', data.objeto.bairro == '' ? false : true);
    $('#cidade').val(data.objeto.cidade);
    $('#cidade_nome').val(data.objeto.cidade_nome);
    $('#cidade_uf').val(data.objeto.cidade_nome ? data.objeto.cidade_nome + ' / ' + data.objeto.uf : '');
    $('#uf').val(data.objeto.uf);
    if (data.objeto.cidade === '') {
        checkCepErr(data);
    }
}

function checkCepErr(data) {
    $('#modal-mensagem .modal-body').html('Este CEP não foi localizado. Por favor, verifique.');
    $('#modal-mensagem').modal('toggle');
}

function jumpTab() {
    var url = decodeURIComponent(window.location.search.substring(1));
    var urlFull = window.location.href;
    var parametros = url.split('&');
    for (i = 0; i < parametros.length; i++) {
        var nome = parametros[i].split('=');
        if (urlFull.indexOf('#tab-pagamentos') > -1 || urlFull.indexOf('#tab-cartao') > -1) {
            verificarDocumento();
        } else if ((nome[0] == 'hash_compra' && nome[1] != '') || urlFull.indexOf('#tab-historico') > -1) {
            loadHistorico();
        } else if (urlFull.indexOf('#tab-dados') > -1) {
            loadDados();
        }
        
    }
}

function loadCartoes() {
    $('[href="#tab-pagamentos"]').tab('show');
    //$('[href="#tab-cartoes"]').tab('show');
}

function loadHistorico() {
    $('[href="#tab-historico"]').tab('show');
}

function loadDados () {
    $('[href="#tab-dados"]').tab('show');
}

function loadDadosCartao() {
    $('#modal-mensagem .modal-body').html('Antes de cadastrar um cartão, por favor preencha seus dados pessoais.');
    $('#modal-mensagem').modal('toggle');
    $('#modal-mensagem').on('hidden.bs.modal', function () {
        loadDados();
    });
}

function verificarDocumento() {
    doAjax($('#url-verificar-documento').val(), 'POST', '', 'JSON', 4000, 'documentoOK', 'documentoErr');
}

function documentoOK() {
    loadCartoes();
}

function documentoErr() {
    loadDadosCartao();
}

$('#navPills a').on('click', function (e) {
    e.preventDefault();
    var page = $(this).parents('ul').data('nav');
    $(this).tab('show');
})


// Redireciona simulando o click no painel vertical
$("#acesso-horizontal").click(function () {
    $("#acesso-vertical").trigger('click');
});
$("#dados-pessoais-horizontal").click(function () {
    $("#dados-pessoais-vertical").trigger('click');
});
$("#meus-cartoes-horizontal").click(function () {
    $("#meus-cartoes-vertical").trigger('click');
});
$("#finalizar-compra-horizontal").click(function () {
    $("#finalizar-compra-vertical").trigger('click');
});

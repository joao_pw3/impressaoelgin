<?php
$this->title = 'ADM Easy for Pay';
?>

<div class="container">
	<div class="row">
		<h2 class="tituloGeral"><i class="fa fa-clone"></i> Remover Datas: <?=$evento->nome?></h2>
	</div>
	<div class="gridAgenda3Out">
		<div class="gridAgenda3 gridAgenda3Head">		
			<div>Data</div>
			<div>Evento</div>
			<div>Ação</div>
		</div>
		<?php
		foreach ($evento->datas as $data) {?>
			<div class="gridAgenda3">
				<div><?=$data->data?></div>
				<div><?=$evento->nome?></div>
				<div><a href="<?=\yii\helpers\Url::to(['remover-datas', 'evento'=>$evento->codigo, 'data'=>$data->data])?>" class="fontA botUnic">&#xf1f8;</a></div>
			</div>
		<?php }
		?>
	</div>
</div>
<?php
namespace app\assets;

use yii\web\AssetBundle;

class LightboxAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'enginelt/js/visuallightbox.js',
    ];
    public $depends = [
       'yii\web\YiiAsset',
    ];
    public $publishOptions = [
        'forceCopy' => true,
    ];
}

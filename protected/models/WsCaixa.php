<?php
namespace app\models;
use app\models\Ws;

/**
 * Classe para Caixa - Easy for Pay
 * obs.: parâmetros entre colchetes são opcionais
 * Padrões do ending point entsai para o caixa:
 *      saida:1 + ajuste:0 = fechamento 
 *      saida:0 + ajuste:0 = abertura
 *      saida:1 + ajuste:1 = sangria 
 *      saida:0 + ajuste:1 = reforço
 */
class WsCaixa extends Ws 
{
    /**
     * Método construtor - obrigado a passar o token
     */
    public function __construct($tokenVendedor=''){
        if (empty($tokenVendedor)) {
            throw new \Exception("Token não foi declarado", 1);        
        }
        $this->setToken($tokenVendedor, 'V');
    }
    
    /**
     * Fazer operações de caixa (abrir / fechar caixa, registrar informações, etc)
     * @param type $post
     *      id: string (255) (id do caixa)
     *      saida: boolean (0 = false / 1 = true)
     *      objeto: base64_encode(<json>) - qualquer informação relacionada que deva ser registrada
     */
    public function fazerAcaoCaixa ($post=[]) {
        if (!empty($post) && is_array($post)) {
            return json_decode($this->call('entsai/', 'POST', $post, true));
        } 
        return $this->getApiError('Não foi possível fazer ação de caixa. Faltam informações para efetuar o procedimento.');
    }
    
    /**
     * Ler operações de caixa
     * @param type $post
     * @return type
     */
    public function lerAcaoCaixa ($post=[]) {
        if (!empty($post) && is_array($post)) {
            return json_decode($this->call('entsai/' . $post['id'] . '/' . 
            (empty($post['dateStart']) && empty($post['dateFinish']) ? '' : $post['dateStart'] . 't00:00:00/' . $post['dateFinish'] . 't23:59:59'), 
            'GET'));
        } 
        return $this->getApiError('Não foi possível ler ação de caixa. Faltam informações para efetuar o procedimento.');
    }
    
    /**
     * Consultar saldo atual para o operador de caixa
     * @return type
     */
    public function saldoCaixa($post) 
    {
        if (!empty($post)) {
            return json_decode($this->call('entsai/' . $post['id'] . '/' . $post['data_ini'] . '/' . $post['data_fim'] . '/saldo', 'GET'));
        }
        return $this->getApiError('Não foi possível consultar o saldo. Faltam informações para efetuar o procedimento');
    }
    
}
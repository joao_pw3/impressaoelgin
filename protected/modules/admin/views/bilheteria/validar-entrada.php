<?php 
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\assets\VendaAsset;
use yii\helpers\Url;

VendaAsset::register($this);
?>

<div class="ajustaRodape">
	<h1><span style="font-weight:200;">Ingressos</span> - Consulta ingresso avulso</h1>
	<input type="hidden" id="url-filipeta" value="<?= Url::to(['venda/filipeta']); ?>">
	<?php if(Yii::$app->session->hasFlash('sucesso')) {?>
		<p class="alert alert-success"><?=Yii::$app->session->getFlash('sucesso')?></p>
	<?php }?>
	<?php $formA= ActiveForm::begin(['id'=>'consultar-qrcode-form', 'class'=>'col-sm-12']) ?>
	<div class="row">
		<div class="col-sm-12 semPad">
			<?= $formA->field($model, 'idEvento')->dropDownList(ArrayHelper::map($eventos, 'id', 'data', 'evento'))->label('Selecione o evento') ?>
		</div>
		<div class="col-sm-6" style="padding-left:0px;">
			<?= $formA->field($model, 'qrcode')->label('Leia o QR Code') ?>
		</div>
		<div class="col-sm-6" style="padding-right:0px;">
			<?= $formA->field($model, 'documento')->label('Digite o documento') ?>
		</div>
	</div>
	<div class="form-group" style="text-align:center;">
		<?= Html::submitButton('Consultar', ['class' => 'btn btn-primary btn-pagar']) ?>
	</div>
	<?php ActiveForm::end() ?>


	<?php if(is_array($model->lista) && count($model->lista)>0) {
		//coloca em ordem reversa (recente p/ antigo)
		$lista=array_reverse($model->lista)?>
		<hr>
		<div class="col-sm-12"><h3 class="aliCentro">Revise as informações abaixo para validar a entrada</h3></div><br clear="all"><br>
		<?php foreach($lista as $m){?>
			<div class="col-sm-4">
				<p><strong>Evento:</strong> <?=$m->objIngresso->agenda->nome?> - <?=$m->objIngresso->agenda->data?></p>
				<p><strong>Comprador:</strong> <?=$m->objIngresso->comprador?> - <?=$m->objIngresso->comprador_documento?></p>
				
			</div>	
			<div class="col-sm-5">
				<h4><?=$m->objIngresso->produto?></h4>
				<p><strong>Ocupante deste ingresso:</strong> <?=$m->objIngresso->ocupante_nome?> - <?=$m->objIngresso->documento?></p>				
				
			</div>
			<div class="col-sm-3">
				<div class="col-sm-6 aliCentro">
					<?php  if($m->objIngresso->utilizacoes==0){?>					
						<?php $formB= ActiveForm::begin(['id'=>'validar-entrada-form', 'action'=>'validar-ingresso', 'class'=>'col-sm-12']) ?>
						<?= Html::hiddenInput('id_agenda', $m->objIngresso->agenda->id) ?>
						<?= Html::hiddenInput('qrcode', $m->objIngresso->qrcode) ?>
						<?= Html::submitButton('VALIDAR', ['class' => 'botCircleIng font-awesome btn btn-primary', 'title'=>'Validar']) ?>
						<?php ActiveForm::end() ?>
					<?php }?>
				</div>
				<div class="col-sm-6 aliCentro">
					<?php if($m->objIngresso->utilizacoes==0){?>	
						<?= Html::button('Imprimir filipeta',['class'=>'botCircleIng2 font-awesome imprimir-filipeta btn btn-default','type'=>'button', 'data-compra'=>$m->objIngresso->compra->codigo]);?>
					<?php }?>
				</div>
			</div>
			<div class="col-sm-12">
				<?php if($m->objIngresso->utilizacoes>0){
					$dataObj=\DateTime::createFromFormat('Y-m-d H:i:s',$m->objIngresso->utilizacoes_ultima_data);?>
					<p class="alert alert-info">Este ingresso já foi utilizado em <?=$dataObj->format('d/m/Y H:i:s')?></p>
				<?php } ?>
			</div>
			<p class="clearfix"></p>
				<?php 
				if (count($m->objCompra->carrinho->produtos)>1) {
					$outrosProdutos=[];
					foreach ($m->objCompra->carrinho->produtos as $produto) {
						if($produto->codigo == $m->objIngresso->produto_codigo) continue;
						$outrosProdutos[]=$produto->nome;
							
					};
					if(count($outrosProdutos)>0){?>
						<div class="col-sm-12">
							<h5>Outros produtos desta compra:</h5>
							<p><?=implode('<br />',$outrosProdutos)?></p>
						</div>	
					<?php } 
				} ?>
				<p class="clearfix"></p>
				<hr />
		<?php }?>
	<?php }?>
</div>
<?php
namespace app\models;
use Yii;
use app\models\Api;

/**
 * Classe para Clientes - Easy for Pay - Mineirão
 */
class ApiCliente extends Api
{
    public $logs;
    
    /**
     * Autenticar cliente
     * @param type $post Dados do cliente: user_id, [senha], [face_token], [expira (tempo em horas)]
     * @param boolean $apenasChecagem Checar se usuário existe (não registrar informações em sessão)
     */
    public function autenticarCliente($post=[], $apenasChecagem=false)
    {
        $this->setLog($apenasChecagem ? 'Checar usuário' : 'Login no Site');
        if (!empty($post) && is_array($post)) {
            $this->logs->log($apenasChecagem ? 'Checar' : 'Login', $post);
            $returnApi = json_decode($this->call('auth/', 'POST', $post, true, 'A'));
            $this->logs->log($returnApi->successo === '0' ? ($apenasChecagem ? 'Falha ao checar usuário' : 'Falha ao efetuar login') : ($apenasChecagem ? 'Checagem efetuada com sucesso' : 'Login efetuado com sucesso'), $returnApi);
            if (!$apenasChecagem) {
                $this->setSessaoCliente($post, $returnApi);
            }
            return $returnApi;
        }
        $this->logs->log($apenasChecagem ? 'Checagem inválida. Formulário vazio.' : 'Login inválido. Formulário vazio.');
        return $this->getApiError($apenasChecagem ? 'Não foi possível checar cliente.' : 'Não foi possível validar o cliente.');
    } 

    /**
     * Registrar informações do cliente em sessão
     * @param array $post Dados do cliente
     * @param object $api Retorno do objeto de autenticação de cliente
     */
    private function setSessaoCliente($post=[], $api=[])
    {
        if (is_array($post) && !empty($post) && isset($api->successo) && $api->successo === '1') {
            $session = Yii::$app->session;
            $session->open();
            $session['user_id'] = $post['user_id'];
            $session['user_token'] = $api->objeto->user_token;
            $this->setSessaoClienteDados();
            $this->setSessaoClienteCartao();
            $this->setSessaoLimiteCompra();
            switch($session['user_id']){
                case 'admin@easyforpay.com.br':
                case 'joao@easyforpay.com.br':
                    $session['nivelAcesso']='super-admin';
                break;
                case 'op_bilheteria_exemplo@estadiomineirao.com.br':
                    $session['nivelAcesso']='operadorBilheteria';
                break;
            }
            $session->close();
        }
    }
    
    /**
     * Gravar dados do cliente em sessão: nome e documento
     */
    private function setSessaoClienteDados () 
    {
        $returnApi = $this->getDadosComprador();
        if (isset($returnApi->objeto->comprador) && !empty($returnApi->objeto->comprador) && !empty($returnApi->objeto->comprador->nome) && !empty($returnApi->objeto->comprador->documento)) {
            $session = Yii::$app->session;
            $session->open();
            $session['nome'] = $returnApi->objeto->comprador->nome;
            $session['documento'] = $returnApi->objeto->comprador->documento;
            $session->close();
        }
    }
    
    /**
     * Ler dados do cartão favorito do cliente e registrá-los na sessão
     */
    public function setSessaoClienteCartao()
    {
        $returnApi = (new ApiCartao)->consultarFavorito();
        if (!empty($returnApi->codigo_cartao)) {
            $session = Yii::$app->session;
            $session->open();
            $session['codigo_cartao'] = $returnApi->codigo_cartao;
            $session->close();
        }
    }
    
    /**
     * Identificar limite de compras para o CPF do cliente, por evento
     */
    public function setSessaoLimiteCompra() 
    {
        $this->setLog('Validacao de limites de compra');
        $returnApi = (new ApiAgenda())->buscarEvento(['ativo' => '1']);
        if ($returnApi->successo == '1' && isset($returnApi->objeto) && !empty($returnApi->objeto)) {
            $session = Yii::$app->session;
            $session->open();
            $limites = [];
            foreach($returnApi->objeto as $evento) {

                if(sizeof($evento->tags)>0) {
                    foreach($evento->tags as $tag) {
                        if ($tag->nome == 'apelido') {
                            $eventoApelido = $tag->valor;
                            break;
                        }
                    }
                }
                $totalEvento = (new ApiCarrinho)->checarComprasDocumento($session['documento'], $eventoApelido);
                $limites[$eventoApelido] = $totalEvento;
            }
            $session['limiteCompra'] = $limites;
            $this->logs->log('Limites de compra.', $limites);
            return;
        }
        $this->logs->log('Nao ha eventos ativos para validar limite de compra.');
    }
    
    /**
     * Consultar dados do cliente
     */
    public function getDadosCliente()
    {
        $this->setLog('Consulta dados do cliente');
        $session = Yii::$app->session;
        $session->open();
        if (isset($session['user_token']) && !empty($session['user_token'])) {
            $this->logs->log('Consultar dados do cliente', $session['user_token']);
            $returnApi = json_decode($this->call('auth/' . $session['user_token'], 'GET', NULL, true, 'A'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar dados do cliente' : 'Consulta aos dados do cliente feita com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Consulta a dados do cliente inválida. Token inexistente.');
        return $this->getApiError('Não foi possível consultar os dados do cliente.');
    }
    
    /**
     * Consultar dados do cliente (comprador)
     */
    public function getDadosComprador()
    {
        $this->setLog('Consulta dados do comprador');
        $session = Yii::$app->session;
        $session->open();
        if (isset($session['user_token']) && !empty($session['user_token'])) {
            $this->logs->log('Consultar dados do comprador', $session['user_token']);
            $returnApi = json_decode($this->call('cadastro/' . $session['user_token'], 'GET', NULL, false, 'A'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar dados do comprador' : 'Consulta aos dados do comprador feita com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Consulta a dados do comprador inválida. Token inexistente.');
        return $this->getApiError('Não foi possível consultar os dados do comprador.');
    }
    
    /**
     * Criar conta
     * @param type $post Dados do cliente: user_id, [senha], [face_token]
     */
    public function criarConta($post=[]) 
    {
        $this->setLog('Criar nova conta');
        if (!empty($post) && is_array($post)) {
            $this->logs->log('Nova conta', $post);
            $returnApi = json_decode($this->call('conta/', 'POST', $post, true, 'A'));
            $this->logs->log($returnApi->successo === '0' && $returnApi->erro->codigo === '056' ? 'Nova conta: cliente já existe' : ($returnApi->successo === '0' ? 'Falha ao criar nova conta' : 'Conta criada com sucesso'), $returnApi);
            return $returnApi;
        }
        $this->logs->log('Criação de nova conta inválida. Formulário vazio.');
        return $this->getApiError('Não foi possível cadastrar uma nova conta.');
    }
    
    /**
     * Atualizar conta de cliente
     * @param $post Dados do cliente
     */
    public function atualizarConta($post=[])
    {
        $this->setLog('Atualizacao de conta');
        $session = Yii::$app->session;
        $session->open();
        if (is_array($post) && isset($session['user_token']) && !empty($session['user_token']) && isset($post['user_id']) && !empty($post['user_id'])) {
            $this->logs->log('Atualizar conta', $post);
            $returnApi = json_decode($this->call('conta/' . $session['user_token'], 'PUT', $post, true, 'A'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao atualizar conta' : 'Conta atualizada com sucesso', $returnApi);
            if ($returnApi->successo === '1') {
                $this->setSessaoClienteDados([
                    'nome' => $post['nome'], 
                    'documento' => $post['documento'],
                ]);
            }
            return $returnApi;
        }
        $this->logs->log('Atualização de conta inválida. Formulário vazio ou usuário deslogado.');
        return $this->getApiError('Não foi possível atualizar a conta.');
    }
    
    /**
     * Criar cadastro completo de cliente
     * @param array $post Array com informações do cliente: [documento], [nome], [apelido], [sexo], [data_nasc], [ddd], [celular], [cep], 
     *                                                      [endereco], [num], [compl], [cidade], [bairro], [foto]
     */
    public function criarContaCompleta($post=[])
    {
        $this->setLog('Criar conta completa');
        $session = Yii::$app->session;
        $session->open();
        
        if (!empty($post) && isset($session['user_token']) && !empty($session['user_token'])) {
            $this->logs->log('Registro de conta completa', $post);
            $returnApi = json_decode($this->call('cadastro/' . $session['user_token'], 'PUT', ['comprador' => $post], true, 'U'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao registrar conta completa' : 'Conta completa criada com sucesso', $returnApi);
            if ($returnApi->successo === '1') {
                $this->setSessaoClienteDados([
                    'nome' => $post['nome'], 
                    'documento' => $post['documento'],
                ]);
            }
            return $returnApi;
        }
        $this->logs->log('Criação de conta completa inválida. Formulário vazio.');
        return $this->getApiError('Não foi possível registrar a conta completa. Formulário vazio.');
    }

    /**
     * Consultar endereço pelo CEP
     * @param array $post 
     */
    public function consultarCep($post=[]) 
    {
        $this->setLog('Consulta de CEP');
        if (isset($post['cep']) && !empty($post['cep'])) {
            $this->logs->log('Consultar CEP', $post);
            $returnApi = json_decode($this->call('cep/' . $post['cep'], 'GET', NULL, false, 'A'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar CEP' : 'CEP consultado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Consulta de CEP inválida. Parâmetro não informado.');
        return $this->getApiError('Não foi possível consultar o CEP. Parâmetro não informado.');
    }
    
    /**
     * Consultar cidade
     * @param array $post Dados para consulta: [filtro]
     */
    public function consultarCidade($post=[])
    {
        $this->setLog('Consulta cidades');
        if (isset($post['filtro']) && !empty($post['filtro'])) {
            $this->logs->log('Consultar cidades', $post);
            $returnApi = json_decode($this->call('cidades/', 'POST', $post, true, 'A'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar cidades' : 'Cidades consultadas com sucesso', $returnApi);
            return $returnApi;
        }  
        $this->logs->log('Consulta de cidades inválida. Parâmetro não informado.');
        return $this->getApiError('Não foi possível consultar cidades. Parâmetro não informado.');
    }

    /**
     * Consultar compras de um usuário
     */
    public function getComprasUsuario(){
        $this->setLog('Consultar compras de um usuário');
        $this->logs->log('consultar a partir do token');
        $session = Yii::$app->session;
        $session->open();
        if(isset($session['user_token']) && $session['user_token'] != false){
            $compras=json_decode($this->call('compras/','GET',null,1,'U'));
            if($compras->successo){
                $this->logs->log('Token: '.$session['user_token']);
                return $compras;
            }
        }
        $this->logs->log('Erro: token de usuário nulo ou vazio');
        return (object)['successo' => '0', 'erro' => ['mensagem' => 'O token do usuário não pode ser vazio ou nulo']];
    }

    /**
     * Consulta os créditos na conta do usuário
     * @param $user_id o id do usuário - e-mail ou documento
     */
    public function getCreditosUsuario($user_id){
        $this->setLog('Consultar créditos de um usuário');
        $this->logs->log('User ID: '.$user_id);
        $creditos=json_decode($this->call('creditos/'.$user_id, 'GET'));
        if(isset($creditos->successo) && $creditos->successo == '1'){
            $this->logs->log('Total de créditos: '.$creditos->objeto[0]->creditos);
            return $creditos;
        }
    
        $this->logs->log('Erro: '.$creditos->erro->mensagem);
        return $creditos;
    }
    
    /**
     * Trazer o link do boleto para o painel do cliente, consultando um código nas compras do usuário. Não pode estar vencido
     */
    public function clienteLinkBoleto($codigo){
        $compras=$this->getComprasUsuario();
        if($compras->successo){
            foreach ($compras->objeto as $obj) {
                if($codigo==$obj->compras->codigo){
                    if($obj->compras->validade<date('d/m/Y'))
                        return '<i class="fa fa-times fa-2x plus-red" title="Boleto vencido"></i>';
                    if($obj->compras->codigo_status!=2 || $obj->compras->url_boleto=='')
                        return '<i class="fa fa-times fa-2x plus-red" title="Indisponível"></i>';
                    return '<a target="_blank" href="'.$obj->compras->url_boleto.'"><i class="fa fa-barcode fa-2x" title="Visualizar boleto"></i></a>';
                }
            }
        }

        return '';
    }

    public function getQrCodes(){
        return json_decode($this->call('qrcode/','GET',[],true,'U'));
    }
}
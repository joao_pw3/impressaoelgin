<table class="uppercase table table-responsive table-striped">
	<thead>
		<tr>
			<th>Data da venda</th>
			<th>Código da venda</th>
			<th>Bloco</th>
			<th>Fileira</th>
			<th>Cadeira</th>
			<th>Nome do ocupante</th>
			<th>Documento</th>
			<th>Nome do comprador</th>
			<th>Documento do comprador</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$bloco='';
		$fileira='';
		$assento='';
		foreach($produtos->objeto as $produto){
		$p=explode(' ', $produto->produto->nome);
		if(is_array($p)&&count($p)==4){
			$fileira=preg_replace('/\d/', '', $p[1]);
			$assento=preg_replace('/\D/', '', $p[1]);							
			$bloco=$p[3];
		}
		?>
		<tr>
			<td><?=$produto->cobranca->data_venda?></td>
			<td><?=$produto->cobranca->codigo?></td>
			<td><?=$bloco?></td>
			<td><?=$fileira?></td>
			<td><?=$assento?></td>
			<td><?=$produto->identificacao->nome?></td>
			<td><?=$produto->identificacao->documento?></td>
			<td><?=$produto->comprador->nome?></td>
			<td><?=$produto->comprador->documento?></td>
		</tr>
		<?php }?>
	</tbody>
</table>
<?php
$this->title='ADM Estádio Mineirão :: Vendas';
?>
<div>
	<h1>Total vendido</h1>
	<p>Mineirão Tribunas: <?=$somaQtdeTribunas?> vendidos<br />
		Total arrecadado: <?= number_format($somaValoresTribunas,2,',','.')?>
	</p>
	<p>Embaixada Mineirão: <?=$somaQtdeEmbaixadas?> vendidos<br />
		Total arrecadado: <?= number_format($somaValoresEmbaixadas,2,',','.')?>
	</p>
</div>
	
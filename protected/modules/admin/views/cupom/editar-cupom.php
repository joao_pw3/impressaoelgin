<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use app\assets\CupomAsset;
use kartik\number\NumberControl;

CupomAsset::register($this);
$this->title = 'Cupom - Editar';

$form = ActiveForm::begin([
    'id' => 'cupom-form'
]);

$idAgenda = explode('_', $model->id);
echo $form->field($model, 'id_agenda')->hiddenInput(['value' => $idAgenda[1] . '_' . $idAgenda[2]])->label(false);
echo $form->field($model, 'id')->hiddenInput(['value' => $model->id])->label(false);
?>
<div class="row">
    <h1><span style="font-weight:200;">Cupom</span> - Editar</h1>
    <div class="col-xs-6">
        <?= $form->field($model, 'nome')->textInput(['id' => 'nome']); ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
		<?= $form->field($model, 'descricao')->textInput(['id' => 'descricao']); ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-1" align="center"><input type="radio" name="desconto" value="desconto_fixo" class="checkbox-desconto"<?= $model->desconto_fixo > 0 ? ' checked' : ''; ?>></div>
    <div class="col-xs-5">
        <?= $form->field($model, 'desconto_fixo')->textInput()->widget(NumberControl::className(), [
            'maskedInputOptions' => [
                'prefix' => 'R$ ',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'digits' => 2,
                'allowMinus' => false,
                'autoUnmask'=>true,
                'unmaskAsNumber'=>true
            ],
            'displayOptions' => [
                'placeholder' => 'Desconto em R$',
                'class' => 'form-control kv-monospace',
                'id' => 'desconto_fixo'
            ]
        ]); ?>
    </div>
    <div class="col-xs-1" align="center"><input type="radio" name="desconto" value="desconto_percentual" class="checkbox-desconto"<?= $model->desconto_percentual > 0 ? ' checked' : ''; ?>></div>
    <div class="col-xs-5">
        <?= $form->field($model, 'desconto_percentual')->textInput()->widget(NumberControl::className(), [
            'disabled' => true,
            'maskedInputOptions' => [
                'suffix' => ' %',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'max' => 100,
                'digits' => 2,
                'allowMinus' => false,
                'autoUnmask'=>true,
                'unmaskAsNumber'=>true
            ],
            'displayOptions' => [
                'placeholder' => 'Desconto em %',
                'class' => 'form-control kv-monospace',
                'id' => 'desconto_percentual'
            ]
        ]); ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-6">
        <?= $form->field($model, 'validade_inicial')->widget(DateTimePicker::className(), [
            'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
            'readonly' => true,
            'pluginOptions' => [
                'todayBtn' => true,
                'autoclose' => true,
                'format' => 'dd/mm/yyyy hh:ii',
                'startDate' => date('d/m/Y H:i'),
            ],
            'options' => [
                'id' => 'validade_inicial'
            ]
        ]); ?>
    </div>
    <div class="col-xs-6">
        <?= $form->field($model, 'validade_final')->widget(DateTimePicker::className(), [
            'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
            'readonly' => true,
            'pluginOptions' => [
                'todayBtn' => true,
                'autoclose' => true,
                'format' => 'dd/mm/yyyy hh:ii',
                'startDate' => date('d/m/Y H:i'),
            ],
            'options' => [
                'id' => 'validade_final'
            ]
        ]); ?>
    </div>
</div>
<div class="row" align="center">
    <div class="col-xs-4">
        <label class="control-label">Tipo</label><br>
        <?php echo strpos($model->id, 'PUB') === false ? 'Privado' : 'Público'; ?>
        <?= $form->field($model, 'tipo')->hiddenInput(['value' => strpos($model->id, 'PUB') === false ? 'PRIV' : 'PUB'])->label(false); ?>
    </div>
    <div class="col-xs-4">
        <?= $form->field($model, 'cumulativo')->radioList(['0' => 'Não', '1' => 'Sim']); ?>
    </div>
    <div class="col-xs-4">
        <?= $form->field($model, 'ativo')->radioList(['0' => 'Não', '1' => 'Sim']); ?>
    </div>
</div>

<div class="form-group" align="right">
    <?= Html::submitButton('Atualizar', ['class' => 'btn btn-primary', 'id' => 'btn-cupom']) ?>
</div>
<?php ActiveForm::end(); ?>
<br><br><br>
<?php 
use yii\widgets\MaskedInput;
use yii\helpers\Html;
?>
<div class="tabCartao2"> 
	<div class="row">
		<div class="col-md-12 col-xs-12">		        
            <?= $form->field($cartao, 'nome_cartao')->textInput([
                'class' => 'form-control',
                'placeholder' => 'Nome conforme cartão',
                'title' => 'Nome conforme cartão',
                'id' => 'nome_cartao'
            ]); ?>
        </div>
    </div>
    
	<div class="row">
		<div class="col-md-6 col-xs-6">
			<div class="row">
				<div class="col-md-6 col-xs-6">
					<?= $form->field($cartao, 'numero_cartao')->textInput(['class' => 'form-control'])->widget(MaskedInput::class, [
						'mask' => '9999.9999.9999.9999',
						'options' => [
							'class' => 'form-control',
							'placeholder' => 'Número do cartão',
							'title' => 'Número do cartão',
							'id' => 'numero_cartao'
						]
					]); ?>
				</div>
				<div class="col-md-6 col-xs-6">
					<?= $form->field($cartao, 'cvv')->passwordInput()
					->widget(MaskedInput::class, [
						'mask' => '999[9]',
						'options' => [
							'class' => 'form-control password_disc',
							'placeholder' => 'CVV (3 ou 4 dígitos)',
							'title' => 'CVV (3 ou 4 dígitos)',
						]
					]); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-xs-6">
					<?= $form->field($cartao, 'mes_venc')->textInput()
					->widget(MaskedInput::className(), [
						'mask' => '99',
						'options' => [
							'class' => 'form-control',
							'id' => 'mes_venc',
							'placeholder' => 'Mês de validade (MM)',
							'title' => 'Mês de validade (MM)',
						]
					]); ?>
				</div>
				<div class="col-md-6 col-xs-6">
					<?= $form->field($cartao, 'ano_venc')->textInput()
					->widget(MaskedInput::className(), [
						'mask' => '9999',
						'options' => [
							'class' => 'form-control',
							'id' => 'ano_venc',
							'placeholder' => 'Ano de validade (AAAA)',
							'title' => 'Ano de validade (AAAA)',
						]
					]); ?>
				</div>
			</div>
		</div>
            <div class="col-md-6 col-xs-6">
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <?= Html::img('@web/images/cvv2.png', ['style' => 'max-width:200px; display:block; margin-top:20px;', 'width' => '100%']); ?>
                    </div>
					<div class="col-md-6 col-xs-6">
						<p>Utilizamos o código CVV para confirmação de suas compras. <br>Localize-o atrás do seu cartão, conforme a figura abaixo. Para algumas bandeiras a posição do código pode ser diferente e pode também estar na frente do cartão.</p>
					</div>
                </div>
            </div>
	</div>
	
    <?= $form->field($cartao, 'forma')->hiddenInput(['id' => 'forma'])->label(false); ?>
    
    <?= $form->field($cartao, 'parcela')->hiddenInput(['id' => 'parcela'])->label(false); ?>
    
    <div class="row">
        <div class="col-md-3">
            <span class="contato">
                <input type="button" value="<?= isset($clienteApi->objeto->user_id) ? 'Adicionar' : 'Prosseguir'; ?>" id="btn-cartao" data-loading-text="Processando..." class="icon-next carrinho">
            </span>
        </div>
        <?php if(Yii::$app->session->get('idCarrinho') != ''){ ?>
	        <div class="col-md-9" style="text-align:right;">
	            <span class="contato carrDest2">
		<input type="button" value="Ir para o pedido, cadastro meu cartão depois" id="btn-ir-para-carrinho" class="icon-next carrinho">
	            </span>
	        </div>
	    <?php } ?>
    </div> 
	
	<div class="row">
		<div class="col-md-12 col-xs-12">
			<hr class="rico2">
			<div class="alert alert-warning">
				Ao cadastrar seu cartão, será <u>debitado R$1,00 para validação</u>. O lançamento aparecerá como "EASYFORPAY*Mineirao" e será estornado em seguida. O procedimento será feito uma única vez. 
			</div>
		</div>
	</div>
    
</div>
<?php $this->registerJs('urlCarrinho="'.\yii\helpers\Url::to(['site/resumo']).'";') ?>
<?php
namespace app\models;

use Yii;
use yii\base\Model;
use \app\models\Cliente;
use \app\models\Ingressos;
use \app\models\WsIngresso;
use \app\models\Compras;
use \app\models\Conta;

class Bilheteria extends Model{
    public $qrcode;
    public $documento;
    public $localizar;
    public $comprador;
    public $idEvento;
    public $statusIngresso = [
        ''  => 'Escolha um status de ingresso',
        'R' => 'Reservado',
        'P' => 'Em produção',
        'N' => 'Enviado ao estádio', 
        'E' => 'Entregue ao estádio', 
        'A' => 'Associado ao mifare', 
        'L' => 'Liberado para bilheteria',
        'T' => 'Retirado pelo cliente',
        'I' => 'Inválido',
        'B' => 'Bloqueado',
        'S' => 'Segunda via solicitada',
        '2' => 'Segunda via recebida'
    ];
    public $status;
    public $lote;
    public $produto;
    public $objConsulta;
    public $objIngresso;
    public $objCompra;
    public $unidade;
    public $lista;

    /**
     * Validação 
     * qrcode deve ter um formato específico JSON/array
     */
    public function rules()
    {
        return [
            ['idEvento', 'required', 'on'=>'consultar-ingresso'],
            ['qrcode', 'either', 'params' => ['other' => 'documento']],
            ['documento', 'string', 'length' => ['min' => 5]],
            [['qrcode', 'documento', 'localizar', 'status', 'lote', 'produto', 'comprador'], 'safe'],
        ];
    }

    public function either($attr, $params)
    {
        $field1 = $this->getAttributeLabel($attr);
        $field2 = $this->getAttributeLabel($params['other']);
        if (empty($attr) || empty($this->{$params['other']})) {
            $this->addError($attr, "{$field1} ou {$field2} deve ser preenchido.");
            return false;
        }

        return true;
    }

    /**
     * Qual tipo de pesquisa do ingreso
     */
    public function procurarIngresso(){
        $session = Yii::$app->session;
        $session->open();
        $objApiCliente=new Cliente;
        $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));        
        $this->unidade=$objApiCliente;
        if($this->qrcode) return $this->procurarIngressoQrCode();
        if($this->documento) return $this->procurarIngressoDocumento();
    }

    /**
     * Procurar um ingresso com o qrcode na base.
     */
    public function procurarIngressoQrCode(){
        $ws=new WsIngresso($this->unidade->user_token);
        return $ws->consultar(['qrcode'=>$this->qrcode,'id_agenda_data'=>$this->idEvento]);
    }

    /**
     * Procurar documento nas compras e em ocupantes.
     */
    public function procurarIngressoDocumento(){
    	$vendas=[];
        $codVendasEncontradas=[];
        $documento = preg_replace('/(\W)/','', $this->documento);
        $ws = new WsIngresso($this->unidade->user_token);

        return $ws->consultar(['documento'=>$documento,'id_agenda_data'=>$this->idEvento]);
    }

    /**
     * Criar objeto Ingresso em $this->objIngresso a partir de um código criado
     */
    public function dadosIngressos(){
        $this->lista=[];
        if ($this->objConsulta) {
            foreach ($this->objConsulta as $objConsulta) {
                $objConta=new Conta;
                $objConta->nome=$objConsulta->comprador;
                $objConta->documento=$objConsulta->comprador_documento;
                if(!isset($objConsulta, $objConsulta->codigo_venda)) continue;
                $obj=new Bilheteria;
                $obj->objCompra = new Compras($objConta);
                $obj->objCompra->codigo=$objConsulta->codigo_venda;
                $obj->objCompra->umaCompra();
                $obj->objCompra->infoCarrinho();
                $obj->objIngresso=new Ingressos($obj->objCompra);
                $obj->objIngresso->loadFromApi($objConsulta);
                $this->lista[]=$obj;
            }
        }
    }

    /**
     * Marcar ingresso como utilizado após a leitura do qrcode
     * @param integer $agenda o código do evento
     * @param string $qrcode o código QR do ingresso
     * @param integer $data o código da data do evento
    */
    public function marcarIngressoUtilizado($agenda,$qrcode,$data){
        $session = Yii::$app->session;
        $session->open();
        $objApiCliente=new Cliente;
        $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
        $ws=new WsIngresso($objApiCliente->user_token);
        return $ws->utilizar([
            'id_agenda'=>$agenda,
            'qrcode'=>$qrcode,
            'data'=>$data
        ]);
    }
}
<?php
namespace app\modules\admin\assets;
use yii\web\AssetBundle;

class IngressosAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/assets';
    
    public $css = [
    ];
    
    public $js = [
        'js/ajax.js',
        'js/ingressos.js'
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    
    public $publishOptions = [
        'forceCopy' => true
    ];
}

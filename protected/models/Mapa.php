<?php
namespace app\models;

use Yii;
use yii\base\Model;
use SimpleXMLElement;
use app\models\Tags;

class Mapa extends Model 
{
    public $iconeEspecial = [
        'acompanhante' => '&#xf067;',
        'cadeirante' => '&#xf193;',
        'mobilidade' => '&#xf29d;',
        'obeso' => '&#xf234;'
    ];
    public $assentos;

    public function rules() 
    {
        return [
        ];
    }

    public function attributeLabels()
    {
        return[
        ];
    }

    /**
     * Dado um evento, retornar o mapa correspondente
     * @param integer $idDataEvento Id do evento
     * @todo CONSIDERAR CARRINHO DE COMPRAS PARA IDENTIFICAR PRODUTOS RESERVADOS PARA O CLIENTE
     */
    public function getMapa($idDataEvento)
    {
        if ((int)$idDataEvento > 0) {
            $tags = new Tags;
            $session = Yii::$app->session;
            $session->open();
            $objApiCliente = new Cliente;
            $token=$session->get('token_unidade',false);
            $pesquisaRede='0';
            if(!$token){
                $token=$objApiCliente->getTokenMatriz();
                $pesquisaRede='1';
            }
            $objApiCliente->buscarUnidadePor('user_token',$token);
            $agenda = new Agenda($objApiCliente);
            $agenda->buscarEventosPorFiltro(['id_data' => $idDataEvento,'rede'=>$pesquisaRede]);   
            if(isset($agenda->lista[0]->tags))
                $tagAgenda = $tags->objetoTags($agenda->lista[0]->tags);
            if (!isset($tagAgenda->mapa)) {
                return '<hr><p>Não há mapa definido para este evento. Certifique-se que este produto possui mapa.</p>';
            }
            $this->setAssentos($tagAgenda->assento);
            $filtroTags['id_agenda']=$idDataEvento;
            if($this->getAssentos()=="marcado") {
                $filtroTags['tags'] = [['nome' => 'tipo', 'valor' => 'assento']];
            }
            $produtos = new Produto($objApiCliente);
            $produtos->buscarProdutosFiltro($filtroTags);
            $root=\yii\helpers\Url::to(['/'],true);
            $arqMapa = file_get_contents($root . '/maps/' . $tagAgenda->mapa . '.html');
            $mapa = new SimpleXMLElement($arqMapa);
            $mapa->registerXPathNamespace('svg', 'http://www.w3.org/2000/svg');

        } else {
            die('Não há arquivo para o mapa solicitado.');
        }    
        if (!empty($produtos->lista)) {
            $arrStatus = ['fillGray' => 1, 'fillRed' => 2, 'fillOrange' => 3];
            foreach($produtos->lista as $produto) {
                $tagsTmp = $tags->objetoTags($produto->tags);
                $nomeTmp = explode(' ', $produto->nome);
                if($this->getAssentos()=="marcado") {
                    $id = $tagAgenda->mapa . strtoupper($nomeTmp[1]) . $nomeTmp[4];
                    $buscaXpath = '//svg:g[@id="' . $id . '"]';
                    $busca = $mapa->xpath($buscaXpath);
                    
                    if (!empty($busca)){
                        $corAssento = (((int)$produto->disponivel == 0 && (int)$produto->vendido > 0) || 
                                        (int)$produto->ativo == 0 ? 'fillRed' : 
                                       (((int)$produto->disponivel == 0 && (int)$produto->reservado > 0) || 
                                       $produto->reserva_exclusiva != '' ? 'fillOrange' : 'fillGray'));
                        
                        $strTitulo = $corAssento == 'fillRed' ? 'Produto indisponível' : 
                            ($corAssento == 'fillOrange' ? 'Produto reservado' : 'Produto disponível');
                        
                        $textoEspecial = !isset($tagsTmp->especial) || empty($tagsTmp->especial) ? '' : 
                            (isset($tagsTmp->especial) && $tagsTmp->especial == 'acompanhante' ? ' - ' . strtoupper($tagsTmp->fileira) . $tagsTmp->assento . ' (Acompanhante)' : 
                            (isset($tagsTmp->especial) && $tagsTmp->especial == 'cadeirante' ? ' - ' . strtoupper($tagsTmp->fileira) . $tagsTmp->assento . ' (Cadeirante)' : 
                            (isset($tagsTmp->especial) && $tagsTmp->especial == 'mobilidade' ? ' - ' . strtoupper($tagsTmp->fileira) . $tagsTmp->assento . ' (Mobilidade reduzida)' : 
                            ' - ' . $tagsTmp->fileira . $tagsTmp->assento . ' (Obeso)')));
                        
                        $textoAssento = !isset($tagsTmp->especial) || empty($tagsTmp->especial) ? $busca[0]->text : 
                            (isset($tagsTmp->especial) && $tagsTmp->especial == 'acompanhante' ? '&#xf067;' : 
                            (isset($tagsTmp->especial) && $tagsTmp->especial == 'cadeirante' ? '&#xf193;' : 
                            (isset($tagsTmp->especial) && $tagsTmp->especial == 'mobilidade' ? '&#xf29d;' : '&#xf234;')));
                        $classTexto = $textoAssento != $tagsTmp->assento ? ' font-awesome' : '';                    
                        
                        $busca[0]->a['id'] = $produto->codigo;
                        $busca[0]->a['href'] = 'javascript:;';
                        $busca[0]->a['class'] = 'assento'; 
                        $busca[0]->a['onclick'] = 'assento(' . $produto->codigo . ', ' . $arrStatus[$corAssento] . ', "' . $produto->reserva_exclusiva . '");'; 
                        $busca[0]->a['data-ativo'] = $produto->ativo;
                        $busca[0]->a['data-disponivel'] = $produto->disponivel;
                        $busca[0]->a['data-oferecido'] = $produto->oferecido;
                        $busca[0]->a['data-reservado'] = $produto->reservado;
                        $busca[0]->a['data-vendido'] = $produto->vendido;
                        $busca[0]->a['data-produto'] = strtoupper($tagsTmp->fileira) . '-' . $tagsTmp->assento;
                        $busca[0]->a['data-res-exclusiva'] = $produto->reserva_exclusiva;
                        $busca[0]->a->circle['cx'] = $busca[0]->circle['cx'];
                        $busca[0]->a->circle['cy'] = $busca[0]->circle['cy'];
                        $busca[0]->a->circle['r'] = $busca[0]->circle['r'];
                        $busca[0]->a->circle['class'] = str_replace('fillGray', $corAssento, $busca[0]->circle['class']) . ' circle-' . $produto->codigo . ' fileira' . strtoupper($tagsTmp->fileira) . ' todos';
                        $busca[0]->a->circle['data-codigo'] = $produto->codigo;
                        $busca[0]->a->title['id'] = 'title-' . $id;
                        $busca[0]->a->title = $strTitulo . $textoEspecial;
                        $busca[0]->a->text['x'] = (int)$busca[0]->text['x'];
                        $busca[0]->a->text['y'] = (int)$busca[0]->text['y'];
                        $busca[0]->a->text['class'] = 'fillLightGray fntTitle' . $classTexto;
                        $busca[0]->a->text = $textoAssento;
                        unset($busca[0]->circle);
                        unset($busca[0]->text); 
                    }
                }
            }
            return html_entity_decode($mapa->asXML(), ENT_NOQUOTES, 'UTF-8');
        }            
        return false;
    }

    /**
     * Retornar mapa para evento com assento livre
     * @param string $mapa Identificador do mapa 
     */
    public static function getMapaLivre($mapa='')
    {
        if (!empty($mapa) && is_file(Yii::getAlias('@webroot') . '/maps/' . $mapa . '.html')) {
            return file_get_contents(Yii::getAlias('@webroot') . '/maps/' . $mapa . '.html');
        }
        return false;
    }


    /**
     * @return mixed
     */
    public function getAssentos()
    {
        return $this->assentos;
    }

    /**
     * @param mixed $assentos
     *
     * @return self
     */
    public function setAssentos($assentos)
    {
        $this->assentos = $assentos;

        return $this;
    }
}
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\Modal;

Modal::begin([
    'header' => '<h4 align="center">Matriz</h4>',
    'size'   => 'modal-md',
    'id'     => 'modal-matriz',
    'options'=> [
        'class' => 'modal-center'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ]
]);

$tags->scenario = 'create';

$form = ActiveForm::begin([
    'id' => 'matriz-form', 
    'action' => Url::to(['produto/nova-matriz'])
]); ?>

<?php $model->ativo = '1'; ?>

<?= $form->field($model, 'id_agenda')->dropDownList(ArrayHelper::map($eventos, 'id', 'data', 'evento')); ?>
<?= $form->field($model, 'nome'); ?>
<?= $form->field($model, 'valor')->hiddenInput(['value' => '0'])->label(false); ?>
<?= $form->field($model, 'descricao')->textarea(['rows' =>5]); ?>
<?= $form->field($model, 'ativo')->radioList(['1' => 'Ativo', '0' => 'Inativo'])->label('Status'); ?>
<?= $form->field($tags, '[0]nome')->hiddenInput(['value' => 'tipo'])->label(false); ?>
<?= $form->field($tags, '[0]valor')->hiddenInput(['value' => 'matriz'])->label(false); ?>
<?= $form->field($tags, '[1]nome')->hiddenInput(['value' => 'matriz'])->label(false); ?>
<?= $form->field($tags, '[1]valor')->textInput(['id' => 'id-matriz'])->label('Identificador da matriz'); ?>

<?= Html::submitButton('Cadastrar', ['class' => 'btn btn-primary', 'id' => 'btn-nova-matriz']); ?>

<?php ActiveForm::end(); ?>

<?php Modal::end(); ?>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use kartik\date\DatePicker;
use app\assets\RelatorioCaixaAsset;
use app\assets\CapptaAsset;
use yii\bootstrap\Modal;

$this->title = 'Caixa - Relatório de vendas';
CapptaAsset::register($this);
RelatorioCaixaAsset::register($this);
?>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <?php $this->registerJs("capptaInit();", View::POS_READY); ?>
        </div>
    </div>
    <h1><span style="font-weight:200;">Caixa</span> - Relatório de vendas</h1>
    <?= Html::beginForm('','post',['class'=>'form-inline'])?>
    <div class="form-group">
        <label class="control-label">Data inicial:</label>
        <?= DatePicker::widget([
            'name' => 'de',
            'value' => date('d/m/Y', $de->getTimestamp()),
            'language' => 'pt-BR',
            'pluginOptions' => [
                'format' => 'dd/mm/yyyy'
            ]
        ]);?>
    </div>
    <div class="form-group">
        <label class="control-label">Data final:</label>
        <?= DatePicker::widget([
            'name' => 'ate',
            'value' => date('d/m/Y', $ate->getTimestamp()),
            'language' => 'pt-BR',
            'pluginOptions' => [
                'format' => 'dd/mm/yyyy'
            ]
        ]);?>
    </div>
    <div class="form-group">
        <button class="btn btn-default" type="submit">Buscar</button>
    </div>
    <?= Html::endForm()?>
    <?php 
    if (isset($relatorio) && count($relatorio) > 0) { ?>
    <hr>
    <input type="hidden" id="url-estorno" value="<?= Url::to(['estorno']); ?>">
    <input type="hidden" id="url-detalhes" value="<?= Url::to(['detalhes']); ?>">
    <div id="tabela-vendas-transacoes">
        <table id="table-caixa" class="table table-responsive table-hover">
            <thead>
                <tr>
                    <th>Tipo</th>
                    <th>Data</th>
                    <th>Código</th>
                    <th>Status</th>
                    <th>Forma pagto.</th>
                    <th>Valor</th>
                    <th>Acumulado</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <?php
            $total = 0;
            foreach ($relatorio as $data_tipo => $registro) {
                foreach ($registro as $index => $dados) {
                    if (stristr($data_tipo, '_venda')) {
                        $recibo='';
                        $capptaObjeto = json_decode(base64_decode($dados->vendas->movimento->objeto));
                        if ($dados->vendas->movimento->status == 'Estornado' && isset($capptaObjeto->conciliador)) {
                            $recibo = $capptaObjeto->conciliador->receipt->customerReceipt;
                        }
                        $total = $dados->vendas->status == 'Autorizado' ? $total + $dados->vendas->valor : $total - $dados->vendas->valor;
            ?>
                <tr class="" data-id="<?= $dados->vendas->codigo; ?>">
                    <td>Venda</td>
                    <td><?= $dados->vendas->data; ?></td>
                    <td><?= $dados->vendas->codigo; ?></td>
                    <td><?= $dados->vendas->status; ?></td>
                    <td><?= $dados->vendas->meio_de_pagamento->descricao; ?></td>
                    <td align="right">R$ <?= number_format($dados->vendas->valor, 2, ',', '.'); ?></td>
                    <td align="right">R$ <?= number_format($total, 2, ',', '.');?></td>
                    <td class="hidden"><?= base64_encode($recibo); ?></td>
                    <td><button class="btn btn-primary detalhes" data-conciliador="<?= $dados->vendas->id_conciliacao; ?>" data-detalhe="<?= $dados->id_carrinho; ?>" data-meio-pagto="<?= $dados->vendas->meio_de_pagamento->descricao;?>" title="Ver detalhes">detalhes</button></td>
                </tr>
            <?php 
                    } else {
                        $objeto = json_decode(base64_decode($dados->objeto)); 
                        $objeto->valor = empty($objeto->valor) ? 0 : $objeto->valor;
                        $total = ($dados->ajuste == 1 && $dados->operacao == 'E') || ($dados->ajuste == 0 && $dados->operacao == 'E') ? $total + (float)$objeto->valor : $total - (float)$objeto->valor;
            ?>
                <tr>
                    <td><?= $dados->ajuste == 1 ? ($dados->operacao == 'E' ? 'Reposição' : 'Sangria') : ($dados->operacao == 'E' ? 'Abertura' : 'Fechamento'); ?></td>
                    <td><?= $dados->data; ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="right">R$ <?= number_format((float)$objeto->valor, 2, ',', '.'); ?></td>
                    <td align="right">R$ <?= number_format((float)$total, 2, ',', '.'); ?></td>
                    <td></td>
                </tr>
            <?php
                        if ($dados->ajuste == 0 && $dados->operacao == 'S') {
            ?>
                <tr<?= $dados->ajuste == 0 && $dados->operacao == 'S' ? ' class="caixa-total"' : '';?>>
                    <td>Saldo/Quebra</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="right">R$ <?= number_format($total, 2, ',', '.'); ?></td
                    <td></td>
                </tr>
            <?php
                            $total =  0;
                        }
                    }
                }
            } ?>
            </tbody>
        </table>
    </div>
    <button type="button" class="btn btn-primary" id="btn-print-detalhes">Imprimir detalhes</button>
    <a href="<?= Url::to(['exportar-csv-vendas', 'data_ini' => date('d/m/Y', $de->getTimestamp()), 'data_fim' => date('d/m/Y', $ate->getTimestamp())]); ?>" class="btn btn-primary">CSV detalhes</a>
    <p></p>
    <p>
        * Compras por cartão: estorno é válido apenas para compras efetuadas no mesmo dia.<br>
        Compras em espécie: só podem ser canceladas no prazo de até 07 (sete) dias corridos a partir da data de compra.
    </p> 
    <?php } else { ?>
    <div class="row">
        <p>Não há vendas deste operador para o período selecionado.</p>
    </div>
    <?php } ?>
</div>

<?php Modal::begin([
    'header' => '<h4 align="center">Estorno</h4>',
    'size' => 'modal-md',
    'id' => 'modal-estorno',
    'options' => [
        'class' => 'modal-center'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ],
    'footer' => '
       <div class="col-md-12" align="center">
           <button type="button" class="btn btn-danger" id="btn-cancel">Cancelar</button>
           <button type="button" class="btn btn-success" id="btn-confirmar">Confirmar</button>
       </div>'
]); ?>
<form id="form-estorno">
    <input type="hidden" id="codigo" name="codigo">
    <input type="hidden" id="unidade" name="unidade">
    <div class="row">
        <div class="col-xs-3"><label class="control-label">Código</label></div>
        <div class="col-xs-9" id="div-codigo"></div>
    </div>
    <div class="row">
        <div class="col-xs-3"><label class="control-label">Motivo</label></div>
        <div class="col-xs-9"><textarea name="motivo" id="motivo" cols="40" rows="5"></textarea></div>
    </div>
</form>
<?php Modal::end(); ?>

<?php Modal::begin([
    'header' => '<h4 align="center">Consulta</h4>',
    'size'   => 'modal-md',
    'id'     => 'modal-venda',
    'options'=> [
        'class' => 'modal-center'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ]
]);
Modal::end(); ?>
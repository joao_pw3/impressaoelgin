<?php
namespace app\models;
use app\models\Api;
use yii\base\Exception;

/**
 * Classe para gerenciamento de operadores - Easy for Pay - Mineirão
 */
class ApiOperadores extends Api
{
    public $logs;
    
    /**
     * Incluir operador
     * @return object {'successo': '1'}
     */
    public function cadastrarOperador($post)
    {
        $returnApi = json_decode($this->call('operador/', 'POST', $post, true, 'V'));
        $this->setLog('Cadastro de operador');
        if($returnApi->successo==0)
            $this->logs->log('Falha ao cadastrar o operador ('.$post['email'].').');
        else
            $this->logs->log('Operador cadastrado com sucesso ('.$post['email'].').');
        return $returnApi; 
    }
    
    /**
     * Lista de operadores
     * @return object {'successo': '1', 'objeto': '...'}
     */
    public function listaOperadores()
    {
        $returnApi = json_decode($this->call('operador/', 'GET', null, true, 'V'));
        if($returnApi->successo==0){
            $this->setLog('Falha ao buscar operadores');
        }        
        return $returnApi;        
    }
    
    /**
     * Atualizar um registro de operador
     * @return object {'successo': '1', 'objeto': '...'}
     */
    public function alterarOperador($post){
        $returnApi = json_decode($this->call('operador/'.$post['email'], 'PUT', $post, true, 'V'));
        if($returnApi->successo==0){
            $this->setLog('Falha ao alterar o operador '.$post['email']);
        }        
        return $returnApi;  
    }
    
    /**
     * Excluir um registro de operador
     * @return object {'successo': '1', 'objeto': '...'}
     */
    public function excluirOperador($email){
        $returnApi = json_decode($this->call('operador/'.$email, 'DELETE', null, true, 'V'));
        if($returnApi->successo==0){
            $this->setLog('Falha ao excluir o operador '.$email);
        }        
        return $returnApi;  
    }
}
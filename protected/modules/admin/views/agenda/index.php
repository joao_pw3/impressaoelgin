<?php
use \yii\helpers\Url;
use \app\models\Tags;

$this->title = 'ADM Easy for Pay' . (isset($matriz) && $matriz == true ? ' (matriz)' : '');
?>
<div class="container">
	<div class="row">
		<h2 class="tituloGeral"><i class="fa fa-clone"></i> Agenda - Eventos Cadastrados <?= isset($matriz) && $matriz == true ? ' (matriz)' : ''; ?></h2>
	</div>
        <div class="gridAgendaOut">
            <div class="gridAgenda gridAgendaHead">
                <div>Nome</div>
                <div>Descrição</div>
                <div>Período</div>
                <div>Status</div>
                <div>Tags</div>
                <div>Edição</div>
            </div>
            <?php
            if (!$agenda->hasErrors() && count($agenda->lista) > 0) {
                foreach ($agenda->lista as $evento) {
                    $tagsObject = Tags::objetoTags($evento->tags);
                    ?>
                    <div class="gridAgenda">
                        <div><a href="<?= Url::to(['registro-evento', 'evento' => $evento->codigo]) ?>" class="gridAgendaLink"><?= $evento->nome ?></a></div>
                        <div><p><?= $evento->descricao ?></p></div>
                        <div>
                            <p>
                                <?php 
                                $_dataIni=\DateTime::createFromFormat('d/m/Y H:i:s', $evento->dataInicial);
                                if($_dataIni!==false) echo $_dataIni->format('d/m/Y H:i') ?> - 
                                <?php
                                $_dataFim=\DateTime::createFromFormat('d/m/Y H:i:s', $evento->dataFinal);
                                if($_dataFim!==false) echo $_dataFim->format('d/m/Y H:i') ?> 
                            </p>
                        </div>
                        <div><p><?= $evento->ativo ? 'Ativo' : 'Inativo' ?></p></div>
                        <div><p>Tags:<?= isset($evento->tags) ? Tags::stringTags($evento->tags) : 'nenhuma' ?></p></div>
                        <div>
                            <a href="<?= Url::to(['adicionar-tag', 'evento' => $evento->codigo]) ?>" class="fontA botEsq" title="Adicionar Tag">&#xf02b;</a>
                            <a href="<?= Url::to(['adicionar-anexo', 'evento' => $evento->codigo]) ?>" class="fontA botMeio" title="Adicionar Anexo">&#xf055;</a>	
                            <a href="<?= Url::to(['registro-evento', 'evento' => $evento->codigo]) ?>" class="fontA botDir" title="Detalhes do Evento">&#xf044;</a>
                        </div>
                    </div>
        <?php
    }
}
?>
        </div>
</div>
<br>
<br>
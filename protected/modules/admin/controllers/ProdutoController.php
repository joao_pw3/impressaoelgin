<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\controllers\AbstractController;
use app\models\Produto;
use app\models\WsProduto;
use app\models\Tags;
use app\models\Anexos;
use yii\web\UploadedFile;
use yii\web\Response;
use app\models\Agenda;
use app\models\Cliente;
use app\models\Mapa;
use app\models\Carrinho;

/**
 * Controle de Produto
 */
class ProdutoController extends AbstractController
{
    
    /**
     * Renders the index view for the module
     * @param boolean $matriz Boolean indicando se é uma ação para matriz ou não
     * @return string
     */
    public function actionIndex($matriz=false)
    {
        $session = $this->_sessionOpen();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));

        $model = new Produto($objApiCliente);        
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post('Produto');
            $matriz = (boolean)$post['matriz'];
            if (isset($post['id_agenda']) && $post['id_agenda'] > 0) {
                $model->buscarProdutosFiltro(['id_agenda' => $post['id_agenda']], false);
            }
        }
        
        $agenda = new Agenda($objApiCliente);
        (boolean)($matriz) ? $agenda->buscarEventosPorFiltro(['nome' => 'MATRIZ%']) : $agenda->listaTodosEventos();
        $datasEventos = $agenda->listaDatasEventos($matriz == true ? '' : 'MATRIZ');
        return $this->render('index', [
            'produto' => $model,
            'eventos' => $datasEventos,
            'matriz' => $matriz
        ]); 
    }
    
    /**
     * Cadastro de novo produto
     */
    public function actionAssentos() 
    {
        $session = $this->_sessionOpen();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
        
        $agenda = new Agenda($objApiCliente);
        $agenda->buscarEventosPorFiltro(['nome' => 'MATRIZ%']);
        $eventos = $agenda->getAgendaData();
        $datasEventos = $agenda->listaDatasEventos();
        
        $model = new Produto($objApiCliente);
        $model->buscarProdutosFiltro(['id_agenda' => implode(',', $eventos),
            'tags' => [
                ['nome' => 'tipo',  'valor' => 'matriz']
            ]
        ]);
        return $this->render('assentos', [
            'matrizes' => $model->lista,
            'model' => $model,
            'eventos' => $datasEventos,
            'tags' => new Tags
        ]);
    }
    
    /**
     * Cadastrar uma nova matriz e suas tags
     * @return type
     */
    public function actionNovaMatriz () 
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post('Produto'))) {
            $session = $this->_sessionOpen();
            $objApiCliente = new Cliente;
            $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
            $produto = new Produto($objApiCliente);            
            if ($produto->checkTag(Yii::$app->request->post('Tags'), 'matriz')) {
                return (object)['successo' => '0', 'erro' => ['mensagem' => 'Já existe uma matriz com o identificador "' . Yii::$app->request->post('Tags')[1]['valor'] . '"']];
            }
            if ($produto->load(Yii::$app->request->post())) {
                $returnApi = $produto->criarProduto();
                if ($returnApi->successo == '1') {
                    return $produto->salvarTag(Yii::$app->request->post('Tags'), $returnApi->objeto->codigo);
                }
                return $returnApi;
            }
            return (object)['successo' => '0', 'erro' => ['mensagem' => 'Não foi possível cadastrar a matriz']];
        }
        return (object)['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros para cadastrar nova matriz.']];
    }
    
    /**
     * Dado um identificador de matriz, buscar os seus setores
     * @return Produto
     */
    public function actionSetor() 
    {
        if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $post = Yii::$app->request->post();
            $session = $this->_sessionOpen();
            $objApiCliente=new Cliente;
            $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
            $model = new Produto($objApiCliente); 
            $model->buscarProdutosFiltro(['id_agenda' => $post['data_agenda'],
                'tags' => [
                    ['nome' => 'matriz',  'valor' => $post['matriz']],
                    ['nome' => 'tipo',  'valor' => 'setor']
                ]
            ]);
            return (object)['successo' => '1', 'setores' => $model];
        }
        return (new WsProduto)->getApiError('Faltam parâmetros para buscar os setores.');
    }
    
    /**
     * Cadastrar um novo setor e suas tags
     * @return type
     */
    public function actionNovoSetor () 
    {
        if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post('Produto'))) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $session = $this->_sessionOpen();
            $objApiCliente=new Cliente;
            $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
            $produto = new Produto($objApiCliente); 
            if ($produto->checkTag(Yii::$app->request->post('Tags'))) {
                return (new WsProduto)->getApiError('Já existe um setor identificado como "' . Yii::$app->request->post('Tags')[2]['valor'] . '"');
            }
            if ($produto->load(Yii::$app->request->post())) {
                $returnApi = $produto->criarProduto();
                if ($returnApi->successo == '1') {
                    return $produto->salvarTag(Yii::$app->request->post('Tags'), $returnApi->objeto->codigo);
                }
                return $returnApi;
            }
            return (new WsProduto)->getApiError('Não foi possível cadastrar o setor');
        }
        return (new WsProduto)->getApiError('Faltam parâmetros para cadastrar novo setor.');
    }
    
    /**
     * Dado um identificador de matriz e um de setor, buscar as suas fileiras
     * @return Produto
     */
    public function actionFileira() 
    {
        if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $post = Yii::$app->request->post();
            $session = $this->_sessionOpen();
            $objApiCliente=new Cliente;
            $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
            $model = new Produto($objApiCliente); 
            $model->buscarProdutosFiltro(['id_agenda' => $post['data_agenda'],
                'tags' => [
                    ['nome' => 'matriz',  'valor' => $post['matriz']],
                    ['nome' => 'setor',  'valor' => $post['setor']],
                    ['nome' => 'tipo', 'valor' => 'fileira']
                ]
            ]);
            return (object)['successo' => '1', 'fileiras' => $model];
        }
        return (new WsProduto)->getApiError('Faltam parâmetros para buscar as fileiras.');
    }
    
    /**
     * Cadastrar um nova fileira e suas tags
     * @return type
     */
    public function actionNovaFileira() 
    {
        if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post('Produto'))) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $session = $this->_sessionOpen();
            $objApiCliente=new Cliente;
            $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
            return (new Produto($objApiCliente))->criarFileira(Yii::$app->request->post());
        }
        return (new WsProduto)->getApiError('Faltam parâmetros para cadastrar novo setor.');
    }
    
    /**
     * Dado um identificador de matriz, um setor e uma fileira, carregar seus assentos
     */
    public function actionAssento()
    {
        if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $post = Yii::$app->request->post();
            $session = $this->_sessionOpen();
            $objApiCliente=new Cliente;
            $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
            $produto = new Produto($objApiCliente); 
            $produto->buscarProdutosFiltro(['id_agenda' => $post['data_agenda'],
                'tags' => [
                    ['nome' => 'matriz',  'valor' => $post['matriz']],
                    ['nome' => 'setor',  'valor' => $post['setor']],
                    ['nome' => 'fileira',  'valor' => $post['fileira']],
                    ['nome' => 'tipo', 'valor' => 'assento']
                ]
            ]);
            return (object)['successo' => '1', 'assentos' => $produto];
        }
        return (new WsProduto)->getApiError('Faltam parâmetros para buscar os assentos.');
    }
    
    /**
     * Dado um identificador de matriz, setor e fileira, registrar seus assentos
     */
    public function actionNovoAssento () 
    {
        if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post('Produto'))) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $session = $this->_sessionOpen();
            $objApiCliente=new Cliente;
            $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
            return (new Produto($objApiCliente))->criarAssento(Yii::$app->request->post());
        }
        return (new WsProduto)->getApiError('Faltam parâmetros para cadastrar novo assento.');
    }
    
    /**
     * Editar uma matriz de assentos, definindo assentos especiais, preço, estoque, status, reservas
     * @return type
     */
    public function actionEditarAssentos ($matriz=false) 
    {   
        $session = $this->_sessionOpen();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
        $agenda = new Agenda($objApiCliente);
        (boolean)($matriz) ? $agenda->buscarEventosPorFiltro(['nome' => 'MATRIZ%']) : $agenda->listaTodosEventos();
        $datasEventos = $agenda->listaDatasEventos($matriz == true ? '' : 'MATRIZ');
        $model = new Produto($objApiCliente);
        $mapa = '';
        if (Yii::$app->request->post()) {
            $agendaData = explode('_', Yii::$app->request->post('Produto')['id_agenda']);
            $mapa = (new Mapa())->getMapa($agendaData[1]);
        }
        return $this->render('editar-assentos', [
            'model' => $model,
            'mapa' => $mapa,
            'eventos' => $datasEventos,
            'id_agenda' => isset(Yii::$app->request->post('Produto')['id_agenda']) ? Yii::$app->request->post('Produto')['id_agenda'] : ''
        ]);
    }
    
    /**
     * Registrar tags para assentos especiais: acompanhante, cadeirante, mobilidade reduzida e obeso
     */
    public function actionAssentoEspecial () 
    {
        if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $session = $this->_sessionOpen();
            $objApiCliente=new Cliente;
            $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
            return (new Produto($objApiCliente))->assentoEspecial(Yii::$app->request->post());
        }
        return (new WsProduto)->getApiError('Faltam parâmetros para cadastrar novo assento.');
    }
    
    /**
     * Alterar produtos em lote
     */
    public function actionAlterarLote () 
    {
        if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $session = $this->_sessionOpen();
            $objApiCliente=new Cliente;
            $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
            $post = Yii::$app->request->post();
            $qtdCodigos = sizeof($post['codigos']);
            $loop=0;
            $margem=100;
            while ($qtdCodigos>0) {
                $inicio=$loop*$margem;
                if($qtdCodigos<$margem){
                    $limite=$qtdCodigos;
                }                                
                $lote[$loop]['codigos'] = array_slice($post['codigos'],$inicio,$margem);   
                if (isset($post['alterar_valor'])) {
                    $lote[$loop]['alterar_valor'] = $post['alterar_valor'];
                } else if ($post['alterar_status'] != null) {
                    $lote[$loop]['alterar_status'] = (int)$post['alterar_status'];
                    $lote[$loop]['justificativa'] = $post['justificativa'];
                }
                $loteEnviado[$loop] = (new Produto($objApiCliente))->alterarEmLote($lote[$loop]);
                $qtdCodigos=$qtdCodigos-$margem;
                $loop++;
            }
            foreach ($loteEnviado as $key=>$item) {
                if($item->successo=='1')
                    $success=true;
                
                if($item->successo=='0')
                    $error=true;    
            }

            if(isset($error) && $error)
                return (object) ['successo'=> '0'];
            elseif(isset($success) && $success) {
                return (object) ['successo'=> '1'];
            }
        }
        return (new WsProduto)->getApiError('Faltam parâmetros para alterar produtos em lote.');
    }
    
    /**
     * Alterar estoque (oferecido) de um produto
     */
    public function actionAlterarEstoqueLote () 
    {
        $session = $this->_sessionOpen();
        $objApiCliente=new Cliente;
        $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
        $model = new Produto($objApiCliente);
        if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $post = Yii::$app->request->post();
            $qtdCodigos = sizeof($post['codigos']);
            $loop=0;
            $margem=100;
            while ($qtdCodigos>0) {
                $inicio=$loop*$margem;
                if($qtdCodigos<$margem){
                    $limite=$qtdCodigos;
                }                                
                $lote[$loop]['codigos'] = array_slice($post['codigos'],$inicio,$margem);   
                $lote[$loop]['alterar_estoque'] = $post['alterar_estoque'];
                $lote[$loop]['acao'] = $post['acao'];
                $loteEnviado[$loop] = $model->alterarEstoqueLote($lote[$loop]);
                $qtdCodigos=$qtdCodigos-$margem;
                $loop++;
            }
            foreach ($loteEnviado as $key=>$item) {
                if($item->successo=='1')
                    $success=true;
                
                if($item->successo=='0')
                    $error=true;    
            }

            if(isset($error) && $error)
                return (object) ['successo'=> '0'];
            elseif(isset($success) && $success) {
                return (object) ['successo'=> '1'];
            }
        }
        return (object)['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros para alterar estoque em lote.']];
    }
    
    /**
     * Reservar produto para um cliente
     */
    public function actionReservarProduto ()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $session = $this->_sessionOpen();
            $objApiCliente=new Cliente;
            $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
            return (new Produto($objApiCliente))->salvarTagReserva(Yii::$app->request->post());
        }
        return (new ApiProduto)->getApiError('Faltam parâmetros para reservar produto');
    }
    
    /**
     * Consultar carrinho à partir do código do produto
     */
    public function actionConsultarCarrinhoProduto()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $session = $this->_sessionOpen();
            $objApiCliente = new Cliente;
            $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
            return (new Carrinho($objApiCliente))->consultarCarrinhoProduto(Yii::$app->request->post('codigo'));
        }
        return (object)['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros para consultar o carrinho.']];
    }

    /**
     * Criar novo produto usando ActiveForm e a classe model correspondente
     * @param boolean $matriz Boolean indicando se é uma ação para matriz ou não
     */
    public function actionNovoProduto($matriz=false)
    {
        $session=Yii::$app->session;
        $session = $this->_sessionOpen();
        $objApiCliente=new Cliente;
        $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
        $model=new Produto($objApiCliente);
        $agenda=new Agenda($objApiCliente);
        $eventos = $matriz == true ? $agenda->buscarEventosPorFiltro(['nome' => 'MATRIZ%']) : $agenda->listaTodosEventos($model->unidade->user_token); 
        $datasEventos = $agenda->listaDatasEventos($matriz == true ? '' : 'MATRIZ');
        //$model->scenario='novo';
        if (Yii::$app->request->post('Produto')) {
            $model->load(Yii::$app->request->post());
            if($model->criarProduto($model->unidade->user_token))
                return $this->redirect(['index']);
        }
        return $this->render('criar',[
            'model' => $model, 
            'eventos' => $datasEventos, 
            'matriz' => $matriz
        ]);
    }

    /**
     * load model - trazer um registro em model a partir de um parâmetro
     * @param int $produto o código do produto a ser buscado
     */
    private function loadModel($produto){
        $session=Yii::$app->session;
        $session = $this->_sessionOpen();
        $objApiCliente=new Cliente;
        $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));
        $model=new Produto($objApiCliente);
        $model->codigo=$produto;
        $model->buscarProduto($model->unidade->user_token);
        return $model;
    }

    /**
     * Mostrar um registro de um Produto
     * @param int $Produto o código do Produto
     */
    public function actionRegistroProduto($produto){
        $model=$this->loadModel($produto);
        if($model->hasErrors())
            return $this->render('/default/error',['name'=>'Erro buscando produto','message'=>'O produto não pôde ser encontrado com este ID ('.$produto.')']);
        return $this->render('view',['produto'=>$model]);
    }

    /**
     * Mostrar um registro de um produto
     * @param int $produto o código do produto
     */
    public function actionEditarProduto($produto){
        $model=$this->loadModel($produto);        
        $agenda=new Agenda($model->unidade);
        $eventos=$agenda->listaTodosEventos($model->unidade->user_token);
        $datasEventos=$agenda->listaDatasEventos();
        if($model->hasErrors())
            return $this->render('/default/error',['name'=>'Erro buscando produto','message'=>'O produto não pôde ser encontrado com este ID ('.$evento.')']);
        if (Yii::$app->request->post('Produto')) {
            $model->load(Yii::$app->request->post());
            if($model->atualizarProduto($model->unidade->user_token))
                return $this->redirect(['index']);
        }
        return $this->render('editar',['model'=>$model, 'eventos'=>$datasEventos]);
    }

    /**
     * Alterar estoque (oferecido) de um produto
     */
    public function actionAlterarEstoque($produto){
        $model=$this->loadModel($produto);
        if($model->hasErrors())
            return $this->render('/default/error',['name'=>'Erro buscando produto','message'=>'O produto não pôde ser encontrado com este ID ('.$produto.')']);
        $model->scenario='movimento-estoque';
        if (Yii::$app->request->post('Produto')) {
            $model->load(Yii::$app->request->post());
            if($model->tipoMovimentoEstoque=='remover')
                $model->unidadesEstoque = '-'.$model->unidadesEstoque;
            if($model->alterarEstoque(['codigo'=>$produto,'tid'=>microtime(),'unidades'=>$model->unidadesEstoque], $model->unidade->user_token))
                return $this->redirect(['registro-produto', 'produto'=>$produto]);
        }
        return $this->render('alterar-estoque',['produto'=>$model]);
    }

    /**
     * Adiconar tags a um produto
     * @param int $produto o código do produto
     */
    public function actionAdicionarTags($produto){
        $model=$this->loadModel($produto);
        if($model->hasErrors())
            return $this->render('/default/error',['name'=>'Erro buscando produto','message'=>'O produto não pôde ser encontrado com este ID ('.$produto.')']);
        $modelTags=new Tags;
        $modelTags->scenario='create';
        if(Yii::$app->request->post('Tags')){
            $modelTags->load(Yii::$app->request->post());
            $addTag=$modelTags->adicionar($model->getWs(),$produto);
            if($addTag->successo){
                Yii::$app->session->setFlash('tag-adicionada','Tag de produto adicionada com sucesso');
                return $this->redirect(['registro-produto','produto'=>$produto]);
            } else $modelTags->addError('nome',$addTag->erro->mensagem);
        }
        return $this->render('add-tag',['produto'=>$model,'model'=>$modelTags]);
    }

    /**
     * Adicionar anexos (upload) a um produto
     * @param int $produto o código do produto
     */
    public function actionAdicionarAnexo($produto){
        $model=$this->loadModel($produto);
        if($model->hasErrors())
            return $this->render('/default/error',['name'=>'Erro buscando produto','message'=>'O produto não pôde ser encontrado com este ID ('.$produto.')']);
        $modelAnexos=new Anexos;
        $modelAnexos->scenario='create';
        if(Yii::$app->request->post('Anexos')){
            $modelAnexos->load(Yii::$app->request->post());
            $modelAnexos->imgBase64(UploadedFile::getInstance($modelAnexos, 'objeto'));            
            if($modelAnexos->validate()){
                $addAnexo=$modelAnexos->adicionar($model->getWs(),$produto);
                if($addAnexo->successo){
                    Yii::$app->session->setFlash('anexo-adicionado','Anexo de produto adicionado com sucesso');
                    return $this->redirect(['registro-produto','produto'=>$produto]);
                } else $modelAnexos->addError('nome',$addAnexo->erro->mensagem);
            }
        }
        return $this->render('add-anexo',['produto'=>$model,'model'=>$modelAnexos]);
    }

    /**
     * Remover tags a um produto
     * @param int $produto o código do produto
     * @param string $nomeTag o nome da tag a ser removida
     */
    public function actionRemoverTags($produto,$nomeTag=false){
        $model=$this->loadModel($produto);
        if($model->hasErrors())
            return $this->render('/default/error',['name'=>'Erro buscando produto','message'=>'O produto não pôde ser encontrado com este ID ('.$produto.')']);
        if($nomeTag){
            if($model->removerTag($nomeTag,$model->unidade->user_token))
                return $this->redirect(['registro-produto','produto'=>$produto]);
        }
        $modelTags=new Tags;
        $modelTags->scenario='lista';
        $modelTags->listFromObjTags($model->tags);

        return $this->render('remover-tags',['tags'=>$modelTags, 'produto'=>$model]);
    }

    /**
     * Remover anexos a um produto
     * @param int $produto o código do produto
     * @param string $nomeAnexo o nome do anexo a ser removido
     * @param string $tipoAnexo o tipo do anexo a ser removido
     */
    public function actionRemoverAnexos($produto,$nomeAnexo=false,$tipoAnexo=false){
        $model=$this->loadModel($produto);
        if($model->hasErrors())
            return $this->render('/default/error',['name'=>'Erro buscando produto','message'=>'O produto não pôde ser encontrado com este ID ('.$produto.')']);
        if($nomeAnexo&&$tipoAnexo){
            if($model->removerAnexo($nomeAnexo,$tipoAnexo,$model->unidade->user_token))
                return $this->redirect(['registro-produto','produto'=>$produto]);
        }
        $modelAnexos=new Anexos;
        $modelAnexos->scenario='lista';
        $modelAnexos->listFromObjAnexos($model->anexos);

        return $this->render('remover-anexos',['anexos'=>$modelAnexos, 'produto'=>$model]);
    }

    /**
     * Remover um produto
     */
    public function actionExcluir($produto){
        $model=$this->loadModel($produto);
        if($model->hasErrors())
            return $this->render('/default/error',['name'=>'Erro buscando produto','message'=>'O produto não pôde ser encontrado com este ID ('.$produto.')']);
        if (Yii::$app->request->post('Produto')) {
            $ex=$model->excluirProduto();
            if(!$ex->successo)
                $model->addError('codigo','Não foi possível excluir o produto - '.$ex->erro->mensagem);
            else return $this->redirect(['index']);
        }
        return $this->render('excluir',['produto'=>$model]);
    }
    
    /**
     * Pesquisa avançada - para gerenciar estoque, matriz base, etc
     */
    public function actionPesquisarDevelop(){
        $session=Yii::$app->session;
        $session = $this->_sessionOpen();
        $objApiCliente=new Cliente;
        $objApiCliente->buscarUnidadePor('user_token',$session->get('token_unidade'));        
        $model=new Produto($objApiCliente);
        $model->buscarProdutosFiltro(['id_agenda'=>'24','tags'=>[['nome'=>'tipo','valor'=>'assento']]]);
        $corrigir=[];
        foreach ($model->lista as $produto) {
            if($produto->oferecido != 1)
                $corrigir[$produto->oferecido][]=[$produto->nome,$produto->codigo];
            if($produto->disponivel != 1)
                $corrigir[$produto->oferecido][]=[$produto->nome,$produto->codigo];
            if($produto->reservado >= 1)
                $corrigir['reservados'][]=[$produto->nome,$produto->codigo];
            if($produto->vendido >= 1)
                $corrigir['vendidos'][]=[$produto->nome,$produto->codigo];
        }
        echo "<pre>";
        print_r($corrigir);
        echo "</pre>";
    }
}
<?php
namespace app\models;
use app\models\Api;
use yii\base\Exception;

/**
 * Classe para Agenda - Easy for Pay - Mineirão
 * obs.: parâmetros entre colchetes são opcionais
 */
class ApiAgenda extends Api
{
    public $logs;
    
    /**
     * Criar evento na agenda
     * @param type $post Dados do evento: nome, [descricao], data (inicial, final, [ativa]), ativo
     * @return object {'successo': '1', 'codigo': <id do evento criado>}
     */
    public function criarEvento($post=[])
    {
        $this->setLog('Cadastro de evento');
        if (!empty($post) && is_array($post)) {
            $this->logs->log('Cadastrar evento', $post);
            $returnApi = json_decode($this->call('agenda/', 'POST', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao cadastrar evento' : 'Evento cadastrado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Cadastro de evento inválido. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível cadastrar o evento.');
    }
    
    /**
     * Alterar evento
     * @param type $post Dados do evento: [nome], [descricao], [data (inicial, final, [ativa])], [ativo]
     * @return object {'successo': '1', 'alterado': '1'}
     */
    public function alterarEvento($post=[])
    {
        $this->setLog('Alteração de evento');
        if (!empty($post) && is_array($post) && isset($post['codigo']) && $post['codigo'] > 0) {
            $this->logs->log('Alterar evento', $post);
            $returnApi = json_decode($this->call('agenda/' . $post['codigo'], 'PUT', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao alterar evento' : 'Evento alterado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Alteração de evento inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível alterar o evento.');
    }
    
    /**
     * Excluir evento
     * @param type $post Dados do evento: codigo
     * @return object {'successo': '1', 'removido': '1'}
     */
    public function excluirEvento($post=[])
    {
        $this->setLog('Exclusão de evento');
        if (isset($post['codigo']) && $post['codigo'] > 0) {
            $this->logs->log('Excluir evento', $post);
            $returnApi = json_decode($this->call('agenda/' . $post['codigo'], 'DELETE'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao excluir evento' : 'Evento excluído com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Exclusão de evento inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível excluir o evento.');
    }
    
    /**
     * Consultar evento
     * @param json $post Parâmetros para busca
     * @param integer $id ID de um evento
     * @return object {'successo': '1', 'eventos': {...}}
     */
    public function buscarEvento($post=null,$id=null)
    {
//        $this->setLog('Consulta de evento');
//        $this->logs->log('Consultar evento', $post);
        $returnApi = json_decode($this->call('agenda/buscar/' . (!is_null($id) ? $id : ''), 'PUT', (!is_null($post) ? $post : NULL), (!is_null($post) ? true : false)));
//        $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar evento' : 'Evento consultado com sucesso', $returnApi);
        return $returnApi;
    }
    
    /**
     * Consultar evento e retornar tags em array associativa
     * @param json $post Parâmetros para busca
     * @param integer $id ID de um evento
     * @return object {'successo': '1', 'eventos': {...}}
     */
    public function buscarEventoTags($post=null,$id=null)
    {
//        $this->setLog('Consulta de evento e tags associativas');
        $evento=$this->buscarEvento($post,$id);
        if($evento->successo!=1)
            throw new Exception('Falha ao consultar evento',1);
        foreach ($evento->objeto as $prod) {
            $tags=[];
            foreach ($prod->tags as $tag) {
                $tags[$tag->nome]=$tag->valor;
            }
            $prod->tags=$tags;
        }
        return $evento;
    }
    
    /**
     * Adicionar tag para evento
     * @param type $post Dados da tag: nome, [valor]
     * @return object
     */
    public function adicionarTagEvento($post=[])
    {
        $this->setLog('Adição de tag de evento');
        if (isset($post['codigo']) && $post['codigo'] > 0) {
            $this->logs->log('Adicionar tag de evento', $post);
            $returnApi = json_decode($this->call('agenda/' . $post['codigo'] . '/tag', 'POST', ['nome' => $post['nome'], 'valor' => isset($post['valor']) && !empty($post['valor']) ? $post['valor'] : '']));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao adicionar tag de evento' : 'Tag de evento adicionada com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Adição de tag de evento inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível adicionar tag de evento.');
    }
    
    /**
     * Excluir tag para evento
     * @param type $post Dados da tag: nome
     * @return object
     */
    public function excluirTagEvento($post=[])
    {
        $this->setLog('Exclusão de tag de evento');
        if (isset($post['codigo']) && $post['codigo'] > 0) {
            $this->logs->log('Excluir tag de evento', $post);
            $returnApi = json_decode($this->call('agenda/' . $post['codigo'] . '/tag', 'DELETE', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao excluir tag de evento' : 'Tag de evento excluída com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Exclusão de tag de evento inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível excluir tag de evento.');
    }
    
    /**
     * Adicionar anexo para evento
     * @param type $post Dados de anexo: nome, tipo, objeto
     * @return object
     */
    public function adicionarAnexoEvento($post=[])
    {
        $this->setLog('Adição de anexo de evento');
        if (isset($post['codigo']) && $post['codigo'] > 0) {
            $this->logs->log('Adicionar anexo de evento', $post);
            $returnApi = json_decode($this->call('agenda/' . $post['codigo'] . '/anexo', 'POST', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao adicionar anexo de evento' : 'Anexo de evento adicionado com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Adição de anexo de evento inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível adicionar anexo de evento.');
    }

    /**
     * Excluir anexo para evento
     * @param type $post Dados de anexo: nome
     * @return object
     */
    public function excluirAnexoEvento($post=[])
    {
        $this->setLog('Exclusão de anexo de evento');
        if (isset($post['codigo']) && $post['codigo'] > 0) {
            $this->logs->log('Excluir anexo de evento', $post);
            $returnApi = json_decode($this->call('agenda/' . $post['codigo'] . '/anexo', 'DELETE', $post));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao excluir anexo de evento' : 'Anexo de evento excluído com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Exclusão de anexo de evento inválida. Formulário vazio.', array_merge($post, $_SESSION));
        return $this->getApiError('Não foi possível excluir anexo de evento.');
    }    
    
    /**
     * Encontrar anexo específico de um evento pelo nome. Retirar o endereço da API antes de solicitar o endpoint
     * @param object $anexo registro de anexo de um evento
     * @param string $nome nome do anexo a ser buscado
     * @return object anexo
    */
    public function findAnexo($anexo,$nome){
//        $this->setLog('Buscando anexo');
        $urlAnexo;
        foreach($anexo as $a){
            if($a->nome == $nome){
                $urlAnexo=$a->url; 
            }
        }
        if(!isset($urlAnexo)) return '';
        $apiIndex=strpos($urlAnexo, 'v1/');
        $urlAnexo=substr($urlAnexo, $apiIndex+3);
        $call=$this->call($urlAnexo, 'GET');
        if($call == ''){
//            $this->setLog('Erro localizando anexo',$anexo);
            throw new Exception('Não foi possível encontrar o anexo solicitado',1);
        }
//        $this->logs->log('Retorno: ',$call);
        return $call;
    }
    
    /**
     * Ler arquivo à partir de um endereço fornecido e transformá-lo em base64
     * Exemplos: C:\wamp\www\mineirao\html\images\banner5.jpg OU /var/www/html/images/banner5.jpg
     */
    public function lerArquivo($arquivo='')
    {
        if (!empty($arquivo)) {
            $type = pathinfo($arquivo, PATHINFO_EXTENSION);
            $data = file_get_contents($arquivo);
            $arqBase64 = base64_encode($data);
            return ['tipo' => $type, 'arq' => $arqBase64];
        }
        return $this->getApiError('Não foi possível enviar arquivo.');
    }

}
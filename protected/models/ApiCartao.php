<?php
namespace app\models;
use Yii;
use app\models\Api;

/**
 * Classe para Cartões - Easy for Pay - Mineirão
 */
class ApiCartao extends Api
{
    public $logs;
    
    /**
     * Gravar os dados de cartão do cliente
     * @param array $post Dados do cartão do cliente: nome_cartao, numero_cartao, cvv, mes_venc, ano_venc, senha
     */
    public function adicionarCartao($post=[]) 
    {
        $this->setLog('Adiciona cartao de cliente');
        $session = Yii::$app->session;
        $session->open();
        if (!empty($post) && is_array($post) && isset($session['user_token']) && !empty($session['user_token'])) { 
            $returnApi = json_decode($this->call('cartoes/', 'POST', $post, true, 'U'));
            $post['numero_cartao'] = 'últimos dígitos: ' . substr($post['numero_cartao'], -4);
            $post['cvv'] = '****';
            $post['senha'] = '****';
            $this->logs->log('Adicionar cartão', $post);
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao adicionar cartão' : 'Cartão adicionado com sucesso', $returnApi);
            if ($returnApi ->successo === '1') {
                (new ApiCliente)->setSessaoClienteCartao();
            }
            return $returnApi;
        }
        $this->logs->log('Adição de cartão inválido. Formulário vazio ou usuário deslogado.');
        return $this->getApiError('Não foi possível adicionar o cartão.');
    }
    
    /**
     * Consultar os cartões do cliente
     */
    public function consultarCartoes() 
    {
        $this->setLog('Consulta cartoes do cliente');
        $session = Yii::$app->session;
        $session->open();
        if (isset($session['user_token']) && !empty($session['user_token'])) {
            $this->logs->log('Consultar cartões', $session['user_token']);
            $returnApi = json_decode($this->call('cartoes/' . $session['user_token'], 'GET', NULL, NULL, 'U'));
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao consultar cartões' : 'Cartões consultados com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Consulta de cartões inválida. Usuário deslogado.');
        return $this->getApiError('Não foi possível consultar os cartões.');
    }
    
    /**
     * Checar cartão de cliente
     * @param array $post Dados do cartão: número do cartão
     */
    public function checarCartao($post=[]) 
    {
        $this->setLog('Checagem de cartao');
        $session = Yii::$app->session;
        $session->open();
        if (is_array($post) && !empty($post) && isset($session['user_token']) && !empty($session['user_token'])) {
            $this->logs->log('Checar cartão', $post);
            $returnApi = json_decode($this->call('cartoes/' . $session['user_token'], 'GET', NULL, NULL, 'U'));
            if ($returnApi->successo === '1' && !empty($returnApi->objeto)) {
                foreach($returnApi->objeto as $index => $customerCard) {
                    if ($returnApi->ultimos_digitos === substr($post['numero_cartao'], -4)) {
                        $this->logs->log('Cartão encontrado.', $post);
                        return $customerCard;
                    }
                }
            }
            $this->logs->log('Cliente não possui cartões.');
            return $this->getApiError('Não foi possível encontrar cartões deste cliente.');
        }
        $this->logs->log('Checagem de cartão inválida. Formulário vazio ou usuário deslogado.');
        return $this->getApiError('Não foi possível checar o cartão.');
    }
    
    /**
     * Excluir cartão do cliente
     * @param array $post Dados do cartão: código do cartão
     */
    public function excluirCartao($post = []) 
    {
        $this->setLog('Exclusao de cartao');
        $session = Yii::$app->session;
        $session->open();
        if (!empty($post) && is_array($post) && isset($session['user_token']) && !empty($session['user_token'])) {
            $this->logs->log('Excluir cartão', $post);
            $returnApi = json_decode($this->call('cartoes/' . $post['codigo_cartao'], 'DELETE', NULL, NULL, 'U'));
            if ($returnApi ->successo === '1') {
                (new ApiCliente)->setSessaoClienteCartao();
            }
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao excluir cartão' : 'Cartão excluído com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Exclusão de cartão inválida. Formulário vazio ou usuário deslogado.');
        return $this->getApiError('Não foi possível excluir o cartão.');
    }
    
    /**
     * Definir um cartão do cliente como favorito
     * @param array $post Dados do cartão: codigo_cartao
     */
    public function marcarFavorito($post = []) 
    {
        $this->setLog('Cartao favorito');
        $session = Yii::$app->session;
        $session->open();
        if (!empty($post) && is_array($post) && isset($session['user_token']) && !empty($session['user_token'])) {
            $this->logs->log('Marcar cartão como favorito', $post);
            $returnApi = json_decode($this->call('cartoes/' . $post['codigo_cartao'] . '/favorito', 'PUT', NULL, NULL, 'U'));
            if ($returnApi ->successo === '1') {
                (new ApiCliente)->setSessaoClienteCartao();
            }
            $this->logs->log($returnApi->successo === '0' ? 'Falha ao marcar cartão como favorito' : 'Cartão marcado como favorito com sucesso', $returnApi);
            return $returnApi;
        }
        $this->logs->log('Não foi possível marcar cartão como favorito. Formulário vazio ou usuário deslogado.');
        return $this->getApiError('Não foi possível marcar o cartão como favorito.');
    }
    
    /**
     * Identificar e retornar o cartão favorito do cliente
     */
    public function consultarFavorito() 
    {
        $this->setLog('Identificar cartao favorito');
        $session = Yii::$app->session;
        $session->open();
        if (isset($session['user_token']) && !empty($session['user_token'])) {
            $this->logs->log('Identificar cartão favorito', $session['user_token']);
            $returnApi = json_decode($this->call('cartoes/', 'GET', NULL, NULL, 'U'));
            if ($returnApi->successo === '1') {
                foreach ($returnApi->objeto as $index => $card) {
                    if ($card->favorito === '1') {
                        $this->logs->log('Cartão favorito encontrado', $card);
                        return $card;
                    }
                }
            }
            $this->logs->log('Cliente não possui cartões ou não possui um cartão favorito.');
            return $this->getApiError('Cliente não possui cartões ou não possui um cartão favorito.');
        }
        $this->logs->log('Identificação de cartão favorito inválida. Usuário deslogado.');
        return $this->getApiError('Não foi possível identificar o cartão favorito.');
    }
    
}
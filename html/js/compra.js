var passo = 0;
var idCompra = '';
var vendaConciliacao=false;
var vendaBrinde=false;
var vendaCreditos=false;

$(document).ready(function () {
    
    $('#autorizar-pagamento').click(function() {
        $(this).button('loading');
        var btAutorizar=$(this);
        /*if($("#Conciliacao_doc").length==0 && $("#Conciliacao_doc").val()!=''){
            checkDoc($("#Conciliacao_doc").val())
        }*/
        var conciliar=opcaoConciliar();
        if(conciliar[1]=='nao-confirma'){
            $(btAutorizar).button('reset');
            return false;
        }
        if(conciliar[1]=='confirma') return false;
        
        var brinde=opcaoBrinde();
        if(brinde[1]=='confirma') return false;
        
        if(conciliar[0]==false || brinde[0]==false) {
            var opc=$('input[name="check-pagar"]:checked').val();
            if(opc=='pagar-creditos')
                return autorizarVendaCreditos(btAutorizar);
            if ($('#parcelas option:selected').val() == '') {
                $('#modal-mensagem .modal-body').html('Para finalizar sua compra, vá ao quadro "Compra Total" e escolha a quantidade de parcelas.');
                $('#modal-mensagem').modal('toggle');
                $('#parcelas').focus();
                $(btAutorizar).button('reset');
                return false;
            }
            if ($('#codigo_cartao').val() == '') {
                $('#modal-mensagem .modal-body').html('Para finalizar sua compra é necessário cadastrar um cartão para pagamento. Acesse sua área de cliente, clicando <a href="' + $('#url-cliente').val() + '">aqui</a>.');
                $('#modal-mensagem').modal('toggle');
                $(btAutorizar).button('reset');
                return false;
            }
            if ($('#senha').val() === '' || isNaN($('#senha').val())) {
                $('#senha').val('').focus();
                $('#modal-mensagem .modal-body').html('Para autorizar sua compra, informe o CVV do cartão selecionado, com 3 ou 4 dígitos.');
                $('#modal-mensagem').modal('toggle');
                $(btAutorizar).button('reset');
                return false;
            }
            doAjax ($('#url-limite').val(), 'POST', '', 'JSON', 5000, 'limiteOK', 'limiteErr');
            return false;
        }
    });    
    
    $('#parcelas').change(function () {
        setValorParcelado($(this).val());
    });
    
    setValorParcelado(1);
    
    /*$('#senha, #parcelas').keypress(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }
    });*/
    
    $('#btn-aceite').click(function () {
        $(this).attr('disabled', 'disabled');
        $('#info-card').addClass('aceite-visto');
        $('#info-card label').addClass('aceite-visto');
        $('#autorizar-pagamento').button('reset');
    });
    

    $('[data-toggle="popover"]').popover(); 	
    $('#nome').focus();
    $('#nome').focusout(function() {
        $('#nome').popover('disable');
    });
		
	$('#popoverEmail').focus();
	$('#popoverEmail').focusout(function() {
        $('#popoverEmail').popover('disable');
    });
    var totalProd = 0;
    var totalEmbaix = 0;
    var totalCamarot = 0;
    
    $('#qtde-prod').on('input',function(){
        var valor = parseInt($(this).val().replace('_', ''));
        totalProd = 1800 * valor;
        if (!isNaN(totalProd)) {
            $('#total-prod').val('R$ ' + parseFloat(totalProd));
            $('#total-geral').val(totalProd + totalEmbaix + totalCamarot);
            $('#total-tela').val('R$ ' + parseFloat($('#total-geral').val()));
        }
    });

    $('#qtde-embaix').on('input',function(){
        var valor = parseInt($(this).val().replace('_', ''));
        totalEmbaix = 1000 * valor;
        if (!isNaN(totalEmbaix)) {
            $('#total-embaix').val('R$ ' + parseFloat(totalEmbaix));
            $('#total-geral').val(totalProd + totalEmbaix + totalCamarot);
            $('#total-tela').val('R$ ' + parseFloat($('#total-geral').val()));
        }
    });

    $('#qtde-camarot').on('input',function(){
        var valor = parseInt($(this).val().replace('_', ''));
        totalCamarot = 2500 * valor;
        if (!isNaN(totalCamarot)) {
            $('#total-camarot').val('R$ ' + parseFloat(totalCamarot));
            $('#total-geral').val(totalProd + totalEmbaix + totalCamarot);
            $('#total-tela').val('R$ ' + parseFloat($('#total-geral').val()));
        }
    });

});
/*
 * Validar limite de compras por CPF, por evento
 * @param {type} data
 * @returns {Boolean}
 */
function limiteOK (data) {
    doAjax ($('#url-temporada').val(), 'POST', '', 'JSON', 500000, 'temporadaOK', 'temporadaErr');
    return false;
}

function limiteErr(data) {
    $('#modal-mensagem .modal-body').html(data.erro.mensagem);
    $('#modal-mensagem').modal('toggle');
    $('#autorizar-pagamento').button('reset')
    return false;
}

/**
 * Validar compra de temporada
 * @param {type} data
 * @returns {Boolean}
 */
function temporadaOK (data) {
    if (passo == 3) {
        autorizarCobranca();
        return false;
    }
    enviarCobranca();
}

function temporadaErr (data) {
    $('#modal-mensagem .modal-body').html('A renovação não está disponível para este cliente. Por favor, confira se o documento ou o e-mail utilizado está correto e tente novamente.');
    $('#modal-mensagem').modal('toggle');
    $('#autorizar-pagamento').button('reset')
    return false;
}

/**
 * Calcular valor parcelado e mostrá-lo
 * @param {type} parcelas
 * @returns {undefined}
 */
function setValorParcelado(parcelas) {
    var total = $('#preco-final').val() / parcelas;
    $('#preco-final-parcelado').val(total*parcelas);
    //$('#vlr-parcelado').html('R$ ' + total.toFixed(2).replace('.', ','));
    //$('#vlr-parcelado').html('R$ ' + (total*parcelas).toLocaleString('latn',{'minimumFractionDigits':'2','maximumFractionDigits':'2'}));
}

function enviarCobranca () {
    doAjax ($('#url-cobranca').val(), 'POST', $('#form-pagamento').serialize(), 'JSON', 500000, 'enviarCobrancaOK', 'enviarCobrancaErr');
    return false;
}

function enviarCobrancaOK (data) {
    idCompra = data.objeto.codigo_cobranca;
    if (vendaConciliacao) {
        showMsg('Venda conciliada. Favor confirmar o recebimento do voucher com o cliente');
        $('#autorizar-pagamento').button('reset');
        $('#div-carrinho').addClass('hidden');
        $('#modal-mensagem').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            window.location.href = $('#url-index').val();
        });
        return false;
    }
    if (vendaBrinde) {
        showMsg('Pedido concluído! Você será redirecionado para a área do cliente');
        $('#autorizar-pagamento').button('reset');
        $('#div-carrinho').addClass('hidden');
        $('#modal-mensagem').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            window.location.href = $('#url-email-compra').val() + '?id_compra=' + idCompra;
        });
        return false;
    }

    doAjax ($('#url-consulta').val(), 'POST', $('#form-pagamento').serialize(), 'JSON', 500000, 'consultarCobrancaOK', 'consultarCobrancaErr');
    return false;
}

function enviarCobrancaErr (data) {
    passo = 1;
    $('#modal-mensagem .modal-body').html('Erro ao enviar a cobrança: ' + data.erro.mensagem);
    $('#modal-mensagem').modal('toggle');
}

function consultarCobrancaOK (data) {
    if (data.hasOwnProperty('objeto') && data.objeto.hasOwnProperty('codigo') && data.objeto.codigo != '') {
        idCompra = data.objeto.codigo;
        if(vendaCreditos)
            efetuarPagamentoCreditos(idCompra,data.objeto.total);
        else
            autorizarCobranca();
        return false;
    }
    $('#modal-mensagem .modal-body').html('Erro ao consultar a cobrança.');
    $('#modal-mensagem').modal('toggle');
}

function consultarCobrancaErr (data) {
    passo = 2;
    $('#modal-mensagem .modal-body').html('Erro ao consultar a cobrança: ' + data.erro.mensagem);
    $('#modal-mensagem').modal('toggle');
    $('#autorizar-pagamento').button('reset')
}

function efetuarPagamentoCreditos (id_cobranca,preco_final) {
    doAjax ($('#url-pagamento-creditos').val(), 'POST', {'codigo_cobranca':id_cobranca,'preco_final':preco_final}, 'JSON', 500000, 'autorizarCobrancaOK', 'autorizarCobrancaErr');
    return false;
}

function autorizarCobranca () {
    doAjax ($('#url-autoriza').val(), 'POST', $('#form-pagamento').serialize() + '&id_compra=' + idCompra, 'JSON', 500000, 'autorizarCobrancaOK', 'autorizarCobrancaErr');
    return false;
}

function autorizarCobrancaOK (data) {
    window.location.href = $('#url-email-compra').val() + '?id_compra=' + idCompra + '#tab-historico';
    return false;
}

function autorizarCobrancaErr (data) {
    passo = 3;
    $('#modal-mensagem .modal-body').html('Erro ao autorizar a cobrança: ' + data.erro.mensagem);
    $('#modal-mensagem').modal('toggle');
     $('#autorizar-pagamento').button('reset')
}

function opcaoConciliar(){
    if($("#Conciliacao_tipo").length==0) return [false,null];
    var tipo=$("#Conciliacao_tipo").val();
    var id=$("#Conciliacao_id").val();
    var doc=$("#Conciliacao_doc").val();
    $("#msgErroConciliacao").html('');
    if(tipo!='' && id!='' && doc!=''){
        if(confirm('IMPORTANTE! Esse pedido já foi pago?')){
            doAjax ($('#url-limite').val(), 'POST', '', 'JSON', 5000, 'limiteOK', 'limiteErr');
            vendaConciliacao=true;
            return [true,'confirma'];
        }
        else return [true,'nao-confirma'];
    } else {
        if(tipo=='' && (id!='' || doc!='')){
            showMsg('Selecione um tipo de conciliação');
            return [true,'nao-confirma'];        
        }else if(id=='' && (tipo!='' || doc!='')){
            showMsg('Informe uma identificação para localizar essa compra. Se for em dinheiro, informe a quantia.');
            return [true,'nao-confirma'];        
        }else if(doc=='' && (id!='' || id!='')){
            showMsg('Informe o documento do cliente');
            return [true,'nao-confirma'];        
        } else return [false,null];
    }
    return [true,false];
}

function opcaoBrinde(){
    if($("#preco-final").val()!=0) return [false,null];
    doAjax ($('#url-limite').val(), 'POST', '', 'JSON', 5000, 'limiteOK', 'limiteErr');
    vendaBrinde=true;
    return [true,'confirma'];
}

function checkDoc(doc){
    $.getJSON('check-conta',{user_id:doc},function(data){
        if(data.successo==false){
            showMsg('Você precisa cadastrar este usuário');
            $("#msgConciliacao").html('<a id="novoCadastroConciliacao">Cadastrar</a>');
            return [true,'nao-confirma'];
        }
    })
}

function mostrarSaldoUsuario(url){
    doAjax(url, 'GET', '', 'JSON', 10000, 'saldoUsuarioOk', 'saldoUsuarioErro');
}

function saldoUsuarioOk(data){
    if($("#check-pagar-creditos").length<=0) return;
    if(data.creditos=='0.00'){
        $("#check-pagar-cartao").prop('checked',true);
        $("#check-pagar-creditos").prop('checked',false).attr('disabled','disabled');
        $("#pagamento-creditos-saldo-usuario").html('0');
    }
    $("#check-pagar-creditos")[0].removeAttribute('disabled');
    $("#pagamento-creditos-saldo-usuario").html('['+data.creditosFormatado+']');
}

function saldoUsuarioErro(data){
    $("#check-pagar-creditos").prop('checked',false).attr('disabled','disabled');
    $("#check-pagar-cartao").prop('checked',true);
    $("#pagamento-creditos-saldo-usuario").html('[R$ 0,00]');
}

function autorizarVendaCreditos(btn){
    if(confirm('Ao confirmar, você usará seus créditos Mineirão para autorizar essa venda')){
        vendaCreditos=true;
        doAjax ($('#url-limite').val(), 'POST', '', 'JSON', 10000, 'limiteOK', 'limiteErr');
        return [true,'confirma'];
    }
}
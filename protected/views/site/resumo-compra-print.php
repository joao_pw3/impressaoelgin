<?php
use app\models\ApiAgenda;
use app\models\Mineiraocard;
use app\components\VendaWidget;
use app\models\Tags;
?>
<page class="mineirao-vouchers" size="A4">
<?php
if (isset($carrinho->objeto->produtos) && !empty($carrinho->objeto->produtos)) { 
    $mapa='';
    foreach ($carrinho->objeto->produtos as $index => $produto) {
        $unidades=1;
        $agenda = new ApiAgenda();
        $mineiraocardNome='';
        $mineiraocardDoc='';
        $mineiraocardTel='';
        $mineiraocardEmail='';
        $classSectionEspecial='';
        
        if(isset($carrinho->objeto->tags)){
            foreach($carrinho->objeto->tags as $tag){
                $nome=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_nome');
                if($nome!=false)
                    $mineiraocardNome=$nome;
                $doc=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_documento');
                if($doc!=false)
                    $mineiraocardDoc=$doc;
                $tel=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_telefone');
                if($tel!=false)
                    $mineiraocardTel=$tel;
                $email=Mineiraocard::checkTagCarrinho($tag,$produto->codigo,'_email');
                if($email!=false)
                    $mineiraocardEmail=$email;
            }
        }

        if(isset($produto->tags)){
            foreach ($produto->tags as $index_tag => $tag) {
                if ($tag->nome == 'matriz') {
                    $evento = $agenda->buscarEvento(json_encode(['tags' => [['nome' => 'apelido', 'valor' => $tag->valor]]]));
                }
            }
        }
        if(!isset($produto->tags) && isset($produto->agenda->codigo))
            $evento = $agenda->buscarEvento(null,$produto->agenda->codigo);

        $nomeEvento='';
        $banner='';
        if($evento->successo==1){
            $nomeEvento=$evento->objeto[0]->nome;
            if (isset($evento->objeto[0]->anexos)) {
                foreach ($evento->objeto[0]->anexos as $index_anexo => $anexo) {
                    if ($anexo->nome == 'bannervitrine' || $anexo->nome == 'imgEvento') {
                        $banner = $anexo->url;
                    }
                }
            }
            if (isset($evento->objeto[0]->tags)) {
                foreach ($evento->objeto[0]->tags as $index_tags => $tags) {
                    if ($tags->nome == 'assento') {
                        $mapa = $tags->valor;
                    }
                    if($tags->nome=='layout_print_especial')
                        $classSectionEspecial=$tags->valor;
                }            
            }
        }

        while ($unidades <= $produto->unidades) {

            $ingressoNome = isset($produto->cupom->nome) ? $produto->cupom->nome : $produto->nome;
            $ingressoCupom = isset($produto->cupom->id) ? $produto->cupom->id : ''; 
            $ocupantes= (array) (new Tags)->objetoTags($carrinho->objeto->tags);
            if($ingressoCupom != '')
                $codigo = $produto->codigo.'_'.$ingressoCupom;
            else
                $codigo = $produto->codigo;

            if($mapa != '') {
                $qrCode = VendaWidget::qrCodeIngresso($produto->qrcodes[$unidades-1], 'print');
                $documentoOcupante = $ocupantes[$codigo.'_'.$unidades.'_documento'];
                $mineiraocardNome = $ocupantes[$codigo.'_'.$unidades.'_nome'];
            } else {
                $qrCode=VendaWidget::qrCodeVoucher($compra->objeto[0]->vendas->codigo, $produto, (object)['nome'=>$mineiraocardNome,'doc'=>$mineiraocardDoc],'print'); 
                $documentoOcupante = $ocupantes[$codigo.'_documento'];
            }
        ?>
            <section<?=$classSectionEspecial!= '' ?' class="'.$classSectionEspecial.'"' :''?>>
                <div class="superior2">
                    <h2 class="ladoDir">
                        <?php if($mapa=='') { ?>
                            Este é o seu voucher Mineirão. Enquanto o seu cartão personalizado para acesso a toda a temporada está em produção,  apresente este voucher na bilheteria e retire seu ingresso para a partida.
                        <?php } ?>
                        <?php if($mapa!='') { ?>
                            Este voucher é seu ingresso. Apresente-o para validar sua entrada.
                        <?php } ?>
                    </h2>
                </div>
                <div class="boxTable"<?=$classSectionEspecial=='camarote1' ?' style="width:66.66666667%;float:left;padding-left:15px;padding-right:15px;"' :''?>>
                    <div>
                        <?=$qrCode?>
                    </div>
                    <div>
                        <h2><?=$mineiraocardNome?></h2>
                        <hr class="solido">
                        <p class="margemTop fontMaior">
                        <strong><?=$nomeEvento;?></strong><br>
                        <strong><?=$ingressoNome;?></strong></p>
                        <p>
                        Identificador: <?=$compra->objeto[0]->vendas->codigo?><br />
                        Documento: <?=$documentoOcupante ?><br />
                        AUTORIZADO<br />
                        </p>
                        <?php if($mapa=='') { ?>
                            <p class="margemTop2">
                                Seu cartão entrará em produção e ficará disponível <u>após 15 dias úteis</u>. Compareça na bilheteria <u>portando um documento com foto e este voucher</u> para retirar seu cartão de entrada. Informe-se nos nossos canais de atendimento quais dias a bilheteria estará aberta</u>.
                            </p>
                        <?php } ?>
                    </div>
                </div>
                <?php if($classSectionEspecial=='camarote1') {
                    echo '<img style="width:33.33333333%;float:left;padding-left:15px;padding-right:15px;padding-top:45px" src="'.Yii::getAlias('@web').'/images/cmarote_final.png" />';

                    echo '<hr style="clear:both" />';

                    echo '<img src="'.Yii::getAlias('@web').'/images/Convite_mapa_PeB_2_final.png" />';
                }?>
                <?php if($classSectionEspecial==''){ ?>
                    <hr>
                <?php } ?>
            </section>        
        <?php
            $unidades++;
        }
    }
}?>
</page>
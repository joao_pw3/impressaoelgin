<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\Carrinho;
use app\models\Cobranca;
use app\models\Ocupante;
use app\models\Cartao;
use app\models\Produto;
use app\models\Cliente;
use app\models\WsCarrinho;
use app\models\Cupom;
use app\models\Agenda;
use app\models\Taxa;
use app\models\Tags;

class CompraController extends Controller
{
    public $enableCsrfValidation = true;
    
    /**
     * Em caso de erros, retornar a página 
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Ações anteriores a uma action da classe
     * @param type $action
     * @return type
     */
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    /**
     * Escolha de produtos
     * @return type
     */
    public function actionIndex () 
    {
        $session = Yii::$app->session;
        $session->open();
        $produtos = new Produto;
        $produtos->buscarProdutosFiltro([
            'ativo' => '1',
            'tags' => [
        ['nome' => 'grupo', 'valor' => 'bloco-2']
            ]
        ]);
        return $this->render('index', [
            'produtos' => $produtos->lista
        ]);
    }
    
    /**
     * Adicionar produto ao carrinho de compras
     * Formato do Post: [produto]: {'codigo': '', 'unidades': ''}
     */
    public function actionAdicionarProdutoCarrinho () 
    {
        try {
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $session = Yii::$app->session;
                $session->open();
                $objApiCliente = new Cliente;
                $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
                $produtos = new Produto($objApiCliente);  
                $produtos->codigo=Yii::$app->request->post()["produto_codigo"];            
                $produtos->buscarProduto();
                $agenda = new Agenda($objApiCliente);
                $verificacao = $agenda->selecionouOutraUnidade($produtos->vendedor->documento);
                if ($verificacao->successo == '0') {
                    return $verificacao;
                }
                $session->set('documento_unidade', $produtos->vendedor->documento);
                return (new Carrinho($objApiCliente))->atualizarCarrinho(Yii::$app->request->post());
            }
            return (new WsCarrinho)->getApiError('Faltam parâmetros para adicionar produto ao carrinho.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao adicionar produto ao carrinho', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Remover produto do carrinho de compras e da sessão
     */
    public function actionRemoverProdutoCarrinho () 
    {
        $session = Yii::$app->session;
        $session->open();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
        try {
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $post = Yii::$app->request->post();
                $carrinho = new Carrinho($objApiCliente);
                return $carrinho->atualizarCarrinho(['produto_codigo' => $post['produto_codigo'], 'produto_unidades' => $post['produto_unidades']]);
            }
            return (new WsCarrinho)->getApiError('Faltam parâmetros para adicionar produto ao carrinho.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao remover produto do carrinho', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Adicionar cupom ao carrinho de compras
     * Formato do Post: [cupom]: {'id': '', 'unidades': ''}
     */
    public function actionAdicionarCupomCarrinho () 
    {
        $session = Yii::$app->session;
        $session->open();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
        try {
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $post = Yii::$app->request->post('Ocupante');
                $return = true;
                foreach ($post as $index => $ocupante) {
                    $returnApi = (new Carrinho($objApiCliente))->atualizarCarrinho($ocupante);
                    if ($returnApi->successo == '0') {
                        $return = false;
                    }
                }
                return $return ? (object)['successo' => '1'] : (object)['successo' => '0', 'erro' => ['mensagem' => 'Falha ao adicionar ingresso ao carrinho.']];
            }
            return (object)['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros para adicionar ingresso ao carrinho.']];
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao adicionar ingresso ao carrinho', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Remover cupom do carrinho de compras (cliente mudou cupom para inteira)
     */
    public function actionRemoverCupomCarrinho ()
    {
        $session = Yii::$app->session;
        $session->open();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
        try {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                return (new Carrinho($objApiCliente))->removerCupomCarrinho(Yii::$app->request->post('Ocupante'));
            }
            return (object)['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros para remover cupom do carrinho.']];
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao remover cupom do carrinho', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Adicionar tags ao carrinho de compras
     * Formato do Post: [tag]: {'nome': '', 'valor': ''}
     */
    public function actionAdicionarTagCarrinho () 
    {
        $session = Yii::$app->session;
        $session->open();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
        try {
            if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $post = Yii::$app->request->post();
                return (new Carrinho($objApiCliente))->atualizarCarrinho($post);
            }
            return (new WsCarrinho)->getApiError('Faltam parâmetros para adicionar tag ao carrinho.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao adicionar tag ao carrinho', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Identificar tipo de ingresso e dados de ocupantes
     * @param integer $evento Id do evento
     * @param integer $data Id da data do evento
     * @return type
     */
    public function actionIngresso ($evento=null, $data=null) 
    {   

        $session = Yii::$app->session;
        $session->open();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
        
        $model = new Cupom(new Cliente);

        $objTags = new Tags();
        $taxa=new Taxa($objApiCliente);

        $modelCarrinho=new Carrinho($objApiCliente);
        $carrinho = $modelCarrinho->consultarCarrinho();
        if(is_null($evento) && isset($carrinho->successo) && $carrinho->successo=='1')
            $evento=$carrinho->objeto->produtos[0]->agenda->codigo;

        $ocupantes = new Ocupante(false);
        try {
            if (Yii::$app->request->post()) {
                $post = Yii::$app->request->post('Ocupante');
                if($post==''){
                    $modelCarrinho->limparCarrinho();
                    echo json_encode(['successo'=>'1','objeto'=>['evento'=>$evento]]);
                    exit();
                }
                $return = $ocupantes->salvarEmLote($post);
                if(!isset($return->successo))
                    $ocupantes->addError('produto_codigo','Não foi possível salvar os dados enviados');
                if(isset($return->successo) && !$return->successo)
                    $ocupantes->addError('produto_codigo',$return->erro->mensagem);
                if(isset($return->successo) && $return->successo){
                    $acao=isset($_GET['acao']) ?$_GET['acao'] :'';
                    if($acao=='continuar-compra'){
                        echo json_encode(['successo'=>'1','objeto'=>['evento'=>$evento]]);
                        exit();
                    }
                    $this->redirect(['/site/pagamento', 'evento'=>$evento, 'data'=>$data]);
                }
                
            }
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao salvar ocupantes', 'message' => $e->getMessage()]);
        }
        return $this->render('ingresso', [
            'evento' => $evento,
            'data' => $data,
            'ocupante' => $ocupantes,
            'objCupom' => $model,
            'carrinho' => $carrinho,
            'listaProdutos'=>$this->geraListaProdutos($objTags,$carrinho),
            'objTags' => $objTags,
            'taxa' => $taxa,
        ]);
    }

    /**
     * Autorização de pagamento
     * @param integer $evento Id do evento atual
     * @param integer $data Id do data do evento atual
     * @return type
     */
    public function actionPagamento ($evento=null, $data=null) 
    {
        $session = Yii::$app->session;
        $session->open();
        $session->set('url-finaliza-pagamento', base64_encode(Yii::$app->request->url));

        if(empty($session['user_id'])) {
            return $this->redirect(['/site/login']);
            exit;
        }

        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
        $cobranca=new Cobranca($objApiCliente);
        $modelCarrinho=new Carrinho($objApiCliente);
        $carrinho = $modelCarrinho->consultarCarrinho();
        if(is_null($evento) && isset($carrinho->successo) && $carrinho->successo=='1')
            $evento=$carrinho->objeto->produtos[0]->agenda->codigo;
        $taxa = new Taxa($objApiCliente);

        $objTags = new Tags();
        return $this->render('pagamento', [
            'evento' => $evento,
            'data' => $data,
            'taxa' => $taxa,
            'carrinho' => $carrinho,
            'cobranca' => $cobranca,
            'cartoes' => (new Cartao)->consultarCartoes(),
            'listaProdutos'=>$this->geraListaProdutos($objTags,$carrinho)
        ]);
    }
    
    /**
     * Registrar intenção de compra
     * Adicionar taxa de conveniência passando o segundo parâmetro de Cobranca.enviarCobranca como true
     */
    public function actionEnviarCobranca () 
    {
        try {
            if (Yii::$app->request->isAjax && Yii::$app->request->post('Cobranca')) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $session = Yii::$app->session;
                $session->open();
                $objApiCliente = new Cliente;
                $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
                $cobranca=new Cobranca($objApiCliente);
                return $cobranca->enviarCobranca(Yii::$app->request->post('Cobranca'),true);
            }
            return (new WsCobranca)->getApiError('Faltam parâmetros para enviar cobrança.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao registrar intenção de compra.', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Consultar registro de intenção de compra
     */
    public function actionConsultarCobranca () 
    {
        try {
            if (Yii::$app->request->isAjax && Yii::$app->request->post('Cobranca')) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $session = Yii::$app->session;
                $session->open();
                $objApiCliente = new Cliente;
                $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
                $cobranca=new Cobranca($objApiCliente);
                return $cobranca->consultarCobranca(Yii::$app->request->post('Cobranca'));
            }
            return (new WsCobranca)->getApiError('Faltam parâmetros para consultar cobrança.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao consultar cobrança.', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Autorizar cobrança
     */
    public function actionAutorizarCobranca () 
    {
        try {
            if (Yii::$app->request->isAjax && Yii::$app->request->post('Cobranca')) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $session = Yii::$app->session;
                $session->open();
                $objApiCliente = new Cliente;
                $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
                $cobranca=new Cobranca($objApiCliente);
                //
                $cobranca->load(Yii::$app->request->post());
                $autoriza=$cobranca->autorizarPagamento(Yii::$app->request->post('Cobranca'));
                if(!isset($autoriza->successo)){
                    return (new WsCobranca($objApiCliente->user_token))->getApiError('Não foi possível efetuar o pagamento');
                    exit();
                }
                if($autoriza->successo){
                    $cobranca->validate();
                    $cobranca->envioEmailConfirmacao();
                }
                return $autoriza;

            }
            return (new WsCobranca($objApiCliente->user_token))->getApiError('Faltam parâmetros para autorizar cobrança.');
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao autorizar cobrança.', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Ações pós-venda
     */
    public function actionPosVenda () 
    {
        $session = Yii::$app->session;
        $session->open();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                return (new Carrinho($objApiCliente))->posVenda();
            }
            return (object)['successo' => '0', 'erro' => ['mensagem' => 'Faltam parâmetros para autorizar cobrança.']];
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao fazer ação pós-venda.', 'message' => $e->getMessage()]);
        }
    }
    
    /**
     * Action para pegar a taxa de conveniência por ajax
    */
    public function actionMostrarTaxa () 
    {
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
                $post=Yii::$app->request->post();
                $session = Yii::$app->session;
                $session->open();
                $objApiCliente = new Cliente;
                $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
                $taxa=new Taxa($objApiCliente);
                echo json_encode(['successo'=>'1','objeto'=>[
                    'valor'=>number_format($post['valor'],2,',','.'),
                    'exibirTaxa'=>number_format($taxa->exibirTaxa($post['valor']),2,',','.'),
                    'exibirValorComTaxa'=>number_format($taxa->exibirValorComTaxa($post['valor']),2,',','.')
                ]]);
                exit();
            }
            return (object)['successo' => '0', 'erro' => ['mensagem' => 'Erro na consulta da taxa.']];
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Não foi possível calcular o preço.', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Montar a lista de produtos para ingressos e pagamento
     */
    private function geraListaProdutos($objTags,$carrinho){
        $listaProdutos=[];
        if(isset($carrinho->objeto,$carrinho->objeto->produtos)){
            if(isset($carrinho->objeto->tags)){
                $tags = $objTags->arrayTags($carrinho->objeto->tags);             
            }
            foreach ($carrinho->objeto->produtos as $index => $produto) {
                $contagemQtde=1;
                while ($contagemQtde <= $produto->unidades) {
                    $indiceNameForm=$contagemQtde;
                    $cupom=isset($produto->cupom) ?'_'.$produto->cupom->id :'';
                    $linhaProduto = ['codigo'=>$produto->codigo.$cupom.'_'.$indiceNameForm,'indexForm'=>$indiceNameForm,'objeto'=>$produto,'tipo'=>'inteira','nome'=>'','documento'=>'','email'=>'','telefone'=>''];
                    if(isset($tags)){
                        foreach ($tags as $k=>$v) {
                            if($linhaProduto['codigo'].'_nome' == $k)
                                $linhaProduto['nome']=$v;
                            if($linhaProduto['codigo'].'_documento' == $k)
                                $linhaProduto['documento']=$v;
                            if($linhaProduto['codigo'].'_email' == $k)
                                $linhaProduto['email']=$v;
                            if($linhaProduto['codigo'].'_telefone' == $k)
                                $linhaProduto['telefone']=$v;
                        }
                    }
                    if(isset($produto->cupom))
                        $linhaProduto['tipo']=$produto->cupom->id;
                    
                    $listaProdutos[]=$linhaProduto;
                    $contagemQtde++;
                }
            }
        }
        return $listaProdutos;
    }

    /**
     * Voltar para compra anterior (localizar a unidade pelo carrinho e alterar para continuar processo de compra)
     */
    public function actionConcluirCompra(){
        $session = Yii::$app->session;
        $session->open();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('documento', $session->get('documento_unidade'));
        $session->set('token_unidade',$objApiCliente->user_token);
        $this->redirect(['/compra/ingresso']);
    }

    /**
     * Voltar para compra anterior para o Mineirão - considerando qual evento, o redirect deverá ser compra/ingresso ou site/blocos
     */
    public function actionConcluirCompraMineirao(){
        $session = Yii::$app->session;
        $session->open();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('user_token', $objApiCliente->getTokenMatriz());
        $session->set('token_unidade',$objApiCliente->user_token);
        $modelCarrinho=new Carrinho($objApiCliente);
        $carrinho=$modelCarrinho->consultarCarrinho();
        $produto=$carrinho->objeto->produtos[0];
        if(isset($produto->tags)){
            $tags=Tags::arrayTags($produto->tags);
            if(isset($tags['bloco'],$tags['fileira'],$tags['cadeira'])){
                $idEventoTemporada=YII_ENV=='dev' ?808 :810;
                return $this->redirect(\yii\helpers\Url::to(['/site/blocos','id'=>$idEventoTemporada,'area'=>$tags['area']]));
            }
        }
        return $this->redirect(['/compra/ingresso']);
    }

    /**
     * Iniciar uma nova compra - zera o carrinho da unidade anterior e redireciona para a página anterior pelo referer
     */
    public function actionNovaCompra(){
        $session = Yii::$app->session;
        $session->open();
        $objApiCliente = new Cliente;
        $objApiCliente->buscarUnidadePor('documento', $session->get('documento_unidade'));
        $carrinho=new Carrinho($objApiCliente);
        $carrinho->limparCarrinho();
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Validar cupom privado
     */
    public function actionValidarCupom(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $post=Yii::$app->request->post();
            $session = Yii::$app->session;
            $session->open();
            $objApiCliente = new Cliente;
            $objApiCliente->buscarUnidadePor('user_token', $session->get('token_unidade'));
            $objCupom=new Cupom($objApiCliente);
            $prefixoCupom='PRIV_'.$post['evento'].'_'.$post['data'].'_';
            $objCupom->buscarCupom(['id'=>$prefixoCupom.$post['cupom']]);
            if(is_array($objCupom->lista) && count($objCupom->lista)===1){
                $objCarrinho=new Carrinho($objApiCliente);
                $carrinho=$objCarrinho->consultarCarrinho();
                $idEventoDataCupom=Cupom::infoEventoData($objCupom->lista[0]->id);
                if($objCupom->lista[0]->estoque->disponiveis>0)
                    return (object)['successo' => '1','objeto'=>['cupom'=>['id'=>$objCupom->lista[0]->id, 'nome'=>$objCupom->lista[0]->nome, 'descricao'=>$objCupom->lista[0]->descricao, 'desconto_fixo'=>$objCupom->lista[0]->desconto_fixo, 'desconto_percentual'=>$objCupom->lista[0]->desconto_percentual]]];       
            }
            return (object)['successo' => '0', 'erro'=>['mensagem'=>'Cupom não encontrado']];      
        }
        return (object)['successo' => '0', 'erro' => ['mensagem' => 'Parâmetros inválidos.']];
    }
}
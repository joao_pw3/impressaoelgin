<?php
use yii\helpers\Html;
use yii\helpers\Url;
$idEventoTemporada=810;
$idEventoCamaroteCopa=887;
$imgEventoCamaroteCopa=Yii::getAlias('@web').'/images/brasil_belgica.jpg';

if(YII_ENV=='dev'){
    $idEventoTemporada=808;
    $idEventoCamaroteCopa=1120;
}
?>
<div class="eventos" id="eventos">
    
    <div class="container">

		<div class="small-12 columns wow fadeInUp margFinal" data-wow-delay=".3s" style="animation-delay: 0.3s; animation-name: fadeInUp;">
			
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <li data-target="#myCarousel" data-slide-to="2"></li>
								<li data-target="#myCarousel" data-slide-to="3"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
								<div class="item active">
                                    <a href="<?= Url::to(['/agenda','evento'=>$idEventoCamaroteCopa]); ?>">
                                        <img src="<?php echo $imgEventoCamaroteCopa?>" alt="">
                                    </a>
                                </div>
								
								<div class="item">
                                    <img src="<?php echo Yii::getAlias('@web')?>/images/banner6.jpg" alt="">
								</div>

                                <div class="item">
                                    <a href="<?= Url::to(['site/blocos','id'=>$idEventoTemporada,'area'=>'Oeste']); ?>">
                                        <img src="<?php echo Yii::getAlias('@web')?>/images/banner7.jpg" alt="">
                                    </a>
                                </div>

                                <div class="item">
                                    <a href="<?= Url::to(['site/blocos','id'=>$idEventoTemporada,'area'=>'Leste']); ?>">
                                        <img src="<?php echo Yii::getAlias('@web')?>/images/banner9.jpg" alt="">
                                    </a>
								</div>
									
								
			  </div>

			  <!-- Left and right controls -->
			  <a class="left carousel-control alinhaL" href="#myCarousel" data-slide="prev">
				<span class="setLeft alinhaC"><img src="<?php echo Yii::getAlias('@web')?>/images/setL.png" alt=">"></span>
				<span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control alinhaL" href="#myCarousel" data-slide="next">
				<span class="setRight alinhaC"><img src="<?php echo Yii::getAlias('@web')?>/images/setR.png" alt="<"></span>
				<span class="sr-only">Next</span>
			  </a>
			</div>


        </div>
        <br>
        <br>
	
        <?php foreach($eventos as $index => $evento) { 
            if (isset($evento->tags,$evento->tags['apelido']) && $evento->tags['apelido']=='temporada2018') { ?>
                <div style="margin-bottom:25px;">
                    <div class="col-md-6 about-left wow fadeInLeft animated" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                        <a href="<?= Url::to(['site/blocos','id'=>$evento->codigo, 'area'=>'Oeste']); ?>"><?= Html::img($evento->anexos['bannervitrine_2']); ?></a>
                    </div>
                    <div class="col-md-4 about-right wow fadeInRight animated textosAprest" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                            <div>Mineirão Tribuna</div>                    
                            Muito mais do que futebol. Garanta já o seu lugar, compre aqui!
                    </div>
                    <div class="col-md-2 about-right wow fadeInRight animated" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">               
                        <span class="row contato">               
                            <a class="areas seta" href="<?= Url::to(['site/blocos','id'=>$evento->codigo, 'area'=>'Oeste']); ?>">Esgotado</a>
                        </span>
                    </div>
                    <hr style="padding-top:25px;">
                </div>
                <div style="margin-bottom:25px;">
                    <div class="col-md-6 about-left wow fadeInLeft animated" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                        <a href="<?= Url::to(['site/blocos','id'=>$evento->codigo, 'area'=>'Leste']); ?>"><?= Html::img($evento->anexos['bannervitrine_1']); ?></a>
                    </div>
                    <div class="col-md-4 about-right wow fadeInRight animated textosAprest" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                            <div>Mineirão Embaixada</div>                    
                            Você mais perto da história. Garanta já o seu lugar, compre aqui!
                    </div>
                    <div class="col-md-2 about-right wow fadeInRight animated" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">               
                        <span class="row contato">               
                            <a class="areas seta" href="<?= Url::to(['site/blocos','id'=>$evento->codigo, 'area'=>'Leste']); ?>">Comprar</a> 
                        </span>
                    </div>
                    <hr style="padding-top:25px;">
                </div>
                <div style="margin-bottom:25px;">
                    <div class="col-md-6 about-left wow fadeInLeft animated" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                        <a href="<?= Url::to(['site/areas','id'=>$evento->codigo]); ?>"><?= Html::img($evento->anexos['bannervitrine_0']); ?></a>
                    </div>
                    <div class="col-md-4 about-right wow fadeInRight animated textosAprest" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                            <div>Mineirão Camarote</div>                    
                            Garanta já o seu lugar, compre aqui!
                    </div>
                    <div class="col-md-2 about-right wow fadeInRight animated" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">               
                        <span class="row contato">               
                            <a class="areas seta" href="<?= Url::to(['site/areas','id'=>$evento->codigo]); ?>">Comprar</a> 
                        </span>
                    </div>
                    <hr style="padding-top:25px;">
                </div>
                
            <?php }
            else {  
				$linkEvento=Url::to(['site/areas','id'=>$evento->codigo]);
				if(isset($evento->tags,$evento->tags['assento']) && $evento->tags['assento']=='livre')
					$linkEvento=Url::to(['/agenda','evento'=>$evento->codigo]);
				?>
				<div style="margin-bottom:25px;">
                   
				   <div class="col-md-6 about-left wow fadeInLeft animated" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
						<a href="<?= $linkEvento; ?>">
							<?= isset($evento->anexos['bannervitrine']) ?Html::img($evento->anexos['bannervitrine']) : ''; ?>
							<?= isset($evento->anexos['imgEvento']) ?Html::img($evento->anexos['imgEvento']) : ''; ?>
						</a>
                    </div>
                    <div class="col-md-4 about-right wow fadeInRight animated textosAprest" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">
                            <div><?= $evento->nome; ?></div>                    
                                <?php
                                $objData=\DateTime::createFromFormat('d/m/Y H:i:s',$evento->data->inicial);
                                $diaSemana=['','Seg','Ter','Qua','Qui','Sex','Sab','Dom'];
                                ?>
                                    <div>
                                        <?= $diaSemana[$objData->format('N')].' '.$objData->format('d/m') ?> - 
                                        Horário: <?= $objData->format('H'); ?>h
                                    </div>
                            <?= $evento->descricao; ?>
                    </div>
                   
                    <div class="col-md-2 about-right wow fadeInRight animated" data-wow-delay=".5s" style="visibility: visible; -webkit-animation-delay: .5s;">               
                        <span class="row contato">               
                            <a class="areas seta" href="<?= $linkEvento; ?>">Comprar</a> 
                        </span>
                    </div>
                    <hr style="padding-top:25px;">
                </div>
            
            <?php } ?>
        <?php } ?>
        
    </div>	
</div>

<?php $this->registerJsFile(
    '@web/js/mineirao.js', [
        'depends' => [
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapAsset',
            'yii\bootstrap\BootstrapPluginAsset',
        ]
    ]
);
?>
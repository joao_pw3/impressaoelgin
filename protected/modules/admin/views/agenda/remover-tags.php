<?php
$this->title = 'ADM Easy for Pay';
?>

<div class="container">
	<div class="row">
		<h2 class="tituloGeral"><i class="fa fa-clone"></i> Remover Tags: <?=$evento->nome?></h2>
	</div>

	<div class="gridAgenda3Out">
		<div class="gridAgenda3 gridAgenda3Head">		
			<div>Nome</div>
			<div>Valor</div>
			<div>Ação</div>
		</div>
		<?php
		foreach ($tags->lista as $model) {?>
			<div class="gridAgenda3">
				<div><?=$model->nome?></div>
				<div><?=$model->valor?></div>
				<div><a href="<?=\yii\helpers\Url::to(['remover-tags', 'evento'=>$evento->codigo, 'nomeTag'=>$model->nome])?>" class="fontA botUnic">&#xf1f8;</a></div>
			</div>
		<?php }
		?>
	</div>
</div>
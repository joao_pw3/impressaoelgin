<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'ADM Estádio Mineirão :: Adicionar permissão';
?>
<div class="container row">
    <h2 class="tituloGeral"><i class="fa fa-clone"></i> Adicionar permissão</h2>
    <?php $form = ActiveForm::begin(); ?>
        <div class="form-group col-md-6">
            <?= $form->field($model, 'permissoesUsuario')->checkboxList($permissoes,['multi' => true]) ?>
        </div>

        <div class="form-group col-md-6">
            <?= $form->field($model, 'usuario')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="form-group col-md-12">
            <?= $form->field($model, 'inativo')->hiddenInput(['value'=>'0'])->label(false) ?>
            <?= Html::submitButton('Confirmar' , ['class'=>'btn btn-success']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
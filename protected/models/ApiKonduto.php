<?php
namespace app\models;
use yii\base\Exception;
use app\models\Api;

/**
 * Classe geral de acesso à API Konduto
 */

class ApiKonduto extends Api
{
    private $token = [
        //token Konduto
        'K' => YII_ENV_DEV ? 'T8CFF8B3EFE60D1EBDFBB' : 'PA82908FB2E2CC1A200BE'
    ];
    
    public $url = 'https://api.konduto.com/v1/';

    public function call($endpoint, $method, $data = null, $parse = true, $key = 'K') 
    {
        if (!$endpoint) {
            return false;
        }

        $api = new Api();

        $data = $parse ? $api->parse($data) : $data;
        $headers = [
            'Accept: application/json', 
            'Content-Type: application/json', 
            'Authorization: Basic ' . $this->token[$key]
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url . $endpoint);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if (!empty($data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if (!$result = curl_exec($ch)) {
            return curl_error($ch);
        } else {

        	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        	$message = $result;

        	$return = ['code' => $httpcode, 'message' => $message];

        	return $return;
        }
    }
}
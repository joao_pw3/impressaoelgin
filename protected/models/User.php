<?php

namespace app\models;
use \app\models\Api;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $user_token;
    public $tipo;
    public $nome_comprador;
    public $nome_vendedor;
    public $conta_usuario;

    public $cod_usuario;
    public $usuario;
    public $ativo;
    public $nome_cliente;
    public $cod_cliente;
    public $nome_empresa;
    public $cod_empresa;
    public $cod_operador;
    public $comprador;
    public $cartao;
    public $vendedor;
    public $operador;
    public $expirado;
    public $programa;
    public $programa_nome;

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $api=new Api;
        $auth=json_decode($api->call('auth/'.$token,'GET',null,false,'A'));
        if(isset($auth->successo) && $auth->successo==1){
            $obj=[
                'cod_usuario'=>$auth->objeto->cod_usuario,
                'usuario'=>$auth->objeto->usuario,
                'ativo'=>$auth->objeto->ativo,
                'nome_cliente'=>$auth->objeto->nome_cliente,
                'cod_cliente'=>$auth->objeto->cod_cliente,
                'nome_empresa'=>$auth->objeto->nome_empresa,
                'cod_empresa'=>$auth->objeto->cod_empresa,
                'cod_operador'=>$auth->objeto->cod_operador,
                'comprador'=>$auth->objeto->comprador,
                'cartao'=>$auth->objeto->cartao,
                'vendedor'=>$auth->objeto->vendedor,
                'operador'=>$auth->objeto->operador,
                'expirado'=>$auth->objeto->expirado,
                'programa'=>$auth->objeto->programa,
                'programa_nome'=>$auth->objeto->programa_nome
            ];
            return new static($obj);
        }
        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username,$password)
    {
        /*foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }*/
        $api=new Api;
        $auth=json_decode($api->call('auth','POST',['user_id'=>$username,'senha'=>$password],true,'A'));
        if($auth->successo==1)
            return new static($auth->objeto);
        if(isset($auth->erro))
            return $auth;
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->cod_usuario;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->user_token;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}

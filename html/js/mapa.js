var deltaX = 0;
var deltaY = 0;
var scale = 1.0;

var drag = {
    elem: null,
    x: 0,
    y: 0,
    state: false
};

var delta = {
    x: 0,
    y: 0
};

var svgMapa = $("#holder-mapa > svg");
var _svgWidth;
var _holderWidth;

function zoomMapaReset(){
	_svgWidth=svgMapa.width();
    _holderWidth=$("#holder-mapa").width();
    var pctMapaAtual=_svgWidth/_holderWidth*100;
    if ($("#zoomMapa").length > 0) {
        $("#zoomMapa").slider( "option", "min", pctMapaAtual );
    }
}

function zoomMapaInit(){
	$("#zoomMapa").slider({
	  slide: function( event, ui ) {
	  	mudarWidthMapa(ui.value);
	  	posicionarMapa();
	  }
	});

	//listenMouseDownMapa();
}

function posicionarMapa(){
	if(svgMapa.width()>svgMapa.parent().width())
		svgMapa.css({'left': '0','transform': 'translate(0, 0)'})
	if(svgMapa.width()<=svgMapa.parent().width())
		svgMapa.css({'left': '50%','transform': 'translate(-50%, 0)'})
}

function mudarWidthMapa(int){
	var intRound=Math.round(int);
	svgMapa.css({
		'height':'auto',
		'width':intRound+'%'
	})
	
}

function listenMouseDownMapa(){
	svgMapa.mousedown(function(e){
		if(e.target.tagName=='svg'){
			drag.elem = $('.room');
	        drag.x = e.pageX;
	        drag.y = e.pageY;
	        drag.state = true;
			listenMouseMoveMapa(e)
		}
	})

	svgMapa.mouseup(function() {
	    if (drag.state) {
	        drag.state = false;
	    }
	});

	$("#holder-mapa").mouseleave(function(){
		if (drag.state) {
	        drag.state = false;
	    }
	})
}

function listenMouseMoveMapa(e){
	svgMapa.mousemove(function(e) {
	    drag.elem=svgMapa
	    if (drag.state) {
	        delta.x = e.pageX - drag.x;
	        delta.y = e.pageY - drag.y;
	     
	        var cur_offset = $(drag.elem).offset();

	        $(drag.elem).offset({
	            left: (cur_offset.left + delta.x),
	            top: (cur_offset.top + delta.y)
	        });

	        drag.x = e.pageX;
	        drag.y = e.pageY;
	    }
	});
}

function adicionarOuvinteSlider(){
	svgMapa = $("#holder-mapa > svg");
	jQuery('#zoomMapa').slider( "option", "value", 0);
}

function acaoAposCarregarMapa(){
    adicionarOuvinteSlider();
	zoomMapaReset();
    marcarSelecionados();    
    adicionarOuvinteClickAssento();
}

$(document).ready(function(){
	$(window).resize(function(){
		zoomMapaReset();
	})
})
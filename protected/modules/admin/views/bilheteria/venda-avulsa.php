 <?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\modules\admin\assets\BilheteriaAsset;
use app\models\ApiProduto;
use yii\web\View;
use yii\helpers\Html;

BilheteriaAsset::register($this);

?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php $form=ActiveForm::begin(['options'=>['id'=>'venda-avulsa-produto-form']])?>
            <p><label>ID do produto <input type="text" name="produto" /></label></p>
            <p><button class="btn btn-primary" type="submit">Adicionar ao carrinho</button></p>
            <?php ActiveForm::end()?>
            <a class="btn btn-danger" href="<?=Url::to(['venda-avulsa','cancelar'=>1])?>">Cancelar operação</a>
            <hr>
            <p id="cappta-status"></p>
        </div>
        <div class="col-md-9">
            <?php if(isset($carrinho->objeto) && $carrinho->successo && is_array($carrinho->objeto->produtos)){?>
                <div id="retorno-carrinho">
                    <p>Produtos adicionados ao carrinho</p>
                    <ul>
                    <?php foreach ($carrinho->objeto->produtos as $produto) {
                        $tags=ApiProduto::tagsArray($produto->tags,1)?>
                        <li><strong>Evento: <?=$tags->matriz?></strong></li>
                        <li><?=$produto->nome?></li>
                    <?php }?>
                    </ul>
                    <p class="alert alert-info">Total a pagar: R$ <?=number_format($carrinho->objeto->totais->total,2,',','.')?></p>
                    <p>Confirme os dados acima e selecione a forma de pagamento</p>
                    <?php
                        $this->registerJs("
                            capptaInit();
                        ",View::POS_READY);?>
                </div>
                <div id="cappta">
                    <p>Forma de pagamento</p>
                    <?php ActiveForm::begin(['options'=>['id'=>'venda-avulsa-checkout-form']]);
                    echo Html::radioList('tipo', '1',['debito' => 'Débito','credito' => 'Crédito']);
                    echo Html::dropDownList('parcelas',null,$parcelas,['class'=>'hidden']);
                    echo Html::hiddenInput('total',number_format($carrinho->objeto->totais->total,2,'.',''));
                    echo Html::button('CONFIRMAR',['class'=>'btn btn-primary','type'=>'submit']);
                    ActiveForm::end()?>
                </div>
            <?php } ?>
            <?php if($erro) {?>
                <p class="alert alert-warning"><?=$erro->mensagem?></p>
            <?php }?>
        </div>
    </div>
</div>
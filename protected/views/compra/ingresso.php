<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Ocupante;
use app\assets\IngressoAsset;
use app\widgets\Ingresso;
use app\models\Taxa;
use yii\bootstrap\Modal;

IngressoAsset::register($this);

$this->title = 'Ocupantes';
if (isset($carrinho->objeto->produtos) && !empty($carrinho->objeto->produtos)) { 
    $this->params['breadcrumbs'][] = ['label' => 'Mapa de Assentos', 'url' => ['/agenda', 'evento' => $evento]];
    $this->params['breadcrumbs'][] = $this->title;
    ?>    
    <div class="container">
            <input type="hidden" id="url-ocupante" value="<?= Url::to(['compra/salvar-ocupante']); ?>">
            <input type="hidden" id="url-pagar" value="<?= Url::to(['compra/pagamento']); ?>">
            <input type="hidden" id="url-del-produto" value="<?= Url::to(['compra/remover-produto-carrinho']); ?>">
            <input type="hidden" id="url-cupom" value="<?= Url::to(['compra/adicionar-cupom-carrinho']); ?>">
            <input type="hidden" id="url-evento" value="<?= Url::to(['/agenda','evento'=>isset($evento) ?$evento :'']); ?>">
            <input type="hidden" id="evento" value="<?= $evento; ?>">
            <input type="hidden" id="data_evento" value="<?= $data; ?>">
			<?php
            $tituloEvento = '';
            $form = ActiveForm::begin(['id' => 'form-ingresso']);
            foreach ($listaProdutos as $indexForm=>$_produto) {
                $produto=$_produto['objeto'];
                $objCupom->buscarCupom([
                    'id' => 'PUB_' . $produto->agenda->codigo . '_' . $produto->agenda->id_data_agenda . '_%',
                    'ativo' => '1'
                ]);
                if(isset($objCupom->successo) && $objCupom->successo)
                    $cupons=$objCupom->lista;
                
                if ($tituloEvento != $produto->agenda->nome . ' - ' . $produto->agenda->horario) { ?>
					<br>
                    <div class="row tarjaCz">
                        <?= $produto->agenda->nome; ?> - <?= $produto->agenda->horario; ?>
                    </div>
					<br>
                    <!-- <div class="row">
                        <div class="col-xs-2 tituloOcup" align="center">Ingressos<hr></div>
						<div class="col-xs-8 tituloOcup" align="center">Ocupantes<hr></div>                        
                        <div class="col-xs-2 tituloOcup" align="center">Produtos<hr></div>
                    </div> -->
                    <?php $tituloEvento = $produto->agenda->nome . ' - ' . $produto->agenda->horario;
                } ?>
                <div class="row telaOcupantes" id="row-<?= $produto->codigo.'-'.$indexForm; ?>">
                    
					<div class="col-xs-2 exibirValoresIngressos" style="padding-right:6px;">
                        <?php
                        echo Ingresso::getSelectTipo($_produto['objeto'], $objCupom->lista, $indexForm, $produto->valor_liquido-$produto->valor_desconto); ?>
                        <span id="taxa-<?=$produto->codigo.'-'.$indexForm?>">
                            <small style="line-height:14px; display:block; margin-top:5px; position:relative; margin-left:5px;">
                                Ingresso R$ <?=number_format($produto->valor_desconto,2,',','.')?>
								<div class="informaPos">Apresentar documento de identidade com foto.</div>
                            </small>
                        </span>
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-cupom" data-produtoselect="ingresso-<?=$produto->codigo.'-'.$indexForm?>" data-idevento="<?=$produto->agenda->codigo?>" data-iddata="<?=$produto->agenda_data?>">
                        Tem um cupom?
                        </button>
                    </div>
					
					<div class="col-xs-2">
                        <?= $form->field($ocupante, "nome[$indexForm]")->textInput([
                            'id' => 'nome-' . $_produto['codigo'],
                            'value' => $_produto['nome'],
                            'class' => 'nomeOc form-control',
                            'autocomplete' => 'name'
                        ])->label('Nome*'); ?>
                    </div>
                    <div class="col-xs-2">
                        <?= $form->field($ocupante, "documento[$indexForm]")->textInput([
                            'id' => 'documento-' . $_produto['codigo'],
                            'value' => $_produto['documento'],
                            'class' => 'docOc form-control',
                        ])->label('Documento*'); ?>
                    </div>
                    <div class="col-xs-2">
                        <?= $form->field($ocupante, "email[$indexForm]")->textInput([
                            'id' => 'email-' . $_produto['codigo'],
                            'value' => $_produto['email'],
                            'autocomplete' => 'email'
                        ])->label('E-mail (opcional)'); ?>
                    </div>
                    <div class="col-xs-2">
                        <?= $form->field($ocupante, "telefone[$indexForm]")->textInput([
                            'id' => 'telefone-' . $_produto['codigo'],
                            'value' => $_produto['telefone'],
                            'autocomplete' => 'tel'
                        ])->label('Telefone (opcional)'); ?>
                    </div>
                    
                    <div class="col-xs-2">
                        <?php
                        $tagsProduto=[];
                        $cupom=isset($_produto['objeto']->cupom) ?$_produto['objeto']->cupom->id :'inteira';
                        if(isset($_produto['objeto']->tags)) {
                            $tagsProduto=$objTags->objetoTags($_produto['objeto']->tags);
                            if(isset($tagsProduto->fileira,$tagsProduto->assento)){?>
                            <div class="div-circle2 div-ingresso2">
                                <span title="Fileira <?= $tagsProduto->fileira; ?> - Assento <?= $tagsProduto->assento; ?>"><?= $tagsProduto->fileira . '-' . $tagsProduto->assento; ?></span>
                            </div>
                            <?php }?>
                        <?php }?>
                        <?php if (isset($tagsProduto,$tagsProduto->especial)) { ?>
                        <div class="div-especial2">
                            <span class="font-awesome" title="<?= $tagsProduto->especial == 'cadeirante' ? 'Cadeirante' : ($tagsProduto->especial == 'mobilidade' ? 'Mobilidade reduzida' : ($tagsProduto->especial == 'obeso' ? 'Obeso' : 'Acompanhante')); ?>">
                                <?= $tagsProduto->especial == 'cadeirante' ? '&#xf193;' : ($tagsProduto->especial == 'mobilidade' ? '&#xf29d' : ($tagsProduto->especial == 'obeso' ? '&#xf234;' : '&#xf067')); ?>
                            </span>
                        </div>
                        <?php } ?>
                        <div>
                            <span class="font-awesome corCz2 del-carrinho" data-indice="<?= $_produto['indexForm']?>"  data-produto="<?= $produto->codigo?>" id="btn-del-<?= $produto->codigo.'-'.$indexForm; ?>" title="Remover do carrinho">&#xf1f8;</span>
                        </div>
                    </div>
                    <div>
                        <?= $form->field($ocupante, "produto_codigo[$indexForm]")->hiddenInput(['value' => $produto->codigo])->label(false); ?>
                        <?php
                        if(isset($taxa->id))
                            echo $form->field($ocupante, "taxa[$indexForm]")->hiddenInput(['value' => $taxa->id])->label(false); ?>
                    </div>
                </div>
            <?php   
            } ?>
            <hr>
            <div class="row">			
                <?php if($ocupante->hasErrors('produto_codigo')) {?>
                    <div class="col-xs-12">
                        <p class="alert alert-warning"><?php echo $ocupante->getErrors('produto_codigo')[0]; ?></p>
                    </div>
                <?php }?>
                <div class="col-xs-6 text-right">
                    <input type="button" class="carrinho seta2" id="btn-continuar-compra" value="Continuar comprando">
                </div>
                <div class="col-xs-6 text-left">
                    <input type="submit" class="carrinho seta3" value="Pagar" id="btn-pagar">
                </div>
            </div>
            <?php
            ActiveForm::end();
            ?>
    </div>
<?php
} else { ?>
    <div class="container">
        <p>Seu carrinho de compras expirou</p>
        <p><?= Html::a('Fazer nova compra', Url::to(['/'])); ?></p>
    </div>    
<?php } ?>


<!-- Modal - Imagens -->
<?php Modal::begin([
    'size'   => 'modal-sm',
    'id'     => 'modal-cupom',
    'header' => '<h4 align="center">Cupom</h4>',
    'options'=> [
        'class' => 'modal-center fade'
    ],
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ],
    'clientEvents'  => [
        'hidden.bs.modal' => 'function(e){
            $("#input-cupom").val("");
            $("#consulta-cupom").button("reset");
            $("#consulta-cupom-retorno").text("");
            $("#consulta-cupom-retorno").addClass("hidden");
        }',
        'show.bs.modal' => 'function(e){btnModalOpen=e.relatedTarget}'
    ]
]);?>
<div class="form-group">
    <label class="control-label" for="input-cupom">Código do seu cupom.*</label>
    <input type="text" class="form-control" name="cupom" id="input-cupom">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
    <button type="button" class="btn btn-primary" id="consulta-cupom" data-loadingtext="Validando...">OK</button>
    <br>
    <p class="hidden alert alert-warning" id="consulta-cupom-retorno"></p>
</div>
<?php Modal::end(); ?>

<?php $this->registerJs('
    urlCalcularTaxa="'.Url::to('mostrar-taxa').'";
    urlValidarCupom="'.Url::to('validar-cupom').'";'
,\yii\web\View::POS_END) ?>


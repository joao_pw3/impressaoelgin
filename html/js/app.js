// JavaScript Document
$(document).ready(function () {

    /*
     EXIBIR MENU MOBILE
     ------------------------------*/
    $('.bt_menu').click(function () {
        $(".menu_mobile").slideDown("medium");
    });

    $(".menu_mobile .close").click(function () {
        $('.menu_mobile').slideUp("medium");
    });

    $(".menu_mobile .mais").click(function () {
        $(this).hide();
        $(this).next().show();
        $(this).next().next().show('medium');
        //$(this).find("ul").show('mediu');
    });
    $(".menu_mobile .menos").click(function () {
        $(this).hide();
        $(this).prev().show();
        $(this).next("ul").hide('medium');
    });


    /*
     EXIBIR BUSCA
     ------------------------------*/
    $('.icon-search').click(function () {
        $(".busca").slideDown("medium");
    });
    $(".busca").mouseleave(function () {
        $(this).slideUp("fast");
    });


    /*
     PAGINAÇÃO
     ------------------------------*/
    $('#pagination').on('click', 'a', function (e) {
        e.preventDefault();
        var links = $(this).attr('href');
        $('#pagination .load').fadeIn('medium');
        $.post(links, function (data) {
            var content = $(data).find('#loop').contents();
            var pagination = $(data).find('#pagination');
            $('#pagination').html('');
            $('#pagination').html(pagination.html());
            $('#loop').append(content);
            // CARREGA SCRIPT FACEBBOK APOS LOAD
            FB.XFBML.parse();
            $(document).foundation('equalizer', 'reflow');
        });
    });


    $('.socket').addClass('animated fadeOut');
    setTimeout(function () {
        $('#preloader').addClass('animated fadeOutDown').fadeOut(500);
        new WOW().init();
    }, 300);
});


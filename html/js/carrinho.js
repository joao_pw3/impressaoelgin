$(document).ready(function () {
    $(document).on('hidden.bs.modal', '.carrinho-tempo', function (e) {
        e.preventDefault();
        window.location.href = $('#url-index').val();
    });    
});

function getTimeRemaining(endtime) {
    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    return {
        'total': t,
        'minutes': minutes,
        'seconds': seconds
    };
}

function initializeClock() {
    var limite = false;
    $('#relogio-carrinho').removeClass('hidden');
    function updateClock() {
        var t = getTimeRemaining(deadline);
        if (!limite) {
            $('#clockdiv .minutes').html(('0' + t.minutes).slice(-2));
            $('#clockdiv .seconds').html(('0' + t.seconds).slice(-2));
        }
        if ((t.minutes == 0 && t.seconds == 0) || t.minutes < 0 || t.seconds < 0) {
            showMsg('O tempo para efetuar sua compra terminou. Por favor, comece novamente.');
            $('#modal-mensagem').addClass('carrinho-tempo');
            limite = true;
        } 
    }
    updateClock();
    var timeinterval = setInterval(updateClock, 1000);
}
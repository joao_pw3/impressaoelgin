<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\ApiAgenda;
use app\models\ApiProduto;
use SimpleXMLElement;

class MapaController extends Controller 
{
    
    /**
     * Geração automática de mapas dos assentos do Mineirão
     * @param type $id Id do evento
     * @param type $area Área do estádio
     * @param type $bloco Bloco do estádio
     * @return string
     */
    public function actionMapa ($id='', $area='', $bloco='') 
    {
        try {
            $this->layout = false;
            $produtos = [];
            $arqMapa = '';
            $mapa = '';
            if (!empty($id) && !empty($area) && !empty($bloco)) {
                $evento = (new ApiAgenda)->buscarEventoTags(null, $id);
                $produtos = (new ApiProduto)->buscarProdutoTags(json_encode(['tags' => 
                    [
                        ['nome' => 'tipo', 'valor' => 'assento'],
                        ['nome' => 'matriz', 'valor' => $evento->objeto[0]->tags['apelido']],
                        ['nome' => 'area', 'valor' => $area],
                        ['nome' => 'bloco', 'valor' => $bloco]
                    ]
                ]), NULL, false);
                if (is_file(Yii::getAlias('@webroot') . '/maps/bloco_' . $bloco . '.html')) {
                    $arqMapa = file_get_contents((YII_ENV_DEV ? 'http://localhost/mineirao/html' : 'http://vendas.estadiomineirao.com.br') . '/maps/bloco_' . $bloco . '.html');
                    $mapa = new SimpleXMLElement($arqMapa);
                    $mapa->registerXPathNamespace('svg', 'http://www.w3.org/2000/svg');
                } else {
                    die('Não há arquivo para o mapa solicitado.');
                }
            }
            if (!empty($produtos)) {
                $arrStatus = ['fillGreen' => 1, 'fillRed' => 2, 'fillYellow' => 3, 'fillOrange' => 4, 'fillGray' => 5];
                foreach($produtos->objeto as $index => $produto) {
                    $nomeTmp = explode(' ', $produto->nome);
                    $id = $nomeTmp[3] . $nomeTmp[1];
                    $buscaXpath = '//svg:g[@id="' . $id . '"]';
                    $busca = $mapa->xpath($buscaXpath);
                    
                    if (!empty($busca)){
                        $isReserva = $produto->estoque->reserva_exclusiva != '' ? $produto->estoque->reserva_exclusiva : '';
                        $corAssento = ((int)$produto->estoque->disponivel == 0 && (int)$produto->estoque->vendido > 0 ? 'fillRed' : 
                            ((int)$produto->ativo == '0' ? 'fillGray' : 
                            ((int)$produto->estoque->disponivel == 0 && (int)$produto->estoqu->reservado > 0 ? 'fillYellow' : 
                            ($isReserva != '' ? 'fillOrange' : 'fillGreen'))));
                        $corTexto = $corAssento == 'fillGray' || $corAssento == 'fillGreen' ? 'fntBlack' : 'fntBlack';
                        $strTitle = $corAssento == 'fillRed' ? 'Produto vendido' : 
                                ($corAssento == 'fillGray' ? 'Assento bloqueado' : 
                                ($corAssento == 'fillYellow' ? 'Produto reservado (carrinho)' : 
                                ($corAssento == 'fillOrange' ? 'Produto reservado (cliente)' : 'Produto disponível')));
                        $busca[0]->a['id'] = $produto->codigo;
                        $busca[0]->a['href'] = '#';
                        $busca[0]->a['class'] = 'reserva-assento'; 
                        $busca[0]->a['onclick'] = 'assento(' . $produto->codigo . ', ' . $arrStatus[$corAssento] . ', "' . $isReserva . '");'; 
                        $busca[0]->a['data-ativo'] = $produto->ativo;
                        $busca[0]->a['data-disponivel'] = $produto->estoque->disponivel;
                        $busca[0]->a['data-oferecido'] = $produto->estoque->oferecido;
                        $busca[0]->a['data-reservado'] = $produto->estoque->reservado;
                        $busca[0]->a['data-vendido'] = $produto->estoque->vendido;
                        $busca[0]->a['data-produto'] = $produto->tags['fileira'] . $produto->tags['cadeira'];
                        $busca[0]->a['data-res-exclusiva'] = $isReserva;
                        $busca[0]->a->circle['cx'] = $busca[0]->circle['cx'];
                        $busca[0]->a->circle['cy'] = $busca[0]->circle['cy'];
                        $busca[0]->a->circle['r'] = $busca[0]->circle['r'];
                        $busca[0]->a->circle['class'] = str_replace('fillWhite', $corAssento, $busca[0]->circle['class']);
                        $busca[0]->a->title['id'] = 'title-' . $id;
                        $busca[0]->a->title = $strTitle;
                        $busca[0]->a->text['x'] = (int)$busca[0]->text['x'] + 2;
                        $busca[0]->a->text['y'] = (int)$busca[0]->text['y'] - 2;
                        $busca[0]->a->text['class'] = $corTexto;
                        $busca[0]->a->text = $busca[0]->text;
                        unset($busca[0]->circle);
                        unset($busca[0]->text); 
                    }
                }
                echo "
                <script type='text/javascript'>
                function assento(id, disponibilidade, cpfReserva) {
                    //iOS
                    try{
                            webkit.messageHandlers.assento.postMessage(id, disponibilidade, cpfReserva);
                    }catch(err){}

                    //Android
                    try{
                            Android.assento(id, disponibilidade, cpfReserva);
                    }catch(err){}
                }
                </script>";
                echo $mapa->asXML();
                die;
            }
            echo 'Não há informações para gerar o mapa.';
        } catch (Exception $e) {
            return $this->render('error', ['name' => 'Erro ao fazer mapa', 'message' => $e->getMessage()]);
        }
    }
}

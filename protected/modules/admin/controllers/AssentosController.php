<?php
namespace app\modules\admin\controllers;

use Yii;
use yii\web\Response;
use app\models\EventosModel;
use app\models\CompraModel;
use app\models\ClienteModel;
use app\models\CartaoModel;
use app\models\ApiCliente;
use app\models\ApiProduto;
use app\models\ApiCartao;
use app\models\ApiCarrinho;
use yii\data\ArrayDataProvider;
use app\models\Logs;
use app\models\Renovacao;
use app\models\Transacoes;
use app\modules\admin\controllers\AbstractController;
use yii\web\Controller;

class AssentosController extends Controller
{
    public $logs;

    public function actionIndex()
    {
        return $this->render('/default/index');
    }

    public function setLog($logName) 
    {
        $this->logs = new Logs($logName, Yii::getAlias('@app') . '/logs/');
    }
   /** 
     * Método auxiliar para remover anexos
     */
    public function actionRemoveranexos(){
        //retornar todos os blocos
        $model=new ApiProduto();
        $blocos=$model
            ->buscarProduto(json_encode(
                [
                    "tags"=>[
                        ["nome"=>"tipo","valor"=>"bloco"],
                    ],
                ]));
        // retirar os anexos
        foreach ($blocos->objeto as $bloco) {
            $model
                ->excluirAnexoProduto([
                        'codigo'=>$bloco->codigo,
                        'nome'=>'svgmapa',
                        'tipo'=>'txt'
                    ]
                );
        }
    }

    /**
     * Método auxiliar para listar os blocos 
     */
    public function actionAnexoblocosoeste(){
        //lista de svg
        $svg = [
            '101'=>'<path class="bloco" id="bloco_101" d="M350 212c20,9 40,17 60,25 7,3 14,5 20,8l25 -53 81 -172c-17,-7 -34,-14 -51,-21l-169 151 34 60z"/>',
            '102'=>'<path class="bloco" id="bloco_102" d="M536 21l-81 172c11,3 22,6 33,9 15,4 30,8 46,11 15,4 31,7 46,11 10,2 20,5 30,7l61 -164c-46,-14 -90,-29 -134,-45z"/>',
            '103'=>'<path class="bloco" id="bloco_103" d="M670 66l-61 164 16 4c15,3 31,7 46,10 15,3 31,6 46,9 15,3 31,6 46,8 12,2 23,4 35,6l40 -159c-58,-12 -115,-26 -170,-42z"/>',
            '104'=>'<path class="bloco" id="bloco_104" d="M840 108l-40 159 12 2c16,2 31,5 47,7 16,2 31,4 47,6 16,2 32,4 47,6 16,2 31,3 47,5l21 -157c-62,-7 -122,-16 -181,-28z"/>',
            '105'=>'<path class="bloco" id="bloco_105" d="M1000 293l0 0c49,5 100,8 154,9l7 -157c-47,-2 -94,-5 -140,-10l-21 157z"/>',
            '106'=>'<path class="bloco" id="bloco_106" d="M1161 146l-7 157c62,3 120,2 172,0l-7 -156c-15,1 -30,1 -45,1l0 51 -33 0 -2 0 -33 0 0 -51c-15,0 -30,-1 -45,-1z"/>',
            '107'=>'<path class="bloco" id="bloco_107" d="M1319 146l7 156c56,-2 107,-5 154,-10l-21 -156c-46,5 -93,8 -140,10z"/>',
            '108'=>'<path class="bloco" id="bloco_108" d="M1574 282c16,-2 31,-4 47,-6 16,-2 31,-5 47,-7 4,-1 8,-1 12,-2l-40 -159c-59,12 -119,21 -181,28l21 157c16,-1 31,-3 47,-5 16,-2 32,-4 47,-6z"/>',
            '109'=>'<path class="bloco" id="bloco_109" d="M1640 108l40 159c12,-2 23,-4 35,-6 16,-3 31,-6 47,-8 16,-3 31,-6 46,-9 21,-4 42,-9 63,-13l-61 -164c-55,16 -112,30 -170,42z"/>',
            '110'=>'<path class="bloco" id="bloco_110" d="M1810 66l61 164c10,-2 20,-4 30,-7 31,-7 61,-15 92,-22 11,-3 22,-6 33,-9l-81 -172c-43,17 -88,32 -134,45z"/>',
            '111'=>'<path class="bloco" id="bloco_111" d="M1943 21l81 172 25 52c7,-3 14,-5 21,-8 20,-8 40,-16 60,-25l34 -60 -169 -151c-17,7 -34,14 -51,21z"/>',
            'vip105'=>'<path class="bloco" id="bloco_vip105" d="M1000 293l-12 91c13,2 25,3 38,4 13,1 27,3 40,4 12,1 25,2 37,3 12,1 23,1 35,2 0,0 0,0 0,0l11 0c0,0 0,0 0,0l4 -95c-53,-1 -104,-4 -154,-10z"/>',
            'vip106'=>'<path class="bloco" id="bloco_vip106" d="M1153 304l-4 93c6,0 13,1 19,1 23,1 47,1 70,1 1,0 1,0 2,0 24,0 47,0 70,-1 6,0 13,0 19,-1l-4 -93c-59,3 -117,3 -173,0z"/>',
            'vip107'=>'<path class="bloco" id="bloco_vip107" d="M1326 303l4 95c28,-1 56,-3 83,-5 13,-1 27,-2 40,-4 13,-1 25,-3 38,-4l-12 -92 0 -1c-52,6 -103,9 -154,10z"/>',
            'vip201'=>'<path class="bloco" id="bloco_vip201" d="M1149 398c-4,0 -7,0 -11,-1 4,0 7,0 11,1zm-83 -5l-8 84c35,3 71,5 107,7l3 -84c-6,0 -13,0 -19,-1 -4,0 -7,0 -11,-1 -24,-1 -48,-3 -72,-5z"/>',
            'vip202'=>'<path class="bloco" id="bloco_vip202" d="M1240 484l0 0zm1 0l0 0zm0 -84c-1,0 -1,0 -2,0 1,0 1,0 2,0zm-72 -1l-3 84c25,1 49,1 74,1l0 0 0 0c25,0 49,0 73,-1l-3 -84c-23,1 -47,1 -70,1l-2 0c-24,0 -47,0 -70,-1z"/>',
            'vip203'=>'<path class="bloco" id="bloco_vip203" d="M1311 398l3 84c36,-1 72,-3 107,-7l-8 -84c-28,2 -56,4 -83,5 -6,0 -13,1 -19,1z"/>',
        ];
        
        //retornar todos os blocos
        $model=new ApiProduto();
        $blocos=$model
            ->buscarProduto(json_encode(
                [
                    "tags"=>[
                        ["nome"=>"tipo","valor"=>"bloco"],
                    ],
                ]));
        //fazer o for de cada bloco fazendo um switch com tag do nome do bloco e associando o path do svg
        foreach ($blocos->objeto as $bloco) {
            foreach($bloco->tags as $tag){
                if($tag->nome=='bloco' && array_key_exists($tag->valor, $svg)){
                    //salvar o anexo
                    /*$model
                        ->adicionarAnexoProduto([
                            'codigo'=>$bloco->codigo,
                            'nome'=>'svgmapa',
                            'tipo'=>'txt',
                            'objeto'=>base64_encode($svg[$tag->valor])
                        ]);*/
                }
            }          
        }
    }

    /**
     * Método auxiliar para criar anexos
    */
    public function actionAnexocamarotesoeste(){
        $svg = [
            'C101'=>'<path class="camarote" id="camarote_C101" d="M463 258l25 -56c-11,-3 -22,-6 -33,-9l-25 53c5,2 10,4 16,6 6,2 12,4 17,7z"/>',
            'C102'=>'<path class="camarote" id="camarote_C102" d="M508 274l26 -61c-15,-4 -31,-8 -46,-11l-25 56 0 0 19 7c0,0 0,0 0,0 9,3 17,6 26,9z"/>',
            'C103'=>'<path class="camarote" id="camarote_C103" d="M554 289l26 -66c-15,-4 -31,-7 -46,-11l-26 61 11 4c12,4 23,8 35,12z"/>',
            'C104'=>'<path class="camarote" id="camarote_C104" d="M600 304l26 -70c-5,-1 -11,-2 -16,-4 -10,-2 -20,-4 -30,-7l-26 66 2 1c12,4 25,8 37,12l7 2z"/>',
            'C105'=>'<path class="camarote" id="camarote_C105" d="M646 317l25 -74c-15,-3 -31,-6 -46,-10l-26 70c10,3 21,6 31,9l15 4 0 0z"/>',
            'C106'=>'<path class="camarote" id="camarote_C106" d="M693 330l25 -77c-16,-3 -31,-6 -46,-9l-25 74c8,2 15,4 23,6 8,2 16,4 24,6z"/>',
            'C107'=>'<path class="camarote" id="camarote_C107" d="M741 341l24 -80c-16,-3 -31,-6 -46,-8l-25 77 14 4c11,3 22,5 33,8z"/>',
            'C108'=>'<path class="camarote" id="camarote_C108" d="M789 352l23 -83c-4,-1 -8,-1 -12,-2 -12,-2 -23,-4 -35,-6l-24 80 5 1c13,3 26,6 39,9l3 1z"/>',
            'C109'=>'<path class="camarote" id="camarote_C109" d="M837 361l21 -86c-16,-2 -31,-5 -47,-7l-23 83c12,3 24,5 36,7l12 2z"/>',
            'C110'=>'<path class="camarote" id="camarote_C110" d="M886 370l20 -88c-16,-2 -31,-4 -47,-6l-21 86c9,2 18,3 28,5 7,1 14,2 21,4z"/>',
            'C111'=>'<path class="camarote" id="camarote_C111" d="M934 378l19 -90c-16,-2 -32,-4 -47,-6l-20 88 19 3c10,2 20,3 30,4z"/>',
            'C112'=>'<path class="camarote" id="camarote_C112" d="M987 384l12 -92c-16,-1 -31,-3 -47,-5l-19 90 11 2c14,2 28,4 42,5z"/>',
            'C113'=>'<path class="camarote" id="camarote_C113" d="M1480 293l12 92c14,-2 28,-3 42,-5 4,0 7,-1 11,-1l-18 -90c-16,2 -31,3 -47,5z"/>',
            'C114'=>'<path class="camarote" id="camarote_C114" d="M1594 370l-20 -88c-16,2 -32,4 -47,6l18 90c10,-1 20,-3 30,-4l19 -3 0 0c0,0 0,0 0,0z"/>',
            'C115'=>'<path class="camarote" id="camarote_C115" d="M1643 361l-21 -86c-16,2 -31,4 -47,6l20 88c7,-1 14,-2 21,-4 9,-2 18,-3 28,-5z"/>',
            'C116'=>'<path class="camarote" id="camarote_C116" d="M1621 276l21 86c4,-1 8,-2 12,-2 12,-2 24,-5 36,-7l-23 -83c-16,2 -31,5 -47,7z"/>',
            'C117'=>'<path class="camarote" id="camarote_C117" d="M1715 261c-12,2 -23,4 -35,6 -4,1 -8,1 -12,2l23 83c1,0 2,0 3,-1 13,-3 26,-6 39,-9 2,0 4,-1 5,-1l-24 -80z"/>',
            'C118'=>'<path class="camarote" id="camarote_C118" d="M1715 261l24 80c11,-3 22,-5 33,-8 5,-1 9,-2 14,-4l-25 -77c-15,3 -31,6 -47,8z"/>',
            'C119'=>'<path class="camarote" id="camarote_C119" d="M1761 253l25 77c8,-2 16,-4 24,-6 8,-2 15,-4 23,-6l-25 -73c-15,3 -31,6 -46,9z"/>',
            'C120'=>'<path class="camarote" id="camarote_C120" d="M1854 234c-15,3 -31,7 -46,10l25 73c5,-1 10,-3 15,-4 10,-3 21,-6 31,-9l-26 -70z"/>',
            'C121'=>'<path class="camarote" id="camarote_C121" d="M1854 234l26 70c2,-1 4,-1 7,-2 13,-4 25,-8 37,-12 1,0 1,0 2,-1l-26 -66c-10,2 -20,5 -30,7 -5,1 -11,2 -16,4z"/>',
            'C122'=>'<path class="camarote" id="camarote_C122" d="M1971 274l-26 -61c-15,4 -30,7 -46,11l26 66c12,-4 23,-8 35,-12 4,-1 7,-2 11,-4z"/>',
            'C123'=>'<path class="camarote" id="camarote_C123" d="M2017 257l-25 -56c-15,4 -30,8 -46,11l26 61c9,-3 17,-6 26,-9 6,-2 13,-5 19,-7z"/>',
            'C124'=>'<path class="camarote" id="camarote_C124" d="M2024 193c-11,3 -22,6 -33,9l25 56c6,-2 12,-4 17,-7 5,-2 10,-4 15,-6l-25 -52z"/>',
            'C201'=>'<path class="camarote" id="camarote_C201" d="M384 290l26 -53c-20,-8 -40,-16 -60,-25l-24 47c19,11 39,21 58,31z"/>',
            'C202'=>'<path class="camarote" id="camarote_C202" d="M445 251c-5,-2 -10,-4 -15,-6l0 0c-7,-3 -14,-5 -20,-8l-26 53c12,6 24,12 36,18l26 -57c0,0 0,0 0,0z"/>',
            'C203'=>'<path class="camarote" id="camarote_C203" d="M446 251l-26 57c12,6 24,11 37,17l26 -60c-6,-2 -13,-5 -19,-7l0 0 0 0 -17 -7 0 0c0,0 0,0 0,0z"/>',
            'C204'=>'<path class="camarote" id="camarote_C204" d="M482 264l-26 60c12,5 25,11 37,16l25 -63c-4,-1 -7,-2 -11,-4 -9,-3 -17,-6 -26,-9z"/>',
            'C205'=>'<path class="camarote" id="camarote_C205" d="M519 277l-25 63c12,5 25,10 38,15l25 -66c-1,0 -1,0 -2,-1 -12,-4 -23,-8 -35,-12z"/>',
            'C206'=>'<path class="camarote" id="camarote_C206" d="M556 290l-25 66c13,5 25,10 38,14l24 -68c-13,-4 -25,-8 -37,-12z"/>',
            'C207'=>'<path class="camarote" id="camarote_C207" d="M593 302l-24 68c13,5 26,9 39,14l23 -70c-10,-3 -21,-6 -31,-9 -2,-1 -4,-1 -7,-2z"/>',
            'C208'=>'<path class="camarote" id="camarote_C208" d="M631 313l-23 70c13,4 26,9 39,13l22 -73c-8,-2 -15,-4 -23,-6 -5,-1 -10,-3 -15,-4z"/>',
            'C209'=>'<path class="camarote" id="camarote_C209" d="M669 323l-22 73c13,4 26,8 40,12l21 -75c-5,-1 -9,-2 -14,-4 -8,-2 -16,-4 -24,-6z"/>',
            'C210'=>'<path class="camarote" id="camarote_C210" d="M707 333l-21 75c13,4 27,7 40,11l19 -76c-2,0 -4,-1 -5,-1 -11,-3 -22,-5 -33,-8z"/>',
            'C211'=>'<path class="camarote" id="camarote_C211" d="M746 342l-19 76c13,4 27,7 41,10l18 -78c-13,-3 -26,-6 -39,-9z"/>',
            'C212'=>'<path class="camarote" id="camarote_C212" d="M785 351l-18 78c14,3 27,6 41,9l16 -79c-12,-2 -24,-5 -36,-7 -1,0 -2,0 -3,-1z"/>',
            'C213'=>'<path class="camarote" id="camarote_C213" d="M825 359l-16 79c14,3 28,6 41,9l15 -80c-9,-2 -18,-3 -28,-5 -4,-1 -8,-2 -12,-2z"/>',
            'C214'=>'<path class="camarote" id="camarote_C214" d="M864 366l-15 80c14,3 28,5 42,8l13 -81c-6,-1 -13,-2 -19,-3 -7,-1 -14,-2 -21,-4z"/>',
            'C215'=>'<path class="camarote" id="camarote_C215" d="M905 373l-13 81c14,2 28,5 42,7l11 -82c-4,0 -7,-1 -11,-2 -10,-1 -20,-3 -30,-4z"/>',
            'C216'=>'<path class="camarote" id="camarote_C216" d="M976 467l11 -83c-14,-2 -28,-3 -42,-5l-11 82c14,2 28,4 42,6z"/>',
            'C217'=>'<path class="camarote" id="camarote_C217" d="M1015 472l10 -83c-13,-1 -25,-3 -38,-4l-11 83c13,2 26,3 39,5z"/>',
            'C218'=>'<path class="camarote" id="camarote_C218" d="M1058 476l8 -84c-13,-1 -27,-2 -40,-4l-10 83c14,2 28,3 43,4z"/>',
            'C219'=>'<path class="camarote" id="camarote_C219" d="M1414 392l8 84c14,-1 29,-3 43,-4l-10 -83c-13,1 -27,3 -40,4z"/>',
            'C220'=>'<path class="camarote" id="camarote_C220" d="M1454 389l10 83c13,-1 26,-3 39,-5l-11 -83c-13,2 -25,3 -38,4z"/>',
            'C221'=>'<path class="camarote" id="camarote_C221" d="M1492 384l11 83c14,-2 28,-4 42,-6l-11 -82c-14,2 -28,4 -42,5z"/>',
            'C222'=>'<path class="camarote" id="camarote_C222" d="M1535 379l11 82c14,-2 28,-4 42,-7l-13 -81c-10,2 -20,3 -30,4 -4,1 -7,1 -11,1z"/>',
            'C223'=>'<path class="camarote" id="camarote_C223" d="M1575 373l13 81c14,-2 28,-5 42,-8l-15 -80c-7,1 -14,2 -21,4 -6,1 -13,2 -19,3z"/>',
            'C224'=>'<path class="camarote" id="camarote_C224" d="M1615 366l15 80c14,-3 28,-6 41,-8l-16 -79c-4,1 -8,2 -12,2 -9,2 -18,3 -28,5z"/>',
            'C225'=>'<path class="camarote" id="camarote_C225" d="M1655 359l16 79c14,-3 27,-6 41,-9l-18 -78c-1,0 -2,0 -3,1 -12,3 -24,5 -36,7z"/>',
            'C226'=>'<path class="camarote" id="camarote_C226" d="M1694 351l18 78c14,-3 27,-7 41,-10l-19 -76c-13,3 -26,6 -39,9z"/>',
            'C227'=>'<path class="camarote" id="camarote_C227" d="M1733 342l19 76c13,-4 27,-7 40,-11l-21 -75c-11,3 -22,5 -33,8 -2,0 -4,1 -5,1z"/>',
            'C228'=>'<path class="camarote" id="camarote_C228" d="M1772 333l21 75c13,-4 27,-8 40,-12l-22 -73c-8,2 -16,4 -24,6 -5,1 -9,2 -14,4z"/>',
            'C229'=>'<path class="camarote" id="camarote_C229" d="M1810 323l22 73c13,-4 26,-8 39,-13l-23 -71c-5,1 -10,3 -15,4 0,0 0,0 0,0 -8,2 -15,4 -23,6z"/>',
            'C230'=>'<path class="camarote" id="camarote_C230" d="M1849 313l23 71c13,-4 26,-9 39,-14l-24 -68c-2,1 -4,1 -7,2 -10,3 -21,6 -31,9z"/>',
            'C231'=>'<path class="camarote" id="camarote_C231" d="M1886 302l24 68c13,-5 26,-9 38,-14l-25 -66c-12,4 -25,8 -37,12z"/>',
            'C232'=>'<path class="camarote" id="camarote_C232" d="M1924 290l25 66c13,-5 25,-10 38,-15l-25 -63c-12,4 -23,8 -35,12 -1,0 -1,0 -2,1z"/>',
            'C233'=>'<path class="camarote" id="camarote_C233" d="M1961 277l25 63c12,-5 25,-10 37,-16l-26 -60c-9,3 -17,6 -26,9 -4,1 -7,2 -11,4z"/>',
            'C234'=>'<path class="camarote" id="camarote_C234" d="M1998 264l26 60c12,-5 24,-11 37,-17l-26 -57c0,0 0,0 0,0 -6,2 -11,4 -17,7 -6,2 -13,5 -19,7z"/>',
            'C235'=>'<path class="camarote" id="camarote_C235" d="M2034 251l26 57c12,-6 24,-12 36,-18l-26 -53c-7,3 -14,5 -21,8 -5,2 -10,4 -15,6l0 0z"/>',
            'C236'=>'<path class="camarote" id="camarote_C236" d="M2070 237l26 53c20,-10 39,-20 58,-31l-25 -48c-20,9 -40,17 -60,25z"/>',

        ];
        //buscar produtos que sejam camarote
        $modelProduto=new ApiProduto;
        $camarotes=$modelProduto
            ->buscarProduto(json_encode(
                [
                    "tags"=>[
                        ["nome"=>"tipo","valor"=>"camarote"],
                    ],
                ]));
        //fazer o for de cada camarote fazendo um switch com tag do nome do camarote e associando o path do svg
        foreach ($camarotes->objeto as $camarote) {
            foreach($camarote->tags as $tag){
                if($tag->nome=='camarote' && array_key_exists($tag->valor, $svg)){
                    //salvar o anexo
                    /*$modelProduto
                        ->adicionarAnexoProduto([
                            'codigo'=>$camarote->codigo,
                            'nome'=>'svgmapa',
                            'tipo'=>'txt',
                            'objeto'=>base64_encode($svg[$tag->valor])
                        ]);*/
                }
            }          
        }

    }

    /**
     * Método auxiliar para listar os blocos 
     */
    public function actionAnexoblocosleste(){
        //lista de svg
        $svg = [
            '123'=>'<path class="bloco" id="bloco_123" d="M569 52c-42,-15 -85,-32 -129,-50l-170 259c1,0 2,1 2,1 5,3 10,5 15,8 51,12 102,23 153,32l128 -250z"/>',
            '124'=>'<path class="bloco" id="bloco_124" d="M698 94c-42,-13 -85,-27 -129,-43l-128 250c10,2 21,4 31,6 14,2 28,5 41,7 11,2 23,4 34,6 2,0 5,1 7,1 14,2 27,4 41,6l13 2 91 -235z"/>',
            '125'=>'<path class="bloco" id="bloco_125" d="M827 129c-42,-10 -86,-21 -130,-34l-91 235c9,1 18,3 27,4 13,2 27,4 40,5 13,2 26,3 40,5 13,2 26,3 39,4l0 0 15 2 59 -221z"/>',
            '126'=>'<path class="bloco" id="bloco_126" d="M959 153c-43,-6 -87,-14 -131,-24l-59 221c8,1 16,2 24,2 13,1 26,2 39,3 13,1 26,2 39,3 13,1 26,2 39,3l18 1 33 -209z"/>',
            '127'=>'<path class="bloco" id="bloco_127" d="M1091 165c-44,-2 -88,-6 -133,-12l-33 209 21 1c13,1 26,1 38,2 13,1 25,1 38,1 1,0 1,0 2,0l0 -97 0 -3 3 0 58 0 5 -101z"/>',
            '128'=>'<path class="bloco" id="bloco_128" d="M1158 267l-133 0 0 101c43,2 87,2 133,2l0 -102z"/>',
            '129'=>'<path class="bloco" id="bloco_129" d="M1152 167c-20,0 -41,0 -61,-1l-5 101 66 0 78 0 -6 -101c-24,1 -48,2 -72,2z"/>',
            '130'=>'<path class="bloco" id="bloco_130" d="M1292 268l-146 0 0 101c1,0 1,0 2,0 46,0 94,-2 143,-3l0 -98z"/>',
            '131'=>'<path class="bloco" id="bloco_131" d="M1357 151c-45,7 -89,11 -133,13l6 101 59 0 3 0 0 3 0 96c12,0 24,-1 36,-2 13,-1 25,-1 38,-2l26 -2 -34 -208z"/>',
            '132'=>'<path class="bloco" id="bloco_132" d="M1488 126c-44,10 -88,19 -131,25l34 208 13 -1c13,-1 26,-2 39,-3 13,-1 26,-2 39,-3 13,-1 26,-2 39,-4 10,-1 19,-2 29,-3l-61 -219z"/>',
            '133'=>'<path class="bloco" id="bloco_133" d="M1618 91c-44,14 -87,25 -130,35l61 219 10 -1c13,-1 26,-3 39,-5 13,-2 26,-3 40,-5 13,-2 27,-4 40,-6 11,-2 22,-3 32,-5l-92 -230z"/>',
            '134'=>'<path class="bloco" id="bloco_134" d="M1747 48c-44,16 -87,31 -129,43l92 233 8 -1c13,-2 27,-4 40,-6 14,-2 27,-5 41,-7 13,-2 26,-5 39,-7 13,-2 26,-5 39,-7l-130 -245z"/>',
            '135'=>'<path class="bloco" id="bloco_135" d="M1866 1c-41,17 -80,32 -119,46l130 247c53,-10 106,-22 159,-35l-170 -255z"/>',
        ];
        
        //retornar todos os blocos
        $model=new ApiProduto();
        $blocos=$model
            ->buscarProduto(json_encode(
                [
                    "tags"=>[
                        ["nome"=>"tipo","valor"=>"bloco"],
                        ["nome"=>"area","valor"=>"Leste"],
                        ["nome"=>"matriz","valor"=>"temporada2018"],
                    ],
                ]));
        //fazer o for de cada bloco fazendo um switch com tag do nome do bloco e associando o path do svg
        foreach ($blocos->objeto as $bloco) {
            foreach($bloco->tags as $tag){
                if($tag->nome=='bloco' && array_key_exists($tag->valor, $svg)){
                    //salvar o anexo
                    /*$salvar=$model
                        ->adicionarAnexoProduto([
                            'codigo'=>$bloco->codigo,
                            'nome'=>'svgmapa',
                            'tipo'=>'txt',
                            'objeto'=>base64_encode($svg[$tag->valor])
                        ]);*/
                }
            }          
        }
    }

    /**
     * Método auxiliar para criar anexos
    */
    public function actionAnexocamarotesleste(){
        $svg = [
            'C237'=>'<path class="camarote" id="camarote_C237" d="M440 302c-51,-10 -102,-20 -153,-32 48,25 99,47 151,68 5,2 10,4 16,6l18 -36c-10,-2 -21,-4 -31,-6z"/>',
            'C238'=>'<path class="camarote" id="camarote_C238" d="M512 315c-14,-2 -28,-5 -41,-7l-18 36c13,5 26,10 39,14l20 -43z"/>',
            'C239'=>'<path class="camarote" id="camarote_C239" d="M553 321c-14,-2 -27,-4 -41,-7l-20 43c13,5 26,9 39,13l21 -50z"/>',
            'C240'=>'<path class="camarote" id="camarote_C240" d="M594 328c-14,-2 -27,-4 -41,-6l-21 50c13,4 26,8 40,13l22 -56z"/>',
            'C241'=>'<path class="camarote" id="camarote_C241" d="M634 333c-9,-1 -18,-3 -27,-4 -4,-1 -9,-1 -13,-2l-22 56c13,4 26,8 39,11 0,0 0,0 1,0l23 -62z"/>',
            'C242'=>'<path class="camarote" id="camarote_C242" d="M674 338c-13,-2 -27,-3 -40,-5l-23 62c13,4 27,7 40,11l23 -67z"/>',
            'C243'=>'<path class="camarote" id="camarote_C243" d="M714 343c-13,-2 -26,-3 -40,-5l-23 67c13,3 27,7 40,10l22 -72z"/>',
            'C244'=>'<path class="camarote" id="camarote_C244" d="M753 348c-13,-1 -26,-3 -39,-4l-22 72c13,3 27,6 40,9l21 -77z"/>',
            'C245'=>'<path class="camarote" id="camarote_C245" d="M768 349c-5,-1 -10,-1 -15,-2l-21 77c13,3 27,5 40,8l20 -81c-8,-1 -16,-2 -24,-2z"/>',
            'C246'=>'<path class="camarote" id="camarote_C246" d="M831 355c-13,-1 -26,-2 -39,-3l-20 81c5,1 10,2 16,3 8,1 16,3 25,4l19 -85z"/>',
            'C247'=>'<path class="camarote" id="camarote_C247" d="M870 358c-13,-1 -26,-2 -39,-3l-19 85c13,2 27,4 40,6l17 -88z"/>',
            'C248'=>'<path class="camarote" id="camarote_C248" d="M908 361c-13,-1 -26,-2 -39,-3l-17 88c13,2 27,4 40,6l15 -91z"/>',
            'C249'=>'<path class="camarote" id="camarote_C249" d="M947 363c-7,0 -14,-1 -21,-1 -6,0 -12,-1 -18,-1l-15 91c13,2 27,3 41,5l13 -94z"/>',
            'C250'=>'<path class="camarote" id="camarote_C250" d="M985 365c-13,-1 -26,-1 -38,-2l-13 94c11,1 22,2 33,3 3,0 5,0 8,1l11 -96z"/>',
            'C251'=>'<path class="camarote" id="camarote_C251" d="M1023 366c-13,0 -25,-1 -38,-1l-11 96c13,1 27,2 41,3l9 -97z"/>',
            'C252'=>'<path class="camarote" id="camarote_C252" d="M1061 370c-11,0 -22,-1 -33,-1l-3 0 0 -3 0 0c-1,0 -1,0 -2,0l-9 97c13,1 27,2 41,2l6 -96z"/>',
            'C253'=>'<path class="camarote" id="camarote_C253" d="M1099 371c-13,0 -26,0 -38,-1l-6 96c13,1 27,1 41,1l4 -96z"/>',
            'C254'=>'<path class="camarote" id="camarote_C254" d="M1137 371c-13,0 -26,0 -38,0l-4 96c13,0 27,1 41,1l1 -97z"/>',
            'C255'=>'<path class="camarote" id="camarote_C255" d="M1152 468l0 0zm1 0l0 0zm22 -97c-7,0 -14,0 -21,0 -1,0 -1,0 -2,0 -5,0 -10,0 -15,0l-1 97c5,0 10,0 16,0 0,0 0,0 0,0l0 0 0 0c8,0 16,0 24,0l-2 -97z"/>',
            'C256'=>'<path class="camarote" id="camarote_C256" d="M1213 370c-13,0 -25,1 -38,1l2 97c14,0 27,0 41,-1l-4 -97z"/>',
            'C257'=>'<path class="camarote" id="camarote_C257" d="M1251 369c-13,0 -26,1 -38,1l4 97c14,0 27,-1 41,-2l-7 -96z"/>',
            'C258'=>'<path class="camarote" id="camarote_C258" d="M1289 368c-13,0 -25,1 -37,1l7 96c14,-1 27,-1 41,-2l-9 -95 -1 0z"/>',
            'C259'=>'<path class="camarote" id="camarote_C259" d="M1327 363c-12,1 -24,1 -36,2l0 0 0 3 -2 0 9 95c14,-1 27,-2 40,-3 0,0 0,0 0,0l-12 -97z"/>',
            'C260'=>'<path class="camarote" id="camarote_C260" d="M1366 361c-13,1 -25,1 -38,2l12 97c14,-1 27,-3 41,-4l-14 -95z"/>',
            'C261'=>'<path class="camarote" id="camarote_C261" d="M1391 359c-9,1 -17,1 -26,2l14 95c14,-1 27,-3 40,-5l-16 -92c-4,0 -9,1 -13,1z"/>',
            'C262'=>'<path class="camarote" id="camarote_C262" d="M1443 356c-13,1 -26,2 -39,3l16 92c14,-2 27,-4 40,-6l-18 -90z"/>',
            'C263'=>'<path class="camarote" id="camarote_C263" d="M1481 352c-13,1 -26,2 -39,3l18 90c14,-2 27,-4 40,-7l-20 -86z"/>',
            'C264'=>'<path class="camarote" id="camarote_C264" d="M1520 348c-13,1 -26,3 -39,4l20 86c6,-1 11,-2 17,-3 8,-1 16,-3 24,-4l-21 -83z"/>',
            'C266'=>'<path class="camarote" id="camarote_C266" d="M1549 345c-10,1 -19,2 -29,3l21 83c13,-3 27,-5 40,-8l-22 -79c-3,0 -7,1 -10,1z"/>',
            'C267'=>'<path class="camarote" id="camarote_C267" d="M1600 340c-14,2 -27,3 -41,5l23 79c14,-3 28,-6 42,-9l-24 -74z"/>',
            'C268'=>'<path class="camarote" id="camarote_C268" d="M1638 334c-13,2 -26,3 -40,5l0 0c1,0 1,0 2,0l24 74c13,-3 25,-6 38,-9l-24 -69z"/>',
            'C269'=>'<path class="camarote" id="camarote_C269" d="M1678 329c-13,2 -27,4 -40,6l24 69c11,-3 22,-6 33,-9 2,-1 5,-1 7,-2l-24 -64z"/>',
            'C270'=>'<path class="camarote" id="camarote_C270" d="M1710 324c-11,2 -22,3 -32,5l24 64c13,-4 27,-8 40,-12l-23 -58c-3,0 -5,1 -8,1z"/>',
            'C271'=>'<path class="camarote" id="camarote_C271" d="M1758 316c-13,2 -27,4 -40,6l23 58c13,-4 26,-8 40,-13l-23 -52z"/>',
            'C272'=>'<path class="camarote" id="camarote_C272" d="M1799 309c-14,2 -27,5 -41,7l23 52c13,-4 26,-9 39,-14l-21 -45z"/>',
            'C273'=>'<path class="camarote" id="camarote_C273" d="M1838 302c-13,2 -26,5 -39,7l21 45c12,-4 25,-9 37,-14l-19 -39z"/>',
            'C274'=>'<path class="camarote" id="camarote_C274" d="M1877 295c-13,3 -26,5 -39,7l19 39c4,-1 7,-3 10,-4 58,-23 113,-48 166,-76 1,0 2,-1 2,-1 -53,13 -106,24 -159,35z"/>',
        ];
        //buscar produtos que sejam camarote
        $modelProduto=new ApiProduto;
        $camarotes=$modelProduto
            ->buscarProduto(json_encode(
                [
                    "tags"=>[
                        ["nome"=>"tipo","valor"=>"camarote"],
                        ["nome"=>"area","valor"=>"Leste"],
                        ["nome"=>"matriz","valor"=>"temporada2018"],
                    ],
                ]));
        //fazer o for de cada camarote fazendo um switch com tag do nome do camarote e associando o path do svg
        foreach ($camarotes->objeto as $camarote) {
            foreach($camarote->tags as $tag){
                if($tag->nome=='camarote' && array_key_exists($tag->valor, $svg)){
                    //salvar o anexo
                    /*$salvar=$modelProduto
                        ->adicionarAnexoProduto([
                            'codigo'=>$camarote->codigo,
                            'nome'=>'svgmapa',
                            'tipo'=>'txt',
                            'objeto'=>base64_encode($svg[$tag->valor])
                        ]);*/
                }
            }          
        }

    }

     /**
      * Listar produtos de forma simples (verificar duplicados)
      * filtrado por tags
      */
    public function actionListaprodutos($pag=1){
        $apiProduto = new ApiProduto(); 
        $produtos = $apiProduto->buscarProduto(json_encode(
                [
                    "tags"=>[
                        ["nome"=>"tipo","valor"=>"assento"],
                        ["nome"=>"bloco","valor"=>"130"],
                        ["nome"=>"matriz","valor"=>"temporada2018"],
                    ],
                    "registro_inicial"=>500*($pag-1),
                ])
        );
        foreach ($produtos->objeto as $p) {
                echo $p->codigo.'<br />';

        }
        /*foreach ($produtos->objeto as $p) {
            if($p->estoque->oferecido==0 && $p->estoque->vendido==0 && $p->estoque->reservado==0){
                echo $p->codigo;
                echo '<br />';
            }
        }*/
    }

    /**
     * Método auxiliar para editar estoque dos assentos
     */
    public function actionSetestoqueassentos(){
        /*$modelProduto=new ApiProduto;       
        $update['codigo']='';
        $update['tid']=microtime(1);
        $update['unidades']=1;
        $alterarEstoque=$modelProduto->adicionarEstoqueProduto($update);*/

        
    }

    /**
     * Método auxiliar para editar estoque dos assentos
     */
    public function actionConsultatotais(){
        $modelProduto=new ApiProduto;       
        $totais=$modelProduto->consultarTotaisEstoqueProduto([
                'tags'=>[
                    [
                        "nome"=>"matriz",
                        "valor"=>"temporada2018"
                    ],
                
                    [
                        "nome"=>"tipo",
                        "valor"=>"assento"
                    ],
                
                    [
                        "nome"=>"bloco",
                        "valor"=>"108"
                    ]
            
                ]
            ]);
        var_dump($totais);
    }

    public function actionListarenovacao(){
        $lista=\app\models\Renovacao::find()
        ->where(['email'=>'leolara@hotmail.com'])
        ->orWhere(['email'=>'leogodinho100@hotmail.com'])
        ->orWhere(['email'=>'pamelasilvia23@hotmail.com'])
        ->all();
        return $this->render('listarenovacao',['lista'=>$lista]);
    }
    /**
     * Método auxiliar para editar estoque dos camarotes (sem usar foreach)
     */
    /*public function actionSetestoquecamarote($c,$u){
        $modelProduto=new ApiProduto;
        $lugares=$modelProduto
            ->buscarProduto(json_encode(
                [
                    "tags"=>[
                        ["nome"=>"camarote","valor"=>$c],
                    ],
                ]));
        if($lugares->successo!=1) echo 'Não encontrado';
        else{
            $obj=$lugares->objeto[0];
            $update['codigo']=$obj->codigo;
            $update['tid']=microtime(1);
            $update['unidades']=$u;
            $adicionar=$modelProduto->adicionarEstoqueProduto($update);
            echo '<pre>';print_r($adicionar);echo '</pre>';
        }
    }*/



    /**
     * Método auxiliar para editar estoque dos blocos (sem usar foreach)
     */
    /*public function actionSetestoquebloco($b,$u){
        $modelProduto=new ApiProduto;
        $lugares=$modelProduto
            ->buscarProduto(json_encode(
                [
                    "tags"=>[
                        ["nome"=>"bloco","valor"=>$b],
                    ],
                ]));
        if($lugares->successo!=1) echo 'Não encontrado';
        else{
            $obj=$lugares->objeto[0];
            $update['codigo']=$obj->codigo;
            $update['tid']=microtime(1);
            $update['unidades']=$u;
            $adicionar=$modelProduto->adicionarEstoqueProduto($update);
            echo '<pre>';print_r($adicionar);echo '</pre>';
        }
    }*/

    /**
     * Método auxiliar para editar os preços dos assentos e ajustar o estoque inicial
     * Blocos: R$ 1.800
     * Camarote: R$ 130.000 (dividido pela qtde de lugares no camarote)
     */
    public function actionSetprecosestoque(){
        $modelProduto=new ApiProduto;
        $lugares=$modelProduto
            ->buscarProduto(json_encode(
                [
                    "tags"=>[
                        ["nome"=>"matriz","valor"=>"temporada2018"],
                        ["nome"=>"area","valor"=>"Leste"],
                        ["nome"=>"tipo","valor"=>"assentos"],
                    ],
                ]));
        foreach ($lugares->objeto as $obj) {
            $update=[];
            switch($obj->nome){
                case 'Bloco 123 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=1800.00;
                    $update['unidades']=514;
                break;
                case 'Bloco 124 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=1800.00;
                    $update['unidades']=298;
                break;
                case 'Bloco 125 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=1800.00;
                    $update['unidades']=220;
                break;
                case 'Bloco 126 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=1800.00;
                    $update['unidades']=469;
                break;
                case 'Bloco 127 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=1800.00;
                    $update['unidades']=442;
                break;
                case 'Bloco 128 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=1800.00;
                    $update['unidades']=101;
                break;
                case 'Bloco 129 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=1800.00;
                    $update['unidades']=282;
                break;
                case 'Bloco 130 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=1800.00;
                    $update['unidades']=109;
                break;
                case 'Bloco 131 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=1800.00;
                    $update['unidades']=443;
                break;
                case 'Bloco 132 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=1800.00;
                    $update['unidades']=463;
                break;
                case 'Bloco 133 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=1800.00;
                    $update['unidades']=228;
                break;                
                case 'Bloco 134 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=1800.00;
                    $update['unidades']=293;
                break;                
                case 'Bloco 135 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=1800.00;
                    $update['unidades']=521;
                break;
                case 'Camarote C237 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=45;
                break;
                case 'Camarote C238 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C239 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C240 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C241 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C242 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C243 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C244 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C245 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C246 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C247 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C248 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C249 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C250 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C251 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C252 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C253 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C254 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C255 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C256 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C257 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C258 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C259 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C260 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C261 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C262 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C263 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C264 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C265 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C266 Leste':
                    $update['tid']=microtime(1);
                   // $update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C267 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C268 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C269 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C270 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C271 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C272 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C273 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=20;
                break;
                case 'Camarote C274 Leste':
                    $update['tid']=microtime(1);
                    //$update['valor']=130000.00;
                    $update['unidades']=44;
                break;
                
            }
            $update['codigo']=$obj->codigo;
            //$modelProduto->adicionarTagProduto([
                    //'codigo'=>$update['codigo'],
                    //'nome'=>'capacidade',
                    //'valor'=>$update['estoque']
                //]);
            //$modelProduto->alterarProduto($update);
            $modelProduto->adicionarEstoqueProduto($update);
            usleep(100);
        }
    }

    public function actionSetprecos(){
        $modelProduto=new ApiProduto;
        $lugares=$modelProduto
            ->buscarProduto(json_encode(
                [
                    "tags"=>[
                        ["nome"=>"matriz","valor"=>"base"],
                        ["nome"=>"area","valor"=>"Oeste"],
                    ],
                ]));
        foreach ($lugares->objeto as $obj) {
            $update=[];
            $update['valor']=1;
            switch($obj->nome){
                case 'Bloco 101 Oeste':
                    $update['valor']=1800.00;
                    $update['estoque']=189;
                break;
                case 'Bloco 102 Oeste':
                    $update['valor']=1800.00;
                    $update['estoque']=302;
                break;
                case 'Bloco 103 Oeste':
                    $update['valor']=1800.00;
                    $update['estoque']=242;
                break;
                case 'Bloco 104 Oeste':
                    $update['valor']=1800.00;
                    $update['estoque']=273;
                break;
                case 'Bloco 105 Oeste':
                    $update['valor']=1800.00;
                    $update['estoque']=190;
                break;
                case 'Bloco 106 Oeste':
                    $update['valor']=1800.00;
                    $update['estoque']=225;
                break;
                case 'Bloco 107 Oeste':
                    $update['valor']=1800.00;
                    $update['estoque']=191;
                break;
                case 'Bloco 108 Oeste':
                    $update['valor']=1800.00;
                    $update['estoque']=270;
                break;
                case 'Bloco 109 Oeste':
                    $update['valor']=1800.00;
                    $update['estoque']=241;
                break;
                case 'Bloco 110 Oeste':
                    $update['valor']=1800.00;
                    $update['estoque']=300;
                break;
                case 'Bloco 111 Oeste':
                    $update['valor']=1800.00;
                    $update['estoque']=187;
                break;
                case 'Bloco VIP Inferior 105 Oeste':
                    $update['valor']=1800.00;
                    $update['estoque']=87;
                break;
                case 'Bloco VIP Inferior 106 Oeste':
                    $update['valor']=1800.00;
                    $update['estoque']=115;
                break;
                case 'Bloco VIP Inferior 107 Oeste':
                    $update['valor']=1800.00;
                    $update['estoque']=87;
                break;
                case 'Bloco VIP Superior 201 Oeste':
                    $update['valor']=1800.00;
                    $update['estoque']=42;
                break;
                case 'Bloco VIP Superior 202 Oeste':
                    $update['valor']=1800.00;
                    $update['estoque']=126;
                break;
                case 'Bloco VIP Superior 203 Oeste':
                    $update['valor']=1800.00;
                    $update['estoque']=42;
                break;                
                case 'Cadeira A1 bloco 101':
                    $update['valor']=1500.00;
                    $update['estoque']=1;
                break;
                case 'Camarote C101 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C102 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C103 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C104 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C105 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C106 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C107 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C108 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C109 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C110 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C111 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C112 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C113 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C114 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C115 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C116 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C117 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C118 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C119 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C120 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C121 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C122 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C123 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C124 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=20;
                break;
                case 'Camarote C201 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=62;
                break;
                case 'Camarote C202 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C203 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C204 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C205 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C206 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C207 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C208 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C209 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C210 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C211 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C212 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C213 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C214 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C215 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C216 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C217 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C218 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C219 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C220 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C221 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C222 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C223 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C224 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C225 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C226 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C227 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C228 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C229 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C230 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C231 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C232 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C233 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C234 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C235 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=18;
                break;
                case 'Camarote C236 Oeste':
                    $update['valor']=130000.00;
                    $update['estoque']=64;
                break;
            }
            //$update['codigo']=$obj->codigo;
            /*$modelProduto->adicionarTagProduto([
                    'codigo'=>$update['codigo'],
                    'nome'=>'capacidade',
                    'valor'=>$update['estoque']
                ]);*/
            //$modelProduto->alterarProduto($update);
        }
    }

    /**
     * Método auxiliar para o estoque base dos assentos
     */
    public function actionSetestoque(){
        $modelProduto=new ApiProduto;
        $lugares=$modelProduto
            ->buscarProduto(json_encode(
                [
                    "tags"=>[
                        ["nome"=>"matriz","valor"=>"base"],
                        ["nome"=>"area","valor"=>"Oeste"],
                    ],
                ]));
        foreach ($lugares->objeto as $obj) {
            $update=[];
            switch($obj->nome){
                case 'Bloco 101 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=189;
                break;
                case 'Bloco 102 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=302;
                break;
                case 'Bloco 103 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=242;
                break;
                case 'Bloco 104 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=273;
                break;
                case 'Bloco 105 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=190;
                break;
                case 'Bloco 106 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=225;
                break;
                case 'Bloco 107 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=191;
                break;
                case 'Bloco 108 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=270;
                break;
                case 'Bloco 109 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=241;
                break;
                case 'Bloco 110 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=300;
                break;
                case 'Bloco 111 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=187;
                break;
                case 'Bloco VIP Inferior 105 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=87;
                break;
                case 'Bloco VIP Inferior 106 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=115;
                break;
                case 'Bloco VIP Inferior 107 Oeste':
                    $update['tid']=microtime(1);
                    $update['estoque']=87;
                break;
                case 'Bloco VIP Superior 201 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=42;
                break;
                case 'Bloco VIP Superior 202 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=126;
                break;
                case 'Bloco VIP Superior 203 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=42;
                break; 
                case 'Camarote C101 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C102 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C103 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C104 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C105 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C106 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C107 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C108 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C109 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C110 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C111 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C112 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C113 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C114 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C115 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C116 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C117 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C118 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C119 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C120 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C121 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C122 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C123 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C124 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=20;
                break;
                case 'Camarote C201 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=62;
                break;
                case 'Camarote C202 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C203 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C204 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C205 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C206 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C207 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C208 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C209 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C210 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C211 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C212 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C213 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C214 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C215 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C216 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C217 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C218 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C219 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C220 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C221 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C222 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C223 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C224 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C225 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C226 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C227 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C228 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C229 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C230 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C231 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C232 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C233 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C234 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C235 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=18;
                break;
                case 'Camarote C236 Oeste':
                    $update['tid']=microtime(1);
                    $update['unidades']=64;
                break;
            }
            $update['codigo']=$obj->codigo;
            $modelProduto->adicionarEstoqueProduto($update);
            usleep(100);
        }
    }

    /**
     * Método auxiliar para criar produtos
     */
    public function actionCriarprodutosleste(){
        // nomeação dos blocos da área vermelha
        $ini=123;
        $fim=135;
        while($ini<=$fim){
            $post=[
                "nome"=>"Bloco ".$ini." Leste",
                "descricao"=>"Mineirão Cativa bloco ".$ini." Leste",
                "valor"=>"0",
                "ativo"=>"1",
            ];
            $model=new ApiProduto();
            $novo=$model->criarProduto($post);
            //cadastrar tags
            if($novo->successo==1){
                $tag1=$model->adicionarTagProduto([
                    'codigo'=>$novo->objeto->codigo,
                    'nome'=>'area',
                    'valor'=>'Leste'
                ]);

                $tag2=$model->adicionarTagProduto([
                    'codigo'=>$novo->objeto->codigo,
                    'nome'=>'bloco',
                    'valor'=>$ini
                ]);

                $tag3=$model->adicionarTagProduto([
                    'codigo'=>$novo->objeto->codigo,
                    'nome'=>'tipo',
                    'valor'=>'bloco'
                ]);

                $tag4=$model->adicionarTagProduto([
                    'codigo'=>$novo->objeto->codigo,
                    'nome'=>'matriz',
                    'valor'=>'temporada2018'
                ]);
            }
            $ini++;
        }
        // nomeação dos camarotes da área vermelha
        //$ini=237;
        //$fim=274;
        /*while($ini<=$fim){
            $post=[
                "nome"=>"Camarote C".$ini." Leste",
                "valor"=>"0",
                "ativo"=>"0",
            ];
            $model=new ApiProduto();
            $novo=$model->criarProduto($post);
            //cadastrar tags
            if($novo->successo==1){
                $tag1=$model->adicionarTagProduto([
                    'codigo'=>$novo->objeto->codigo,
                    'nome'=>'area',
                    'valor'=>'Leste'
                ]);

                $tag2=$model->adicionarTagProduto([
                    'codigo'=>$novo->objeto->codigo,
                    'nome'=>'camarote',
                    'valor'=>'C'.$ini
                ]);

                $tag3=$model->adicionarTagProduto([
                    'codigo'=>$novo->objeto->codigo,
                    'nome'=>'tipo',
                    'valor'=>'camarote'
                ]);

                $tag4=$model->adicionarTagProduto([
                    'codigo'=>$novo->objeto->codigo,
                    'nome'=>'matriz',
                    'valor'=>'base'
                ]);
            }
            $ini++;
        }*/
    }

    /**
     * Método auxiliar para criar produtos
     */
    public function actionCriarprodutosoeste(){
        // nomeação dos blocos (Mineirão Cativa)
        //$ini=101;
        //$fim=111;
        // nomeação dos blocos vip (Mineirão Tribuna)
        //$ini=105;
        //$fim=107;
        // nomeação dos blocos vip superior (Camarote Brahma)
        //$ini=201;
        //$fim=203;
        // nomeação dos camarotes
        //$ini=101;
        //$fim=124;
        $ini=201;
        $fim=236;
        while($ini<=$fim){
            $post=[
                "nome"=>"Camarote C".$ini." Oeste",
                "descricao"=>"Camarote C".$ini." - área oeste",
                "valor"=>"0",
                "ativo"=>"1",
            ];
            /*$post=[
                "nome"=>"Bloco ".$ini." Oeste",
                "descricao"=>"Mineirão Cativa bloco ".$ini." - área oeste",
                "valor"=>"0",
                "ativo"=>"1",
            ];*/
            /*$post=[
                "nome"=>"Bloco VIP Inferior ".$ini." Oeste",
                "descricao"=>"Mineirão Tribuna bloco ".$ini." - área oeste",
                "valor"=>"0",
                "ativo"=>"1",
            ];*/
            /*$post=[
                "nome"=>"Bloco VIP Superior ".$ini." Oeste",
                "descricao"=>"Camarote Brahma bloco ".$ini." - área oeste",
                "valor"=>"0",
                "ativo"=>"1",
            ];*/
            $model=new ApiProduto();
            $novo=$model->criarProduto($post);
            //cadastrar tags
            if($novo->successo==1){
                $model->adicionarTagProduto([
                    'codigo'=>$novo->objeto->codigo,
                    'nome'=>'area',
                    'valor'=>'Oeste'
                ]);
                $model->adicionarTagProduto([
                    'codigo'=>$novo->objeto->codigo,
                    'nome'=>'camarote',
                    'valor'=>'C'.$ini
                ]);
                $model->adicionarTagProduto([
                    'codigo'=>$novo->objeto->codigo,
                    'nome'=>'tipo',
                    'valor'=>'camarote'
                ]);
                /*$model->adicionarTagProduto([
                    'codigo'=>$novo->objeto->codigo,
                    'nome'=>'bloco',
                    'valor'=>'vip'.$ini
                ]);
                $model->adicionarTagProduto([
                    'codigo'=>$novo->objeto->codigo,
                    'nome'=>'tipo',
                    'valor'=>'bloco'
                ]);
                $model->adicionarTagProduto([
                    'codigo'=>$novo->objeto->codigo,
                    'nome'=>'vip',
                    'valor'=>''
                ]);*/
                $model->adicionarTagProduto([
                    'codigo'=>$novo->objeto->codigo,
                    'nome'=>'matriz',
                    'valor'=>'base'
                ]);
            }
            $ini++;
        }
    }
    
    /**
     * Adicionar tag a todos os produtos
     */
    public function actionAtualizarProdutos()
    {
        $apiProduto = new ApiProduto(); 
        $produtos = $apiProduto->buscarProduto();
        foreach ($produtos->objeto as $index => $produto) {
            $returnUpdate = $apiProduto->adicionarTagProduto([
                'codigo' => $produto->codigo,
                'nome' => 'matriz',
                'valor' => 'base'
            ]);
            echo 'produto:' . $produto->codigo . ' - ' . $returnUpdate->successo . '<br>';
        }
    }

    /**
     * Uma simples contagem de produtos baseado em tags
     */
    public function actionContagemprodutos(){
        $apiProduto = new ApiProduto(); 
        $produtos = $apiProduto->buscarProduto(json_encode(
                [
                    "tags"=>[
                        // /*/
                        ["nome"=>"matriz","valor"=>"base"],
                        /*/
                        ["nome"=>"matriz","valor"=>"temporada"],
                        //*/
                        ["nome"=>"tipo","valor"=>"bloco"],
                        //["nome"=>"vip","valor"=>""],
                        ["nome"=>"area","valor"=>"Leste"],
                    ],
                ])
        );
        echo 'Total de produtos: '.count($produtos->objeto);
    }

     /**
      * Deletar produto - ATENÇÃO! Esse método não pede confirmação e é irreversível. Use com cautela
      */
    public function actionDeleteprodutos(){
        $apiProduto = new ApiProduto(); 
        $produtos = $apiProduto->buscarProduto(json_encode(
                [
                    "tags"=>[
                        ["nome"=>"area","valor"=>"Oeste"],
                        ["nome"=>"matriz","valor"=>"temporada2018"],
                    ],
                ])
        );
        
        foreach ($produtos->objeto as $p) {
            if($p->codigo>28074)
                //$apiProduto->excluirProduto(['codigo'=>$p->codigo]);
                echo $p->codigo.': '.$p->nome.'<br />';
        }

        exit(0);
    }

    /**
     * Método auxiliar para cadastro de assentos
     * Cada bloco é representando por um array cujo índice representa a fileira e o valor a quantidade de assentos.
     * Para cada assento, chamar o endpoint de cadastro da API
     * Usar depois o endpoint tags para identificar assentos especiais
     */
    public function actionSetassentos(){
        // ÁREA OESTE
        /*$bloco101=[
            'A'=>7,
            'B'=>8,
            'C'=>9,
            'D'=>9,
            'E'=>10,
            'F'=>10,
            'G'=>11,
            'H'=>12,
            'I'=>12,
            'J'=>13,
            'K'=>13,
            'L'=>14,
            'M'=>15,
            'N'=>15,
            'O'=>16,
            'P'=>15,
        ];
        $bloco102=[
            'A'=>26,
            'B'=>27,
            'C'=>27,
            'D'=>27,
            'E'=>27,
            'F'=>28,
            'G'=>28,
            'H'=>28,
            'I'=>28,
            'J'=>28,
            'K'=>28,
        ];
        $bloco103=[
            'A'=>22,
            'B'=>22,
            'C'=>22,
            'D'=>22,
            'E'=>22,
            'F'=>22,
            'G'=>22,
            'H'=>22,
            'I'=>22,
            'J'=>22,
            'K'=>22,
        ];
        $bloco104=[
            'A'=>24,
            'B'=>24,
            'C'=>25,
            'D'=>25,
            'E'=>25,
            'F'=>25,
            'G'=>25,
            'H'=>25,
            'I'=>25,
            'J'=>25,
            'K'=>25,
        ];
        $bloco105=[
            'A'=>17,
            'B'=>17,
            'C'=>17,
            'D'=>17,
            'E'=>17,
            'F'=>17,
            'G'=>17,
            'H'=>17,
            'I'=>18,
            'J'=>18,
            'K'=>18,
        ];
        $bloco106=[
            'A'=>12,
            'B'=>12,
            'C'=>22,
            'D'=>22,
            'E'=>22,
            'F'=>22,
            'G'=>22,
            'H'=>22,
            'I'=>23,
            'J'=>23,
            'K'=>23,
        ];
        $bloco107=[
            'A'=>17,
            'B'=>17,
            'C'=>17,
            'D'=>17,
            'E'=>17,
            'F'=>17,
            'G'=>17,
            'H'=>18,
            'I'=>18,
            'J'=>18,
            'K'=>18,
        ];
        $bloco108=[
            'A'=>24,
            'B'=>24,
            'C'=>24,
            'D'=>24,
            'E'=>24,
            'F'=>25,
            'G'=>25,
            'H'=>25,
            'I'=>25,
            'J'=>25,
            'K'=>25,
        ];
        $bloco109=[
            'A'=>21,
            'B'=>22,
            'C'=>22,
            'D'=>22,
            'E'=>22,
            'F'=>22,
            'G'=>22,
            'H'=>22,
            'I'=>22,
            'J'=>22,
            'K'=>22,
        ];
        $bloco110=[
            'A'=>26,
            'B'=>26,
            'C'=>27,
            'D'=>27,
            'E'=>27,
            'F'=>27,
            'G'=>28,
            'H'=>28,
            'I'=>28,
            'J'=>28,
            'K'=>28,
        ];
        $bloco111=[
            'A'=>7,
            'B'=>8,
            'C'=>8,
            'D'=>9,
            'E'=>10,
            'F'=>10,
            'G'=>11,
            'H'=>12,
            'I'=>12,
            'J'=>13,
            'K'=>14,
            'L'=>14,
            'M'=>15,
            'N'=>15,
            'O'=>16,
            'P'=>13,
        ];
        $blocovip105=[
            'L'=>18,
            'M'=>18,
            'N'=>18,
            'O'=>18,
            'P'=>15,
        ];        
        $blocovip106=[
            'L'=>23,
            'M'=>23,
            'N'=>23,
            'O'=>23,
            'P'=>23,
        ];
        $blocovip107=[
            'L'=>18,
            'M'=>18,
            'N'=>18,
            'O'=>18,
            'P'=>15,
        ];
        $blocovip201=[
            'Q'=>7,
            'R'=>7,
            'S'=>7,
            'T'=>7,
            'U'=>7,
            'V'=>7,
        ];
        $blocovip202=[
            'Q'=>21,
            'R'=>21,
            'S'=>21,
            'T'=>21,
            'U'=>21,
            'V'=>21,
        ];
        $blocovip203=[
            'Q'=>7,
            'R'=>7,
            'S'=>7,
            'T'=>7,
            'U'=>7,
            'V'=>7,
        ];*/

        // ÁREA LESTE
        
        /*$bloco123=[
            'A'=>25,
            'B'=>25,
            'C'=>26,
            'D'=>26,
            'E'=>27,
            'F'=>27,
            'G'=>27,
            'H'=>28,
            'I'=>28,
            'J'=>28,
            'K'=>27,
            'L'=>21,
            'M'=>11,
            'N'=>11,
            'O'=>11,
            'P'=>9,
            'Q'=>9,
            'R'=>9,
            'S'=>22,
            'T'=>22,
            'U'=>23,
            'V'=>23,
            'W'=>23,
            'X'=>26,
        ];*/

        /*$bloco124=[
            'A'=>14,
            'B'=>14,
            'C'=>14,
            'D'=>14,
            'E'=>14,
            'F'=>14,
            'G'=>14,
            'H'=>14,
            'I'=>14,
            'J'=>14,
            'K'=>13,
            'L'=>9,
            'M'=>15,
            'N'=>15,
            'O'=>15,
            'P'=>15,
            'Q'=>15,
            'R'=>15,
            'S'=>15,
            'T'=>15,
            'U'=>16,
        ];*/

        /*$bloco125=[
            'A'=>10,
            'B'=>10,
            'C'=>10,
            'D'=>10,
            'E'=>10,
            'F'=>10,
            'G'=>10,
            'H'=>11,
            'I'=>11,
            'J'=>11,
            'K'=>11,
            'L'=>6,
            'M'=>11,
            'N'=>11,
            'O'=>11,
            'P'=>11,
            'Q'=>11,
            'R'=>11,
            'S'=>11,
            'T'=>11,
            'U'=>12,
        ];*/

        /*$bloco126=[
            'A'=>24,
            'B'=>24,
            'C'=>24,
            'D'=>24,
            'E'=>24,
            'F'=>24,
            'G'=>24,
            'H'=>24,
            'I'=>24,
            'J'=>24,
            'K'=>25,
            'L'=>17,
            'M'=>20,
            'N'=>20,
            'O'=>20,
            'P'=>20,
            'Q'=>20,
            'R'=>21,
            'S'=>21,
            'T'=>21,
            'U'=>24,
        ];*/

        /*$bloco127=[
            'A'=>24,
            'B'=>24,
            'C'=>24,
            'D'=>24,
            'E'=>24,
            'F'=>24,
            'G'=>24,
            'H'=>24,
            'I'=>24,
            'J'=>24,
            'K'=>25,
            'L'=>17,
            'M'=>17,
            'N'=>17,
            'O'=>17,
            'P'=>17,
            'Q'=>18,
            'R'=>18,
            'S'=>18,
            'T'=>18,
            'U'=>20,
        ];*/

        /*$bloco128=[
            'A'=>11,
            'B'=>11,
            'C'=>11,
            'D'=>11,
            'E'=>11,
            'F'=>11,
            'G'=>11,
            'H'=>11,
            'I'=>13,
        ];*/

        /*$bloco129=[
            'A'=>24,
            'B'=>24,
            'C'=>24,
            'D'=>24,
            'E'=>24,
            'F'=>24,
            'G'=>24,
            'H'=>24,
            'I'=>24,
            'J'=>24,
            'K'=>25,
            'L'=>17,
        ];*/

        /*$bloco130=[
            'A'=>12,
            'B'=>12,
            'C'=>12,
            'D'=>12,
            'E'=>12,
            'F'=>12,
            'G'=>12,
            'H'=>12,
            'I'=>13,
        ];*/

        /*$bloco131=[
            'A'=>24,
            'B'=>24,
            'C'=>24,
            'D'=>24,
            'E'=>24,
            'F'=>24,
            'G'=>24,
            'H'=>24,
            'I'=>24,
            'J'=>24,
            'K'=>25,
            'L'=>17,
            'M'=>17,
            'N'=>17,
            'O'=>17,
            'P'=>17,
            'Q'=>18,
            'R'=>18,
            'S'=>18,
            'T'=>18,
            'U'=>21,
        ];*/

        /*$bloco132=[
            'A'=>24,
            'B'=>24,
            'C'=>24,
            'D'=>24,
            'E'=>24,
            'F'=>24,
            'G'=>24,
            'H'=>24,
            'I'=>24,
            'J'=>24,
            'K'=>24,
            'L'=>16,
            'M'=>20,
            'N'=>20,
            'O'=>20,
            'P'=>20,
            'Q'=>20,
            'R'=>20,
            'S'=>20,
            'T'=>20,
            'U'=>23,
        ];*/

        /*$bloco133=[
            'A'=>11,
            'B'=>11,
            'C'=>11,
            'D'=>11,
            'E'=>11,
            'F'=>11,
            'G'=>11,
            'H'=>11,
            'I'=>11,
            'J'=>11,
            'K'=>11,
            'L'=>6,
            'M'=>11,
            'N'=>11,
            'O'=>11,
            'P'=>11,
            'Q'=>11,
            'R'=>11,
            'S'=>11,
            'T'=>11,
            'U'=>13,
        ];*/

        /*$bloco134=[
            'A'=>13,
            'B'=>13,
            'C'=>13,
            'D'=>13,
            'E'=>13,
            'F'=>14,
            'G'=>14,
            'H'=>14,
            'I'=>14,
            'J'=>14,
            'K'=>14,
            'L'=>10,
            'M'=>14,
            'N'=>14,
            'O'=>15,
            'P'=>15,
            'Q'=>15,
            'R'=>15,
            'S'=>15,
            'T'=>15,
            'U'=>16,
        ];*/

       /* $bloco135=[
            'A'=>25,
            'B'=>26,
            'C'=>26,
            'D'=>26,
            'E'=>27,
            'F'=>27,
            'G'=>27,
            'H'=>28,
            'I'=>29,
            'J'=>29,
            'K'=>28,
            'L'=>23,
            'M'=>13,
            'N'=>13,
            'O'=>12,
            'P'=>9,
            'Q'=>9,
            'R'=>9,
            'S'=>20,
            'T'=>21,
            'U'=>22,
            'V'=>22,
            'W'=>23,
            'X'=>27,
        ];*/

        $this->setLog('Chamada à API para cadastro de produtos - bloco 135');
        foreach($bloco135 as $k=>$v){
            $x=1;
            while($x<=$v){
                $tags=[
                    ['codigo'=>0,'nome'=>'tipo','valor'=>'assento'],
                    ['codigo'=>0,'nome'=>'bloco','valor'=>'135'],
                    ['codigo'=>0,'nome'=>'fileira','valor'=>$k],
                    ['codigo'=>0,'nome'=>'cadeira','valor'=>$x],
                    ['codigo'=>0,'nome'=>'area','valor'=>'Leste'],
                    ['codigo'=>0,'nome'=>'matriz','valor'=>'base'],
                ];
                $this->logs->log('Tags do produto', $tags);
        
                $modelProduto=new ApiProduto;
                $produto=$modelProduto->criarProduto([
                        'nome'=>'Cadeira '.$k.$x.' bloco 135',
                        'valor'=>1,
                        'ativo'=>1,
                    ]);
                $this->logs->log('INSERT Produto', $produto);
                //echo 'criar produto com nome Cadeira '.$k.$x.' bloco 110'.'<br />';
                if($produto->successo==1){
                    foreach ($tags as $tag) {
                        $tag['codigo']=$produto->objeto->codigo;
                        $modelProdutoTag=new ApiProduto;
                        $tagInsert=$modelProdutoTag->adicionarTagProduto($tag);
                        $this->logs->log('INSERT tag', $tag);
                        //echo 'cadastrar tag '.$tag['nome'].'='.$tag['valor'].'<br />';
                    }
                } else {
                    echo '<h3>O produto e tags abaixo não foram cadastrados:</h3>';
                    print_r($produto);
                    echo '<br />';
                    print_r($tags);
                    echo '<hr />';
                }
                //echo '<hr />';
                $x++;
            }
        }

        exit(0);
    }

    /**
     * Renovação - percorrer registros para pesquisar o ID do produto pelas tags. Gerar base64 do ID do produto e salvar o ID limpo, o código com base64 documento do comprador com base64
    */
    public function actionRenovacao(){
        $clientes=Renovacao::find()->where(['bloco'=>'108'])->all();
        $apiProduto=new ApiProduto;
        foreach($clientes as $cliente){
            $produto = $apiProduto->buscarProduto(json_encode(
                [
                    "tags"=>[
                        /*/
                        ["nome"=>"matriz","valor"=>"base"],
                        /*/
                        ["nome"=>"matriz","valor"=>"temporada2018"],
                        //*/
                        ["nome"=>"bloco","valor"=>$cliente->bloco],
                        ["nome"=>"fileira","valor"=>$cliente->fileira],
                        ["nome"=>"cadeira","valor"=>$cliente->assento],
                    ],
                ])
            );

            if($produto->successo==0){
                echo 'Falha procurando o produto:';
                print_r($cliente->id);
                echo '<hr />';
            }
            else{
                /*echo '<pre>';
                print_r($cliente->attributes);
                echo '</pre>';
                echo '<pre>';
                print_r($produto->objeto);
                echo '</pre>';
    
                echo '<hr />';*/

                $cliente->codproduto=$produto->objeto[0]->codigo;
                $cliente->produtobase64=base64_encode($produto->objeto[0]->codigo);
                $cliente->cpfbase64=base64_encode($cliente->cpf);
                $cliente->url_compra='https://vendas.estadiomineirao.com.br/index.php?r=site%2Ftemporada&id=810&area=Oeste&reserva='.$cliente->produtobase64.'&usuario='.$cliente->cpfbase64;

                /*if($cliente->save())
                    echo '<p>Cliente atualizado ('.$cliente->id.')</p>';
                else{
                    echo '<p>Erro ao tentar salvar o cliente id '.$cliente->id.'</p>';
                    print_r($cliente->getErrors());
                }*/
            }
        }
    }

    /**
     * Renovação - percorrer registros para pesquisar o ID do produto pelas tags. Gerar base64 do ID do produto e salvar o ID limpo, o código com base64 documento do comprador com base64
    */
    public function actionRenovacao2(){
        $clientes=Renovacao::find()->where(['bloco'=>'111'])->all();
        $apiProduto=new ApiProduto;
        foreach($clientes as $cliente){
            $produto = $apiProduto->buscarProduto(json_encode(
                [
                    "tags"=>[
                        /*/
                        ["nome"=>"matriz","valor"=>"base"],
                        /*/
                        ["nome"=>"matriz","valor"=>"temporada2018"],
                        //*/
                        ["nome"=>"bloco","valor"=>$cliente->bloco],
                        ["nome"=>"fileira","valor"=>$cliente->fileira],
                        ["nome"=>"cadeira","valor"=>$cliente->assento],
                    ],
                ])
            );

            if($produto->successo==0){
                echo 'Falha procurando o produto:';
                print_r($cliente->id);
                echo '<hr />';
            }
            else{    
                $cliente->codproduto=$produto->objeto[0]->codigo;
                $cliente->produtobase64=base64_encode($produto->objeto[0]->codigo);
                $cliente->cpfbase64=base64_encode($cliente->cpf);
                $cliente->url_compra='https://vendas.estadiomineirao.com.br/index.php?r=site%2Ftemporada&id=810&area=Oeste&reserva='.$cliente->produtobase64.'&usuario='.$cliente->cpfbase64;

                /*echo '<pre>';
                print_r('código do produto: '.$cliente->codproduto);
                echo '<br />';
                print_r('código produto base 64: '.$cliente->produtobase64);
                echo '<br />';
                print_r('CPF: '.$cliente->cpf);
                echo '<br />';
                print_r('CPF base 64: '.$cliente->cpfbase64);
                echo '</pre>';
                
    
                echo '<hr />';*/

                if($cliente->save())
                    echo '<p>Cliente atualizado ('.$cliente->id.')</p>';
                else{
                    echo '<p>Erro ao tentar salvar o cliente id '.$cliente->id.'</p>';
                    print_r($cliente->getErrors());
                }

            }
            
        }
    }
    
    public function actionLeste()
    {
        $arrEmail = ['adrumond@ourominas.com.br', 'alexarui@hotmail.com', 'advjvanio@gmail.com', 'andresanonato@hotmail.com', 'claralaughton@gmail.com',
            'lucasfragareis@gmail.com', 'miltoncaires@hotmail.com', 'cleuner.alves@hotmail.com', 'fer003@icloud.com', 'ferramentaria@protominas.com.br', 
            'adaoinacio1950@gmail.com', 'leo@sisloc.com', 'zesilveira4@gmail.com', 'glauberrfs@gmail.com', 'lavajatojosie@hotmail.com', 
            'claudetema@gmail.com', 'pedrobrasilvieira@hotmail.com', 'wallaceji27@hotmail.com'];
        foreach ($arrEmail as $email) {
            echo 'e-mail: ' . $email . ' - base64: ' . base64_encode($email) . '<br>';
        }
//        $clientes = Renovacao::find()->where(['setor' => 'Mineirão Cativa'])->all();
//        if ($clientes) {
//            foreach ($clientes as $cliente) {
//                $cliente->url_compra = 'http://easyp.com.br/mineirao/html/index.php?r=site%2Ftemporada&id=808&area=Leste&reserva=' . $cliente->produtobase64 . '&usuario=' . base64_encode($cliente->email);
////                $cliente->url_compra = 'https://vendas.estadiomineirao.com.br/index.php?r=site%2Ftemporada&id=810&area=Leste&reserva=' . $cliente->produtobase64 . '&usuario=' . base64_encode($cliente->email);
//                if ($cliente->save()) {
//                    echo '<p>Cliente atualizado (' . $cliente->id . '). Url: ' . $cliente->url_compra . '</p>';
//                } else {
//                    echo '<p>Erro ao tentar salvar o cliente id ' . $cliente->id . '</p>';
//                    print_r($cliente->getErrors());
//                }
//            }
//            die();
//        }
//        echo 'Não há registros.';
    }

    /**
     * Correção de tags para temporada2018 área Leste
     * primeiro remover a tag matriz (cujo valor era base)
     * depois clonar a tag matriz2/temporada2018 para matriz/temporada2018 MANUALMENTE PELA API
     */
    public function actionTagsmatriz($pag=0){
        $apiProduto = new ApiProduto(); 
        $produtos = $apiProduto->buscarProduto(json_encode(
                [
                    "tags"=>[
                        
                        ["nome"=>"matriz2","valor"=>"temporada2018"]
                    ],
                    "registro_inicial"=>500*($pag),
                ])
        );
        foreach ($produtos->objeto as $p) {
            $removerTag=new ApiProduto();
            //$excluir=$removerTag->excluirTagProduto(['codigo'=>$p->codigo,'nome'=>'matriz']);
            if($excluir->successo==1)
                echo 'removida tag produto '.$p->codigo.'<br />';
            else
                echo '<b>ERRO removendo tag - produto '.$p->codigo.'</b><br />';
        }
    }

    /**
     * Alterar nomes de produtos - área roxa = tribunas, tribunas vip inferior e camarote brahma; área vermelha = embaixadas
     */
    public function actionAlterarNomesProdutos($pag=1){
        $apiProduto = new ApiProduto(); 
        $produtos = $apiProduto->buscarProduto(json_encode(
                [
                    "tags"=>[                        
                        ["nome"=>"matriz","valor"=>"temporada2018"],
                        ["nome"=>"tipo","valor"=>"assento"],
                        ["nome"=>"bloco","valor"=>"vip106"]
                    ],
                    "registro_inicial"=>500*($pag-1),
                ])
        );
        echo '<small>';
        foreach ($produtos->objeto as $p) {
            $nomeOk=str_replace(['Tribuna', ' VIP'], ['Cadeira', ''], $p->nome);
            //*
            $modelProduto=new ApiProduto;
            $alterar=$modelProduto->alterarProduto(['codigo'=>$p->codigo,'nome'=>$nomeOk]);
            print_r($alterar);echo "<br />";
            /*/
            echo $p->nome.' -> '.$nomeOk;echo '<br />';
            //*/
        }
        echo '</small>';

        /*$apiProduto = new ApiProduto(); 
        $produtos = $apiProduto->buscarProduto(json_encode(
                [
                    "tags"=>[                        
                        ["nome"=>"tipo","valor"=>"assento"],
                        ["nome"=>"bloco","valor"=>"vip107"],
                    ],
                    "registro_inicial"=>500*($pag-1),
                ])
        );
        echo '<small>';
        foreach ($produtos->objeto as $p) {
            $modelProduto=new ApiProduto;
            $nomeOk=str_replace('Camarote Brahma', 'bloco VIP', $p->nome);
            //echo $nomeOk.'<br />';
            $alterar=$modelProduto->alterarProduto(['codigo'=>$p->codigo,'nome'=>$nomeOk]);
            print_r($alterar);echo '<br />';
        }
        echo '</small>';*/

        /*$apiProduto = new ApiProduto(); 
        $produtos = $apiProduto->buscarProduto(json_encode(
                [
                    "tags"=>[                        
                        ["nome"=>"tipo","valor"=>"assento"],
                        ["nome"=>"bloco","valor"=>"vip203"],
                    ],
                    "registro_inicial"=>500*($pag-1),
                ])
        );
        echo '<small>';
        foreach ($produtos->objeto as $p) {
            $modelProduto=new ApiProduto;
            $nomeOk=str_replace('bloco VIP', 'Camarote Brahma', $p->nome);
            //echo $nomeOk.'<br />';
            $alterar=$modelProduto->alterarProduto(['codigo'=>$p->codigo,'nome'=>$nomeOk]);
            print_r($alterar);echo '<br />';
        }
        echo '</small>';*/
        /*$apiProduto = new ApiProduto(); 
        $produtos = $apiProduto->buscarProduto(json_encode(
                [
                    "tags"=>[                        
                        ["nome"=>"tipo","valor"=>"assento"],
                        ["nome"=>"area","valor"=>"Leste"],
                        ["nome"=>"bloco","valor"=>"135"],
                        ["nome"=>"fileira","valor"=>"X"],
                    ],
                    "registro_inicial"=>500*($pag-1),
                ])
        );*/
        /*echo '<small>';
        foreach ($produtos->objeto as $p) {
            $modelProduto=new ApiProduto;
            $nomeOk=str_replace('cativa cativa', 'cativa', $p->nome);
            $alterar=$modelProduto->alterarProduto(['codigo'=>$p->codigo,'nome'=>$nomeOk]);
            print_r($alterar);echo '<br />';
            echo $p->nome.'<br />';
        }
        echo '</small>';*/
    }

    /**
     * Alterar descotno de produtos
     */
    public function actionAlterarDescontoProdutos($pag=1){
        $apiProduto = new ApiProduto(); 
        $produtos = $apiProduto->buscarProduto(json_encode(
                [
                    "desconto"=>["fixo"=>"0.00"],
                    "tags"=>[                        
                        ["nome"=>"matriz","valor"=>"temporada2018"],
                        ["nome"=>"area","valor"=>"Oeste"],
                        ["nome"=>"tipo","valor"=>"assento"],
                    ],
                ])
        );
        echo '<small>';
        foreach ($produtos->objeto as $p) {
            
            //*
            $modelProduto = new ApiProduto();
            $alterar=$modelProduto->alterarProduto(['codigo'=>$p->codigo,
                'desconto'=>['fixo'=>'200']
            ]);
            print_r($alterar);echo "<br />";
            /*/
            echo $p->codigo.': '.$p->nome;echo '<br />';
            //*/
        }
        echo '</small>';
    }

    /**
     * Listar produtos dos carrinhos
     */
    public function actionProdutosVenda(){
        $vendas=[100558,100525,100524,100471,100461,100441,100433,100418,100406,100342,100290,100289,100211];
        foreach ($vendas as $v) {
            $apiCarrinho=new ApiCarrinho;
            $objVenda=$apiCarrinho->consultarVendasFiltro(['codigo'=>$v]);
            if($objVenda->successo==0) {echo 'erro ao consultar venda cod '.$v; continue;}
            echo '<pre>';
            echo '<strong>Venda: '.$v.'</strong> | <strong>Carrinho: '.$objVenda->objeto[0]->id_carrinho.'</strong><br>';
            $carrinho=$apiCarrinho->buscarProdutosVenda($v);
            foreach($carrinho->objeto->tags as $tag){
                $nomeTag=explode('_', $tag->nome);
                echo 'Produto: '.$nomeTag[0];
                echo '<br>';
                echo $nomeTag[1].': '.$tag->valor;
                echo '<br>';
                echo '<br>';
            }
            echo '</pre>';
        }
    }
    
    /**
     * Reativar 80 cadeiras bloqueadas do bloco 110
     * Solicitação da Bruna - 23/01/2018 - Marco
     */
    public function actionBloco110 () 
    {
        $produto = new ApiProduto();
        $arrLugares = [
//            /* FILEIRA A - 17 LUGARES */
//            20669, 20671, 20672, 20673, 20675, 20676, 20677, 20678, 20679, 20680, 20681, 20682, 20684, 20687, 20688, 20689, 20693,
//
//            /* FILEIRA B - 16 LUGARES */
//            20694, 20695, 20696, 20697, 20700, 20701, 20704, 20706, 20707, 20708, 20709, 20714, 20716, 20717, 20718, 20719, 
//
//            /* FILEIRA C - 12 LUGARES */
//            20720, 20721, 20723, 20724, 20725, 20726, 20727, 20728, 20733, 20740, 20742, 20743,
//
//            /* FILEIRA D - 17 LUGARES*/
//            20748, 20751, 20753, 20754, 20755, 20756, 20757, 20758, 20759, 20761, 20762, 20763, 20764, 20765, 20767, 20769, 20770, 
//
//            /* FILEIRA E - 19 LUGARES */
//            20775, 20778, 20779, 20780, 20781, 20782, 20783, 20784, 20785, 20786, 20787, 20788, 20789, 20793, 20794, 20795, 20796, 20798, 
            




            //20800,

            /* FILEIRA F - 13 LUGARES*/
            //20803, 20804, 20807, 20810, 20811, 20812, 20813, 20814, 20815, 20816, 20819, 20826, 20827,

            /* FILEIRA G - 6 LUGARES */
            //20828, 20829, 20830, 20831, 20832, 20833
        ];
        
        echo '<pre>';
        foreach($arrLugares as $codigo) {
            echo 'código: ' . $codigo . '<br>';
            $produto->alterarProduto([
                'codigo' => $codigo,
                'ativo' => '1'
            ]);
            $returnApi = $produto->buscarProduto(null, $codigo);
            if ($returnApi->successo == '1') {
                echo 'status: ' . $returnApi->objeto[0]->ativo . '<br>';
            } else {
                echo 'falha ao consultar produto<br>';
            }
            usleep(100);
            echo '<hr>';
        }
        echo '</pre>';
    }

     /**
      * Listar produtos para criar objeto JS do mapa de assentos matriz base
      * filtrado por tags
      */
    public function actionListaProdutosJs($pag=1){
        $apiProduto = new ApiProduto(); 
        $produtos = $apiProduto->buscarProduto(json_encode(
                [
                    "tags"=>[
                        ["nome"=>"bloco","valor"=>"101"],
                        ["nome"=>"tipo","valor"=>"assento"],
                        ["nome"=>"matriz","valor"=>"base"],
                    ],
                    "registro_inicial"=>500*($pag-1),
                ])
        );
        if($produtos->successo){
            $js=json_encode($produtos->objeto,JSON_PRETTY_PRINT);
            file_put_contents(Yii::getAlias('@webroot').'/js/mapa-geral.js', print_r($js,1), FILE_APPEND);
        }
    }
    
    public function actionBloco128 () 
    {
        $produtos = (new ApiProduto)->buscarProduto(json_encode([
            'tags' => [
                ['nome' => 'matriz', 'valor' => 'temporada2018'],
                ['nome' => 'area', 'valor' => 'leste'],
                ['nome' => 'tipo', 'valor' => 'assento'],
                ['nome' => 'bloco', 'valor' => '128'],
            ]
        ]));     
        echo '<pre>';
        foreach ($produtos->objeto as $index => $produto) {
            $arrTmp = explode(' ', $produto->nome);
            echo 'assento-' . $produto->codigo . ' - ' . $arrTmp[3] . $arrTmp[1] . '<br>';
        }
        echo '</pre>';
    }
    
    public function actionEstoqueBloco127 () 
    {
        $produtos = (new ApiProduto)->buscarProduto(json_encode([
            'tags' => [
                ['nome' => 'matriz', 'valor' => 'temporada2018'],
                ['nome' => 'area', 'valor' => 'leste'],
                ['nome' => 'tipo', 'valor' => 'assento'],
                ['nome' => 'bloco', 'valor' => '127'],
            ]
        ]));     
        echo '<pre>';
        $i=0;
        foreach ($produtos->objeto as $index => $produto) {
            $modelProduto=new ApiProduto;       
            $update['codigo']=$produto->codigo;
            $update['tid']=time()+$i*30;
            $update['unidades']=1;
            /*print_r($update);
            echo '<hr>';
            print_r($produto->codigo.' - '.$produto->nome);
            echo '<br>';
            print_r('Oferecido '.$produto->estoque->oferecido);
            echo '<hr>';
            echo '<hr>';*/
            $i++;
            $alterarEstoque=$modelProduto->adicionarEstoqueProduto($update);
        }
        echo '</pre>'; exit();
    }
}
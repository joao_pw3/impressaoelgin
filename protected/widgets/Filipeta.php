<?php
namespace app\widgets;
use \yii\bootstrap\Widget;
use app\models\Ingressos;
use app\models\Compras;
use app\models\Tags;
use app\models\Conta;
        
class Filipeta extends Widget
{
    public static function geraExibicao($compra)
    {
        $model = new Compras(new Conta);
        $model->getVouchers($compra);  
		$ocupantes= (array) (new Tags)->objetoTags($model->carrinho->tags);
		$campos = ['documento','nome','email','telefone'];     

        $html = '';
        if (isset($model->retorno_conciliacao->receipt->customerReceipt)) {
            $html .= '<pre>'.$model->retorno_conciliacao->receipt->customerReceipt.'</pre>';
        }
        foreach ($model->carrinho->produtos as $produto) {
        	$cupom = isset($produto->cupom->id) ? '_'.$produto->cupom->id : '';
            $cupomNome = isset($produto->cupom->nome) ? $produto->cupom->nome : 'Inteira';
        	$produtoCupom = $produto->codigo.$cupom;
        	if(!isset($arrayProduto[$produtoCupom]))
                $arrayProduto[$produtoCupom]=1;  

            $html .='<div class="row">';
            foreach ($produto->qrcodes as $qrcode) { 
                $code=(new Ingressos($model))->getQrCode($qrcode->qrcode)->writeDataUri();
                $string = null;
                foreach ($campos as $value) {
                    $keyOcupante = isset($ocupantes[$produtoCupom.'_'.$arrayProduto[$produtoCupom].'_'.$value]) ? $ocupantes[$produtoCupom.'_'.$arrayProduto[$produtoCupom].'_'.$value] : false;

                    if($keyOcupante!==false)
                        $string.= ucfirst($value).' : '. $keyOcupante.'<br/>';
                }
            $html .= '
                <div>
					<table>
						<tr>
							<td>
								<img src="'.$code.'" />
							</td>
						</tr>
						<tr>
							<td>
								<h2>
									'.$produto->agenda->nome.'<br>
									'.$produto->agenda->horario.'
									<hr>                    
									'.$model->carrinho->cobranca->vendedor->nome.'
								</h2>    
								<strong>'.$produto->nome.' - '.$cupomNome.'</strong><br>
								<strong>Identificador - '.$model->carrinho->cobranca->codigo.'</strong><br>
								<strong>Ocupante:</strong><br/>
								'.$string.'								
							</td>
						</tr>
						<tr>
							<td>
								<p>Este é seu ingresso. Apresente-o na entrada com seu documento e guarde para localizar seu assento</p>
							</td>
						</tr>
					</table>
                </div>';             
            }
        $html .= '</div>';
        	$arrayProduto[$produtoCupom]+=$arrayProduto[$produtoCupom];
        }
        return $html;
    }
}
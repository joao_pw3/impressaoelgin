<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 *
 */
class BoletoModel extends Model
{
    public $nome;
    public $documento;
    public $email;
    public $cep;
    public $endereco;
    public $numero;
    public $complemento;
    public $bairro;
    public $municipio;
    public $uf;
    public $valor;
    public $vencimento;
    public $mensagem;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // fields are required
            [['nome', 'documento', 'email', 'cep', 'endereco', 'numero', 'bairro', 'municipio', 'uf', 'valor', 'vencimento'], 'required'],
            [['complemento', 'mensagem'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'nome' => 'Nome Completo / Razão Social',
            'documento' => 'Documento',
            'email' => 'E-mail',
            'cep' => 'CEP',
            'endereco' => 'Endereço',
            'numero' => 'Nº',
            'complemento' => 'Complemento',
            'bairro' => 'Bairro',
            'municipio' => 'Cidade',
            'uf' => 'UF',
            'valor' => 'Valor',
            'vencimento' => 'Vencimento',
            'mensagem' => 'Mensagem',
        ];
    }    
}

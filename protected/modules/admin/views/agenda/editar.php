<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\AdminAgendaAsset;
use kartik\datetime\DateTimePicker;
AdminAgendaAsset::register($this);
$this->title = 'ADM Easy for Pay';
        
$form = ActiveForm::begin(['id' => 'agenda-form']); ?>

<div class="container">
	<div class="row">
		<h2 class="tituloGeral"><i class="fa fa-clone"></i> Agenda - Editar Evento</h2>
	</div>
	
	<?= $form->field($model, 'nome') ?>
	<div div class="col-md-6" style="padding-left:0px;">
		<?= $form->field($model, 'descricao')->textarea(['rows' =>5]) ?>
	</div>
	<!-- <div div class="col-md-6" style="padding-right:0px;">
		<?= $form->field($model, 'sinopse')->textarea(['rows' =>5]) ?>
	</div>
	<br clear="all">
	<div div class="col-md-6" style="padding-left:0px;">
		<?= $form->field($model, 'direcao')->textarea(['rows' =>3]) ?>
	</div>
	<div div class="col-md-6" style="padding-right:0px;">
		<?= $form->field($model, 'atores')->textarea(['rows' =>3]) ?>
	</div>
	<br clear="all">
	<div div class="col-md-6" style="padding-left:0px;">
		<?= $form->field($model, 'premios')->textarea(['rows' =>3]) ?>
	</div>
	<div div class="col-md-6" style="padding-right:0px;">
		<?= $form->field($model, 'classificacao')->textarea(['rows' =>3]) ?>
	</div> -->
	<div div class="col-md-4" style="padding-left:0px;">
		<?= $form->field($model, 'duracao') ?>
	</div>
	<br clear="all">
	<div class="col-md-12 semPleft">
		<h5>Qual o período em que o evento ficará em cartaz?</h5>
	</div>
	<div class="col-md-6 semPleft">
	<?= $form->field($model, 'dataInicial')->widget(DateTimePicker::className(), [
		'type' => DateTimePicker::TYPE_COMPONENT_APPEND,//mostrar botões no fim do input (há outras opções)
		'readonly' => true,// impede o usuário de escrever (evita erros de digitação)
		'pluginOptions' => [
			'todayBtn' => true,// destaca o dia atual no calendário
			'autoclose' => true,// fecha o calendário automaticamente no final das escolhas do cliente
			'format' => 'dd/mm/yyyy hh:ii', // formato da data (já coloquei no formato da API)
			'startDate' => date('d/m/Y H:i'), // bloquear datas anteriores ao dia atual
		]

	]); ?>
	</div>
	<div class="col-md-6" style="padding-right:0px;">
	<?= $form->field($model, 'dataFinal')->widget(DateTimePicker::className(), [
		'type' => DateTimePicker::TYPE_COMPONENT_APPEND,//mostrar botões no fim do input (há outras opções)
		'readonly' => true,// impede o usuário de escrever (evita erros de digitação)
		'pluginOptions' => [
			'todayBtn' => true,// destaca o dia atual no calendário
			'autoclose' => true,// fecha o calendário automaticamente no final das escolhas do cliente
			'format' => 'dd/mm/yyyy hh:ii', // formato da data (já coloquei no formato da API)
			'startDate' => date('d/m/Y H:i'), // bloquear datas anteriores ao dia atual
		]

	]); ?>
	</div>
	<br clear="all">
	<div class="col-md-12 semPleft">
		<h5>Definir quando o evento começa a ser exibido no site (opcional)</h5>
	</div>
	<div class="col-md-6 semPleft">
	<?= $form->field($model, 'dataAtivar')->widget(DateTimePicker::className(), [
		'type' => DateTimePicker::TYPE_COMPONENT_APPEND,//mostrar botões no fim do input (há outras opções)
		'readonly' => true,// impede o usuário de escrever (evita erros de digitação)
		'pluginOptions' => [
			'todayBtn' => true,// destaca o dia atual no calendário
			'autoclose' => true,// fecha o calendário automaticamente no final das escolhas do cliente
			'format' => 'dd/mm/yyyy hh:ii', // formato da data (já coloquei no formato da API)
			'startDate' => date('d/m/Y H:i'), // bloquear datas anteriores ao dia atual
		]

	]); ?>
	</div>
	<div class="col-md-6" style="padding-right:0px;">
	<?= $form->field($model, 'dataDesativar')->widget(DateTimePicker::className(), [
		'type' => DateTimePicker::TYPE_COMPONENT_APPEND,//mostrar botões no fim do input (há outras opções)
		'readonly' => true,// impede o usuário de escrever (evita erros de digitação)
		'pluginOptions' => [
			'todayBtn' => true,// destaca o dia atual no calendário
			'autoclose' => true,// fecha o calendário automaticamente no final das escolhas do cliente
			'format' => 'dd/mm/yyyy hh:ii', // formato da data (já coloquei no formato da API)
			'startDate' => date('d/m/Y H:i'), // bloquear datas anteriores ao dia atual
		]

	]); ?>
	</div>
	<div class="col-md-12" style="padding-right:0px;">
		<?= $form->field($model, 'ativo')->radioList(['1'=>'Sim','0'=>'Não']) ?>
	</div>
	<div class="form-group" style="text-align:center;">
		<?= Html::submitButton('Salvar', ['class' => 'btn btn-primary btn-pagar']) ?>
	</div>

	<?php ActiveForm::end(); ?>
	<?php $this->registerJs("
	  $('#agenda-duracao').mask('00:00:00');
	", \yii\web\View::POS_READY);
	?>
	<br>
	<br>
	<br>
</div>